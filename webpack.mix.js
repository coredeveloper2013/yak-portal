let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js/app.js');
mix.js('resources/js/app-vue.js', 'public/js/app-vue.js');
mix.js('resources/js/_v2/main.js','public/js/_v2/main.js');
mix.sass('resources/sass/app.scss', 'public/css/app.css');
mix.sass('resources/sass/_v2/main.scss', 'public/css/_v2/main.css');

mix.js('resources/views/campaigns/vue.js', 'public/js/campaigns/vue.js');
mix.js('resources/views/landing/vue.js', 'public/js/landing/vue.js');
mix.js('resources/views/billing/vue.js', 'public/js/billing/vue.js');
mix.js('resources/views/agreement/vue.js', 'public/js/agreement/vue.js');
mix.js('resources/views/staff/landing/vue.js', 'public/js/staff/landing/vue.js');
mix.js('resources/views/staff/sales/vue.js', 'public/js/staff/sales/vue.js');
mix.js('resources/views/staff/tasks/vue.js', 'public/js/staff/tasks/vue.js');
mix.js('resources/views/staff/tasks/add/vue.js', 'public/js/staff/tasks/add_vue.js');
mix.js('resources/views/other_projects/vue.js', 'public/js/other_projects/vue.js');
mix.js('resources/views/staff/other_projects/vue.js', 'public/js/staff/other_projects/vue.js');
mix.js('resources/views/staff/campaign/vue.js', 'public/js/staff/campaign/vue.js');
mix.js('resources/views/staff/clientmanager/vue.js', 'public/js/staff/clientmanager/vue.js');
mix.js('resources/views/staff/retention/vue.js', 'public/js/staff/retention/vue.js');
mix.js('resources/views/staff/agreement/vue.js', 'public/js/staff/agreement/vue.js');
mix.js('resources/views/billing/vue-detail.js', 'public/js/billing/vue-detail.js');
mix.js('resources/views/staff/invoice/vue.js', 'public/js/staff/invoice/vue.js');

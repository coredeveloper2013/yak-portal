<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class LandingQuestionAnswers extends Model
{
    use Notifiable;

    protected $fillable = ['landing_pages_id', 'question', 'answer'];

    public function landing_page(){
        return $this->belongsTo(LandingPage::class, 'landing_pages_id');
    }
}

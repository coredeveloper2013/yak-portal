<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Agreement extends Model
{
    use Notifiable;

    protected $fillable = ['user_id', 'campaign_id', 'name', 'date', 'amount', 'status'];

    protected $appends = ['human_date'];

    public function getStatusAttribute($value){
        return strtoupper($value);
    }

    public function getHumanDateAttribute(){
        return Carbon::parse($this->getOriginal('date'))->toFormattedDateString();
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function campaign(){
        return $this->belongsTo(Campaign::class);
    }

    public function document(){
        return $this->hasOne(AgreementDocuSign::class, 'module_id', 'id');
    }
}

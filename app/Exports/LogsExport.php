<?php

namespace App\Exports;
use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class LogsExport implements FromCollection, WithHeadings
{
    use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    private $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function collection()
    {
        $users = new \App\User();
        if($this->request->filter['status']){
            $users = $users->whereStatus($this->request->filter['status']);
        }

        if($this->request->filter['queryString']){
            $users = $users->where("name" , "LIKE" , "%".$this->request->filter['queryString']."%");
        }
        $users = $users->whereType('customer')->whereId( $this->request->user_id);
        $logSearch = $this->request->logFilter;
        $users = $users->with(['request_logs','request_logs.logDetails'])->orderBy('created_at', 'DESC')->first();
        //dd($this->user_id);
        $exportArray = [];
        if ($users->request_logs)
        {
            foreach ($users->request_logs as $log)
            {
                if ($log->logDetails)
                {
                    $content = json_decode($log->logDetails->content);
                    $temp_log = [];
                    $temp_log['ip_address'] = $content->ip_address;
                    $temp_log['url'] = URL($content->uri);
                    $temp_log['Name'] = $users->name;
                    $temp_log['date'] = $log->logDetails->created_at->format('d/M/Y');
                    $temp_log['time'] = $log->logDetails->created_at->format('H:m');

                    $exportArray[] = $temp_log;

                }
            }
        }

        return Collect($exportArray);


    }
    public function headings(): array
    {
        return [
            'IP Address',
            'URL',
            'Name',
            'Date',
            'Time'
        ];
    }
}

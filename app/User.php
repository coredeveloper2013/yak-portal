<?php

namespace App;

use App\Models\Appointment;
use App\Models\Staff\Campaign as StaffCampaign;
use App\Models\Staff\Holiday as StaffHoliday;
use App\Models\Staff\Task as StaffTask;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, Billable, HasApiTokens, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'phone', 'assigned_to_id', 'email', 'password', 'image', 'job_position', 'type', 'status', 'rights', 'no_of_leave', 'member_color', 'created_at'
    ];

    protected $appends = ['image_url'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'stripe_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function company()
    {
        return $this->hasOne(Company::class);
    }

    public function userHoliday()
    {
        return $this->hasOne(StaffHoliday::class);
    }

    public function campaigns()
    {
        return $this->hasMany(Campaign::class);
    }

    public function staffCampaigns()
    {
        return $this->hasMany(StaffCampaign::class);
    }

    public function leads()
    {
        return $this->hasMany(Leads::class);
    }

    public function leaddeals()
    {
        return $this->hasMany(LeadDeal::class);
    }

    public function memberNotes()
    {
        return $this->hasMany(Note::class, 'member_id', 'id')->with('client');
    }

    public function notes()
    {
        return $this->hasMany(Note::class)->with('user');
    }

    public function memberAppointments()
    {
        return $this->hasMany(Appointment::class, 'member_id', 'id');
    }

    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }

    public function lead_comments()
    {
        return $this->hasMany(LeadComments::class);
    }

    public function lead_questions()
    {
        return $this->hasMany(LeadQuestion::class);
    }

    public function landing_pages()
    {
        return $this->hasMany(LandingPage::class);
    }

    public function assigned_landing_pages()
    {
        return $this->hasManyThrough(LandingPage::class, Invoice::class, 'user_id', 'assigned_to', 'id');
    }

    public function agreement_landing_page()
    {
        return $this->hasOne(LandingPage::class);
    }

    public function user_landing_page()
    {
        return $this->hasOne(LandingPage::class)->with('landing_question_answers');
    }

    public function assigned_campaigns()
    {
        return $this->hasManyThrough(Campaign::class, Invoice::class, 'user_id', 'assigned_id');
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class)->latest('created_at');
    }

    public function getOutstandingInvoiceAmount()
    {
        return $this->hasMany(Invoice::class)->where('status', 'pending')->sum('total_amount');
    }

    public function clientPaymentInvoices()
    {
        return $this->hasMany(Invoice::class)
            ->orderBy('paid_date', 'DESC')
            ->where('status', 'completed');
    }
    public function clientInvoices()
    {
        return $this->hasMany(Invoice::class)
            ->groupBy('user_id')
            ->selectRaw('user_id, id, SUM(total_amount) as totalAmount, COUNT(id) as totalRow');
    }

//    public function clientComments(){
//        return $this->hasMany(Comment::class)
//            ->groupBy('user_id')
//            ->selectRaw('user_id, COUNT(id) as totalComments');
//    }

    public function lastComments()
    {
        return $this->hasOne(Comment::class)
            ->latest();
    }

    public function agreements()
    {
        return $this->hasMany(Agreement::class);
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }

    public function other_projects()
    {
        return $this->hasMany(OtherProject::class);
    }

    public function tasks()
    {
        return $this->belongsToMany(Task::class, 'user_tasks', 'user_id', 'task_id');
    }

    public function staffTasks()
    {
        return $this->belongsToMany(StaffTask::class, 'user_tasks', 'user_id', 'task_id');
    }

    public function taskComments()
    {
        return $this->hasMany(Comment::class);
    }

    public function campaignComments()
    {
        return $this->hasMany(CampaignComments::class, 'user_id', 'id');
    }

    public function landingComments()
    {
        return $this->hasMany(LandingPageComments::class, 'user_id', 'id');
    }

    public function business_info()
    {
        return $this->hasOne(UserBusinessInformation::class);
    }

    public function docu_sign()
    {
        return $this->hasMany(AgreementDocuSign::class);
    }

    public function salesPerson()
    {
        return $this->hasMany(Campaign::class, 'assigned_id', 'id');
    }

    public function campaignSpecialist()
    {
        return $this->hasMany(Campaign::class, 'campaign_specialist', 'id');
    }

    public function lpSpecialist()
    {
        return $this->hasMany(Campaign::class, 'lp_specialist', 'id');
    }

    public function accounManager()
    {
        return $this->hasMany(Campaign::class, 'account_manager', 'id');
    }

    public function campaignCreatedBy()
    {
        return $this->hasMany(Campaign::class, 'created_by', 'id');
    }

    public function getUserDataById($id = '')
    {
        if (empty($id)) {
            return [];
        }
        $user = DB::select("SELECT u.id, u.`name`, u.`email`, u.`image`, u.`phone` FROM users u  WHERE u.id=" . $id);
        if (!empty($user)) {
            $company = DB::select("SELECT c.id, c.`name`, c.`address` FROM companies c  WHERE c.user_id=" . $id);
            $user[0]->company = (!empty($company)) ? (array)$company[0] : [];
            return (array)$user[0];
        } else {
            return [];
        }
    }

    public function request_logs()
    {
        return $this->hasMany(TelescopeEntriesTags::class, 'tag', 'id');
    }

    public function userLogsWithLogDetails()
    {
        return $this->hasMany(TelescopeEntriesTags::class, 'tag', 'id')->with('logDetails');
    }

    public function userLogs()
    {
        return $this->hasMany(TelescopeEntriesTags::class, 'tag', 'id');
    }

    public function getImageUrlAttribute()
    {
        if (isset($this->image)) {
            return asset('/storage/avatars/' . $this->image);
        } else {
            return asset('staff/images/user-1.jpg');
        }
    }
}

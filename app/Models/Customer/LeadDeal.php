<?php

namespace  App\Models\Customer;

//use App\LeadDealQuestion;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class LeadDeal extends Model {
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name', 'budget', 'source', 'user_id', 'status', 'phone', 'email', 'point_of_contact', 'active'
    ];
    protected $appends = ['is_assigned'];

    /*public $statusEnums = [
        'needs_chasing' => 'NEED CHASING',
        'follow_up_scheduled' => 'FOLLOW UP SCHEDULED',
        'pending_decision' => 'PENDING DECISION',
        'deal_agreed' => 'DEAL AGREED',
        'onboarding_call_booked' => 'ONBOARDING CALL BOOKED',
        'deal_completed' => 'DEAL COMPLETED',
        'not_right_now' => 'NOT RIGHT NOW'
    ];*/

    public $statusEnums = [
        /*'needs_chasing' => 'Need Chasing',
        'follow_up_scheduled' => 'Follow Up Scheduled',
        'pending_decision' => 'Pending Decision',
        'deal_agreed' => 'Deal Agreed',
        'onboarding_call_booked' => 'Onboarding Call Booked',
        'deal_completed' => 'Deal Completed',
        'not_right_now' => 'Not Right Now',*/

        'untouched' => 'Untouched',
        'attempted' => 'Attempted',
        'information_sent' => 'Information Sent',
        'appointment_scheduled' => 'Appointment Scheduled',
        'quoted' => 'Quoted',
        'sold' => 'Sold',
        'not_interested' => 'Not Interested',
        'deal_lost' => 'Deal Lost'
    ];

    public function LeadDealQuestion(){
        return $this->hasMany(LeadDealQuestion::class,'lead_deal_id','id');
}
    public function user(){
        return $this->belongsTo(User::class);
}

    public function getIsAssignedAttribute(){
        if(!empty($this->getOriginal('user_id'))){
            return 'Y';
        }
        return "N";
    }

    public static function get_deals_count_by_user_id($user_id) {
        if (Auth::user()->type == 'admin')
        {
            if ($user_id == -1)
            {
                return self::select('user_id', 'status', DB::raw('COUNT(*) AS deals_count'), DB::raw('SUM(budget) AS budget_count'))
                    ->where('active', 1)->groupby('status')->get();
            }
            else{
                return self::select('user_id', 'status', DB::raw('COUNT(*) AS deals_count'), DB::raw('SUM(budget) AS budget_count'))
                    ->where('user_id', $user_id)->where('active', 1)->groupby('status')->get();
            }

        }
        else {
            return self::select('user_id', 'status', DB::raw('COUNT(*) AS deals_count'), DB::raw('SUM(budget) AS budget_count'))
                ->where('user_id', $user_id)->where('active', 1)->groupby('status')->get();
        }
    }

    public static function get_deals_by_user_id($user_id, $status) {
        if (Auth::user()->type == 'admin')
        {
            if ($user_id == -1)
            {
                $result = self::select('id', 'user_id', 'name', 'budget', 'source', 'status', 'phone', 'email', 'point_of_contact', 'created_at', 'active')
                    ->where('status', $status)
                    ->where('active', 1)
                    ->get();
            }
            else{
                $result = self::select('id', 'user_id', 'name', 'budget', 'source', 'status', 'phone', 'email', 'point_of_contact', 'created_at', 'active')
                    ->where('user_id', $user_id)
                    ->where('status', $status)
                    ->where('active', 1)
                    ->get();
            }

        }
        else {
            $result = self::select('id', 'user_id', 'name', 'budget', 'source', 'status', 'phone', 'email', 'point_of_contact', 'created_at', 'active')
                ->where('user_id', $user_id)
                ->where('status', $status)
                ->where('active', 1)
                ->get();
        }

        if (!empty($result)) {
            foreach ($result as $key => $value) {
                $query =    "SELECT * FROM `lead_deals_questions` ld WHERE ld.`lead_deal_id`=".$value->id." limit 1";
                $question = DB::select($query);
                $result[$key]->questions = (!empty($question)) ? $question : [];
            }
        }
        return $result->toArray();
    }

    public static function get_deals_data($user_id, $search = '', $linked='') {

        $query =    "SELECT
                ld.`id`,
                ld.`user_id`,
                ld.`name`,
                ld.`budget`,
                ld.`source`,
                ld.`status`,
                ld.`phone`,
                ld.`email`,
                ld.`point_of_contact`,
                ld.`created_at`,
                ld.`active`
            FROM
                `lead_deals` ld
                INNER JOIN `lead_deals_questions` ldq
                    ON ld.`id` = ldq.`lead_deal_id`
            WHERE ld.`active` = 0
                AND ld.`deleted_at` IS NULL ";

        if ($linked == 'yes') {
            $query .= " AND ld.`user_id` = ".$user_id;
        } else if ($linked == 'no') {
            $query .= " AND ld.`user_id` IS NULL ";
        }

        if (!empty($search)) {
            $query .=    " AND ldq.`answer` LIKE '%".$search."%' ";
        }
        $query .= " GROUP BY ld.`id` ORDER BY ld.id DESC ";
        $leads = DB::select($query);

        if (!empty($leads)) {
            foreach ($leads as $key => $value) {
                $query =    "SELECT * FROM `lead_deals_questions` ld WHERE ld.`lead_deal_id`=".$value->id." order by id";
                $question = DB::select($query);
                $leads[$key]->questions = (!empty($question)) ? $question : [];
            }
        }
        return $leads;

        // return $result->toArray();
    }

    public static function get_deals_data_by_id($id) {
        $res = self::select('id', 'user_id', 'name', 'budget', 'source', 'status', 'phone', 'email', 'point_of_contact', 'created_at', 'active')
            ->where('id', $id)->get();

        $query =    "SELECT * FROM `lead_deals_questions` ld WHERE ld.`lead_deal_id`=".$res[0]->id." order by id";
        $question = DB::select($query);
        $res[0]->questions = (!empty($question)) ? $question : [];

        return $res;
    }

    public static function get_deals_data_admin($search = '', $linked='') {

        $query =    "SELECT
                ld.`id`,
                ld.`status`,
                ld.`user_id`,
                ld.`created_at`,
                ld.`name`,
                ld.`active`
            FROM
                `lead_deals` ld
                INNER JOIN `lead_deals_questions` ldq
                    ON ld.`id` = ldq.`lead_deal_id`
            WHERE ld.`active` = 0
                AND ld.`deleted_at` IS NULL ";

        if ($linked == 'yes') {
            $query .= " AND ld.`user_id` IS NOT NULL ";
        } else if ($linked == 'no') {
            $query .= " AND ld.`user_id` IS NULL ";
        }

        if (!empty($search)) {
            $query .=    " AND ldq.`answer` LIKE '%".$search."%' ";
        }
        $query .= " GROUP BY ld.`id` ORDER BY ld.id DESC ";
        $leads = DB::select($query);

        if (!empty($leads)) {
            foreach ($leads as $key => $value) {
                $query =    "SELECT * FROM `lead_deals_questions` ld WHERE ld.`lead_deal_id`=".$value->id." order by id";
                $question = DB::select($query);
                $leads[$key]->questions = (!empty($question)) ? $question : [];
            }
        }

        return $leads;
        // return $result->toArray();
    }

}

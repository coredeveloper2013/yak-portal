<?php

namespace  App\Models\Customer;

use App\LandingPage;
use App\Models\Staff\Campaign;
use App\OtherProject;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Invoice extends Model
{
    use Notifiable;

    protected $fillable = ['user_id', 'campaign_id', 'response', 'landing_pages_id', 'other_project_id', 'type', 'status', 'amount', 'vat', 'total_amount', 'paid_date'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function campaign(){
        return $this->belongsTo(Campaign::class, 'campaign_id');
    }

}

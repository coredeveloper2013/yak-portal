<?php

namespace App\Models\Customer;

use Illuminate\Database\Eloquent\Model;

class LeadDealQuestion extends Model
{
	protected $table = 'lead_deals_questions';
	protected $fillable = ['lead_deal_id', 'question', 'answer', 'created_at', 'updated_at'];

    public function lead(){
        return $this->belongsTo(LeadDeal::class);
    }
}

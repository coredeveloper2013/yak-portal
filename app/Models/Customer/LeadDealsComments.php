<?php

namespace App\Models\Customer;
use Illuminate\Database\Eloquent\Model;
use DB;

class LeadDealsComments extends Model
{
    protected $fillable = [
        'message', 'user_id', 'lead_id'
    ];

    public static function get_comments($id) {

		$users= DB::table('lead_deals_comments as c')
        ->join('users as u', 'u.id', '=', 'c.user_id')
		    ->select('c.*','u.name as userName', 'u.name', 'u.image', 'u.type')
		    ->where('lead_id',$id)->get();

        foreach ( $users as $key => $value) {
            $users[$key]->user = (object)[];
            $users[$key]->user->name = $value->name;
            $users[$key]->user->image = $value->image;
            $users[$key]->user->type = $value->type;
            $value->created =date('F d,Y g:i A',strtotime($value->created_at));
        }

        return $users;
	 }
}

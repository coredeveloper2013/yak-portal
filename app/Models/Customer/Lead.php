<?php

namespace App\Models\Customer;

use App\Campaign;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lead extends Model
{
    use SoftDeletes;

    protected $fillable = ['status', 'submitted_on', 'user_id','campaign_id', 'unbounce_id', 'deleted_at', 'sms_sent', 'email_sent'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function campaign(){
        return $this->belongsTo(Campaign::class);
    }
}

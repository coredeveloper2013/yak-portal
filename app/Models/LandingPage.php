<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class LandingPage extends Model
{
    use Notifiable;

    protected $fillable = ['user_id', 'name', 'logo', 'description', 'url' ,'cost', 'date', 'budget', 'assigned_to','created_by',
        'status', 'trying_to_sell', 'customer_benefit', 'leads_questions', 'leads_contact_details', 'review_pages', 'reasons' ,'reviews'];

    protected $appends = ['human_date'];

    protected $casts = [
        'leads_questions' => 'array',
        'leads_contact_details' => 'array',
        'review_pages' => 'array',
        'reasons' => 'array',
        'reviews' => 'array',
    ];


    /*public function getStatusAttribute($value){
        if(strpos($value, "_") !== false){
            return strtoupper(str_replace("_", " ", $value));
        }
        return strtoupper($value);
    }*/

    public function getAll($request){
        $pages=  self::select('landing_pages.*','u.name as assigned','u1.name as customer','landing_pages.status as statuss');
        if ($request->has('filter')) {
            if ($request->filter['status'] !== null) {
                $pages = $pages->where('landing_pages.status', $request->filter['status']);
            }
        } else if (!empty($request->status_mob)) {
            $pages = $pages->where('landing_pages.status', $request->status_mob);
        }
        if ($request->has('id')) {
            $pages = $pages->where('landing_pages.id', $request->id);
        }
        $pages = $pages->join('users as u','u.id','landing_pages.assigned_to');
        $pages = $pages->join('users as u1','u1.id','landing_pages.user_id');
        $pages = $pages->orderBy('created_at', 'DESC')->paginate();

        return $pages;

    }

    public function getHumanDateAttribute(){
        return Carbon::parse($this->getOriginal('date'))->toFormattedDateString();
    }

    public function campaign(){
        return $this->hasOne(Campaign::class, 'landing_pages_id');
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function assigned_to(){
        return $this->belongsTo(User::class, 'assigned_to');
    }

    public function assigned(){
        return $this->belongsTo(User::class, 'assigned_to');
    }

    public function created_by(){
        return $this->belongsTo(User::class, 'created_by');
    }

    public function invoices(){
        return $this->hasMany(Invoice::class, 'landing_pages_id', 'id');
    }

    public function comments(){
        return $this->hasMany(LandingPageComments::class, 'landing_page_id', 'id');
    }

    public function landing_question_answers(){
        return $this->hasMany(LandingQuestionAnswers::class, 'landing_pages_id', 'id');
    }

}

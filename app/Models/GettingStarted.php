<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class GettingStarted extends Model
{
    protected $fillable = ['title', 'description', 'image', 'is_header'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function member()
    {
        return $this->belongsTo(User::class, 'member_id', 'id');
    }
}

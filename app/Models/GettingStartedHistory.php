<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class GettingStartedHistory extends Model
{
    protected $fillable = ['user_id', 'getting_starteds_id', 'is_marked'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function member()
    {
        return $this->belongsTo(User::class, 'member_id', 'id');
    }
}

<?php

namespace App\Models;

use App\LeadComments;
use App\LeadQuestion;
use App\Models\Staff\Campaign;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoutePermission extends Model
{
    protected $fillable = ['user_id', 'route_id'];
    public function route()
    {
        return $this->belongsTo(Route::class);
    }
}

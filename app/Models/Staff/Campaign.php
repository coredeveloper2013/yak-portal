<?php

namespace App\Models\Staff;

use DB;
use Carbon\Carbon;
use App\Agreement;
use App\CampaignComments;
use App\Company;
use App\Invoice;
use App\InvoiceInstallments;
use App\LandingPage;
use App\Leads;
use App\Notification;
use App\User;
use App\UserBusinessInformation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;


class Campaign extends Model
{
    use Notifiable, SoftDeletes;

    protected $fillable = ['user_id', 'name', 'recurring_option', 'recurring_way', 'start_date', 'end_date', 'landing_pages_id', 'amount', 'type', 'assigned_id', 'account_manager', 'campaign_specialist', 'lp_specialist', 'status', 'created_by', 'updated_by', 'compaign_amount', 'max_add_amount'];

    //make relation between campaign and landing pages
    public function landing_pages()
    {
        return $this->belongsTo(LandingPage::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function salesPerson()
    {
        return $this->belongsTo(User::class, 'assigned_id');
    }
    public function userDetails()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function companyDetails()
    {

        return $this->belongsTo(Company::class, 'user_id', 'user_id');
    }
    public function businessDetails()
    {
        return $this->belongsTo(UserBusinessInformation::class, 'user_id', 'user_id');
    }

    public function campaignSpecialist()
    {
        return $this->belongsTo(User::class, 'campaign_specialist');
    }

    public function lpSpecialist()
    {
        return $this->belongsTo(User::class, 'lp_specialist');
    }

    public function accounManager()
    {
        return $this->belongsTo(User::class, 'account_manager');
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class, 'campaign_id', 'id');
    }

    public function invoice()
    {
        return $this->hasOne(Invoice::class, 'campaign_id', 'id');
    }

    public function lead()
    {
        return $this->hasOne(Leads::class);
    }

    public function notification()
    {
        return $this->hasMany(Notification::class);
    }

    public function agreements()
    {
        return $this->hasMany(Agreement::class);
    }
    public function agreement()
    {
        return $this->hasOne(Agreement::class);
    }
    public function campaignComments()
    {
        return $this->hasMany(CampaignComments::class);
    }

    public function installments()
    {
        return $this->hasMany(InvoiceInstallments::class, 'module_id', 'id')->where('module_type', 'campaigns');
    }
}

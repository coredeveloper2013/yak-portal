<?php

namespace App\Models\Staff;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Task extends Model
{
    protected $fillable = [
        'name',
        'description',
        'assigned_by',
        'due_date',
        'attachments',
        'file_type',
        'status',
        'priority',
        'module_id',
        'link_type'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function userTasks() {
        return $this->hasMany(UserTask::class)->with('user:id,name');
    }

    public function subTasks() {
        return $this->hasMany(SubTask::class);
    }

    public function comment() {
        return $this->hasMany(Comment::class)->latest()->with('user');
    }
}

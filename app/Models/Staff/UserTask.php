<?php

namespace App\Models\Staff;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class UserTask extends Model
{
    use Notifiable;
    protected $fillable = ['task_id', 'user_id'];

    public function user() {
        return $this->belongsTo(User::class);
    }
}

<?php

namespace App\Models\Staff;

use App\Models\Staff\Campaign;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Unbounce extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = "unbounce_link";

    protected $fillable = [
        'unbounce_id', 'campaign_id', 'user_id', 'is_new_bussiness_lead', 'cron_status'
    ];

    public function campaigns()
    {
        return $this->belongsTo(Campaign::class,'campaign_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

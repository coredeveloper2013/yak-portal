<?php

namespace App\Models\Staff;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Holiday extends Model
{
    protected $table = 'holidays';
    protected $fillable = ['userId','description', 'number_of_days', 'date_of_holiday','reason','availed'];

    private function user()
    {
        return $this->belongsTo(User::class, 'userId', 'id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Company extends Model
{
    use Notifiable;

    protected $fillable = ['user_id', 'name', 'address'];

    public function user(){
        return $this->belongsTo(User::class);
    }
}

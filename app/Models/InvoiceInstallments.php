<?php

namespace App\Models;

use Auth;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class InvoiceInstallments extends Model {

	use Notifiable;
    protected $table = "invoice_installments";
    protected $fillable = ['user_id', 'module_id', 'module_type', 'amount', 'installment_date', 'is_sended'];

    public function campaign(){
        return $this->belongsTo(Campaign::class);
    }

    public function projects(){
        return $this->belongsTo(Campaign::class);
    }
}

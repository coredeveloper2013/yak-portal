<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserHoliday extends Model
{
    protected $fillable = ['user_id', 'days'];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

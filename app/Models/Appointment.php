<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    protected $fillable = ['title', 'date', 'from', 'to', 'status', 'user_id', 'member_id', 'description'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function member()
    {
        return $this->belongsTo(User::class, 'member_id', 'id');
    }
}

<?php

namespace App\Models;

//use App\Company;
use App\Models\Company;
use App\Models\Appointment;
use App\Models\Customer\LeadDeal;
use App\Models\Staff\Campaign;
use App\Models\Staff\Campaign as StaffCampaign;
use App\Models\Staff\Holiday as StaffHoliday;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, Billable, HasApiTokens, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'assigned_to_id', 'phone', 'email', 'password', 'image', 'job_position', 'type', 'status', 'rights', 'no_of_leave', 'member_color'
    ];

    protected $appends = ['image_url'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'stripe_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function company()
    {
        return $this->hasOne(Company::class);
    }

    public function userHoliday()
    {
        return $this->hasOne(StaffHoliday::class);
    }

    public function dashboardCompany()
    {
        $data = $this->hasOne(Company::class)->selectRaw('user_id')/*
        ->groupBy('user_id')
        ->selectRaw('user_id, id, COUNT(id) as totalRow')*/;
        //dd($data->count());
        return $this->hasOne(Company::class);
    }

    public function campaigns()
    {
        return $this->hasMany(Campaign::class);
    }

    public function leads()
    {
        return $this->hasMany(Leads::class);
    }
    public function leaddeals()
    {
        return $this->hasMany(LeadDeal::class);
    }

    public function memberAppointments()
    {
        return $this->hasMany(Appointment::class, 'member_id', 'id');
    }

    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }

    public function lead_comments()
    {
        return $this->hasMany(LeadComments::class);
    }

    public function lead_questions()
    {
        return $this->hasMany(LeadQuestion::class);
    }

    public function landing_pages()
    {
        return $this->hasMany(LandingPage::class);
    }

    public function dashboardLandingPages()
    {
        $data = $this->hasMany(LandingPage::class)
            ->groupBy('status')
            ->groupBy('user_id')
            ->selectRaw('user_id, id, status, COUNT(id) as totalRow');
        return $data;
    }

    public function dashboardLandingPagesOne()
    {
        $data = $this->hasOne(LandingPage::class);
        //dd($data->get());
        return $data;
    }

    public function staffCampaigns()
    {
        return $this->hasMany(StaffCampaign::class);
    }

    public function dashboardStaffCampaigns()
    {
        $data = $this->hasMany(StaffCampaign::class)
            ->groupBy('status')
            ->groupBy('user_id')
            ->selectRaw('user_id, id, status, COUNT(id) as totalRow');
        return $data;
    }

    public function assigned_landing_pages()
    {
        return $this->hasManyThrough(LandingPage::class, Invoice::class, 'user_id', 'assigned_to', 'id');
    }

    public function assigned_campaigns()
    {
        return $this->hasManyThrough(Campaign::class, Invoice::class, 'user_id', 'assigned_id');
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function clientInvoices(){
        return $this->hasMany(Invoice::class)
            ->groupBy('user_id')
            ->selectRaw('user_id, id, SUM(total_amount) as totalAmount, COUNT(id) as totalRow');
    }

//    public function clientComments(){
//        return $this->hasMany(Comment::class)
//            ->groupBy('user_id')
//            ->selectRaw('user_id, COUNT(id) as totalComments');
//    }

    public function lastComments(){
        return $this->hasOne(Comment::class)
            ->latest();
    }

    public function agreements(){
        return $this->hasMany(Agreement::class);
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }

    public function other_projects()
    {
        return $this->hasMany(OtherProject::class);
    }

    public function tasks()
    {
        return $this->belongsToMany(Task::class, 'user_tasks', 'user_id', 'task_id');
    }

    public function taskComments()
    {
        return $this->hasMany(Comment::class);
    }

    public function campaignComments()
    {
        return $this->hasMany(CampaignComments::class, 'user_id', 'id');
    }

    public function landingComments()
    {
        return $this->hasMany(LandingPageComments::class, 'user_id', 'id');
    }

    public function business_info()
    {
        return $this->hasOne(UserBusinessInformation::class);
    }

    public function docu_sign()
    {
        return $this->hasMany(AgreementDocuSign::class);
    }

    public function salesPerson()
    {
        return $this->hasMany(Campaign::class, 'assigned_id', 'id');
    }

    public function campaignSpecialist()
    {
        return $this->hasMany(Campaign::class, 'campaign_specialist', 'id');
    }

    public function lpSpecialist()
    {
        return $this->hasMany(Campaign::class, 'lp_specialist', 'id');
    }

    public function accounManager()
    {
        return $this->hasMany(Campaign::class, 'account_manager', 'id');
    }

    public function getUserDataById($id = '')
    {
        if (empty($id)) {
            return [];
        }
        $user = DB::select("SELECT u.id, u.`name`, u.`email`, u.`image`, u.`phone` FROM users u  WHERE u.id=" . $id);
        if (!empty($user)) {
            $company = DB::select("SELECT c.id, c.`name`, c.`address` FROM companies c  WHERE c.user_id=" . $id);
            $user[0]->company = (!empty($company)) ? (array)$company[0] : [];
            return (array)$user[0];
        } else {
            return [];
        }
    }

    public function userLogs()
    {
        return $this->hasMany(TelescopeEntriesTags::class, 'tag', 'id');
    }

    public function getImageUrlAttribute()
    {
        if (isset($this->image)) {
            return asset('/storage/avatars/' . $this->image);
        } else {
            return asset('staff/images/user-1.jpg');
        }
    }
}

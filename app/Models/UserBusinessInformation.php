<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class UserBusinessInformation extends Model
{
    use Notifiable;

    protected $fillable = [
        'user_id', 'email_address', 'phone', 'address', 'website', 'facebook_link', 'twitter_link', 'instagram_link',
        'notes', 'trading_name', 'recurring_way', 'recurring_amount'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
}

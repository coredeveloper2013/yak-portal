<?php

namespace App\Models;

use App\LeadComments;
use App\LeadQuestion;
use App\Models\Staff\Campaign;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Route extends Model
{
    use SoftDeletes;
    protected $fillable = ['title', 'route', 'is_customer'];
}

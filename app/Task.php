<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class Task extends Model
{
    use Notifiable;

    protected $fillable = [
        'name', 'description', 'assigned_by', 'due_date', 'attachments',
        'file_type', 'status', 'priority', 'module_id', 'link_type'
    ];

    protected $appends = ['human_due_date', 'module_name', 'module_type'];

    protected $casts = [
        'attachments' => 'array'
    ];

    public function getHumanDueDateAttribute(){
        if(Carbon::parse($this->getOriginal('due_date'))->format('Y-m-d') == Carbon::now()->format('Y-m-d')){
            return "Today " .  date('h:i A', strtotime($this->getOriginal('due_date')));
        }
        return Carbon::parse($this->getOriginal('due_date'))->format('d M, H A');
    }

    public function getModuleNameAttribute(){
        if($this->getOriginal('link_type') == 'campaign'){
            return $this->campaign ? $this->campaign->name:'';
        }
        if($this->getOriginal('link_type') == 'landing_page'){
            return $this->landing_page ? $this->landing_page->name:'';
        }

        if($this->getOriginal('link_type') == 'other_project'){
            return $this->other_project ? $this->other_project->name:'';
        }
        return null;
    }


    public function getModuleTypeAttribute(){
        if($this->getOriginal('link_type') == 'campaign'){
            return 'Campaign';
        }
        if($this->getOriginal('link_type') == 'landing_page'){
            return 'Landing Page';
        }

        if($this->getOriginal('link_type') == 'other_project'){
            return 'Other Project';
        }
        return null;
    }


    public function scopeTodayTasks($query){
        return $query->whereDate('due_date', Carbon::today());
    }

    public function scopeUpcomingTasks($query){
        return $query->whereDate('due_date', '>', Carbon::today());
    }


    public function scopeUserTasks($query, $userId){
        return $query->where('user_id', $userId);
    }

    public function scopeStatusInProgress($query){
        return $query->where('status', 'in-progress');
    }
    public function scopeStatusPending($query){
        return $query->where('status', 'pending');
    }

    public function scopeStatusCompleted($query){
        return $query->where('status', 'completed');
    }

    public function scopeLowPriority($query){
        $query->where('priority', 'low');
    }

    public function scopeMediumPriority($query){
        $query->where('priority', 'medium');
    }

    public function scopeHighPriority($query){
        $query->where('priority', 'high');
    }

    public function campaign(){
        return $this->belongsTo(Campaign::class, 'module_id', 'id');
    }

    public function landing_page(){
        return $this->belongsTo(LandingPage::class, 'module_id', 'id');
    }

    public function other_project(){
        return $this->belongsTo(OtherProject::class, 'module_id', 'id');
    }

    public function subTasks(){
        return $this->hasMany(SubTask::class);
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function taskUsers(){
        return $this->belongsToMany(User::class, 'user_tasks', 'task_id', 'user_id');
    }
}

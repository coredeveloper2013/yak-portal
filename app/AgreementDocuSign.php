<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class AgreementDocuSign extends Model
{
    protected $table = 'zoho_douments_listing';
    protected $fillable = ['user_id', 'module_id', 'type','document_id', 'action_id', 'request_id', 'response', 'status', 'created_at', 'updated_at'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function agreements(){
        return $this->belongsTo(Agreement::class, 'module_id', 'id');
    }
}

<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Twilio\Rest\Client;

class NotificationAlert extends Model
{
    use Notifiable;

    protected $fillable = ['user_id', 'name', 'send_on','type', 'description', 'status', 'twilio_status'];

    protected $appends = ['human_date'];

    public function getHumanDateAttribute(){
        return Carbon::parse($this->getOriginal('created_at'))->toFormattedDateString();
    }

    public function getStatusAttribute($value){
        return strtoupper($value);
    }

    public function getNameAttribute($value){
        return ucfirst($value);
    }

    public static  function sendMessage($message, $recipients)
    {
        $account_sid = env("TWILIO_SID");
        $auth_token = env("TWILIO_AUTH_TOKEN");
        $twilio_number = env("TWILIO_NUMBER");
        //dd($twilio_number);
        $client = new Client($account_sid, $auth_token);

        try {
            $res = $client->messages->create($recipients, ['from' => $twilio_number, 'body' => $message] );
        } catch (\Exception $exception) {
            $res = 'Exception :'.$exception->getMessage();
        }
         return $res;
    }


    public static function addNumberToTwilio ($number, $name){

        $account_sid = env("TWILIO_SID");
        $auth_token = env("TWILIO_AUTH_TOKEN");
        $twilio_number = env("TWILIO_NUMBER");
        $client = new Client($account_sid, $auth_token);

        try {
            $validation_request = $client->validationRequests
                ->create($number,
                    ["friendlyName" => $name]
                );

            dd($validation_request->friendlyName);

        } catch (\Exception $exception) {
            echo 'Exception :'.$exception->getMessage();
        }
    }


}

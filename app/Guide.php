<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Guide extends Model
{
    protected $table = 'guides';
    protected $fillable = ['title', 'description', 'image','url','status'];

}

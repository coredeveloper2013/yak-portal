<?php

namespace App;

use DB;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class BackupLeads extends Model
{
    use Notifiable, SoftDeletes;
    protected $table = 'backup_leads';
    protected $fillable = ['leads_id' , 'user_id' , 'campaign_id','change_by','is_newBusinessHub',
    'unbounce_id' ];

}

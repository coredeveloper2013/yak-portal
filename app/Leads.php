<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Leads extends Model
{
    use SoftDeletes;

    protected $fillable = ['status', 'submitted_on', 'user_id','campaign_id', 'unbounce_id', 'deleted_at', 'sms_sent', 'email_sent'];

    public function lead_questions(){
        return $this->hasMany(LeadQuestion::class,'lead_id');
    }

    public function lead_comments(){
        return $this->hasMany(LeadComments::class,'lead_id');
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function campaign(){
        return $this->belongsTo(Campaign::class);
    }
}

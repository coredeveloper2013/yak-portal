<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeadComments extends Model
{
    //
    public function lead(){
        return $this->belongsTo(Leads::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeadQuestion extends Model
{
	 protected $fillable = ['user_id', 'lead_id', 'headings', 'answers', 'created_at', 'updated_at'];

    public function lead(){
        return $this->belongsTo(Leads::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}

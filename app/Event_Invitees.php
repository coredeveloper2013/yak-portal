<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event_Invitees extends Model
{
	protected $table = 'event_invitees';
    protected $fillable = ['name' , 'phone_number' , 'event_id'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class UserTasks extends Model
{
    use Notifiable;

    protected $fillable = ['task_id', 'user_id'];


}

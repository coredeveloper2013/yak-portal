<?php

namespace App\Mail;
use PDF;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendInvoiceEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $invoiceData;
    public function __construct($invoice)
    {
        $this->invoiceData = $invoice;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //$logoImage = URL('/images/ppc.123.png');
        $logoImage = public_path().'/images/ppc_pdf_logo.jpg';
        $pdf = PDF::loadView('staff.invoice.download', ['invoice' => $this->invoiceData, 'page_title' => 'Download Invoice', 'logo' =>$logoImage]);
        return $this->subject('Invoice From YAK PORTAL')
            ->view('staff.invoice.mail' , ['invoice' => $this->invoiceData])
            ->attachData($pdf->output(), "invoice.pdf");
    }
}

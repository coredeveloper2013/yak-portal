<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class SubTask extends Model
{
    use Notifiable;

    protected $fillable = [
        'task_id', 'name', 'status'
    ];

    protected $appends =['human_created_date'];


    public function getHumanCreatedDateAttribute(){
        return Carbon::parse($this->getOriginal('created_at'))->toFormattedDateString();
    }

    public function scopeStatusPending($query){
        return $query->where('status', 'pending');
    }

    public function scopeStatusCompleted($query){
        return $query->where('status', 'completed');
    }

    public function task(){
        return $this->belongsTo(Task::class);
    }
}

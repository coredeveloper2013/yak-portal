<?php

namespace App\Helpers;

use App\Models\RoutePermission;
use Library\Response;
use Illuminate\Support\Facades\Validator;
use Auth;
use Illuminate\Support\Str;

class Helper
{
    static function checkPermission(){
        $routeName   = \Route::currentRouteName();
        $user_id     = Auth()->user()->id;
        $permissions = RoutePermission::where('user_id', $user_id)->with('route')->get()->keyBy('route.route')->toArray();
        if(!array_key_exists($routeName, $permissions)) return abort(403);
    }

    public function checkRecordExist($query,$type = 'json'){
        $exist = false;
        if($query->exists()){
            $exist = true;
        }

        if(!$exist){
            if($type == 'web'){
                abort(404);
            }else{
                $this->response()->send([
                    'msg' => 'Record not found.',
                    'status' => 'error',
                    'mob_status' => 'success',
                ]);
            }
        }
    }

    public function toObject($array): object
    {
        $object = new \stdClass();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $value = self::toObject($value);
            }
            $object->$key = $value;
        }

        return $object;
    }

    /**
     * @param $dotedArray
     * @return array
     */
    public function expandDotArray($dotedArray): array
    {
        if (!is_array($dotedArray)) {
            return false;
        }

        $array = [];
        foreach ($dotedArray as $key => $val) {
            array_set($array, $key, $val);
        }

        return $array;
    }


    public function response()
    {
        return new Response();
    }

    // validation messages
    public function getValidationMessages()
    {
        return [
            'required' => ':Attribute field is required',
            'digits' => ':attribute must be of 11 digits',
            'date' => ':attribute should be a date',
            'name.required' => 'Please enter the name.',
            'email.required' => 'Please enter the email.',
            'email.email' => 'Please enter correct email format.',
            'email.unique' => 'User with this email already exist.',
            'password.required' => 'Please enter the password.',
            'password.confirmed' => 'Password does not match.',
            'selectdRoles.required' => 'Please select any role for this user.',
            'roleName.required' => 'Role name is required.',
            'permissionName.required' => 'Permission name is required.',
            'roleName.unique' => 'Role with this name already exist.',
            'permissions.required' => 'Please assign any permission to this role.',
            'permissionName.unique' => 'Permission with this name already exist.',
            'amount.min' => 'Please enter positive integer'
        ];
    }

    // check validation and return
    public function runValidation($rules, $messages = [])
    {
        $messages = array_merge($this->getValidationMessages(), $messages);
        $validator = Validator::make(request()->all(), $rules, $messages);

        if ($validator->fails()) {
            $response = [
                'status' => 'error',
                'msg' => $validator->errors()->first(),
            ];
            $this->response()->send($response);
        }
    }

    public function phoneNumber0to92($mobileNo)
    {
        $zero = $mobileNo[0];

        if ($zero != '0') { //if number starting without 0
            $mobileNo = str_pad($mobileNo, 11, '0', STR_PAD_LEFT); //appending 0 at the starting of mobile

            $zero = $mobileNo[0];
        }

        if ($zero == '0') {
            return substr_replace($mobileNo, '92', 0, 1);
        }

        return $mobileNo;
    }

    public function format_cnic($cnic)
    {
        $first_num = substr_replace($cnic, '-', 5, 0);
        $formatted_cnic = substr_replace($first_num, '-', 13, 0);
        return $formatted_cnic;
    }

    public function split_customer_name($name)
    {
        $parts = array();

        while (strlen(trim($name)) > 0) {
            $name = trim($name);
            $string = preg_replace('#.*\s([\w-]*)$#', '$1', $name);
            $parts[] = $string;
            $name = trim(preg_replace('#' . $string . '#', '', $name));
        }

        if (empty($parts)) {
            return false;
        }

        $parts = array_reverse($parts);
        $name = array();
        $name['first_name'] = $parts[0];
        $name['middle_name'] = (isset($parts[2])) ? $parts[1] : '';
        $name['last_name'] = (isset($parts[2])) ? $parts[2] : (isset($parts[1]) ? $parts[1] : $parts[0]);

        return $name;
    }

    function redirectUserTo(){
        if(Auth::user() && (Auth::user()->type === 'staff' || Auth::user()->type === 'admin')){
            // return redirect()->route('staff.dashboard');
            return redirect()->route('_v2.staff.home-2');
        }


        return redirect()->route('login');
    }

    public function redirectIf($request){
        if (Str::startsWith($request->path(), 'api')) {
            $message = 'You are not allowed to visit that page.';
            $this->response()->setStatus('error')->setCode(404)->setMessage($message)->send(['data' => '']);
        }
        return $this->redirectUserTo();
    }


    function isAllowedModule($module_access_key){

         if(Auth::user()->type === 'admin')
         {
            return true;
         }
         else
         {
             $rights = array();

            if(Auth::user()->rights != null)
             {
                $rights = explode(',' , \Auth::user()->rights );
                if (in_array($module_access_key, $rights)) {
                   return true;
                }

               return false;

             }

             return false;
         }


    }

}

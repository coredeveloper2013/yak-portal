<?php

namespace App\Helpers;

use Twilio\Rest\Client;

class TwilioHelper
{

    public static function addNumberToTwilio ($number, $service){

        $account_sid = getenv("TWILIO_SID");
        $auth_token = getenv("TWILIO_AUTH_TOKEN");
//        $service = getenv("TWILIO_SERVICE1");
        $client = new Client($account_sid, $auth_token);

        try {

//            $phone_number = $client->messaging->v1->services($service)
//                ->phoneNumbers
//                ->create($number);
//
//            return  $phone_number->sid;


            $validation_request = $client->validationRequests
                ->create($number, // phoneNumber
                    ["friendlyName" => "My Home Phone Number"]
                );

            dd($validation_request->friendlyName);


        } catch (\Exception $exception) {
            echo 'Exception : '.$exception->getMessage();
        }
    }


    public static  function sendMessage($message, $recipients)
    {
        $account_sid = getenv("TWILIO_SID");
        $auth_token = getenv("TWILIO_AUTH_TOKEN");
        $twilio_number = getenv("TWILIO_NUMBER");
        $client = new Client($account_sid, $auth_token);

        try {
            $client->messages->create($recipients,
                ['from' => $twilio_number, 'body' => $message] );
        } catch (\Exception $exception) {
            echo 'Exception :'.$exception->getMessage();
        }

    }

    public static function createService ($service_name){
        $account_sid = getenv("TWILIO_SID");
        $auth_token = getenv("TWILIO_AUTH_TOKEN");

        $client = new Client($account_sid, $auth_token);

        $service = $client->messaging->v1->services
            ->create($service_name);

        return $service->sid;
    }

}

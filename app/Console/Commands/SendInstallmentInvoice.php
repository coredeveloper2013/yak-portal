<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use DB;
use App\Invoice;
use App\Campaign;
use App\CronStatus;
use App\InvoiceInstallments;


class SendInstallmentInvoice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'installment:send-invoice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Installment to invoices users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "Starting...";
        echo "\n";
        $sendArr = [];
        $date = date('Y-m-d');
        $pending_invoices = InvoiceInstallments::where(['is_sended' => 'N', 'installment_date' => $date]);
       

        if ($pending_invoices->exists()) {
            $pending_invoices =  $pending_invoices->get();

            $oInvoice = new Invoice();
            $vat = $oInvoice->vat;

            foreach ($pending_invoices as $key => $invoice) {

                $amount = $invoice->amount;
                $totalAmount = $amount + (($amount/100) * $vat);

                $insertArr = [
                    'user_id' => $invoice->user_id,
                    'status' => 'pending',
                    'amount' => $amount,
                    'vat' => $vat,
                    'total_amount' => $totalAmount,
                    'landing_pages_id' => null
                ];

                if ($invoice->module_type == 'campaigns') {
                    $insertArr['type'] = 'campaign';
                    $insertArr['campaign_id'] = $invoice->module_id;
                    $updData = [
                        'status' => 'pending'
                    ];
                    DB::table('campaigns')->where('id', $invoice->module_id)->update($updData);
                } else {
                    $insertArr['type'] = 'other_project';
                    $insertArr['other_project_id'] = $invoice->module_id;
                }
                Invoice::create($insertArr);
                $invoice->update(['is_sended' => 'Y']);
            }

        } else {
            echo "No Installment Invoice Found to send......";
            echo "\n";
            exit;
        }
        echo "Finished......";
        echo "\n";
    }
}

<?php

namespace App\Console\Commands;

use App\User;
use App\AgreementDocuSign;
use Illuminate\Console\Command;
use Library\Zoho as ZohoLibrary;
use Illuminate\Support\Facades\DB;;

class VerifyDocumentSignature extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zoho:verify_documents';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verify The document status and save as pdf file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $docnuments = DB::select("SELECT * FROM zoho_douments_listing WHERE status = 'send' ORDER BY id ASC");
        if (!empty($docnuments)) {
            foreach ($docnuments as $key => $value) {
                $status = ZohoLibrary::getDocuStatus($value);
            }
        }
    }
}

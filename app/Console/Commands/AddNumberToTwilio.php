<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\NotificationAlert;
use App\Helpers\TwilioHelper;

class AddNumberToTwilio extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twilio:add-number';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add database phone number to Twilio';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */


    public function handle()
    {
        try {
            echo "Add number to twilio";
            echo "\n";
            // get all pending sms
            $alert_users  = NotificationAlert::where(['type'=>'sms', 'twilio_status' => '0'])->get();


            foreach ($alert_users as $key => $user){
//                echo  "Adding number => ". $user->send_on;
                $service_id = TwilioHelper::createService('Notification');
                echo "service_id : ". $service_id;
                $number_sid = TwilioHelper::addNumberToTwilio($user->send_on, $service_id);
                dd($number_sid);
                    $user->update(['twilio_status' => '1']);

                }
            echo  "Number added";

            echo "\n";

        } catch(\Exception $e) {
            echo "\n";
            echo 'Exception came';
            echo "\n";
            echo $e->getMessage();
        }
    }
}

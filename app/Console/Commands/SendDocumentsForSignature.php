<?php

namespace App\Console\Commands;

use App\User;
use App\AgreementDocuSign;
use Illuminate\Console\Command;
use Library\Zoho as ZohoLibrary;
use Illuminate\Support\Facades\DB;

class SendDocumentsForSignature extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zoho:send_documents';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send the documents for signutre from zoho library';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $oUser = new User();
        $docnuments = DB::select("SELECT * FROM zoho_douments_listing WHERE status = 'pending' ORDER BY id ASC");
        if (!empty($docnuments)) {
            foreach ($docnuments as $key => $value) {
                $agreements = DB::select("SELECT * FROM agreements WHERE id = ".$value->module_id);
                $campaigns = DB::select("SELECT * FROM campaigns WHERE id = ".$agreements[0]->campaign_id);
                $value->amount = $campaigns[0]->amount;
                $value->with_installment = $campaigns[0]->with_installment;
                $value->installment_count = ($campaigns[0]->installment_count != 0) ? $campaigns[0]->installment_count : 1;
                $userData = $oUser->getUserDataById($value->user_id);
                $value->name = $userData['name'];
                $value->email = $userData['email'];
                $value->phone = (!empty($userData['phone'])) ? $userData['phone'] : '' ;
                $value->address = (!empty($userData['company']) && !empty($userData['company']['address'])) ? $userData['company']['address'] : '' ;
                $status = ZohoLibrary::sendDocument($value);
            }
        }
    }
}

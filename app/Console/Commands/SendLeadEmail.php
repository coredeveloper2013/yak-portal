<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\User;
use App\LeadEmail;
use Carbon\Carbon;
use App\LeadQuestion;
use App\Mail\LeadsEmail;
use App\Jobs\SendMailJob;
use App\NotificationAlert;

use DB;
use App\Leads;
use App\Campaign;
use App\CronStatus;



class SendLeadEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lead:send-email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send pending Email to all leads';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    

    public function handle()
    {
        echo "Starting...";
        echo "\n";
        $sendArr = [];
        $pending_leads = Leads::where(['email_sent' => '0']);
        if ($pending_leads->exists()) {
            $pending_leads =  $pending_leads->get();
            foreach ($pending_leads as $key => $lead) {
                $campaignsData = Campaign::where(['id'=> $lead->campaign_id]);
                if (!$campaignsData->exists()) {
                    echo 'No Campaign Found Against Lead: '.$lead->id;
                    echo "\n";
                    $cronStatus = CronStatus::create([
                        'type' => 'EMAIL',
                        'alert_id' => 'Error 1',
                        'campaign_id' => 'No Campaign Found',
                        'lead_id' => $lead->id,
                        'module_type' => 'campaign_leads',
                        'send_on' => 'Error 1',
                        'send_by' => 'Twillio',
                        'respons' => json_encode(['No Campaign Found against this lead'])
                    ]);
                    $lead->update(['email_sent' => '1']);
                    continue;
                }
                $campaignsData = $campaignsData->first();

                $queryNoti = 'SELECT * FROM `notification_alerts` WHERE type ="email" AND user_id = '.$campaignsData->user_id;
                $notificationData = DB::select($queryNoti);

                $query = 'SELECT * FROM `lead_questions` WHERE lead_id = '.$lead->id;
                $questionsData = DB::select($query);

                $mail_array = array();
                if (!empty($questionsData)) {
                    foreach ($questionsData as $question) {
                        $temp_array = array(
                            'heading' => $question->headings,
                            'ans' => $question->answers,
                        );

                        $mail_array[] = $temp_array;
                    }
                }

                if (!empty($mail_array) && !empty($notificationData)) {
                    echo "sending...";
                    echo "\n";
                    foreach ($notificationData as $key => $notification) {
                        $resArr = dispatch(new SendMailJob($notification->send_on, new LeadsEmail($mail_array) ) );

                        $cronStatus = CronStatus::create([
                            'type' => 'EMAIL',
                            'alert_id' => 'Noti ID:'.$notification->id.' User ID:'.$notification->user_id,
                            'campaign_id' => $campaignsData->id,
                            'lead_id' => $lead->id,
                            'module_type' => 'campaign_leads',
                            'send_on' => $notification->send_on,
                            'send_by' => 'yak-portal',
                            'respons' => json_encode($resArr)
                        ]);
                        echo  'Lead Send as email on =>  '.$notification->send_on;
                        echo "\n";
                    }
                } else {
                    echo "Error......";
                    echo "\n";
                    $resArr = [
                        'question' => (!empty($questionsData)) ? json_encode($mail_array) : 'No Question found agianst Lead: '.$lead->id,
                        'notification' => (!empty($notificationData)) ? 'Found' : 'No SMS Notification Alert found agianst User: '.$campaignsData->user_id
                    ];
                    $cronStatus = CronStatus::create([
                        'type' => 'EMAIL',
                        'alert_id' => 'Error 2',
                        'campaign_id' => $campaignsData->id,
                        'lead_id' => $lead->id,
                        'module_type' => 'campaign_leads',
                        'send_on' => 'Not Sended',
                        'send_by' => '0',
                        'response' => json_encode($resArr)
                    ]);
                }
                $lead->update(['email_sent' => '1']);
            }

        } else {
            echo "No Leads Found to send......";
            echo "\n";
        }
        echo "Finished......";
        echo "\n";
    }



    /*public function handle()
    {
        //

        try {

            // get all pending sms
            $alert_users  = NotificationAlert::where('type','email')->get();
            foreach ($alert_users as $key => $user){
                $pending_leads = LeadEmail::where(['status'=> 0, 'alert_user' => $user->id])->groupBy('lead_id')
                    ->get();

                // get lead pending email
                foreach ($pending_leads as $key => $lead){
                    $pending_questions = LeadEmail::where(['lead_id'=> $lead->lead_id,'alert_user' => $user->id ])->get();

                    $mail_array = array();
                    foreach ($pending_questions as $question){
                        $current_question  = LeadQuestion::find($question->question_id);
                        $temp_array = array(
                            'heading' => $current_question->headings,
                            'ans' => $current_question->answers,
                        );

                        $mail_array[] = $temp_array;
                        // send email here and update status to 1(sent)
                        $question->update(['status' => 1]);
                    }
                    echo "sending email";
                    dispatch(new SendMailJob(
                            $user->send_on,
                            new LeadsEmail($mail_array))
                    );
                    echo  "Sent to => ". $user->send_on ;

                    echo "\n";

                }

            }


        } catch(\Exception $e) {
            echo "\n";
            echo 'Exception came';
            echo "\n";
            echo $e->getMessage();
        }
    }*/
}

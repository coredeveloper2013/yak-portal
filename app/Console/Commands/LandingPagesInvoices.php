<?php

namespace App\Console\Commands;

use App\Invoice;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class LandingPagesInvoices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'landingPage:invoices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generating Invoices for Landing Pages after a month';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $lastMonthDate = Carbon::now()->subMonths(1);

        $landingPages = DB::select("SELECT
          *,
          DATE(created_at) AS last_date
        FROM
          invoices
        WHERE id IN (SELECT MAX(i.id) AS id FROM `invoices` AS i
        WHERE i.`type` = 'landing_page'
         GROUP BY i.`landing_pages_id`)
        AND DATE(created_at) = '".Carbon::parse($lastMonthDate)->format('Y-m-d')."'");
        foreach ($landingPages as $landingPage){
            if($landingPage->status != 'completed'){
                Invoice::create([
                    'user_id' => $landingPage->user_id,
                    'type' => $landingPage->type,
                    'status' => 'pending',
                    'landing_pages_id' => $landingPage->landing_pages_id,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]);
            }
        }

        echo 'invoices generated successfully for all the past month landing pages';
    }
}

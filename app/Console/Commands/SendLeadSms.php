<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use DB;
use App\Leads;
use App\LeadSms;
use App\Campaign;
use App\CronStatus;
use App\LeadQuestion;
use App\NotificationAlert;


class SendLeadSms extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lead:send-sms';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send pending sms to all leads';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "Starting...";
        echo "\n";
        $sendArr = [];
        $pending_leads = Leads::where(['sms_sent' => '0']);

        if ($pending_leads->exists()) {
            $pending_leads =  $pending_leads->get();
            foreach ($pending_leads as $key => $lead) {
                $campaignsData = Campaign::where(['id'=> $lead->campaign_id]);
                if (!$campaignsData->exists()) {
                    echo 'No Campaign Found Against Lead: '.$lead->id;
                    echo "\n";
                    $cronStatus = CronStatus::create([
                        'type' => 'SMS',
                        'alert_id' => 'Error 1',
                        'campaign_id' => 'No Campaign Found',
                        'lead_id' => $lead->id,
                        'module_type' => 'campaign_leads',
                        'send_on' => 'Error 1',
                        'send_by' => 'Twillio',
                        'respons' => json_encode(['No Campaign Found against this lead'])
                    ]);
                    $lead->update(['sms_sent' => '1']);
                    continue;
                }
                $campaignsData = $campaignsData->first();

                $queryNoti = 'SELECT * FROM `notification_alerts` WHERE type ="sms" AND user_id = '.$campaignsData->user_id;
                $notificationData = DB::select($queryNoti);

                $query = 'SELECT * FROM `lead_questions` WHERE lead_id = '.$lead->id;
                $questionsData = DB::select($query);

                $sms_body = "";
                if (!empty($questionsData)) {
                    $sms_body = 'New Lead From YAK PORTAL ';
                    $sms_body .= "\n";
                    $sms_body .= "\n";
                    foreach ($questionsData as $question) {
                        $sms_body .= $question->headings;
                        $sms_body .= "\n";
                        $sms_body .= $question->answers;
                        $sms_body .= "\n";
                        $sms_body .= "\n";
                    }
                }

                if (!empty($sms_body) && !empty($notificationData)) {
                    echo "sending...";
                    echo "\n";
                    foreach ($notificationData as $key => $notification) {
                        $resArr = NotificationAlert::sendMessage($sms_body, $notification->send_on);

                        $cronStatus = CronStatus::create([
                            'type' => 'SMS',
                            'alert_id' => 'Noti ID:'.$notification->id.' User ID:'.$notification->user_id,
                            'campaign_id' => $campaignsData->id,
                            'lead_id' => $lead->id,
                            'module_type' => 'campaign_leads',
                            'send_on' => $notification->send_on,
                            'send_by' => 'Twillio',
                            'respons' => json_encode($resArr)
                        ]);
                        echo  'Lead Send as sms on =>  '.$notification->send_on;
                        echo "\n";
                    }
                } else {
                    echo "Error......";
                    echo "\n";
                    $resArr = [
                        'question' => (!empty($questionsData)) ? $sms_body : 'No Question found agianst Lead: '.$lead->id,
                        'notification' => (!empty($notificationData)) ? 'Found' : 'No SMS Notification Alert found agianst User: '.$campaignsData->user_id
                    ];
                    $cronStatus = CronStatus::create([
                        'type' => 'SMS',
                        'alert_id' => 'Error 2',
                        'campaign_id' => $campaignsData->id,
                        'lead_id' => $lead->id,
                        'module_type' => 'campaign_leads',
                        'send_on' => 'Not Sended',
                        'send_by' => '0',
                        'response' => json_encode($resArr)
                    ]);
                }
                $lead->update(['sms_sent' => '1']);
            }

        } else {
            echo "No Leads Found to send......";
            echo "\n";
        }
        echo "Finished......";
        echo "\n";
    }

    /*public function handleOld()
    {


            // get all pending sms
            echo "start";
            $notificationData  = NotificationAlert::where('type','sms')->get();
            foreach ($notificationData as $key => $notification){
                $pending_leads = LeadSms::where(['status'=> 0, 'alert_user' => $notification->id])->groupBy('lead_id')->get();

                // get lead pending sms
                foreach ($pending_leads as $key => $lead) {
                    $pending_questions = LeadSms::where(['lead_id'=> $lead->lead_id, 'alert_user' => $notification->id ])->get();

                    $sms_body = 'New Lead From YAK PORTAL ';
                    $sms_body .= "\n";
                    $sms_body .= "\n";
                    foreach ($pending_questions as $question) {
                        $current_question  = LeadQuestion::find($question->question_id);
                        $sms_body .= $current_question->headings;
                        $sms_body .= "\n";
                        $sms_body .= $current_question->answers;
                        $sms_body .= "\n";
                        $sms_body .= "\n";
                        // send sms here and update status to 1(sent)
                        $question->update(['status' => 1]);
                    }

                    echo "sending...";
                    try {
                        NotificationAlert::sendMessage($sms_body, $notification->send_on);

                    } catch (Exception $e) {
                        echo "Exception:". $e->getMessage();
                    }

                    echo  $notification->send_on .' =>  '. $sms_body;

                    echo "\n";

                }

            }

    }*/
}

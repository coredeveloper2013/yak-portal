<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Invoice;
use App\Campaign;
use Carbon\Carbon;
use App\Notification;
use App\OtherProject;
use DB;
class SendMonthlyInvoice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Invoice:sendMonthly';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send emails to monthly comapigns and projects';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              $now = Carbon::now();
              $dates = [];
              for ($i = 1; $i <= 12; $i++) {
                   $temp_date = Carbon::now()->subMonth($i);
                   $temp_date = $temp_date->format('Y-m-d');
                   $dates[]= $temp_date;
              }

              // $now = Carbon::now()->subDays(1);
              // $now = Carbon::now();
              // $startDate = $now->format('Y-m-d 00:00:00');
              // $endDate = $now->format('Y-m-d 23:59:59');
              // create and send invoices for projects

               $projects = OtherProject::with('user')->where([
                        ['status', '=', 'pending'],
                        ['cost_type', '=', 'monthly'],
                        ['is_deleted' , '=', 'N']
                    ])
                    ->whereNotNull('no_of_months')
                    ->whereIn(DB::raw("DATE(created_at)"),$dates);
              if ($projects->exists()) {
                  $projects = $projects->get();
                  foreach ($projects as $project) {
                     $date = Carbon::parse($project->created_at);
                     $diff_months = $date->diffInMonths($now);
                     if ($project->no_of_months >= $diff_months)
                         {
                             $oInvoice = new Invoice();
                             $vat = $oInvoice->vat;
                             $amount = $project->price;
                             $totalAmount = $amount + (($amount/100) * $vat);

                             $invoice = Invoice::create([
                                 'user_id' => $project->user_id,
                                 'type' => 'other_project',
                                 'status' => 'pending',
                                 'amount' => $amount,
                                 'vat' => $vat,
                                 'total_amount' => $totalAmount,
                                 'other_project_id' => $project->id,
                             ]);

                             echo "invoice created with ID # ".$invoice->id." of project #".$project->id.'<br/>';
                         }
                  }
              } else {
                  echo "No Record Found \n";
              }
       //end create and send invoices for projects

       } catch(\Exception $e) {
            echo "\n";
            echo 'Exception came';
            echo "\n";
            echo $e->getMessage();
        }
    }
}

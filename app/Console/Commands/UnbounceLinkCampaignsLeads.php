<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use DB;
use Auth;
use App\Invoice;
use Carbon\Carbon;
use App\Agreement;
use App\Notification;
use App\Helpers\Helper;
use App\AgreementDocuSign;
use App\Unbounce;
use App\Leads;
use App\LeadQuestion;
use App\LeadDeal;
use App\LeadDealQuestion;
use Illuminate\Support\Facades\Validator;
use App\BackupLeads;


class UnbounceLinkCampaignsLeads extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'unbounce:link';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'unbounce link with campaign leads  ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

       $appKey = 'd12f072ccc809b29767b92fec66c9b27';
       $Unbounce=Unbounce::all();
       // $Unbounce=Unbounce::where('cron_status', 'pending')->get();
       //$Unbounce=Unbounce::whereDay('created_at', now()->day)->get();

       foreach ($Unbounce as $ukey => $unb) {
            $status = $unb->update(['cron_status' => 'executing']);

            $userId=$unb->user_id;
            $campaignId=$unb->campaign_id;
            $unbounceId=$unb->unbounce_id;
            $url = 'https://api.unbounce.com/pages/'.$unbounceId.'/leads?limit=1000';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, "$appKey");
            $result = curl_exec($ch);
            curl_close($ch);
            $result = json_decode($result,true);
            //call API
            if(!empty($result['leads'])) {
                foreach ($result['leads'] as $key => $value) {
                    $unbLeadsId= $value['id'];

                    if(!$unb['is_new_bussiness_lead']){
                        $exsit =Leads::where(['unbounce_id'=>$value['id'],'campaign_id'=>$campaignId])->withTrashed();
                        if ($exsit->exists()) {
                            $unb->update(['cron_status' => 'already_exist']);
                            echo " Continue 1 \n"; 
                            continue;
                        }
                        $exsit = $exsit->get();
                        $leads = new Leads();
                        $leads->campaign_id = $campaignId;
                        $leads->user_id = $userId;
                        $leads->unbounce_id = $unbLeadsId;
                        $leads->submitted_on = Carbon::now();
                        $leads->sms_sent = '0';
                        $leads->email_sent = '0';
                        $isSaved = $leads->save();
                        $insertedId = $leads->id;
                        if($insertedId){
                            foreach ($value['form_data'] as $key2 => $value2) {
                                LeadQuestion::create([
                                    'user_id' =>   $userId,
                                    'lead_id' => $insertedId,
                                    'headings' => $key2,
                                    'answers' => $value2[0]
                                ]);
                            }
                        }
                        echo $insertedId;
                        echo " Leads \n";
                    } else {
                        $is_new_exist =LeadDeal::where(['unbounce_id' => $value['id'], 'user_id' => $userId])->withTrashed();
                         $is_BackupLeads =BackupLeads::where(['unbounce_id' => $value['id']
                                                            , 'user_id' => $userId]);
                        if($is_new_exist->exists() || $is_BackupLeads->exists()){
                            $unb->update(['cron_status' => 'already_exist']);
                            echo " Continue 2 \n"; 
                            continue;
                        }
                        $is_new_exist = $is_new_exist->get();
                        $leads = new LeadDeal();
                        $leads->user_id = $userId;
                        $leads->unbounce_id = $unbLeadsId;
                        $isSaved = $leads->save();
                        $insertedId = $leads->id;
                        if($insertedId){
                            foreach ($value['form_data'] as $key2 => $value2) {
                                LeadDealQuestion::create([
                                    'lead_deal_id' => $insertedId,
                                    'question' => $key2,
                                    'answer' => $value2[0]
                                ]);
                            }
                        }

                        echo $insertedId;
                        echo " Lead Deals \n";
                    }
                    $unb->update(['cron_status' => 'executed']);
                }
            } else {
                dump($result);
                echo $unb->id." \n No Record Found \n";
            }
        }
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
class ClearCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:all-cache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear All cache in system';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "start clearing cache";
        Artisan::call('cache:clear');
        Artisan::call('config:clear');
        echo "end clearing cache";
    }
}

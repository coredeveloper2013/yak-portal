<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */

    protected $commands = [
        //
        '\App\Console\Commands\SendLeadSms',
        '\App\Console\Commands\SendLeadEmail',
        '\App\Console\Commands\ClearCache',
        '\App\Console\Commands\SendMonthlyInvoice',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $filePath = storage_path() . '/logs/crons.log';

        $schedule->command('installment:send-invoice')
            ->everyMinute()
            ->sendOutputTo($filePath);

         $schedule->command('lead:send-sms')
            ->everyMinute()
            ->sendOutputTo($filePath);

        $schedule->command('lead:send-email')
            ->everyMinute()
            ->sendOutputTo($filePath);

        $schedule->command('zoho:verify_documents')
            ->everyMinute()
            ->sendOutputTo($filePath);

        $schedule->command('zoho:send_documents')
            ->everyMinute()
            ->sendOutputTo($filePath);

        $schedule->command('unbounce:link')
            ->everyMinute()
            ->sendOutputTo($filePath);

        $schedule->command('clear:all-cache')
            ->everyFiveMinutes();

        $schedule->command('Invoice:sendMonthly')
            ->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}

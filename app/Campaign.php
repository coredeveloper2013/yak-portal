<?php

namespace App;

use DB;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Campaign extends Model
{
    use Notifiable, SoftDeletes;

    protected $fillable = ['user_id', 'name', 'recurring_option', 'recurring_way', 'start_date', 'end_date', 'landing_pages_id', 'amount', 'type', 'assigned_id', 'account_manager', 'campaign_specialist', 'lp_specialist', 'status', 'created_by', 'updated_by', 'compaign_amount', 'max_add_amount'];

    protected $appends = ['landing_page_name', 'human_date', 'created_human', 'given_date', 'type_text', 'business_details_obj', 'company_details_obj', 'user_details_obj', 'sales_person_obj', 'campaign_specialist_obj', 'lp_specialist_obj', 'account_manager_obj', 'customer_status'];

    public function getLandingPageNameAttribute()
    {
        if ($this->landing_pages()->exists()) {
            return $this->landing_pages()->first()->name;
        }
        return "N/A";
    }

    public function getCustomerStatusAttribute()
    {
        $value = $this->getOriginal('status');
        $value = str_replace(" ", "_", $value);
        $value = strtolower($value);
        if (in_array($value, ['live', 'billing_detail'])) {
            return "Live";
        } else if (in_array($value, ['pending', 'awaiting'])) {
            return "Pending";
        } else {
            return "Completed";
        }
    }

    public function getbusinessDetailsObjAttribute()
    {
        if ($this->businessDetails()->exists()) {
            return $this->businessDetails()->first();
        }
        return (object)[];
    }
    public function getCompanyDetailsObjAttribute()
    {
        if ($this->companyDetails()->exists()) {
            return [
                'id' => $this->companyDetails()->first()->id,
                'name' => $this->companyDetails()->first()->name,
                'address' => !empty($this->companyDetails()->first()->address) ? $this->companyDetails()->first()->address : '',
            ];
        }
        return (object)[];
    }

    public function getUserDetailsObjAttribute()
    {
        if ($this->userDetails()->exists()) {
            return [
                'id' => $this->userDetails()->first()->id,
                'name' => $this->userDetails()->first()->name,
                'job_position' => $this->userDetails()->first()->job_position,
                'email' => $this->userDetails()->first()->email,
                'status' => $this->userDetails()->first()->status,
                'phone' => $this->userDetails()->first()->phone
            ];
        }
        return (object)[];
    }


    public function getSalesPersonObjAttribute()
    {
        if ($this->salesPerson()->exists()) {
            return [
                'id' => $this->salesPerson()->first()->id,
                'name' => $this->salesPerson()->first()->name,
                'type' => $this->salesPerson()->first()->type
            ];
        }
        return (object)[];
    }

    public function getCampaignSpecialistObjAttribute()
    {
        if ($this->campaignSpecialist()->exists()) {
            return ['id' => $this->campaignSpecialist()->first()->id, 'name' => $this->campaignSpecialist()->first()->name, 'type' => $this->campaignSpecialist()->first()->type];
        }
        return (object)[];
    }

    public function getLpSpecialistObjAttribute()
    {
        if ($this->lpSpecialist()->exists()) {
            return ['id' => $this->lpSpecialist()->first()->id, 'name' => $this->lpSpecialist()->first()->name, 'type' => $this->lpSpecialist()->first()->type];
        }
        return (object)[];
    }

    public function getAccountManagerObjAttribute()
    {
        if ($this->accounManager()->exists()) {
            return ['id' => $this->accounManager()->first()->id, 'name' => $this->accounManager()->first()->name, 'type' => $this->accounManager()->first()->type];
        }
        return (object)[];
    }

    public function getTypeTextAttribute()
    {
        $type = $this->getOriginal('type');
        if ($type == 'one_off') {
            return 'One Off';
        } else if ($type == 'monthly') {
            return 'Monthly';
        }
        return $type;
    }

    public function getMaxAddAmountAttribute($value)
    {
        $res = DB::select("SELECT SUM(compaign_amount) as max_add_amount FROM campaigns WHERE user_id=" . $this->getOriginal('user_id'));
        return $res[0]->max_add_amount;
    }

    /*public function getTypeAttribute($value){
        if(strpos($value, "_") !== false){
            return ucfirst(str_replace("_", " ", $value));
        }
        return ucfirst($value);
    }*/

    public function getStatusAttribute($value)
    {
        if (strpos($value, "_") !== false) {
            return ucfirst(str_replace("_", " ", $value));
        }
        return ucfirst($value);
    }

    public function getGivenDateAttribute($value)
    {
        if (!empty($this->getOriginal('created_at'))) {
            return Carbon::parse($this->getOriginal('created_at'))->format('Y-m-d');
        }
        return null;
    }

    public function getRepeatAttribute($value)
    {
        return ucfirst($value);
    }

    public function getHumanDateAttribute()
    {
        if ($this->getOriginal('start_date') !== null and $this->getOriginal('end_date') !== null) {
            return Carbon::parse($this->getOriginal('start_date'))->format('d/m/Y') . '-' . Carbon::parse($this->getOriginal('end_date'))->format('d/m/Y');
        }
        return null;
    }

    public function getCreatedHumanAttribute()
    {
        if (!empty($this->getOriginal('created_at'))) {
            return Carbon::parse($this->getOriginal('created_at'))->format('d, M Y');
        }
        return null;
    }

    //make relation between campaign and landing pages
    public function landing_pages()
    {
        return $this->belongsTo(LandingPage::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function salesPerson()
    {
        return $this->belongsTo(User::class, 'assigned_id');
    }
    public function userDetails()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function companyDetails()
    {

        return $this->belongsTo(Company::class, 'user_id', 'user_id');
    }
    public function businessDetails()
    {
        return $this->belongsTo(UserBusinessInformation::class, 'user_id', 'user_id');
    }

    public function campaignSpecialist()
    {
        return $this->belongsTo(User::class, 'campaign_specialist');
    }

    public function lpSpecialist()
    {
        return $this->belongsTo(User::class, 'lp_specialist');
    }

    public function accounManager()
    {
        return $this->belongsTo(User::class, 'account_manager');
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class, 'campaign_id', 'id');
    }

    public function invoice()
    {
        return $this->hasOne(Invoice::class, 'campaign_id', 'id');
    }

    public function lead()
    {
        return $this->hasOne(Leads::class);
    }

    public function notification()
    {
        return $this->hasMany(Notification::class);
    }

    public function agreements()
    {
        return $this->hasMany(Agreement::class);
    }
    public function agreement()
    {
        return $this->hasOne(Agreement::class);
    }
    public function campaignComments()
    {
        return $this->hasMany(CampaignComments::class);
    }

    public function installments()
    {
        return $this->hasMany(InvoiceInstallments::class, 'module_id', 'id')->where('module_type', 'campaigns');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignComments extends Model
{
    //
    protected $fillable = ['campaign_id' , 'description' , 'user_id'];
    protected $appends = ['description_br'];
    public function campaign(){
        return $this->belongsTo(Campaign::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function getDescriptionBrAttribute(){
        if(!empty($this->description)){
            return nl2br($this->description);
        }
        return $this->description;
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeadSms extends Model
{

    protected $fillable = ['alert_user', 'lead_id', 'question_id', 'status', 'created_at', 'updated_at'];


    public function lead(){
        return $this->belongsTo(Leads::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}

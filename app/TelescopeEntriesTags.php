<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TelescopeEntriesTags extends Model
{
    protected $table = "telescope_entries_tags";

    
    public function logDetails()
    {
        return $this->hasOne(TelescopeEntries::class,'uuid','entry_uuid')->orderBy('created_at' , 'DESC');
    }
}

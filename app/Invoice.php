<?php

namespace App;

use DB;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Invoice extends Model
{
    use Notifiable;

    protected $fillable = ['user_id', 'campaign_id', 'landing_pages_id', 'other_project_id', 'type', 'status', 'amount', 'vat', 'total_amount', 'paid_date', 'created_at'];
    protected $appends = ['human_date', 'title', 'human_paid_date', 'response_arr', 'paid_as', 'deleted_parent'];
    protected $hidden = ['response'];

    public $vat = 20;
    protected $amount = 0;

    public $typeArr = [
        'campaign' => 'Campaign',
        'landing_page' => 'Landing Page',
        'other_project' => 'Project'
    ];

    public function getResponseArrAttribute(){
        /*if(!empty($this->getOriginal('response'))){
            return json_decode($this->getOriginal('response'), true);
        }*/
        return [];
    }

    public function getPaidAsAttribute(){
        if(!empty($this->getOriginal('response'))){
            $res = json_decode($this->getOriginal('response'), true);
            if (!empty($res['payment_type'])) {
                return $res['payment_type'];
            }
        }
        return 'card_charge';
    }

    public function getDeletedParentAttribute(){
        if($this->campaign()->withTrashed()->exists()){
            $campaignData = $this->campaign()->withTrashed()->first();
            return (!empty($campaignData->deleted_at)) ? 'Y' : 'N' ;
        } else if ($this->other_project()->exists()) {
            return $this->other_project->is_deleted;
        } else if ($this->landing_page()->exists()) {
            return 'N';
        }
        return 'N';
    }

    public function getHumanDateAttribute(){
        if(!empty($this->getOriginal('created_at'))){
            return Carbon::parse($this->getOriginal('created_at'))->toFormattedDateString();
        }
        return '';
        /*if($this->campaign()->withTrashed()->exists()){
            $campaignData = $this->campaign()->withTrashed()->first();
            return Carbon::parse($campaignData->created_at)->toFormattedDateString();
        } else if ($this->other_project()->exists()) {
            return Carbon::parse($this->other_project->created_at)->toFormattedDateString();
        } else if ($this->landing_page()->exists()) {
            return Carbon::parse($this->landing_page->created_at)->toFormattedDateString();
        } else {
            return '';
        }*/
    }

    public function getHumanPaidDateAttribute(){
        if($this->getOriginal('paid_date') != null){
            return Carbon::parse($this->getOriginal('paid_date'))->toFormattedDateString();
        }
        return null;

    }

    public function getAmountAttribute($value){
        if (empty($value)) {
            $amount = $this->amount;
            if($this->campaign()->withTrashed()->exists()){
                $campaignData = $this->campaign()->withTrashed()->first();
                $amount = $campaignData->amount;
            } else if ($this->other_project()->exists()) {
                $amount = $this->other_project->price;
            } else if ($this->landing_page()->exists()) {
                $amount = $this->landing_page->cost;
            }
            return $amount;
        }
        return $value;
    }

    public function getVatAttribute($value){
        if (empty($value)) {
            return $this->vat;
        }
        return $value;
    }

    public function getTotalAmountAttribute($value){
        $totalAmount = $value;
        if(empty($totalAmount)){
            $pkID = $this->getOriginal('id');
            $vat = (!empty($this->getOriginal('vat'))) ? $this->getOriginal('vat') : $this->vat;
            $amount = (!empty($this->getOriginal('amount'))) ? $this->getOriginal('amount') : $this->amount;

            if ($amount <= 0) {
                if($this->campaign()->withTrashed()->exists()){
                    $campaignData = $this->campaign()->withTrashed()->first();
                    $amount = $campaignData->amount;
                } else if ($this->other_project()->exists()) {
                    $amount = $this->other_project->price;
                } else if ($this->landing_page()->exists()) {
                    $amount = $this->landing_page->cost;
                }
            }

            $totalAmount = $amount + (($amount/100) * $vat);
            DB::table('invoices') ->where('id', $pkID) ->update(['amount' => $amount, 'vat' => $vat, 'total_amount' => $totalAmount]);
            return $totalAmount;
        }
        return $totalAmount;
    }

    public function getTitleAttribute(){
        if($this->campaign()->withTrashed()->exists()){
            $campaignData = $this->campaign()->withTrashed()->first();
            return $campaignData->name;
        } else if ($this->other_project()->exists()) {
            return $this->other_project->project_name;
        } else if ($this->landing_page()->exists()) {
            return $this->landing_page->name;
        }
        return '';
    }

    public function getStatusAttribute($value){
        return strtoupper($value);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function campaign(){
        return $this->belongsTo(Campaign::class, 'campaign_id');
    }

    public function landing_page(){
        return $this->belongsTo(LandingPage::class, 'landing_pages_id');
    }

    public function other_project(){
        return $this->belongsTo(OtherProject::class, 'other_project_id');
    }
}

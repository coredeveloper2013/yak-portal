<?php

namespace App;

use Auth;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class TicketChat extends Model {
	use Notifiable;
    protected $table = "tickets_chat";
    protected $fillable = ['user_id', 'comment_text', 'created_at'];

    public function getCommentsByTicketId($id='')
    {
        $oUser = new User();
        $comments =   DB::select("SELECT 
                                    st.`id`,
                                    st.`ticket_id`,
                                    st.`user_id`,
                                    st.`chat_text`,
                                    st.`created_at`
                                FROM
                                  `tickets_chat` st
                                WHERE st.`ticket_id` = ".$id);

        if (!empty($comments)) {
            foreach ($comments as $key => $value) {
                $userData = $oUser->getUserDataById($value->user_id);
                $comments[$key]->user_name = $userData['name'];
                $comments[$key]->user_image = $userData['image'];
                $comments[$key]->human_date = Carbon::parse($value->created_at)->format('M d, Y H:i');
            }
        }

        return $comments;
    }

    public function getCommentsByCommentId($id='')
    {
        $oUser = new User();
        $comments =   DB::select("SELECT 
                                    st.`id`,
                                    st.`ticket_id`,
                                    st.`user_id`,
                                    st.`chat_text`,
                                    st.`created_at`
                                FROM
                                  `tickets_chat` st
                                WHERE st.`id` = ".$id);
        if (!empty($comments)) {
            foreach ($comments as $key => $value) {
                $userData = $oUser->getUserDataById($value->user_id);
                $comments[$key]->user_name = $userData['name'];
                $comments[$key]->user_image = $userData['image'];
                $comments[$key]->human_date = Carbon::parse($value->created_at)->format('M d, Y H:i');
            }
            $comments = $comments[0];
        }

        return $comments;
    }
}

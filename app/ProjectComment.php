<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ProjectComment extends Model
{
    use Notifiable;

    protected $fillable = ['module_id', 'user_id', 'module_type', 'comment_text', 'created_at'];

    public function other_project(){
        return $this->belongsTo(OtherProject::class, 'module_id');
    }
}

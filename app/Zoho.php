<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zoho extends Model
{
	protected $table = 'auth_zoho_token';
	protected $fillable = ['access_token','refresh_token', 'url','response', 'created_at','updated_at'];
}

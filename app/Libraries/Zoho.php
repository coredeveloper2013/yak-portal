<?php

namespace Library;

use App\Zoho as ZohoModel;
use InvalidArgumentException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Pagination\LengthAwarePaginator;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\TemplateProcessor;

class Zoho
{

    protected $accountServer = '';
    protected $scope = 'ZohoSign.documents.CREATE,ZohoSign.documents.READ,ZohoSign.documents.UPDATE,ZohoSign.documents.DELETE';
    protected $client_id = '1000.B0YL3QF4TNRSPRGI7DA4BN8RMMZC6R';
    protected $client_secret = 'e3dd02ec47b2bd6a7291c6fe0647a84756cc68d2fd';
    protected $state = 'testing';
    protected $response_type = 'code';
    protected $redirect_uri = '';
    //protected $redirect_uri = route('zoho-code-genrate');
    protected $access_type = 'offline';
    protected $prompt = 'consent';

    public function __construct() {
        $this->redirect_uri = url('/zoho-code');

        // Genrating Refresh Token
        /*$tokenData = DB::select("SELECT * FROM auth_zoho_token ORDER BY id DESC LIMIT 1");
        if (!empty($tokenData)) {
            $request = $tokenData[0];
            $expiryTime = strtotime("+50 minutes", strtotime($request->created_at));
            if ($expiryTime <= time()) {
                $prameters = [
                    'refresh_token' => $request->refresh_token,
                    'client_id' => $this->client_id,
                    'client_secret' => $this->client_secret,
                    'redirect_uri' => $this->redirect_uri,
                    'grant_type' => 'refresh_token',
                    'prompt' => $this->prompt,
                ];
                $queryString = '';
                foreach ($prameters as $key => $value) {
                    $queryString .= $key."=".$value."&";
                }
                $queryString = "?".rtrim($queryString, "&");

                $this->accountServer = $request->url;
                $requestUrl = $this->accountServer.'/oauth/_v2/token';
                //dd($requestUrl);

                $curl = curl_init($requestUrl.$queryString);
                curl_setopt($curl, CURLOPT_TIMEOUT, 30);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                $response = curl_exec($curl);
                curl_close ($curl);
                $res = json_decode($response,true);
                if (!empty($res['access_token'])) {
                    ZohoModel::create([
                        'access_token' => $res['access_token'],
                        'refresh_token' => $request->refresh_token,
                        'url' => $this->accountServer,
                        'response' => json_encode($res)
                    ]);
                }
            }
        }*/

   }

    public static function genrateCode() {
        $_this = new self;
        $url = 'https://accounts.zoho.com/oauth/_v2/auth?scope='.$_this->scope.'&client_id='.$_this->client_id.'&state='.$_this->state.'&response_type='.$_this->response_type.'&redirect_uri='.$_this->redirect_uri.'&access_type='.$_this->access_type.'&prompt='.$_this->prompt;
        //dd($url);
        return $url;
    }

    public static function genrateToken($request) {
        $_this = new self;
        $accountServer = 'accounts-server';

        if (!empty($request->code) && !empty($request->$accountServer)) {
            $prameters = [
                'code' => $request->code,
                'client_id' => $_this->client_id,
                'client_secret' => $_this->client_secret,
                'redirect_uri' => $_this->redirect_uri,
                'grant_type' => 'authorization_code',
                'prompt' => $_this->prompt,
            ];
            //dd($prameters);
            $queryString = '';
            foreach ($prameters as $key => $value) {
                $queryString .= $key."=".$value."&";
            }
            $queryString = "?".rtrim($queryString, "&");

            $_this->accountServer = $request->$accountServer;
            $requestUrl = $_this->accountServer.'/oauth/_v2/token';

            $curl = curl_init($requestUrl.$queryString);
            curl_setopt($curl, CURLOPT_TIMEOUT, 30);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($curl);
            curl_close ($curl);
            $res = json_decode($response,true);
            if (!empty($res['refresh_token'])) {
                ZohoModel::create([
                    'access_token' => $res['access_token'],
                    'refresh_token' => $res['refresh_token'],
                    'url' => $_this->accountServer,
                    'response' => json_encode($res)
                ]);
                dd('Inserted Successfully');
            }


            /*$data = Http::post($requestUrl, $prameters);
            $res = $data;*/
        } else {
            $res = $request->all();
        }

        dd($res);
    }

    public static function getLastData() {
        $tokenData = DB::select("SELECT * FROM auth_zoho_token ORDER BY id DESC LIMIT 1");
        if (empty($tokenData)) {
            dd("Please Hit the Genrate Code Api First");
        }
        $data = $tokenData[0];
        return $data;
    }

    public static function genrateRefreshToken() {
        $_this = new self;
        $request = self::getLastData();

        $prameters = [
            'refresh_token' => $request->refresh_token,
            'client_id' => $_this->client_id,
            'client_secret' => $_this->client_secret,
            'redirect_uri' => $_this->redirect_uri,
            'grant_type' => 'refresh_token',
            'prompt' => $_this->prompt,
        ];
        //dd($prameters);
        $queryString = '';
        foreach ($prameters as $key => $value) {
            $queryString .= $key."=".$value."&";
        }
        $queryString = "?".rtrim($queryString, "&");

        $_this->accountServer = $request->url;
        $requestUrl = $_this->accountServer.'/oauth/_v2/token';

        $curl = curl_init($requestUrl.$queryString);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close ($curl);
        $res = json_decode($response,true);
        if (!empty($res['access_token'])) {
            ZohoModel::create([
                'access_token' => $res['access_token'],
                'refresh_token' => $request->refresh_token,
                'url' => $_this->accountServer,
                'response' => json_encode($res)
            ]);
            echo "Inserted Successfully\n";
        }
    }

    public static function sendDocument($params=[]) {
        $_this = new self;
        $authData = self::getLastData();
        $fileRealPath = self::read_docx($params);

        if (empty($fileRealPath)) {
            echo "Issue while generating Amount File \n";
            return '';
        }

        $expiryTime = strtotime("+50 minutes", strtotime($authData->created_at));
        if ($expiryTime <= time()) {
            self::genrateRefreshToken();
            $authData = self::getLastData();
        }

        $curl = curl_init("https://sign.zoho.eu/api/v1/fieldtypes");
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization:Zoho-oauthtoken '.$authData->access_token));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close ($curl);
        $fieldTypes = json_decode($response, true);



        $request = [
            'requests' => [
                'request_name' => 'YAK Agreement',
                'actions' => [
                    [
                        'recipient_name' => $params->name,//'Muhammad Salis',
                        'recipient_email' => $params->email,//,'msalis.tariq@gmail.com',
                        'action_type' => 'SIGN',
                        'private_notes' => 'Please get back to us for further queries',
                        'signing_order' => 0,
                        'verify_recipient' => true,
                        'verification_type' => 'EMAIL',
                        'recipient_phonenumber' => '',
                        'recipient_countrycode' => '',
                        'verification_code' => '',
                    ],
                ],
                'expiration_days' => 14,
                'is_sequential' => true,
                'email_reminders' => true,
                'reminder_period' =>  8,
            ],
        ];

        $requestData = json_encode($request);
        $POST_DATA = [
            'file' => new \CURLFile($fileRealPath),
            'data' => $requestData
        ];
        $curl = curl_init('https://sign.zoho.eu/api/v1/requests');
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Authorization:Zoho-oauthtoken '.$authData->access_token,
            'Content-Type:multipart/form-data'
        ]);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $POST_DATA);
        $response = curl_exec($curl);
        curl_close ($curl);
        $documentCreate = json_decode($response,true);

        $request = [
            'requests' => [
                'actions' => $documentCreate['requests']['actions'],
                "deleted_actions" => [],
                "request_name" => $documentCreate['requests']['request_name'],
                'document_ids' => [
                    [
                        'document_id' => $documentCreate['requests']['document_ids'][0]['document_id'],
                        'document_order' => $documentCreate['requests']['document_ids'][0]['document_order'],
                    ],
                ],
                'expiration_days' => 15,
                'is_sequential' => true,
                'email_reminders' => true,
                'reminder_period' =>  7,
            ],
        ];

        foreach($fieldTypes['field_types'] as $key => $fieldType){
            if(in_array($key,[2])){
                $request['requests']['actions'][0]['fields'][] = [
                    "field_type_id" => $fieldType['field_type_id'],
                    "field_type_name" => $fieldType['field_type_name'],
                    "field_category" => $fieldType['field_category'],
                    "field_label" => $fieldType['field_type_name'],
                    "is_mandatory" => ($fieldType['is_mandatory']) ? true : false,
                    "page_no" => 3,
                    "field_name" => $fieldType['field_type_name'],
                    "document_id" => $documentCreate['requests']['document_ids'][0]['document_id'],
                    "action_id" => $documentCreate['requests']['actions'][0]['action_id'],
                    "description_tooltip" => "",
                    "x_coord" => 350,
                    "y_coord" => 423,
                    "abs_width" => 100,
                    "abs_height" => 40
                ];
            }
        }
        unset($request['requests']['actions'][0]['allow_signing']);
        unset($request['requests']['actions'][0]['action_status']);

        $requestData = json_encode($request);

        $POST_DATA = array(
            'data' => $requestData
        );

        $curl = curl_init("https://sign.zoho.eu/api/v1/requests/".$documentCreate['requests']['request_id']."/submit");
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Authorization:Zoho-oauthtoken '.$authData->access_token,
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $POST_DATA);
        $response = curl_exec($curl);
        curl_close ($curl);
        $sendDocument = json_decode($response,true);

        if ($sendDocument['status'] == 'success') {
            unlink($fileRealPath);
            $updData = [
                'document_id' => $sendDocument['requests']['document_ids'][0]['document_id'],
                'action_id' => $sendDocument['requests']['actions'][0]['action_id'],
                'request_id' => $sendDocument['requests']['request_id'],
                'response' => json_encode($sendDocument),
                'status' => 'send'
            ];
            DB::table('zoho_douments_listing')->where('id', $params->id)->update($updData);

            echo $sendDocument['message']." on ".$params->email."\n";
        } else {
            //dd($sendDocument);
            echo $sendDocument['message']. " --- Somethig Went Wrong \n";
        }
    }

    public static function getDocuStatus($docuData) {
        $_this = new self;
        $authData = self::getLastData();

        $expiryTime = strtotime("+50 minutes", strtotime($authData->created_at));
        if ($expiryTime <= time()) {
            self::genrateRefreshToken();
            $authData = self::getLastData();
        }

        $id = (!empty($docuData->id)) ? $docuData->id : '' ;
        $type = (!empty($docuData->type)) ? $docuData->type : '' ;
        $userId = (!empty($docuData->user_id)) ? $docuData->user_id : '' ;
        $moduleId = (!empty($docuData->module_id)) ? $docuData->module_id : '' ;
        $requestId = (!empty($docuData->request_id)) ? $docuData->request_id : '' ;
        $documentId = (!empty($docuData->document_id)) ? $docuData->document_id : '' ;

        $fileName = 'docu_signed_'.$id.'_'.$moduleId.'_'.$type.'_'.$userId.'.pdf';

        $curl = curl_init("https://sign.zoho.eu/api/v1/requests/".$requestId);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Authorization:Zoho-oauthtoken '.$authData->access_token,
        ));
        $response = curl_exec($curl);
        curl_close ($curl);
        $response = json_decode($response, true);
        //dd($response);
        if ($response['status'] == 'success' && $response['requests']['request_status'] == 'completed') {
            $authData = self::saveDocu($docuData);
        } else {
            // dd($response);
            echo $fileName." Not signed Yet";
        }
    }

    public static function saveDocu($docuData) {
        $_this = new self;
        $authData = self::getLastData();

        $id = (!empty($docuData->id)) ? $docuData->id : '' ;
        $type = (!empty($docuData->type)) ? $docuData->type : '' ;
        $userId = (!empty($docuData->user_id)) ? $docuData->user_id : '' ;
        $moduleId = (!empty($docuData->module_id)) ? $docuData->module_id : '' ;
        $requestId = (!empty($docuData->request_id)) ? $docuData->request_id : '' ;
        $documentId = (!empty($docuData->document_id)) ? $docuData->document_id : '' ;

        $fileName = 'docu_signed_'.$id.'_'.$moduleId.'_'.$type.'_'.$userId.'.pdf';

        $fileRealPath = storage_path("app/public/aggrements/".$fileName);

        /*if (file_exists($fileRealPath)) {
            unlink($fileRealPath);
        }*/

        ob_start();
        $curl = curl_init("https://sign.zoho.eu/api/v1/requests/".$requestId."/documents/".$documentId."/pdf");
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Authorization:Zoho-oauthtoken '.$authData->access_token,
        ));
        //curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close ($curl);
        $html = ob_get_contents();
        ob_end_clean();
        file_put_contents($fileRealPath, $html);

        $updData = [
            'status' => 'signed'
        ];
        DB::table('zoho_douments_listing')->where('id', $id)->update($updData);

        $updData = [
            'status' => 'completed'
        ];
        DB::table('agreements')->where('id', $moduleId)->update($updData);

        $agreementData = DB::select("SELECT id, campaign_id, status FROM agreements WHERE id = ".$moduleId." LIMIT 1");
        if (!empty($agreementData)) {
            $campaign_id = $agreementData[0]->campaign_id;
            $campaignInvoice = DB::select("SELECT id, status FROM invoices WHERE campaign_id = ".$campaign_id." ORDER BY id ASC");
            if (!empty($campaignInvoice) && strtolower($campaignInvoice[0]->status) == 'completed') {
                $updData = [
                    'status' => 'awaiting'
                ];
                DB::table('campaigns')->where('id', $campaign_id)->update($updData);
            }
        }


        echo $fileName." Successfully Signed"."\n";
    }

    public static function read_docx($params){
        $striped_content = '';
        $content = '';

        $source = storage_path('app/public/account.docx');
        $source1 = storage_path('app/public/account_aggrement.docx');

        $templateProcessor = new TemplateProcessor($source);
        $templateProcessor->setValue('C_NAME', $params->name);
        $templateProcessor->setValue('C_EMAIL', $params->email);
        $templateProcessor->setValue('C_PHONE', $params->phone);
        $templateProcessor->setValue('C_ADDRESS', $params->address);
        $templateProcessor->setValue('C_AMOUNT', ' £ '.$params->amount);
        $templateProcessor->setValue('C_INSTALLMENT', $params->installment_count);
        $templateProcessor->saveAs($source1);
        if (file_exists($source1)) {
            return $source1;
        }
        return '';
    }
}

<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class OtherProject extends Model {
	use Notifiable;
    protected $fillable = ['user_id', 'project_name', 'description', 'start_date','end_date', 'no_of_months', 'price', 'budget', 'cost_type', 'other_status', 'created_by', 'status', 'is_deleted', 'deleted_by', 'deleted_at'];

    protected $appends = ['human_start_date', 'human_end_date', 'status_upper', 'picker_start_date', 'picker_end_date', 'end_in', 'type_text'];

    public function getTypeTextAttribute(){
        $type = $this->getOriginal('cost_type');
        if($type == 'one_off'){
            return 'One Off';
        } else if($type == 'monthly'){
            return 'Monthly';
        }
        return $type;
    }

    public function getEndDateAttribute($value){
        if(!empty($value)) {
            return Carbon::parse($value)->format('Y-m-d');
        }
        return null;
    }

    public function getEndInAttribute(){
        if($this->getOriginal('cost_type') == 'monthly') {
            return ($this->getOriginal('no_of_months') > 1) ? $this->getOriginal('no_of_months').' Months' : $this->getOriginal('no_of_months').' Month';
        } else {
            return Carbon::parse($this->getOriginal('end_date'))->format('M d,Y');

        }
        return null;
    }

    public function getHumanStartDateAttribute(){
        if(!empty($this->getOriginal('start_date'))) {
            return Carbon::parse($this->getOriginal('start_date'))->format('M d,Y');
        }
        return null;
    }

    public function getPickerStartDateAttribute(){
        if(!empty($this->getOriginal('start_date'))) {
            return Carbon::parse($this->getOriginal('start_date'))->format('Y-m-d');
        }
        return null;
    }

    public function getHumanEndDateAttribute(){
        if(!empty($this->getOriginal('end_date'))) {
            return Carbon::parse($this->getOriginal('end_date'))->format('M d,Y');
        }
        return null;
    }

    public function getPickerEndDateAttribute(){
        if(!empty($this->getOriginal('end_date'))) {
            return Carbon::parse($this->getOriginal('end_date'))->format('Y-m-d');
        }
        return null;
    }

    public function getStatusUpperAttribute(){
        $status = $this->getOriginal('status');
        if(!empty($status)) {
            if ($status == 'pending') {
                return 'IN PROGRESS';
            } 
            return strtoupper($status);
        }
        return null;
    }

    public function assignedmodule() {
        return $this->hasMany(AssignedModule::class, 'module_id', 'id');
    }

    public function invoices(){
        return $this->hasMany(Invoice::class, 'other_project_id', 'id');
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function installments(){
        return $this->hasMany(InvoiceInstallments::class, 'module_id', 'id')->where('module_type', 'projects');
    }
}

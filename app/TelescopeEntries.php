<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TelescopeEntries extends Model
{
    protected $table = "telescope_entries";


     public function tags(){
        return $this->belongsTo(TelescopeEntriesTags::class, 'entry_uuid', 'uuid');
    }


}

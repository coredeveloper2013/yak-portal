<?php

namespace App;

use DB;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class CronStatus extends Model
{
    use Notifiable;

    protected $table = 'cron_status';

    protected $fillable = ['type', 'alert_id', 'campaign_id','lead_id', 'module_type', 'send_on', 'send_by', 'response'];
}

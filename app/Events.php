<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
     protected $fillable = ['name' , 'description' , 'category_id' , 'event_time' , 'is_send_sms'];

     public function category(){
        return $this->belongsTo(Categories::class);
    }
}
 
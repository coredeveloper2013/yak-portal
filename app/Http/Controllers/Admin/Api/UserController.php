<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\Http\Resources\Users as UsersResource;
use Validator;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
class UserController extends Controller
{
    //
  	protected $_helper;
    public function __construct()
    {
        $this->_helper = new Helper();
    }
// public function getUser(Request $request) {
//         if($request->id){
//              $user = User::find($request->id);
//              dd($user);
//              if($user){
//                     $data = [
//                     'status' => 'success',
//                     'msg' => 'Record Found.',
//                     ];
//                     $data['data']['user'] = UsersResource::collection($user);
//                     $this->_helper->response()->setCode(200)->send($data);
//              }else{
//                 $data = [
//                 'status' => 'error',
//                 'msg' => 'Record not Found.',
//                 'data' =>[]
//             ];
//             $this->_helper->response()->setCode(200)->send($data);

//              }
//          }
//      }

    public function index(Request $request) {
        if($request->id){
            $user = \App\User::find($request->id);
             if($user){
                    $data = [
                    'status' => 'success',
                    'msg' => 'Record Found.',
                    ];
                    $data['data']['user'] = $user;
                    $this->_helper->response()->setCode(200)->send($data);
             }else{
                $data = [
                'status' => 'error',
                'msg' => 'Record not Found.',
                'data' =>[]
            ];
            $this->_helper->response()->setCode(200)->send($data);

             }
        }else{
        $user = \App\User::all();
        }
        
        if($user){
	         $data = [
	            'status' => 'success',
	            'msg' => 'Record Found.',
	        ];
	        $data['data']['users'] = UsersResource::collection($user);
	        

	        $this->_helper->response()->setCode(200)->send($data);
    	}else{
    		$data = [
	            'status' => 'error',
	            'msg' => 'Record not Found.',
	            'data' =>[]
	        ];
	        $this->_helper->response()->setCode(200)->send($data);
    	}
        //return view('admin.user.index', compact('result'));
    }


    public function save(Request $request) {
    	$this->_helper->runValidation([
             		'name' => 'required',
                    'username' => 'required|unique:users',
                    'email' => 'required|unique:users',
                    'password' => 'required|min:6'
        ],[
            'name.required' => 'Name field required',
            'username.required' => 'UserName field required',
            'email.required' => 'Email  field required',
            'password.required' => 'Password field required',
        ]);

        	
           // return redirect()->back()->withErrors($validator)->withInput();
        
        $obj = new \App\User();
        $obj->name = $request->name;
        $obj->email = $request->email;
        $obj->username = $request->username;
        $obj->password = Hash::make($request->password);
        $obj->type = $request->type;
        $isSaved = $obj->save();

        $data = [
            'status' => 'error',
            'msg' => 'There might be some errors, Try again later.',
        ];
        if($isSaved){
          $data = [
	            'status' => 'success',
	            'msg' => 'Record Saved successfully',
	        ];
	        $this->_helper->response()->setCode(200)->send($data);

	    }
        //return redirect()->back()->with('success', __('msg.created_successfully'));
    }

    public function update(Request $request) {

      
             $this->_helper->runValidation([
            'id' => 'required'
        ],[
            'id.required' => 'User required'
        ]);

           $objLanding = \App\User::find($request->id);
           if(!$objLanding){
           	$data = [
            'status' => 'error',
            'msg' => 'Record Not Found.',
        			];
        	$this->_helper->response()->setCode(200)->send($data);
           }
        if ($request->name !== null)
            $objLanding->name = $request->name;
        if ($request->type !== null)
            $objLanding->type = $request->type;
        if ($request->email !== null)
            $objLanding['email'] = $request->email;
        if ($request->password !== null){
        	 $this->_helper->runValidation([
                    'password' => 'required|min:6'
        ],[
            'password.required' => 'password must be at least 6 character long'
        ]);
            $objLanding['password'] = Hash::make($request->password);;
        }
        if ($request->username !== null)
            $objLanding['username'] = $request->username;
       		
       		 $objLanding->save();
        //$result = \App\User::where('id', $request->id)->update($objLanding);
        if ($objLanding){
        		$data = [
		            'status' => 'success',
		            'msg' => 'Record update successfully.',
        			];
        }
           // return redirect()->back()->with('success', __('msg.updated_successfully'));
        else{
        	$data = [
		            'status' => 'error',
		            'msg' => 'Record Not Saved successfully.',
        			];
        }
            //return redirect()->back()->with('error', __('msg.some_error'));
        	$this->_helper->response()->setCode(200)->send($data);

    }

    public function destroy(Request $request) {
        //$news = \App\User::where('id', $request->id)->delete();
        $user = new \App\User();
        $user = $user->find($request->id);
        $user->delete();
        $user->save();

        $data = [
            'status' => 'success',
            'msg' => 'Record Deleted Saved successfully.',
		];
        $this->_helper->response()->setCode(200)->send($data);

        //return redirect()->back()->with('success', __('msg.deleted_successfully'));
    }
}

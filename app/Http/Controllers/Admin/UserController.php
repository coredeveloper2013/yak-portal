<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller {

    public function index() {

        $result = \App\User::where('type', 'staff')->get();
        return view('admin.user.index', compact('result'));
    }

    public function add(Request $request) {
        return view('admin.user.add');
    }

    public function edit(Request $request, $id) {
        $result = \App\User::find($id);

        return view('admin.user.edit', compact('result'));
    }

    public function save(Request $request) {
        $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'username' => 'required|unique:users',
                    'email' => 'required|unique:users',
                    'password' => 'required|min:6',
                'type'=> 'required'
                        ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $obj = new \App\User();
        $obj->name = $request->name;
        $obj->email = $request->email;
        $obj->username = $request->username;
        $obj->password = Hash::make($request->password);
        $obj->type = $request->type;
        $obj->save();

        return redirect()->back()->with('success', __('msg.created_successfully'));
    }

    public function update(Request $request) {

        $validator = Validator::make($request->all(), [
                    'id' => 'required'
                        ]
        );
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        if ($request->name !== null)
            $objLanding['name'] = $request->name;
        if ($request->email !== null)
            $objLanding['email'] = $request->email;
        if ($request->password !== null)
            $objLanding['password'] = Hash::make($request->password);;
        if ($request->username !== null)
            $objLanding['username'] = $request->username;

        $objLanding['type'] = $request->type;

        $result = \App\User::where('id', $request->id)->update($objLanding);
        if ($result)
            return redirect()->back()->with('success', __('msg.updated_successfully'));
        else
            return redirect()->back()->with('error', __('msg.some_error'));
    }

    public function del(Request $request, $id) {
        //$news = \App\User::where('id', $id)->delete();
        $user = new \App\User();
        $user = $user->find($id);
        $user->delete();
        $user->save();
        return redirect()->back()->with('success', __('msg.deleted_successfully'));
    }

}

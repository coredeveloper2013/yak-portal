<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\User;

class LoginController extends Controller {

    protected $guard = 'admin';
    protected $redirectTo = '/admin/users';
    protected $loginPath = '/admin/login';

    public function __construct() {
        $this->redirectTo = '/admin';
    }

    protected function validator(array $data) {
        return Validator::make($data, [
                    'name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:admins',
                    'password' => 'required|min:6|confirmed',
        ]);
    }

    public function logout() {
        Auth::logout();
        return redirect('/admin/login');
    }

    public function login(Request $request) {



        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);

        $admin = User::where(['type' => 'admin', 'email' => $request->email])->first();
        if (!$admin) {
            return redirect($this->loginPath)->with('error', 'Admin bulunamadi.');
        }

        if (Hash::check($request->password, $admin->password)) {
            Auth::guard('admin')->login($admin);
            return redirect('/admin/users');
        }

        return redirect($this->loginPath)
                        ->withInput($request->only('email', 'remember'))
                        ->withErrors(['email' => 'Incorrect email address or password']);
    }

    public function showLoginForm() {
        if (Auth::guard('admin')->check()) {
            return redirect('/admin/users');
        }

        return view('admin.auth.login');
    }

}

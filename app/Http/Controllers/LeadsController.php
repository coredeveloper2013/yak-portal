<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Campaign;
use App\Leads;
use App\Helpers\Helper;
use Auth;

class LeadsController extends Controller
{
    private $_helper;

    public function __construct(){
        $this->_helper = new Helper();
    }

    public function index($campain_id)
    {
        $campaign = Campaign::findOrFail($campain_id);

        $leads = Auth::user()->leads();;
        $leads =  $leads->where('campaign_id', $campain_id);
        $leads =  $leads->orderBy('id', 'DESC');
        
        if(!$leads->exists()){
            $leads = [];
        }else{
            $leads = $leads->first();
        }

        $data['campaign'] = $campaign;
        $data['leads'] = $leads;
        $data['page_title'] = "Leads";
        if (request()->wantsJson()) {
            $this->_helper->response()->send(['data' => $data]);
        } else {
            return view('leads.index', $data);
        }
    }
}

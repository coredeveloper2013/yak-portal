<?php

namespace App\Http\Controllers\Staff;

use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\Campaigns as CampaignsResource;
use App\Helpers\Helper;
use Auth;
use App\Campaign;
use App\User;
use App\LandingPage;

class StaffAccountController extends Controller
{
    private $_helper;

    public function __construct(){
        $this->_helper = new Helper();

        $this->middleware(function ($request, $next) {
            if( $this->_helper->isAllowedModule('staff_account') == false)
		   		{
		   			return abort(404);
		   		}

            return $next($request);
        });
    }

    public function index(){
        $users = User::all();

        return view('staff.staffAccount.index');
    }

    public function uploadProfile(Request $request)
    {
        if($request->hasFile('img')){
            //save user image
            $user = Auth::user();
            $image = Auth::user()->id.'-'.$request->file('img')->getClientOriginalName();
            $ext = $request->file('img')->getClientOriginalExtension();

            if(Storage::exists('avatars/'.$image)){
                Storage::delete('avatars/'.$image);
            }

            $path = $request->file('img')->storeAs(
                'avatars', $image
            );

            $user->image = $image;

            $user->save();

            return redirect()->back()->with('general', 'Profile Image Updated.');
        }
    }
}

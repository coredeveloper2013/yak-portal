<?php

namespace App\Http\Controllers\Staff;

use App\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\Campaigns as CampaignsResource;
use App\Helpers\Helper;
use Auth;
use App\Campaign;
use App\LandingPage;
use DB;
class WorkLoadConrtroller extends Controller
{
    private $_helper;

    public function __construct(){
        $this->_helper = new Helper();
    }

    public function index(){
        $sum_camp = DB::table('campaigns')
            ->select(DB::raw('status, sum(amount) as sum'))
            ->groupBy('status')
            ->pluck('sum', 'status');

        $count_camp = DB::table('campaigns')
            ->select(DB::raw('status, count(amount) as total'))
            ->groupBy('status')
            ->pluck('total', 'status');

        $staff_camp = DB::table('campaigns as c')
            ->join('users as u', 'u.id', '=', 'c.assigned_id')
            ->select(DB::raw('COUNT(c.id) total, c.assigned_id, u.name staff_name'))
            ->groupBy('c.assigned_id')
            ->get();
            $users=User::where('type','staff')->get();
            $campp=[];
            foreach ($users as $key => $value) {
            $compains=DB::table('campaigns as c')
            ->select(DB::raw('COUNT(c.id) total'))
            ->where('c.assigned_id',$value->id)
            ->get();
             $project=DB::table('other_projects as p')
             ->join('assigned_modules as am','am.module_id','p.id')
            ->select(DB::raw('COUNT(p.id) total'))
            ->where('am.assign_to',$value->id)
            ->where('am.module_type','other_projects')
            ->get();

            $landingPage=DB::table('landing_pages as l')
            ->select(DB::raw('COUNT(l.id) total'))
            ->where('l.assigned_to',$value->id)
            ->get();

              $temp['total']=($compains[0]->total)+($project[0]->total)+($landingPage[0]->total);
              $temp['camp_total']= $compains[0]->total;
              $temp['proj_total']= $project[0]->total;
              $temp['landing_total']= $landingPage[0]->total;
              $temp['staff_name']= $value->name;
              $temp['assigned_id']= $value->id;

              $campp[]= $temp;
            }
          
        $staff_pages = DB::table('landing_pages as l')
            ->join('users as u', 'u.id', '=', 'l.assigned_to')
            ->select(DB::raw('COUNT(l.id) total, l.assigned_to, u.name staff_name'))
            ->groupBy('l.assigned_to')
            ->get();  

        return view('staff.workload.index',[
            'sum_camp' => $sum_camp,
            'count_camp' => $count_camp,
            // 'staff_camp' => $staff_camp,
            'staff_camp' => $campp,
            'staff_pages' => $staff_pages,
        ]);
    }
}
 	
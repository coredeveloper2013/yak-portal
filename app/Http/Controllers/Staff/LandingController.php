<?php

namespace App\Http\Controllers\Staff;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Invoice;
use App\LandingPage;
use App\LandingPageComments;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Auth;

class LandingController extends Controller
{
    protected $_helper;

    public function __construct()
    {
        $this->_helper = new Helper();

        $this->middleware(function ($request, $next) {
            if( $this->_helper->isAllowedModule('landing_pages') == false)
                {
                    return abort(404);
                }

            return $next($request);
        });
    }

    public function listing()
    {
        /*$members = \App\User::where('type', 'staff')->get();
        $customers = \App\User::where('type', 'customer')->get();

        $data = [
            'members' => $members,
            'customers' => $customers
        ];*/
        $data = [];
        return view('staff.landing.index', $data);
    }

    public function index()
    {
        $request = request();
        $pages = new \App\LandingPage();
        $pages=$pages->getAll($request);
        $this->_helper->response()->setCode(200)->send(['data' => $pages]);
    }

    public function allStaffMembers()
    {

        $members = \App\User::where('type', 'staff')->get();

        $this->_helper->response()->send(['data' => $members]);

    }

    public function allCustomers()
    {

        $customers = \App\User::where('type', 'customer')->get();

        $this->_helper->response()->send(['data' => $customers]);

    }

    public function create()
    {
        $request = request();

        //dd($request->all());

        $messages = [
            'amount.min' => 'Please enter positive integer',
            'created_by.required' => 'You must select any cutomer'
        ];

        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            //'cost' => 'required|numeric|min:0|not_in:0',
            //'budget' => 'required|numeric|min:0|not_in:0',
            'description' => 'required',
            'url' => 'required|url',
            'status' => 'required',
            'date' => 'required',
            'created_by' => 'required'
        ], $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $objLanding = new \App\LandingPage();
        $objLanding->user_id = $request->created_by;
        $objLanding->name = $request->name;
        $objLanding->cost = 0;//$request->cost;
        $objLanding->description = $request->description;
        $objLanding->url = $request->url;
        $objLanding->status = $request->status;
        $objLanding->date = Carbon::parse($request->date)->format('m/d/Y');
        $objLanding->budget = 0;//$request->budget;
        $objLanding->assigned_to = $request->staff_member;
        $objLanding->created_by = Auth::user()->id;
        $objLanding->save();

        /*Invoice::create([
            'user_id' => $request->created_by,
            'type' => 'landing_page',
            'status' => 'pending',
            'campaign_id' => null,
            'landing_pages_id' => $objLanding->id
        ]);*/

        return redirect()->route('staff.landing');
    }

    public function update()
    {

        $request = request();

        $messages = [
            'amount.min' => 'Please enter positive integer',
            'created_by.required' => 'You must select any cutomer'
        ];

        $validator = Validator::make($request->all(), [
            'id' => 'required|string',
            'name' => 'required|string',
            //'cost' => 'required|numeric|min:0|not_in:0',
            //'budget' => 'required|numeric|min:0|not_in:0',
            'description' => 'required',
            'url' => 'required|url',
            'status' => 'required',
            'date' => 'required',
            'created_by' => 'required'
        ], $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput()->with('modal','Edit');
        }
        if ($request->created_by !== null) {
            $obj['user_id'] =  ($request->created_by);
        }
        if ($request->name !== null) {
            $obj['name'] =  ($request->name);
        }
        if (!empty($request->cost)) {
            $obj['cost'] = $request->cost;
        }
        if ($request->description !== null) {
            $obj['description'] = $request->description;
        }
        if ($request->url !== null) {
            $obj['url'] = $request->url;
        }
        if ($request->status !== null) {
            $obj['status'] = $request->status;
        }
        if ($request->date !== null) {
            $obj['date'] = Carbon::parse($request->date)->format('m/d/Y');;
        }
        if (!empty($request->budget)) {
            $obj['budget'] = $request->budget;
        }
        if ($request->assigned_to !== null) {
            $obj['assigned_to'] = $request->assigned_to;

        }


        $result = \App\LandingPage::where('id', $request->id)->update($obj);

        return redirect()->route('staff.landing');
    }


    public function assignToMe()
    {
        $landing = new \App\LandingPage();

        $landing = $landing->find(request()->id);

        $landing->assigned_to = Auth::user()->id;

        $landing->save();

        $this->_helper->response()->send('');

    }

    public function assignToOther(){
        $landing = new \App\LandingPage();

        $landing = $landing->find(request()->id);

        $landing->assigned_to = request()->assigned_to_id;

        $landing->save();

        $this->_helper->response()->send('');
    }

    public function show($id)
    {
        $landing_page = LandingPage::with([
                            'comments' , 
                            'assigned'  => function ($query) {
                                $query->withTrashed();
                            }, 
                            'user' => function ($query) {
                                $query->withTrashed();
                                $query->with(['company']);
                                $query->with('business_info');
                            }
                        ])->where('id', $id)->first();

        /*echo "<pre>";
        print_r(json_encode($landing_page));
        echo "</pre>";
        exit;*/
        $data = [
            'landing_page' => $landing_page
        ];
        return view('staff.landing.details', $data);
    }
    public function postComment()
    {

        $request = request();

        LandingPageComments::create([
            'landing_page_id' => $request->id,
            'user_id' => Auth::user()->id,
            'description' => $request->text
        ]);

        $landing_page = LandingPage::with('comments')->where('id', $request->id)->first();
       // dd($landing_page);
        $this->_helper->response()->send(['data' => $landing_page]);
    }
}

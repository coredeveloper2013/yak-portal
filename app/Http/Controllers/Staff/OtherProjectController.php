<?php

namespace App\Http\Controllers\Staff;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class OtherProjectController extends Controller {

	public function __construct() {
        $this->_helper = new Helper();

        $this->middleware(function ($request, $next) {
            if( $this->_helper->isAllowedModule('projects') == false)
                {
                    return abort(404);
                }

            return $next($request);
        });
    }

    public function index() {
        return view('staff.other_projects.index');
    }

    public function detail($id) {
    	$data = [];
    	$projects = DB::select("SELECT p.*, u.`name` as customer_name, u.`image` as customer_image, u.`email` as customer_email, u.`status` as customer_status, u.`phone` as customer_phone FROM `other_projects` p JOIN users u ON p.`user_id` = u.`id` WHERE p.`is_deleted` = 'N' AND p.`id` = ".$id);

    	if (!empty($projects)) {
    		$data['projects'] = $projects[0];
    		$data['projects']->user = (object)[];
    		$data['projects']->user->id = $projects[0]->user_id;
    		$data['projects']->user->name = $projects[0]->customer_name;
    		$data['projects']->user->image = $projects[0]->customer_image;
    		$data['projects']->user->email = $projects[0]->customer_email;
    		$data['projects']->user->status = $projects[0]->customer_status;
    		$data['projects']->user->phone = $projects[0]->customer_phone;

    		$business_info = DB::select("SELECT t.* FROM `user_business_information` t WHERE t.`user_id`  = ".$projects[0]->user_id);
    		$data['projects']->user->business_info = $business_info[0];

    		if (request()->has('tab') && isset(request()->tab)) {
	            if (request()->tab == 'notes') {
	                $comments = DB::select("SELECT u.`name`, u.`image`, u.`type`, c.`id`, c.`comment_text`, c.`created_at` FROM `project_comments` c JOIN users u ON c.`user_id` = u.`id` WHERE c.`module_id`  = ".$id." AND c.`module_type`='other_projects' ORDER BY c.`id` ASC");
					if (!empty($comments)) {
						foreach ($comments as $key => $value) {
							$comments[$key]->user = (object)[];
							$comments[$key]->user->name = $value->name;
				    		$comments[$key]->user->image = $value->image;
				    		$comments[$key]->user->type = $value->type;
							$comments[$key]->date = Carbon::parse($value->created_at)->format('M d Y');
							$comments[$key]->time = Carbon::parse($value->created_at)->format('g:i A');
						}
					}
					$data['projects']->comments = $comments;
	            } elseif (request()->tab == 'assign_detail') {
	                $assignsTo = DB::select("SELECT u.`name`, u.`image`, u.`type` FROM `assigned_modules` am JOIN users u ON am.`assign_to` = u.`id` WHERE am.`module_id`  = ".$id." AND am.`module_type`='other_projects'");
					$data['projects']->assigned_to = $assignsTo;
	            } elseif (request()->tab == 'tasks') {
	                $tasks = DB::select("SELECT t.* FROM `tasks` t WHERE t.`module_id`  = ".$id." AND t.`link_type`='other_project'");
					$data['projects']->tasks = $tasks;
	            } elseif (request()->tab == 'installment') {
	                $installment = DB::select("SELECT t.* FROM `invoice_installments` t WHERE t.`module_id`  = ".$id." AND t.`module_type`='projects'");
					$data['projects']->installments = $installment;
	            }
	        } else {
		    	$assignsTo = DB::select("SELECT u.`name`, u.`image`, u.`type` FROM `assigned_modules` am JOIN users u ON am.`assign_to` = u.`id` WHERE am.`module_id`  = ".$id." AND am.`module_type`='other_projects'");
				$data['projects']->assigned_to = $assignsTo;

				$tasks = DB::select("SELECT t.* FROM `tasks` t WHERE t.`module_id`  = ".$id." AND t.`link_type`='other_project'");
				$data['projects']->tasks = $tasks;

				$comments = DB::select("SELECT u.`name`, u.`image`, u.`type`, c.`id`, c.`comment_text`, c.`created_at` FROM `project_comments` c JOIN users u ON c.`user_id` = u.`id` WHERE c.`module_id`  = ".$id." AND c.`module_type`='other_projects' ORDER BY c.`id` ASC");
				if (!empty($comments)) {
					foreach ($comments as $key => $value) {
						$comments[$key]->date = Carbon::parse($value->created_at)->format('M d Y');
						$comments[$key]->time = Carbon::parse($value->created_at)->format('g:i A');
					}
				}
				$data['projects']->comments = $comments;

				$installment = DB::select("SELECT t.* FROM `invoice_installments` t WHERE t.`module_id`  = ".$id." AND t.`module_type`='projects'");
				$data['projects']->installments = $installment;
	        }
		}

		if (request()->wantsJson()) {
			$this->_helper->response()->setCode(200)->send(['data' => $data]);
		} else {
			if (!empty($data['projects']) && !empty($data['projects']->comments)) {
				foreach ($data['projects']->comments as $key => $value) {
					$data['projects']->comments[$key]->comment_text = nl2br($value->comment_text);
				}
			}

			return view('staff.other_projects.projectdetail', $data);
		}
    }
}

<?php

namespace App\Http\Controllers\Staff\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\Campaigns as CampaignsResource;
use App\Http\Resources\TargetR;
use App\Http\Resources\Users as UserResource;
use App\Http\Resources\UserTarget as UserTarget;
use App\Helpers\Helper;
use Auth;
use App\Campaign;
use App\Target;
use App\User;
use App\Invoice;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Gate;


class TargetsController extends Controller
{
    //
  private $_helper;

  public function __construct(){
    $this->_helper = new Helper();
  }

  public function add()
  {
    $request = request();

    $this->_helper->runValidation([
      'year' => 'required',

      'month' => 'required|string',
      'user' => 'required',
      'new_business' => 'required',
      'retention' => 'required',
      'project' => 'required',
    ]  );

    $alreadyexist=Target::where(['user_id'=>intval( $request->user),'month'=>$request->month,'busnissYear'=>date('Y-m-d', strtotime($request->year.'-01-01'))])->get();
    if( !empty($alreadyexist[0]->month)){
      $response = [
        'status' => 'error',
        'msg' => 'Target already.',
      ];
      $this->_helper->response()->send($response);
    }
    $User = Target::create([
      'month' => $request->month,
      'user_id' => intval( $request->user),
      'new_business' => $request->new_business,
      'busnissYear' =>date('Y-m-d', strtotime($request->year.'-01-01')),
      'project' => $request->project,
      'retention' => $request->retention
    ]);

    $this->_helper->response()->setMessage('Target Saved Successfully.')->send('');
  }

  public function update()
  {
    $request = request();

    $this->_helper->runValidation([
      'id' => 'required',
      'year' => 'required',
      'month' => 'required|string',
      'user' => 'required',
      'new_business' => 'required',
      'retention' => 'required',
      'project' => 'required',

    ]  );


    $alreadyexist=Target::where(['user_id'=>intval( $request->user),'month'=>$request->month,'busnissYear'=>date('Y-m-d', strtotime($request->year.'-01-01'))])->get();
    if( !empty($alreadyexist[0]->month ) and $alreadyexist[0]->id != $request->id){
      $response = [
        'status' => 'error',
        'msg' => 'Target already.',
      ];
      $this->_helper->response()->send($response);
    }
    if ($request->month !== null)
      $obj['month'] = $request->month;
    if ($request->year !== null)
      $obj['busnissYear'] = date('Y-m-d', strtotime($request->year.'-01-01'));
    if ($request->new_business !== null)
      $obj['new_business'] = $request->new_business;
    if ($request->retention !== null)
      $obj['retention'] = $request->retention; 
     if ($request->project !== null)
      $obj['project'] = $request->project; 
    $result = \App\Target::where('id', $request->id)->update($obj);

    $this->_helper->response()->setMessage('Target Updated Successfully.')->send('');
  }


  public function get(Request $request)
  {
    $data = [
     'status' => 'success',
     'msg' => 'Success.',
     'data' => [],
   ];
   $year=date('Y');
   if(!empty($request->year))
   { 
    $year=$request->year;

  }
  if(!$request->id) 
  { 
	     //  $terget = Target::all();
	     //  $data['data']['targets'] =  ($terget);
    $users =  User::where('type','staff');
    if(!Gate::allows('isAdmin'))
      $users =  $users->where('id',Auth::user()->id); 
    $users =  $users->get(); 
    $data['data']['targets']=[];
    foreach($users as $key => $value) {

     $item['user']= new UserResource($value);
     $item['targets']=TargetR::collection( 
      Target::where('user_id',$value->id)
      ->whereYear('busnissYear',$year )
      ->orderBy('targets.month','asc')
      ->get());
     $data['data']['targets'][]=$item;

   }
 }
 else 
 { 
   $terget = new TargetR(Target::find($request->id));
   $data['data']['targets'] =  ($terget);

 }

 $data['data']['isAdmin']=Gate::allows('isAdmin');
 $this->_helper->response()->setCode(200)->send($data);
}
}

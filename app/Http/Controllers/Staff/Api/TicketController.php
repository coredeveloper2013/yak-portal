<?php

namespace App\Http\Controllers\Staff\Api;

use Auth;
use Carbon\Carbon;
use App\TicketChat;
use App\SupportTicket;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class TicketController extends Controller
{
    public function __construct() {
        $this->_helper = new Helper();
    }

    public function listing() {
        $request = request();
        $oTicket = new SupportTicket();
	    $ticket = $oTicket->getAllDataWithComments($request);

        $this->_helper->response()->setCode(200)->send(['data' => $ticket]);
    }

    public function getComments() {
    	$request = request();
        $oComments = new TicketChat();

    	if (empty($request->id)) {
            $data['msg'] = 'Parameter Missing';
            $this->_helper->response()->setCode(200)->send($data);
        }


	    $comments = $oComments->getCommentsByTicketId($request->id);

	    $data['show_comments'] = (!empty($comments)) ? 'Y' : 'N' ;
	    $data['comments'] = $comments;

        $this->_helper->response()->setCode(200)->send(['data' => $data]);
    }

    public function AssignTicket() {
        $data = [
            'status' => 'error',
            'data' => [],
        ];

        $request = request();
        if (empty($request->id) || empty($request->staff_id)) {
            $data['msg'] = 'Parameter Missing';
            $this->_helper->response()->setCode(200)->send($data);
        }

        $members = \App\User::whereIn('type', ['staff', 'admin'])->where('id', $request->staff_id);

        if (!$members->exists()) {
            $data['msg'] = 'Invalid Staff member or Staff member has been deactivated.';
            $this->_helper->response()->setCode(200)->send($data);
        }

        $projects = new SupportTicket();
        $projects = $projects->where('status', '!=', 'deleted')->where('id', $request->id);

        if (!$projects->exists()) {
            $data['msg'] = 'Invalid Ticket or ticket has been deleted.';
            $this->_helper->response()->setCode(200)->send($data);
        }

        $projects = $projects->first();

        if (!empty($projects->assigned_to)) {
            $data['msg'] = 'This Ticket already assigned to another staff member.';
            $this->_helper->response()->setCode(200)->send($data);
        }

        $projects->assigned_to = $request->staff_id;
        $projects->save();

        $msg = 'Successfull Assigned to the selected staff member.';
        if (Auth::user()->id == $request->staff_id) {
	        $msg = 'Successfull Assigned to you.';
        }
        $data = [
            'status' => 'success',
            'msg' => $msg,
            'data' => [],
        ];

        $this->_helper->response()->setCode(200)->send($data);
    }

    public function addComment() {
        $data = [
            'status' => 'error',
            'data' => [],
        ];

        $request = request();

        if (empty($request->id)) {
            $data['msg'] = 'Invalid Project Id';
            $this->_helper->response()->setCode(200)->send($data);
        }

        if (empty($request->text)) {
            $data['msg'] = 'Enter some text in note area';
            $this->_helper->response()->setCode(200)->send($data);
        }

        $projects = new SupportTicket();
        $projects = $projects->where('status', '!=', 'deleted')->where('id', $request->id);

        if (!$projects->exists()) {
            $data['msg'] = 'Invalid Ticket or ticket has been deleted.';
            $this->_helper->response()->setCode(200)->send($data);
        }
        $projects = $projects->first();

        /*ProjectComment::create([
            'module_id' => $request->id,
            'user_id' => Auth::user()->id,
            'comment_text' => $request->text,
            'module_type' => 'other_projects',
        ]);*/

        $comments = new TicketChat();
        $comments->ticket_id = $request->id;
        $comments->user_id = Auth::user()->id;
        $comments->chat_text = $request->text;
        $comments->save();
        $insertedId = $comments->id;

        $commentData = $comments->getCommentsByCommentId($insertedId);

        $msg = 'Comment successfully added.';
        $data = [
            'status' => 'Success',
            'msg' => $msg,
            'data' => $commentData,
        ];

        $this->_helper->response()->setCode(200)->send($data);
    }

    public function destroyTicket() {
        $data = [
            'status' => 'error',
            'data' => [],
        ];

        $request = request();

        if (empty($request->id)) {
            $data['msg'] = 'Id not found';
            $this->_helper->response()->setCode(200)->send($data);
        }

        $projects = new SupportTicket();
        $projects = $projects->where('status', '!=', 'deleted')->where('id', $request->id);

        if (!$projects->exists()) {
            $data['msg'] = 'Invalid Ticket or ticket has been deleted already.';
            $this->_helper->response()->setCode(200)->send($data);
        }
        $projects = $projects->first();

        $projects->status = 'deleted';
        $projects->updated_at = date('Y-m-d H:i:s');
        $projects->save();

        $msg = 'Ticket successfully deleted.';
        $data = [
            'status' => 'success',
            'msg' => $msg,
            'data' => [],
        ];

        $this->_helper->response()->setCode(200)->send($data);
    }

    public function completeTicket() {
        $data = [
            'status' => 'error',
            'data' => [],
        ];

        $request = request();

        if (empty($request->id)) {
            $data['msg'] = 'Id not found';
            $this->_helper->response()->setCode(200)->send($data);
        }

        $projects = new SupportTicket();
        $projects = $projects->where('status', '!=', 'deleted')->where('id', $request->id);

        if (!$projects->exists()) {
            $data['msg'] = 'Invalid Ticket or ticket has been deleted.';
            $this->_helper->response()->setCode(200)->send($data);
        }
        $projects = $projects->first();

        $projects->status = 'completed';
        $projects->updated_at = date('Y-m-d H:i:s');
        $projects->save();

        $msg = 'Mark as completed.';
        $data = [
            'status' => 'success',
            'msg' => $msg,
            'data' => [],
        ];

        $this->_helper->response()->setCode(200)->send($data);
    }
}

<?php

namespace App\Http\Controllers\Staff\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Categories;
use App\AssignedModule;

use App\Events;
use App\Event_Invitees;
use App\Helpers\Helper;
use Carbon\Carbon;

class CalenderConrtroller extends Controller
{

	private $_helper;

    public function __construct(){
        $this->_helper = new Helper();
    }

    public function storeCategory(Request $request)
    {
    	$request = request();

        $this->_helper->runValidation([
            'name'  => 'required|string',
            'color' => 'required|string',
            'staff_id.*' => 'required'
        ]  );

    	$category = Categories::create([
            'name'  => $request->name,
            'color' => $request->color   
        ]);
      if (!empty($request->staff_id)) {
                foreach ($request->staff_id as $asignee){
                    AssignedModule::create([
                        'module_id' => $category->id,
                        'assign_to' => $asignee,
                        'module_type' => 'category',
                    ]);
                }
            }
    	$this->_helper->response()->setMessage('Category Saved Successfully.')->send('');
    }


    public function storeEvent(Request $request)
    {
        $request = request();

        $this->_helper->runValidation([
            'name'              => 'required|string',
            'description'       => 'required|string',
            'event_time'        => 'required|string',
            'category_id'       => 'required|integer',
            'is_send_sms'       => 'required|integer',
            'invitee_names.*'   => 'required|string',
            'invitee_phones.*'  => 'required|string'
        ]  );

        $event = Events::create([
            'name'          => $request->name,
            'description'   => $request->description,
            'event_time'    => Carbon::parse($request->event_time)->format('Y-m-d h:m:i'),
            'category_id'   => $request->category_id,
            'is_send_sms'   => $request->is_send_sms  
        ]);

        for ($i = 0; $i < count($request->invitee_names); $i++) {
            $invitee = array(
                'name' => $request->invitee_names[$i],
                'phone_number' =>  $request->invitee_phones[$i],
                'event_id' =>  $event->id,
            );
            Event_Invitees::create($invitee);
        }
        
        $this->_helper->response()->setMessage('Event Saved Successfully.')->send('');
    }

    public function getEvent()
    {
       $request = request(); 
       $events = Events::with(['category'])
                 ->where('event_time' , '>=' , Carbon::parse($request->start)->format('Y-m-d 00:00:00'))
                 ->where('event_time' , '<=' , Carbon::parse($request->end)->format('Y-m-d 23:59:59'))
                 ->whereIn('category_id' , $request->categories)
                 ->get();

       $result = [];
       foreach ($events as $event) {
          $category_class = $event->category->color;
          $split = explode("-", $category_class);
          $event_color = $split[count($split)-1] . '-e';
          $temp = array(
            'title'     => $event->name, 
            'start'     => $event->event_time , 
            'className' => $event_color, 
        );
        $result[]= $temp; 
       }
       return json_encode($result);
    }
}

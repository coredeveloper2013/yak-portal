<?php

namespace App\Http\Controllers\Staff\Api;

use Auth;
use App\User;
use App\Company;
use App\Invoice;
use App\Campaign;
use App\Agreement;
use Carbon\Carbon;
use App\LandingPage;
use App\Notification;
use App\OtherProject;
use App\Helpers\Helper;
use App\CampaignComments;
use App\AgreementDocuSign;
use App\InvoiceInstallments;
use Illuminate\Http\Request;
use App\UserBusinessInformation;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\CampaignsPaginate as CampaignsPaginateResource;

class CampaignsController extends Controller
{
    private $_helper;

    public function __construct()
    {
        $this->_helper = new Helper();
    }

    public function getCampaigns(Request $request)
    {
        $data = [
            'status' => 'success',
            'msg' => 'Success.',
            'data' => [],
        ];

        $campaigns = new Campaign();
        if ($request->has('filter')) {
            if (!empty($request->filter['status'])) {
                $campaigns = $campaigns->where('status', $request->filter['status']);
            }
        } else if (!empty($request->status_mob)) {
            $campaigns = $campaigns->where('status', $request->status_mob);
        }

        if (Auth::user()->type != 'admin') {
            $campaigns = $campaigns->where(function ($q) {
                $q->where('campaign_specialist', Auth::user()->id)
                    ->orWhere('assigned_id', Auth::user()->id)
                    ->orWhere('lp_specialist', Auth::user()->id)
                    ->orWhere('account_manager', Auth::user()->id);
            });
        }

        $campaigns = $campaigns->orderBy('id', 'DESC')->paginate();
        $data['data'] =  new CampaignsPaginateResource($campaigns);
        $this->_helper->response()->setCode(200)->send($data);
    }

    public function recordById()
    {
        $data = [
            'status' => 'error',
            'data' => [],
        ];

        $request = request();

        if (empty($request->id)) {
            $data['msg'] = 'Id not found';
            $this->_helper->response()->setCode(200)->send($data);
        }

        $oCampaign = new Campaign();
        $oCampaign = $oCampaign->where('id', $request->id);

        if (!$oCampaign->exists()) {
            $data['msg'] = 'Record with provided Id not found';
            $this->_helper->response()->setCode(200)->send($data);
        }

        $oCampaign = $oCampaign->first();

        $data = [
            'status' => 'success',
            'msg' => 'Success',
            'data' => $oCampaign,
        ];

        $this->_helper->response()->setCode(200)->send($data);
    }

    public function assign()
    {
        $request = request();

        $data = [
            'status' => 'success',
            'msg' => 'Campaign Assign Successfully.',
        ];

        $this->_helper->runValidation([
            'user' => 'required|exists:users,id',
            'campaign' => 'required'
        ]);
        $user = $request->user;
        if (empty($user)) {
            $user = Auth::user()->id;
        }

        $campaigns = Campaign::find($request->campaign);
        $campaigns->assigned_id = $user;
        $campaigns->save();

        $this->_helper->response()->setCode(200)->send($data);
    }

    public function status()
    {
        $request = request();

        $data = [
            'status' => 'success',
            'msg' => 'Campaign Status Successfully Changed.',
        ];

        $this->_helper->runValidation([
            'status' => 'required',
            'campaign' => 'required'
        ]);

        //dd($request->all());

        $campaigns = Campaign::find($request->campaign);
        $campaigns->status = $request->status;
        $campaigns->save();

        $this->_helper->response()->setCode(200)->send($data);
    }

    //for data add
    public function add()
    {

        $request = request();
//        return $request->all();
        $sessionUser = Auth::user();



        if (!is_array($request->installment_amount)) {
            $installment_amount = explode(",", $request->installment_amount);
            $request->merge(['installment_amount' => $installment_amount]);
        }

        if (!is_array($request->installment_date)) {
            $installment_date = explode(",", $request->installment_date);
            $request->merge(['installment_date' => $installment_date]);
        }

        if (empty($request->installment_amount)) {
            $request->merge(['installment_amount' => []]);
        }

        if (empty($request->installment_date)) {
            $request->merge(['installment_date' => []]);
        }

        $validationArr = [
            'name' => 'required|string',
            'date' => 'required|date',
            'assigned_id' => 'required|exists:users,id',
            'compaign_amount' => 'required|numeric|min:0|not_in:0',
            'password' => 'required|confirmed|min:6',
            // 'type' => 'required|in:one_off,monthly'
        ];

        $enable_installment = (!empty($request->enable_installment)) ? $request->enable_installment : 'N';

        if (empty($request->edit_id)) {
            $validationArr['amount'] = 'required|numeric|min:0|not_in:0';
            if ($enable_installment == 'Y') {
                $validationArr['installment_amount.*'] = 'required|numeric|min:0|not_in:0';
                $validationArr['installment_date.*'] = 'required|date';
            }
        }

        if (!empty($request->campaign_specialist)) {
            $validationArr['campaign_specialist'] = 'exists:users,id';
        }

        if (!empty($request->lp_specialist)) {
            $validationArr['lp_specialist'] = 'exists:users,id';
        }

        if (!empty($request->account_manager)) {
            $validationArr['account_manager'] = 'exists:users,id';
        }

        $validationMsgs = [
            'assigned_id.required' => 'Sales Person is required',
            'assigned_id.exists' => 'Invalid Sales Person selected'
        ];

        $this->_helper->runValidation($validationArr, $validationMsgs);

        // $oLandingPage = new LandingPage();
        // $oLandingPage = $oLandingPage->where('id', $request->landing_id)->first();
        // $customerId = $oLandingPage->user_id;
        //$customerId = $request->user_id;

        if (empty($request->edit_id)) {
            $addUser = new User();
            $addUser->name = $request->user_name;
            $addUser->phone = $request->mobile;
            $addUser->job_position = $request->job_position;
            $addUser->email = $request->email;
            $addUser->password = bcrypt(12345678);
            $addUser->status = $request->status;
            $addUser->type = 'customer';
            $addUser->save();
            $customerId = $addUser->id;
        }
        if (!empty($request->edit_id)) {
            $campaignData = Campaign::find($request->edit_id);
            $addUser = User::find($campaignData->user_id);
            $addUser->name = $request->user_name;
            $addUser->phone = $request->mobile;
            $addUser->job_position = $request->job_position;
            $addUser->email = $request->email;
            $addUser->status = $request->status;
            $addUser->type = 'customer';
            $addUser->save();
            $customerId = $addUser->id;
        }









        Company::updateOrCreate(
            [
                'user_id' => $customerId
            ],
            [
                'address' => ($request->address) ? $request->address : null,
                'name' => $request->company
            ]
        );

        UserBusinessInformation::updateOrCreate(
            [
                'user_id' => $customerId
            ],
            [
                'email_address' => ($request->work_email) ? $request->work_email : null,
                'phone' => ($request->work_phone) ? $request->work_phone : null,
                'address' => ($request->address) ? $request->address : null,
                'website' => ($request->website) ? $request->website : null,
                'facebook_link' => ($request->facebook_link) ? $request->facebook_link : null,
                'twitter_link' => ($request->twitter_link) ? $request->twitter_link : null,
                'Instagram_link' => ($request->Instagram_link) ? $request->Instagram_link : null,
                'notes' => ($request->notes) ? $request->notes : null
            ]
        );

        $installmentArr = [];
        $totalInstallments = 0;
        if (empty($request->edit_id) && $enable_installment == 'Y') {
            if (count($request->installment_amount) < 2) {
                $data['status'] = 'error';
                $data['msg'] = 'Please Enter Atleast 2 installments.';
                $this->_helper->response()->setCode(200)->send($data);
            }

            if (count($request->installment_amount) > 3) {
                $data['status'] = 'error';
                $data['msg'] = 'Only 3 Installments are allowed.';
                $this->_helper->response()->setCode(200)->send($data);
            }

            if (count($request->installment_amount) != count($request->installment_date)) {
                $data['status'] = 'error';
                $data['msg'] = 'Every amount in installment must have date.';
                $this->_helper->response()->setCode(200)->send($data);
            }

            $totalCampaignAmount =  $request->amount;
            $calcAmount = 0;

            $date_change_msg = '';
            foreach ($request->installment_amount as $key => $value) {
                if ($key > 0) {
                    $lastDate = $request->installment_date[$key - 1];
                    $crntDate = $request->installment_date[$key];
                } else {
                    $lastDate = date('Y-m-d', strtotime('-1 day', time())); //date('Y-m-d');
                    $crntDate = $request->installment_date[$key];
                }
                $datediff = strtotime($crntDate) - strtotime($lastDate);
                $days = $datediff / (60 * 60 * 24);

                $keymsg1 = '';
                $keymsg2 = '';

                if (($key + 1) == 1) {
                    $keymsg1 = '1st';
                }

                if (($key + 1) == 2) {
                    $keymsg1 = '2nd';
                }

                if (($key + 1) == 3) {
                    $keymsg1 = '3rd';
                }

                if ($key <= 0) {
                    $keymsg2 = 'today';
                }

                if ($key == 1) {
                    $keymsg2 = '1st installment';
                }

                if ($key == 2) {
                    $keymsg2 = '2nd installment';
                }

                if ($key == 3) {
                    $keymsg2 = '3rd installment';
                }

                if ($days < 1) {
                    $date_change_msg = $keymsg1 . ' installment date must be greater then ' . $keymsg2 . ' date.';
                    break;
                }
                $amount = (!empty($value)) ? (float) $value : 0;
                $calcAmount = $calcAmount + $amount;
            }

            if (!empty($date_change_msg)) {
                $data['status'] = 'error';
                $data['msg'] = $date_change_msg;
                $this->_helper->response()->setCode(200)->send($data);
            }

            if ($totalCampaignAmount != $calcAmount) {
                $data['status'] = 'error';
                $data['msg'] = 'All installments amount must be equal to Contract Value.';
                $this->_helper->response()->setCode(200)->send($data);
            }

            $totalInstallments =  count($request->installment_amount);
        }

        $campaign = new Campaign();

        if (!empty($request->edit_id)) {

            $campaign = $campaign->where('id', $request->edit_id);
            if ($sessionUser->type != 'admin') {
                $campaign = $campaign->where(function ($q) {
                    $q->where('campaign_specialist', Auth::user()->id)
                        ->orWhere('assigned_id', Auth::user()->id)
                        ->orWhere('lp_specialist', Auth::user()->id)
                        ->orWhere('account_manager', Auth::user()->id);
                });
            }

            if (!$campaign->exists()) {
                $data['status'] = 'error';
                $data['msg'] = 'Record with provided Id not found';
                $this->_helper->response()->setCode(200)->send($data);
            }
            $campaign = $campaign->first();

            if (!empty($request->assigned_id)) {
                $campaign->assigned_id = $request->assigned_id;
            }
        } else {
            $campaign->status = 'pending';
            $campaign->created_by = $sessionUser->id;
            $campaign->assigned_id = $request->assigned_id;
            $campaign->amount = $request->amount;
            $campaign->with_installment = $enable_installment;
            $campaign->installment_count = $totalInstallments;
        }

        $campaign->user_id = $customerId;
        $campaign->recurring_option = $request->recurring_option;
        $campaign->recurring_way = $request->recurring_way;
        $campaign->landing_pages_id = $request->landing_id;
        $campaign->updated_by = $sessionUser->id;
        $campaign->name = $request->name;
        $campaign->start_date = Carbon::parse($request->date)->format('Y-m-d');
        $campaign->compaign_amount = $request->compaign_amount;
        //$campaign->type = $request->type;
        $campaign->type = 'one_off';


        if (!empty($request->campaign_specialist)) {
            $campaign->campaign_specialist = $request->campaign_specialist;
        }

        if (!empty($request->lp_specialist)) {
            $campaign->lp_specialist = $request->lp_specialist;
        }

        if (!empty($request->account_manager)) {
            $campaign->account_manager = $request->account_manager;
        }

        $campaign->save();
        $insertedId = $campaign->id;

        if (empty($request->edit_id)) {
            $campaignCount = Campaign::where('user_id', $customerId)->get()->count();
            $otherProjectsCount = OtherProject::where('user_id', $customerId)->get()->count();
            $user = User::where('id', $customerId)->first();
            if ($user->status == 'new' && ($campaignCount >= 2 || $otherProjectsCount >= 2)) {
                $user->update(['status' => 'recurring']);
            }

            if ($enable_installment == 'Y') {
                foreach ($request->installment_amount as $key => $value) {
                    $installmentArr[] = [
                        'user_id' => $customerId,
                        'module_id' => $insertedId,
                        'module_type' => 'campaigns',
                        'amount' => $value,
                        'installment_date' => date('Y-m-d', strtotime($request->installment_date[$key])),
                    ];
                }

                foreach ($installmentArr as $key => $value) {
                    InvoiceInstallments::create([
                        'user_id' => $value['user_id'],
                        'module_id' => $value['module_id'],
                        'module_type' => $value['module_type'],
                        'amount' => $value['amount'],
                        'installment_date' => $value['installment_date']
                    ]);
                }
            } else {
                $oInvoice = new Invoice();
                $vat = $oInvoice->vat;
                $amount = $request->amount;
                $totalAmount = $amount + (($amount / 100) * $vat);

                Invoice::create([
                    'user_id' => $customerId,
                    'type' => 'campaign',
                    'status' => 'pending',
                    'amount' => $amount,
                    'vat' => $vat,
                    'total_amount' => $totalAmount,
                    'campaign_id' => $campaign->id,
                    'landing_pages_id' => null
                ]);
            }


            Notification::create([
                'user_id' => $customerId,
                'campaign_id' => $campaign->id,
                'type' => 'sms',
                'for' => 'customer'
            ]);

            Notification::create([
                'user_id' => $customerId,
                'campaign_id' => $campaign->id,
                'type' => 'email',
                'for' => 'customer'
            ]);

            $agreement = Agreement::create([
                'user_id' => $customerId,
                'campaign_id' => $campaign->id,
            ]);

            $agreementSign = AgreementDocuSign::create([
                'user_id' => $customerId,
                'module_id' => $agreement->id,
                'type' => 'agreements',
            ]);

            $message = 'Campaign Saved Successfully.';
        } else {
            $message = 'Campaign Updated Successfully.';
        }


        $this->_helper->response()->setMessage($message)->send('');
    }

    public function destroy(Request $request)
    {
        if (Auth::user()->type != 'admin') {
            $data = [
                'status' => 'error',
                'msg' => 'You are not authorized to complete this action.',
            ];
            $this->_helper->response()->setCode(200)->send($data);
        }
        $campaign = new Campaign();
        $campaign = $campaign->find($request->campaign_id);
        $campaign->delete();
        $campaign->save();
        $data = [
            'status' => 'success',
            'msg' => 'Campaign Successfully deleted',
        ];
        $this->_helper->response()->setCode(200)->send($data);
    }

    public function destroyComment(Request $request)
    {
        $res = CampaignComments::where('id', $request->comment_id)->where('campaign_id', $request->parent_id)->delete();
        if ($res) {
            $data = [
                'status' => 'success',
                'msg' => 'Comment Successfully deleted',
            ];
        } else {
            $data = [
                'status' => 'error',
                'msg' => 'Something Went wrong',
            ];
        }
        $this->_helper->response()->setCode(200)->send($data);
    }
}

<?php

namespace App\Http\Controllers\Staff\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use PDF;
use Auth;
use Stripe;
use App\User;
use App\Invoice;
use Carbon\Carbon;
use App\Agreement;
use App\Helpers\Helper;
use Laravel\Cashier\Cashier;
use Illuminate\Support\Facades\Validator;

class InvoiceController extends Controller
{
    public function __construct() {
        $this->_helper = new Helper();
        $this->_request = request();
    }

    public function getAllInvoices() {
        $invoices = Invoice::with([
			        	'campaign' => function ($query) {
			        		$query->withTrashed();
				        }, 
			        	'landing_page', 
			        	'other_project', 
			        	'user' => function ($query) {
			        		$query->withTrashed();
			        		$query->with(['company']);
				        }
				    ])->orderBy('id', 'DESC')->paginate();

        //$user = User::find($invoices[0]->user_id);
        $this->_helper->response()->send(['data' => $invoices]);
    }

    public function getSetupIntent() {
        return $this->_request->user()->createSetupIntent();
    }

    public function chargeCard()
    {
        $invoiceId = request()->id;
        $invoice = new Invoice();
        $invoiceData = $invoice->with([
        	'user' => function ($query) {
                $query->withTrashed();
            },
            'campaign' => function ($query) {
                $query->withTrashed();
            }, 
        	'landing_page', 
        	'other_project'
        ])->find($invoiceId);


        if (empty($invoiceData)) {
            $data = [
                'status' => 'error',
                'msg' => 'Invalid Invoice Id',
            ];

            $this->_helper->response()->setCode(200)->send($data);
        }

        if (!empty($invoiceData->user->deleted_at)) {
            $data = [
                'status' => 'error',
                'msg' => 'Customer is deleted of this invoice',
            ];
            $this->_helper->response()->setCode(200)->send($data);
        }

        if ($invoiceData->deleted_parent == 'Y') {
            $data = [
                'status' => 'error',
                'msg' => $invoice->typeArr[$invoiceData->type].' is deleted of this invoice',
            ];
            $this->_helper->response()->setCode(200)->send($data);
        }

        if (strtolower($invoiceData->status) != "pending") {
            $data = [
                'status' => 'error',
                'msg' => 'Invoice Already Paid',
            ];

            $this->_helper->response()->setCode(200)->send($data);
        }

        $user = User::find($invoiceData->user_id);
        if (!empty(request()->mark) && request()->mark == 'yes') {
        	$invoice = new \App\Invoice();
            $invoice = $invoice->find(request()->id);
            $invoice->status = 'completed';
            $invoice->response = json_encode(['paid_by' => Auth::user()->id, 'paid_from' => 'staff_side', 'payment_type' => 'marked']);
            $invoice->paid_date = Carbon::now()->format('Y-m-d H:i:s');
            $invoice->save();

            if (request()->type == 'campaign') {
                $status = "pending";
                $agreements = new Agreement();
                $agreements = $agreements->where('campaign_id', $invoice->campaign_id);

                if ($agreements->exists()) {
                    $agreements = $agreements->first();
                    if (strtolower($agreements->status) == 'completed') {
                        $status = "awaiting";
                    }
                }

                $campaign = new \App\Campaign();
                $campaign = $campaign->withTrashed()->find($invoice->campaign_id);
                $campaign->status = $status;
                $campaign->save();
            } else if (request()->type == 'other_project') {
                /*$othrProjct = new \App\OtherProject();
                $othrProjct = $othrProjct->find($invoice->other_project_id);
                $othrProjct->status = 'completed';
                $othrProjct->save();*/
            } else if (request()->type == 'landing_page') {
                /*$landing = new \App\LandingPage();
                $landing = $landing->find($invoice->landing_pages_id);
                $landing->status = 'completed';
                $landing->save();*/
            }

            $data = [
                'status' => 'success',
                'msg' => 'Payment Successful.',
            ];
            $this->_helper->response()->setCode(200)->send($data);
        } else {
        	$stripeSecret = getenv("STRIPE_SECRET");
	        Stripe\Stripe::setApiKey($stripeSecret);
	        //Stripe\Stripe::setApiKey('sk_test_IASGr1XRw5losTOlqxxyPeIL00hKxC0Gd6');

	        if (empty($user->stripe_id)) {
		        $chargeRes = Stripe\Charge::create([
		            'amount' => request()->amount * 100,
		            'currency' => 'gbp',
		            'source' => request()->token['id'],
		            'description' => 'Staff Paid'
		        ]);
	        } else {
	            $customerId = $user->stripe_id;
	            $chargeRes = Stripe\Charge::create([
		            'customer' => $customerId,
		            'amount' => request()->amount * 100,
		            'currency' => 'gbp',
		            'description' => 'User Paid'
		        ]);
	        }

	        if($chargeRes->amount_refunded == 0 && empty($chargeRes->failure_code) && $chargeRes->paid == 1 && $chargeRes->captured == 1) {
	        	$chargeRes->paid_by = Auth::user()->id;
	        	$chargeRes->paid_from = 'staff_side';
	        	$chargeRes->payment_type = 'card_charge';
	        	
	            $invoice = new \App\Invoice();
	            $invoice = $invoice->find(request()->id);
	            $invoice->status = 'completed';
	            $invoice->response = json_encode($chargeRes);
	            $invoice->paid_date = Carbon::now()->format('Y-m-d H:i:s');
	            $invoice->save();

	            if (request()->type == 'campaign') {
	                $status = "pending";
	                $agreements = new Agreement();
	                $agreements = $agreements->where('campaign_id', $invoice->campaign_id);

	                if ($agreements->exists()) {
	                    $agreements = $agreements->first();
	                    if (strtolower($agreements->status) == 'completed') {
	                        $status = "awaiting";
	                    }
	                }

	                $campaign = new \App\Campaign();
	                $campaign = $campaign->withTrashed()->find($invoice->campaign_id);
	                $campaign->status = $status;
	                $campaign->save();
	            } else if (request()->type == 'other_project') {
	                /*$othrProjct = new \App\OtherProject();
	                $othrProjct = $othrProjct->find($invoice->other_project_id);
	                $othrProjct->status = 'completed';
	                $othrProjct->save();*/
	            } else if (request()->type == 'landing_page') {
	                /*$landing = new \App\LandingPage();
	                $landing = $landing->find($invoice->landing_pages_id);
	                $landing->status = 'completed';
	                $landing->save();*/
	            }

	            $data = [
	                'status' => 'success',
	                'msg' => 'Payment Successful.',
	            ];
	        } else {
	            $data = [
	                'status' => 'error',
	                'msg' => 'Something Went Wrong',
	            ];
	        }

	        $this->_helper->response()->setCode(200)->send($data);
        }
    }
}

<?php

namespace App\Http\Controllers\Staff\Api;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Invoice;
use App\LandingPage;
use App\LandingPageComments;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Auth;
use Illuminate\Http\Request;

class LandingController extends Controller
{
    protected $_helper;

    public function __construct()
    {
        $this->_helper = new Helper();
    }

    public function index() {
        $request = request();
        $pages = new \App\LandingPage();
        $pages = $pages->getAll($request);
        $this->_helper->response()->setCode(200)->send(['data' => $pages]);
    }

    public function allStaffMembers() {
        $members = \App\User::where('type', 'staff')->get();
        $this->_helper->response()->send(['data' => $members]);
    }

    public function allCustomers() {
        $customers = \App\User::where('type', 'customer')->get();
        $this->_helper->response()->send(['data' => $customers]);
    }

    public function create() {
        $request = request();

        //dd($request->all());

        $messages = [
            'amount.min' => 'Please enter positive integer',
            'created_by.required' => 'You must select any cutomer'
        ];


        $this->_helper->runValidation([
            'name' => 'required|string',
            //'cost' => 'required|numeric|min:0|not_in:0',
            //'budget' => 'required|numeric|min:0|not_in:0',
            'description' => 'required',
            'url' => 'required|url',
            'status' => 'required',
            'date' => 'required',
            'created_by' => 'required'
        ], $messages);

        $objLanding = new \App\LandingPage();
        $objLanding->user_id = $request->created_by;
        $objLanding->name = $request->name;
        $objLanding->cost = 0;//$request->cost;
        $objLanding->description = $request->description;
        $objLanding->url = $request->url;
        $objLanding->status = $request->status;
        $objLanding->date = Carbon::parse($request->date)->format('m/d/Y');
        $objLanding->budget = 0;//$request->budget;
        $objLanding->assigned_to = $request->staff_member;
        $objLanding->created_by = Auth::user()->id;
        $objLanding->save();

        /*Invoice::create([
            'user_id' => $request->created_by,
            'type' => 'landing_page',
            'status' => 'pending',
            'campaign_id' => null,
            'landing_pages_id' => $objLanding->id
        ]);*/

        $data = [
            'status' => 'error',
            'msg' => 'Landing Page Successfully Saved',
        ];

        $this->_helper->response()->setCode(200)->send($data);
    }

    public function update() {
        $request = request();

        $this->_helper->runValidation([
            'name' => 'required|string',
            //'cost' => 'required|numeric|min:0|not_in:0',
            //'budget' => 'required|numeric|min:0|not_in:0',
            'customer_id' => 'required|numeric|exists:users,id',
            'staff_id' => 'required|numeric|exists:users,id',
            'description' => 'required',
            'url' => 'required|url',
            'status' => 'required',
            'date' => 'required',
        ]);

        $message = 'Landing Page Successfully Saved';
        
        $landingPage = new LandingPage();
        if (!empty($request->id)) {
            $landingPage = $landingPage->where('id', $request->id);

            if (!$landingPage->exists()) {
                $data['status'] = 'error';
                $data['msg'] = 'Record with provided Id not found';
                $this->_helper->response()->setCode(200)->send($data);
            }
            $landingPage = $landingPage->first();
            $message = 'Landing Page Successfully Updated';
        }

        $landingPage->name =  $request->name;
        $landingPage->status = $request->status;
        $landingPage->cost = 0;//$request->cost;
        $landingPage->budget = 0;//$request->budget;
        $landingPage->description = $request->description;
        $landingPage->url = $request->url;
        $landingPage->date = Carbon::parse($request->date)->format('m/d/Y');
        $landingPage->user_id =  ($request->customer_id);
        $landingPage->assigned_to = $request->staff_id;
        $landingPage->save();
        $insertedId = $landingPage->id;


       $data = [
            'status' => 'success',
            'msg' => $message,
        ];

        $this->_helper->response()->setCode(200)->send($data);
    }

    public function postComment() {

        $this->_helper->runValidation([
            'id' => 'required|exists:landing_pages,id',
            'text' => 'required'
        ]);
        $request = request();

        LandingPageComments::create([
            'landing_page_id' => $request->id,
            'user_id' => Auth::user()->id,
            'description' => $request->text
        ]);

        $landing_page = LandingPage::with('comments')->where('id', $request->id)->first();
       // dd($landing_page);
        $this->_helper->response()->send(['data' => $landing_page]);
    }
    public function show($id)
    {
        $withArray = [];
        $withArray['user'] = function ($query) {
            $query->withTrashed();
        };

        if (request()->has('tab') && isset(request()->tab)) {
            if (request()->tab == 'notes') {
                $withArray['comments'] = function ($query) {
                    $query->with([
                        'user' => function ($query1) {
                            $query1->withTrashed();
                        }
                    ]);
                };
            } elseif (request()->tab == 'assign_detail') {
                $withArray[] = 'assigned';
            }
        } else {
            $withArray[] = 'comments';
            $withArray[] = 'assigned';
        }
        $landing_page = LandingPage::with($withArray)->where('id', $id)->first();
        $this->_helper->response()->send(['data' => $landing_page]);

    }

    public function destroyComment(Request $request) {
        $res = LandingPageComments::where('id', $request->comment_id)->where('landing_page_id', $request->parent_id)->delete();
        if ($res) {
            $data = [
                'status' => 'success',
                'msg' => 'Comment Successfully deleted',
            ];
        } else {
            $data = [
                'status' => 'error',
                'msg' => 'Something Went wrong',
            ];
        }
       $this->_helper->response()->setCode(200)->send($data);
    }
}

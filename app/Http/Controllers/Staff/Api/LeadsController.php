<?php

namespace App\Http\Controllers\Staff\Api;

use DB;
use App\Campaign;
use App\Http\Controllers\Controller;
use App\Notification;
use Illuminate\Http\Request;
use App\Http\Resources\Leads as LeadsResource;
use App\Http\Resources\LeadComments as LeadCommentsResource;
use App\Leads;
use App\LeadComments;
use App\LeadQuestion;
use Auth;
use App\Helpers\Helper;
use Carbon\Carbon;

class LeadsController extends Controller
{
    private $_helper;

    public function __construct(){
        $this->_helper = new Helper();
    }

    public function status(){
        $data = [
            'status' => 'success',
            'msg' => 'Status Changed Successfully!',
            'data' => [],
        ];

        $request = request();
        $leads = new Leads();
        $leads = $leads->where('id', $request->lead)->where('campaign_id', $request->campaign)->first();
        $leads->status = $request->status;
        $leads->save();
        
        $data['data']['leads'] =  new LeadsResource($leads);

        $this->_helper->response()->setCode(200)->send($data);

    }

    public function getLeads(Request $request)
    {
        $this->_helper->runValidation([
            'campaign' => 'required',
        ],[
            'campaign.required' => 'campaign required',
        ]);

        $data = [
            'status' => 'success',
            'msg' => 'Success.',
            'data' => [],
        ];

        $leads = new Leads();

        if(!empty($request->lead)){
            $leads =  $leads->with(['lead_questions']);
            //$leads =  $leads->where('user_id', Auth::user()->id);
            $leads =  $leads->where('id',  $request->lead);
            $leads =  $leads->where('campaign_id', $request->campaign);
            $leads =  $leads->orderBy('id', 'DESC');
        }else{
            $leads =  $leads->with(['lead_questions']);
            //$leads =  $leads->with(['lead_comments']);
            //$leads =  $leads->where('user_id', Auth::user()->id);
            $leads =  $leads->where('campaign_id', $request->campaign);
            $leads =  $leads->orderBy('id', 'DESC');
        }

        $this->_helper->checkRecordExist($leads);

        $leads = $leads->first();

        $data['data']['leads'] =  new LeadsResource($leads);

        $this->_helper->response()->setCode(200)->send($data);
    }

    public function leadSearch(Request $request)
    {
        $this->_helper->runValidation([
            'campaign' => 'required',
            'last_lead_id' => 'numeric',
        ],[
            'campaign.required' => 'campaign required',
        ]);

        $data = [
            'status' => 'success',
            'msg' => 'Success.',
            'data' => [],
        ];

        $leads = new Leads();
        $leads = $leads->where('campaign_id',$request->campaign);
        if(!empty($request->search)){
            $leads = $leads->whereHas('lead_questions', function ($query) use ($request) {
                $query->where('answers', 'like', '%' . $request->search . '%');
            });
        }

        if (!empty($request->last_lead_id)) {
            $leads = $leads->where('id', '<', $request->last_lead_id);
        }

        $leads = $leads->limit(10);
        $leads = $leads->orderBy('id', 'DESC');

        $this->_helper->checkRecordExist($leads);
        $leads = $leads->get();


        if(!empty($leads)){
            $leads = LeadsResource::collection($leads);
        } else {
            $leads = [];
        }

        $data['data']['leads'] = $leads;

        $this->_helper->response()->setCode(200)->send($data);
    }

    public function saveLeads(Request $request)
    {
        $this->_helper->runValidation([
            'campaign_id' => 'required|exists:campaigns,id',
            'heading.*' => 'required',
            'answer.*' => 'required'
        ]);

        /*$oCampaign = new Campaign();
        $oCampaign = $oCampaign->where('id', $request->campaign_id)->first();
        $customerId = $oCampaign->user_id;*/
        
        if (count($request->heading) != count($request->answer)) {
            $data = [
                'status' => 'error',
                'mob_status' => 'success',
                'msg' => 'Total Questions must be equal to total answers.',
                'data' => []
            ];
            $this->_helper->response()->setCode(200)->send($data);
        }

        $leads = new Leads();
        $leads->campaign_id = $request->campaign_id;
        $leads->user_id = Auth::user()->id;
        $leads->submitted_on =  Carbon::now();
        $isSaved = $leads->save();
        $insertedId = $leads->id;

        foreach ($request->heading as $key => $value) {
            LeadQuestion::create([
                'user_id' => Auth::user()->id,
                'lead_id' => $insertedId,
                'headings' => $request->heading[$key],
                'answers' => $request->answer[$key]
            ]);
        }

        $data = [
            'status' => 'success',
            'msg' => 'Lead Saved Successfully.',
            'data' => [
                'getLeads' => new LeadsResource($leads),
            ],
        ];

        $this->_helper->response()->setCode(200)->send($data);
    }

    public function saveComments(Request $request)
    {
        $this->_helper->runValidation([
            'message' => 'required',
            'lead_id' => 'required',
        ],[
            'message.required' => 'Message field required',
            'lead_id.required' => 'Lead field required',
        ]);

        $leads = new LeadComments();
        $leads->user_id = Auth::user()->id;
        $leads->lead_id = $request->lead_id;
        $leads->message = $request->message;
        $isSaved = $leads->save();

        $data = [
            'status' => 'error',
            'mob_status' => 'success',
            'msg' => 'There might be some errors, Try again later.',
        ];

        if($isSaved){
            $data = [
                'status' => 'success',
                'msg' => 'Comment Saved Successfully.',
            ];
        }

        $this->_helper->response()->setCode(200)->send($data);
    }

    public function getComments(Request $request)
    {
        $this->_helper->runValidation([
            'id' => 'required',
        ],[
            'id.required' => 'Lead required',
        ]);

        $data = [
            'status' => 'success',
            'msg' => 'Success.',
        ];

        $comments = new LeadComments();

        $comments =  $comments->with(['user']);
        //$comments =  $comments->where('user_id', Auth::user()->id);
        $comments =  $comments->where('lead_id', $request->id);

        $this->_helper->checkRecordExist($comments);

        $comments = $comments->get();

        $data['data']['comments'] = LeadCommentsResource::collection($comments);

        $this->_helper->response()->setCode(200)->send($data);
    }

    public function destroy(Request $request) {
        if (Auth::user()->type != 'admin') {
            $data = [
                'status' => 'error',
                'mob_status' => 'success',
                'msg' => 'You are not authorized to complete this action.',
            ];
            $this->_helper->response()->setCode(200)->send($data);
        }
        $leads = new Leads();
        $leads = $leads->find($request->lead);
        $leads->delete();
        $leads->save();
        $data = [
            'status' => 'success',
            'msg' => 'Lead Successfully deleted',
        ];
       $this->_helper->response()->setCode(200)->send($data);
    }
}

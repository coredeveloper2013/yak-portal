<?php

namespace App\Http\Controllers\Staff\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\Campaigns as CampaignsResource;
use App\Http\Resources\Users as UserResource;
use App\Helpers\Helper;
use Auth;
use App\Campaign;
use App\Guide;
use App\Invoice;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Gate;


class GuideController extends Controller
{
    private $_helper;

    public function __construct(){
        $this->_helper = new Helper();
    }

    public function get(Request $request)
    {
        $data = [
            'status' => 'success',
            'msg' => 'Success.',
            'data' => [],
        ];
       if($request->id) 
      {  $guides = Guide::find($request->id);
      $data['data']['guides'] =  ($guides);}
        else
        {$guides = Guide::all();
            foreach ($guides as $key => $value) {
            $value['descriptionF']= strip_tags($value['description'],'<br>');
            $value['image']= url($value['image']);
                $value['msg'] =\Carbon\Carbon::createFromTimeStamp(strtotime($value['created_at']))->diffForHumans() ;
            }
         $data['data']['guides'] =   ($guides);}
         
         $data['data']['isAdmin']=Gate::allows('isAdmin');
        $this->_helper->response()->setCode(200)->send($data);
    }
 public function delete(Request $request)
    {
       $news = \App\Guide::where('id', $request->id)->delete();
        $this->_helper->response()->setCode(200)->send($news);
    }
     

    public function add()
    {
        $request = request();

        $this->_helper->runValidation([
            'title' => 'required|string',
            'description' => 'required|string',
            'status' => 'required',
            'image' => 'required|mimes:jpeg,png'
        ]  );
        $fileName='/images/b2.jpg';
  
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                if ($file->isValid()) {
                   
            $file_name = (date('mdis')) . '_' . $file->getClientOriginalName();
            $p = $file->move('./img/', $file_name);
            if($p){

$fileName='/img/'.$file_name;
            }
            }
            }
          
   
            

          




        $User = Guide::create([
            'title' => $request->title,
            'description' => $request->description,
            'url' =>  '',
            'answer' => $request->answer,
            'status' => $request->status,
            'image' => $fileName,
        ]);

        $this->_helper->response()->setMessage('Guide Saved Successfully.')->send('');
    }
      public function update()
    {
        $request = request();

        $this->_helper->runValidation([
            'id' => 'required' ,
            'description' => 'required|string',
            'title' => 'required|string',
            // 'url' => 'required',
            'status' => 'required',
            // 'file' => 'required|mimes:jpeg,png'
        ]  );
         
  
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                if ($file->isValid()) {
                   
            $file_name = (date('mdis')) . '_' . $file->getClientOriginalName();
            $p = $file->move('./img/', $file_name);
            if($p){
            $fileName='/img/'.$file_name;
            $obj['image'] = $fileName;
            }
            }
            }
         if ($request->description !== null)
            $obj['description'] = $request->description;
         if ($request->title !== null)
            $obj['title'] = $request->title;
         if ($request->url !== null)
            $obj['url'] = $request->url;
         if ($request->status !== null)
            $obj['status'] = $request->status; 
         
        $result = \App\Guide::where('id', $request->id)->update($obj);

        $this->_helper->response()->setMessage('Guide Updated Successfully.')->send('');
    }
}

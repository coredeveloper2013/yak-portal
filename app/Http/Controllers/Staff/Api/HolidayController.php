<?php

namespace App\Http\Controllers\Staff\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\Campaigns as CampaignsResource;
use App\Http\Resources\Users as UserResource;
use App\Helpers\Helper;
use Auth;
use App\Campaign;
use App\Holiday;
use App\User;
use App\Invoice;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Gate;

class HolidayController extends Controller {

    private $_helper;

    public function __construct() {
        $this->_helper = new Helper();
    }

    public function get(Request $request) {
        $data = [
            'status' => 'success',
            'msg' => 'Success.',
            'data' => [],
        ];
        if ($request->userId) {
            $holidays = Holiday::where('userId', $request->userId)->whereBetween('date_of_holiday', [
                        Carbon::now()->startOfYear(),
                        Carbon::now()->endOfYear(),
                    ])->orderBy('date_of_holiday', 'ASC')->get();
            $noOfleave = (User::find($request->userId)->no_of_leave);
            ;
            $data['data']['noOfLeave'] = $noOfleave;

            $data['data']['dayLeft'] = $noOfleave - $holidays->count() . ' Days Left';

            foreach ($holidays as $key => $value) {
                if (date('Y-m-d') < date('Y-m-d', strtotime($value['date_of_holiday']))) {
                    $to = \Carbon\Carbon::createFromFormat('Y-m-d', $value['date_of_holiday']);
                    $from = \Carbon\Carbon::createFromFormat('Y-m-d', date('Y-m-d'));
                    $diff_in_minutes = $from->diffInDays($to);
                    // $data['data']['dayLeft'] = $diff_in_minutes.' Days Left';
                }
                $value['date_of_holiday'] = date('d F Y', strtotime($value['date_of_holiday']));
            }
            $data['data']['holidays'] = ($holidays);
        } else if ($request->id) {
            $holidays = Holiday::find($request->id);
            $data['data']['holidays'] = ($holidays);
        }
        $data['userName'] = \App\User::select('name')->where('id', $request->userId)->first()->name;
        $data['data']['isAdmin'] = Gate::allows('isAdmin');
        $this->_helper->response()->setCode(200)->send($data);
    }

    public function getByUser(Request $request) {
        $data = [
            'status' => 'success',
            'msg' => 'Success.',
            'data' => [],
        ];

        $holidays = Holiday::where('userId', $request->userId)->get();
        $data['data']['holidays'] = ($holidays);

        $data['data']['isAdmin'] = Gate::allows('isAdmin');
        $this->_helper->response()->setCode(200)->send($data);
    }

    public function delete(Request $request) {
        $holidays = Holiday::where('id', $request->id)
                        ->whereDate('date_of_holiday', '<', Carbon::now())->get();
        if (empty($holidays[0]) || Auth::user()->type=="admin") {
            $news = \App\Holiday::where('id', $request->id)->delete();
            $this->_helper->response()->setCode(200)->send($news);
        } else {
            $response = [
                'status' => 'error',
                'msg' => 'Agreements not delete,please contact to Admin',
            ];
            $this->_helper->response()->send($response);
        }
    }

    public function add() {
        $request = request();

        $holidays = Holiday::where('userId', $request->user)->whereBetween('date_of_holiday', [
                    Carbon::now()->startOfYear(),
                    Carbon::now()->endOfYear(),
                ])->orderBy('date_of_holiday', 'ASC')->get();
        $noOfleave = (User::find($request->user)->no_of_leave);

        $remaining = $noOfleave - $holidays->count();
        if ($remaining < 1) {
            $response = [
                'status' => 'error',
                'msg' => 'Remaining Agreements less then one',
            ];
            $this->_helper->response()->send($response);
        }
        $sameDateUser = Holiday::where('userId', $request->user)->where('date_of_holiday', date('Y-m-d', strtotime($request->date)))->first();
        if (!empty($sameDateUser->userId)) {
            $response = [
                'status' => 'error',
                'msg' => 'Agreements already been taken on same date',
            ];
            $this->_helper->response()->send($response);
        }
        $this->_helper->runValidation([
            // 'days' => 'required|string',
            'date' => 'required|string',
            'reason' => 'required|max:15',
            'user' => 'required'
        ]);

        $User = Holiday::create([
                    // 'number_of_days' => intval($request->days),
                    'date_of_holiday' => date('Y-m-d', strtotime($request->date)),
                    'reason' => $request->reason,
                    'description' => $request->description? : '',
                    'availed' => 0,
                    'userId' => intval($request->user),
        ]);

        $this->_helper->response()->setMessage('Agreements Saved Successfully.')->send('');
    }

    public function update() {
        $request = request();

        $sameDateUser = Holiday::where('userId', $request->user)->where('date_of_holiday', date('Y-m-d', strtotime($request->date)))->first();
        if (!empty($sameDateUser->userId)) {
            $response = [
                'status' => 'error',
                'msg' => 'Agreements already been taken on same date',
            ];
            $this->_helper->response()->send($response);
        }
        $this->_helper->runValidation([
            // 'days' => 'required|string',
            'date' => 'required|string',
            'reason' => 'required',
            'user' => 'required'
        ]);


        if ($request->days !== null)
            $obj['number_of_days'] = intval($request->days);
        if ($request->date !== null)
            $obj['date_of_holiday'] = intval($request->date);
        if ($request->reason !== null)
            $obj['reason'] = $request->reason;
        if ($request->user !== null)
            $obj['userId'] = $request->user;
        if ($request->description !== null)
            $obj['description'] = $request->user;


        $result = \App\Holiday::where('id', $request->id)->update($obj);

        $this->_helper->response()->setMessage('Agreements Updated Successfully.')->send('');
    }

    public function leave() {
        $request = request();
        $this->_helper->runValidation([
            'id' => 'required',
            'no_of_leave' => 'required'
        ]);
        if ($request->no_of_leave !== null)
            $obj['no_of_leave'] = intval($request->no_of_leave);
        $result = \App\User::where('id', $request->id)->update($obj);

        $this->_helper->response()->setMessage('User Updated Successfully.')->send('');
    }

}

<?php

namespace App\Http\Controllers\Staff\Api;

use Auth;
use App\User;
use App\Campaign;
use App\LeadDeal;
use Carbon\Carbon;
use App\Helpers\Helper;
use App\LeadDealQuestion;
use App\LeadDealsComments;
use App\BackupLeads;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Resources\Users as UserResource;
use App\Http\Resources\Campaigns as CampaignsResource;
use Illuminate\Support\Facades\DB;

class DealsController extends Controller {

    private $_helper;

    public function __construct() {
        $this->_helper = new Helper();
    }

    public function delete(Request $request) {
        $news = \App\LeadDeal::where('id', $request->id)->delete();
        $this->_helper->response()->setMessage('Lead has been deleted successfully.')->send($news);
    }

    public function add() {
        $request = request();
        $sessionUser = Auth::user();
        $type = $sessionUser->type;

        $validArr = [
            'lead_name' => 'required',
            'question.*' => 'required',
            'answer.*' => 'required'
        ];

        if ($type != 'admin') {
            $validArr['user_id'] = 'required|exists:users,id';
        } else {
            if (!empty($request->user_id)) {
                $validArr['user_id'] = 'exists:users,id';
            }
        }
        $this->_helper->runValidation($validArr);

        if (count($request->question) != count($request->answer)) {
            $data = [
                'status' => 'error',
                'msg' => 'Total Questions must be equal to total answers.',
                'data' => []
            ];
            $this->_helper->response()->setCode(200)->send($data);
        }

        $leads = new LeadDeal();
        if (!empty($request->user_id)) {
            $leads->user_id = $request->user_id;
        }
        $leads->name = $request->lead_name;
        $isSaved = $leads->save();
        $insertedId = $leads->id;

        foreach ($request->question as $key => $value) {
            LeadDealQuestion::create([
                'lead_deal_id' => $insertedId,
                'question' => $request->question[$key],
                'answer' => $request->answer[$key]
            ]);
        }
        $this->_helper->response()->setMessage('Leads Saved Successfully.')->send('');
    }

    public function leads() {
        $request = request();
        $user_data = Auth::user();
        $user_id = $user_data->id;

        if ($user_data->type == 'admin') {
            if ($request->user_id < 0) {
                $linked = (!empty($request->linked)) ? $request->linked : 'all';
                $leads_data = LeadDeal::get_deals_data_admin($request->search, $linked);
            } else if ($request->user_id == 0) {
                $linked = (!empty($request->linked)) ? $request->linked : 'no';
                $leads_data = LeadDeal::get_deals_data_admin($request->search, $linked);
            }  else {
                $linked = (!empty($request->linked)) ? $request->linked : 'yes';
                $leads_data = LeadDeal::get_deals_data($request->user_id, $request->search, $linked);
            }
        } else {
            $linked = (!empty($request->linked)) ? $request->linked : 'yes';
            $leads_data = LeadDeal::get_deals_data($user_id, $request->search, $linked);
        }

        $users = User::where('type', 'staff')->get();

        $oLeadDeal = new LeadDeal();
        $list = $oLeadDeal->statusEnums;

        foreach ($leads_data as $key => $value) {

            $value->created = date('F d,Y', strtotime($value->created_at));
            $value->statusName = $list[$value->status];
            if (!empty($value->user_id)) {
                $value->userName = (User::find($value->user_id))->name;
            } else {
                $value->userName = '';
            }
        }

        $data['data'] = ['leads' => $leads_data, 'users' => $users];
        $this->_helper->response()->setCode(200)->send($data);

        // return view('staff.newbusinesshub.leads', ['leads_data' => $leads_data,'users'=>$users]);
    }

    public function update() {
        $request = request();

        $this->_helper->runValidation([
            'id' => 'required',
            'name' => 'required|string',
            'budget' => 'required|string',
            'lead_name' => 'required',
            'email' => 'required|email',
            'point_of_contact' => 'required',
            'phone' => 'required',
            'source' => 'required'
        ]);

        $obj['name'] = $request->lead_name;
        if ($request->budget !== null)
            $obj['budget'] = $request->budget;
        if ($request->source !== null)
            $obj['source'] = $request->source;
        if ($request->user_id !== null)
            $obj['user_id'] = $request->user_id;
        if ($request->email !== null)
            $obj['email'] = $request->email;
        if ($request->point_of_contact !== null)
            $obj['point_of_contact'] = $request->point_of_contact;
        if ($request->phone !== null)
            $obj['phone'] = $request->phone;

        $result = \App\LeadDeal::where('id', $request->id)->update($obj);

        $this->_helper->response()->setMessage('Leads Updated Successfully.')->send('');
    }

    public function status() {
        $request = request();

        $this->_helper->runValidation([
            'id' => 'required',
            'status' => 'required',
        ]);

        if ($request->status !== null)
            $obj['status'] = $request->status;
        $obj['active'] = 1;


        $result = \App\LeadDeal::where('id', $request->id)->update($obj);

        $this->_helper->response()->setMessage('Updated Successfully.')->send('');
    }

    public function addComments() {
        $request = request();

        $this->_helper->runValidation([
            'message' => 'required|string',
            'lead_id' => 'required|string',
        ]);
        $User = LeadDealsComments::create([
                    'message' => $request->message,
                    'lead_id' => $request->lead_id,
                    'user_id' => Auth::id()
        ]);
        $this->_helper->response()->setMessage('Comments Saved Successfully.')->send('');
    }

    public function comments() {
        $request = request();

        $this->_helper->runValidation([
            'lead_id' => 'required|string',
        ]);

        $result = \App\LeadDeal::where('id', $request->lead_id)->withTrashed();
        if (!$result->exists()) {
            $data['status'] = 'error';
            $data['msg'] = 'Record with provided Id not found';
            $data['data'] = ['comments' => [], 'lead_data' => []];
            $this->_helper->response()->setCode(200)->send($data);
        }

        $result = $result->first();

        $LeadDealsComments = LeadDealsComments::get_comments($request->lead_id);
        $data['data'] = ['comments' => $LeadDealsComments, 'lead_data' => $result];
        $this->_helper->response()->setCode(200)->send($data);
    }

    public function active() {
        $request = request();

        $this->_helper->runValidation([
            'lead_id' => 'required',
        ]);

        $result = \App\LeadDeal::where('id', $request->lead_id);
        if (!$result->exists()) {
            $data['status'] = 'error';
            $data['msg'] = 'Record with provided Id not found';
            $this->_helper->response()->setCode(200)->send($data);
        }

        $result = $result->first();

        if (empty($result->user_id)) {
            $data['status'] = 'error';
            $data['msg'] = 'Please assign this deal to some staff member first';
            $this->_helper->response()->setCode(200)->send($data);
        }

        $obj['active'] = 1;
        $result = \App\LeadDeal::where('id', $request->lead_id)->update($obj);

        $this->_helper->response()->setMessage('Updated Successfully.')->send('');
    }

    public function updateLead(Request $request) {

        $sessionUser = Auth::user();
        $type = $sessionUser->type;
        $validArr = [
            'lead_id' => 'required|exists:lead_deals,id',
            'lead_name' => 'required',
            'question.*' => 'required',
            'answer.*' => 'required'
        ];

        if (!empty($request->user_id)) {
            $validArr['user_id'] = 'exists:users,id';
        }

        $this->_helper->runValidation($validArr);

        if (count($request->question) != count($request->answer)) {
            $data = [
                'status' => 'error',
                'msg' => 'Total Questions must be equal to total answers.',
                'data' => []
            ];
            $this->_helper->response()->setCode(200)->send($data);
        }

        try {
            DB::beginTransaction();
            //update main table
            $leadId = LeadDeal::find($request->lead_id);
            if (!empty($request->user_id)) {
                if($request->user_id != $leadId->user_id && !empty($leadId->user_id)){
                    $insArr = [
                        'leads_id'    => $leadId->id,
                        'user_id'     => $leadId->user_id,
                        'change_by'   => $sessionUser->id,
                        'unbounce_id'   => $leadId->unbounce_id,
                        // 'campaign_id'     => $request->question[$key],
                        'is_newBusinessHub' => ($leadId->unbounce_id ? 'Y' : 'N'  )
                    ];
                    BackupLeads::create($insArr);
                }

                $leadId->user_id = $request->user_id;
            }
            $leadId->name = $request->lead_name;
            $leadId->save();
            //remove old lead deal questions first
            LeadDealQuestion::where('lead_deal_id', $request->lead_id)->delete();
            //add new question answers
            foreach ($request->question as $key => $value) {
                LeadDealQuestion::create([
                    'lead_deal_id' => $request->lead_id,
                    'question' => $request->question[$key],
                    'answer' => $request->answer[$key]
                ]);
            }
            DB::commit();
            $this->_helper->response()->setMessage('Leads Update Successfully.')->send('');
        } catch (\Throwable $th) {
            DB::rollback();
            $data = [
                'status' => 'error',
                'msg' => $th->getMessage(),
                'data' => []
            ];
            $this->_helper->response()->setCode(200)->send($data);
        }
    }

    public function assign(){
        $request = request();

        $data = [
            'status' => 'success',
            'msg' => 'Campaign Assign Successfully.',
        ];

        $this->_helper->runValidation([
            'user_id' => 'required|exists:users,id',
            'lead_id' => 'required|exists:lead_deals,id'
        ]);

        $result = \App\LeadDeal::where('id', $request->lead_id);
        if (!$result->exists()) {
            $data['status'] = 'error';
            $data['msg'] = 'Record with provided Id not found';
            $this->_helper->response()->setCode(200)->send($data);
        }

        $obj['user_id'] = $request->user_id;
        $result = $result->update($obj);

        $this->_helper->response()->setMessage('Updated Successfully.')->send('');
    }

}

<?php

namespace App\Http\Controllers\Staff\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\Campaigns as CampaignsResource;
use App\Http\Resources\Users as UserResource;
use App\Helpers\Helper;
use Auth;
use App\Campaign;
use App\User;
use App\Invoice;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Gate;


class StaffAccountController extends Controller
{
    private $_helper;

    public function __construct(){
        $this->_helper = new Helper();
    }

    public function get(Request $request)
    {
        $data = [
            'status' => 'success',
            'msg' => 'Success.',
            'data' => [],
        ];
        if($request->id) {
            $user = User::find($request->id);
            $data['data']['users'] =  new UserResource($user);
        } else {
            $limit = (!empty($request->limit) ) ? (int)$request->limit : 15 ;
            $page = (isset($request->page) ) ? (int)$request->page : 1 ;
            if ($page < 1) {
                $page = 1;
            }
            $pageLimit = $page - 1;

            // $user = User::all();
            if(!empty($request->type) && $request->type=="all")
                $user = User::where('type','!=','customer');

            if(!empty($request->type) && $request->type=="admin")
                $user = User::where('type','admin');

            if(!empty($request->type) && $request->type=="staff")
                $user = User::where('type','staff');

            if (isset($request->page)) {
                $total = $user->count();
                $totalPages = $total / $limit;
                $Add1 = $total % $limit;
                $totalPages = ($Add1 > 0) ? (int)$totalPages + 1 : (int)$totalPages ;

                if ($page > $totalPages) {
                    $page = $totalPages;
                    $pageLimit = $totalPages-1;
                }

                $userArr = $user->offset($pageLimit*$limit)->limit($limit)->get();
                $data['data']['meta'] = [
                    'total' => $total,
                    'count' => count($userArr),
                    'per_page' => $limit,
                    'current_page' => $page,
                    'total_pages' => $totalPages,
                ];
                $data['data']['users'] =  UserResource::collection($userArr);
            } else {
                $user = $user->get();
                $data['data']['users'] =  UserResource::collection($user);
            }
        }

        $data['data']['isAdmin'] = Gate::allows('isAdmin');
        $this->_helper->response()->setCode(200)->send($data);
     }

     public function get_rights(Request $request) {
        $user_id = (!empty($request->id)) ? $request->id : Auth::user()->id ;
        $user = new User();
        $user = $user->where('id', $user_id);
        if (!$user->exists()) {
            $data['msg'] = 'User Not Exists with this Id or User is Deleted.';
            $this->_helper->response()->setCode(200)->send($data);
        }
        $user = $user->first();
        $data['msg'] = 'Success';
        $data['data']['rights'] = (!empty($user->rights)) ? explode(',', $user->rights) : [] ;
        $this->_helper->response()->setCode(200)->send($data);
    }

    public function delete(Request $request) {
        //$news = \App\User::where('id', $request->id)->delete();
        $user = new \App\User();
        $user = $user->find($request->id);
        $user->delete();
        $user->save();
       $this->_helper->response()->setCode(200)->send(true);
    }


    public function add() {
        $request = request();

        $this->_helper->runValidation([
            'name' => 'required|string',
            // 'username' => 'required',
            'email' => 'required|email|unique:users',
            'type' => 'required|in:admin,staff',
            'password' => 'required|min:6',
            //'no_of_leave' => 'min:1'

        ]  );
        $User = User::create([
            'name' => $request->name,
            'no_of_leave' => $request->no_of_leave?:15,
            'email' => $request->email,
            // 'username' => $request->username,
            'password' => Hash::make($request->password),
            'type' => $request->type,

        ]);




        $this->_helper->response()->setMessage('User Saved Successfully.')->send('');
    }

    public function update() {
        $request = request();

        $this->_helper->runValidation([
            'id' => 'required',
            'name' => 'required',
            // 'username' => 'required',
            'email' => 'required|email|unique:users,email,'.$request->id,
            'type' => 'required|in:admin,staff',
            'password' => 'required'
        ]  );

        if ($request->name !== null)
            $obj['name'] = $request->name;
        if ($request->email !== null)
            $obj['email'] = $request->email;
        if ($request->password !== null)
            $obj['password'] = Hash::make($request->password);;
        if ($request->username !== null)
            $obj['username'] = $request->username;
          if ($request->no_of_leave !== null)
            $obj['no_of_leave'] = $request->no_of_leave;

        $obj['type'] = $request->type;
        $result = \App\User::where('id', $request->id)->update($obj);




        $this->_helper->response()->setMessage('User Updated Successfully.')->send('');
    }

    public function update_rights($value='') {
        $request = request();
        $rights = $request->rights ? implode(',', $request->rights) : null;
        $user = \App\User::where('id' , $request->id)->first();
        if($user){
            $user->rights = $rights ;
            $user->save();
            $this->_helper->response()->setMessage('User Rights Saved Successfully.')->send('');
        }
        else
        {

        }
    }
}

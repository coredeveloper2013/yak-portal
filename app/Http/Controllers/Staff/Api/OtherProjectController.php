<?php

namespace App\Http\Controllers\Staff\Api;

use Auth;
use App\Invoice;
use App\Campaign;
use Carbon\Carbon;
use App\User;
use App\OtherProject;
use App\AssignedModule;
use App\ProjectComment;
use App\Helpers\Helper;
use App\InvoiceInstallments;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class OtherProjectController extends Controller
{

    public function __construct() {
        $this->_helper = new Helper();
    }

    public function listing() {
        $request = request();

        $projects = new OtherProject();

        if($request->has('filter')){
            if ($request->filter['status'] !== null) {
                $projects = $projects->where('status', $request->filter['status']);
            }
        } else if (!empty($request->status_mob)) {
            $projects = $projects->where('status', $request->status_mob);
        }
        
        $projects = $projects->where('is_deleted', 'N')->orderBy('id', 'DESC')->paginate();
        $this->_helper->response()->setCode(200)->send(['data' => $projects]);
    }

    public function recordById() {
        $data = [
            'status' => 'error',
            'data' => [],
        ];

        $request = request();

        if (empty($request->id)) {
            $data['msg'] = 'Id not found';
            $this->_helper->response()->setCode(200)->send($data);
        }

        $projects = new OtherProject();
        $projects = $projects->where('is_deleted', 'N')->where('id', $request->id);

        if (!$projects->exists()) {
            $data['msg'] = 'Record with provided Id not found';
            $this->_helper->response()->setCode(200)->send($data);
        }
        $projects = $projects->first();

        $data = [
            'status' => 'success',
            'msg' => 'Success',
            'data' => $projects,
        ];

        $this->_helper->response()->setCode(200)->send($data);
    }

    public function allStaffMembers()
    {
        $members = \App\User::where('type', 'staff')->get();
        $this->_helper->response()->send(['data' => $members]);
    }

    public function allCustomers()
    {
        $customers = \App\User::where('type', 'customer')->get();
        $this->_helper->response()->send(['data' => $customers]);
    }

    public function create()
    {
        $request = request();
        if (!empty($request->staff_id) && !is_array($request->staff_id)) {
            $staff_id = explode(",", $request->staff_id);
            $request->merge(['staff_id' => $staff_id]);
        } else if (empty($request->staff_id)) {
            $request->merge(['staff_id' => []]);
        }

        if (!empty($request->installment_amount)) {
            $installment_amount = explode(",", $request->installment_amount);
            $request->merge(['installment_amount' => $installment_amount]);
        } else {
            $request->merge(['installment_amount' => []]);
        }

        if (!empty($request->installment_date)) {
            $installment_date = explode(",", $request->installment_date);
            $request->merge(['installment_date' => $installment_date]);
        } else {
            $request->merge(['installment_date' => []]);
        }

        $enable_installment = (!empty($request->enable_installment)) ? $request->enable_installment : 'N' ;
        if (!empty($request->cost_type) && $request->cost_type == 'one_off') {
            //$enable_installment = 'N';
        } else {
            $enable_installment = 'N';
        }
        
        $validationArr = [
            'project_name' => 'required|string',
            'description' => 'required',
            'budget' => 'required|numeric|min:0|not_in:0'
        ];
        
        if (empty($request->edit_id)) {
            $validationArr['price'] = 'required|numeric|min:0|not_in:0';
            $validationArr['start_date'] = 'required';
            $validationArr['cost_type'] = 'required';
            $validationArr['customer_id'] = 'required';
            $validationArr['staff_id'] = 'required|array';

            if ($enable_installment == 'Y') {
                $validationArr['installment_amount.*'] = 'required|numeric|min:0|not_in:0';
                $validationArr['installment_date.*'] = 'required|date';
            }

            if (!empty($request->cost_type) && $request->cost_type == 'monthly') {
                $validationArr['ends_in'] = 'required|numeric|min:0|not_in:0';
            } else {
                $validationArr['end_date'] = 'required|date|after_or_equal:start_date';
            }
        } else {
            $validationArr['edit_id'] = 'required|numeric|min:0|not_in:0';
        }


        $this->_helper->runValidation($validationArr);

        $installmentArr = [];
        $totalInstallments = 0;

        if (empty($request->edit_id) && $enable_installment == 'Y') {
            if (count($request->installment_amount) < 2 ) {
                $data['status'] = 'error';
                $data['msg'] = 'Please Enter Atleast 2 installments.';
                $this->_helper->response()->setCode(200)->send($data);
            }

            if (count($request->installment_amount) > 3) {
                $data['status'] = 'error';
                $data['msg'] = 'Only 3 Installments are allowed.';
                $this->_helper->response()->setCode(200)->send($data);
            }

            if (count($request->installment_amount) != count($request->installment_date)) {
                $data['status'] = 'error';
                $data['msg'] = 'Every amount in installment must have date.';
                $this->_helper->response()->setCode(200)->send($data);
            }

            $totalCampaignAmount =  $request->price;
            $calcAmount = 0;

            $date_change_msg = '';
            foreach ($request->installment_amount as $key => $value) {
                if ($key > 0) {
                    $lastDate = $request->installment_date[$key-1];
                    $crntDate = $request->installment_date[$key];
                } else {
                    $lastDate = date('Y-m-d', strtotime('-1 day', time()));//date('Y-m-d');
                    $crntDate = $request->installment_date[$key];
                }
                $datediff = strtotime($crntDate) - strtotime($lastDate);
                $days = $datediff / (60 * 60 * 24);

                $keymsg1 = '';
                $keymsg2 = '';

                if (($key+1) == 1) {
                    $keymsg1 = '1st';
                }

                if (($key+1) == 2) {
                    $keymsg1 = '2nd';
                }

                if (($key+1) == 3) {
                    $keymsg1 = '3rd';
                }

                if ($key <= 0) {
                    $keymsg2 = 'today';
                }

                if ($key == 1) {
                    $keymsg2 = '1st installment';
                }

                if ($key == 2) {
                    $keymsg2 = '2nd installment';
                }

                if ($key == 3) {
                    $keymsg2 = '3rd installment';
                }

                if ($days < 1) {
                    $date_change_msg = $keymsg1.' installment date must be greater then '.$keymsg2.' date.';
                    break;
                }
                $amount = (!empty($value)) ? (float) $value : 0 ;
                $calcAmount = $calcAmount + $amount;
            }

            if (!empty($date_change_msg)) {
                $data['status'] = 'error';
                $data['msg'] = $date_change_msg;
                $this->_helper->response()->setCode(200)->send($data);
            }

            if ($totalCampaignAmount != $calcAmount) {
                $data['status'] = 'error';
                $data['msg'] = 'All installments amount must be equal to Contract Value.';
                $this->_helper->response()->setCode(200)->send($data);
            }

            $totalInstallments =  count($request->installment_amount);
        }

        $oOtherProject = new OtherProject();

        if (!empty($request->edit_id)) {
            $oOtherProject = $oOtherProject->where('is_deleted', 'N')->where('id', $request->edit_id);

            if (!$oOtherProject->exists()) {
                $data['msg'] = 'Record with provided Id not found';
                $this->_helper->response()->setCode(200)->send($data);
            }
            $oOtherProject = $oOtherProject->first();
        } else {
            $oOtherProject->created_by = Auth::user()->id;
            $oOtherProject->user_id = $request->customer_id;
            $oOtherProject->price = $request->price;
            $oOtherProject->with_installment = $enable_installment;
            $oOtherProject->installment_count = $totalInstallments;

            $oOtherProject->cost_type = $request->cost_type;
            $oOtherProject->no_of_months = $request->ends_in;
            if (!empty($request->cost_type) && $request->cost_type != 'monthly') {
                $oOtherProject->end_date = Carbon::parse($request->end_date)->format('Y-m-d H:i:s');
            }
        }

        $oOtherProject->project_name = $request->project_name;
        $oOtherProject->description = $request->description;
        $oOtherProject->start_date = Carbon::parse($request->start_date)->format('Y-m-d H:i:s');
        $oOtherProject->budget = $request->budget;
        $oOtherProject->other_status = $request->project_status;
        //dd($oOtherProject);
        $oOtherProject->save();

        $insertedId = $oOtherProject->id;


        if (empty($request->edit_id)) {
            $campaignCount = Campaign::where('user_id', $request->customer_id)->get()->count();
            $otherProjectsCount = OtherProject::where('user_id', $request->customer_id)->get()->count();
            $user = User::where('id', $request->customer_id)->first();
            if ($user->status == 'new' && ($campaignCount >= 2 || $otherProjectsCount >= 2)) {
                $user->update(['status' => 'recurring']);
            }

            if (!empty($request->staff_id)) {
                foreach ($request->staff_id as $asignee){
                    AssignedModule::create([
                        'module_id' => $insertedId,
                        'assign_to' => $asignee,
                        'module_type' => 'other_projects',
                    ]);
                }
            }

            if ($enable_installment == 'Y') {
                foreach ($request->installment_amount as $key => $value) {
                    $installmentArr[] = [
                        'user_id' => $request->customer_id,
                        'module_id' => $insertedId,
                        'module_type' => 'projects',
                        'amount' => $value,
                        'installment_date' => date('Y-m-d', strtotime($request->installment_date[$key])),
                    ];
                }

                foreach ($installmentArr as $key => $value) {
                    InvoiceInstallments::create([
                        'user_id' => $value['user_id'],
                        'module_id' => $value['module_id'],
                        'module_type' => $value['module_type'],
                        'amount' => $value['amount'],
                        'installment_date' => $value['installment_date']
                    ]);
                }
            } else {
                $oInvoice = new Invoice();
                $vat = $oInvoice->vat;
                $amount = $request->price;
                $totalAmount = $amount + (($amount/100) * $vat);

                $status = Invoice::create([
                    'user_id' => $request->customer_id,
                    'type' => 'other_project',
                    'status' => 'pending',
                    'amount' => $amount,
                    'vat' => $vat,
                    'total_amount' => $totalAmount,
                    'other_project_id' => $insertedId,
                ]);
            }

            $msg = 'Project and invoice created successfully.';
        } else {
            $msg = 'Project Updated successfully.';
        }

        $this->_helper->response()->setMessage($msg)->send(['data' => '']);
    }

    public function changeStatus() {
        $data = [
            'status' => 'error',
            'data' => [],
        ];

        $request = request();
        if (empty($request->id)) {
            $data['msg'] = 'Id not found';
            $this->_helper->response()->setCode(200)->send($data);
        }
        $projects = new OtherProject();
        $projects = $projects->where('is_deleted', 'N')->where('id', $request->id);

        if (!$projects->exists()) {
            $data['msg'] = 'Record with provided Id not found';
            $this->_helper->response()->setCode(200)->send($data);
        }
        $status = (!empty($request->status)) ? $request->status : 'pending' ;
        $projects = $projects->first();
        $projects->status = $status;
        $projects->save();

        $msg = 'Status successfully changed.';
        $data = [
            'status' => 'success',
            'msg' => $msg,
            'data' => [],
        ];

        $this->_helper->response()->setCode(200)->send($data);
    }

    public function destroyProject() {
        $data = [
            'status' => 'error',
            'data' => [],
        ];

        $request = request();

        if (empty($request->id)) {
            $data['msg'] = 'Id not found';
            $this->_helper->response()->setCode(200)->send($data);
        }

        $projects = new OtherProject();
        $projects = $projects->where('is_deleted', 'N')->where('id', $request->id);

        if (!$projects->exists()) {
            $data['msg'] = 'Record with provided Id not found';
            $this->_helper->response()->setCode(200)->send($data);
        }
        $projects = $projects->first();

        $projects->is_deleted = 'Y';
        $projects->deleted_by = Auth::user()->id;
        $projects->deleted_at = date('Y-m-d H:i:s');
        $projects->save();

        $msg = 'Project successfully deleted.';
        $data = [
            'status' => 'success',
            'msg' => $msg,
            'data' => [],
        ];

        $this->_helper->response()->setCode(200)->send($data);
    }

    public function addComment() {
        $data = [
            'status' => 'error',
            'data' => [],
        ];

        $request = request();

        if (empty($request->id)) {
            $data['msg'] = 'Invalid Project Id';
            $this->_helper->response()->setCode(200)->send($data);
        }

        if (empty($request->text)) {
            $data['msg'] = 'Enter some text in note area';
            $this->_helper->response()->setCode(200)->send($data);
        }

        $projects = new OtherProject();
        $projects = $projects->where('is_deleted', 'N')->where('id', $request->id);

        if (!$projects->exists()) {
            $data['msg'] = 'You cannot add note to this project';
            $this->_helper->response()->setCode(200)->send($data);
        }
        $projects = $projects->first();

        /*ProjectComment::create([
            'module_id' => $request->id,
            'user_id' => Auth::user()->id,
            'comment_text' => $request->text,
            'module_type' => 'other_projects',
        ]);*/

        $comments = new ProjectComment();
        $comments->module_id = $request->id;
        $comments->user_id = Auth::user()->id;
        $comments->comment_text = $request->text;
        $comments->module_type = 'other_projects';
        $comments->save();
        $insertedId = $comments->id;

        $msg = 'Note successfully added.';
        $data = [
            'status' => 'success',
            'msg' => $msg,
            'data' => $comments,
        ];

        $this->_helper->response()->setCode(200)->send($data);
    }

    public function destroyComment(Request $request) {
        $res = ProjectComment::where('id', $request->comment_id)->where('module_id', $request->parent_id)->where('module_type', 'other_projects')->delete();
        if ($res) {
            $data = [
                'status' => 'success',
                'msg' => 'Comment Successfully deleted',
            ];
        } else {
            $data = [
                'status' => 'error',
                'msg' => 'Something Went wrong',
            ];
        }
       $this->_helper->response()->setCode(200)->send($data);
    }
}

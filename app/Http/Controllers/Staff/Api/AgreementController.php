<?php

namespace App\Http\Controllers\Staff\Api;

use DB;
use Auth;
use App\Agreement;
use App\Helpers\Helper;
use App\AgreementDocuSign;
use App\Http\Controllers\Controller;

class AgreementController extends Controller
{
    protected $_helper;

    public function __construct()
    {
        $this->_helper = new Helper();
    }

    public function index(){
        $request = request();

        $agreements = new Agreement();
        $agreements = $agreements->with([
            'user' => function ($query) {
                $query->with('company');
            }, 
            'campaign' => function ($query) {
                $query->withTrashed();
            }, 
            'document' => function ($query) {
                $query->where('type', 'agreements');
            }
        ]);
        //$agreements = Auth::user()->agreements();

        if($request->has('filter')){
            if ($request->filter['selectedStartDate'] !== null && $request->filter['selectedEndDate'] !== null) {
                $agreements = $agreements->
                whereBetween('created_at', [$request->filter['selectedStartDate'], $request->filter['selectedEndDate']]);
            }

            if ($request->filter['status'] !== null) {
                $agreements = $agreements->where('status', $request->filter['status']);
            }
        }
        //$agreements = $agreements->where('user_id', Auth::user()->id);


        $agreements = $agreements->orderBy('created_at', 'DESC')->paginate();

        $this->_helper->response()->send(['data' => $agreements]);
    }

    public function resend() {
        $data = [
            'status' => 'error',
            'data' => [],
        ];

        $request = request();

        if (empty($request->id)) {
            $data['msg'] = 'Id not found';
            $this->_helper->response()->setCode(200)->send($data);
        }

        $docu = new AgreementDocuSign();
        $docu = $docu->where('type', 'agreements')->where('status', 'send')->where('module_id', $request->id);

        if (!$docu->exists()) {
            $data['msg'] = 'Record with provided Id not found or already in pending state or signed';
            $this->_helper->response()->setCode(200)->send($data);
        }

        DB::table('zoho_douments_listing')
           ->where('module_id', $request->id)
           ->where('type', 'agreements')
           ->where('status', 'send')
           ->update(['status' => 'pending']);

        $data = [
            'status' => 'success',
            'msg' => 'The agreement will be resent immediately.',
            'data' => [],
        ];

        $this->_helper->response()->setCode(200)->send($data);
    }

    public function markAsSigned() {
        $data = [
            'status' => 'error',
            'data' => [],
        ];

        $request = request();
        $sessionUser = Auth::user();

        if ($sessionUser->type != 'admin') {
            $data['msg'] = 'You are not authorized for this action.';
            $this->_helper->response()->setCode(200)->send($data);
        }

        if (empty($request->id)) {
            $data['msg'] = 'Id not found';
            $this->_helper->response()->setCode(200)->send($data);
        }

        $docu = new AgreementDocuSign();
        $docu = $docu->where('type', 'agreements')->where('status', 'send')->where('module_id', $request->id);

        if (!$docu->exists()) {
            $data['msg'] = 'Record with provided Id not found or already in pending or signed state';
            $this->_helper->response()->setCode(200)->send($data);
        }
        $docu = $docu->first();
        $response = json_decode($docu->response);
        if (is_array($response)) {
            $response['mark_by'] = $sessionUser->id;
        } else {
            $response->mark_by = $sessionUser->id;
        }

        DB::table('zoho_douments_listing')
           ->where('id', $docu->id)
           ->update(['status' => 'mark_as_signed', 'response' => json_encode($response)]);

        $updData = [
            'status' => 'completed'
        ];
        DB::table('agreements')->where('id', $docu->module_id)->update($updData);

        $agreementData = DB::select("SELECT id, campaign_id, status FROM agreements WHERE id = ".$docu->module_id." LIMIT 1");
        if (!empty($agreementData)) {
            $campaign_id = $agreementData[0]->campaign_id;
            $campaignInvoice = DB::select("SELECT id, status FROM invoices WHERE campaign_id = ".$campaign_id." ORDER BY id DESC LIMIT 1");
            if (!empty($campaignInvoice) && strtolower($campaignInvoice[0]->status) == 'completed') {
                $updData = [
                    'status' => 'awaiting'
                ];
                DB::table('campaigns')->where('id', $campaign_id)->update($updData);
            }
        }

        $data = [
            'status' => 'success',
            'msg' => 'Marked as signed successfully.',
            'data' => [],
        ];

        $this->_helper->response()->setCode(200)->send($data);
    }
}

<?php

namespace App\Http\Controllers\Staff\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\Campaigns as CampaignsResource;
use App\Http\Resources\Users as UserResource;
use App\Helpers\Helper;
use Auth;
use App\Campaign;
use App\Faq;
use App\Invoice;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Gate;


class FaqsController extends Controller
{
    private $_helper;

    public function __construct(){
        $this->_helper = new Helper();
    }

    public function get(Request $request)
    {
        $data = [
            'status' => 'success',
            'msg' => 'Success.',
            'data' => [],
        ];
       if($request->id) 
      {  $user = Faq::find($request->id);
      $data['data']['faqs'] =  ($user);}
        else
        {$user = Faq::all();
         $data['data']['faqs'] =   ($user);
 foreach ( $data['data']['faqs'] as $key => $value) {
            $value['isFrequent']= $value['isFrequent']==1?'Frequently':'General';
            $value['status']= $value['status']==1?'Active':'DeActive';
       }
     }
        

         $data['data']['isAdmin']=Gate::allows('isAdmin');
        $this->_helper->response()->setCode(200)->send($data);
    }
 public function delete(Request $request)
    {
       $news = \App\Faq::where('id', $request->id)->delete();
        $this->_helper->response()->setCode(200)->send($news);
    }
     

    public function add()
    {
        $request = request();
        
        
        $this->_helper->runValidation([
            'question' => 'required|string',
            'answer' => 'required|string'
        ]  );
        $isFrequent = empty($request->isFrequent)?0:1;
        $status = empty($request->status)?0:1;
        $User = Faq::create([
            'question' => $request->question,
            'answer' => $request->answer,
            'isFrequent' => $isFrequent,
            'status' => $status,
        ]);

        $this->_helper->response()->setMessage('Faqs Saved Successfully.')->send('');
    }
      public function update()
    {
        $request = request();

        $this->_helper->runValidation([
            'id' => 'required',
            'question' => 'required',
            'answer' => 'required'
 
        ]  );
         if ($request->question !== null)
            $obj['question'] = $request->question;
        if ($request->answer !== null)
            $obj['answer'] = $request->answer;
        if($request->isFrequent)
            $obj['isFrequent'] = 1;
        else
            $obj['isFrequent'] = 0;

        $obj['status'] = empty($request->status)?0:1;

        
        $result = \App\Faq::where('id', $request->id)->update($obj);

          


        $this->_helper->response()->setMessage('Faqs Updated Successfully.')->send('');
    }
}

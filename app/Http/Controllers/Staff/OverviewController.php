<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\Helper;

use App\Campaign;
use App\LandingPage;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class OverviewController extends Controller
{
   private $_helper;

   public function __construct(){
        $this->_helper = new Helper();

        /*$this->middleware(function ($request, $next) {
            if( $this->_helper->isAllowedModule('overview') == false) {
				//return abort(404);
                return redirect(url('/staff/campaigns'));
	   		}

            return $next($request);
        });*/

    }

   public function index($value='')
   {
        return redirect()->route('_v2.staff.staff.home-2');
   		//check access


   		//end access
   		$start_date =  Carbon::now()->startOfWeek(Carbon::MONDAY)->format('Y-m-d 00:00:00');
   		$end_date =  Carbon::now()->endOfWeek(Carbon::MONDAY)->format('Y-m-d 23:59:59');
   		//totals sales statistics this week
   		$totals_this_week = $this->getTotalBetweenDates($start_date , $end_date);

   		//totals sales statistics last week
   		$start_date = Carbon::now()->startOfWeek(Carbon::MONDAY)->subWeek()->format('Y-m-d 00:00:00');
   		$end_date   = Carbon::now()->startOfWeek(Carbon::MONDAY)->subDays(1)->format('Y-m-d 23:59:59');

   		$totals_last_week = $this->getTotalBetweenDates($start_date , $end_date);

   		$change_percentage = array(
   			'total_orders' => $this->getPercentageChange($totals_last_week[0]->total_orders, $totals_this_week[0]->total_orders),

   			'avg_order_value' => $this->getPercentageChange($totals_last_week[0]->avg_order_value, $totals_this_week[0]->avg_order_value),

   			'total_sales' => $this->getPercentageChange($totals_last_week[0]->total_sales, $totals_this_week[0]->total_sales),
   		);


   		//totals sales graphs last week

		//   		$graphs_last_week = $this->getTotalBetweenDatesGroupBy($start_date , $end_date);
		//   		$total_orders_graph = array();
		//   		$i = 0;
		//   		foreach ($graphs_last_week as $data) {
		//   			$temp = [];
		//   			$temp[0] = $i;
		//   			$temp[1] = (int)$data->total_orders;
		//   			$temp[2] = $data->total_orders .' on '.Carbon::parse($data->created_at)->toFormattedDateString() ;
		//   			$total_orders_graph[] = $temp;
		//   			$i++;
		//
		//   		}
		//
		//   		$total_avg_amount_graph = array();
		//   		$i = 0;
		//   		foreach ($graphs_last_week as $data) {
		//   			$temp = [];
		//   			$temp[0] = $i;
		//   			$temp[1] = (int)$data->avg_order_value;
		//   			$temp[2] = '£'.number_format($data->avg_order_value).' on '.Carbon::parse($data->created_at)->toFormattedDateString();
		//   			$total_avg_amount_graph[] = $temp;
		//   			$i++;
		//
		//   		}
		//
		//   		$total_sales_graph = array();
		//   		$i = 0;
		//   		foreach ($graphs_last_week as $data) {
		//   			$temp = [];
		//   			$temp[0] = $i;
		//   			$temp[1] = (int)$data->total_sales;
		//   			$temp[2] = '£'.number_format($data->total_sales).' on '.Carbon::parse($data->created_at)->toFormattedDateString();
		//   			$total_sales_graph[] = $temp;
		//   			$i++;
		//
		//   		}

   		//get total business of last month
   		 $start_date =  new Carbon('first day of last month');
   		 $end_date =  new Carbon('last day of last month');
   		 $last_month_total_new = $this->getTotalNewBusinessBetweenDates($start_date , $end_date);


   		 //get totat retention last month
   		 $last_month_total_retention = $this->getTotalNewRetentionBetweenDates($start_date , $end_date);

   		 //get total business of this month
   		 $start_date =  new Carbon('first day of this month');
   		 $end_date =  new Carbon('last day of this month');
   		 $this_month_total_new = $this->getTotalNewBusinessBetweenDates($start_date , $end_date);
   		 //dd($this_month_total_new , $start_date ,$end_date);

   		 //get this month total retention

   		 $this_month_total_retention = $this->getTotalNewRetentionBetweenDates($start_date , $end_date);

   		 //get biggest orders this month
   		 $this_month_biggest_order = $this->getBiggestOrdersBetweenDates($start_date , $end_date);

   		 //get lastet customers
   		 $this_month_customers = $this->getLatestCustomers($start_date , $end_date);

   		 //new bussiness chart this year
   		 $start_date =  Carbon::now()->startOfYear()->format('Y-m-d 00:00:00');
   		 $end_date =  Carbon::now()->format('Y-m-d 23:59:59');
   		 $this_year_new_business = $this->getTotalNewBusinessBetweenDatesByMonth($start_date , $end_date);
   		  $total_new_business_graph = $this->getByMonthDataSet($this_year_new_business);

   		 //get retention this year
   		  $this_year_retention = $this->getTotalRetentionBetweenDatesByMonth($start_date , $end_date);
   		  $this_year_retention_graph = $this->getByMonthDataSet($this_year_retention);
   		 //get support tickets
   		 $closedTickets = $this->ticketCounts('completed');
   		 $activeTickets = $this->ticketCounts('active');
   		 $allTickets = $this->ticketCounts();
   		 $percentSolved = 100;
   		 if ($allTickets[0]->total_tickets > 0) {
   		 	$percentSolved = ($allTickets[0]->total_tickets / 100) * $closedTickets[0]->total_tickets;
   		 }

   		 //get customers for pie chart by group
   		 $customers_by_status = $this->getCustomersGroups();
   		 $customers_by_status_graph = $this->customerDataSet($customers_by_status);

   		 //get staff by sales
   		 $start_date =  Carbon::now()->startOfMonth()->format('Y-m-d 00:00:00');
   		 $end_date =  Carbon::now()->format('Y-m-d 23:59:59');
   		 // echo $start_date =  new Carbon('first day of this month')->format('Y-m-d 00:00:00');
   		 // echo $end_date =  new Carbon('last day of this month')->format('Y-m-d 23:59:59');
   		 $employePerformance = $this->employePerformance($start_date , $end_date);
   		 //dd($employePerformance);
   		 $data = [
   			'totals_this_week' 			=> $totals_this_week,
   			'change_percentage' 		=> $change_percentage,
			//'total_orders_graph' 		=> $total_orders_graph,
			//'total_avg_amount_graph' 	=> $total_avg_amount_graph,
			//'total_sales_graph' 		=> $total_sales_graph,
   			'last_month_total_new' 		=> $last_month_total_new,
   			'this_month_total_new' 		=> $this_month_total_new,
   			'this_month_biggest_order' 	=> $this_month_biggest_order,
   			'this_month_customers' 		=> $this_month_customers,
   			'closedTickets'				=> $closedTickets,
   			'activeTickets'				=> $activeTickets,
   			'percentSolved'				=> $percentSolved,
   			'this_month_total_retention'=> $this_month_total_retention,
   			'last_month_total_retention'=> $last_month_total_retention,
   			'total_new_business_graph'  => $total_new_business_graph,
   			'this_year_retention_graph'	=> $this_year_retention_graph,
   			'customers_by_status'		=> $customers_by_status,
   			'customers_by_status_graph'	=> $customers_by_status_graph,
   			'employePerformance'		=> $employePerformance
		   ];

		if (request()->wantsJson()) {
			// I'm from API
			$data = ['data' => $data];
			$this->_helper->response()->send($data);
		} else {
			//return view('staff.dashboard.index', $data);
			return view('_v2.staff.dashboard', $data);
		}
   }

   function getPercentageChange($last_week_value, $this_week_value){
   		if ( $this_week_value == 0 ) {
		   $percentChange = ($last_week_value==0)?0:-100;
		}
		else {
		   $percentChange = (1 - $last_week_value / $this_week_value) * 100;
		}
		return $percentChange;
	}

	public function getTotalBetweenDates($dateFrom , $dateTo )
	{
		/*$data = DB::select('SELECT SUM(total_orders) as total_orders, AVG(avg_amount) as avg_order_value , SUM(total_amount) as total_sales from (

			    select COUNT(id) as total_orders , AVG(amount) as avg_amount , SUM(amount) as total_amount  from campaigns where created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'"

				UNION ALL
			    select COUNT(id) as total_orders, AVG(cost) as avg_amount , SUM(cost) as total_amount from landing_pages where created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'"

			    UNION ALL
			    select COUNT(id) as total_orders, AVG(price) as avg_amount , SUM(price) as total_amount from other_projects where created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'"

			) x');*/

		$query = 'SELECT SUM(total_orders) as total_orders, AVG(avg_amount) as avg_order_value , SUM(total_amount) as total_sales from (

			    select COUNT(id) as total_orders , AVG(amount) as avg_amount , SUM(amount) as total_amount  from campaigns where created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'"

			    UNION ALL
			    select COUNT(id) as total_orders, AVG(price) as avg_amount , SUM(price) as total_amount from other_projects where created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'"

			) x';


		$data = DB::select($query);

		return $data;
	}

	public function getTotalNewBusinessBetweenDates($dateFrom , $dateTo)
	{
		/*$data = DB::select('SELECT SUM(total_orders) as total_orders, AVG(avg_amount) as avg_order_value , SUM(total_amount) as total_sales from (

			    select COUNT(id) as total_orders , AVG(amount) as avg_amount , SUM(amount) as total_amount  from  (
           			select * from campaigns  where created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'" GROUP BY user_id ORDER BY MIN(id) ) temp

				UNION ALL
			    select COUNT(id) as total_orders, AVG(cost) as avg_amount , SUM(cost) as total_amount from  (
           			select * from landing_pages  where created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'" GROUP BY user_id ORDER BY MIN(id) ) temp

			    UNION ALL
			    select COUNT(id) as total_orders, AVG(price) as avg_amount , SUM(price) as total_amount from (
           			select * from other_projects  where created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'" GROUP BY user_id ORDER BY MIN(id) ) temp

			) x');*/


		$data = DB::select('SELECT SUM(total_orders) as total_orders, AVG(avg_amount) as avg_order_value , SUM(total_amount) as total_sales from (

			    select COUNT(id) as total_orders , AVG(amount) as avg_amount , SUM(amount) as total_amount  from  (
           			select * from campaigns  where created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'" GROUP BY user_id ORDER BY MIN(id) ) temp

			    UNION ALL
			    select COUNT(id) as total_orders, AVG(price) as avg_amount , SUM(price) as total_amount from (
           			select * from other_projects  where created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'" GROUP BY user_id ORDER BY MIN(id) ) temp

			) x');

		return $data;
	}

	public function getTotalNewRetentionBetweenDates($dateFrom , $dateTo)
	{
		/*$data = DB::select('SELECT SUM(total_orders) as total_orders, AVG(avg_amount) as avg_order_value , SUM(total_amount) as total_sales from (

			 select COUNT(id) as total_orders , AVG(amount) as avg_amount , SUM(amount) as total_amount from campaigns where id NOT IN (select id from campaigns GROUP BY user_id ORDER BY MIN(id)) and created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'"

			    UNION ALL
			 select COUNT(id) as total_orders, AVG(cost) as avg_amount , SUM(cost) as total_amount from landing_pages   where id NOT IN (select id from landing_pages GROUP BY user_id ORDER BY MIN(id)) and created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'"


			 UNION ALL
			 select COUNT(id) as total_orders, AVG(price) as avg_amount , SUM(price) as total_amount from  other_projects  where id NOT IN (select id from other_projects GROUP BY user_id ORDER BY MIN(id)) and  created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'"



			) x ');*/

		$data = DB::select('SELECT SUM(total_orders) as total_orders, AVG(avg_amount) as avg_order_value , SUM(total_amount) as total_sales from (

			 select COUNT(id) as total_orders , AVG(amount) as avg_amount , SUM(amount) as total_amount from campaigns where id NOT IN (select id from campaigns GROUP BY user_id ORDER BY MIN(id)) and created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'"

			 UNION ALL
			 select COUNT(id) as total_orders, AVG(price) as avg_amount , SUM(price) as total_amount from  other_projects  where id NOT IN (select id from other_projects GROUP BY user_id ORDER BY MIN(id)) and  created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'"



			) x ');

		return $data;
	}

	public function getTotalBetweenDatesGroupBy($dateFrom , $dateTo )
	{
		/*$data = DB::select('SELECT SUM(total_orders) as total_orders, AVG(avg_amount) as avg_order_value , SUM(total_amount) as total_sales , created_at from (

			    select COUNT(id) as total_orders , AVG(amount) as avg_amount , SUM(amount) as total_amount , created_at from campaigns where created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'" GROUP BY created_at

				UNION ALL
			    select COUNT(id) as total_orders, AVG(cost) as avg_amount , SUM(cost) as total_amount , created_at from landing_pages where created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'" GROUP BY created_at

			    UNION ALL
			    select COUNT(id) as total_orders, AVG(price) as avg_amount , SUM(price) as total_amount , created_at from other_projects where created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'"  GROUP BY created_at

			) x GROUP BY created_at');*/

		$data = DB::select('SELECT SUM(total_orders) as total_orders, AVG(avg_amount) as avg_order_value , SUM(total_amount) as total_sales , created_at from (

			    select COUNT(id) as total_orders , AVG(amount) as avg_amount , SUM(amount) as total_amount , created_at from campaigns where created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'" GROUP BY created_at

			    UNION ALL
			    select COUNT(id) as total_orders, AVG(price) as avg_amount , SUM(price) as total_amount , created_at from other_projects where created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'"  GROUP BY created_at

			) x GROUP BY created_at');

		return $data;
	}

	public function getBiggestOrdersBetweenDates($dateFrom , $dateTo )
	{
		/*$data = DB::select('SELECT orders.* , name, image from (

			 select amount as total_amount , DATE_FORMAT(created_at , "%m/%d/%Y at %h:%i  %p" ) as created_at , status , user_id from campaigns where created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'"

			 UNION ALL
			 select cost as total_amount , DATE_FORMAT(created_at , "%m/%d/%Y at %h:%i  %p" ) as created_at , status , user_id from landing_pages where created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'"

			 UNION ALL
			 select price as total_amount , DATE_FORMAT(created_at , "%m/%d/%Y at %h:%i  %p" ) as created_at , status , user_id from other_projects where created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'"

			 ) orders
			 join users on orders.user_id = users.id

			 ORDER BY total_amount DESC ');*/

		$data = DB::select('SELECT orders.* , name, image from (

			 select amount as total_amount , DATE_FORMAT(created_at , "%m/%d/%Y at %h:%i  %p" ) as created_at , status , user_id from campaigns where created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'"

			 UNION ALL
			 select price as total_amount , DATE_FORMAT(created_at , "%m/%d/%Y at %h:%i  %p" ) as created_at , status , user_id from other_projects where created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'"

			 ) orders
			 join users on orders.user_id = users.id

			 ORDER BY total_amount DESC ');

		return $data;
	}

	public function getLatestCustomers($dateFrom , $dateTo ) {
		$data = DB::select('SELECT name, image, DATE_FORMAT(created_at , "%m/%d/%Y at %h:%i  %p" ) as created_at from users  where created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'" AND type="customer" ORDER BY created_at DESC ');
		return $data;
	}

	public function ticketCounts($status = false)
	{
		if($status)
		{
			$data = DB::select('SELECT COUNT(id) total_tickets FROM `support_tickets` where status = "'.$status.'" and status != "deleted"');

			return $data;
		}

		$data = DB::select('SELECT COUNT(id) total_tickets FROM `support_tickets` where status != "deleted" ');

		return $data;

	}

	public function getTotalNewBusinessBetweenDatesByMonth($dateFrom , $dateTo)
	{
		/*$data = DB::select('SELECT SUM(total_orders) as total_orders, AVG(avg_amount) as avg_order_value , SUM(total_amount) as total_sales, created_at from (

			 select COUNT(id) as total_orders , AVG(amount) as avg_amount , SUM(amount) as total_amount , created_at from (
			 select * from campaigns where created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'" GROUP BY user_id, MONTH(created_at) ORDER BY MIN(id) ) temp  GROUP BY   MONTH(created_at)

			 UNION ALL
			 select COUNT(id) as total_orders, AVG(cost) as avg_amount , SUM(cost) as total_amount , created_at from (
			 select * from landing_pages where created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'" GROUP BY user_id, MONTH(created_at) ORDER BY MIN(id) ) temp GROUP BY   MONTH(created_at)

			 UNION ALL
			 select COUNT(id) as total_orders, AVG(price) as avg_amount , SUM(price) as total_amount, created_at from (
			 select * from other_projects where created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'" GROUP BY user_id, MONTH(created_at) ORDER BY MIN(id) ) temp GROUP BY   MONTH(created_at)

			 ) x  GROUP BY   MONTH(created_at)');*/

		$data = DB::select('SELECT SUM(total_orders) as total_orders, AVG(avg_amount) as avg_order_value , SUM(total_amount) as total_sales, created_at from (

			 select COUNT(id) as total_orders , AVG(amount) as avg_amount , SUM(amount) as total_amount , created_at from (
			 select * from campaigns where created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'" GROUP BY user_id, MONTH(created_at) ORDER BY MIN(id) ) temp  GROUP BY   MONTH(created_at)

			 UNION ALL
			 select COUNT(id) as total_orders, AVG(price) as avg_amount , SUM(price) as total_amount, created_at from (
			 select * from other_projects where created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'" GROUP BY user_id, MONTH(created_at) ORDER BY MIN(id) ) temp GROUP BY   MONTH(created_at)

			 ) x  GROUP BY   MONTH(created_at)');

		return $data;
	}

	public function getByMonthDataSet($data)
	{

		$dataSet = array();
   		 $legends = [];
   		 $legends[0] = 'Month';
   		 $legends[1] = 'Business';
   		 $dataSet[] =  $legends;
   		 $i = 0;
   		 if ($data)
         {
             foreach ($data as $month) {
                 $temp = [];
                 $temp[0] = Carbon::parse($month->created_at)->format('M');
                 $temp[1] = (int)$month->total_sales;
                 $dataSet[] = $temp;
                 $i++;

             }
         }
   		 else
         {
             $temp[0] = Carbon::now()->format('M');
             $temp[1] = 0;
             $dataSet[] = $temp;
         }


		return $dataSet;
	}


	public function getTotalRetentionBetweenDatesByMonth($dateFrom , $dateTo)
	{
		/*$data = DB::select('SELECT SUM(total_orders) as total_orders, AVG(avg_amount) as avg_order_value , SUM(total_amount) as total_sales, created_at from (

		 select COUNT(id) as total_orders , AVG(amount) as avg_amount , SUM(amount) as total_amount , created_at from campaigns where id NOT IN (select id from campaigns GROUP BY user_id ORDER BY MIN(id)) and created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'"  GROUP BY   MONTH(created_at)

		 UNION ALL
		 select COUNT(id) as total_orders, AVG(cost) as avg_amount , SUM(cost) as total_amount , created_at  from landing_pages where id NOT IN (select id from landing_pages GROUP BY user_id ORDER BY MIN(id)) and created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'"  GROUP BY   MONTH(created_at)


		 UNION ALL
		 select COUNT(id) as total_orders, AVG(price) as avg_amount , SUM(price) as total_amount , created_at  from other_projects where id NOT IN (select id from other_projects GROUP BY user_id ORDER BY MIN(id)) and created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'" GROUP BY   MONTH(created_at)



		 ) x GROUP BY   MONTH(created_at)');*/

		$data = DB::select('SELECT SUM(total_orders) as total_orders, AVG(avg_amount) as avg_order_value , SUM(total_amount) as total_sales, created_at from (

		 select COUNT(id) as total_orders , AVG(amount) as avg_amount , SUM(amount) as total_amount , created_at from campaigns where id NOT IN (select id from campaigns GROUP BY user_id ORDER BY MIN(id)) and created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'"  GROUP BY   MONTH(created_at)


		 UNION ALL
		 select COUNT(id) as total_orders, AVG(price) as avg_amount , SUM(price) as total_amount , created_at  from other_projects where id NOT IN (select id from other_projects GROUP BY user_id ORDER BY MIN(id)) and created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'" GROUP BY   MONTH(created_at)



		 ) x GROUP BY   MONTH(created_at)');

		return $data;
	}

	public function getCustomersGroups()
	{
		$data = DB::select('SELECT count(id) as total_customers , status FROM `users` WHERE status != "lost" and type = "customer" GROUP BY status');

		//make data set for chart

		return $data;

	}

	public function customerDataSet($data)
	{
		$dataSet = array();
   		 $legends = [];
   		 $legends[0] = 'Status';
   		 $legends[1] = 'Total';
   		 $dataSet[] =  $legends;
   		 $i = 0;
	   	 foreach ($data as $type) {
	   			$temp = [];
	   			$temp[0] = $type->status;
	   			$temp[1] = (int)$type->total_customers;
	   			$dataSet[] = $temp;
	   			$i++;

	   		}

		return $dataSet;
	}

	public function employePerformance($dateFrom , $dateTo)
	{
		$campaignNewBusiness = ' SELECT COUNT(id) as total_orders , SUM(amount) as total_amount , staff_id , NULL as retention_total_orders , NULL as retention_total_sales FROM (
	 		SELECT campaigns.id , amount, assigned_id as staff_id FROM campaigns WHERE campaigns.created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'"  GROUP BY campaigns.user_id ORDER BY MIN(campaigns.id) ) temp  GROUP BY staff_id ';

	    $campaignRetension = ' SELECT  NULL as total_orders , NULL as total_amount ,  assigned_id as staff_id , COUNT(campaigns.id) as retention_total_orders , SUM(amount) as retention_total_sales  FROM campaigns WHERE campaigns.id NOT IN (SELECT campaigns.id FROM campaigns GROUP BY user_id ORDER BY MIN(campaigns.id)) and campaigns.created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'" GROUP BY staff_id ';

	   /*$landingPageNewBusiness = ' SELECT COUNT(id) as total_orders, SUM(cost) as total_amount , staff_id, NULL as retention_total_orders , NULL as retention_total_sales   FROM (
 			SELECT landing_pages.id , cost , 	assigned_to as staff_id   FROM landing_pages WHERE created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'"   GROUP BY user_id ORDER BY MIN(id) ) temp  GROUP BY staff_id ';

		$landingPageRentenion = 'SELECT NULL as total_orders , NULL as total_amount , assigned_to as staff_id , COUNT(landing_pages.id) as retention_total_orders,  SUM(cost) as retention_total_sales  FROM landing_pages WHERE id NOT IN (SELECT id FROM landing_pages GROUP BY user_id ORDER BY MIN(id)) and created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'" GROUP BY staff_id';*/

		$otherProjectNewBusiness = ' SELECT COUNT(id) as total_orders, SUM(price) as total_amount , staff_id, NULL as retention_total_orders , NULL as retention_total_sales   FROM (
 			SELECT other_projects.id , price, assign_to as staff_id  FROM other_projects
		     JOIN
		     assigned_modules on assigned_modules.module_id = other_projects.id AND assigned_modules.module_type="other_projects"
		     WHERE other_projects.created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'"  GROUP BY user_id ORDER BY MIN(other_projects.id) ) temp  GROUP BY staff_id ';

       $otherProjectRetention = ' SELECT  NULL as total_orders , NULL as total_amount , assign_to as staff_id , COUNT(other_projects.id) as retention_total_orders, SUM(price) as retention_total_sales  FROM other_projects
			JOIN
			     assigned_modules on assigned_modules.module_id = other_projects.id AND assigned_modules.module_type = "other_projects"
			WHERE other_projects.id NOT IN (SELECT other_projects.id FROM other_projects GROUP BY user_id ORDER BY MIN(other_projects.id)) and other_projects.created_at BETWEEN "'.$dateFrom.'" AND "'.$dateTo .'" GROUP BY staff_id ';

		/*$data = DB::SELECT(' SELECT SUM(total_orders) as total_orders, SUM(total_amount) as total_sales , staff_id , SUM(retention_total_orders) as retention_total_orders , SUM(retention_total_sales)  as retention_total_sales , name  FROM (
			'.$campaignNewBusiness .' UNION ALL
			'.$campaignRetension.' UNION ALL
			'.$landingPageNewBusiness.' UNION ALL
			'.$landingPageRentenion.'  UNION ALL
			'.$otherProjectNewBusiness.'  UNION ALL
			'.$otherProjectRetention.'
			) x
			join users on x.staff_id = users.id and type="staff"
			 group by staff_id');*/

		$queryFull = ' SELECT SUM(total_orders) as total_orders, SUM(total_amount) as total_sales , staff_id , SUM(retention_total_orders) as retention_total_orders , SUM(retention_total_sales)  as retention_total_sales , name  FROM (
			'.$campaignNewBusiness .' UNION ALL
			'.$campaignRetension.' UNION ALL
			'.$otherProjectNewBusiness.'  UNION ALL
			'.$otherProjectRetention.'
			) x
			join users on x.staff_id = users.id and type="staff"
			 group by staff_id';

		$data = DB::select($queryFull);

		return $data;
	}



}

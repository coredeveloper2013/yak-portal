<?php

namespace App\Http\Controllers\Staff;

use http\Client\Curl\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use Auth;
use App\Invoice;
use Carbon\Carbon;
use App\Agreement;
use App\Notification;
use App\Helpers\Helper;
use App\AgreementDocuSign;
use App\Unbounce;
use App\Leads;
use App\LeadDeal;
use App\LeadDealQuestion;
use App\LeadQuestion;
use Illuminate\Support\Facades\Validator;
class UnbounceController extends Controller
{

    protected $_helper;

    public function __construct()
    {
        $this->_helper = new Helper();

        $this->middleware(function ($request, $next) {
            if( $this->_helper->isAllowedModule('unbounce') == false) {
                return abort(404);
            }

            return $next($request);
        });
    }

    public function index($id = -1)
    {
        $request = request();
        //        $campaigns = Auth::user()->campaigns();
        $campaigns = \App\Campaign::orderBy('created_at', 'DESC')->get();
        $unbounce='';
        if(Auth::user()->type=="admin")
        {
            $unbounce=Unbounce::getALl($id);
        }
        else{
            $unbounce=Unbounce::getALl(Auth::id());
        }

        $members =  \App\User::where('type', 'staff')->get();
        if (request()->wantsJson()) {
            $data = [
                'campaigns' =>  $campaigns,
                'unbounce' =>  $unbounce,
                'members' => $members
            ];
            $this->_helper->response()->setCode(200)->send(['data' => $data]);
        } else {
            return view('staff.unbounce.index', [
                'page_title' => 'Unbounce',
                'campaigns' =>  $campaigns,
                'unbounce' =>  $unbounce,
                'members' => $members,
                'user_search_id' => $id
            ]);
        }
       
	}


    public function add()
    {
        $request = request();


        $this->_helper->runValidation([
             'unbounce_id' => 'required',
             'user_id' => 'required',
             'campaign' => 'required_without:is_new_bussiness_lead',
        ]  );

         $exist= \App\Unbounce::where(['unbounce_id' => $request->unbounce_id,'campaign_id' => $request->campaign])->get();
         $is_new_exist= \App\Unbounce::where(['unbounce_id' => $request->unbounce_id,'is_new_bussiness_lead' => 1])->get();
         if(!empty($exist[0]) && !empty($is_new_exist[0])){

            $data = [
                'status' => 'error',
                'msg' => 'Already exist.',
                'data' => []
            ];
            $this->_helper->response()->setCode(200)->send($data);
         }
         $campaign = \App\Unbounce::create([
            'user_id' => Auth::user()->type == 'admin' ? $request->user_id : Auth::user()->id,
            'is_new_bussiness_lead' => $request->exists('is_new_bussiness_lead') ? 1 : 0,
            'unbounce_id' => $request->unbounce_id,
            'campaign_id' =>  $request->exists('campaign') ? $request->campaign : 0

        ]);

        $this->_helper->response()->setMessage('Saved Successfully.')->send('');
    }

    function delete()
    {
        $request = request();

        $this->_helper->runValidation([
            'id' => 'required'
        ]  );
        $obj = Unbounce::find($request->id);
        $check = $obj->delete();
        if ($check)
            $this->_helper->response()->setMessage('Deleted Successfully.')->send('');
        else
            $this->_helper->response()->setMessage('Something Went Wrong.')->send('');

    }

    public function cron()
    {

       $appKey = 'd12f072ccc809b29767b92fec66c9b27';
       $Unbounce=Unbounce::all();
       // $Unbounce=Unbounce::whereDay('created_at', now()->day)->get();

       foreach ($Unbounce as $ukey => $unb) {
           $userId=$unb->user_id;
           $campaignId=$unb->campaign_id;
           $unbounceId=$unb->unbounce_id;
        $url = 'https://api.unbounce.com/pages/'.$unbounceId.'/leads?limit=1000';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$appKey");
        $result = curl_exec($ch);
        curl_close($ch);
        $result=  json_decode(  $result,true);
       //call API
       if(!empty($result['leads']))
        foreach ($result['leads'] as $key => $value) {
            $unbLeadsId= $value['id'];
            if(!$unb['is_new_bussiness_lead']){

        $exsit =Leads::where(['unbounce_id'=>$value['id'],'campaign_id'=>$campaignId])->get();
            if(!empty($exsit[0])){
             continue;
            }
        $leads = new Leads();
        $leads->campaign_id = $campaignId;
        $leads->user_id = $userId;
        $leads->unbounce_id =      $unbLeadsId;
        $leads->submitted_on =  Carbon::now();
        $isSaved = $leads->save();
        $insertedId = $leads->id;
            if($insertedId){
            foreach ($value['form_data'] as $key2 => $value2) {
            LeadQuestion::create([
                'user_id' =>   $userId,
                'lead_id' => $insertedId,
                'headings' => $key2,
                'answers' => $value2[0]
            ]);

            }}

    }else{

        $is_new_exist =LeadDeal::where(['unbounce_id'=>$value['id'],'user_id'=> $userId])->get();
           if(!empty($is_new_exist[0])){
             continue;
            }
          $leads = new LeadDeal();
                    $leads->user_id = $userId;
                    $leads->unbounce_id = $unbLeadsId;
                    $isSaved = $leads->save();
                    $insertedId = $leads->id;
            if($insertedId){
            foreach ($value['form_data'] as $key2 => $value2) {
            LeadDealQuestion::create([
                            'lead_deal_id' => $insertedId,
                            'question' => $key2,
                            'answer' => $value2[0]
                        ]);
            }}

    }
        }
    }




    }
 public function cron1()
    {
       $appKey = 'd12f072ccc809b29767b92fec66c9b27';
       $Unbounce=Unbounce::all();
       // $Unbounce=Unbounce::whereDay('created_at', now()->day)->get();

       foreach ($Unbounce as $ukey => $unb) {
           $userId=$unb->user_id;
           $campaignId=$unb->campaign_id;
           $unbounceId=$unb->unbounce_id;
        $url = 'https://api.unbounce.com/pages/'.$unbounceId.'/leads?limit=1000';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$appKey");
        $result = curl_exec($ch);
        curl_close($ch);
        $result=  json_decode(  $result,true);
       //call API

        dd( $Unbounce);
        if(!empty($result['leads']))
        foreach ($result['leads'] as $key => $value) {
                $unbLeadsId= $value['id'];
                $exsit =Leads::where(['unbounce_id'=>$value['id'],'campaign_id'=>$campaignId])->get();
                $is_new_exist =Unbounce::where(['unbounce_id'=>$value['id'],'is_new_bussiness_lead'=> 1])->get();

                if(!empty($exsit[0])){
                    $leads = new Leads();
                    $leads->campaign_id = $campaignId;
                    $leads->user_id = $userId;
                    $leads->unbounce_id =      $unbLeadsId;
                    $leads->submitted_on =  Carbon::now();
                    $isSaved = $leads->save();
                    $insertedId = $leads->id;
                    if($insertedId){
                        foreach ($value['form_data'] as $key2 => $value2) {
                            LeadQuestion::create([
                                'user_id' =>   $userId,
                                'lead_id' => $insertedId,
                                'headings' => $key2,
                                'answers' => $value2[0]
                            ]);
                        }
                    }
                }
                else if (!empty($is_new_exist[0])) {

                    $leads = new LeadDeal();
                    $leads->user_id = $userId;
                    $isSaved = $leads->save();
                    $insertedId = $leads->id;

                    foreach ($value['form_data'] as $key2 => $value2) {
                        LeadDealQuestion::create([
                            'lead_deal_id' => $insertedId,
                            'question' => $key2,
                            'answer' => $value2[0]
                        ]);
                    }
                }

            }
        }
    }

}

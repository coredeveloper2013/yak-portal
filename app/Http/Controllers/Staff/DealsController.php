<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\LeadDeal;
use App\User;
use Auth;
use App\Helpers\Helper;

class DealsController extends Controller {

    //
   private $_helper;

   public function __construct(){
        $this->_helper = new Helper();

        $this->middleware(function ($request, $next) {
            if( $this->_helper->isAllowedModule('leads') == false && $this->_helper->isAllowedModule('deals') == false)
                {
                    return abort(404);
                }

            return $next($request);
        });

    }

    public function deals($user_id = -1) {
        $users = User::where('type', 'staff')->get();
        $searchID = $user_id;
        $data = [
            'deals_data' => $this->parse_leads_data($user_id),
            'users' =>     $users,
            'searchID' => $searchID
        ];
        return view('staff.newbusinesshub.deals', $data);
    }

    public function leads() {
        $sessionUser = Auth::user();
        $user_id = $sessionUser->id;
        if ($sessionUser->type == 'admin')
            $leads_data = LeadDeal::get_deals_data_admin();
        else
            $leads_data = LeadDeal::get_deals_data($user_id);

        $oLeadDeal = new LeadDeal();
        $list = $oLeadDeal->statusEnums;
        $users = User::where('type', 'staff')->get();

        return view('staff.newbusinesshub.leaddetails', ['leads_data' => $leads_data, 'users' => $users, 'listStatus' => $list, 'session_user' => $sessionUser]);
    }

    private function parse_leads_data($id) {
        

        $deals_aggregate_data = [
            'budget_count' => 0,
            'deals_count' => 0,
        ];

        $leads_data = [];
        if (Auth::user()->type == 'admin') {
            $user_id = $id;
        } else {
            $user_id = Auth::id();
        }
        $user_lead_counts_data = LeadDeal::get_deals_count_by_user_id($user_id);

        $deals_data = [];
        $oLeadDeal = new LeadDeal();
        foreach ($oLeadDeal->statusEnums as $key => $value) {
            $deals_data[$key]['name'] = $value;
            $deals_data[$key]['deals_aggregate_data'] = $deals_aggregate_data;
            $deals_data[$key]['deals_data'] = [];
        }

        // map deals aggregate data
        foreach ($user_lead_counts_data as $lcd_key => $lcd_val) {

            if (array_key_exists($lcd_val->status, $deals_data)) {

                $deals_data[$lcd_val->status]['deals_aggregate_data']['budget_count'] = $lcd_val->budget_count;
                $deals_data[$lcd_val->status]['deals_aggregate_data']['deals_count'] = $lcd_val->deals_count;

                // get deals data
                $deal_data = LeadDeal::get_deals_by_user_id($user_id, $lcd_val->status);
                foreach ($deal_data as $key => $value) {
                    $deal_data[$key]['userName'] = (User::find($value['user_id']))->name;
                    $deal_data[$key]['title'] = 'Lead No #'.@$value['id'];
                }

                if (isset($deal_data) && !empty($deal_data)) {

                    $deals_data[$lcd_val->status]['deals_data'] = $deal_data;

                    unset($deal_data);
                }
            }
        }

        return $deals_data;
    }

    public function dealDetail() {
        $request = request();
        $user_id = Auth::id();

        $leads_data = LeadDeal::get_deals_data_by_id($request->id);
        $users = User::where('type', 'staff')->get();

        $oLeadDeal = new LeadDeal();
        $list = $oLeadDeal->statusEnums;

        foreach ($leads_data as $key => $value) {

            $value['created'] = date('F d,Y', strtotime($value['created_at']));
            $value['statusName'] = $list[$value['status']];
            $value['userName'] = (User::find($value['user_id']))->name;
        }

        $data['data'] = ['leads' => $leads_data, 'users' => $users, 'list' => $list];
        //        $this->_helper->response()->setCode(200)->send($data);
        if (request()->wantsJson()) {
            $data = ['leads' => $leads_data, 'users' => $users, 'list' => $list];

            $this->_helper->response()->send(['data' => $data]);
        } else {
            return view('staff.newbusinesshub.dealdetail', ['leads' => $leads_data, 'users' => $users, 'list' => $list]);
        }
        // return view('staff.newbusinesshub.leads', ['leads_data' => $leads_data,'users'=>$users]);
    }

}

<?php

namespace App\Http\Controllers\Staff;

use Auth;
use Carbon\Carbon;
use App\Agreement;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class AgreementController extends Controller
{

    protected $_helper;

    public function __construct()
    {
        $this->_helper = new Helper();

        $this->middleware(function ($request, $next) {
            if( $this->_helper->isAllowedModule('agreements') == false)
                {
                    return abort(404);
                }

            return $next($request);
        });
    }


    public function index(){

        $request = request();

        $agreements = new Agreement();
        $agreements = $agreements->with(['user', 'campaign', 'document' => function ($query) {
            $query->where('type', 'agreements');
        }]);
        //$agreements = Auth::user()->agreements();

        if($request->has('filter')){
            if ($request->filter['selectedStartDate'] !== null && $request->filter['selectedEndDate'] !== null) {
                $agreements = $agreements->
                whereBetween('created_at', [$request->filter['selectedStartDate'], $request->filter['selectedEndDate']]);
            }

            if ($request->filter['status'] !== null) {
                $agreements = $agreements->where('status', $request->filter['status']);
            }
        }
        //$agreements = $agreements->where('user_id', Auth::user()->id);


        $agreements = $agreements->orderBy('created_at', 'DESC')->paginate();

        $this->_helper->response()->send(['data' => $agreements]);
    }
}

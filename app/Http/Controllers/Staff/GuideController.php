<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\Campaigns as CampaignsResource;
use App\Helpers\Helper;
use Auth;
use App\Guide;
use App\User;
use App\LandingPage;

class GuideController extends Controller
{
    private $_helper;

    public function __construct(){
        $this->_helper = new Helper();
        $this->middleware(function ($request, $next) {
            if( $this->_helper->isAllowedModule('guide') == false)
                {
                    return abort(404);
                }

            return $next($request);
        });
    }

    public function index() {
    	
        return view('staff.guide.index');
    }
    
    public function guide() {
    	$guides=Guide::all();
         foreach ($guides as $key => $value) {
            $value['descriptionF']= strip_tags($value['description'],'<br>');
         $value['msg'] =\Carbon\Carbon::createFromTimeStamp(strtotime($value['created_at']))->diffForHumans() ;
            }
    	return view('staff.guide.guide',compact('guides'));
    }
}

<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\Campaigns as CampaignsResource;
use App\Helpers\Helper;
use Auth;
use App\Campaign;
use App\User;
use App\LandingPage;
use DB;

class WorkloadController extends Controller
{
    private $_helper;

    public function __construct(){
        $this->_helper = new Helper();

        $this->middleware(function ($request, $next) {
            if( $this->_helper->isAllowedModule('work_load') == false)
                {
                    return abort(404);
                }

            return $next($request);
        });
    }

    public function index(){
        $sum_camp = DB::table('campaigns')
            ->select(DB::raw('status, sum(amount) as sum'))
            ->groupBy('status')
            ->pluck('sum', 'status');

        $count_camp = DB::table('campaigns')
            ->select(DB::raw('status, count(amount) as total'))
            ->groupBy('status')
            ->pluck('total', 'status');

        $staff_camp = DB::table('campaigns as c')
            ->join('users as u', 'u.id', '=', 'c.assigned_id')
            ->select(DB::raw('COUNT(c.id) total, c.assigned_id, u.name staff_name'))
            ->groupBy('c.assigned_id')
            ->get();
        /*$staff_pages = DB::table('landing_pages as l')
            ->join('users as u', 'u.id', '=', 'l.assigned_to')
            ->select(DB::raw('COUNT(l.id) total, l.assigned_to, u.name staff_name'))
            ->groupBy('l.assigned_to')
            ->get();*/

        $data = [
            'sum_camp' => $sum_camp,
            'count_camp' => $count_camp,
            'staff_camp' => $staff_camp,
            'staff_pages' => [],//$staff_pages,
        ];
        if (request()->wantsJson()) {
            $this->_helper->response()->send(['data' => $data]);
        } else {
            return view('staff.workload.index', $data);
        }
    }
}

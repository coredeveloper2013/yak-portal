<?php

namespace App\Http\Controllers\Staff;

use PDF;
use Auth;
use App\User;
use App\Invoice;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use App\Mail\SendInvoiceEmail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class InvoiceController extends Controller
{
   private $_helper;

   public function __construct(){
        $this->_helper = new Helper();

        $this->middleware(function ($request, $next) {
            if( $this->_helper->isAllowedModule('invoice') == false)
                {
                    return abort(404);
                }

            return $next($request);
        });

    }

    public function index() {
        $stripePub = getenv("STRIPE_KEY");
        return view('staff.invoice.index', ['stripe_pub' => $stripePub]);
    }

    public function donloadInvoice($id='') {
        //$invoice = Invoice::with(['campaign', 'landing_page', 'other_project'])->find($id);
        $invoice = Invoice::with([
            'campaign' => function ($query) {
                $query->withTrashed();
            }, 
            'landing_page', 
            'other_project' , 
            'user.business_info'
        ])->where('id', $id);
        if (!$invoice->exists()) {
            if (request()->wantsJson()) {
                $data['status'] = 'error';
                $data['msg'] = 'No Record Found against this id.';
                $this->_helper->response()->send($data);
            } else {
                return redirect()->back()->withErrors([]);
            }
        }

        $invoice = $invoice->first();

        if (request()->wantsJson()) {
            set_time_limit(300);
            $logoImage = URL('/images/ppc_pdf_logo.jpg');
            $logoImage = public_path().'/images/ppc_pdf_logo.jpg';

            $pdf = PDF::loadView('staff.invoice.download', ['invoice' => $invoice, 'page_title' => 'Download Invoice', 'logo' =>$logoImage]);
            $dir = 'temp';
            //$dir = 'public/temp';
            $downloadLink = 'storage/temp';

            if(!Storage::exists($dir)) {
                Storage::makeDirectory($dir, 0775, true); //creates directory
            }

            $file_name = $id.'_invoice_download.pdf';
            $file_name_dir = $dir.'/'.$file_name;
            $download_link = $downloadLink.'/'.$file_name;

            $content = $pdf->output();
            Storage::put($file_name_dir, $content);

            $public_url = URL($download_link);
            $data['status'] = 'success';
            $data['msg'] = 'Successfully created the pdf';
            $data['data']['url'] = $public_url;
            $this->_helper->response()->send($data);
        } else {
            $logoImage = URL('/images/ppc_pdf_logo.jpg');

            $data = ['invoice' => $invoice, 'page_title' => 'Download Invoice', 'logo' =>$logoImage];
            $pdf = PDF::loadView('staff.invoice.download', $data);
            return $pdf->download('invoice.pdf');
            //return view('staff.invoice.download', $data);
        }
    }

    function sendInvoice(Request $request)
    {
        $id=$request->id;
        $invoice = Invoice::with([
            'campaign' => function ($query) {
                $query->withTrashed();
            }, 
            'landing_page', 
            'other_project'
        ])->where('id', $id)->first();;
        \Mail::to($invoice->user->email)->send(new SendInvoiceEmail($invoice));
        $data['status'] = 'success';
        $data['msg'] = 'Mail sent successfully';
        if (\Mail::failures()) {
            $data['status'] = 'error';
            $data['msg'] = 'Something went wrong';
            $this->_helper->response()->send($data);
        }
        $this->_helper->response()->send($data);
    }
}

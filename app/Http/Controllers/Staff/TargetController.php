<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\Campaigns as CampaignsResource;
use App\Helpers\Helper;
use Auth;
use App\Guide;
use App\Target;
use App\User;
use App\LandingPage;

class TargetController extends Controller
{
    private $_helper;

    public function __construct(){
        $this->_helper = new Helper();
        
        $this->middleware(function ($request, $next) {
            if( $this->_helper->isAllowedModule('target') == false)
                {
                    return abort(404);
                }

            return $next($request);
        });
    }

    public function index(){
    	$targets = Target::all();
    	
    	// foreach ($variable as $key => $value) {
    	// 	# code...
    		
    	// 	// $targets['count' = Targets::where('useId',$value)
    	// }

        return view('staff.target.index',compact('targets'));
    }
     
}

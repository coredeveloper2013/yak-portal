<?php

namespace App\Http\Controllers\Staff;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use DB;

class DashboardController extends Controller
{
    protected $_helper;

    public function __construct()
    {
        $this->_helper = new Helper();
    }

    public function leaderBoard()
    {
        // $months[] = Carbon::now()->isoFormat('MMMM');
        // $monthNumbers[Carbon::now()->isoFormat('MMMM Y')] = 0;
        // for ($i = 1; $i < 6; $i++) {
        for ($i = 5; $i >= 0; $i--) {
            $months[] = $month = Carbon::now()->firstOfMonth()->subMonths($i)->isoFormat('MMMM');
            $monthYear = Carbon::now()->firstOfMonth()->subMonths($i)->isoFormat('MMMM Y');
            $monthNumbers[$monthYear] = 0;
        }

        $offset = 0;
        $limit = 4;
        if (request()->has('offset')) {
            // $offset = request()->offset;
        }

        $totalRecords = User::where('type', 'staff')->count();
        // $users = \Illuminate\Support\Facades\DB::table('users')->where('type', '=', 'staff')->offset($offset)->limit($limit)->get();
        $users = \Illuminate\Support\Facades\DB::table('users')->where('type', '=', 'staff')->get();

        $crntMnth = Carbon::now()->format('Y-m');
        $lastMnth = Carbon::now()->subMonth()->format('Y-m');

        $data = [];
        foreach ($users as $user) {
            $data[$user->id]['user_name'] = $user->name;
            $data[$user->id]['reports'] = $monthNumbers;
            $data[$user->id]['chartValues'] = [0, 0, 0, 0, 0, 0];

            $lastMonthSale = 0;
            $currentMonthSale = 0;

            //get first landing page against single user
            
            //$landingPages = $this->getAllLandingPages($user->id);

            //get invoice of every landing page
            
            /*foreach ($landingPages as $landingPage) {
                $count = 0;
                $crntMonthCounted = false;
                $lastMonthCounted = false;
                foreach ($monthNumbers as $key => $month) {
                    $temp = explode(" ", $key);



                    $yearMon = Carbon::parse($landingPage->created_at)->format('Y-m');
                    $loopDate = $temp[1].'-'.Carbon::parse($temp[0])->format('m');

                    if ($yearMon == $crntMnth && $crntMonthCounted !== true) {
                        $crntMonthCounted = true;
                        $currentMonthSale = $currentMonthSale + $landingPage->cost;
                    }

                    if ($yearMon == $lastMnth && $lastMonthCounted !== true) {
                        $lastMonthCounted = true;
                        $lastMonthSale = $lastMonthSale + $landingPage->cost;
                    }

                    if ($yearMon == $loopDate) {
                        $data[$user->id]['reports'][$key] = $data[$user->id]['reports'][$key] + 1;
                        //$data[$user->id]['chartValues'][$count] = $data[$user->id]['reports'][$key];
                        //$data[$user->id]['chartValues'][$count] = $currentMonthSale;
                        $data[$user->id]['chartValues'][$count] = $data[$user->id]['chartValues'][$count] + $landingPage->cost;
                    }

                    $count++;
                }
            }*/

            //get first campaign against single user
            $campaigns = $this->getAllCampaigns($user->id);

            //get invoice of every campaign for user
            foreach ($campaigns as $campaign) {
                $count = 0;
                $crntMonthCounted = false;
                $lastMonthCounted = false;
                foreach ($monthNumbers as $key => $month) {

                    $temp = explode(" ", $key);

                    $yearMon = Carbon::parse($campaign->created_at)->format('Y-m');
                    $loopDate = $temp[1].'-'.Carbon::parse($temp[0])->format('m');

                    if ($yearMon == $crntMnth && $crntMonthCounted !== true) {
                        $crntMonthCounted = true;
                        $currentMonthSale = $currentMonthSale + $campaign->amount;
                    }

                    if ($yearMon == $lastMnth && $lastMonthCounted !== true) {
                        $lastMonthCounted = true;
                        $lastMonthSale = $lastMonthSale + $campaign->amount;
                    }

                    if ($yearMon == $loopDate) {
                        $data[$user->id]['reports'][$key] = $data[$user->id]['reports'][$key] + 1;
                        //$data[$user->id]['chartValues'][$count] = $data[$user->id]['reports'][$key];
                        //$data[$user->id]['chartValues'][$count] = $currentMonthSale;
                        $data[$user->id]['chartValues'][$count] = $data[$user->id]['chartValues'][$count] + $campaign->amount;
                    }
                    $count++;
                }
            }


            $projects = $this->getAllProjects($user->id);

            //get invoice of every campaign for user
            foreach ($projects as $project) {
                $count = 0;
                $crntMonthCounted = false;
                $lastMonthCounted = false;
                foreach ($monthNumbers as $key => $month) {

                    $temp = explode(" ", $key);

                    $yearMon = Carbon::parse($project->created_at)->format('Y-m');
                    $loopDate = $temp[1].'-'.Carbon::parse($temp[0])->format('m');

                    if ($yearMon == $crntMnth && $crntMonthCounted !== true) {
                        $crntMonthCounted = true;
                        $currentMonthSale = $currentMonthSale + $project->price;
                    }

                    if ($yearMon == $lastMnth && $lastMonthCounted !== true) {
                        $lastMonthCounted = true;
                        $lastMonthSale = $lastMonthSale + $project->price;
                    }

                    if ($yearMon == $loopDate) {
                        $data[$user->id]['reports'][$key] = $data[$user->id]['reports'][$key] + 1;
                        //$data[$user->id]['chartValues'][$count] = $data[$user->id]['reports'][$key];
                        //$data[$user->id]['chartValues'][$count] = $currentMonthSale;
                        $data[$user->id]['chartValues'][$count] = $data[$user->id]['chartValues'][$count] + $project->price;
                    }
                    $count++;
                }
            }

            $data[$user->id]['lastMonthSale'] = $lastMonthSale;
            $data[$user->id]['currentMonthSale'] = $currentMonthSale;
        }

        if (request()->wantsJson() && request()->method() == "GET") {
            $this->_helper->response()->send(['data' => [
                'fields' => $months,
                'total' => $totalRecords,
                'limit' => $users->count(),
                'offset' => $offset + $limit,
                'lastoffset' => ($offset - $limit >= 0) ? $offset - $limit : 0,
                'data' => $data
            ]]);
        } else {
            $this->_helper->response()->send([
                'fields' => $months,
                'total' => $totalRecords,
                'limit' => $users->count(),
                'offset' => $offset + $limit,
                'lastoffset' => ($offset - $limit >= 0) ? $offset - $limit : 0,
                'data' => $data
            ]);
        }
    }



    public function retentionLeaderBoard()
    {
        // $months[] = Carbon::now()->isoFormat('MMMM');
        // $monthNumbers[Carbon::now()->isoFormat('MMMM Y')] = 0;
        // for ($i = 1; $i < 6; $i++) {
        for ($i = 5; $i >= 0; $i--) {
            $months[] = $month = Carbon::now()->firstOfMonth()->subMonths($i)->isoFormat('MMMM');
            $monthYear = Carbon::now()->firstOfMonth()->subMonths($i)->isoFormat('MMMM Y');
            $monthNumbers[$monthYear] = 0;
        }

        $offset = 0;
        $limit = 4;
        if (request()->has('offset')) {
            // $offset = request()->offset;
        }

        $totalRecords = User::where('type', 'staff')->count();
        $users = \Illuminate\Support\Facades\DB::table('users')->where('type', '=', 'staff')
            // ->offset($offset)->limit($limit)->get();
            ->get();

        $crntMnth = Carbon::now()->format('Y-m');
        $lastMnth = Carbon::now()->subMonth()->format('Y-m');


        $data = [];
        foreach ($users as $user) {
            $data[$user->id]['user_name'] = $user->name;
            $data[$user->id]['reports'] = $monthNumbers;
            $data[$user->id]['chartValues'] = [0, 0, 0, 0, 0, 0];

            $lastMonthSale = 0;
            $currentMonthSale = 0;

            //get first landing page against single user
            
            //$landingPages = $this->getRetentionLandingPages($user->id);


            //get invoice of every landing page
            /*foreach ($landingPages as $landingPage) {
                $count = 0;
                $crntMonthCounted = false;
                $lastMonthCounted = false;
                foreach ($monthNumbers as $key => $month) {

                    $temp = explode(" ", $key);
                    $loopDate = $temp[1].'-'.Carbon::parse($temp[0])->format('m');
                    $yearMon = Carbon::parse($landingPage->created_at)->format('Y-m');

                    if ($yearMon == $crntMnth && $crntMonthCounted !== true) {
                        $crntMonthCounted = true;
                        $currentMonthSale = $currentMonthSale + $landingPage->cost;
                    }

                    if ($yearMon == $lastMnth && $lastMonthCounted !== true) {
                        $lastMonthCounted = true;
                        $lastMonthSale = $lastMonthSale + $landingPage->cost;
                    }


                    if ($yearMon == $loopDate) {
                        $data[$user->id]['reports'][$key] = $data[$user->id]['reports'][$key] + 1;
                        //$data[$user->id]['chartValues'][$count] = $data[$user->id]['reports'][$key];
                        //$data[$user->id]['chartValues'][$count] = $currentMonthSale;
                        $data[$user->id]['chartValues'][$count] = $data[$user->id]['chartValues'][$count] + $landingPage->cost;
                    }

                    $count++;
                }
            }*/


            //get first campaign against single user
            $campaigns = $this->getRetentionCampaigns($user->id);

            //get invoice of every campaign page
            foreach ($campaigns as $campaign) {
                $count = 0;
                $crntMonthCounted = false;
                $lastMonthCounted = false;
                foreach ($monthNumbers as $key => $month) {
                    $temp = explode(" ", $key);

                    $yearMon = Carbon::parse($campaign->created_at)->format('Y-m');
                    $loopDate = $temp[1].'-'.Carbon::parse($temp[0])->format('m');

                    if ($yearMon == $crntMnth && $crntMonthCounted !== true) {
                        $crntMonthCounted = true;
                        $currentMonthSale = $currentMonthSale + $campaign->amount;
                    }

                    if ($yearMon == $lastMnth && $lastMonthCounted !== true) {
                        $lastMonthCounted = true;
                        $lastMonthSale = $lastMonthSale + $campaign->amount;
                    }

                    if ($yearMon == $loopDate) {
                        $data[$user->id]['reports'][$key] = $data[$user->id]['reports'][$key] + 1;
                        // $data[$user->id]['chartValues'][$count] = $data[$user->id]['reports'][$key];
                        //$data[$user->id]['chartValues'][$count] = $currentMonthSale;
                        $data[$user->id]['chartValues'][$count] = $data[$user->id]['chartValues'][$count] + $campaign->amount;
                    }
                    $count++;
                }
            }

            //get first campaign against single user
            $projects = $this->getRetentionProjects($user->id);

            //get invoice of every campaign page
            foreach ($projects as $project) {
                $count = 0;
                $crntMonthCounted = false;
                $lastMonthCounted = false;
                foreach ($monthNumbers as $key => $month) {
                    $temp = explode(" ", $key);

                    $yearMon = Carbon::parse($project->created_at)->format('Y-m');
                    $loopDate = $temp[1].'-'.Carbon::parse($temp[0])->format('m');

                    if ($yearMon == $crntMnth && $crntMonthCounted !== true) {
                        $crntMonthCounted = true;
                        $currentMonthSale = $currentMonthSale + $project->price;
                    }

                    if ($yearMon == $lastMnth && $lastMonthCounted !== true) {
                        $lastMonthCounted = true;
                        $lastMonthSale = $lastMonthSale + $project->price;
                    }

                    if ($yearMon == $loopDate) {
                        $data[$user->id]['reports'][$key] = $data[$user->id]['reports'][$key] + 1;
                        //$data[$user->id]['chartValues'][$count] = $data[$user->id]['reports'][$key];
                        //$data[$user->id]['chartValues'][$count] = $currentMonthSale;
                        $data[$user->id]['chartValues'][$count] = $data[$user->id]['chartValues'][$count] + $project->price;
                    }
                    $count++;
                }
            }


            $data[$user->id]['lastMonthSale'] = $lastMonthSale;
            $data[$user->id]['currentMonthSale'] = $currentMonthSale;
        }

        if (request()->wantsJson() && request()->method() == "GET") {
            $this->_helper->response()->send(['data' => [
                'fields' => $months,
                'total' => $totalRecords,
                'limit' => $users->count(),
                'offset' => $offset + $limit,
                'lastoffset' => ($offset - $limit >= 0) ? $offset - $limit : 0,
                'data' => $data
            ]]);
        } else {
            $this->_helper->response()->send([
                'fields' => $months,
                'total' => $totalRecords,
                'limit' => $users->count(),
                'offset' => $offset + $limit,
                'lastoffset' => ($offset - $limit >= 0) ? $offset - $limit : 0,
                'data' => $data
            ]);
        }
    }


    //landing pages
    protected function getAllLandingPages($userId)
    {
        return \Illuminate\Support\Facades\DB::select("SELECT
                  lp.*
                FROM
                  `landing_pages` lp
                WHERE lp.`assigned_to` = ".$userId."
                  AND id IN
                  (SELECT
                    MIN(id)
                  FROM
                    landing_pages
                    WHERE `assigned_to` = ".$userId."
                  GROUP BY `user_id`)");

    }


    protected function getRetentionLandingPages($userId)
    {
        return \Illuminate\Support\Facades\DB::select("SELECT
                  lp.*
                FROM
                  `landing_pages` lp
                WHERE lp.`assigned_to` = ".$userId."
                  AND id NOT IN
                  (SELECT
                    MIN(id)
                  FROM
                    landing_pages
                    WHERE `assigned_to` = ".$userId."
                  GROUP BY `user_id`)");

    }


    //campaigns
    protected function getAllCampaigns($userId) {
        $query =    "SELECT 
                      lp.* 
                    FROM
                      `campaigns` lp 
                    WHERE lp.`assigned_id` = ".$userId." 
                      AND id IN 
                      (SELECT 
                        MIN(id) 
                      FROM
                        campaigns 
                      WHERE 1 = 1 
                      GROUP BY `user_id`)";
        return \Illuminate\Support\Facades\DB::select($query);
    }


    protected function getRetentionCampaigns($userId)
    {
        return \Illuminate\Support\Facades\DB::select("SELECT
                                    lp.*
                                    FROM
                                    `campaigns` lp
                                    WHERE lp.`assigned_id` = ".$userId."
                                    AND id NOT IN
                                    (SELECT
                                    MIN(id)
                                    FROM
                                    campaigns
                                    WHERE 1=1
                                    GROUP BY `user_id`)");
    }

    //Projects
    protected function getAllProjects($userId)
    {
        $query =    "SELECT 
                      op.*,
                      am.`assign_to` AS assigned_id 
                    FROM
                      `other_projects` op 
                      JOIN `assigned_modules` am 
                        ON op.`id` = am.`module_id` 
                    WHERE am.`module_type` = 'other_projects' 
                      AND am.`assign_to` = ".$userId." 
                      AND op.`id` IN 
                      (SELECT 
                        MIN(op.`id`) 
                      FROM
                        `other_projects` op 
                        JOIN `assigned_modules` am 
                          ON op.`id` = am.`module_id` 
                      WHERE am.`module_type` = 'other_projects' 
                      GROUP BY op.`user_id`)";
        return \Illuminate\Support\Facades\DB::select($query);
    }


    protected function getRetentionProjects($userId)
    {
        $query =    "SELECT 
                      op.*,
                      am.`assign_to` AS assigned_id 
                    FROM
                      `other_projects` op 
                      JOIN `assigned_modules` am 
                        ON op.`id` = am.`module_id` 
                    WHERE am.`module_type` = 'other_projects' 
                      AND am.`assign_to` = ".$userId." 
                      AND op.`id` NOT IN 
                      (SELECT 
                        MIN(op.`id`) 
                      FROM
                        `other_projects` op 
                        JOIN `assigned_modules` am 
                          ON op.`id` = am.`module_id` 
                      WHERE am.`module_type` = 'other_projects' 
                      GROUP BY op.`user_id`)";
        return \Illuminate\Support\Facades\DB::select($query);
    }
}

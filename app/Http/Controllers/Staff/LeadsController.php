<?php

namespace App\Http\Controllers\Staff;


use Auth;
use App\Leads;
use App\Campaign;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LeadsController extends Controller
{
    private $_helper;

    public function __construct(){
        $this->_helper = new Helper();
    }

    public function index($campain_id)
    {
        $campaign = Campaign::findOrFail($campain_id);
        $leads = new Leads();

        $leads = $leads->where('campaign_id', $campain_id)->orderBy('created_at', 'DESC');

        if(!$leads->exists()){
            $leads = [];
        }else{
            $leads = $leads->first();
        }

        $members = \App\User::where('type', 'staff')->get();
        $data['staff_members'] = $members;

        $data['campaign'] = $campaign;
        $data['leads'] = $leads;
        $data['page_title'] = "Leads";
        if (request()->wantsJson()) {
            // $data = ['campaign' => $campaign, 'is_retention' => $is_retention];
            $this->_helper->response()->send(['data' => $data]);
        } else {
        return view('staff.campaign.detail',$data);
        }
    }
}

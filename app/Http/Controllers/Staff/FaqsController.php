<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\Campaigns as CampaignsResource;
use App\Helpers\Helper;
use Auth;
use App\Campaign;
use App\User;
use App\LandingPage;

class FaqsController extends Controller
{
    private $_helper;

    public function __construct(){
        $this->_helper = new Helper();
        $this->middleware(function ($request, $next) {
            if( $this->_helper->isAllowedModule('faqs') == false)
		   		{
		   			return abort(404);
		   		}

            return $next($request);
        });
    }

    public function index(){
        $users = User::all();
  
        return view('staff.faqs.index');
    }
}

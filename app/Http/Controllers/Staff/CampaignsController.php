<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\Campaigns as CampaignsResource;
use App\Helpers\Helper;
use Auth;
use App\Campaign;
use App\User;
use App\LandingPage;
use App\CampaignComments;

class CampaignsController extends Controller
{
    private $_helper;

    public function __construct(){
        $this->_helper = new Helper();

        $this->middleware(function ($request, $next) {
            if( $this->_helper->isAllowedModule('campaigns') == false)
                {
                    return abort(404);
                }

            return $next($request);
        });
    }

    public function index(){
        $landing_pages = LandingPage::all();

        $users = User::all();

        return view('staff.campaign.index',[
            'landing_pages' => $landing_pages,
            'users' => $users,
        ]);
    }
    public function create(){
        $landing_pages = LandingPage::all();
        $users = User::all();

        return view('staff.campaign.create',[
            'landing_pages' => $landing_pages,
            'users' => $users,
        ]);
    }

    public function detail($id=''){
        $withArray = [];
        $withArray['user'] = function ($query) {
            $query->with('company');
            $query->with('business_info');
            $query->withTrashed();
        };

        if (request()->has('tab') && isset(request()->tab)) {
            if (request()->tab == 'notes') {
                $withArray['campaignComments'] = function ($query) {
                    $query->with([
                        'user' => function ($query1) {
                            $query1->withTrashed();
                        }
                    ]);
                };
            } elseif (request()->tab == 'assign_detail') {
                $withArray[] = 'campaignSpecialist';
                $withArray[] = 'salesPerson';
                $withArray[] = 'lpSpecialist';
                $withArray[] = 'accounManager';
            } elseif (request()->tab == 'installment') {
                $withArray[] = 'installments';
            }
        } else {
            $withArray[] = 'landing_pages';
            $withArray[] = 'campaignComments';
            $withArray[] = 'campaignSpecialist';
            $withArray[] = 'salesPerson';
            $withArray[] = 'lpSpecialist';
            $withArray[] = 'accounManager';
            $withArray[] = 'invoice';
            $withArray['invoice'] = function ($query1) {
                $query1->orderBy('id', 'DESC')->first();
            };
            $withArray[] = 'installments';
            $withArray[] = 'agreement';
        }

        $campaign = Campaign::with($withArray)->where('id', $id);
        if (!$campaign->exists()) {
            return abort(404);
        }
        $campaign = $campaign->first();
        $is_retention = Campaign::where('user_id', $campaign->user_id)->where('id', '<', $campaign->id)->count();
        if (request()->wantsJson()) {
            $data = ['campaign' => $campaign, 'is_retention' => $is_retention];
            $this->_helper->response()->send(['data' => $data]);
        } else {
            return view('staff.campaign.campaign_detail', compact('campaign', 'is_retention'));
        }
    }

    public function postComment()
    {
        $this->_helper->runValidation([
            'id' => 'required|exists:campaigns,id',
            'text' => 'required'
        ]);

        $request = request();

        CampaignComments::create([
            'campaign_id' => $request->id,
            'user_id' => Auth::user()->id,
            'description' => $request->text
        ]);

        $campaign = Campaign::with(['campaignComments'])->find($request->id);

        $this->_helper->response()->send(['data' => $campaign]);
    }
}

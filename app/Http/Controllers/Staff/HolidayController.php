<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\Campaigns as CampaignsResource;
use App\Helpers\Helper;
use Auth;
use App\Guide;
use App\User;
use App\LandingPage;

class HolidayController extends Controller
{
    private $_helper;

    public function __construct(){
        $this->_helper = new Helper();

        $this->middleware(function ($request, $next) {
            if( $this->_helper->isAllowedModule('holiday') == false)
                {
                    return abort(404);
                }

            return $next($request);
        });
    }

    public function index(){
    	$users=User::where('type','staff')->get();
        return view('staff.holiday.index',compact('users'));
    }
    public function guide()
    {
    	$guides=Guide::all();
         foreach ($guides as $key => $value) {
            $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i',$value['created_at']);
            $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', date('Y-m-d H:s:i'));
            $diff_in_minutes = $to->diffInMinutes($from);
            $msg='Updated '.floor($diff_in_minutes).' minutes ago';
            if($diff_in_minutes>60){
            $hours=$diff_in_minutes/60;
            $msg='Updated '.floor($hours).' hours ago';
            if($hours>12)
            {
            $days=$hours/12;
            $msg='Updated '.floor($days).' days ago';
            }
            }
            $value['msg'] =$msg;
        }
    	return view('staff.guide.guide',compact('guides'));
    }
}

<?php

namespace App\Http\Controllers\Staff;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
class StaffController extends Controller
{

    public function logout(Request $request) {
        Auth::logout();
        return redirect('/login');
    }
}

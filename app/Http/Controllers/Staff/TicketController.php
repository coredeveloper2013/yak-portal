<?php

namespace App\Http\Controllers\Staff;

use Carbon\Carbon;
use App\SupportTicket;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Helper;

class TicketController extends Controller {


   private $_helper;

   public function __construct(){
        $this->_helper = new Helper();

        $this->middleware(function ($request, $next) {
            if( $this->_helper->isAllowedModule('support_ticket') == false)
		   		{
		   			return abort(404);
		   		}

            return $next($request);
        });
        
    }
    
    public function index() {
    	$members = \App\User::where('type', 'staff')->get();
    	$data['staff_members'] = $members;
        return view('staff.support_ticket.index', $data);
    }
}

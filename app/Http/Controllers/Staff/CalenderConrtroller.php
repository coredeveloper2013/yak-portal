<?php

namespace App\Http\Controllers\Staff;
use App\Helpers\Helper;
use App\User;

use App\Categories;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CalenderConrtroller extends Controller
{
    private $_helper;

    public function __construct(){
        $this->_helper = new Helper();
    }

    public function index()
    {
        $user = \Auth::user();
        $categories = Categories::with(['assignedmodule' => function($q) use  ($user){
            if ($user->type == 'staff') {
                $q->where('assign_to',  $user->id);
            }
           }])->get();
        
        $users      = User::where('type' , 'staff')->get();
    	return view('staff.Calender.index' , [
    		'categories' => $categories,
            'users'      => $users,
            'page_title' => 'Calender'
    	]);
    }


    public function storeCategory ($value='')
    {
    	# code...
    }
}

<?php

namespace App\Http\Controllers;

use App\Guide;
use App\Helpers\Helper;

use Illuminate\Http\Request;
use Auth;
use App\Campaign;
use App\Invoice;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Gate;
class GuideController extends Controller
{
    protected $_helper;

    public function __construct()
    {
        $this->_helper = new Helper();
    }

    public function index()
    {
    	$guides=Guide::where('status',1)->get();
        foreach ($guides as $key => $value) {
                        $value['descriptionF']= strip_tags($value['description'],'<br>');

            $value['msg'] =\Carbon\Carbon::createFromTimeStamp(strtotime($value['created_at']))->diffForHumans() ;
        }
        if (request()->wantsJson()) {
            $this->_helper->response()->setCode(200)->send(['data' => ['guides' => $guides]]);
        } else
            return view('guide.index', ['page_title' => 'Guide', 'guidesJ' =>  json_encode($guides), 'guides' => $guides]);
    }
    public function get(Request $request)
    {
        $data = [
            'status' => 'success',
            'msg' => 'Success.',
            'data' => [],
        ];
        
        
        $guides = Guide::all();
            foreach ($guides as $key => $value) {
            $value['descriptionF']= strip_tags($value['description'],'<br>');
            $value['image']= url($value['image']);
                $value['msg'] =\Carbon\Carbon::createFromTimeStamp(strtotime($value['created_at']))->diffForHumans() ;
            }
         $data['data']['guides'] =   ($guides);
         
         $data['data']['isAdmin']=Gate::allows('isAdmin');
        $this->_helper->response()->setCode(200)->send($data);
    }
}

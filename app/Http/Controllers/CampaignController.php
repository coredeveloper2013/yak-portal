<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Invoice;
use Carbon\Carbon;
use App\Agreement;
use App\Notification;
use App\Helpers\Helper;
use App\AgreementDocuSign;
use Illuminate\Support\Facades\Validator;

class CampaignController extends Controller
{
    protected $_helper;

    public function __construct()
    {
        $this->_helper = new Helper();
    }

    public function index()
    {
        $request = request();

        $campaigns = Auth::user()->campaigns();

        if($request->has('filter')){
            if (!empty($request->filter['selectedStartDate']) && !empty($request->filter['selectedEndDate'])) {
                $endDate = Carbon::parse($request->filter['selectedEndDate'])->format('Y-m-d ').'23:59:59';
                $startDate = Carbon::parse($request->filter['selectedStartDate'])->format('Y-m-d H:i:s');
                $campaigns = $campaigns->whereBetween('created_at', [$startDate, $endDate]);
            }

            if (!empty($request->filter['status'])) {
                if ($request->filter['status'] == 'live') {
                    $searchArr = ['live', 'billing_detail'];
                } else if ($request->filter['status'] == 'pending') {
                    $searchArr = ['pending', 'awaiting'];
                } else {
                    $searchArr = ['followed_up', 'completed'];
                }
                $campaigns = $campaigns->whereIn('status', $searchArr);
            }
        }


        $campaigns = $campaigns->orderBy('created_at', 'DESC')->paginate();

        $this->_helper->response()->send(['data' => $campaigns]);
    }

    public function create()
    {
        $landing_pages = Auth::user()->landing_pages()->get();

        return view('campaigns.create', ['landing_pages' => $landing_pages, 'page_title' => 'Create Campaign']);
    }

    public function add()
    {
        dd('CampaignController Line No. '.__LINE__);
        $request = request();

        $messages = [
            'amount.min' => 'Please enter positive integer'
        ];

        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            //'landing_id' => 'required|exists:landing_pages,id',
            'amount' => 'required|numeric|min:0|not_in:0',
            'status' => 'required|in:completed,pending',
            //'repeat' => 'required'
        ], $messages);


        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $campaign = \App\Campaign::create([
            'user_id' => Auth::user()->id,
            'name' => $request->name,
            'start_date' => Carbon::parse($request->start_date)->format('m/d/Y'),
            'end_date' => Carbon::parse($request->end_date)->format('m/d/Y'),
            'landing_pages_id' => ($request->landing_id) ? $request->landing_id : null,
            'amount' => $request->amount,
            'type' => $request->type,
            'status' => $request->status,
//            'repeat' => $request->repeat
        ]);

        Invoice::create([
            'user_id' => Auth::user()->id,
            'type' => 'campaign',
            'status' => 'pending',
            'campaign_id' => $campaign->id,
            'landing_pages_id' => null
        ]);

        Notification::create([
            'user_id' => Auth::user()->id,
            'campaign_id' => $campaign->id,
            'type' => 'sms',
            'for' => 'customer'
        ]);
        Notification::create([
            'user_id' => Auth::user()->id,
            'campaign_id' => $campaign->id,
            'type' => 'email',
            'for' => 'customer'
        ]);

        $agreement = Agreement::create([
            'user_id' => Auth::user()->id,
            'campaign_id' => $campaign->id,
        ]);

        $agreementSign = AgreementDocuSign::create([
            'user_id' => Auth::user()->id,
            'module_id' => $agreement->id,
            'type' => 'agreements',
        ]);

        return redirect()->route('campaigns');

    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OtherProjectController extends Controller {
    public function index() {
        return view('other_projects.index', ['page_title' => 'Projects']);
    }

    public function create() {
        return view('other_projects.order', ['page_title' => 'Create Projects']);
    }
}

<?php

namespace App\Http\Controllers;

use Auth;
use Storage;
use App\User;
use App\Company;
use App\Helpers\Helper;
use App\UserBusinessInformation;
use Illuminate\Support\Facades\Validator;
//use Illuminate\Support\Facades\Storage;
use App\Exports\LogsExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ClientController extends Controller
{
    protected $_helper;
    protected $withRelation;
    public function __construct()
    {
        $this->_helper = new Helper();

        $this->middleware(function ($request, $next) {
            if ($this->_helper->isAllowedModule('clients_manager') == false) {
                return abort(404);
            }

            return $next($request);
        });

        $this->withRelation = [
            'company',
            'business_info',
            'campaigns',
            'landing_pages',
            'other_projects',
            'request_logs', 'request_logs.logDetails'
        ];
    }

    public function index()
    {
        $request = request();

        $users = new \App\User();

        $users = $users->whereType('customer');

        if ($request->filter['status']) {
            $users = $users->whereStatus($request->filter['status']);
        }

        if ($request->filter['queryString']) {
            $users = $users->where("name", "LIKE", "%" . $request->filter['queryString'] . "%");
        }

        $data = ['data' => $users->with($this->withRelation)->orderBy('created_at', 'DESC')->get()];

        $this->_helper->response()->send($data);
    }
    public function store()
    {
        /*if (request()->has('userId') && request()->userId) {
            $msg = 'Client Updated Successfully.';
            $validator = Validator::make(request()->all(), [
                'name' => 'bail|required',
                //'job_position' => 'bail|required',
                'mobile' => 'bail|required|min:10|numeric|unique:users,phone,' . request()->userId,
                'email' => 'bail|required|email|unique:users,email,' . request()->userId,
                //'password' => 'bail|required|min:8|confirmed',
                //'work_email' => 'bail|nullable|email|unique:user_business_information,email_address,null,' . request()->userId . ',user_id,',
                //'work_phone' => 'bail|nullable|min:10|numeric|unique:user_business_information,phone,null,' . request()->userId . ',user_id,',
                //'address' => 'bail|nullable',
                'website' => 'bail|nullable|url',
                'facebook_link' => 'bail|nullable|url',
                'twitter_link' => 'bail|nullable|url',
                'Instagram_link' => 'bail|nullable|url',
                'company' => 'bail|required|min:3'
            ], [
                'name.required' => 'Please enter name',
                'mobile.required' => 'Please enter phone number',
                //'job_position.required' => 'Please enter job position',
                'numeric' => 'Only numbers are allowed',
                'email' => 'Please enter valid email address',
                'url' => 'Please enter valid url',
                'work_email.email' => 'Please enter valid email address',
            ]);
        } else {
            $msg = 'Client Created Successfully.';
            $validator = Validator::make(request()->all(), [
                'name' => 'bail|required',
                //'job_position' => 'bail|required',
                'mobile' => 'bail|required|min:10|numeric|unique:users,phone',
                'email' => 'bail|required|email|unique:users,email',
                'password' => 'bail|required|min:8|confirmed',
                //'work_email' => 'bail|nullable|email|unique:user_business_information,email_address',
                //'work_phone' => 'bail|nullable|min:10|numeric|unique:user_business_information,phone',
                //'address' => 'bail|nullable',
                'website' => 'bail|nullable|url',
                'facebook_link' => 'bail|nullable|url',
                'twitter_link' => 'bail|nullable|url',
                'Instagram_link' => 'bail|nullable|url',
                'company' => 'bail|required|min:3'
            ], [
                'name.required' => 'Please enter name',
                'mobile.required' => 'Please enter phone number',
                //'job_position.required' => 'Please enter job position',
                'numeric' => 'Only numbers are allowed',
                'email' => 'Please enter valid email address',
                'url' => 'Please enter valid url',
                'work_email.email' => 'Please enter valid email address',
            ]);
        }

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }*/
        dd( request());
        $request = request();



        $updateOrCreateWhereArr = [
            'id' => $request->userId
        ];

        $updateOrCreateArr = [
            'name' => $request->name,
            //'job_position' => $request->job_position,
            'phone' => $request->mobile,
            'email' => $request->email,
            //'status' => $request->status
        ];

        if (empty($request->userId)) {
            $updateOrCreateArr['password'] = bcrypt($request->password);
        }

        $user = User::updateOrCreate($updateOrCreateWhereArr, $updateOrCreateArr);

        Company::updateOrCreate(
            [
                'user_id' => $user->id
            ],
            [
                //'address' => ($request->address) ? $request->address : null,
                'name' => $request->company
            ]
        );

        UserBusinessInformation::updateOrCreate(
            [
                'user_id' => $user->id
            ],
            [
                //'email_address' => ($request->work_email) ? $request->work_email : null,
                //'phone' => ($request->work_phone) ? $request->work_phone : null,
                //'address' => ($request->address) ? $request->address : null,
                'trading_name' => ($request->trading_name) ? $request->trading_name : null,
                'recurring_way' => ($request->recurring_way) ? $request->recurring_way : null,
                'recurring_amount' => ($request->recurring_amount) ? $request->recurring_amount : null,
                'website' => ($request->website) ? $request->website : null,
                'facebook_link' => ($request->facebook_link) ? $request->facebook_link : null,
                'twitter_link' => ($request->twitter_link) ? $request->twitter_link : null,
                'Instagram_link' => ($request->Instagram_link) ? $request->Instagram_link : null,
                'notes' => ($request->notes) ? $request->notes : null
            ]
        );

        return redirect()->back()->with('success', $msg);
    }

    public function removeClient()
    {
        if (Auth::user()->type != 'admin') {
            $data = [
                'status' => 'error',
                'msg' => 'You are not authorized to do this action',
                'data' => [],
            ];
            $this->_helper->response()->setCode(200)->send($data);
        }

        $request = request();

        $user = new \App\User();

        $user = $user->find($request->id);

        $user->delete();

        $user->save();

        $this->_helper->response()->send('');
    }

    public function exportClient()
    {
        $request = request();
        $filename = 'logs' . time() . '.xlsx';
        //$path1 = public_path('storage/exports/') .$filename;

        $dir = 'exports';
        $path1 = $dir . '/' . $filename;
        if (!Storage::exists($dir)) {
            Storage::makeDirectory($dir, 0775, true); //creates directory
        }


        Excel::store(new LogsExport($request), $path1);
        $path = ENV('APP_URL') . 'storage/exports/' . $filename;
        //dd($path);
        $this->_helper->response()->send($path);
        //return Excel::download(new UsersExport, 'logs.xlsx');
    }

    /**
     *
     * APIS FOR CLIENT MANAGER
     */
    public function getAllClients(Request $request)
    {
        $this->_helper->runValidation([
            'user_type' => 'nullable|in:recurring,new,gold,lost'
        ]);
        $clients = User::with('company:user_id,name')->select('id', 'name', 'type', 'status', 'image')->where('type', 'customer');
        if ($request->has('user_type') && isset($request->user_type)) {
            $clients = $clients->where('status', $request->user_type);
        }

        if ($request->has('search') && !empty($request->search)) {
            $clients = $clients->where("name", "LIKE", "%" . $request->search . "%");
        }
        $clients = $clients->orderBy('created_at', 'DESC')->paginate();
        $this->_helper->response()->setCode(200)->send($clients);
    }
    public function getClient(User $user)
    {
        $this->_helper->runValidation([
            'tab' => 'nullable|in:contactinfo,campaigns,landingpages,projects'
        ]);
        $client = $user;
        $user_id = $user->id;
        $withArray = null;
        if (request()->has('tab') && isset(request()->tab)) {
            if (request()->tab == 'contactinfo') {
                $withArray = ['company', 'business_info'];
            } elseif (request()->tab == 'campaigns') {
                $campaigns = User::find($user_id)->campaigns()->paginate();
                $client->campaigns = $campaigns;
            } elseif (request()->tab == 'landingpages') {
                $landing_pages = User::find($user_id)->landing_pages()->paginate();
                $client->landing_pages = $landing_pages;
            } else {
                $other_projects = User::find($user_id)->other_projects()->paginate();
                $client->other_projects = $other_projects;
            }
        } else {
            $withArray = [
                'company',
                'business_info',
                'campaigns',
                'landing_pages',
                'other_projects',
            ];
        }
        if (isset($withArray)) {
            $client = User::with($withArray)->where('id', $user_id)->first();
        }

        $this->_helper->response()->setCode(200)->send($client);
    }
    public function updateClient(Request $request)
    {
        $message = null;

        if (request()->has('userId') && isset(request()->userId)) {
            $message = 'Client Updated Successfully.';
            $this->_helper->runValidation([
                'userId' => 'required|exists:users,id',
                'name' => 'bail|required',
                'job_position' => 'bail|required',
                'mobile' => 'bail|required|min:10|numeric|unique:users,phone,' . request()->userId,
                'email' => 'bail|required|email|unique:users,email,' . request()->userId,
                //'password' => 'bail|required|min:8|confirmed',
                'work_email' => 'bail|nullable|email|unique:user_business_information,email_address,null,' . request()->userId . ',user_id,',
                'work_phone' => 'bail|nullable|min:10|numeric|unique:user_business_information,phone,null,' . request()->userId . ',user_id,',
                'address' => 'bail|nullable',
                'website' => 'bail|nullable|url',
                'facebook_link' => 'bail|nullable|url',
                'twitter_link' => 'bail|nullable|url',
                'Instagram_link' => 'bail|nullable|url',
                'company' => 'bail|required|min:3'
            ], [
                'name.required' => 'Please enter name',
                'mobile.required' => 'Please enter phone number',
                'job_position.required' => 'Please enter job position',
                'numeric' => 'Only numbers are allowed',
                'email' => 'Please enter valid email address',
                'url' => 'Please enter valid url',
                'work_email.email' => 'Please enter valid email address',
            ]);
        } else {
            $message = 'Client Created Successfully.';

            $this->_helper->runValidation([
                'name' => 'required',
                'job_position' => 'required',
                'mobile' => 'required|min:10|numeric|unique:users,phone',
                'email' => 'required|email|unique:users,email',
                'password' => 'required|min:8|confirmed',
                'work_email' => 'nullable|email|unique:user_business_information,email_address',
                'work_phone' => 'nullable|min:10|numeric|unique:user_business_information,phone',
                'address' => 'nullable',
                'website' => 'nullable|url',
                'facebook_link' => 'nullable|url',
                'twitter_link' => 'nullable|url',
                'Instagram_link' => 'nullable|url',
                'company' => 'required|min:3',
                'status' => 'required|in:recurring,new,gold,lost'

            ], [
                'name.required' => 'Please enter name',
                'mobile.required' => 'Please enter phone number',
                'job_position.required' => 'Please enter job position',
                'numeric' => 'Only numbers are allowed',
                'email' => 'Please enter valid email address',
                'url' => 'Please enter valid url',
                'work_email.email' => 'Please enter valid email address',
            ]);
        }

        try {
            // $message = 'Client Successfully Added';
            DB::beginTransaction();
            $updateOrCreateWhereArr = ['id' => $request->userId];
            $updateOrCreateArr = [
                'name' => $request->name,
                'job_position' => $request->job_position,
                'phone' => $request->mobile,
                'email' => $request->email,
                'status' => $request->status
            ];

            if (empty($request->userId)) {
                $updateOrCreateArr['password'] = bcrypt($request->password);
            }

            $user = User::updateOrCreate($updateOrCreateWhereArr, $updateOrCreateArr);

            Company::updateOrCreate(
                ['user_id' => $user->id],
                [
                    'address' => ($request->address) ? $request->address : null,
                    'name' => $request->company
                ]
            );

            UserBusinessInformation::updateOrCreate(
                ['user_id' => $user->id],
                [
                    'email_address' => ($request->work_email) ? $request->work_email : null,
                    'phone' => ($request->work_phone) ? $request->work_phone : null,
                    'address' => ($request->address) ? $request->address : null,
                    'website' => ($request->website) ? $request->website : null,
                    'facebook_link' => ($request->facebook_link) ? $request->facebook_link : null,
                    'twitter_link' => ($request->twitter_link) ? $request->twitter_link : null,
                    'Instagram_link' => ($request->Instagram_link) ? $request->Instagram_link : null,
                    'notes' => ($request->notes) ? $request->notes : null
                ]
            );

            DB::commit();
            $data = [
                'status' => 'success',
                'msg' => $message,
            ];
            $this->_helper->response()->setCode(200)->send($data);
        } catch (\Throwable $th) {
            DB::rollback();
            $data = [
                'status' => 'false',
                'msg' => $th->getMessage()
            ];
            $this->_helper->response()->setCode(200)->send($data);
            //throw $th;
        }
    }
    public function deleteClient($user_id)
    {
        if (Auth::user()->type != 'admin') {
            $data = [
                'status' => 'error',
                'msg' => 'You are not authorized to do this action',
                'data' => [],
            ];
            $this->_helper->response()->setCode(401)->send($data);
        }

        $client = User::find($user_id);
        if ($client) {
            $client->delete();
            $data = [
                'status' => 'success',
                'msg' => 'Client Removed Successfully',
            ];
            $this->_helper->response()->setCode(200)->send($data);
        } else {
            $data = [
                'status' => 'false',
                'msg' => 'Record Not Found',
            ];
            $this->_helper->response()->setCode(200)->send($data);
        }
    }
}

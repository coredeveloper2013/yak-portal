<?php

namespace App\Http\Controllers;

use Auth;
use Stripe;
use App\Agreement;
use App\Invoice;
use Carbon\Carbon;
use App\Helpers\Helper;
use Laravel\Cashier\Cashier;
use PDF;
use Illuminate\Support\Facades\Validator;

class BillingController extends Controller
{
    protected $_helper;

    protected $_request;

    public function __construct()
    {
        $this->_helper = new Helper();
        $this->_request = request();

    }

    public function getUserInvoices()
    {
        $invoices = Auth::user()->invoices()->with([
            'campaign' => function ($query) {
                $query->withTrashed();
            }, 
            'landing_page', 
            'other_project'
        ])->orderBy('id', 'DESC')->paginate();
        $this->_helper->response()->send(['data' => $invoices]);
    }

    /**
     * Creates an intent for payment so we can capture the payment
     * method for the user.
     *
     * @param Request $request The request data from the user.
     */
    public function getSetupIntent()
    {
        return $this->_request->user()->createSetupIntent();
    }


    /**
     * Updates a subscription for the user
     *
     * @param Request $request The request containing subscription update info.
     */
    public function updateSubscription()
    {
        $request = $this->_request;
        $user = $request->user();
        $planID = $request->get('plan');
        $paymentID = $request->get('payment');

        if (!$user->subscribed('ppc')) {
            $user->newSubscription('ppc', $planID)
                ->create($paymentID);
        } else {
            $user->subscription('ppc')->swap($planID);
        }

        return response()->json([
            'subscription_updated' => true
        ]);
    }

    /**
     * Adds a payment method to the current user.
     *
     * @param Request $request The request data from the user.
     */
    public function postPaymentMethods()
    {
        $request = $this->_request;
        $user = $request->user();
        $paymentMethodID = $request->get('payment_method');

        if ($user->stripe_id == null) {
            $user->createAsStripeCustomer();
        }

        $user->addPaymentMethod($paymentMethodID);
        $user->updateDefaultPaymentMethod($paymentMethodID);

        return response()->json(null, 204);
    }

    /**
     * Returns the payment methods the user has saved
     *
     * @param Request $request The request data from the user.
     */
    public function getPaymentMethods()
    {
        $request = $this->_request;
        $user = $request->user();

        $methods = array();

        if ($user->hasPaymentMethod()) {
            foreach ($user->paymentMethods() as $method) {
                array_push($methods, [
                    'id' => $method->id,
                    'brand' => $method->card->brand,
                    'last_four' => $method->card->last4,
                    'exp_month' => $method->card->exp_month,
                    'exp_year' => $method->card->exp_year,
                ]);
            }
        }

        return response()->json($methods);
    }

    /**
     * Removes a payment method for the current user.
     *
     * @param Request $request The request data from the user.
     */
    public function removePaymentMethod()
    {
        $request = $this->_request;
        $user = $request->user();
        $paymentMethodID = $request->get('id');

        $paymentMethods = $user->paymentMethods();

        foreach ($paymentMethods as $method) {
            if ($method->id == $paymentMethodID) {
                $method->delete();
                break;
            }
        }

        return response()->json(null, 204);
    }

    public function chargeCard()
    {
        $user = Auth::user();
        $invoice = new \App\Invoice();
        $invoice = $invoice->find(request()->id);
        if (empty($invoice)) {
            $data = [
                'status' => 'error',
                'msg' => 'Invalid Invoice Id',
            ];

            $this->_helper->response()->setCode(200)->send($data);
        }

        if ($invoice->deleted_parent == 'Y') {
            $data = [
                'status' => 'error',
                'msg' => $invoice->typeArr[$invoice->type].' is deleted of this invoice',
            ];
            $this->_helper->response()->setCode(200)->send($data);
        }

        if (strtolower($invoice->status) != "pending") {
            $data = [
                'status' => 'error',
                'msg' => 'Invoice Already Paid',
            ];

            $this->_helper->response()->setCode(200)->send($data);
        }
        $stripeSecret = getenv("STRIPE_SECRET");
        Stripe\Stripe::setApiKey($stripeSecret);
        //Stripe\Stripe::setApiKey('sk_test_IASGr1XRw5losTOlqxxyPeIL00hKxC0Gd6');

        if (empty($user->stripe_id)) {
            //$customer =  request()->user()->createOrGetStripeCustomer();

            $customer = Stripe\Customer::create([
                            'email' =>  $user->email,
                            'source'  => request()->token['id']
                        ]);

            $customerId = $customer->id;
        } else {
            $customerId = $user->stripe_id;
        }


        $chargeRes = Stripe\Charge::create([
            'customer' => $customerId,
            'amount' => request()->amount * 100,
            'currency' => 'gbp',
            //'source' => request()->token['id'],
            'description' => 'Test payment'
        ]);

        if($chargeRes->amount_refunded == 0 && empty($chargeRes->failure_code) && $chargeRes->paid == 1 && $chargeRes->captured == 1) {

            if (empty($user->stripe_id)) {
                $user->stripe_id = $customerId;
                $user->card_brand = request()->token['card']['brand'];
                $user->card_last_four = request()->token['card']['last4'];
                $user->trial_ends_at = request()->token['card']['exp_year'].'-'.request()->token['card']['exp_month'].'-01';
                $user->save();
            }

           
            $invoice->status = 'completed';
            $invoice->response = json_encode($chargeRes);
            $invoice->paid_date = Carbon::now()->format('Y-m-d H:i:s');
            $invoice->save();

            if (request()->type == 'campaign') {
                $status = "pending";
                $agreements = new Agreement();
                $agreements = $agreements->where('campaign_id', $invoice->campaign_id);

                if ($agreements->exists()) {
                    $agreements = $agreements->first();
                    if (strtolower($agreements->status) == 'completed') {
                        $status = "awaiting";
                    }
                }

                /*$data = [
                    'status' => 'error',
                    'msg' => $status,
                    'data' => $agreements,
                ];
                $this->_helper->response()->setCode(200)->send($data);*/

                $campaign = new \App\Campaign();
                $campaign = $campaign->withTrashed()->find($invoice->campaign_id);
                $campaign->status = $status;
                $campaign->save();
            } else if (request()->type == 'other_project') {
                /*$othrProjct = new \App\OtherProject();
                $othrProjct = $othrProjct->find($invoice->other_project_id);
                $othrProjct->status = 'completed';
                $othrProjct->save();*/
            } else if (request()->type == 'landing_page') {
                /*$landing = new \App\LandingPage();
                $landing = $landing->find($invoice->landing_pages_id);
                $landing->status = 'completed';
                $landing->save();*/
            }

            $data = [
                'status' => 'success',
                'msg' => 'Payment Successful.',
            ];
        } else {
            $data = [
                'status' => 'error',
                'msg' => 'Something Went Wrong',
            ];
        }

        $this->_helper->response()->setCode(200)->send($data);
    }

    public function updateCompany()
    {
        $request = $this->_request;

        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $company = Auth::user()->company;

        $company->name = $request->name;

        $company->address = $request->address;

        $company->save();

        return redirect()->back()->with('success', 'Company Updated.');

    }


    public function invoiceDetail($id)
    {
        //$invoices = Invoices()->with(['campaign', 'landing_page', 'other_project'])->orderBy('id', 'DESC')->paginate();
        $invoice = Invoice::with([
            'campaign' => function ($query) {
                $query->withTrashed();
            }, 
            'landing_page', 
            'other_project'
        ])->find($id);
        if (empty($invoice)) {
            if (request()->wantsJson()) {
                $data = [];
                $this->_helper->response()->setCode(200)->send($data);
            } else {
                return redirect()->back()->withErrors([]);
            }
        }

        if (request()->wantsJson()) {
            $data = ['invoice' => $invoice];
            $this->_helper->response()->setCode(200)->send(['data='=>$data]);
        } else {
            $stripePub = getenv("STRIPE_KEY");
            $data = ['invoice' => $invoice, 'invoice_json' => json_encode($invoice), 'page_title' => 'Invoice #' . $id, 'stripe_pub' => $stripePub];
            return view('billing.detail', $data);
        }

    }

    public function donloadInvoice($id) {
        //$invoice = Invoice::with(['campaign', 'landing_page', 'other_project'])->find($id);
        $invoice = Auth::user()->invoices()->with([
            'campaign' => function ($query) {
                $query->withTrashed();
            }, 
            'landing_page', 
            'other_project', 
            'user.business_info'
        ])->where('id', $id);
        if (!$invoice->exists()) {
            return redirect()->back()->withErrors([]);
        }
        $invoice = $invoice->first();
        $pdf = PDF::loadView('billing.download', ['invoice' => $invoice, 'page_title' => 'Download Invoice']);
        return $pdf->download('invoice.pdf');
        //return view('billing.download',['invoice' => $invoice, 'page_title' => 'Download Invoice']);
    }

}

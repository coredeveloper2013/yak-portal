<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Agreement;
use Auth;
class AgreementController extends Controller
{

    protected $_helper;

    public function __construct()
    {
        $this->_helper = new Helper();
    }


    public function index(){

        $request = request();

        $agreements = new Agreement();
        $agreements = $agreements->with([
            'user', 
            'campaign' => function ($query) {
                $query->withTrashed();
            }, 
            'document' => function ($query) {
                $query->where('type', 'agreements');
            }
        ]);
        //$agreements = Auth::user()->agreements();

        if($request->has('filter')){
            if ($request->filter['selectedStartDate'] !== null && $request->filter['selectedEndDate'] !== null) {
                $endDate = Carbon::parse($request->filter['selectedEndDate'])->format('Y-m-d ').'23:59:59';
                $startDate = Carbon::parse($request->filter['selectedStartDate'])->format('Y-m-d H:i:s');
                $agreements = $agreements->whereBetween('created_at', [$startDate, $endDate]);
            }

            if ($request->filter['status'] !== null) {
                $agreements = $agreements->where('status', $request->filter['status']);
            }
        }
        $agreements = $agreements->where('user_id', Auth::user()->id);


        $agreements = $agreements->orderBy('created_at', 'DESC')->paginate();

        $this->_helper->response()->send(['data' => $agreements]);
    }

    public function add(){

        $request = request();

        $messages = [
            'amount.min' => 'Please enter positive integer'
        ];

        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'date' => 'required|date',
            'amount' => 'required|numeric|min:0|not_in:0',
            'status' => 'required|in:completed,pending'
        ], $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        \App\Agreement::create([
            'user_id' => Auth::user()->id,
            'name' => $request->name,
            'date' => Carbon::parse($request->date)->format('m/d/Y'),
            'amount' => $request->amount,
            'status' => $request->status
        ]);

        return redirect()->route('agreements');
    }
}

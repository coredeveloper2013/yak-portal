<?php

namespace App\Http\Controllers;


use App\Invoice;
use App\LandingQuestionAnswers;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Helpers\Helper;
use DB;

class LandingPageController extends Controller
{
    protected $_helper;

    public function __construct()
    {
        $this->_helper = new Helper();
    }


    public function index()
    {

        return view('landing.order', ['page_title' => 'Order a Landing Page']);
    }

    public function listing()
    {
        $request = request();

        $pages = Auth::user()->landing_pages();

        if($request->has('filter')){
            if ($request->filter['selectedStartDate'] !== null && $request->filter['selectedEndDate'] !== null) {
                $endDate = Carbon::parse($request->filter['selectedEndDate'])->format('Y-m-d ').'23:59:59';
                $startDate = Carbon::parse($request->filter['selectedStartDate'])->format('Y-m-d H:i:s');
                $pages = $pages->whereBetween('created_at', [$startDate, $endDate]);
            }


            if ($request->filter['status'] !== null) {
                $pages = $pages->where('status', $request->filter['status']);
            }
        }

        $pages = $pages->orderBy('created_at', 'DESC')->paginate();

        $this->_helper->response()->setCode(200)->send(['data' => $pages]);

//        $result = Auth::user()->landing_pages()->orderBy('created_at', 'DESC')->get();
//        return view('landing.index', compact('result'));
    }

    public function save()
    {
        $request = request();

        $messages = [
            'amount.min' => 'Please enter positive integer',
            'reasons.0.required' => 'Please enter first reason',
            'reasons.1.required' => 'Please enter second reason',
            'reasons.2.required' => 'Please enter third reason',
            'reviews.0.required' => 'Please enter first review',
            'reviews.1.required' => 'Please enter second review',
            'landing_page.0.questions.required' => 'Please enter question # 1',
            'landing_page.1.questions.required' => 'Please enter question # 2',
            'landing_page.2.questions.required' => 'Please enter question # 3',
            'landing_page.3.questions.required' => 'Please enter question # 4',
            'landing_page.0.answers.required' => 'Please enter answer # 1',
            'landing_page.1.answers.required' => 'Please enter answer # 2',
            'landing_page.2.answers.required' => 'Please enter answer # 3',
            'landing_page.3.answers.required' => 'Please enter answer # 4',
        ];

        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'logo' => 'bail|required|image|max:800',  //dimensions:min_width:300,min_height:300,max_width:1000,max_height:1000
            'cost' => 'required|numeric|min:0|not_in:0',
            'description' => 'required',
            'url' => 'required|url',
            'status' => 'required',
//            'date' => 'required',
//            'trying_to_sell' => 'required',
//            'customer_benefit' => 'required',
//            'leads_questions' => 'required|array|size:2',
//            'leads_contact_details' => 'required|array',
//            'review_pages' => 'required|array',
//            'reasons.*' => 'required',
//            'reviews.*' => 'required',
//            'landing_page.*.questions' => 'required',
//            'landing_page.*.answers' => 'required',
        ], $messages);


        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator)->withInput();
        }


        $image = null;
        if($request->hasFile('logo')){
            //save user image
            $image = Auth::user()->id.'-logo'.$request->file('logo')->getClientOriginalName();
            $ext = $request->file('logo')->getClientOriginalExtension();

            if(Storage::exists('landing_logo/'.$image)){
                Storage::delete('landing_logo/'.$image);
            }

            $path = $request->file('logo')->storeAs(
                'landing_logo', $image
            );
        }


        $objLanding = new \App\LandingPage();
        $objLanding->logo = $image;
        $objLanding->user_id = Auth::user()->id;
        $objLanding->name = $request->name;
        $objLanding->cost = $request->cost;
        $objLanding->description = $request->description;
        $objLanding->url = $request->url;
        $objLanding->status = $request->status;
        $objLanding->date = Carbon::now()->format('m/d/Y');
        $objLanding->trying_to_sell = ($request->trying_to_sell) ? $request->trying_to_sell : null;
        $objLanding->customer_benefit = ($request->customer_benefit) ? $request->customer_benefit : null;
        $objLanding->leads_questions = $request->leads_questions;
        $objLanding->leads_contact_details = ($request->leads_contact_details) ? $request->leads_contact_details : null;
        $objLanding->review_pages = $request->review_pages;
        $objLanding->reasons = $request->reasons;
        $objLanding->reviews = $request->reviews;
        $objLanding->save();


        Invoice::create([
            'user_id' => Auth::user()->id,
            'type' => 'landing_page',
            'status' => 'pending',
            'campaign_id' => null,
            'landing_pages_id' => $objLanding->id
        ]);

        foreach ($request->landing_page as $landingPage){
            if(!empty($landingPage['questions'])){
                LandingQuestionAnswers::create([
                    'landing_pages_id' => $objLanding->id,
                    'question' => $landingPage['questions'],
                    'answer' => $landingPage['answers'],
                ]);
            }
        }

        return redirect()->route('landing');

    }

    public function edit()
    {
        $request = request();

        $messages = [
            'amount.min' => 'Please enter positive integer'
        ];

        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'cost' => 'required|numeric|min:0|not_in:0',
            'description' => 'required',
            'url' => 'required|url|unique:landing_pages,url,'.Auth::user()->id,
            'status' => 'required',
            'date' => 'required'
        ], $messages);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        //  $objLanding=new \App\LandingPage();
//         $objLanding['user_id']= Auth::user()->id;
        $objLanding['name'] = $request->name;
        $objLanding['cost'] = $request->cost;
        $objLanding['description'] = $request->description;
        $objLanding['url'] = $request->url;
        $objLanding['status'] = $request->status;
        $objLanding['date'] = \Carbon\Carbon::parse($request->date);
        \App\LandingPage::where('id', $request->id)->update($objLanding);

        return redirect()->back()->with('success', 'updated successfully!');
    }


    public function editIndex($id)
    {

        $result = \App\LandingPage::find($id);
        return view('landing.edit', compact('result'));
    }


}

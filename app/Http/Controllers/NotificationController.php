<?php

namespace App\Http\Controllers;

use App\Notification;
use App\NotificationAlert;
use App\Services\NotificationAlertService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Helper;
use Auth;

class NotificationController extends Controller
{
    protected $_helper;

    public function __construct()
    {
        $this->_helper = new Helper();
        $this->items1 = 20;
        $this->items2 = 20;

    }

    public function index(Request $request)
    {
        //        $businessSms = Notification::where('type', 'sms')->where('for', 'business')->get();
        $items1 = request()->filled('items1') ? request()->items1 : $this->items1;
        $items2 = request()->filled('items2') ? request()->items2 : $this->items2;

        if ($request->sort == 'desc') {
            $businessSms = (new NotificationAlertService($request))->getAlertSMS()->latest()->paginate($items1, ['*'], 'page1');
        } else $businessSms = (new NotificationAlertService($request))->getAlertSMS()->paginate($items1, ['*'], 'page1');


        if ($request->sort2 == 'desc') {
            $businessEmail = (new NotificationAlertService($request))->getAlertEmail()->latest()->paginate($items2, ['*'], 'page2');
        } else $businessEmail = (new NotificationAlertService($request))->getAlertEmail()->paginate($items2, ['*'], 'page2');

//        $businessSms = NotificationAlert::where('type', 'sms')->where('user_id', Auth::user()->id)->get();

        //        $businessEmail = Notification::where('type', 'email')->where('for', 'business')->get();
//        $businessEmail = NotificationAlert::where('type', 'email')->where('user_id', Auth::user()->id)->get();

        //        $customerSms = Notification::where('type', 'sms')->where('for', 'customer')->get();

        //        $customerEmail = Notification::where('type', 'email')->where('for', 'customer')->get();

        //        return view('notifications.index', ['businessSms' => $businessSms, 'businessEmail' => $businessEmail,
        //            'customerSms' => $customerSms, 'customerEmail' => $customerEmail]);
        if (request()->wantsJson()) {
            $data = ['businessSms' => $businessSms, 'businessEmail' => $businessEmail, 'page_title' => 'Notifications'];
            $this->_helper->response()->setCode(200)->send(['data=' => $data]);
        } else {
            return view('_v2.pages.notifications.index', ['businessSms' => $businessSms, 'businessEmail' => $businessEmail, 'page_title' => 'Notifications']);
//            return view('notifications.index', ['businessSms' => $businessSms, 'businessEmail' => $businessEmail, 'page_title' => 'Notifications']);
        }
    }

    public function addSmsAlert(Request $request)
    {

        if ($request->has('id') && $request->id) {
            $msg = 'Alert Updated Successfully.';
            if ($request->type == 'sms') {
                $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'send_on' => 'required|numeric|unique:notification_alerts,send_on,' . $request->id
                ], [
                    'name.required' => 'Please enter name',
                    'send_on.required' => 'Please enter number.',
                    'send_on.numeric' => 'Please enter valid number.'
                ]);

                $notiData = NotificationAlert::where('id', $request->id)->where('user_id', Auth::user()->id);
                if (!$notiData->exists()) {
                    return redirect()->back()->with('error', 'You are not Authrozied to edit this notification');
                }
            } else {
                $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'send_on' => 'required|email|unique:notification_alerts,send_on,' . $request->id
                ], [
                    'name.required' => 'Please enter name',
                    'send_on.required' => 'Please enter number.',
                    'send_on.email' => 'Please enter valid email.'
                ]);
            }
        } else {
            $msg = 'Alert Created Successfully.';
            if ($request->type == 'sms') {
                $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'send_on' => 'required|numeric|unique:notification_alerts,send_on,null,id,user_id,' . Auth::user()->id
                ], [
                    'name.required' => 'Please enter name',
                    'send_on.required' => 'Please enter number.',
                    'send_on.numeric' => 'Please enter valid number.'
                ]);
            } else {
                $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'send_on' => 'required|email|unique:notification_alerts,send_on,null,id,user_id,' . Auth::user()->id
                ], [
                    'name.required' => 'Please enter name',
                    'send_on.required' => 'Please enter number.',
                    'send_on.email' => 'Please enter valid email.'
                ]);
            }
        }


        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        NotificationAlert::updateOrCreate(
            [
                'id' => $request->id
            ],
            [
                'user_id' => Auth::user()->id,
                'name' => $request->name,
                'send_on' => $request->send_on,
                'type' => $request->type,
                'status' => 'pending'
            ]);

        return redirect()->back()->with('success', $msg);
    }

    public function deleteNofificationAlert($id)
    {
        NotificationAlert::where('id', $id)->delete();
        return redirect()->back()->with('success', 'Record deleted successfully ');
    }


    public function updateNotification(Request $request)
    {
        $rules = $messages = $msg = null;
        if ($request->has('type') && isset($request->type) && ($request->type == 'sms' || $request->type == 'email')) {

            if ($request->has('id') && $request->id) {
                $msg = 'Alert Updated Successfully.';
                $notiData = NotificationAlert::where('id', $request->id)->where('user_id', Auth::user()->id);
                if (!$notiData->exists()) {
                    $data = [
                        'status' => 'false',
                        'msg' => 'You are not Authrozied to edit this notification'
                    ];
                    $this->_helper->response()->setCode(200)->send($data);
                }

                $condition = '';
                $message = '';
                $messagesKey = '';
                if ($request->type == 'sms') {
                    $condition = 'required|numeric|unique:notification_alerts,send_on,' . $request->id;
                    $message = 'Please enter valid number.';
                    $messagesKey = 'send_on.numeric';
                } else {
                    $condition = 'required|email|unique:notification_alerts,send_on,' . $request->id;
                    $message = 'Please enter valid email.';
                    $messagesKey = 'send_on.email';
                }
                $rules = [
                    'name' => 'required',
                    'send_on' => $condition
                ];
                $messages = [
                    'name.required' => 'Please enter name',
                    'send_on.required' => 'Please enter number.',
                    $messagesKey => $message
                ];
            } else {
                $msg = 'Alert Created Successfully.';
                $condition = '';
                $message = '';
                $messagesKey = '';
                if ($request->type == 'sms') {
                    $condition = 'required|numeric|unique:notification_alerts,send_on,null,id,user_id,' . Auth::user()->id;
                    $message = 'Please enter valid number.';
                    $messagesKey = 'send_on.numeric';
                } else {
                    $condition = 'required|email|unique:notification_alerts,send_on,null,id,user_id,' . Auth::user()->id;
                    $message = 'Please enter valid email.';
                    $messagesKey = 'send_on.email';
                }
                $rules = [
                    'name' => 'required',
                    'send_on' => $condition
                ];
                $messages = [
                    'name.required' => 'Please enter name',
                    'send_on.required' => 'Please enter number.',
                    $messagesKey => $message
                ];
            }


            $this->_helper->runValidation($rules, $messages);

            NotificationAlert::updateOrCreate(
                [
                    'id' => $request->id
                ],
                [
                    'user_id' => Auth::user()->id,
                    'name' => $request->name,
                    'send_on' => $request->send_on,
                    'type' => $request->type,
                    'status' => 'pending'
                ]
            );

            $data = [
                'status' => 'success',
                'msg' => $msg
            ];
            $this->_helper->response()->setCode(200)->send($data);
        } else {
            $data = [
                'status' => 'false',
                'msg' => 'Please Enter Valid Type First'
            ];
            $this->_helper->response()->setCode(200)->send($data);
        }
    }

    public function removeNofificationAlert(NotificationAlert $notify)
    {
        $notiData = NotificationAlert::where('id', $notify->id)->where('user_id', Auth::user()->id);
        if (!$notiData->exists()) {
            $data = [
                'status' => 'false',
                'msg' => 'You are not Authrozied to Delete this notification'
            ];
            $this->_helper->response()->setHttpCode(403)->send($data);
        }
        NotificationAlert::where('id', $notify->id)->delete();
        $data = [
            'status' => 'success',
            'msg' => 'Notification Alert Removed'
        ];
        $this->_helper->response()->setCode(200)->send($data);
    }
}

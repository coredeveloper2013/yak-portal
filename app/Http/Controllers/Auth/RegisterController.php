<?php

namespace App\Http\Controllers\Auth;

use App\Company;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $rules = [
            'name.required' => 'Please enter your full name',
            'email.required' => 'Please enter your Email',
            'password.required' => 'Set password',
            'companyName.required' => 'Please enter your company name',
            'terms.accepted' => 'You must agree before submitting.'
        ];
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255', 'bail'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users', 'bail'],
            'password' => ['required', 'string', 'min:8', 'bail', 'confirmed'],
            'companyName' => ['required', 'string', 'bail'],
            'terms' => ['accepted', 'bail']
        ], $rules);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user =  User::create([
            'name' => $data['name'],
            'username' => array_key_exists('username', $data) ? $data['username'] : null,
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        Company::create([
            'user_id' => $user->id,
            'name' => $data['companyName']
        ]);

        return $user;
    }
}

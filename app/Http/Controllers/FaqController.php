<?php

namespace App\Http\Controllers;

use App\Faq;
 
use App\Helpers\Helper;

class FaqController extends Controller
{
    protected $_helper;

    public function __construct()
    {
        $this->_helper = new Helper();
    }

    public function index()
    {
    	$faq=Faq::all();
    	return view('support.index',['faq' => $faq, 'page_title' => 'Support']);
    }
    public function faq()
    {
        $faq=Faq::all();
        if (request()->wantsJson()) {
            $this->_helper->response()->setCode(200)->send(['data' => ['faqs' => $faq]]);
        } else
        return view('faq.index',['faq' => $faq, 'page_title' => 'Frequently Asked Questions']);
    }
   
}

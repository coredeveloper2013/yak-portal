<?php

namespace App\Http\Controllers\_v2\Customer;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Stripe;
use App\Agreement;
use App\Models\Customer\Invoice;
use Carbon\Carbon;
use App\Helpers\Helper;
use Laravel\Cashier\Cashier;
use PDF;
use Illuminate\Support\Facades\Validator;

class BillingController extends Controller
{
    public function __construct()
    {

        $this->stripeApiKey = 'sk_test_51Hj21sJy78K7KaFERgTh8CtU5bcJOZ4zWrgyMVVRhGgqulKDVB1F9arPD1DlovqPfjk1auerQ6onrM8MeeWHW5Uh00hiRFivus';
        $this->items        = 10;
        $this->currency     = 'usd';
    }

    public function payNow(Request $request, $id)
    {
        $stripeRes = \Stripe\Stripe::setApiKey(
            $this->stripeApiKey
        );

        $currency = $this->currency;
        $user     = User::where('id', $request->user_id)->first();

        $data = [
            'amount' => (int)($request->amount * 100),
            'currency' => $currency,
            'customer' => $user->stripe_id,
        ];

        $stripeRes2 = \Stripe\Charge::create($data);

        if($stripeRes2->status == "succeeded") {
            $data = [
                'response' => json_encode($stripeRes2),
                'status' => "completed",
                'paid_date' => Carbon::now()
            ];

            $invoice = Invoice::where('id', $id)->first();
            $invoice->update($data);

            return response()->json($invoice);
        }
    }


    public function updatePaymentStatus(Request $request, $id)
    {
        $stripe = new \Stripe\StripeClient(
            $this->stripeApiKey
        );

        $customer   = [];
        $user       = [];

        if($request->saveThisCard) {
            $user = User::where('id', $id);

            \Stripe\Stripe::setApiKey(
                $this->stripeApiKey
            );
            $customer = \Stripe\Customer::create([
                    'source' => 'tok_mastercard',
                    'email' => $user->first()->email
                ]
            );

            $updateData = [
                'stripe_id'      => $customer->id,
                'card_brand'     => request()->token['card']['brand'],
                'card_last_four' => request()->token['card']['last4'],
                'trial_ends_at'  => request()->token['card']['exp_year'].'-'.request()->token['card']['exp_month'].'-01',
            ];

            $user->update($updateData);
            $user = $user->first();
        } else {
            $user = User::where('id', $id)->first();
        }

        return response()->json([
            'status' => 'new Success!',
            'customer' => $customer,
            'user' => $user
        ]);
    }

    public function getInvoice()
    {
        $user_id = Auth()->User()->id;
        $items = request()->filled('items') ? request()->items : $this->items;
        $invoice = Invoice::where('user_id',$user_id)->with('user.company');

        if (request()->filled('search')) {
            $invoice = $invoice->whereHas('user.company', function ($query) {
                return $query->where('name', 'LIKE', '%' . request()->search . '%');
            })->orWhereHas('user', function ($query) {
                return $query->where('name', 'LIKE', '%' . request()->search . '%');
            });
        }

        if(request()->filled('sort')) $invoice = $invoice->orderBy('id', request()->sort);
        else  $invoice = $invoice->latest();

        $invoice = $invoice->paginate($items);

        return response()->json($invoice);
    }

    public function index()
    {
        $user_id = Auth()->User()->id;
        $items = request()->filled('items') ? request()->items : $this->items;
        $invoices = Invoice::where('user_id',$user_id)->with('user.company')
            ->latest()
            ->paginate($items);
        return view('_v2.pages.staff.billing.index',compact('invoices'));
    }

    public function getIntent(Request $request, $id)
    {
        $user   = User::find($id);
        $intent = $user->createSetupIntent();
        return response()->json($intent);
    }

    public function exportPdf(Request $request, $id)
    {
        $this->makeStorageDirectory('export-pdf');

        $invoice = Invoice::query()
            ->where('id',$id)
            ->with('user.company')
            ->first();
        $fileName = "file_" . rand(000000, 999999) . ".pdf";
        $path = storage_path('app/public/export-pdf/') . $fileName;
        PDF::loadView('_v2.export-pdf.invoice', compact('invoice'))->save($path);
        return response()->json(asset("/storage/export-pdf/" . $fileName));
    }

    private function makeStorageDirectory($dir)
    {
        if(!\Storage::disk('local')->has('public/'.$dir)) {
            \Storage::disk('local')->makeDirectory('public/'.$dir);
        }
    }
}

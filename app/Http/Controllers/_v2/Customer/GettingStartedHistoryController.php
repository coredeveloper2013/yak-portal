<?php

namespace App\Http\Controllers\_v2\Customer;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Models\Customer\Agreement;
use App\Models\GettingStarted;
use App\Models\GettingStartedHistory;
use Auth;

class GettingStartedHistoryController extends Controller
{
    public function __construct()
    {
        $this->items = 10;
    }

    public function index()
    {
        dd(GettingStarted::all());
        $items = request()->filled('items') ? request()->items : $this->items;
        $agreements = Agreement::query()
            ->with('campaign')
            ->with('user')
            ->with('company')
            ->with('document')
            //->where('user_id', Auth()->user()->id)
            ->latest()
            ->paginate($items);

        return view('_v2.pages.staff.agreement.index', compact('agreements'));
    }
}

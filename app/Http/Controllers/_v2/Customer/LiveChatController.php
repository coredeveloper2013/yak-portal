<?php

namespace App\Http\Controllers\_v2\Customer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LiveChatController extends Controller
{
    public function index()
    {
        return view('_v2.pages.live_chat.index');
    }
}

<?php

namespace App\Http\Controllers\_v2\Customer;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Models\Customer\Agreement;
use App\Models\GettingStarted;
use App\Models\GettingStartedHistory;
use Auth;

class GettingStartedController extends Controller
{
    public function __construct()
    {
        $this->items = 10;
    }

    public function index()
    {
        $gettingStarteds = GettingStarted::get()->groupBy('is_header');
        $myGettingStartedHistories = GettingStartedHistory::where('user_id', Auth::user()->id)->get()->keyBy('getting_starteds_id')->toArray();

        return view('_v2.pages.customer.getting_started', compact('gettingStarteds', 'myGettingStartedHistories'));
    }

    public function markAsDone($user_id, $getting_starteds_id, $action)
    {
        //dd($user_id, $getting_started_id, $action);
        $myGettingStartedHistories = GettingStartedHistory::updateOrCreate([
            'user_id'            => Auth::user()->id,
            'getting_starteds_id' => $getting_starteds_id
        ],[
            'is_marked'          => $action
        ]);

        return redirect()->back();
    }
}

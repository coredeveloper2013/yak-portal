<?php

namespace App\Http\Controllers\_v2\Customer;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreLead;
use App\Http\Requests\StoreLeadDealComment;
use App\Models\Customer\LeadDeal;
use App\Models\Customer\LeadDealQuestion;
use App\Models\Customer\Lead;
use App\Models\Customer\LeadDealsComments;
use App\Services\LeadService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Resources\Campaigns as CampaignsResource;
use App\Helpers\Helper;
use App\Guide;
use App\User;
use App\LandingPage;
use Illuminate\Support\Facades\Auth;

class LeadController extends Controller
{
    public function __construct()
    {
        $this->items = 20;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = request()->filled('items') ? request()->items:$this->items;

        if ($request->sort == 'asc') {
            $data = (new LeadService($request))->getLeads()->paginate($items);
        }
        else   $data = (new LeadService($request))->getLeads()->latest()->paginate($items);
        return view('_v2.pages.leads.customer_leads', ['page_title' => 'Customer Leads'])->with('leads', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLead $request)
    {
        $user_id = Auth::user()->id;

        $leads = new LeadDeal();
        $leads->name = $request->lead_name;
        $leads->email = $request->lead_email;
        $leads->phone = $request->lead_phone;
        $leads->user_id =  $user_id;
        $leads->created_at = $request->lead_date;
        $leads->save();
        $insertedId = $leads->id;

        if(count($request->question)>0){
            $leadDealQuestion = [];
            foreach ($request->question as $key => $question) {
                $leadDealQuestion[$key] = [
                    'lead_deal_id' => $insertedId,
                    'question' => $request->question[$key],
                    'answer' => $request->answer[$key],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ];
            }
            LeadDealQuestion::insert($leadDealQuestion);
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lead = LeadDeal::where('id', $id);
        $lead = $lead->with('LeadDealQuestion')->with('user:id,type')->first();
        return response()->json($lead);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if($request->edit == '1'){
            $user_id = Auth::user()->id;

            $leads = LeadDeal::where('id',$request->lead_id)->first();
            $leads->name = $request->lead_name;
            $leads->email = $request->lead_email;
            $leads->phone = $request->lead_phone;
            $leads->user_id =  $user_id;
            $leads->created_at = $request->lead_date;
            $leads->update();
            $insertedId = $leads->id;

            $leadDealQuestion = [];
            foreach($request->question as $key => $question) {
                $leadDealQuestion= [
                    'lead_deal_id' => $insertedId,
                    'question' => $request->question[$key],
                    'answer' => $request->answer[$key],
                ];
                LeadDealQuestion::where('lead_deal_id',$insertedId)->updateOrInsert($leadDealQuestion);
            }
        }
        else{
        $lead = LeadDeal::where('id',$request->lead_id)->first();
        $lead->status = $request->status;
       /* $lead->updated_at = $request->comment_date;*/
        $lead->update();

         $user = LeadDealsComments::create([
            'message' => $request->message,
            'lead_id' => $request->lead_id,
            'user_id' => Auth::id()
        ]);
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd($id);
    }
}

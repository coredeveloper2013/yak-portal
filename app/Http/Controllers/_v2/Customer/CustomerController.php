<?php

namespace App\Http\Controllers\_v2\Customer;

use App\Http\Controllers\Controller;
use App\Models\Customer\LeadDeal;
use Illuminate\Http\Request;
use Auth;

class CustomerController extends Controller
{
    public function index()
    {
        $leads = Auth::user()->leads()->count();
        $appointments = Auth::user()->memberAppointments();
        return view('_v2.pages.customer.dashboard', compact('leads', 'appointments'));
    }

    public function customerDashboard()
    {
        $auth = Auth::user();
        $leads = $auth->leaddeals->groupBy('status');

        $oLeadDeal = new \App\LeadDeal();
        $list = $oLeadDeal->statusEnums;

        $leads_data = [];
        foreach ($leads as $key => $lead) {
            $leads_data[] = array(
                'name' => array_key_exists($key, $list) ? $list[$key]:'',
                // 'name' => $key,
                'data' => [count($lead)],
            );
        }

        // dd($leads_data, $leads);


        $appointments = $auth->memberAppointments()->with('user');
        $data = [];
        $data['auth'] = $auth;
        $data['total_lead'] = $auth->leaddeals()->count();
        $data['appointments'] = $appointments->get();
        $data['total_appointment'] = $appointments->count();
        $data['total_invoice_amount'] = $auth->getOutstandingInvoiceAmount();
        $data['leads_data'] = $leads_data;
        return response()->json($data);

    }

    public function customerDashboardLeadsChart(Request $request)
    {
        $leads_data = [];
        if (!empty($request->leads_from_date) && !empty($request->leads_to_date)) {
            $leads = Auth::user()->leaddeals->whereBetween('created_at', [$request->leads_from_date . " 00:00:00", $request->leads_to_date . " 23:59:59"])->groupBy('status');
            foreach ($leads as $key => $lead) {
                $leads_data[] = array(
                    'name' => $key,
                    'data' => [count($lead)],
                );
            }
        }
        return response()->json($leads_data);
    }
}

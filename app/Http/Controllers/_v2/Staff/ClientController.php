<?php

namespace App\Http\Controllers\_v2\Staff;

use App\Campaign;
use App\Http\Resources\LeadComments;
use App\LandingPageComments;
use App\Models\Company;
use App\Task;
use App\Models\Customer\Invoice;
use App\Models\Customer\LeadDeal;
use App\Models\Customer\LeadDealQuestion;
use App\Models\Customer\LeadDealsComments;
use App\Models\LandingPage;
use App\Models\Membership\Plan;
use App\Models\UserBusinessInformation;
use App\Note;
use App\LandingQuestionAnswers;
use App\NotificationAlert;
use App\OtherProject;
use App\Services\LeadService;
use App\Services\NotificationAlertService;
use App\TelescopeEntriesTags;
use App\User;
use Carbon\Carbon;
use Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mpdf\Tag\U;

use PDF;

class ClientController extends Controller
{
    public function __construct()
    {

        $this->stripeApiKey           = 'sk_test_51Hj21sJy78K7KaFERgTh8CtU5bcJOZ4zWrgyMVVRhGgqulKDVB1F9arPD1DlovqPfjk1auerQ6onrM8MeeWHW5Uh00hiRFivus';
        $this->itemsViewCustomerLeads = 20;
        $this->items                  = 10;
        $this->items1                 = 5;
        $this->items2                 = 5;
        $this->currency               = 'usd';
    }


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function viewCustomerLeads(Request $request, $client_id)
    {
        $client = User::select('id','name')->where('id',$client_id)->first();

        $items = request()->filled('items') ? request()->items : $this->itemsViewCustomerLeads;

       $leads = LeadDeal::query()->where('user_id',$client_id);

        if (!empty( $request->search)) {
            $leads = $leads->where( 'name', 'LIKE', '%' . $request->search.'%');
        }
        if ($request->sort == 'asc') {
            $leads = $leads->paginate($items);
        } else   $leads = $leads->latest()->paginate($items);

        return view('_v2.pages.staff.client.lead', compact('leads','client'));
    }

    public function saveLead(Request $request,$userId){
       // dd($request->all());
        $leads = new LeadDeal();
        $leads->name = $request->lead_name;
        $leads->email = $request->lead_email;
        $leads->phone = $request->lead_phone;
        $leads->user_id =  $userId;
        $leads->created_at = $request->lead_date;
        $leads->save();
        $insertedId = $leads->id;

        $leadDealQuestion = [];
        foreach($request->question as $key => $question) {
            $leadDealQuestion[$key] = [
                'lead_deal_id' => $insertedId,
                'question' => $request->question[$key],
                'answer' => $request->answer[$key],
                'created_at' => Carbon::now(),
                'updated_at' =>  Carbon::now()
            ];
        }
        LeadDealQuestion::insert($leadDealQuestion);
        return redirect()->back();
    }

    public function showLead($id)
    {
        $lead = LeadDeal::where('id', $id);
        $lead = $lead->with('LeadDealQuestion')->with('user:id,type')->first();
        return response()->json($lead);
    }

    public function updateLead(Request $request,$userId)
    {
        if($request->edit == '1'){
            $leads = LeadDeal::where('id',$request->lead_id)->first();
            $leads->name = $request->lead_name;
            $leads->email = $request->lead_email;
            $leads->phone = $request->lead_phone;
            $leads->user_id =  $userId;
            $leads->created_at = $request->lead_date;
            $leads->update();
            $insertedId = $leads->id;

            $leadDealQuestion = [];
            foreach($request->question as $key => $question) {
                $leadDealQuestion= [
                    'lead_deal_id' => $insertedId,
                    'question' => $request->question[$key],
                    'answer' => $request->answer[$key],
                ];
                LeadDealQuestion::where('lead_deal_id',$insertedId)->updateOrInsert($leadDealQuestion);
            }
        }
        else{
            $lead = LeadDeal::where('id',$request->lead_id)->first();
            $lead->status = $request->status;
            $lead->updated_at = $request->comment_date;
            $lead->update();

            $user = LeadDealsComments::create([
               'message' => $request->message,
               'lead_id' => $request->lead_id,
               'user_id' => Auth::id()
           ]);
        }
        return redirect()->back();
    }

    public function getClients()
    {
        $items = request()->filled('items') ? request()->items : $this->items;

        $clients = User::query()
            ->where('type', 'customer')
            ->with('company', 'clientPaymentInvoices', 'lastComments', 'user_landing_page');
            /*->with('company', 'clientPaymentInvoices', 'clientInvoices', 'lastComments', 'user_landing_page', 'staffCampaigns');*/

        if (request()->filled('search')) {
            $clients = $clients->whereHas('company', function ($query) {
                return $query->where('name', 'LIKE', '%' . request()->search . '%');
            });
        }

        if (request()->landing_page_status) {
            $clients = $clients->whereHas('landing_pages', function ($query) {
                return $query->where('status', request()->landing_page_status);
            });
        }

        if (request()->campaign_status) {
            $clients = $clients->whereHas('staffCampaigns', function ($query) {
                return $query->where('status', request()->campaign_status);
            });
        }

        if (request()->findBy) $clients = $clients->where('status', request()->findBy);
        if (request()->status) $clients = $clients->where('status', request()->status);

        $clients = $clients->get();
        $clients = $clients->map(function($q) {
            $user = $q->toArray();
            $user['user_landing_page_status'] = str_replace(' ', '_', strtolower($q->user_landing_page ? $q->user_landing_page->status:''));
            return $user;
        });
        $clients = ['data' => $clients->groupBy('user_landing_page_status')];

        return response()->json($clients);
    }

    public function getClientsOLD()
    {
        $items = request()->filled('items') ? request()->items : $this->items;

        $clients = User::query()
            ->where('type', 'customer')
            ->with('company', 'clientPaymentInvoices', 'clientInvoices', 'lastComments', 'landing_pages', 'staffCampaigns');

        if (request()->filled('search')) {
            $clients = $clients->whereHas('company', function ($query) {
                return $query->where('name', 'LIKE', '%' . request()->search . '%');
            });
        }

        if (request()->landing_page_status) {
            $clients = $clients->whereHas('landing_pages', function ($query) {
                return $query->where('status', request()->landing_page_status);
            });
        }

        if (request()->campaign_status) {
            $clients = $clients->whereHas('staffCampaigns', function ($query) {
                return $query->where('status', request()->campaign_status);
            });
        }

        if (request()->findBy) $clients = $clients->where('status', request()->findBy);
        if (request()->status) $clients = $clients->where('status', request()->status);

        //$clients = $clients->paginate($items);
        $clients = $clients->get();
        $clients = ['data' => $clients];
        return response()->json($clients);
    }

    public function updateACampaign(Request $request, $id)
    {
        $campaign = Campaign::where('id', $id);
        $campaign->update([
            'recurring_way' => $request->recurring_way,
            'amount' => $request->amount
        ]);

        return response()->json($campaign->first());
    }

    public function payNow(Request $request, $id)
    {
        $stripeRes = \Stripe\Stripe::setApiKey(
            $this->stripeApiKey
        );

        $currency = $this->currency;
        $user     = User::where('id', $request->user_id)->first();

        $data = [
            'amount' => (int)($request->amount * 100),
            'currency' => $currency,
            'customer' => $user->stripe_id,
        ];

        $stripeRes2 = \Stripe\Charge::create($data);

        if($stripeRes2->status == "succeeded") {
            $data = [
                'response' => json_encode($stripeRes2),
                'status' => "completed",
                'paid_date' => Carbon::now()
            ];

            $invoice = Invoice::where('id', $id)->first();
            $invoice->update($data);

            return response()->json($invoice);
        }
    }

    public function updatePaymentStatus(Request $request, $id)
    {
        $stripe = new \Stripe\StripeClient(
            $this->stripeApiKey
        );

        /*$stripeConfirm = $stripe;
        $stripeRes     = $stripe->paymentIntents->create([
            'amount' => 10 * 100,
            'currency' => 'usd',
            'payment_method_types' => ['card'],
        ]);

        $stripe = $stripeConfirm;

        $stripeRes2 = $stripe->paymentIntents->confirm(
            $stripeRes->id,
            ['payment_method' => 'pm_card_visa']
        );*/

        $customer   = [];
        $user       = [];

        if($request->saveThisCard) {
            $user = User::where('id', $id);

            \Stripe\Stripe::setApiKey(
                $this->stripeApiKey
            );
            $customer = \Stripe\Customer::create([
                    'source' => 'tok_mastercard',
                    'email' => $user->first()->email
                ]
            );

            $updateData = [
                'stripe_id'      => $customer->id,
                'card_brand'     => request()->token['card']['brand'],
                'card_last_four' => request()->token['card']['last4'],
                'trial_ends_at'  => request()->token['card']['exp_year'].'-'.request()->token['card']['exp_month'].'-01',
            ];

            $user->update($updateData);
            $user = $user->first();
        } else {
            $user = User::where('id', $id)->first();
        }

        //\App\Models\Company\Company::where('id', request()->company_id)->where('subscribed_at', NULL)->update(['subscribed_at' => date('Y-m-d')]);

        return response()->json([
            'status' => 'new Success!',
            'customer' => $customer,
            'user' => $user
        ]);
    }

    public function getIntent(Request $request, $id)
    {
        $user   = User::find($id);
        $intent = $user->createSetupIntent();
        return response()->json($intent);
    }

    public function index()
    {
        $items   = request()->filled('items') ? request()->items : $this->items;
        $clients = User::query()
            ->where('type', 'customer')
            /*->with('company', 'clientPaymentInvoices', 'clientInvoices', 'lastComments')*/
            ->with('company', 'clientPaymentInvoices', 'lastComments', 'user_landing_page')
            ->latest()
            ->get();
            /*->paginate($items);*/

        $clients = $clients->map(function($q) {
            $user = $q->toArray();
            $user['user_landing_page_status'] = str_replace(' ', '_', strtolower($q->user_landing_page ? $q->user_landing_page->status:''));
            return $user;
        });
        $clients = ['data' => $clients->groupBy('user_landing_page_status')];
        $staffs = User::where('type', 'staff')
            ->whereIn('job_position', array('account_manager','sales_person', 'campaign_manager'))
            ->get(['id','name']);

        return view('_v2.pages.staff.client.index', compact('clients','staffs'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'password' => 'required|confirmed',
            //'job_position' => 'required',
            //'status' => 'required',
            //'member_color' => 'required',
            'company_name' => 'required',
            'trading_name' => 'required'
        ]);

        $data    = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => Hash::make($request->password),
            'job_position' => $request->job_position,
            //'status' => $request->status,
            //'member_color' => $request->member_color
        ];

        if($request->filled('type')) $data['type'] = $request->type;
        if($request->filled('assigned_to')) $data['assigned_to_id'] = $request->assigned_to['id'];

        $user    = User::create($data);
        $data1   = [
            'user_id' => $user->id,
            'name' => $request->company_name,
        ];
        $company = Company::create($data1);

        $data2                   = [
            'user_id' => $user->id,
            'trading_name' => $request->trading_name,
            'recurring_way' => $request->recurring_way,
            'recurring_amount' => $request->recurring_amount,
            'phone' => $request->work_phone,
            'email_address' => $request->work_email,
            'website' => $request->work_website,
            'notes' => $request->notes,
            'address' => $request->work_address,
        ];
        $userBusinessInformation = UserBusinessInformation::create($data2);

        return response()->json($user);
    }

    public function viewLandingPage($user_id)
    {
        $view_landing_pages = LandingPage::where('user_id', $user_id)
            ->with('user')
            ->get();

        $client = User::query()
            ->where('type', 'customer')
            ->where('id', $user_id)
            //->with('company', 'business_info', 'userLogs', 'userLogs.logDetails')
            ->with('company', 'business_info')
            ->first();

        return view('_v2.pages.staff.client.view_landing_page', compact('view_landing_pages', 'client'));
    }

    public function getLogs($user_id)
    {
        $items = request()->filled('items') ? request()->items : $this->items;

        $logs = TelescopeEntriesTags::where('tag', $user_id)
            ->with('logDetails');
        if (request()->filled('search')) $logs = $logs->whereHas('logDetails', function ($q) {
            $q->where('content', 'LIKE', '%' . request()->search . '%');
            // $q->whereJsonContains('content', 'LIKE', '%'.request()->search.'%');
            // $q->where('content', 'LIKE', '%'.request()->search.'%');
            // $q->whereRaw('json_contains(content, \'["' . request()->search . '"]\')');
        });
        $logs = $logs->paginate($items);

        return response()->json($logs);
    }

    public function logs($user_id)
    {
        $items = request()->filled('items') ? request()->items : $this->items;

        $logs   = TelescopeEntriesTags::where('tag', $user_id)
            ->with('logDetails')
            ->paginate($items);
        $client = User::query()
            ->where('type', 'customer')
            ->where('id', $user_id)
            //->with('company', 'business_info', 'userLogs', 'userLogs.logDetails')
            ->with('company', 'business_info')
            ->first();

        return view('_v2.pages.staff.client.logs', compact('logs', 'client'));
    }

    public function getBilling($id)
    {
        $items   = request()->filled('items') ? request()->items : $this->items;
        $invoice = Invoice::where('user_id', $id)->with('user.company');

        if (request()->filled('search')) {
            $invoice = $invoice->whereHas('user.company', function ($query) {
                return $query->where('name', 'LIKE', '%' . request()->search . '%');
            })->orWhereHas('user', function ($query) {
                return $query->where('name', 'LIKE', '%' . request()->search . '%');
            });
        }

        if (request()->filled('sort')) $invoice = $invoice->orderBy('id', request()->sort);
        else  $invoice = $invoice->latest();

        $invoice = $invoice->paginate($items);

        return response()->json($invoice);
    }

    public function billing($id)
    {
        $client = User::where('id', $id)->first();

        $items    = request()->filled('items') ? request()->items : $this->items;
        $totalRenewal = Invoice::where('user_id', $id)->whereStatus('completed')->count();
        $campaign = Campaign::where('user_id', $id)->orderBy('id', 'asc')->first();

        $invoices = Invoice::where('user_id', $id)->with('user.company')
            ->latest()
            ->paginate($items);

        return view('_v2.pages.staff.client.billing', compact('client', 'invoices', 'totalRenewal', 'campaign'));
    }

    public function exportPdf(Request $request, $id)
    {
        $this->makeStorageDirectory('export-pdf');

        $invoice  = Invoice::query()
            ->where('id', $id)
            ->with('user.company')
            ->first();
        $fileName = "file_" . rand(000000, 999999) . ".pdf";
        $path     = storage_path('app/public/export-pdf/') . $fileName;
        PDF::loadView('_v2.export-pdf.invoice', compact('invoice'))->save($path);
        return response()->json(asset("/storage/export-pdf/" . $fileName));
    }

    private function makeStorageDirectory($dir)
    {
        if (!\Storage::disk('local')->has('public/' . $dir)) {
            \Storage::disk('local')->makeDirectory('public/' . $dir);
        }
    }

    public function getInvoiceData(){
        $campaign = Campaign::select(['id','name'])->orderBy('name')->get();
        $landingPage = LandingPage::select(['id','name'])->orderBy('name')->get();
        $otherProject = OtherProject::select(['id','project_name'])->orderBy('project_name')->get();

        return response()->json(['campaign'=>$campaign,'landingPage'=>$landingPage,'otherProject'=>$otherProject]);
    }

    public function saveInvoice(Request $request,$userId){
        $data    = [
            'user_id' => $userId,
            'campaign_id' => $request->campaign ? $request->campaign['id'] : null,
            'landing_pages_id' => $request->landingPage ?  $request->landingPage['id'] : null,
            'otherProject' => $request->otherProject ? $request->otherProject['id'] : null,
            'type' => 'campaign',
            'description' =>$request->description,
            'status' =>'pending',
            'amount' => $request->amount,
            'vat' => $request->vat,
            'total_amount' => $request->total
        ];
        $invoice = Invoice::create($data);

        return response()->json($invoice);
    }

    public function overview($id)
    {
        $client = User::query()
            ->where('type', 'customer')
            ->where('id', $id)
            ->with('company', 'business_info')
            ->first();
        $users = User::orderBy('name')->get(['id', 'name', 'type']);
        $staffs = User::where('type', 'staff')->get(['id','name']);

        return view('_v2.pages.staff.client.overview', compact('client', 'users', 'staffs'));
    }

    public function show($id)
    {
        $clients = User::query()
            ->where('type', 'customer')
            ->where('id', $id)
            ->with('company', 'business_info')
            ->get();

        return response()->json($clients);
    }

    public function details(Request $request, $id)
    {
        $auth = User::with('clientPaymentInvoices', 'invoices')->find($id);

        $data                               = [];
        $data['members']                    = User::all();
        $data['auth']                       = $auth;
        $data['campaigns_data']             = $this->campaignLiveChart($request, $id);
        $data['tasks']                      = Task::where('user_id', $id)->latest()->take(5)->get();
        $data['notes']                      = $auth->memberNotes()->latest()->get();
        $data['total_outstanding_amount']   = $auth->getOutstandingInvoiceAmount();
        $data['total_payment_amount']       = $auth->clientPaymentInvoices->sum('total_amount');
        $data['invoice_payment_paid_count'] = $auth->clientPaymentInvoices->count();
        $data['appointments']               = $auth->memberAppointments()->with('user')->take(5)->latest()->get();

        return response()->json($data);
    }

    public function campaignLiveChart($request, $id)
    {
        $campaigns_data               = [];
        $series_data                  = [];
        $campaigns_data['categories'] = [];
//        $users = User::with('campaignCreatedBy')->whereHas('campaignCreatedBy', function ($q) {
////            $q->where('created_by', Auth::user()->id);
//        })->get();
        $campaigns = new Campaign();
        if (!empty($request->campaign_from_date) && !empty($request->campaign_to_date)) {
            $campaigns = $campaigns->whereDate('created_at', '>=', $request->campaign_from_date)
                ->whereDate('created_at', '<=', $request->campaign_to_date);
        }

        $campaigns = $campaigns->where(function ($q) use ($id) {
            $q->where('user_id', $id);
        });


        $campaigns = $campaigns->with('user')->groupBy('user_id')->get();

        $status  = Campaign::groupBy('status')->pluck('status');
        $key_val = [];
        $k       = 0;
        foreach ($campaigns as $key2 => $campaign) {
            foreach ($status as $status_key) {
                $get_camp_status_Data = new Campaign();
                if (!empty($request->campaign_from_date) && !empty($request->campaign_to_date)) {
                    $get_camp_status_Data = $get_camp_status_Data->whereDate('created_at', '>=', $request->campaign_from_date)
                        ->whereDate('created_at', '<=', $request->campaign_to_date);
                }

                $get_camp_status_Data = $get_camp_status_Data->where('user_id', $campaign->user_id)->where('status', $status_key)->count();
                if (in_array($status_key, $key_val)) {
                    $index = array_search($status_key, $key_val);
                    $array = $series_data[$index]['data'];
                    array_push($array, $get_camp_status_Data);
                    $series_data[$index] = array(
                        'name' => $status_key,
                        'data' => $array,
                    );
                } else {
                    $key_val[$k]     = $status_key;
                    $series_data[$k] = array(
                        'name' => $status_key,
                        'data' => [$get_camp_status_Data],
                    );
                    $k++;
                }
            }
            $campaigns_data['categories'][] = $campaign->user->name;
        }
        $campaigns_data['series'] = $series_data;
        return $campaigns_data;
    }

    public function staffClientCampaign(Request $request, $id)
    {
        $campaigns_data = $this->campaignLiveChart($request, $id);
        return response()->json($campaigns_data);
    }

    public function saveNote(Request $request, $id)
    {
        $note            = new Note();
        $note->note      = $request->note;
        $note->member_id = $id;
        $note->user_id   = Auth::user()->id;
        $note->save();
        $notes = $this->noteLists($id);
        return response()->json($notes);
    }

    public function deleteNote($user_id, $note_id){
        $note = Note::find($note_id);
        $note->delete();

        $noteLists = $this->noteLists($user_id);

        return response()->json($noteLists);
    }

    public function noteLists($id)
    {
        return Note::with('client')->where('member_id', $id)->latest()->get();
    }

    // Edit Notification Start
    public function showNotification(Request $request, $id)
    {
        //        $businessSms = Notification::where('type', 'sms')->where('for', 'business')->get();
        $client = User::find($id, ['id', 'name']);

        $items1 = request()->filled('items1') ? request()->items1 : $this->items1;
        $items2 = request()->filled('items2') ? request()->items2 : $this->items2;

        $businessSms = NotificationAlert::where('type', 'sms')->where('user_id', $id);
        if (!empty($request->search)) {
            $businessSms = $businessSms->where('name', 'LIKE', '%' . $request->search . '%')
                ->orWhere('send_on', 'LIKE', '%' . $request->search . '%');
        }
        if ($request->sort == 'desc') {
            $businessSms = $businessSms->latest()->paginate($items1, ['*'], 'page1');
        } else $businessSms = $businessSms->paginate($items1, ['*'], 'page1');

        $businessEmail = NotificationAlert::where('type', 'email')->where('user_id', $id);
        if (!empty($request->search2)) {
            $businessEmail = $businessEmail->where('name', 'LIKE', '%' . $request->search2 . '%')
                ->orWhere('send_on', 'LIKE', '%' . $request->search2 . '%');
        }
        if ($request->sort2 == 'desc') {
            $businessEmail = $businessEmail->latest()->paginate($items2, ['*'], 'page2');
        } else $businessEmail = $businessEmail->paginate($items2, ['*'], 'page2');


        return view('_v2.pages.staff.client.notification', ['businessSms' => $businessSms, 'businessEmail' => $businessEmail, 'client' => $client, 'page_title' => 'Notifications']);
//
    }

    public function storeNotification(Request $request, $id)
    {
        NotificationAlert::Create(
            [
                'user_id' => $id,
                'name' => $request->name,
                'send_on' => $request->send_on,
                'type' => $request->type,
                'status' => 'pending'
            ]);

        return redirect()->back()->with('success', 'Create Notification Successfully');

    }

    public function deleteNotification($id)
    {
        NotificationAlert::where('id', $id)->delete();
        return redirect()->back()->with('success', 'Record deleted successfully ');
    }

    // Edit Client Start
    public function showClient($id)
    {
        $clients = User::query()
            ->where('type', 'customer')
            ->where('id', $id)
            ->with('company', 'business_info', 'user_landing_page')
            ->first();

        $landingPages = LandingPage::select(['id','name'])->orderBy('name')->get();
        return view('_v2.pages.staff.client.edit', compact('clients','landingPages'));
    }

    public function updateClient(Request $request, $id)
    {
        // dd($request->company['name']);
        /*$request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'password' => 'required|confirmed',
            'job_position' => 'required',
            'status' => 'required',
            'member_color' => 'required',
            'company_name' => 'required',
        ]);*/

        $logo = $image = '';

        if($request->hasFile('logo')) {
            $basePath = '/staff/upload';
            $image = $request->file('logo');
            $fileExt = $image->getClientOriginalExtension();
            $file_name = time() . '.' . $fileExt;
            $file = $image->move(public_path($basePath), $file_name);
            $fileName = $basePath . '/' . $file_name;
            $logo = $fileName;
        }

        if($request->hasFile('image')) {
            $basePath = '/staff/upload';
            $image = $request->file('image');
            $fileExt = $image->getClientOriginalExtension();
            $file_name = time() . '.' . $fileExt;
            $file = $image->move(public_path($basePath), $file_name);
            $fileName = $basePath . '/' . $file_name;
            $image = $fileName;
        }

        $data               = User::find($id);
        $data->name         = $request->name;
        $data->email        = $request->email;
        $data->phone        = $request->phone;
        $request->password  ? $data->password = Hash::make($request->password):'';
        $data->save();

        if(!empty($request->company_id))    $company = Company::where('id', $request->company_id)->first();
        else                                $company = new Company();

        $company->name    = $request->company_name;
        $company->user_id = $id;
        $company->save();

        /*if ($request->hasFile('logo')) {
            $basePath = '/staff/upload';
            $image = $request->file('attachments');
            $fileExt = $image->getClientOriginalExtension();
            $file_name = time() . '.' . $fileExt;
            $file = $image->move(public_path($basePath), $file_name);
            $fileName = $basePath . '/' . $file_name;
            $task->attachments = $fileName;
            $task->file_type = $fileExt;
        }

        if ($request->hasFile('image')) {
            $basePath = '/staff/upload';
            $image = $request->file('attachments');
            $fileExt = $image->getClientOriginalExtension();
            $file_name = time() . '.' . $fileExt;
            $file = $image->move(public_path($basePath), $file_name);
            $fileName = $basePath . '/' . $file_name;
            $task->attachments = $fileName;
            $task->file_type = $fileExt;
        }*/

        if(!empty($request->business_info_id))  $userBusinessInformation = UserBusinessInformation::where('id', $request->business_info_id)->first();
        else                                    $userBusinessInformation = new UserBusinessInformation();

        $userBusinessInformation->user_id      = $id;
        $userBusinessInformation->trading_name = $request->business_info_trading_name;
        $userBusinessInformation->website      = $request->business_info_website;
        $userBusinessInformation->save();


        if(!empty($request->landing_page_id))   $landingPage = LandingPage::where('id',$request->landing_page_id)->first();
        else                                    $landingPage = new LandingPage();

        $landingPage->user_id           = $id;
        $landingPage->name              = $request->landing_page_name;
        $landingPage->logo              = $logo;
        $landingPage->additional_photo  = $image;
        $landingPage->status            = $request->landing_page_status;
        $landingPage->description       = $request->landing_page_description;
        $landingPage->customer_benefit  = $request->landing_page_customer_benefit;
        $landingPage->other_query       = $request->landing_page_other_query;
        $landingPage->target_location   = $request->landing_page_target_location;
        $landingPage->reviews           = $request->landing_page_reviews;

        $landingPage->save();

        LandingQuestionAnswers::where('landing_pages_id', $landingPage->id)->delete();

        $data = [];
        $key  = 0;

        for ($i=0; $i<=3; $i++) {
            $key = (int)$i + 1;
            if($request->{'landing_page_q'.$key})
                $data[$i] = [
                    'landing_pages_id' => $landingPage->id,
                    'question' => $request->{'landing_page_q' . $key},
                    'answer' => $request->{'landing_page_a' . $key},
                ];
        }

        if($key > 0) LandingQuestionAnswers::insert($data);

        return response()->json($data);
    }

    public function detailsLandingPage($user_id, $landing_page_id){
        $landingPages = LandingPage::where('id',$landing_page_id)->with('user', 'assigned_to')->first();
        $landingComments = LandingPageComments::where('landing_page_id', $landing_page_id)->with('user')->latest()->get();

        //return $landingPage;
        return view('_v2/pages/staff/client/details_landing_page',compact('landingPages', 'landingComments'));
    }


    public function saveLandingPageNote(Request $request,$id){

        $landingPageNote =new LandingPageComments();

        $landingPageNote->landing_page_id = $id;
        $landingPageNote->user_id = Auth::user()->id;
        $landingPageNote->description = $request->note;
        $landingPageNote->save();
        $noteLists = $this->landingPageNoteLists($id);

        return response()->json($noteLists);

    }

    public function deleteLandingPageNote($lp_id,$id){

        $landingPageNote = LandingPageComments::find($id);
        $landingPageNote->delete();

        $noteLists = $this->landingPageNoteLists($lp_id);

        return response()->json($noteLists);
    }

    public function landingPageNoteLists($id)
    {
        return  $landingComments = LandingPageComments::where('landing_page_id', $id)->with('user')->latest()->get();
    }
}

<?php

namespace App\Http\Controllers\_v2\Staff;

use App\Http\Controllers\Controller;
use App\Models\Staff\Comment;
use App\Models\Staff\SubTask;
use App\Models\Staff\Task;
use App\Models\Staff\UserTask;
use Illuminate\Http\Request;
use App\Http\Resources\Campaigns as CampaignsResource;
use App\Http\Requests\StoreTask;
use App\Helpers\Helper;
use App\Guide;
use App\User;
use App\LandingPage;
use Auth;
use Carbon\Carbon;


class TaskController extends Controller
{
    public function __construct()
    {
        $this->items = 100;
    }

    public function saveComment(Request $request, $task_id)
    {
        $data = $request->all();
        $data['user_id'] = Auth()->user()->id;
        $data['task_id'] = $task_id;

        Comment::create($data);
        $comments = Comment::where('task_id', $task_id)->with('user')->latest()->get();

        return response()->json($comments);
    }


    public function changeTaskStatus($id, $status)
    {
        $taskStatus = Task::where('id', $id)->first();
        $taskStatus->update(['status' => $status]);

        return response()->json(1);
    }

    public function changeSubTaskStatus($id, $status)
    {
        $subTaskStatus = SubTask::where('id', $id)->first();
        $subTaskStatus->update(['status' => $status]);

        return response()->json(1);
    }

    public function getTasks()
    {
        $tasks = Task::with('user:id,name')->with('comment', 'userTasks');
        if (request()->filled('search'))
            $tasks = $tasks->where('name', 'like', '%' . request()->search . '%')
                ->orWhere('description', 'like', '%' . request()->search . '%');
        $tasks = $tasks->latest()->get()->groupBy('status')->toArray();

        $users = User::orderBy('name')->get(['id', 'name', 'type']);

        $tasks_completed = array_key_exists('completed', $tasks) ? $tasks['completed'] : [];
        $tasks_pending = array_key_exists('pending', $tasks) ? $tasks['pending'] : [];
        $tasks_in_progress = array_key_exists('in-progress', $tasks) ? $tasks['in-progress'] : [];

        return response()->json(['users' => $users, 'tasks' => ['completed' => $tasks_completed, 'pending' => $tasks_pending, 'in_progress' => $tasks_in_progress]]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::with('user:id,name')->with('comment', 'userTasks')->latest()->get()->groupBy('status')->toArray();
        $users = User::orderBy('name')->get(['id', 'name', 'type']);

        $tasks_completed = array_key_exists('completed', $tasks) ? $tasks['completed'] : [];
        $tasks_pending = array_key_exists('pending', $tasks) ? $tasks['pending'] : [];
        $tasks_in_progress = array_key_exists('in-progress', $tasks) ? $tasks['in-progress'] : [];

        return view('_v2.pages.staff.task.index', ['page_title' => 'Task'])
            ->with('users', $users)
            ->with('tasks', ['completed' => $tasks_completed, 'pending' => $tasks_pending, 'in_progress' => $tasks_in_progress]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTask $request)
    {
        $checkList = preg_replace('!\s+!', ' ', (str_replace(', ', ',', $request->checklist)));
        $checkListArr = array_filter(explode(',', trim($checkList)));
        $fileName = '';
        $fileExt = '';

        if (!empty($request->task_id)) {
            $task = Task::findOrFail($request->task_id);
        } else {
            $task = new Task();
            $task->user_id = Auth()->user()->id;
            $task->assigned_by = Auth()->user()->id;
        }

        if ($request->hasFile('attachments')) {
            $basePath = 'app/public/staff/upload';
            $image = $request->file('attachments');
            $fileExt = $image->getClientOriginalExtension();
            $file_name = time() . '.' . $fileExt;
            $file = $image->move(storage_path($basePath), $file_name);
            $fileName = $basePath . '/' . $file_name;
            $task->attachments = $fileName;
            $task->file_type = $fileExt;
        }
        $task->name = $request->name;
        $task->description = $request->description;
        $task->due_date = $request->due_date;
        $task->status = $request->status;
        $task->priority = $request->priority;
        $task->link_type = $request->link_type;
        $task->save();

        $task->userTasks()->delete();
        $task->subTasks()->delete();
        if ($checkListArr) {
            $sub_tasks = [];
            foreach ($checkListArr as $key => $check) {
                $sub_tasks[$key]['task_id'] = $task->id;
                $sub_tasks[$key]['name'] = $check;
                $sub_tasks[$key]['status'] = 'pending';
                $sub_tasks[$key]['created_at'] = date('Y-m-d H:i:s');
                $sub_tasks[$key]['updated_at'] = date('Y-m-d H:i:s');
            }
            SubTask::insert($sub_tasks);
        }
        if ($request->assigned_to) {
            $user_tasks = [];
            foreach (explode(',', $request->assigned_to) as $key => $assigned_to) {
                $user_tasks[$key]['task_id'] = $task->id;
                $user_tasks[$key]['user_id'] = $assigned_to;
                $user_tasks[$key]['created_at'] = date('Y-m-d H:i:s');
                $user_tasks[$key]['updated_at'] = date('Y-m-d H:i:s');
            }
            UserTask::insert($user_tasks);
        }

        return response()->json(1);

    }

    
    public function clientTaskStore(StoreTask $request)
    {
        //dd($request->all());

        $checkList = preg_replace('!\s+!', ' ', (str_replace(', ', ',', $request->checklist)));
        $checkListArr = array_filter(explode(',', trim($checkList)));
        $fileName = '';
        $fileExt = '';

        if (!empty($request->task_id)) {
            $task = Task::findOrFail($request->task_id);
        } else {
            $task = new Task();
            $task->user_id = $request->client_id;
            $task->assigned_by = Auth()->user()->id;
        }

        if ($request->hasFile('attachments')) {
            $basePath = 'app/public/staff/upload';
            $image = $request->file('attachments');
            $fileExt = $image->getClientOriginalExtension();
            $file_name = time() . '.' . $fileExt;
            $file = $image->move(storage_path($basePath), $file_name);
            $fileName = $basePath . '/' . $file_name;
            $task->attachments = $fileName;
            $task->file_type = $fileExt;
        }
        $task->name = $request->name;
        $task->description = $request->description;
        $task->due_date = $request->due_date;
        $task->status = $request->status;
        $task->priority = $request->priority;
        $task->link_type = $request->link_type;
        $task->save();

        $task->userTasks()->delete();
        $task->subTasks()->delete();
        if ($checkListArr) {
            $sub_tasks = [];
            foreach ($checkListArr as $key => $check) {
                $sub_tasks[$key]['task_id'] = $task->id;
                $sub_tasks[$key]['name'] = $check;
                $sub_tasks[$key]['status'] = 'pending';
                $sub_tasks[$key]['created_at'] = date('Y-m-d H:i:s');
                $sub_tasks[$key]['updated_at'] = date('Y-m-d H:i:s');
            }
            SubTask::insert($sub_tasks);
        }
        if ($request->assigned_to) {
            $user_tasks = [];
            foreach (explode(',', $request->assigned_to) as $key => $assigned_to) {
                $user_tasks[$key]['task_id'] = $task->id;
                $user_tasks[$key]['user_id'] = $assigned_to;
                $user_tasks[$key]['created_at'] = date('Y-m-d H:i:s');
                $user_tasks[$key]['updated_at'] = date('Y-m-d H:i:s');
            }
            UserTask::insert($user_tasks);
        }

        return response()->json(1);

    }

    public function updateStaffMember(Request $request)
    {
        $task = Task::findOrFail($request->task_id);
        $task->userTasks()->delete();
        if ($request->assigned_to) {
            $user_tasks = [];
            foreach (explode(',', $request->assigned_to) as $key => $assigned_to) {
                $user_tasks[$key]['task_id'] = $task->id;
                $user_tasks[$key]['user_id'] = $assigned_to;
                $user_tasks[$key]['created_at'] = date('Y-m-d H:i:s');
                $user_tasks[$key]['updated_at'] = date('Y-m-d H:i:s');
            }
            UserTask::insert($user_tasks);
        }
        return $this->show($task->id);
//        $task->userTasks->sync()
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = Task::where('id', $id)
            ->with('user')
            ->with('userTasks')
            ->with('subTasks')
            ->with('comment')
            ->first();
        return response()->json($task);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $check = Task::find($request->id); //new value
        $check->status = $request->status;
        $check->update();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::findOrFail($id);
        $task->userTasks()->delete();
        $task->subTasks()->delete();
        $task->delete();
        return response()->json(1);
    }
}

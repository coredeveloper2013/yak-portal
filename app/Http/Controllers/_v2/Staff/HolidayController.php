<?php

namespace App\Http\Controllers\_v2\Staff;

use App\Http\Controllers\Controller;
use App\Models\Staff\Holiday;
use App\Models\UserHoliday;
use Illuminate\Http\Request;
use App\Http\Resources\Campaigns as CampaignsResource;
use App\Helpers\Helper;
use App\Guide;
use App\User;
use App\LandingPage;
use Auth;

class HolidayController extends Controller
{
    public function __construct(){
        //
    }

    public function updateUserHoliday(Request $request, $user_id)
    {
        UserHoliday::updateOrCreate(['user_id' => $user_id], $request->all());
        $userHolidays = UserHoliday::get()->keyBy('user_id')->map(function($q) { return $q->days; });

        return response()->json($userHolidays);
    }

    public function getUserHolidays()
    {
        $holidays = Holiday::get();
        $usersId  = array_unique($holidays->pluck('userId')->toArray());
        $userHolidays = UserHoliday::get()->keyBy('user_id')->map(function($q) { return $q->days; });
        $userHolidays = UserHoliday::get()->keyBy('user_id');
        $users    = User::whereIn('id', $usersId)->get()->keyBy('id');
        $allUsers = User::get(['id', 'name']);

        $holidays = $holidays->groupBy('userId');

        return response()->json([
            'holidays' => $holidays,
            'users' => $users,
            'allUsers' => $allUsers,
            'userHolidays' => $userHolidays
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $holidays = Holiday::get();
        $usersId  = array_unique($holidays->pluck('userId')->toArray());
        $userHolidays = UserHoliday::get()->keyBy('user_id')->map(function($q) { return $q->days; });
        $users    = User::whereIn('id', $usersId)->get()->keyBy('id');
        $allUsers = User::get(['id', 'name']);

        $holidays = $holidays->groupBy('userId');

        return view('_v2.pages.staff.holiday.holiday_index', ['page_title' => 'Holiday'], compact('holidays', 'users', 'allUsers', 'userHolidays'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function userStore(Request $request)
    {
        $request->validate([
            'description'=> 'required',
            'userId'=> 'required',
            'date_of_holiday'=> 'required',
            'availed'=> 'required',
        ]);

        $data = [
            'userId' => $request->userId,
            'description' => $request->description,
            'date_of_holiday' => $request->date_of_holiday,
            'availed' => $request->availed
        ];

        $holiday = Holiday::create($data);

        return response()->json($holiday);
    }
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'description'=> 'required',
            'userId'=> 'required',
            'date_of_holiday'=> 'required',
            'availed'=> 'required',
        ]);

        $data = [
            'userId' => $request->userId,
            'description' => $request->description,
            'date_of_holiday' => $request->date_of_holiday,
            'availed' => $request->availed
        ];

        $holiday = Holiday::create($data);
        $thisUserHolidays = Holiday::where('userId', $request->userId)->get();

        return response()->json($thisUserHolidays);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $holiday = Holiday::findOrFail($id);
        $holiday->delete();
        return 204;

    }
}

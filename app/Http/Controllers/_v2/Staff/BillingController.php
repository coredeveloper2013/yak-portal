<?php

namespace App\Http\Controllers\_v2\Staff;

use App\Http\Controllers\Controller;
use App\Models\Customer\Invoice;
use Illuminate\Http\Request;
use Auth;
use Stripe;
use App\Agreement;
use Carbon\Carbon;
use App\Helpers\Helper;
use Laravel\Cashier\Cashier;
use PDF;
use Illuminate\Support\Facades\Validator;
class BillingController extends Controller
{

    public function __construct()
    {
        $this->items = 10;
    }

 /*   public function index()
    {
        return view('_v2.pages.staff.billing.staff-billing');
    }
    */
    public function getInvoice()
    {
        $items = request()->filled('items') ? request()->items : $this->items;
        $invoice = Invoice::where('status','pending')->with('user.company');

        if (request()->filled('search')) {
            $invoice = $invoice->whereHas('user.company', function ($query) {
                return $query->where('name', 'LIKE', '%' . request()->search . '%');
            })->orWhereHas('user', function ($query) {
                return $query->where('name', 'LIKE', '%' . request()->search . '%');
            });
        }

        if(request()->filled('sort')) $invoice = $invoice->orderBy('id', request()->sort);
        else  $invoice = $invoice->latest();

        $invoice = $invoice->paginate($items);

        return response()->json($invoice);
    }

    public function index()
    {
        $items = request()->filled('items') ? request()->items : $this->items;
        $invoices = Invoice::where('status','pending')->with('user.company')
            ->latest()
            ->paginate($items);
        return view('_v2.pages.staff.billing.staff-billing',compact('invoices'));
    }

    public function exportPdf(Request $request, $id)
    {
        $this->makeStorageDirectory('export-pdf');

        $invoice = Invoice::query()
            ->where('id',$id)
            ->with('user.company')
            ->first();
        $fileName = "file_" . rand(000000, 999999) . ".pdf";
        $path = storage_path('app/public/export-pdf/') . $fileName;
        PDF::loadView('_v2.export-pdf.invoice', compact('invoice'))->save($path);
        return response()->json(asset("/storage/export-pdf/" . $fileName));
    }

    private function makeStorageDirectory($dir)
    {
        if(!\Storage::disk('local')->has('public/'.$dir)) {
            \Storage::disk('local')->makeDirectory('public/'.$dir);
        }
    }
}

<?php

namespace App\Http\Controllers\_v2\Staff;

use App\Agreement;
use App\AgreementDocuSign;
use App\Models\Company;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCampaign;
use App\Invoice;
use App\InvoiceInstallments;
use App\Notification;
use App\OtherProject;
use App\UserBusinessInformation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Resources\Campaigns as CampaignsResource;
use App\Helpers\Helper;
use App\Models\Staff\Campaign;
use App\Models\User;
use App\LandingPage;
use App\CampaignComments;
use Illuminate\Support\Facades\Auth;

class CampaignsController extends Controller
{
    private $_helper;

    public function __construct(){
        $this->items = 10;
    }
    public function getCampaign()
    {
        $items = request()->filled('items') ? request()->items : $this->items;
        $campaigns = new Campaign();

        if (Auth::user()->type != 'admin') {
            $campaigns = $campaigns->where(function ($q) {
                $q->where('campaign_specialist', Auth::user()->id)
                    ->orWhere('assigned_id', Auth::user()->id)
                    ->orWhere('lp_specialist', Auth::user()->id)
                    ->orWhere('account_manager', Auth::user()->id);
            });
        }

        $campaignAmount = clone $campaigns;

        $campaigns = $campaigns->with('user', 'salesPerson');

        if(request()->status) $campaigns = $campaigns->where('status', request()->status);
        if(request()->search) {
            $campaigns = $campaigns->where('name', 'LIKE', '%' . request()->search . '%')
                ->orWhereHas('user', function ($query) {
                    return $query->where('name', 'LIKE', '%' . request()->search . '%');
                })
                ->orWhereHas('salesPerson', function ($query) {
                    return $query->where('name', 'LIKE', '%' . request()->search . '%');
                });
        }

        $campaigns = $campaigns->orderBy('id', request()->sort)->paginate($items);

        $user_ids  = $campaigns->unique('user_id')->pluck('user_id')->toArray();

        $campaignAmount = $campaignAmount->whereIn('user_id', $user_ids)
            ->select('user_id', \DB::raw('SUM(compaign_amount) as total_compaign_amount'))
            ->groupBy('user_id')
            ->get()
            ->keyBy('user_id');

        return response()->json(['campaigns' => $campaigns, 'campaignAmount' => $campaignAmount]);
    }

    public function index(){

        $items = request()->filled('items') ? request()->items : $this->items;
        $campaigns = new Campaign();

        if (Auth::user()->type != 'admin') {
            $campaigns = $campaigns->where(function ($q) {
                $q->where('campaign_specialist', Auth::user()->id)
                    ->orWhere('assigned_id', Auth::user()->id)
                    ->orWhere('lp_specialist', Auth::user()->id)
                    ->orWhere('account_manager', Auth::user()->id);
            });
        }

        $campaignAmount = clone $campaigns;

        $campaigns = $campaigns->with('user','salesPerson')->latest()->paginate($items);
        $user_ids  = $campaigns->unique('user_id')->pluck('user_id')->toArray();

        $campaignAmount = $campaignAmount->select('user_id', \DB::raw('SUM(compaign_amount) as total_compaign_amount'))
            ->whereIn('user_id', $user_ids)
            ->groupBy('user_id')
            ->get()
            ->keyBy('user_id');

        return view('_v2.pages.staff.campaign.index', ['campaigns' => $campaigns, 'campaignAmount' => $campaignAmount]);
    }
    public function store(StoreCampaign $request)
    {
     // dd(request()->all());
        $sessionUser = Auth::user();



      /*  if (!is_array($request->installment_amount)) {
            $installment_amount = explode(",", $request->installment_amount);
            $request->merge(['installment_amount' => $installment_amount]);
        }

        if (!is_array($request->installment_date)) {
            $installment_date = explode(",", $request->installment_date);
            $request->merge(['installment_date' => $installment_date]);
        }

        if (empty($request->installment_amount)) {
            $request->merge(['installment_amount' => []]);
        }

        if (empty($request->installment_date)) {
            $request->merge(['installment_date' => []]);
        }

        $enable_installment = (!empty($request->enable_installment)) ? $request->enable_installment : 'N';

        if (empty($request->edit_id)) {
            $validationArr['amount'] = 'required|numeric|min:0|not_in:0';
            if ($enable_installment == 'Y') {
                $validationArr['installment_amount.*'] = 'required|numeric|min:0|not_in:0';
                $validationArr['installment_date.*'] = 'required|date';
            }
        }

        if (!empty($request->campaign_specialist)) {
            $validationArr['campaign_specialist'] = 'exists:users,id';
        }

        if (!empty($request->lp_specialist)) {
            $validationArr['lp_specialist'] = 'exists:users,id';
        }

        if (!empty($request->account_manager)) {
            $validationArr['account_manager'] = 'exists:users,id';
        }

        if (empty($request->edit_id)) {*/

            $addUser = new User();
            $addUser->name = $request->user_name;
            $addUser->phone = $request->mobile;
            $addUser->job_position = $request->job_position;
            $addUser->email = $request->email;
            $addUser->password = bcrypt('password');
            $addUser->status = $request->status;
            $addUser->type = 'customer';
            $addUser->save();
            $customerId = $addUser->id;


       /* }
        if (!empty($request->edit_id)) {
            $campaignData = Campaign::find($request->edit_id);
            $addUser = User::find($campaignData->user_id);
            $addUser->name = $request->user_name;
            $addUser->phone = $request->mobile;
            $addUser->job_position = $request->job_position;
            $addUser->email = $request->email;
            $addUser->status = $request->status;
            $addUser->type = 'customer';
            $addUser->save();
            $customerId = $addUser->id;
        }*/

        Company::updateOrCreate(
            [
                'user_id' => $customerId
            ],
            [
                'address' => ($request->address) ? $request->address : null,
                'name' => $request->company_name
            ]
        );


       $UserBusinessInformation= UserBusinessInformation::updateOrCreate(
            [
                'user_id' => $customerId
            ],
            [
                'email_address' => ($request->work_email) ? $request->work_email : null,
                'phone' => ($request->work_phone) ? $request->work_phone : null,
                'address' => ($request->address) ? $request->address : null,
                'website' => ($request->website) ? $request->website : null,
                'facebook_link' => ($request->facebook_link) ? $request->facebook_link : null,
                'twitter_link' => ($request->twitter_link) ? $request->twitter_link : null,
                'Instagram_link' => ($request->Instagram_link) ? $request->Instagram_link : null,
                'notes' => ($request->notes) ? $request->notes : null
            ]
        );


     /*   $installmentArr = [];
        $totalInstallments = 0;
        if (empty($request->edit_id) && $enable_installment == 'Y') {
            if (count($request->installment_amount) < 2) {
                $data['status'] = 'error';
                $data['msg'] = 'Please Enter Atleast 2 installments.';
                $this->_helper->response()->setCode(200)->send($data);
            }

            if (count($request->installment_amount) > 3) {
                $data['status'] = 'error';
                $data['msg'] = 'Only 3 Installments are allowed.';
                $this->_helper->response()->setCode(200)->send($data);
            }

            if (count($request->installment_amount) != count($request->installment_date)) {
                $data['status'] = 'error';
                $data['msg'] = 'Every amount in installment must have date.';
                $this->_helper->response()->setCode(200)->send($data);
            }

            $totalCampaignAmount =  $request->amount;
            $calcAmount = 0;

            $date_change_msg = '';
            foreach ($request->installment_amount as $key => $value) {
                if ($key > 0) {
                    $lastDate = $request->installment_date[$key - 1];
                    $crntDate = $request->installment_date[$key];
                } else {
                    $lastDate = date('Y-m-d', strtotime('-1 day', time())); //date('Y-m-d');
                    $crntDate = $request->installment_date[$key];
                }
                $datediff = strtotime($crntDate) - strtotime($lastDate);
                $days = $datediff / (60 * 60 * 24);

                $keymsg1 = '';
                $keymsg2 = '';

                if (($key + 1) == 1) {
                    $keymsg1 = '1st';
                }

                if (($key + 1) == 2) {
                    $keymsg1 = '2nd';
                }

                if (($key + 1) == 3) {
                    $keymsg1 = '3rd';
                }

                if ($key <= 0) {
                    $keymsg2 = 'today';
                }

                if ($key == 1) {
                    $keymsg2 = '1st installment';
                }

                if ($key == 2) {
                    $keymsg2 = '2nd installment';
                }

                if ($key == 3) {
                    $keymsg2 = '3rd installment';
                }

                if ($days < 1) {
                    $date_change_msg = $keymsg1 . ' installment date must be greater then ' . $keymsg2 . ' date.';
                    break;
                }
                $amount = (!empty($value)) ? (float) $value : 0;
                $calcAmount = $calcAmount + $amount;
            }

            if (!empty($date_change_msg)) {
                $data['status'] = 'error';
                $data['msg'] = $date_change_msg;
                $this->_helper->response()->setCode(200)->send($data);
            }

            if ($totalCampaignAmount != $calcAmount) {
                $data['status'] = 'error';
                $data['msg'] = 'All installments amount must be equal to Contract Value.';
                $this->_helper->response()->setCode(200)->send($data);
            }

            $totalInstallments =  count($request->installment_amount);
        }*/





       /* if (!empty($request->edit_id)) {

            $campaign = $campaign->where('id', $request->edit_id);
            if ($sessionUser->type != 'admin') {
                $campaign = $campaign->where(function ($q) {
                    $q->where('campaign_specialist', Auth::user()->id)
                        ->orWhere('assigned_id', Auth::user()->id)
                        ->orWhere('lp_specialist', Auth::user()->id)
                        ->orWhere('account_manager', Auth::user()->id);
                });
            }

            if (!$campaign->exists()) {
                $data['status'] = 'error';
                $data['msg'] = 'Record with provided Id not found';
                $this->_helper->response()->setCode(200)->send($data);
            }
            $campaign = $campaign->first();

            if (!empty($request->assigned_id)) {
                $campaign->assigned_id = $request->assigned_id;
            }
        } else {*/

        // Check installments
        $enable_installment = 'N';
        if($request->installmentView) $enable_installment = 'Y';

        $campaign = new Campaign();

        $campaign->status = 'pending';
        $campaign->created_by = $sessionUser->id;
        $campaign->assigned_id = $request->assigned_id;
        $campaign->amount = $request->amount;
        $campaign->with_installment = $enable_installment;
        $campaign->installment_count = count($request->installments);


        $campaign->user_id = $customerId;
        $campaign->recurring_option = $request->recurring_option;
        //$campaign->recurring_way = $request->recurring_way;
        //$campaign->landing_pages_id = $request->landing_id;
        $campaign->updated_by = $sessionUser->id;
        $campaign->name = $request->campaign_name;
        $campaign->start_date = Carbon::parse($request->date)->format('Y-m-d');
        $campaign->compaign_amount = $request->compaign_amount;
        //$campaign->type = $request->type;
        $campaign->type = 'one_off';
        $campaign->campaign_specialist = $request->campaign_specialist;
        $campaign->lp_specialist = $request->lp_specialist;
        $campaign->account_manager = $request->account_manager;

        $campaign->save();
        $insertedId = $campaign->id;

            $installmentArr = [];
            $campaignCount = Campaign::where('user_id', $customerId)->get()->count();
            $otherProjectsCount = OtherProject::where('user_id', $customerId)->get()->count();
            $user = User::where('id', $customerId)->first();
            if ($user->status == 'new' && ($campaignCount >= 2 || $otherProjectsCount >= 2)) {
                $user->update(['status' => 'recurring']);
            }

            if ($enable_installment == 'Y') {
                foreach ($request->installments as $key => $value) {
                    $installmentArr[$key] = [
                        'user_id' => $customerId,
                        'module_id' => $insertedId,
                        'module_type' => 'campaigns',
                        'amount' => $value['amount'],
                       'installment_date' => date('Y-m-d', strtotime($value['date']))
                    ];

                }
                foreach ($installmentArr as $key => $value) {
                    InvoiceInstallments::create([
                        'user_id' => $value['user_id'],
                        'module_id' => $value['module_id'],
                        'module_type' => $value['module_type'],
                        'amount' => $value['amount'],
                        'installment_date' => $value['installment_date']
                    ]);
                }
            } else {
                $oInvoice = new Invoice();
                $vat = $oInvoice->vat;
                $amount = $request->amount;
                $totalAmount = $amount + (($amount / 100) * $vat);

                Invoice::create([
                    'user_id' => $customerId,
                    'type' => 'campaign',
                    'status' => 'pending',
                    'amount' => $amount,
                    'vat' => $vat,
                    'total_amount' => $totalAmount,
                    'campaign_id' => $campaign->id,
                    'landing_pages_id' => null
                ]);
            }


            Notification::create([
                'user_id' => $customerId,
                'campaign_id' => $campaign->id,
                'type' => 'sms',
                'for' => 'customer'
            ]);

            Notification::create([
                'user_id' => $customerId,
                'campaign_id' => $campaign->id,
                'type' => 'email',
                'for' => 'customer'
            ]);

            $agreement = Agreement::create([
                'user_id' => $customerId,
                'campaign_id' => $campaign->id,
            ]);

            $agreementSign = AgreementDocuSign::create([
                'user_id' => $customerId,
                'module_id' => $agreement->id,
                'type' => 'agreements',
            ]);
            return response()->json('Success');

    }
    public function create(){
      // $landing_pages = LandingPage::all();
        $users = User::where('type','staff')->get();

        return view('_v2.pages.staff.campaign.create',['users' => $users]);
    }

    public function detail($id=''){
        $withArray = [];
        $withArray['user'] = function ($query) {
            $query->with('company');
            $query->with('business_info');
            $query->withTrashed();
        };

        if (request()->has('tab') && isset(request()->tab)) {
            if (request()->tab == 'notes') {
                $withArray['campaignComments'] = function ($query) {
                    $query->with([
                        'user' => function ($query1) {
                            $query1->withTrashed();
                        }
                    ]);
                };
            } elseif (request()->tab == 'assign_detail') {
                $withArray[] = 'campaignSpecialist';
                $withArray[] = 'salesPerson';
                $withArray[] = 'lpSpecialist';
                $withArray[] = 'accounManager';
            } elseif (request()->tab == 'installment') {
                $withArray[] = 'installments';
            }
        } else {
            $withArray[] = 'landing_pages';
            $withArray[] = 'campaignComments';
            $withArray[] = 'campaignSpecialist';
            $withArray[] = 'salesPerson';
            $withArray[] = 'lpSpecialist';
            $withArray[] = 'accounManager';
            $withArray[] = 'invoice';
            $withArray['invoice'] = function ($query1) {
                $query1->orderBy('id', 'DESC')->first();
            };
            $withArray[] = 'installments';
            $withArray[] = 'agreement';
        }

        $campaign = Campaign::with($withArray)->where('id', $id);
        if (!$campaign->exists()) {
            return abort(404);
        }
        $campaign = $campaign->first();
        $is_retention = Campaign::where('user_id', $campaign->user_id)->where('id', '<', $campaign->id)->count();
        if (request()->wantsJson()) {
            $data = ['campaign' => $campaign, 'is_retention' => $is_retention];
            $this->_helper->response()->send(['data' => $data]);
        } else {
            return view('_v2.pages.staff.campaign.campaign_detail', compact('campaign', 'is_retention'));
        }
    }

    public function postComment()
    {
        $this->_helper->runValidation([
            'id' => 'required|exists:campaigns,id',
            'text' => 'required'
        ]);

        $request = request();

        CampaignComments::create([
            'campaign_id' => $request->id,
            'user_id' => Auth::user()->id,
            'description' => $request->text
        ]);

        $campaign = Campaign::with(['campaignComments'])->find($request->id);

        $this->_helper->response()->send(['data' => $campaign]);
    }
}

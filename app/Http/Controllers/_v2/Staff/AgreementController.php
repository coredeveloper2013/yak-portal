<?php

namespace App\Http\Controllers\_v2\Staff;

use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use App\Models\Customer\AgreementDocuSign;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Models\Customer\Agreement;
use Auth;

class AgreementController extends Controller
{
    protected $_helper;

    public function __construct()
    {
        $this->items = 10;
    }

    public function getAgreement()
    {
        $items = request()->filled('items') ? request()->items : $this->items;

        $agreements = Agreement::query();
        if (request()->filled('status')) $agreements = $agreements->where('status', request()->status);
        if (request()->filled('search')) {
            $agreements = $agreements->whereHas('campaign', function ($campaign) {
                $campaign->where('name', 'LIKE', '%' . request()->search . '%')
                    ->orWhereHas('user', function ($user) {
                        $user->where('name', 'LIKE', '%' . request()->search . '%')
                            ->orWhereHas('company', function ($company) {
                                $company->where('name', 'LIKE', '%' . request()->search . '%');
                            });
                    });
            });
        }
        $agreements = $agreements->with('campaign', 'user', 'company', 'document')
            //->where('user_id', Auth()->user()->id)
            ->latest()
            ->paginate($items);

        return response()->json($agreements);
    }

    public function index()
    {
        $items = request()->filled('items') ? request()->items : $this->items;
        $agreements = Agreement::query()
            ->with('campaign')
            ->with('user')
            ->with('company')
            ->with('document')
            //->where('user_id', Auth()->user()->id)
            ->latest()
            ->paginate($items);

        return view('_v2.pages.staff.agreement.index', compact('agreements'));
    }

    public function destroy($id)
    {
        $agreement = Agreement::where('id', $id)->delete();
        return response()->json($agreement);
    }

    public function add()
    {

        $request = request();

        $messages = [
            'amount.min' => 'Please enter positive integer'
        ];

        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'date' => 'required|date',
            'amount' => 'required|numeric|min:0|not_in:0',
            'status' => 'required|in:completed,pending'
        ], $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        \App\Agreement::create([
            'user_id' => Auth::user()->id,
            'name' => $request->name,
            'date' => Carbon::parse($request->date)->format('m/d/Y'),
            'amount' => $request->amount,
            'status' => $request->status
        ]);

        return redirect()->route('agreements');
    }

    public function updateAgreementStatus($id){

        $agreement = AgreementDocuSign::where('module_id',$id)->first();;
        $agreement->status = 'signed';
        $agreement->update();

        return response()->json($agreement);
    }
}

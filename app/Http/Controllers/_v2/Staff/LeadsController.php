<?php

namespace App\Http\Controllers\_v2\Staff;

use App\Models\Staff\Campaign;
use App\Http\Controllers\Controller;
use App\Notification;
use Illuminate\Http\Request;
use App\Http\Resources\Leads as LeadsResource;
use App\Http\Resources\LeadComments as LeadCommentsResource;
use App\Models\Leads;
use App\LeadComments;
use App\LeadQuestion;
use App\Helpers\Helper;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class LeadsController extends Controller
{
    private $_helper;

    public function __construct()
    {
        $this->_helper = new Helper();
        $this->items = 3;
    }


    public function getLeadsByCampaign(Request $request, $campaignId)
    {
        $items = request()->filled('items') ? request()->items : $this->items;

        $leads = new Leads();
        $leads = $leads->where('campaign_id', $campaignId);
        if(!empty($request->search)){
            $leads = $leads->with('lead_questions');
            $leads = $leads->whereHas('lead_questions', function ($query) {
                return $query->where('answers', 'LIKE', '%' . request()->search . '%');
        });
        }
        $leads = $leads->with('campaign:id,name')
            ->orderBy('id', 'DESC')
            ->paginate($items);
        return response()->json($leads);
    }

    public function index(Request $request, $id)
    {
        $items = request()->filled('items') ? request()->items : $this->items;

        $leads = new Leads();
        $leads = $leads->where('campaign_id', $id)
                ->with('campaign:id,name')
                ->orderBy('id', 'DESC')
                ->paginate($items);

        $campaign = Campaign::find($id,['id','name']);

        return view('_v2/pages/staff/Lead/index', compact('leads','campaign'));
    }

    public function status(Request $request, $leadId, $campaignId)
    {
        $leads = new Leads();
        $leads = $leads->where('id', $leadId)->where('campaign_id', $campaignId)->first();
        $leads->status = $request->status;
        $leads->save();

        return response()->json($leads);
    }

    public function getLead(Request $request, $leadId, $campaignId)
    {
        $leads = new Leads();

        if(!empty($leadId)){
            $leads = $leads->with('lead_questions')
                ->with('leadCommentsWithUser')
                ->with('user:id,name')
                ->where('id', $leadId)
                ->where('campaign_id', $campaignId)
                ->orderBy('id', 'DESC');
        }
        $leads = $leads->first();
        return response()->json($leads);
    }

   /* public function leadSearch(Request $request)
    {
        $leads = new Leads();
        $leads = $leads->where('campaign_id',$request->campaign);
        if(!empty($request->search)){
            $leads = $leads->whereHas('lead_questions', function ($query) use ($request) {
                $query->where('answers', 'like', '%' . $request->search . '%');
            });
        }

        if (!empty($request->last_lead_id)) {
            $leads = $leads->where('id', '<', $request->last_lead_id);
        }

        $leads = $leads->limit(10);
        $leads = $leads->orderBy('id', 'DESC');

        $this->_helper->checkRecordExist($leads);
        $leads = $leads->get();


        if(!empty($leads)){
            $leads = LeadsResource::collection($leads);
        } else {
            $leads = [];
        }

        $data['data']['leads'] = $leads;

        $this->_helper->response()->setCode(200)->send($data);
    }*/

    public function saveLeads(Request $request,$id)
    {
        $leads = new Leads();
        $leads->campaign_id = $id;
        $leads->user_id = Auth::user()->id;
        $leads->submitted_on = Carbon::now();
        $isSaved = $leads->save();
        $insertedId = $leads->id;

        $question = [];

        foreach($request->questions as $key => $value) {
            $question[$key] = [
                'user_id' => Auth::user()->id,
                'lead_id' => $insertedId,
                'headings' => $value['heading'],
                'answers' => $value['answer'],
                'created_at' => Carbon::now(),
                'updated_at' =>  Carbon::now()
            ];
        }

        $leadQuestion = LeadQuestion::insert($question);

        return response()->json($leadQuestion);
    }

    public function saveComments(Request $request, $id)
    {
        $leads = new LeadComments();
        $leads->user_id = Auth::user()->id;
        $leads->lead_id = $id;
        $leads->message = $request->message;
        $isSaved = $leads->save();

        return response()->json($isSaved);
    }

    public function getComments(Request $request)
    {
        $this->_helper->runValidation([
            'id' => 'required',
        ],[
            'id.required' => 'Lead required',
        ]);

        $data = [
            'status' => 'success',
            'msg' => 'Success.',
        ];

        $comments = new LeadComments();

        $comments =  $comments->with(['user']);
        //$comments =  $comments->where('user_id', Auth::user()->id);
        $comments =  $comments->where('lead_id', $request->id);

        $this->_helper->checkRecordExist($comments);

        $comments = $comments->get();

        $data['data']['comments'] = LeadCommentsResource::collection($comments);

        $this->_helper->response()->setCode(200)->send($data);
    }

    public function destroy(Request $request) {
        if (Auth::user()->type != 'admin') {
            $data = [
                'status' => 'error',
                'mob_status' => 'success',
                'msg' => 'You are not authorized to complete this action.',
            ];
            $this->_helper->response()->setCode(200)->send($data);
        }
        $leads = new Leads();
        $leads = $leads->find($request->lead);
        $leads->delete();
        $leads->save();
        $data = [
            'status' => 'success',
            'msg' => 'Lead Successfully deleted',
        ];
       $this->_helper->response()->setCode(200)->send($data);
    }
}

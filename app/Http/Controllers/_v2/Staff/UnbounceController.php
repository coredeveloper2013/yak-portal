<?php

namespace App\Http\Controllers\_v2\Staff;

use Illuminate\Http\Request;

use Auth;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Staff\Campaign;
use App\Models\Staff\Unbounce;
use Exception;
use Illuminate\Support\Facades\Validator;
class UnbounceController extends Controller
{

    protected $_helper;

    public function __construct()
    {
        $this->items = 3;
    }

    public function index()
    {
        $items = request()->filled('items') ? request()->items : $this->items;
        $users = User::orderBy('name')->get(['id', 'name']);
        $unbounce  = Unbounce::with('campaigns', 'user')/*->where('user_id', Auth::id())*/->paginate($items);

        return view('_v2.pages.staff.unbounce.index', ['page_title' => 'Unbounce','unbounce'=>$unbounce,'users'=>$users]);


	}

    public function getUnbounce()
    {
        $items = request()->filled('items') ? request()->items : $this->items;

        $unbounce = Unbounce::with('campaigns', 'user')/*->where('user_id', Auth::id())*/;
        if (request()->filled('search')) {
            $unbounce = $unbounce->where('unbounce_id', 'LIKE', '%' . request()->search . '%');
            $unbounce = $unbounce->orWhereHas('user', function($q){
                $q->where('name', 'LIKE', '%' . request()->search  . '%');
            });
        }
        if (request()->sort) $unbounce= $unbounce->orderBy('id',request()->sort);
        $unbounce=  $unbounce->paginate($items);

        return response()->json($unbounce);
    }


    public function store(Request $request)
    {
        $data=$request->except(['campaign_id','unbounce_id']);
        if($request->is_new_bussiness_lead){
            $data['is_new_bussiness_lead'] = 1;
            $data['campaign_id']           = 0;
        }else{
            $data['is_new_bussiness_lead'] = 0;
            $data['campaign_id']           = $request->campaign_id;

        }

        $data['user_id'] = $request->user_id;
        $data['unbounce_id'] = $request->unbounce_id;

        Unbounce::create($data);
        return response()->json(1);
    }

    public function cron()
    {
        $appKey = 'd12f072ccc809b29767b92fec66c9b27';
        $Unbounce=Unbounce::all();
        // $Unbounce=Unbounce::whereDay('created_at', now()->day)->get();

        foreach ($Unbounce as $ukey => $unb) {
           $userId=$unb->user_id;
           $campaignId=$unb->campaign_id;
           $unbounceId=$unb->unbounce_id;
            $url = 'https://api.unbounce.com/pages/'.$unbounceId.'/leads?limit=1000';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, "$appKey");
            $result = curl_exec($ch);
            curl_close($ch);
            $result=  json_decode(  $result,true);

           //call API
           if(!empty($result['leads'])) {
               foreach ($result['leads'] as $key => $value) {
                   $unbLeadsId = $value['id'];
                   $exsit = Leads::where(['unbounce_id' => $value['id'], 'campaign_id' => $campaignId])->get();
                   if (!empty($exsit[0])) {
                       continue;
                   }
                   $leads = new Leads();
                   $leads->campaign_id = $campaignId;
                   $leads->user_id = $userId;
                   $leads->unbounce_id = $unbLeadsId;
                   $leads->submitted_on = Carbon::now();
                   $isSaved = $leads->save();
                   $insertedId = $leads->id;
                   if ($insertedId) {
                       foreach ($value['form_data'] as $key2 => $value2) {
                           LeadQuestion::create([
                               'user_id' => $userId,
                               'lead_id' => $insertedId,
                               'headings' => $key2,
                               'answers' => $value2[0]
                           ]);
                       }
                   }
               }
           }
       }
    }

    public function destroy($id)
    {
        try{
            Unbounce::where('id',$id)->delete();
            return response()->json(1);
        }catch(Exception $e){
            return response()->json($e);
        }
    }

    public function getAllCampaigns()
    {
        $campaigns = Campaign::get(['id', 'name']);
        return response()->json(['campaigns' => $campaigns]);
    }

    public function getAllCustomers()
    {
        $customers = User::where('type', 'customer')->orderBy('name', 'asc')->get(['id', 'name']);
        return response()->json(['customers' => $customers]);
    }

}

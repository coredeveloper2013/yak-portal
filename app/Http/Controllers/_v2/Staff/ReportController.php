<?php

namespace App\Http\Controllers\_v2\Staff;

use App\Http\Controllers\Controller;
use App\Invoice;
use App\Models\Appointment;
use App\Models\Staff\Task;
use App\User;
use App\Services\AppointmentService;
use DB;

//use Carbon\Carbon;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function index()
    {
        return view('_v2.pages.staff.report.report');
    }

    public function reportData(Request $request)
    {
        if ($request->month) {
            $month = $request->month;
        } else {
            $month = date('F');
        }
        if ($request->year) {
            $year = $request->year;
        } else {
            $year = date('Y');
        }
        $data['geInvoiceAmountByMonth'] = $this->geInvoiceAmountByMonth($month, $year);
        $data['monthlyRevenue'] = $this->monthlyRevenue($year);
        $data['billingTotalSale'] = $this->billingTotalSale($request);
        $last_6_months = [];
        for ($x = 0; $x <= 6; $x++) {
            $last_6_months[$x] = Carbon::now()->subMonth($x)->format('F'); // November
            $last_6_years[$x] = Carbon::now()->subYear($x)->format('Y'); // November
        }
        $data['last_6_months'] = $last_6_months;
        $data['last_6_years'] = $last_6_years;
        return response()->json($data);
    }

    public function geInvoiceAmountByMonth($month, $year)
    {
        $date = date_parse($month);
        $data = [];
        $users = User::with('clientPaymentInvoices')->where('type', 'staff')->get();
        foreach ($users as $user) {
            $data['data'][] = Invoice::where('user_id', $user->id)->whereMonth('paid_date', $date['month'])->whereYear('paid_date', $year)->sum('total_amount');
            $data['name'][] = $user->name;
        }
        return $data;
    }

    public function monthlyRevenue($year)
    {
        $data = [];
        $month_lists = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        foreach ($month_lists as $month_list) {
            $invoices_count = Invoice::with('user')->whereHas('user', function ($q) {
                $q->where('type', 'staff');
            })->whereMonth('paid_date', $month_list)->whereYear('paid_date', $year)->where('status', 'completed')->sum('total_amount');
            $data['count'][] = $invoices_count;
            $data['monthname'][] = $month_list;
        }
        return $data;

    }

    public function billingTotalSale(Request $request)
    {
        $data = [];
        $oLeadDeal = new Invoice();
        $list = $oLeadDeal->typeArr;

        if (!empty($request->from_date) && !empty($request->to_date)) {
            $sales = Invoice::groupBy('type')
                ->selectRaw('sum(total_amount) as sum, type')
                ->whereBetween('created_at', [$request->from_date . " 00:00:00", $request->to_date . " 23:59:59"])
                ->pluck('sum', 'type');
        } else {
            $sales = Invoice::groupBy('type')
                ->selectRaw('sum(total_amount) as sum, type')
                ->pluck('sum', 'type');
        }

        $total_amount = 0;
        $data['amount'] = [];
        $data['type'] = [];
        foreach ($sales as $key => $sale) {
            $data['amount'][] = (int)$sale;
            $total_amount += (int)$sale;
            $data['type'][] = $list[$key];
        }
        $data['total_amount'] = $total_amount;
        return $data;

    }
}

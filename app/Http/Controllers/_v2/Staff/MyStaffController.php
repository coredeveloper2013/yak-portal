<?php

namespace App\Http\Controllers\_v2\Staff;

use App\Http\Controllers\Controller;
use App\Models\Staff\SubTask;
use App\Models\Staff\Task;
use App\Models\Staff\UserTask;
use App\Models\Route;
use App\Models\RoutePermission;
use Illuminate\Http\Request;
use App\Http\Resources\Campaigns as CampaignsResource;
use App\Helpers\Helper;
use App\Guide;
use App\User;
use App\LandingPage;
use Auth;
use Carbon\Carbon;


class MyStaffController extends Controller
{
    public function __construct()
    {
        $this->items = 3;
    }


    public function updateMyStaff(Request $request, $user_id)
    {
        $user = User::whereId($user_id)->first();

        $data = [
            'name' => $request->name,
            'username' => $request->username,
            'phone' => $request->phone,
            'email' => $request->email,
            'type' => $request->type,
            'member_color' => $request->member_color,
            'status' => $request->status,
            'no_of_leave' => $request->no_of_leave,
            'rights' => $request->rights
        ];

        $user = $user->update($data);

        $staff = [];

        return response()->json(['status' => $user, 'staff' => $staff]);
    }

    public function saveRoutesPermission(Request $request, $user_id)
    {
        $data = $request->all();
        $routePermission = RoutePermission::where('user_id', $user_id)->delete();

        if(count($data) > 0) $routePermission = RoutePermission::insert($data);

        return response()->json($routePermission);
    }

    public function getRoutes($user_id)
    {
        $routes = Route::where('is_customer', 0)->get();
        $userRoutes = RoutePermission::where('user_id', $user_id)->get(['user_id', 'route_id']);
        return response()->json(['routes' => $routes, 'userRoutes' => $userRoutes]);
    }

    public function getMyStaff()
    {
        $items = request()->filled('items') ? request()->items : $this->items;

        $query = User::query();

        if(request()->filled('status')) $query = $query->where('type', request()->status);
        else                                $query = $query->where('type', '!=', 'customer');
        if(request()->filled('search')) $query = $query->where('name', 'like',  "%".request()->search."%");

        $staff = $query->latest()->paginate($items);

        return response()->json($staff);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = request()->filled('items') ? request()->items:$this->items;

        $staff = User::where('type', "!=", 'customer')->latest()->paginate($items);
        return view('_v2.pages.staff.my-staff.index', compact('staff'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

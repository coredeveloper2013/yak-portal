<?php

namespace App\Http\Controllers\_v2\staff;

use App\Campaign;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Customer\LeadDeal;
//use App\User;
use Auth;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('_v2.pages.staff.home.home-2');
    }


    public function dashboardData(Request $request)
    {
        $auth = Auth::user();
        $data = [];
        $data['auth'] = $auth;
        $data['campaigns_data'] = $this->leadsChartData($request);
        // $data['campaigns_data'] = $this->campaignLiveChart($request);
        $data['tasks'] = $auth->staffTasks()->take(5)->get();
        $data['appointments'] = $auth->appointments()->with('user')->take(5)->get();

        $clients = User::query()
            ->where('type', 'customer')
            ->with('dashboardLandingPages', 'dashboardStaffCampaigns');

        /*if(request()->findBy) $clients = $clients->where('status', request()->findBy);
        if(request()->status) $clients = $clients->where('status', request()->status);*/

        $clients = $clients->get();

        $data['user_id'] = [];
        $data['On_Boarding'] = $clients->count();
        $data['LP_Build'] = 0;
        $data['LP_IR'] = 0;
        $data['LP_ER'] = 0;
        $data['Campaign_Build'] = 0;
        $data['Internal_Approval'] = 0;
        $data['Live'] = 0;
        $data['Paused'] = 0;
        $data['Lost'] = 0;

        foreach ($clients as $key => $client) {
            $dashboardLandingPages = $client->dashboardLandingPages->keyBy('status')->toArray();
            $dashboardStaffCampaigns = $client->dashboardStaffCampaigns->keyBy('status')->toArray();

            $data['LP_Build'] += array_key_exists('awaiting_build', $dashboardLandingPages) ? 1 : 0;
            $data['LP_IR'] += array_key_exists('business_review', $dashboardLandingPages) ? 1 : 0;
            $data['LP_ER'] += array_key_exists('customer_review', $dashboardLandingPages) ? 1 : 0;

            $data['Campaign_Build'] += array_key_exists('awaiting', $dashboardStaffCampaigns) ? 1 : 0;
            $data['Internal_Approval'] += array_key_exists('billing_detail', $dashboardStaffCampaigns) ? 1 : 0;
            $data['Live'] += array_key_exists('live', $dashboardStaffCampaigns) ? 1 : 0;
            $data['Paused'] += array_key_exists('pending', $dashboardStaffCampaigns) ? 1 : 0;

            $data['Lost'] += $client->status == 'lost' ? 1 : 0;
            $data['user_id'][$key] = $client->id;
        }

        return response()->json($data);

    }

    public function leadsChartData($request)
    {

        $campaigns_data = [];
        $series_data = [];
        $campaigns_data['categories'] = [];

        $users = User::with('leaddeals')->where('type', 'customer')->get();
        $oLeadDeal = new LeadDeal();
        $status_lists = $oLeadDeal->statusEnums;
        $key_val = [];
        $k = 0;
        foreach ($users as $user) {

            foreach ($status_lists as $status_key => $key_name) {
                if (!empty($request->campaign_from_date) && !empty($request->campaign_to_date)) {
                    $get_camp_status_Data = $user->leaddeals->whereBetween('created_at', [$request->campaign_from_date . " 00:00:00", $request->campaign_to_date . " 23:59:59"])
                            ->where('status', $status_key)->count();
                } else {
                    $get_camp_status_Data = $user->leaddeals->where('status', $status_key)->count();
                }
                if (in_array($status_key, $key_val)) {
                    $index = array_search($status_key, $key_val);
                    $array = $series_data[$index]['data'];
                    array_push($array, $get_camp_status_Data);
                    $series_data[$index] = array(
                        'name' => array_key_exists($status_key, $status_lists) ? ucwords(strtolower($status_lists[$status_key])):'',
                        'data' => $array,
                    );
                } else {
                    $key_val[$k] = $status_key;
                    $series_data[$k] = array(
                        'name' => array_key_exists($status_key, $status_lists) ? ucwords(strtolower($status_lists[$status_key])):'',
                        'data' => [$get_camp_status_Data],
                    );
                    $k++;
                }
            }
            $campaigns_data['categories'][] = $user->name;
        }
        $campaigns_data['series'] = $series_data;
        return $campaigns_data;


//        $campaigns_data = [];
//        $series_data = [];
//        $campaigns_data['categories'] = [];
////        $users = User::with('campaignCreatedBy')->whereHas('campaignCreatedBy', function ($q) {
//////            $q->where('created_by', Auth::user()->id);
////        })->get();
//        $campaigns = new Campaign();
//        if (!empty($request->campaign_from_date) && !empty($request->campaign_to_date)) {
//            $campaigns = $campaigns->whereDate('created_at', '>=', $request->campaign_from_date)
//                ->whereDate('created_at', '<=', $request->campaign_to_date);
//        }
//
//        if (Auth::user()->type != 'admin') {
//            $campaigns = $campaigns->where(function ($q) {
//                $q->where('campaign_specialist', Auth::user()->id)
//                    ->orWhere('assigned_id', Auth::user()->id)
//                    ->orWhere('lp_specialist', Auth::user()->id)
//                    ->orWhere('account_manager', Auth::user()->id);
//            });
//        }
//
//
//        $campaigns = $campaigns->with('user')->groupBy('user_id')->get();
//        $status = Campaign::groupBy('status')->pluck('status');
//        $key_val = [];
//        $k = 0;
//        foreach ($campaigns as $key2 => $campaign) {
//            foreach ($status as $status_key) {
//                $get_camp_status_Data = new Campaign();
//                if (!empty($request->campaign_from_date) && !empty($request->campaign_to_date)) {
//                    $get_camp_status_Data = $get_camp_status_Data->whereDate('created_at', '>=', $request->campaign_from_date)
//                        ->whereDate('created_at', '<=', $request->campaign_to_date);
//                }
//
//                $get_camp_status_Data = $get_camp_status_Data->where('user_id', $campaign->user_id)->where('status', $status_key)->count();
//                if (in_array($status_key, $key_val)) {
//                    $index = array_search($status_key, $key_val);
//                    $array = $series_data[$index]['data'];
//                    array_push($array, $get_camp_status_Data);
//                    $series_data[$index] = array(
//                        'name' => $status_key,
//                        'data' => $array,
//                    );
//                } else {
//                    $key_val[$k] = $status_key;
//                    $series_data[$k] = array(
//                        'name' => $status_key,
//                        'data' => [$get_camp_status_Data],
//                    );
//                    $k++;
//                }
//            }
//            $campaigns_data['categories'][] = $campaign->user->name;
//        }
//        $campaigns_data['series'] = $series_data;
//        return $campaigns_data;
    }
    public function campaignLiveChart($request)
    {

        $campaigns_data = [];
        $series_data = [];
        $campaigns_data['categories'] = [];

        $users = User::with('campaigns')->where('type', 'customer')->get();
        $status_lists = Campaign::groupBy('status')->pluck('status');
        $key_val = [];
        $k = 0;
        foreach ($users as $user) {

            foreach ($status_lists as $status_key) {
                if (!empty($request->campaign_from_date) && !empty($request->campaign_to_date)) {
                    $get_camp_status_Data = $user->campaigns->whereBetween('created_at', [$request->campaign_from_date . " 00:00:00", $request->campaign_to_date . " 23:59:59"])
                            ->where('status', strtolower($status_key))->count();
                } else {
                    $get_camp_status_Data = $user->campaigns->where('status', strtolower($status_key))->count();
                }
                if (in_array($status_key, $key_val)) {
                    $index = array_search($status_key, $key_val);
                    $array = $series_data[$index]['data'];
                    array_push($array, $get_camp_status_Data);
                    $series_data[$index] = array(
                        'name' => $status_key,
                        'data' => $array,
                    );
                } else {
                    $key_val[$k] = $status_key;
                    $series_data[$k] = array(
                        'name' => $status_key,
                        'data' => [$get_camp_status_Data],
                    );
                    $k++;
                }
            }
            $campaigns_data['categories'][] = $user->name;
        }
        $campaigns_data['series'] = $series_data;
        return $campaigns_data;


//        $campaigns_data = [];
//        $series_data = [];
//        $campaigns_data['categories'] = [];
////        $users = User::with('campaignCreatedBy')->whereHas('campaignCreatedBy', function ($q) {
//////            $q->where('created_by', Auth::user()->id);
////        })->get();
//        $campaigns = new Campaign();
//        if (!empty($request->campaign_from_date) && !empty($request->campaign_to_date)) {
//            $campaigns = $campaigns->whereDate('created_at', '>=', $request->campaign_from_date)
//                ->whereDate('created_at', '<=', $request->campaign_to_date);
//        }
//
//        if (Auth::user()->type != 'admin') {
//            $campaigns = $campaigns->where(function ($q) {
//                $q->where('campaign_specialist', Auth::user()->id)
//                    ->orWhere('assigned_id', Auth::user()->id)
//                    ->orWhere('lp_specialist', Auth::user()->id)
//                    ->orWhere('account_manager', Auth::user()->id);
//            });
//        }
//
//
//        $campaigns = $campaigns->with('user')->groupBy('user_id')->get();
//        $status = Campaign::groupBy('status')->pluck('status');
//        $key_val = [];
//        $k = 0;
//        foreach ($campaigns as $key2 => $campaign) {
//            foreach ($status as $status_key) {
//                $get_camp_status_Data = new Campaign();
//                if (!empty($request->campaign_from_date) && !empty($request->campaign_to_date)) {
//                    $get_camp_status_Data = $get_camp_status_Data->whereDate('created_at', '>=', $request->campaign_from_date)
//                        ->whereDate('created_at', '<=', $request->campaign_to_date);
//                }
//
//                $get_camp_status_Data = $get_camp_status_Data->where('user_id', $campaign->user_id)->where('status', $status_key)->count();
//                if (in_array($status_key, $key_val)) {
//                    $index = array_search($status_key, $key_val);
//                    $array = $series_data[$index]['data'];
//                    array_push($array, $get_camp_status_Data);
//                    $series_data[$index] = array(
//                        'name' => $status_key,
//                        'data' => $array,
//                    );
//                } else {
//                    $key_val[$k] = $status_key;
//                    $series_data[$k] = array(
//                        'name' => $status_key,
//                        'data' => [$get_camp_status_Data],
//                    );
//                    $k++;
//                }
//            }
//            $campaigns_data['categories'][] = $campaign->user->name;
//        }
//        $campaigns_data['series'] = $series_data;
//        return $campaigns_data;
    }

    public function dashboardCampaign(Request $request)
    {
        $campaigns_data = $this->campaignLiveChart($request);
        return response()->json($campaigns_data);
    }
}

<?php

namespace App\Http\Controllers;


use App\User;
use App\AgreementDocuSign;
use Illuminate\Http\Request;
use Library\Zoho as ZohoLibrary;
use Illuminate\Support\Facades\DB;

class ZohoController extends Controller {

    public function createCode() {
    	$url = ZohoLibrary::genrateCode();
    	return redirect()->away($url);
    }

    public function createToken() {
    	$request = request();
    	ZohoLibrary::genrateToken($request);
    }

    public function refreshToken() {
    	$url = ZohoLibrary::genrateRefreshToken();
    }

    public function sendDocument() {
        $oUser = new User();
        $docnuments = DB::select("SELECT * FROM zoho_douments_listing WHERE status = 'pending' ORDER BY id ASC");
        if (!empty($docnuments)) {
            foreach ($docnuments as $key => $value) {
                $agreements = DB::select("SELECT * FROM agreements WHERE id = ".$value->module_id);
                $campaigns = DB::select("SELECT * FROM campaigns WHERE id = ".$agreements[0]->campaign_id);
                $value->amount = $campaigns[0]->amount;
                $userData = $oUser->getUserDataById($value->user_id);
                $value->name = $userData['name'];
                $value->email = $userData['email'];
                $status = ZohoLibrary::sendDocument($value);
            }
        }
    }

    public function getDocumentStatus() {
        $docnuments = DB::select("SELECT * FROM zoho_douments_listing WHERE status = 'send' ORDER BY id ASC");
        if (!empty($docnuments)) {
            foreach ($docnuments as $key => $value) {
                $status = ZohoLibrary::getDocuStatus($value);
            }
        }
    }
}

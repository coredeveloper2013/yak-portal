<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Services\AppointmentService;
use App\Services\LeadService;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AppointmentController extends Controller
{
    public function __construct()
    {
        $this->items = 20;
    }

//    public function index(){
//        return view('_v2.pages.appointments.index');
//    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = request()->filled('items') ? request()->items : $this->items;
        $appointments = Appointment::where('member_id', Auth::user()->id)
            ->with('member')
            ->orderBy('date', 'desc')
            ->paginate($items);
        $members = User::all();

        return view('_v2.pages.appointments.appointments-list', compact('members', 'appointments'));
    }

    public function appointmentCalender(Request $request)
    {
        $events = $this->calenderEvents();
        $dots = $this->datePickerDots();
        $members = User::where('type', 'customer')->get();
        return view('_v2.pages.appointments.appointment-calender', compact('members', 'events', 'dots'));
    }

    public function datePickerDots()
    {
        $dots = [];
        $users = User::with('memberAppointments')->whereHas('memberAppointments', function ($q) {
            $q->where('user_id', Auth::user()->id);
        })->get();

        foreach ($users as $user) {
            $dates = [];
            foreach ($user->memberAppointments as $date) {
                $dates[] = date('Y-m-d', strtotime($date->date));
            }
            $dots[] = array(
                'dot' => $user->member_color,
                'dates' => $dates,
            );
        }
        return json_encode($dots);
    }

    public function calenderEvents()
    {
        $events = [];
        $appointments = Appointment::with('member')->where('user_id', Auth::user()->id)->orderBy('date', 'desc')->get();
        foreach ($appointments as $appointment) {
            $events[] = array(
                'title' => $appointment->title,
                'description' => $appointment->description,
                'url' => '#',
                'start' => date('Y-m-d', strtotime($appointment->date)) . ' ' . $appointment->from,
                'end' => date('Y-m-d', strtotime($appointment->date)) . ' ' . $appointment->to,
                'color' => $appointment->member->member_color
            );
        }
        return json_encode($events);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function addAppointment(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'member_id' => 'required',
            'date' => 'required',
        ]);
        $app = new Appointment();
        $app->fill($request->all());
        $app->user_id = auth()->id();
        $app->member_id = $request->member_id['id'];
        $app->date = date('Y-m-d', strtotime($request->date));
        $app->save();

        $data = [];
        $data['appointments'] = $this->appointmentLists($request->member_id['id']);
        $data['events'] = $this->calenderEvents();
        $data['dots'] = $this->datePickerDots();
        return response()->json($data);
    }

    public function appointmentLists($member_id)
    {
        return Appointment::where('member_id', $member_id)->latest()->get();
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'member_id' => 'required',
            'date' => 'required',
        ]);
        $app = new Appointment();
        $app->user_id = auth()->id();
        $app->fill($request->all());
        $app->save();
        return redirect()->back()->with('success', 'Appointment added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $appointment = Appointment::where('id', $id)->with('member')->first();
        return response()->json($appointment);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

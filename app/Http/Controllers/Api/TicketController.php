<?php

namespace App\Http\Controllers\Api;

use Auth;
use Carbon\Carbon;
use App\TicketChat;
use App\SupportTicket;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class TicketController extends Controller
{
    public function __construct() {
        $this->_helper = new Helper();
    }

    public function listing() {
        $request = request();
        $request->status = 'my-tickets-user';
        $oTicket = new SupportTicket();
	    $ticket = $oTicket->getAllDataWithComments($request);

        $this->_helper->response()->setCode(200)->send(['data' => $ticket]);
    }

    public function getComments() {
    	$request = request();
        $oComments = new TicketChat();

    	if (empty($request->id)) {
            $data['msg'] = 'Parameter Missing';
            $this->_helper->response()->setCode(200)->send($data);
        }


	    $comments = $oComments->getCommentsByTicketId($request->id);

	    $data['show_comments'] = (!empty($comments)) ? 'Y' : 'N' ;
	    $data['comments'] = $comments;

        $this->_helper->response()->setCode(200)->send(['data' => $data]);
    }

    public function addComment() {
        $data = [
            'status' => 'error',
            'data' => [],
        ];

        $request = request();

        if (empty($request->id)) {
            $data['msg'] = 'Invalid Project Id';
            $this->_helper->response()->setCode(200)->send($data);
        }

        if (empty($request->text)) {
            $data['msg'] = 'Enter some text in note area';
            $this->_helper->response()->setCode(200)->send($data);
        }

        $projects = new SupportTicket();
        $projects = $projects->where('status', '!=', 'deleted')->where('id', $request->id);

        if (!$projects->exists()) {
            $data['msg'] = 'Invalid Ticket or ticket has been deleted.';
            $this->_helper->response()->setCode(200)->send($data);
        }
        $projects = $projects->first();
        $projects->status = 'active';
        $projects->save();

        $comments = new TicketChat();
        $comments->ticket_id = $request->id;
        $comments->user_id = Auth::user()->id;
        $comments->chat_text = $request->text;
        $comments->save();
        $insertedId = $comments->id;

        $commentData = $comments->getCommentsByCommentId($insertedId);

        $msg = 'Comment successfully added.';
        $data = [
            'status' => 'Success',
            'msg' => $msg,
            'data' => $commentData,
        ];

        $this->_helper->response()->setCode(200)->send($data);
    }

    public function create() {

    	$request = request();
        //dd($request->all());
    	$validationArr = [
            'subject' => 'required|string',
            'question' => 'required|string',
            'file' => 'mimes:jpeg,png,pdf,docx'
        ];

        $this->_helper->runValidation($validationArr);

        $dir = 'tickets';

        if(!Storage::exists($dir)) {
		    Storage::makeDirectory($dir, 0775, true); //creates directory
		}

        if (!empty($request->file('file'))) {
    		$file = $request->file('file');
            $file_name = Auth::user()->id.'_ticket_file_'.md5(time()).'.'.$file->getClientOriginalExtension();
            $path = $file->storeAs($dir, $file_name);
        } else {
            $file_name = '';
        }

        $ticket = new SupportTicket();
        $ticket->user_id = Auth::user()->id;
        $ticket->subject = $request->subject;
        $ticket->attachment = $file_name;
        $ticket->save();
        $insertedId = $ticket->id;

        $comments = new TicketChat();
        $comments->ticket_id = $insertedId;
        $comments->user_id = Auth::user()->id;
        $comments->chat_text = $request->question;
        $comments->save();
        $insertedId = $comments->id;

        $msg = 'Ticket successfully generated.';
        $data = [
            'status' => 'Success',
            'msg' => $msg,
            'data' => [],
        ];

        $this->_helper->response()->setCode(200)->send($data);
    }
}

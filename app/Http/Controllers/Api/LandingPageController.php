<?php

namespace App\Http\Controllers\Api;

use App\Invoice;
use App\LandingQuestionAnswers;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Auth;
use App\Helpers\Helper;
use App\LandingPage;
use App\Http\Resources\LandingPage as LandingPageResource;
class LandingPageController extends Controller
{
    protected $_helper;

    public function __construct()
    {
        $this->_helper = new Helper();
    }

    public function listing()
    {
        $request = request();

        $pages = Auth::user()->landing_pages();

        if($request->has('filter')){
            if ($request->filter['selectedStartDate'] !== null && $request->filter['selectedEndDate'] !== null) {
                $pages = $pages->
                whereBetween('date',
                    [$request->filter['selectedStartDate'], $request->filter['selectedEndDate']]);
            }


            if ($request->filter['status'] !== null) {
                $pages = $pages->where('status', $request->filter['status']);
            }
        }

        $pages = $pages->orderBy('created_at', 'DESC')->paginate();

        $this->_helper->response()->setCode(200)->send(['data' => $pages]);
    }

    public function create()
    {
        $request = request();

        $this->_helper->runValidation([
            'name' => 'required|string',
            //'logo' => 'bail|required|image|max:800',  //dimensions:min_width:300,min_height:300,max_width:1000,max_height:1000
            'cost' => 'required|numeric|min:0|not_in:0',
            'description' => 'required',
            'url' => 'required|url|unique:landing_pages,url',
            'status' => 'required',
            'date' => 'required',
            'trying_to_sell' => 'required',
            'customer_benefit' => 'required',
            'leads_questions' => 'required|array|size:2',
            'leads_contact_details' => 'required|array',
            'review_pages' => 'required|array',
            'reasons.*' => 'required',
            'reviews.*' => 'required',
            'landing_page.*.questions' => 'required',
            'landing_page.*.answers' => 'required',
        ],[
            'amount.min' => 'Please enter positive integer',
            'reasons.0.required' => 'Please enter first reason',
            'reasons.1.required' => 'Please enter second reason',
            'reasons.2.required' => 'Please enter third reason',
            'reviews.0.required' => 'Please enter first review',
            'reviews.1.required' => 'Please enter second review',
            'landing_page.0.questions.required' => 'Please enter question # 1',
            'landing_page.1.questions.required' => 'Please enter question # 2',
            'landing_page.2.questions.required' => 'Please enter question # 3',
            'landing_page.3.questions.required' => 'Please enter question # 4',
            'landing_page.0.answers.required' => 'Please enter answer # 1',
            'landing_page.1.answers.required' => 'Please enter answer # 2',
            'landing_page.2.answers.required' => 'Please enter answer # 3',
            'landing_page.3.answers.required' => 'Please enter answer # 4',
        ]);


        $image = null;

        $objLanding = new LandingPage();
        $objLanding->user_id = Auth::user()->id;
        $objLanding->name = $request->name;
        $objLanding->logo = $image;
        $objLanding->cost = $request->cost;
        $objLanding->description = $request->description;
        $objLanding->url = $request->url;
        $objLanding->status = $request->status;
        $objLanding->date = Carbon::parse($request->date)->format('m/d/Y');
        $objLanding->trying_to_sell = $request->trying_to_sell;
        $objLanding->customer_benefit = $request->customer_benefit;
        $objLanding->leads_questions = $request->leads_questions;
        $objLanding->leads_contact_details = $request->leads_contact_details;
        $objLanding->review_pages = $request->review_pages;
        $objLanding->reasons = $request->reasons;
        $objLanding->reviews = $request->reviews;
        $objLanding->save();


        Invoice::create([
            'user_id' => Auth::user()->id,
            'type' => 'landing_page',
            'status' => 'pending',
            'campaign_id' => null,
            'landing_pages_id' => $objLanding->id
        ]);

        foreach ($request->landing_page as $landingPage){
            LandingQuestionAnswers::create([
                'landing_pages_id' => $objLanding->id,
                'question' => $landingPage['questions'],
                'answer' => $landingPage['answers'],
            ]);
        }

        $msg = 'Landing page and invoice created successfully.';
        $this->_helper->response()->setMessage($msg)->send(['data' => '']);
    }

}

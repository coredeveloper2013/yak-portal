<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Auth;
class BillingController extends Controller
{
    protected $_helper;

    public function __construct()
    {
        $this->_helper = new Helper();
    }

    public function getUserInvoices()
    {
        $invoices = Auth::user()->invoices()->with(['campaign', 'landing_page'])->paginate();

        $this->_helper->response()->send(['data' => $invoices]);
    }
}

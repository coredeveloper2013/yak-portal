<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Invoice;
use Carbon\Carbon;
use Auth;
class CampaignController extends Controller
{
    protected $_helper;

    public function __construct()
    {
        $this->_helper = new Helper();
    }
    public function index()
    {
        $request = request();

        $campaigns = Auth::user()->campaigns();

        if($request->has('filter')){
            if ($request->filter['selectedStartDate'] !== null && $request->filter['selectedEndDate'] !== null) {
                $campaigns = $campaigns->
                whereBetween('date', [$request->filter['selectedStartDate'], $request->filter['selectedEndDate']]);;
            }

            if ($request->filter['status'] !== null) {
                $campaigns = $campaigns->where('status', $request->filter['status']);
            }
        }


        $campaigns = $campaigns->orderBy('created_at', 'DESC')->paginate();

        $this->_helper->response()->send(['data' => $campaigns]);
    }

    public function create()
    {
        $request = request();

        $this->_helper->runValidation([
            'name' => 'bail|required|string',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
//            'landing_id' => 'bail|required|exists:landing_pages,id',
            'amount' => 'bail|required|numeric|min:0|not_in:0',
            'type' => 'required|in:one_off,monthly',
            'status' => 'bail|required|in:completed,pending',
//            'repeat' => 'required'
        ],[
            'amount.min' => 'Please enter positive integer',
            'type.in' => 'Type can be one_off or monthly'
        ]);


        $campaign = \App\Campaign::create([
            'user_id' => Auth::user()->id,
            'name' => $request->name,
            'start_date' => Carbon::parse($request->start_date)->format('m/d/Y'),
            'end_date' => Carbon::parse($request->end_date)->format('m/d/Y'),
            'landing_pages_id' => ($request->landing_id) ? $request->landing_id : null,
            'amount' => $request->amount,
            'type' => $request->type,
            'status' => $request->status,
//            'repeat' => $request->repeat
        ]);

        Invoice::create([
            'user_id' => Auth::user()->id,
            'type' => 'campaign',
            'status' => 'pending',
            'campaign_id' => $campaign->id,
            'landing_pages_id' => null
        ]);

        $this->_helper->response()->setMessage('Campaign and invoice created successfully.')->send(['data' => '']);
    }
}

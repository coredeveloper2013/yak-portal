<?php

namespace App\Http\Controllers\Api;

use Auth;
use App\Invoice;
use Carbon\Carbon;
use App\AssignedModule;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\OtherProjects as OtherProjectsResource;

class OtherProjectController extends Controller
{

    public function __construct() {
        $this->_helper = new Helper();
    }

    public function listing() {
        $request = request();

        $projects = Auth::user()->other_projects();

        if($request->has('filter')){
            if (!empty($request->filter['selectedStartDate']) && !empty($request->filter['selectedEndDate'])) {
                $endDate = Carbon::parse($request->filter['selectedEndDate'])->format('Y-m-d H:i:s');
                $startDate = Carbon::parse($request->filter['selectedStartDate'])->format('Y-m-d H:i:s');

                $projects = $projects->whereBetween('created_at', [$startDate, $endDate]);
            }


            if ($request->filter['status'] !== null) {
                $projects = $projects->where('status', $request->filter['status']);
            }
        }

        $projects = $projects->where('is_deleted', 'N')->orderBy('id', 'DESC')->paginate();
        /*if (!empty($projects)) {
            $projects = OtherProjectsResource::collection($projects);
        }*/

        $this->_helper->response()->setCode(200)->send(['data' => $projects]);
    }
}

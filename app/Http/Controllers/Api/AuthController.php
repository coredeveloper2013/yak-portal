<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use Auth;
use App\Http\Resources\Users as UsersResource;
use App\User;
use App\Company;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    private $_helper;

    public function __construct(){
        $this->_helper = new Helper();
    }

    public function register(Request $request){
        $data = $request->all();
        $this->_helper->runValidation([
            'name' => ['required', 'string', 'max:255', 'bail'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users', 'bail'],
            'password' => ['required', 'string', 'min:8', 'bail', 'confirmed'],
            'companyName' => ['required', 'string', 'bail'],
            'terms' => ['accepted', 'bail']
        ],[
            'name.required' => 'Please enter your full name',
            'email.required' => 'Please enter your Email',
            'password.required' => 'Set password',
            'companyName.required' => 'Please enter your company name',
            'terms.accepted' => 'You must agree before submitting.'
        ]);

        $user =  User::create([
            'name' => $data['name'],
            'username' => array_key_exists('username', $data) ? $data['username'] : null,
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);


        $api_token = $user->createToken('ppc')->accessToken;

        Company::create([
            'user_id' => $user->id,
            'name' => $data['companyName']
        ]);

        $data = [
            'status' => 'success',
            'msg' => 'Register Successfully.',
            'data' => [
                'userData' => new UsersResource($user),
                'authToken' => $api_token,
            ],
        ];

        $this->_helper->response()->setCode(200)->send($data);
    }

    public function login(Request $request){
        $data = $request->all();

        $this->_helper->runValidation([
            'email' => ['required', 'string', 'max:255', 'bail'],
            'password' => ['required', 'string', 'min:8', 'bail'],
        ],[
            'email.required' => 'Please enter your Email',
            'password.required' => 'Set password',
        ]);

        if(Auth::attempt(['email' => $data['email'], 'password' => $data['password']])){
    	    $user = Auth::user();

            $api_token = $user->createToken('ppc')->accessToken;

            $data = [
                'status' => 'success',
                'msg' => 'Login Successfully.',
                'data' => [
                    'userData' => new UsersResource($user),
                    'authToken' => $api_token,
                ],
            ];
    	}else{
            $data = [
                'status' => 'error',
                'msg' => 'Invalid Email/Passowrd.',
            ];
        }
        $this->_helper->response()->setCode(200)->send($data);
    }

}

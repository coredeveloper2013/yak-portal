<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Auth;

class AgreementController extends Controller
{
    protected $_helper;

    public function __construct()
    {
        $this->_helper = new Helper();
    }

    public function index(){

        $request = request();

        $agreements = Auth::user()->agreements();

        if($request->has('filter')){
            if ($request->filter['selectedStartDate'] !== null && $request->filter['selectedEndDate'] !== null) {
                $endDate = Carbon::parse($request->filter['selectedEndDate'])->format('Y-m-d ').'23:59:59';
                $startDate = Carbon::parse($request->filter['selectedStartDate'])->format('Y-m-d H:i:s');
                $agreements = $agreements->whereBetween('created_at', [$startDate, $endDate]);
            }

            if ($request->filter['status'] !== null) {
                $agreements = $agreements->where('status', $request->filter['status']);
            }
        }


        $agreements = $agreements->orderBy('created_at', 'DESC')->paginate();

        $this->_helper->response()->send(['data' => $agreements]);
    }
}

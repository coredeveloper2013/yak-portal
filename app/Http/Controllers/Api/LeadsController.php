<?php

namespace App\Http\Controllers\Api;

use App\Campaign;
use App\Http\Controllers\Controller;
use App\Notification;
use Illuminate\Http\Request;
use App\Http\Resources\Leads as LeadsResource;
use App\Http\Resources\LeadComments as LeadCommentsResource;
use App\Leads;
use App\LeadComments;
use App\LeadQuestion;
use App\LeadSms;
use App\LeadEmail;
use App\NotificationAlert;

use Auth;
use App\Helpers\Helper;
use Carbon\Carbon;

class LeadsController extends Controller
{
    private $_helper;

    public function __construct(){
        $this->_helper = new Helper();
    }

    public function status(){
        $data = [
            'status' => 'success',
            'msg' => 'Status Changed Successfully!',
            'data' => [],
        ];

        $request = request();
        $leads = Auth::user()->leads()->where('id', $request->lead)->where('campaign_id', $request->campaign)->first();
        $leads->status = $request->status;
        $leads->save();


        $leads = Auth::user()->leads()->where('id', $request->lead)->where('campaign_id', $request->campaign)->first();
        $data['data']['leads'] =  new LeadsResource($leads);

        $this->_helper->response()->setCode(200)->send($data);

    }

    public function getLeads(Request $request)
    {
        $this->_helper->runValidation([
            'campaign' => 'required',
        ],[
            'campaign.required' => 'campaign required',
        ]);

        $data = [
            'status' => 'success',
            'msg' => 'Success.',
            'data' => [],
        ];

        $leads = new Leads();

        if(!empty($request->lead)){
            $leads =  $leads->with(['lead_questions']);
            //$leads =  $leads->where('user_id', Auth::user()->id);
            $leads =  $leads->where('id',  $request->lead);
            $leads =  $leads->where('campaign_id', $request->campaign);
            $leads =  $leads->orderBy('created_at', 'DESC');
        }else{
            $leads =  $leads->with(['lead_questions']);
            //$leads =  $leads->with(['lead_comments']);
            //$leads =  $leads->where('user_id', Auth::user()->id);
            $leads =  $leads->where('campaign_id', $request->campaign);
            $leads =  $leads->orderBy('created_at', 'DESC');
        }

        $this->_helper->checkRecordExist($leads);

        $leads = $leads->first();

        $data['data']['leads'] =  new LeadsResource($leads);

        $this->_helper->response()->setCode(200)->send($data);
    }

    public function leadSearch(Request $request)
    {
        $this->_helper->runValidation([
            'campaign' => 'required',
            'last_lead_id' => 'numeric',
        ],[
            'campaign.required' => 'campaign required',
        ]);

        $data = [
            'status' => 'success',
            'msg' => 'Success.',
            'data' => [],
        ];
        $leads = new Leads();
        $leads = $leads->where('campaign_id',$request->campaign);
        
        if(!empty($request->search)){
            $leads = $leads->whereHas('lead_questions', function ($query) use ($request) {
                $query->where('answers', 'like', '%' . $request->search . '%');
            });
        }

        if (!empty($request->last_lead_id)) {
            $leads = $leads->where('id', '<', $request->last_lead_id);
        }
        $leads = $leads->limit(10);
        $leads = $leads->orderBy('id', 'DESC');

        $this->_helper->checkRecordExist($leads);


        $leads = $leads->get();

        if(!empty($leads)){
            $leads = LeadsResource::collection($leads);
        } else {
            $leads = [];
        }

        $data['data']['leads'] = $leads;

        $this->_helper->response()->setCode(200)->send($data);
    }

    public function saveLeads(Request $request)
    {
        $this->_helper->runValidation([
            'heading.*' => 'required',
            'answer.*' => 'required'
        ]);

        if (count($request->heading) != count($request->answer)) {
            $data = [
                'status' => 'error',
                'mob_status' => 'success',
                'msg' => 'Total Questions must be equal to total answers.',
                'data' => []
            ];
            $this->_helper->response()->setCode(200)->send($data);
        }

        $leads = new Leads();
        $leads->campaign_id = $request->campaign_id;
        $leads->user_id = Auth::user()->id;
        $leads->submitted_on =  Carbon::now();
        $isSaved = $leads->save();
        $insertedId = $leads->id;

        foreach ($request->heading as $key => $value) {
            $question = LeadQuestion::create([
                'user_id' => Auth::user()->id,
                'lead_id' => $insertedId,
                'headings' => $request->heading[$key],
                'answers' => $request->answer[$key]
            ]);

            /*$alert_users  = NotificationAlert::where('type','sms')->get();
            $email_users  = NotificationAlert::where('type','email')->get();*/

            $alert_users  = NotificationAlert::where('type','sms')->where('user_id', Auth::user()->id)->get();
            $email_users  = NotificationAlert::where('type','email')->where('user_id', Auth::user()->id)->get();

            foreach ($alert_users as $key => $val){
                $sms = LeadSms::create([
                    'alert_user' => $val->id,
                    'lead_id' => $insertedId,
                    'question_id' => $question->id,
                    'status' => '0'
                ]);
            }

            foreach ($email_users as $key => $val){
                $email = LeadEmail::create([
                    'alert_user' => $val->id,
                    'lead_id' => $insertedId,
                    'question_id' => $question->id,
                    'status' => '0'
                ]);
            }

        }
        /*if(empty($request->lead_id)){
        }else{
            $leads = Auth::user()->leads()->find($request->lead_id);
            if (!$leads->exists()) {
                $data['msg'] = 'Record with provided Id not found';
                $this->_helper->response()->setCode(200)->send($data);
            }
            $insertedId = $request->lead_id;
        }*/

        // Notification::where('campaign_id', $request->campaign_id)->where('user_id', Auth::user()->id)->update([ 'status' => 'completed']);
        //

        $data = [
            'status' => 'success',
            'msg' => 'Lead Saved Successfully.',
            'data' => [
                'getLeads' => new LeadsResource($leads),
            ],
        ];

        $this->_helper->response()->setCode(200)->send($data);
    }

    public function saveComments(Request $request)
    {
        $this->_helper->runValidation([
            'message' => 'required',
            'lead_id' => 'required',
        ],[
            'message.required' => 'Message field required',
            'lead_id.required' => 'Lead field required',
        ]);

        $leads = new LeadComments();
        $leads->user_id = Auth::user()->id;
        $leads->lead_id = $request->lead_id;
        $leads->message = $request->message;
        $isSaved = $leads->save();

        $data = [
            'status' => 'error',
            'mob_status' => 'success',
            'msg' => 'There might be some errors, Try again later.',
        ];

        if($isSaved){
            $data = [
                'status' => 'success',
                'msg' => 'Comment Saved Successfully.',
            ];
        }

        $this->_helper->response()->setCode(200)->send($data);
    }

    public function getComments(Request $request)
    {
        $this->_helper->runValidation([
            'id' => 'required',
        ],[
            'id.required' => 'Lead required',
        ]);

        $data = [
            'status' => 'success',
            'msg' => 'Success.',
        ];

        $comments = new LeadComments();

        $comments =  $comments->with(['user']);
        //$comments =  $comments->where('user_id', Auth::user()->id);
        $comments =  $comments->where('lead_id', $request->id);

        $this->_helper->checkRecordExist($comments);

        $comments = $comments->get();

        $data['data']['comments'] = LeadCommentsResource::collection($comments);

        $this->_helper->response()->setCode(200)->send($data);
    }
}

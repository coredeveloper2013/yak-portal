<?php

namespace App\Http\Controllers;

use Auth;
use App\Task;
use App\SubTask;
use App\Comment;
use App\Campaign;
use Carbon\Carbon;
use App\LandingPage;
use App\OtherProject;
use App\Helpers\Helper;
use App\UserTasks;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class TaskController extends Controller
{
    protected $_helper;

    public function __construct()
    {
        $this->_helper = new Helper();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = new \App\Task();
        $upcomingTasks = new \App\Task();

        if (request()->filter['status']) {
            if (request()->filter['status'] == 'completed') {
                $tasks = $tasks->statusCompleted();
                $upcomingTasks = $upcomingTasks->statusCompleted();
            }

            if (request()->filter['status'] == 'pending') {
                $tasks = $tasks->statusPending();
                $upcomingTasks = $upcomingTasks->statusPending();
            }

            if (request()->filter['status'] == 'in-progress') {
                $tasks = $tasks->statusInProgress();
                $upcomingTasks = $upcomingTasks->statusInProgress();
            }
        }

        $today = $tasks->todayTasks();
        $upcomingTasks = $upcomingTasks->upcomingTasks();

        $this->_helper->response()->send([
            'todayTasks' => $today->withCount(['subTasks' => function ($query) {
                $query->where('status', 'completed');
            }])->with([
                'subTasks', 
                'comments.commentBy' => function ($query) {
                    $query->withTrashed();
                }, 
                'taskUsers' => function ($query) {
                    if (request()->filter['myTasks']) {
                        $query->where('user_id', Auth::user()->id);
                    }
                }
            ])->orderBy('due_date', 'DESC')->limit(5)->get(),


            'upcomingTasks' => $upcomingTasks->withCount(['subTasks' => function ($query) {
                $query->where('status', 'completed');
            }])->with([
                'subTasks', 
                'comments.commentBy' => function ($query) {
                    $query->withTrashed();
                },
                'taskUsers' => function ($query) {
                    if (request()->filter['myTasks']) {
                        $query->where('user_id', Auth::user()->id);
                    }
                }
            ])->orderBy('due_date', 'DESC')->limit(5)->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $request = request();
        $type = '';
        $landingData = [];
        $projectData = [];
        $campaignData = [];

        if (!empty($request->type) && !empty($request->id) && in_array($request->type, ['campaign', 'landing_page', 'other_project'])) {
            $type = $request->type;
        }

        if ($type == 'campaign') {
            $campaign = new Campaign();
            $campaign = $campaign->where('id', $request->id);

            if ($campaign->exists()) {
                $data = $campaign->first();
                $campaignData = ['id' => $data->id, 'name' => $data->name];
            }
        } else if ($type == 'landing_page') {
            $landingPage = new LandingPage();
            $landingPage = $landingPage->where('id', $request->id);

            if ($landingPage->exists()) {
                $data = $projlandingPageects->first();
                $landingData = ['id' => $data->id, 'name' => $data->name];
            }
        } else if ($type == 'other_project') {
            $projects = new OtherProject();
            $projects = $projects->where('id', $request->id);

            if ($projects->exists()) {
                $data = $projects->first();
                $projectData = ['id' => $data->id, 'name' => $data->project_name];
            }
        }

        $data = [
            'type' => $type,
            'campaignData' => json_encode($campaignData),
            'landingData' => json_encode($landingData),
            'projectData' => json_encode($projectData)
        ];

        return view('staff.tasks.add.index', $data);
    }

    public function getStallMembers()
    {
        $members = new \App\User();

        $members = $members->where('type', 'staff');

        $this->_helper->response()->send(['data' => $members->get()]);
    }

    public function getAllCampaigns()
    {

        $campaigns = Campaign::all();

        $this->_helper->response()->send(['data' => $campaigns]);
    }

    public function getAllLandingPages()
    {

        $landingPages = LandingPage::all();

        $this->_helper->response()->send(['data' => $landingPages]);
    }

    public function getOtherProjects()
    {
        $projects = OtherProject::all();

        $this->_helper->response()->send(['data' => $projects]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {

        $request = request();

        $image = null;
        $mimeType = null;
        $ext = null;

        if ($request->file && $request->hasFile('file')) {
            //save user image
            $image = Auth::user()->id . '-' . $request->file('file')->getClientOriginalName();
            $ext = $request->file('file')->getClientOriginalExtension();

            if (Storage::exists('tasks/' . $image)) {
                Storage::delete('tasks/' . $image);
            }

            $path = $request->file('file')->storeAs(
                'tasks', $image
            );
            $mimeType = $request->file('file')->getMimeType();
        }

        $moduleId = ($request->moduleId ?  $request->moduleId : null);
        $type = ($request->type ?  $request->type : null);


        $task = Task::create([
            'name' => $request->title,
            'description' => ($request->description != '') ? $request->description : null,
            'assigned_by' => Auth::user()->id,
            'due_date' => Carbon::createFromFormat('Y-m-d h:i:s A', $request->dueDate),
            'attachments' => $image,
            'file_type' => $mimeType,
            'status' => $request->status,
            'priority' => $request->priority,
            'module_id' => $moduleId,
            'link_type' => $type
        ]);

        if (!empty($request->checklists)) {
            $checklists = explode(",", $request->checklists);
            foreach ($checklists as $checklist) {
                SubTask::create([
                    'task_id' => $task->id,
                    'name' => $checklist,
                    'status' => 'pending'
                ]);
            }
        }

        if (!empty($request->userIds)) {
            $users = explode(",", $request->userIds);
            $task->taskUsers()->attach($users);
        }


        $this->_helper->response()->send('');
    }

    public function postComment()
    {

        $request = request();

        Comment::create([
            'task_id' => $request->taskId,
            'user_id' => Auth::user()->id,
            'description' => $request->comment
        ]);

        $task = Task::with(['subTasks', 'comments.commentBy', 'taskUsers'])->find($request->taskId);

//        $task = $this->index();
        $this->_helper->response()->send(['data' => $task]);
    }

    public function updateStatus()
    {

        $task = Task::with(['subTasks', 'comments.commentBy', 'taskUsers'])->find(request()->taskId);

        $task->status = request()->status;

        $task->save();

        $this->_helper->response()->send(['data' => $task]);
    }

    public function updateChecklistStatus()
    {
        $subtask = SubTask::find(request()->subTaskId);
        $subtask->status = request()->status;
        $subtask->save();
        $task = Task::with(['subTasks', 'comments.commentBy', 'taskUsers'])->find(request()->taskId);
        $this->_helper->response()->send(['data' => $task]);
    }
    public function removeAttachment(){

        $request = request();

        $task = Task::find($request->taskId);

        if(Storage::exists('tasks/'.$task->attachments)){
            Storage::delete('avatars/'.$task->attachments);
        }

        $task->attachments = null;
        $task->file_type = null;

        $task->save();

        $this->_helper->response()->send('');
    }

    /**
     * TASKA APIS
     * 
     */
    public function storeTask(Request $request)
    {
        $this->_helper->runValidation([
            'title' => 'required',
            'description' => 'required',
            'assign_to' => 'required|array',
            'assign_to.*' => 'required|exists:users,id',
            'status' => 'required|in:completed,in-progress,pending',
            'priority' => 'required|in:low,medium,high',
            'due_date' => 'required|date|after-or-equal:today',
            'image' => 'nullable|image',
            'link_id' => 'required-with:link_type|integer',
            'link_type' => 'required-with:link_id|in:campaign,landing_page,other_project',
            'sub_tasks' => 'nullable|array',
            'sub_tasks.*' => 'required',
        ]);

        try {

            if ($request->has('link_id') && isset($request->link_id) && $request->has('link_type') && isset($request->link_type)) {
                //check existence
                $dataFind = false;
                $tempLinkId = $request->link_id;
                if ($request->link_type == 'campaign') {
                    if (Campaign::where('id', $tempLinkId)->exists()) {
                        $dataFind = true;
                    }
                } elseif ($request->link_type == 'landing_page') {
                    if (LandingPage::where('id', $tempLinkId)->exists()) {
                        $dataFind = true;
                    }
                } else {
                    if (OtherProject::where('id', $tempLinkId)->exists()) {
                        $dataFind = true;
                    }
                }

                if (!$dataFind) {
                    $data = [
                        'status' => 'false',
                        'msg' => "Link Type and Id is not correct"
                    ];
                    $this->_helper->response()->setCode(200)->send($data);
                }
            }

            DB::beginTransaction();
            $image = null;
            $mimeType = null;
            if ($request->hasFile('image')) {
                //save user image
                $image = Auth::user()->id . '-' . $request->file('image')->getClientOriginalName();
                if (Storage::exists('tasks/' . $image)) {
                    Storage::delete('tasks/' . $image);
                }
                $path = $request->file('image')->storeAs('tasks', $image);
                $mimeType = $request->file('image')->getMimeType();
            }

            //add main task
            $task = Task::create([
                'name' => $request->title,
                'description' => $request->description ?? null,
                'assigned_by' => Auth::user()->id,
                'due_date' => Carbon::createFromFormat('Y-m-d h:i:s A', $request->due_date),
                'attachments' => $image,
                'file_type' => $mimeType,
                'status' => $request->status,
                'priority' => $request->priority,
                'module_id' => $request->link_id ?? null,
                'link_type' => $request->link_type ?? null,
            ]);


            //add subtasks
            if ($request->has('sub_tasks') &&  !empty($request->sub_tasks)) {
                foreach ($request->sub_tasks as $checklist) {
                    SubTask::create([
                        'task_id' => $task->id,
                        'name' => $checklist,
                        'status' => 'pending'
                    ]);
                }
            }
            //attach task to user
            $task->taskUsers()->attach($request->assign_to);
            DB::commit();
            //send resp
            $data = [
                'status' => 'success',
                'msg' => 'Task Added',
            ];
            $this->_helper->response()->setCode(200)->send($data);
        } catch (\Throwable $th) {
            DB::rollback();
            $data = [
                'status' => 'false',
                'msg' => $th->getMessage()
            ];
            $this->_helper->response()->setCode(200)->send($data);
        }
    }
    public function updateTaskStatus(Request $request, Task $task)
    {
        $this->_helper->runValidation([
            'status' => 'required|in:completed,in-progress,pending',
        ]);
        $task->status = $request->status;
        $task->updated_at = Carbon::now();
        $task->save();
        $data = [
            'status' => 'success',
            'msg' => 'Task Status Update',
        ];
        $this->_helper->response()->setCode(200)->send($data);
    }
    public function updateSubTaskStatus(Request $request, SubTask $subtask)
    {
        $this->_helper->runValidation([
            'status' => 'required|in:completed,pending',
        ]);
        $subtask->status = $request->status;
        $subtask->updated_at = Carbon::now();
        $subtask->save();
        $data = [
            'status' => 'success',
            'msg' => 'Sub Task Status Update',
        ];
        $this->_helper->response()->setCode(200)->send($data);
    }
    public function updateTaskAttachment(Task $task)
    {
        if (Storage::exists('tasks/' . $task->attachments)) {
            Storage::delete('tasks/' . $task->attachments);
        }
        $task->attachments = null;
        $task->file_type = null;
        $task->updated_at = Carbon::now();
        $task->save();
        $data = [
            'status' => 'success',
            'msg' => 'Task Attachment Removed',
        ];
        $this->_helper->response()->setCode(200)->send($data);
    }
    public function taskComment(Request $request)
    {
        $this->_helper->runValidation([
            'task_id' => 'required|exists:tasks,id',
            'comment' => 'required'
        ]);
        Comment::create([
            'task_id' => $request->task_id,
            'user_id' => Auth::user()->id,
            'description' => $request->comment
        ]);
        $data = [
            'status' => 'success',
            'msg' => 'Task Comment added',
        ];
        $this->_helper->response()->setCode(200)->send($data);
    }
    public function getTasks(Request $request)
    {
        $this->_helper->runValidation([
            'status' => 'nullable|in:completed,in-progress,pending',
            'my_tasks' => 'nullable|in:true'
        ]);

        $tasks = null;
        $todays = [];
        $upcoming = [];
        if ($request->has('my_tasks') && isset($request->my_tasks)) {
            //get task task of current login user first
            $myTasksIds = UserTasks::where('user_id', Auth::user()->id)->pluck('task_id')->toArray();
            if (count($myTasksIds)) {
                $tasks = Task::withCount(['subTasks' => function ($query) {
                    $query->where('status', 'completed');
                }])->with('subTasks', 'comments', 'taskUsers')->whereIn('id', $myTasksIds);
            }
        } else {
            $tasks = Task::withCount(['subTasks' => function ($query) {
                $query->where('status', 'completed');
            }])->with('subTasks', 'comments', 'taskUsers');
        }

        if ($tasks) {

            if ($request->has('status') && isset($request->status)) {
                $tasks = $tasks->where('status', $request->status);
            }

            $upcoming=clone $tasks;
            $todays=clone  $tasks;

            $upcoming= $upcoming->upcomingTasks()->get();
            $todays= $todays->todayTasks()->get();
            $alltasks = $tasks->get();

            $data = [
                // 'alltasks'=>$alltasks,
                'todayTasks' =>  $todays,
                'upcomingTasks' => $upcoming
            ];
            
        } else {
            $data = [
                'todays' =>  $todays,
                'upcoming' => $upcoming
            ];
        }

        $this->_helper->response()->setCode(200)->send($data);
    }
    public function getTask(Request $request, Task $task)
    {

        $task = Task::withCount(['subTasks' => function ($query) {
            $query->where('status', 'completed');
        }])->with('subTasks', 'comments', 'taskUsers')
        ->where('id', $task->id)->first();

        $this->_helper->response()->setCode(200)->send($task);
    }
   
}

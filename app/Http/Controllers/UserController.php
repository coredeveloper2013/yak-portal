<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Helpers\Helper;
use DB;

class UserController extends Controller
{
    protected $_helper;

    public function __construct()
    {
        $this->_helper = new Helper();
    }

    public function updateProfile(Request $request)
    {
        $rules = [
            'image' => 'bail|nullable|image|max:800|dimensions:min_width:300,min_height:300,max_width:1000,max_height:1000',
//            'username' => 'bail|required|string',
            'name' => 'required|string|bail',
//            'email' => 'required|unique:users,email,' . Auth::user()->id,
        ];

        if (request()->wantsJson()) {
            $this->_helper->runValidation($rules);
        } else {
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput()->with('general', '');
            }
        }

        $user = Auth::user();

        if ($request->hasFile('img')) {
            //save user image
            $image = Auth::user()->id . '-' . $request->file('img')->getClientOriginalName();
            $ext = $request->file('img')->getClientOriginalExtension();

            if (Storage::exists('avatars/' . $image)) {
                Storage::delete('avatars/' . $image);
            }

            $path = $request->file('img')->storeAs(
                'avatars', $image
            );

            $user->image = $image;
        }

        if ($request->username !== null) {
            $user->username = $request->username;
        }
        if ($request->name !== null) {
            $user->name = $request->name;
        }
        if ($request->email !== null) {
            $user->email = $request->email;
        }
        if ($request->companyName !== null) {
            $user->company()->update(['name' => $request->companyName]);
//            $user->company()->name = $request->companyName;
        }
        if ($request->website !== null) {
            $user->business_info()->update(['website' => $request->website]);
//            $user->business_info()->website = $request->website;
        }
        $user->save();

        if (request()->wantsJson()) {
            $data = [
                'status' => 'success',
                'msg' => 'General Information Updated.',
            ];
            $this->_helper->response()->setCode(200)->send($data);
        } else {
            return redirect()->back()->with('general', 'General Information Updated.');
        }

    }

    public function updatePassword(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'old_password' => 'required|password:web',
            'password' => 'required|confirmed|min:8'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->with('password', '');
        }

        $user = Auth::user();

        $user->password = bcrypt($request->password);

        $user->save();

        return redirect()->back()->with('password', 'Password Updated.');
    }

    public function resetImage()
    {
        $helper = new Helper();

        $user = Auth::user();

        $user->image = null;

        $user->save();

        $helper->response()->send('');
    }

    public function updateCompany()
    {
        $request = request();
        $rules = ['companyName' => 'required'];

        if (request()->wantsJson()) {
            $this->_helper->runValidation($rules);
        } else {
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->with('company', '');
            }
        }

        $company = Auth::user()->company;
        $company->name = $request->companyName;
        if ($request->has('address') && isset($request->address))
            $company->address = $request->address;
        $company->save();

        if (request()->wantsJson()) {
            $data = [
                'status' => 'success',
                'msg' => 'Company Details Updated',
            ];
            $this->_helper->response()->setCode(200)->send($data);
        } else {
            return redirect()->back()->with('company', 'Company Details Updated.');
        }
    }

    public function requestsHistory()
    {
        //
        $logged_id = Auth::user()->id;
        $all_requests = DB::table('telescope_entries_tags as tg')
            ->join("telescope_entries as ent", "ent.uuid", "tg.entry_uuid")
            ->select('ent.content', 'created_at')
            ->where(['tg.tag' => $logged_id, 'ent.type' => "request"])
            ->orderBy('created_at', 'desc')
            ->get();

        $url_list = array();
        foreach ($all_requests as $key => $val) {
            $content = json_decode($val->content);

            $date_time = explode(" ", $val->created_at);

            $temp_array = array(
                'page' => url('/') . $content->uri,
                'ip_address' => $content->ip_address,
                'date' => $date_time[0],
                'time' => $date_time[1]
            );
            $url_list[] = $temp_array;
        }
        return response()->json([
            'request_list' => $url_list,
        ]);
    }

    public function getProfile()
    {
        $user = Auth::user();
        $this->_helper->response()->send(['data' => $user]);
    }

    public function changePassword()
    {
        $this->_helper->runValidation([
            'old_password' => 'required|password:api',
            'password' => 'required|confirmed|min:8'
        ]);
        $user = Auth::user();
        $user->password = bcrypt(request()->password);
        $user->save();
        $data = [
            'status' => 'success',
            'msg' => 'Password Changed Successfully',
        ];
        $this->_helper->response()->setCode(200)->send($data);
    }

}

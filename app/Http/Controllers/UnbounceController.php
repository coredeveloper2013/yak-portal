<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Auth;
use App\Invoice;
use Carbon\Carbon;
use App\Agreement;
use App\Notification;
use App\Helpers\Helper;
use App\AgreementDocuSign;
use App\Unbounce;
use App\Leads;
use App\LeadQuestion;
use Illuminate\Support\Facades\Validator;
class UnbounceController extends Controller
{

    protected $_helper;

    public function __construct()
    {
        $this->_helper = new Helper();
    }
    
    public function index()
    {
        $request = request();

        $campaigns = Auth::user()->campaigns();
        $campaigns = $campaigns->orderBy('created_at', 'DESC')->paginate();
        $unbounce=Unbounce::getALl(Auth::id());
         return view('unbounce.index', ['page_title' => 'Unbounce','campaigns' =>  $campaigns,'unbounce' =>  $unbounce]);

	}
   
   
    public function add()
    {
        $request = request();
        
        
        $this->_helper->runValidation([
             'unbounce_id' => 'required',
            'campaign' => 'required', 
        ]  ); 

         $exist= \App\Unbounce::where(['unbounce_id' => $request->unbounce_id,'campaign_id' => $request->campaign])->get();
         if(!empty($exist[0])){
            $data = [
                'status' => 'error',
                'msg' => 'Already exist.',
                'data' => []
            ];
            $this->_helper->response()->setCode(200)->send($data);
         }
        $campaign = \App\Unbounce::create([
            'user_id' => Auth::user()->id,
            'unbounce_id' => $request->unbounce_id,
            'campaign_id' => $request->campaign
             
        ]);

        $this->_helper->response()->setMessage('Saved Successfully.')->send('');
    }

    public function cron()
    { 

       $appKey = 'd12f072ccc809b29767b92fec66c9b27';
       $Unbounce=Unbounce::all();
       // $Unbounce=Unbounce::whereDay('created_at', now()->day)->get();

       foreach ($Unbounce as $ukey => $unb) {
           $userId=$unb->user_id;
           $campaignId=$unb->campaign_id;
           $unbounceId=$unb->unbounce_id;
        $url = 'https://api.unbounce.com/pages/'.$unbounceId.'/leads?limit=1000';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$appKey");
        $result = curl_exec($ch);
        curl_close($ch); 
        $result=  json_decode(  $result,true); 
       //call API
       if(!empty($result['leads']))
        foreach ($result['leads'] as $key => $value) {
            $unbLeadsId= $value['id'];
        $exsit =Leads::where(['unbounce_id'=>$value['id'],'campaign_id'=>$campaignId])->get();
            if(!empty($exsit[0])){
             continue;
            }
        $leads = new Leads();
        $leads->campaign_id = $campaignId;
        $leads->user_id = $userId;
        $leads->unbounce_id =      $unbLeadsId;
        $leads->submitted_on =  Carbon::now();
        $isSaved = $leads->save();
        $insertedId = $leads->id; 
            if($insertedId){
            foreach ($value['form_data'] as $key2 => $value2) {
            LeadQuestion::create([
                'user_id' =>   $userId,
                'lead_id' => $insertedId,
                'headings' => $key2,
                'answers' => $value2[0]
            ]);
                
            }}
        }
        }

 


    }

}

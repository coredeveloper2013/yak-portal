<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Carbon\Carbon;
use App\TicketChat;
use App\SupportTicket;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class TicketController extends Controller
{
    public function index() {
        return view('tickets.index', ['page_title' => 'Support Tickets']);
    }
}

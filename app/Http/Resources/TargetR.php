<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class TargetR extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $bb=Carbon::parse($this->busnissYear);
        return [
            'id' => $this->id, 
            'month' => $this->month, 
            'new_business' => $this->new_business, 
            'user_id' => $this->user_id, 
            'project' => $this->project, 
            'year' =>  (  $bb->format('Y')),
            'retention' => $this->retention
        ];
    }
}

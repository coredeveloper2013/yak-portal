<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Users as UsersResource;
use App\User;
use App\Company;
use Auth;

class Campaigns extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'assigned' => $this->when(User::where('id', $this->assigned_id)->exists(), function () {
                return new UsersResource(User::find($this->assigned_id));
            }),
            'user' => $this->when(User::where('id', $this->user_id)->exists(), function () {
                return new UsersResource(User::find($this->user_id));
            }),
            // 'company' => $this->when(Company::where('user_id', $this->user_id)->exists(), function () {
            //     return new UsersResource(Company::where('user_id', $this->user_id)->first());
            // }),
            'campaign_specialist' => $this->campaign_specialist,
            'recurring_option' => $this->recurring_option,
            'recurring_way' => $this->recurring_way,
            'lp_specialist' => $this->lp_specialist,
            'account_manager' => $this->account_manager,
            'amount' => $this->amount,
            'max_add_amount' => $this->max_add_amount,
            'compaign_amount' => $this->compaign_amount,
            'type' => $this->type,
            'status' => $this->status,
            'status_upper' => ucwords($this->status),
            'date' => Carbon::parse($this->date)->format('M d,Y'),
        ];
    }
}

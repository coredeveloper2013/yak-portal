<?php

namespace App\Http\Resources;

use App\Http\Resources\Companies as CompaniesResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Company;

class Users extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id, 
            'name' => $this->name, 
            'type' => $this->type, 
            'email' => $this->email, 
            'username' => $this->username, 
            'type' => $this->type,   
            'rights' => $this->rights,     
            'no_of_leave' => $this->no_of_leave,  

            'company' => new CompaniesResource(Company::where('user_id',$this->id)->first()),
        ];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class OtherProjects extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'project_name' => $this->project_name,
            'description' => $this->description,
            'start_date' => Carbon::parse($this->start_date)->format('M d,Y'),
            'end_date' => Carbon::parse($this->end_date)->format('M d,Y'),
            'price' => $this->price,
            'budget' => $this->budget,
            'cost_type' => $this->cost_type,
            'status' => $this->status,
            'status_upper' => ucwords($this->status),
        ];
    }
}

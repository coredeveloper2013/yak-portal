<?php

namespace App\Http\Resources;

use App\Http\Resources\Companies as CompaniesResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Company;

class Guides extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id, 
            'title' => $this->title, 
            'description' => $this->description, 
            'image' => $this->image, 
            'status' => $this->status
        ];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class Leads extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'campaign' => $this->campaign_id,
            'lead_no' => 'Lead No #'.$this->id,
            'lead_questions' => $this->lead_questions,
            //'lead_comments' => $this->lead_comments,
            'status' => $this->status,
            'status_upper' => ucwords($this->status),
            'submitted_on_date' => Carbon::parse($this->submitted_on)->format('M d,Y'),
            'submitted_on_time' => Carbon::parse($this->submitted_on)->format('H:i'),
        ];
    }
}

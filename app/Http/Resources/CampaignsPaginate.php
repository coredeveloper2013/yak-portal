<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Users as UsersResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\Campaigns as CampaignsResource;
use App\User;
use Auth;

class CampaignsPaginate extends ResourceCollection
{
    private $pagination;

    public function __construct($resource)
    {
        $this->pagination = [
            'total' => $resource->total(),
            'count' => $resource->count(),
            'per_page' => $resource->perPage(),
            'current_page' => $resource->currentPage(),
            'total_pages' => $resource->lastPage()
        ];

        $resource = $resource->getCollection();

        parent::__construct($resource);
    }

    public function toArray($request)
    {
        return [
            'data' => CampaignsResource::collection($this->collection),
            'meta' => $this->pagination,
        ];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Users as UsersResource;
use Carbon\Carbon;
use App\User;

class LandingPage extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'user' => new UsersResource(User::find($this->user_id)->first()),
            'description' => $this->description,
            'url' => $this->url,
            'cost' => $this->cost,
            'date' => Carbon::parse($this->date)->format('M d,Y'),
            'status' => ucwords($this->status),
        ];
    }
}

<?php

namespace App\Http\Resources;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

use Illuminate\Http\Resources\Json\JsonResource;

class LeadComments extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'message' => $this->message,
            'userdata' => ['name' => $this->user->name, 'image' => $this->user->image],
            'created_at_date' => Carbon::parse($this->created_at)->format('M d,Y g:i:s A'),
            'created_at_time' => Carbon::parse($this->created_at)->format('H:i'),
        ];
    }
}

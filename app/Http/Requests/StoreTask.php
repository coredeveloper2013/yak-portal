<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTask extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required",
            "description" => "required",
            "assigned_to" => "required",
            "due_date" => "required",
            /*"checklist" => "required",*/
            "status" => "required",
            "priority" => "required",
            /*"link_type" => "required"*/
        ];
    }
}

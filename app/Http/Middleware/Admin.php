<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Helpers\Helper;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->type == 'admin'){
            return $next($request);
        }

        return (new Helper())->redirectIf($request);
    }
}

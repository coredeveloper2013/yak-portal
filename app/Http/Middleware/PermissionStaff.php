<?php

namespace App\Http\Middleware;

use App\Models\RoutePermission;
use Closure;
use Auth;
use App\Helpers\Helper;

class PermissionStaff
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $routeName          = \Route::currentRouteName();
        $routeNameArr       = explode('.', $routeName);
        $routeNameArrSize   = count($routeNameArr);
        $newRouteName       = implode('.', array_slice($routeNameArr, 0, $routeNameArrSize-1));
        $matchableRouteName = $newRouteName.'.index';

        //$found = strpos($routeName, $newRouteName) !== false;

        $home        = '_v2.staff.home-2';
        $user_id     = Auth()->user()->id;
        $permissions = RoutePermission::where('user_id', $user_id)->with('route')->get()->keyBy('route.route')->toArray();
        //dd($matchableRouteName, $newRouteName, $routeName, $permissions, array_key_exists($routeName, $permissions));

        if($routeName == $home) return $next($request);
        if(!array_key_exists($matchableRouteName, $permissions)) return abort(403);


        return $next($request);
    }
}

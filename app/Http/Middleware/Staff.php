<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Helpers\Helper;

class Staff
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user() && (Auth::user()->type == 'staff' || Auth::user()->type == 'admin')){
            return $next($request);
        }

        return (new Helper())->redirectIf($request);
    }
}

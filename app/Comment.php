<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Comment extends Model
{
    use Notifiable;

    protected $fillable = [
        'task_id', 'user_id', 'description'
    ];

    protected $appends = ['human_created_at'];

    public function getHumanCreatedAtAttribute(){
//        return Carbon::parse($this->getOriginal('created_at'))->toDateTimeString();
        return date('h:i A', strtotime($this->getOriginal('created_at')));
    }


    public function commentBy(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function task(){
        return $this->belongsTo(Task::class);
    }
}

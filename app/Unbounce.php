<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Unbounce extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = "unbounce_link";

    protected $fillable = [
        'unbounce_id', 'campaign_id', 'user_id', 'is_new_bussiness_lead', 'cron_status'
    ];
     public static function getALl($id) {

		$us = DB::table('unbounce_link as u')
        ->leftJoin('campaigns as c', 'c.id', '=', 'u.campaign_id')
		    ->select('u.*','c.name as name')
        ->whereNull('c.deleted_at')
        ->whereNull('u.deleted_at');
        if ($id != -1) {
            $us = $us->where('u.user_id',$id);
        }
        $us = $us->get();

      foreach ( $us as $key => $value) {
          $value->created =date('F d,Y',strtotime($value->created_at));
      }

      return $us;
	}
}

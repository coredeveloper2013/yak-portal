<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class AssignedModule extends Model
{
    use Notifiable;

    protected $fillable = ['module_id', 'assign_to', 'module_type'];

    public function other_project(){
        return $this->belongsTo(OtherProject::class, 'module_id');
    }

    public function category(){
        return $this->belongsTo(Categories::class, 'module_id')->where('module_type', 'category');
    }
}

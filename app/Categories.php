<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $fillable = ['name' , 'color'];

    public function events(){
        return $this->hasMany(Events::class, 'category_id', 'id');
    }

    public function assignedmodule() {
        return $this->hasMany(AssignedModule::class, 'module_id', 'id')->where('module_type' , 'category');
    }
}

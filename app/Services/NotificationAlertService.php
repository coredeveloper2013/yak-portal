<?php

namespace App\Services;

use App\Models\Customer\Lead;
use App\NotificationAlert;
use Auth;

class NotificationAlertService
{
    private $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function getAlertSMS()
    {

        $alert = NotificationAlert::where('type', 'sms')->where('user_id', Auth::user()->id);

        if (!empty($this->request->search)) {
            $alert->where('name', 'LIKE', '%' . $this->request->search . '%')
                ->orWhere('send_on', 'LIKE', '%' . $this->request->search . '%');
        }
        return $alert;
    }

    public function getAlertEmail()
    {
        $alert = NotificationAlert::where('type', 'email')->where('user_id', Auth::user()->id);

        if (!empty($this->request->search2)) {
            $alert->where('name', 'LIKE', '%' . $this->request->search2 . '%')
                ->orWhere('send_on', 'LIKE', '%' . $this->request->search2 . '%');
        }
        return $alert;
    }

}

<?php

namespace App\Services;

use App\Models\Appointment;
use App\Models\Customer\Lead;

class AppointmentService
{
    private $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function getAppointments()
    {
        $leads = Appointment::query();
        if (!empty($this->request->search)) {
             $leads->where('title', 'LIKE', '%' . $this->request->search . '%');
        };
        return $leads;
    }

}

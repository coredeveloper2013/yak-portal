<?php

namespace App\Services;

use App\Models\Customer\LeadDeal;
use Illuminate\Support\Facades\Auth;

class LeadService
{
  private $request;

  public function __construct($request, $client_id=0)
  {
    $this->request = $request;
  }

  public function getLeads()
  {
      //$user_id = $this->request->filled('customer') ? $this->request->customer : Auth::user()->id;
      $user_id = $this->request->client_id ? $this->request->client_id : Auth::user()->id;
      $leads = LeadDeal::query()
          ->where('user_id',$user_id);
      if (!empty( $this->request->search)) {
        return $leads->where( 'name', 'LIKE', '%' . $this->request->search.'%');
      }
      else  return $leads;

  }

}

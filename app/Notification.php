<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Notification extends Model
{
    use Notifiable;

    protected $fillable = ['user_id', 'campaign_id', 'type', 'for', 'status', 'sent_status', 'sent_on'];

    protected $appends = ['human_date'];

    public function getHumanDateAttribute(){
        if($this->getOriginal('created_at') != null){
            return Carbon::parse($this->getOriginal('created_at'))->toFormattedDateString();
        }
        return null;
    }

    public function getStatusAttribute($value){
        return strtoupper($value);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function campaign(){
        return $this->belongsTo(Campaign::class);
    }
}

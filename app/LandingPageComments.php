<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LandingPageComments extends Model
{
    protected $fillable = ['landing_page_id' , 'description' , 'user_id' ];
    protected $appends = ['description_br'];
    public function campaign(){
        return $this->belongsTo(LandingPage::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function getDescriptionBrAttribute(){
        if(!empty($this->description)){
            return nl2br($this->description);
        }
        return $this->description;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Target extends Model
{
    //
	protected $table = 'targets';
	protected $fillable = ['month','busnissYear',
	'project','user_id', 'new_business','retention'];
}

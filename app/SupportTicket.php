<?php

namespace App;

use Auth;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class SupportTicket extends Model {
	use Notifiable;
    protected $fillable = ['user_id', 'assigned_to', 'attachment', 'status', 'created_at'];

    public function getAllDataWithComments($request) {
        $oUser = new User();
        $oComments = new TicketChat();
        $limit = 10;

        $appendQuery = '';
        if (!empty($request->status) && $request->status == 'completed') {
            $appendQuery .= ' AND status = "completed" ';
        } else {
            $appendQuery .= ' AND status<>"deleted" ';
        }

        if (!empty($request->status) && $request->status == 'my-tickets') {
            $appendQuery .= ' AND assigned_to ='.Auth::user()->id;
        }

        if (!empty($request->status) && $request->status == 'my-tickets-user') {
            $appendQuery .= ' AND user_id = '.Auth::user()->id;
        } 

        if (!empty($request->status) && $request->status == 'unassigned') {
            $appendQuery .= ' AND (assigned_to IS NULL OR assigned_to = "") AND status <> "completed" ';
        }

        $offset = (!empty($request->offset)) ? $request->offset : 0 ;

        $total = DB::select("SELECT 
                                    COUNT(st.`id`) as total
                                FROM
                                  `support_tickets` st WHERE 1=1 ".$appendQuery." ORDER BY id DESC");

        $query =    "SELECT 
                        st.`id`,
                        st.`subject`,
                        st.`user_id` AS customer_id,
                        st.`assigned_to` AS staff_id,
                        st.`attachment`,
                        st.`created_at` AS task_date
                    FROM
                      `support_tickets` st
                    WHERE 
                        1=1 
                    ".$appendQuery." ORDER BY id DESC 
                    LIMIT ".$offset.", ".$limit;

        $ticket = DB::select($query);

        if (!empty($ticket)) {
            foreach ($ticket as $key => $value) {
                $ticket[$key]->staff_data = $oUser->getUserDataById($value->staff_id);
                $ticket[$key]->customer_data = $oUser->getUserDataById($value->customer_id);
                $ticket[$key]->comments = $oComments->getCommentsByTicketId($value->id);
                $ticket[$key]->human_date = Carbon::parse($value->task_date)->format('M d, Y');
                $ticket[$key]->unassigned = (!empty($value->staff_id)) ? 'N' : 'Y';
            }
        }

        $lastOffset = $offset-$limit;

        $response['tickets'] =  $ticket;
        $response['total_tickets'] =  $total[0]->total;
        $response['show_comments'] = (!empty($ticket)) ? 'Y' : 'N';
        $response['offset_last'] = ($lastOffset > 0) ? $lastOffset : 0 ;
        $response['offset_next'] =  $offset+$limit;
        $response['offset_current'] =  $offset;

        return $response;
    }
}

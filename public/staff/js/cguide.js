 
function showBg(id){
//    listOfGuides= JSON.parse(GUIDES);
    const index = listOfGuides.findIndex( (item)   => item.id == id);
    $('#card-img-tops').attr('src',listOfGuides[index].image);
    $('#BGdescription').html(listOfGuides[index].description);
    $('#BGtitle').html(listOfGuides[index].title);
    $('#BGupdate').html(listOfGuides[index].msg);
    $("#guideModal").modal('show'); 
}

 getAll();
 var listOfGuides;
 function getAll(){
 	loader('show');

    ajx("POST", {  }, `${appURL}/guide/get`, "json", function(response) {
         let htmll='';
           listOfGuides=response.data.guides;
        if(response.data && response.data.guides){
           
        $('#listG').html('');
          
            guides = response.data.guides;
            $.each(guides,function(i,guide){
              let button= '';

            htmll+=  `  <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                                <div class="guide-box">
                                   <img src="${ (guide.image)}">
                                    <div class="text-guide">
                                      <h4> ${ (guide.title)}</h4>
                                        <div class="update-notify">
                                           ${ (guide.msg)}
                                        </div>
                                        <p> ${ (guide.descriptionF)} </p>
                                        <a href="javascript:;" onclick="showBg(${guide.id})" class="read-more">Read more</a>
                                    </div>
                                </div>
                            </div>`;
               
            });
        }
         $('#listG').append(htmll);loader();
    });
    
}



$( "#userId" ).change(function() {
UserIdSelected=$('#userId').val();
  getCampaigns(UserIdSelected);
});
function addTarget(id){
        $('#addNewModel').modal('show');
        $('#userId').val(id);
}
function getItem(id,userId){
    ajx("POST", {id:id }, `${apiURL}/target/get`, "json", function(response) {
         $('#idT').val(response.data.targets.id);
         $('#month').val(response.data.targets.month);
         $('#new_business').val(response.data.targets.new_business);
         $('#retention').val(response.data.targets.retention);
         $('#userId1').val(response.data.targets.user_id);
         $('#year').val(response.data.targets.year);
         $('#project').val(response.data.targets.project);
        $('#addNewModel1').modal('show');
    });
}
$(function () {
     $('#addTarget').on('submit',function(evt){
        evt.preventDefault();
        console.log($(this).serialize());
        ajx("POST", $(this).serialize(), `${apiURL}/target/add`, "json", function(response) {
  getAll($('#businessyeard').val());

            showMsg(response);
            if(response.status == 'success'){
                $('#addNewModel').modal('hide');
                $('#switch1').trigger('change');
                formReset('#addTarget');
            }
        });
    });
     $('#editTarget').on('submit',function(evt){
        evt.preventDefault();
        console.log($(this).serialize());
        ajx("POST", $(this).serialize(), `${apiURL}/target/update`, "json", function(response) {
  getAll($('#businessyeard').val());

            showMsg(response);
            if(response.status == 'success'){
                $('#addNewModel1').modal('hide');
                formReset('#addTarget');
            }
        });
    });



 });

$( "#businessyeard" ).change(function() {
  getAll($('#businessyeard').val());
});

  getAll();
function getAll(year='2020'){
    loader('show');

    ajx("POST", { year:year }, `${apiURL}/target/get`, "json", function(response) {
         let htmll='';
           allTargets=response.data.targets;
        if(response.data && response.data.targets){
        $('#listG').html('');

            targets = response.data.targets;
            $.each(targets,function(i,target){


              let button= '';
                    if(response.data.isAdmin){
                     button=`<a class="dropdown-item" href="#" onclick="deleteItem(${target.id})" ><i class="fas fa-trash"></i> Delete</a>`;
                    }
          htmll+=`<div class="card mb-0">
                        <div class="">
                           <div class="head-t card-body border-bottom">
                              <div class="float-left">
                                 <h4 class="page-title text-uppercase userName1" >${target.user.name}</h4>
                              </div>
                              <div class="float-right">`;
                             if(response.data.isAdmin){
                            htmll+=  `<button type="button" onclick="addTarget(${target.user.id})" class="btn btn-primary" id='addNew'><i class="fas fa-plus"></i> Add New</button>`;
                          }
                              htmll+=` </div>
                           </div>
                           <div class="w-100 col-xl-8 col-lg-8 col-md-8 col-sm-8">
                              <div class="card-body">`;
                              if(target.targets.length){
                              htmll+=` <div class="table-responsive tble-business-holid">
                                 <table class="table text-center table-bordered holi-table td-block">
                                 <tr class="first-border tr-font">
                                       <th class="text-muted"></th>
                                       <th>New Business</th>
                                       <th>Retention</th>
                                       <th>Project</th>
                                         `;
                                       if(response.data.isAdmin){
                                        htmll+=  `
                                       <th></th>`
                                          ;}
                              htmll+= `
                                    </tr>`;
                     $.each(target.targets,function(i,target){
                           htmll+=         `<tr class="tr-font" >
                                       <td class="text-left">${target.month.toUpperCase()}</td>
                                       <td>£${target.new_business}</td>
                                       <td>£${target.retention}</td>
                                       <td>£${target.project}</td>
                                      `;
                                       if(response.data.isAdmin){
                                        htmll+=  ` <td>
<a href="#" class="edit" onclick="getItem(${target.id})" data-toggle="m" data-placement="top" title="Edit">Edit</a></td>
  `
                                          ;}
                              htmll+=         `

                                    </tr>`;
                                     });

                           htmll+=       `  </table>
                              </div>`;}else{
                                 htmll+=`<div class="alert alert-custom">record not found</div>`;
                              }
                            htmll+=`
                              </div>
                           </div>
                        </div>
                     </div>`;

            });
        }
         $('#listG').append(htmll);loader();
    });
}


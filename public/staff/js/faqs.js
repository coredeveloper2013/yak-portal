var tableData = $(".tableData").DataTable({
    "searching": false,
    "lengthChange": false,
    "targets": 'no-sort',
    "bSort": false,
    "order": [],
});
function deleteUser(id){
  Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
    
  if (result.value) {
    ajx("POST", 
        {id:id}
        , `${apiURL}/faqs/del`, "json", function(response) {
             if(response.status=='success'){
              getCampaigns();
            Swal.fire(
      'Deleted!',
      'Item has been deleted.',
      'success'
    )}
        });
   
  }
})

}
function getEditUser(id){
    ajx("POST", {id:id }, `${apiURL}/faqs/get`, "json", function(response) {
      
         $('#id').val(response.data.faqs.id);
         $('#question').val(response.data.faqs.question);
         $('#answer').val(response.data.faqs.answer);
         $('#EisFrequent').val(response.data.faqs.isFrequent);
         $('#Estatus').val(response.data.faqs.status);
         $('#addNewModel1').modal('show');

    });
}

 function getCampaigns(){
    ajx("POST", {  }, `${apiURL}/faqs/get`, "json", function(response) {
        tableData.clear();
        if(response.data && response.data.faqs){
            faqs = response.data.faqs;

            $.each(faqs,function(i,user){
              let button= '';
                    if(response.data.isAdmin){
                     button=  `
                   <span class="btn-group btn-flex">
                    <button type="button" class="btn btn-btn btn-primary btn-sm p-1 px-2 mt-0 btn-sm  " data-toggle="m" data-toggle="tooltip"
                   data-placement="top"  onclick="getEditUser(${user.id})"   title="edit"><i class="fas fa-edit"></i>
               </button>
               <button type="button" class="btn btn-btn btn-primary btn-sm p-1 px-2 mt-0 btn-sm assign-to-user" data-toggle="modal" data-target="#OthersModel"
                   data-toggle="tooltip" onclick="deleteUser(${user.id})" data-placement="top" title="Assign to Others"><i class="fas fa-trash"></i>
               </button></span>
                    `; 
                    }
                let tRow = tableData.row.add([
                    `${i+1}`,
                    `${user.question}`,
                    ((user.isFrequent)   ),
                    ((user.status)),
                      button
                  ,
                ]).draw(false).node();
                $(tRow).attr("data-id", user.id);
            });
        }
        tableData.draw();
    });
}

$(function () {
    $('input[name="date"]').daterangepicker({
        opens: 'left',
        singleDatePicker: true,
        showDropdowns: true,
        minDate: moment(),
        startDate: moment()
    }, function (start, end, label) {
        $("#date").val(start.format('MM/DD/YYYY'));
    });

    $('#switch1').on('change',function(evt){
        evt.preventDefault();
        $('#addCampaign').find('.assigned-users').toggle();
    });
    $('#switch1').trigger('change');

    $('#addNew').on('click',function(){
        $('#addNewModel').modal('show');
    });

    $('#addFaqs').on('submit',function(evt){
        evt.preventDefault();
        console.log($(this).serialize());
        ajx("POST", $(this).serialize(), `${apiURL}/faqs/add`, "json", function(response) {
            showMsg(response);
            if(response.status == 'success'){
                $('#addNewModel').modal('hide');
                $('#switch1').trigger('change');
                formReset('#addFaqs');
                getCampaigns();
            }
        });
    });
  $('#editFaqs').on('submit',function(evt){
        evt.preventDefault();
        ajx("POST", $(this).serialize(), `${apiURL}/faqs/update`, "json", function(response) {
            showMsg(response);
            if(response.status == 'success'){
                $('#addNewModel1').modal('hide');
                $('#switch1').trigger('change');
                formReset('#editFaqs');
                getCampaigns();
            }
        });
    });
    $('body').on('click','.assign-to-me',function(evt){
        evt.preventDefault();
        data = {};
        data.user = '';
        data.campaign = $(this).parents('tr').data('id');
        assignToMe(data);
    });

    $('body').on('click','.assign-to-user',function(evt){
        evt.preventDefault();
        $('#assignUser').find('[name="campaign"]').val($(this).parents('tr').data('id'));
        $('#assignUserModel').modal('show');
    });

    $('#assignUser').on('submit',function(evt){
        evt.preventDefault();
        assignToMe($(this).serialize());
    });

    getCampaigns();
 });



  



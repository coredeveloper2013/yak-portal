function assignToMe(data){
    loader('show');
    ajx("POST", data, `${apiURL}/campaigns/assign`, "json", function(response) {
        showMsg(response);
        loader('hide');
        if(response.status == 'success'){
            $('#assignUserModel').modal('hide');
            formReset('#assignUser');
            getCampaigns();
        }
    });
}

$(function () {
    $('input[name="date"]').daterangepicker({
        opens: 'left',
        singleDatePicker: true,
        showDropdowns: true,
        minDate: moment(),
        startDate: moment()
    }, function (start, end, label) {
        $("#date").val(start.format('MM/DD/YYYY'));
    });

    $('#switch1').on('change',function(evt){
        evt.preventDefault();
        $('#addCampaign').find('.assigned-users').toggle();
    });
    $('#switch1').trigger('change');

    $('#addNew').on('click',function(){
        $('#addNewModel').modal('show');
    });

    $('#addCampaign').on('submit',function(evt){
        evt.preventDefault();
        ajx("POST", $(this).serialize(), `${apiURL}/campaigns/add-campaign`, "json", function(response) {
            showMsg(response);
            if(response.status == 'success'){
                $('#addNewModel').modal('hide');
                $('#switch1').trigger('change');
                formReset('#addCampaign');
                getCampaigns();
            }
        });
    });

    $('body').on('click','.assign-to-me',function(evt){
        evt.preventDefault();
        data = {};
        data.user = '';
        data.campaign = $(this).parents('span').data('id');
        assignToMe(data);
    });

    $('body').on('click','.assign-to-user',function(evt){
        evt.preventDefault();
        $('#assignUser').find('[name="campaign"]').val($(this).parents('span').data('id'));
        $('#assignUserModel').modal('show');
    });

    $('#assignUser').on('submit',function(evt){
        evt.preventDefault();
        assignToMe($(this).serialize());
    });

    getCampaigns();
 });



  



var tableData = $(".tableData").DataTable({
    "searching": false,
    "lengthChange": false,
    "targets": 'no-sort',
    "bSort": false,
    "order": [],
});
var UserIdSelected=0;

function deleteUser(id){
  Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
    
  if (result.value) {
    ajx("POST", 
        {id:id}
        , `${apiURL}/holiday/del`, "json", function(response) {
             if(response.status=='success'){
  getCampaigns($('#userId').val());
              
            Swal.fire(
      'Deleted!',
      'Item has been deleted.',
      'success'
    )}else{
        
                Swal.fire(
      response.status,
       response.msg,
      response.status
    )
    }
        });
   
  }
})

}
function getEditUser(id){
    ajx("POST", {id:id }, `${apiURL}/faqs/get`, "json", function(response) {
      
         $('#id').val(response.data.faqs.id);
         $('#question').val(response.data.faqs.question);
         $('#answer').val(response.data.faqs.answer);
         if(response.data.faqs.isFrequent==1)
            $('#EisFrequent').attr('checked',true);
          else
            $('#EisFrequent').attr('checked',false);
         $('#addNewModel1').modal('show');

    });
}
$( "#userId" ).change(function() {
UserIdSelected=$('#userId').val();
  getCampaigns(UserIdSelected);
});
 function getCampaigns(UserId=0){
          $('input[name="id"]').val(UserId);

    ajx("POST", { userId:UserId }, `${apiURL}/holiday/get`, "json", function(response) {
let htmll='';
            $('#daysleft').html(' ');
        if(response.data && response.data.holidays){
          $('input[name="no_of_leave"]').val(response.data.noOfLeave);
          $('#userName').text(response.userName);
          if(response.data.dayLeft){
            $('#daysleft').html(response.data.dayLeft);
          }
           $('.td-block').html(''); 
          
            holidays = response.data.holidays;
            $.each(holidays,function(i,holiday){

              let button= ''; 
                    if(response.data.isAdmin){ 
                     button=' <td><a href="#" class="edit" onclick="deleteUser('+holiday.id+')" data-toggle="m" data-placement="top" title="DELETE">DELETE</a></td>'; 
                    }
          htmll+='<tr class="tr-font"><td class="text-left">Day '+(i+1)+'</td><td>'+holiday.date_of_holiday+'</td><td class="text-left">'+holiday.reason+'</td>'
          +button+'</tr>';         
               
            });
        }
        $('.td-block').append(htmll);

    });
}

$(function () {
    $('input[name="date"]').daterangepicker({
        opens: 'left',
        singleDatePicker: true,
        showDropdowns: true,
        minDate: moment(),
        startDate: moment()
    }, function (start, end, label) {
        $("#date").val(start.format('MM/DD/YYYY'));
    });

    $('#switch1').on('change',function(evt){
        evt.preventDefault();
        $('#addCampaign').find('.assigned-users').toggle();
    });
    $('#switch1').trigger('change');

    $('#addNew').on('click',function(){
        $('#addNewModel').modal('show');
    });

    $('#addHoliday').on('submit',function(evt){
        evt.preventDefault();
        console.log($(this).serialize());
        ajx("POST", $(this).serialize(), `${apiURL}/holiday/add`, "json", function(response) {
            showMsg(response);
            if(response.status == 'success'){
                $('#addNewModel').modal('hide');
                $('#switch1').trigger('change');
                formReset('#addHoliday');
                getCampaigns($('#userId').val());
            }
        });
    });
    $('#updateLeave').on('submit',function(evt){
        evt.preventDefault();
        console.log($(this).serialize());
        ajx("POST", $(this).serialize(), `${apiURL}/holiday/leave`, "json", function(response) {
            showMsg(response);
            if(response.status == 'success'){
                $('#editLeaveModel').modal('hide');
                formReset('#updateLeave');
                getCampaigns($('#userId').val());
            }
        });
    });
  $('#editFaqs').on('submit',function(evt){
        evt.preventDefault();
        ajx("POST", $(this).serialize(), `${apiURL}/faqs/update`, "json", function(response) {
            showMsg(response);
            if(response.status == 'success'){
                $('#addNewModel1').modal('hide');
                $('#switch1').trigger('change');
                formReset('#editFaqs');
                getCampaigns($('#userId').val());
            }
        });
    });
    $('body').on('click','.assign-to-me',function(evt){
        evt.preventDefault();
        data = {};
        data.user = '';
        data.campaign = $(this).parents('tr').data('id');
        assignToMe(data);
    });

    $('body').on('click','.assign-to-user',function(evt){
        evt.preventDefault();
        $('#assignUser').find('[name="campaign"]').val($(this).parents('tr').data('id'));
        $('#assignUserModel').modal('show');
    });

    $('#assignUser').on('submit',function(evt){
        evt.preventDefault();
        assignToMe($(this).serialize());
    });

    getCampaigns(USERID);
    $('#userId').val(USERID)
 });



  



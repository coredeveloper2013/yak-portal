$('#addLeads').on('submit', function(evt) {
    evt.preventDefault();
    var formData = $(this).serialize();//new FormData($(this)[0]);
    console.log(formData);
    loader('show');
    $.ajax({
        type: 'POST',
        url: `${apiURL}/leads/add`,
        data: formData,
        complete: function() {
            loader();
        },
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            Accpet: "applicationjson"
        },
        success: function(response) {
            loader();
            showMsg(response);
            // showOkWithReload(response);

            if (response.status == 'success') {
                $('#addNewModel').modal('hide');
                $('#switch1').trigger('change');
                location.reload();
                /*formReset('#addLeads');
                getAll();*/
            }
        }

    });

});

$('#updateStatus').on('submit', function(evt) {
    evt.preventDefault();
    var formData = new FormData($(this)[0]);
    console.log(formData);
    loader('show');
    $.ajax({
        type: 'POST',
        url: `${apiURL}/deals/status`,
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        complete: function() {
            loader();
        },
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            Accpet: "applicationjson"
        },
        success: function(response) {
            loader();
            showOkWithReload(response);
            // showMsg(response);
            if (response.status == 'success') {
                $('#addNewModel1').modal('hide');
                $('#switch1').trigger('change');
                formReset('#updateStatus');
                // location.reload();

            }
        }

    });
});

$('#sendMessage').on('submit', function(evt) {
    evt.preventDefault();
    var formData = new FormData($(this)[0]);
    loader('show');
    $.ajax({
        type: 'POST',
        url: `${apiURL}/leads/addComments`,
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        complete: function() {
            loader();
        },
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            Accpet: "applicationjson"
        },
        success: function(response) {
            loader();
            $('#usercommnts').show();
            // showOkWithReload(response);
            getcomments();

            //            showMsg(response);
            if (response.status == 'success') {
                // $('#addNewModel1').modal('hide');
                // $('#switch1').trigger('change');
                formReset('#sendMessage');
                // location.reload();

            }
        }

    });
});

$("#status").change(function(evt) {
    evt.preventDefault();
    var formData = new FormData($('#statuschange')[0]);

    loader('show');
    $.ajax({
        type: 'POST',
        url: `${apiURL}/deals/status`,
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        complete: function() {
            loader();
        },
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            Accpet: "applicationjson"
        },
        success: function(response) {
            loader();
            // showOkWithReload(response);
            showMsg(response);
            if (response.status == 'success') {
                $('#addNewModel1').modal('hide');
                $('#switch1').trigger('change');
                // formReset('#updateStatus');
                // getAll();
                // selectLeads(response.data.leads[0].id);



                // location.reload();

            }
        }

    });
});

$('body').on('click', '.linked', function(event) {
    event.preventDefault();
    $('.linked').removeClass('active');
    $(this).addClass('active');
    getAll();
});

function showMore() {
    $('#detailsModal').modal('show');
}

function activeLeads() {
    loader('show');

    ajx("POST", {
        'lead_id': $('#idd').val()
    }, `${apiURL}/leads/active`, "json", function(response) {
        showMsg(response);
        //showOkWithReload(response);

        if (response.status == 'success') {
            window.location.replace(appURL+'/deals');
            //getAll();
            //$("#selecterMove").css("display", "block");
            //$("#selecterButton").css("display", "none");
        }


    });
}

function assign() {
    if ($('#assign_to').val() == '') {
        return false;
    }

    loader('show');

    ajx("POST", {
        'user_id': $('#assign_to').val(),
        'lead_id': $('#idd').val()
    }, `${apiURL}/leads/assign`, "json", function(response) {
        showMsg(response);
        //showOkWithReload(response);

        if (response.status == 'success') {
            location.reload();
            //getAll();
            //$("#selecterMove").css("display", "block");
            //$("#selecterButton").css("display", "none");
        }


    });
}

function changeStatus(id, name, budget, status) {
    $('#idd').val(id);
    $('#name').val(name);
    $('#budget').val(budget);
    $('#statuss').val(status);
    $('#addNewModel1').modal('show');
}

function showOkWithReload(res) {
    let status = res.status.substr(0,1).toUpperCase()+res.status.substr(1);
    Swal.fire({
        icon: res.status,
        title: status,
        text: res.msg,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'OK!'
    }).then((result) => {
        if (result.value) {
            location.reload();
        }
    })
}

$('#editLeads').on('submit', function(evt) {
    evt.preventDefault();
    var formData = new FormData($(this)[0]);
    loader('show');
    $.ajax({
        type: 'POST',
        url: `${apiURL}/leads/update`,
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        complete: function() {
            loader();
        },
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            Accpet: "applicationjson"
        },
        success: function(response) {
            showMsg(response);
            if (response.status == 'success') {
                $('#addNewModel1').modal('hide');
                $('#switch1').trigger('change');
                formReset('#editGuide');
                getAll();
            }
        }

    });
});

getAll();

var listOfLeads = [];
var listOfUsers = [];

function getAll(linked_text) {
    loader('show');
    $('#listG').html('');
    $('#usercommnts').html('');
    $('#lLeadName').html('');

    if (typeof linked_text !== 'undefined') {
        $('.linked').removeClass('active');
        $('.linked.'+linked_text).addClass('active');
    }

    var linked = $('.linked.active').data('linked');

    ajx("POST", {
        search: $('#chat-search').val(),
        user_id: $('#user-id').val(),
        linked: linked
    }, `${apiURL}/leads/get`, "json", function(response) {
        let htmll = '';

        if (response.data && response.data.leads) {

            listOfLeads = response.data.leads;
            listOfUsers = response.data.users;
            $('#listG').html('');
            leads = response.data.leads;
            var leadNameText = '';
            $.each(leads, function(i, leads) {
                leadNameText = leads.name;
                if (leadNameText == null) {
                    leadNameText = 'Lead No #'+leads.id;
                }
                let button = 'active-chat';
                 htmll += `<a href="javascript:void(0)" onclick="selectLeads(${leads.id})" class="media a${leads.id} chat-block">
                                <div class="media-body">
                                    <div>
                                        <h6>${leadNameText}</h6>
                                        <p>Status: <span class="lead-status1">${leads.statusName}</span></p>
                                    </div>
                                    <div>
                                        <span>${leads.created}</span>
                                    </div>
                                </div>
                            </a>`;
            });
        }
        $('#listG').append(htmll);
        if (response.data.leads.length) {
            selectLeads(response.data.leads[0].id);
            $('.detail-wrap1').show();
            $('#detail-wrap2').hide();

            $('#selecterButton').show();
            $('#sendMessage').show();
        } else {
            $('.detail-wrap1').hide();
            $('#detail-wrap2').show();
            $('#selecterButton').hide();
            $('#sendMessage').show();
            $('#listG').html('<div class="tab-pane fade show active p-2 ml-2">No record found.</div>');

        }
    });

    $('body').on('click', '.clone_block', function(evt) {
        evt.preventDefault();
        var total =  $('.question_block').length;
        var html = $('.question_block:last-child').clone();
        console.log(total);
        html.find('input').val('');
        //var nextCount = parseInt(total) + 1;
        html.find('.remove_block').removeClass('d-none');
        $('.append_block').append(html);
        recountQuestionsBlk();
    });

    $('body').on('click', '.remove_block', function(event) {
        event.preventDefault();
        var parent = $(this).parents('.question_block');
        parent.remove();
        recountQuestionsBlk();
    });
}

function getcomments() {
    loader('show');

    ajx("POST", {
        'lead_id': $('#idd').val()
    }, `${apiURL}/leads/comments`, "json", function(response) {
        let htmll = '';
        if(response.data.comments.length == 0 || response.data.comments.length == '0') {
           $('#usercommnts').hide();
        } else {
           $('#usercommnts').show();
        }
        if (response.data && response.data) {

            $('#usercommnts').html('');
            var comments = response.data.comments;
            var leads_data = response.data.lead_data;

            $.each(comments, function(i, comments) {
                let button = 'active-chat';

                var user_image = "/staff/images/user-1.jpg";
                if (comments.image != '' && comments.image != null) {
                    user_image = storage_url+'/avatars/'+comments.image;
                }

                htmll += ` <li class="user-comment">
                            <a href="javascript:;">
                                <img class="rounded-circle" width="40" height="40" src="${user_image}" alt="">
                            </a>
                            <div class="user-comment-right">
                                <div class="commentContent overflow">
                                   <a class="comment_name" href="javascript:;">${comments.userName}</a>
                                    <span class="commentTxt" dir="auto">${comments.message}</span>
                                </div>
                                <span class="title timeago">${comments.created}</span>
                            </div>
                        </li> `;
            });



            if (leads_data.is_assigned == 'Y') {
                $('.move_to_deals').removeClass('d-none');
                $('#assign_to').val(leads_data.user_id);
            } else {
                $('.move_to_deals').addClass('d-none');
                $('#assign_to').val('');
            }
            /*if (leads_data > 0) {
            } else {
                $('.move_to_deals').removeClass('d-none');
            }*/
        }

        $('#usercommnts').append(htmll);
    });
}

function deleteDeal() {
    ajx("POST", {
        'id': $('#idd').val()
    }, `${apiURL}/leads/del`, "json", function(response) {
        showOkWithReload(response);
    });
}

function recountQuestionsBlk() {
    var length = $('.question_block').length;
    if (length > 0) {
        $.each($('.question_block'), function( index, value ) {
            var nextCount = parseInt(index) + 1;
            $(this).find('.q_count').html(nextCount);
        });
    }
}

function selectLeads(id) {
    $('.detail-wrap1').removeClass('d-none');
    $('.media').removeClass('active-chat');
    $('.a' + id).addClass('active-chat');

    const index = listOfLeads.findIndex((item) => item.id == id);
    var leadData = listOfLeads[index];
    var leadSelectedUserid=leadData.user_id;
    // console.log(leadData.id,leadSelectedUserid);
    $('#leadid').val(leadData.id);
    // var html =  `<div class="col-xl-4 col-lg-6">
    //                 <div class="heading-title-leads">User </div>
    //                 <span id="userName">`+leadData.userName+`</span>
    //             </div>`;
    var html = '';
    var editSelect = `<div class="col-xl-12">
    <div class="form-group">`;
    if(userType=='admin'){
        editSelect+=`<label for="userId">User:</label><select class="form-control" name="user_id" required=""><option disabled="" selected="">Select </option>`;
        for (i = 0; i < listOfUsers.length; i++) {
            if(listOfUsers[i]['id']!=leadSelectedUserid)
            {
                editSelect+=`<option value="`+listOfUsers[i]['id']+`">`+listOfUsers[i]['name']+`</option>`;

            }
            else
            {
                editSelect+=`<option selected value="`+listOfUsers[i]['id']+`">`+listOfUsers[i]['name']+`</option>`;
            }

          }
          editSelect+=`</select>`;
    }else{
        editSelect+=`<input type="hidden" class="form-control"  value="`+leadSelectedUserid+`" name="user_id" id="edit_user_id">`;
    }

    var leadNameText = leadData.name;
    if (leadNameText == null) {
        leadNameText = 'Lead No #'+leadData.id;
    }

    editSelect+=`</div>
                    </div>
                    <div class="col-xl-12">
                        <div class="form-group">
                          <label for="name" class="font-weight-bold black-text w-100">
                              Lead Name
                           </label>
                           <input type="text" class="form-control heading" name="lead_name" aria-describedby="Heading" placeholder="Lead Name" value="`+leadNameText+`">
                        </div>
                     </div>
                `;

    var modal_html = '';
    var edit_html='';
    var editQuescount=0;
    if (leadData.questions.length > 0) {
        $.each(leadData.questions, function(i, qus) {
            // console.log('i:'+i);
            editQuescount=i+1;
            if (i < 3)
            {
                html += `<div class="col-4 col-sm-4 col-md-4">
                        <div class="heading-title-leads">`+qus.question+`</div>
                        <span id="userName">`+qus.answer+`</span>
                     </div>`;
            }
            modal_html += `<div class="col-12 col-sm-12 col-md-12 lead-col-b">
                        <div class="heading-title-leads">`+qus.question+`</div>
                        <span id="userName">`+qus.answer+`</span>
                     </div>`;


            edit_html +=
                `<div class="edit_question_block">
            <div class="form-group mb-2">
               <label for="name" class="font-weight-bold black-text w-100">
                  Question <spna class="edit_q_count">` +
                editQuescount +
                `</spna>`;
            if (i > 0) {
                edit_html += `<a href="javascript:;" class="text-danger float-right edit_remove_block" onclick="remove_edit_fields(this);"><i class="far fa-trash-alt"></i></a>`;
            }
            edit_html +=
                `</label>
               <input type="text" value="` +
                qus.question +
                `" class="form-control heading" required name="question[]" aria-describedby="Heading" placeholder="Question">
            </div>
            <div class="form-group">
               <input type="phone" required class="form-control answer" value="` +
                qus.answer +
                `" name="answer[]" aria-describedby="Answer" placeholder="Answer">
            </div>
            <hr style="width: 50%">
         </div>`;


        });

        edit_html_add_q = `<div class="form-group clearfix text-center">
                    <span class="edit_clone_block btn btn-xs btn-primary m-0"><i class="fas fa-plus"></i> Add Question</span>
                    </div>`;
    }

    if(leadData.questions.length > 3) {
        $('#btn-details-load').show();
    }
    else {
        $('#btn-details-load').hide();
    }

    $('.detail-wrap1').find('.question-body').html(html);
    $('#modal-details-wrap').html(modal_html);
    // $('#edit-form').html("");
    $('#edit-select').html(editSelect);
    $('#edit-form').html(edit_html);
    $('#edit-form-add-q').html(edit_html_add_q);
    $('#idd').val(leadData.id);
    $('#lead_id').val(leadData.id);

    getcomments();
}

function editDeal() {
    $("#editNewModel").modal("show");
}

function remove_edit_fields(e) {
    $(e).parent("label").parent("div").parent("div").remove();
    recountQuestionsEditBlk();
}

$("body").on("click", ".edit_clone_block", function (evt) {
    evt.preventDefault();
    var total = $(".edit_question_block").length;
    total = total + 1;
    // console.log(total);
    var html =
        `<div class="edit_question_block">
            <div class="form-group mb-2">
               <label for="name" class="font-weight-bold black-text w-100">
                  Question <spna class="edit_q_count">` +
                total +
                `</spna>
                  <a href="javascript:;" class="text-danger float-right edit_remove_block" onclick="remove_edit_fields(this)"><i class="far fa-trash-alt"></i></a>
               </label>
               <input type="text"  required class="form-control heading" name="question[]" aria-describedby="Heading" placeholder="Question">
            </div>
            <div class="form-group">
               <input type="phone" required class="form-control answer"  name="answer[]" aria-describedby="Answer" placeholder="Answer">
            </div>
            <hr style="width: 50%">
         </div>`;

    $("#edit-form").append(html);
    // recountQuestionsBlk();
});

function recountQuestionsEditBlk() {
    var length = $(".edit_question_block").length;
    if (length > 0) {
        $.each($(".edit_question_block"), function (index, value) {
            var nextCount = parseInt(index) + 1;
            $(this).find(".edit_q_count").html(nextCount);
        });
    }
}

$('#editLeadsForm').on('submit', function(evt) {
    evt.preventDefault();
    var formData = $(this).serialize();//new FormData($(this)[0]);
    // formData+='&leadId='+leadId;
    // var leadId=$('#idd').val();
    // console.log(listOfUsers);
    loader('show');
    $.ajax({
        type: 'POST',
        url: `${apiURL}/leads/update-lead`,
        data: formData,
        complete: function() {
            loader();
        },
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            Accpet: "applicationjson"
        },
        success: function(response) {
            loader();
            showMsg(response);
            // showOkWithReload(response);

            if (response.status == 'success') {
                $('#addNewModel').modal('hide');
                $('#switch1').trigger('change');
                location.reload();
                /*formReset('#addLeads');
                getAll();*/
            }
        }

    });
});

function getUserDeals(userID) {
    window.location.replace(appURL+ "/deals/" + userID );
}

function loadDeals(id) {

    $("#idd").val(id);
    $("#lead_id").val(id);

    $.ajax({
        url: `${apiURL}/deal-detail/${id}`,
        type: 'GET',
        dataType: 'json',
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            Accpet: "applicationjson"
        }
    })
    .done(function(res) {
        var html = '';
        let leadData = res.data.leads[0];
        if (leadData.questions.length > 0) {
            $.each(leadData.questions, function(i, qus) {
                // console.log('i:'+i);
                html += `<div class="infobox">
                            <strong>`+qus.question+`</strong>
                            `+qus.answer+`
                        </div>`;
            });

        }
        $('.dealsb .userinfo').html(html);
        getcomments();
    })
    .fail(function() {
        console.log("error");
    })
    .always(function(res) {
    });

    return false;


}

function showDealSec() {
    $(".dealsb").show();
    $(".page-content").css("width", "calc(100% - 300px)");
    var cheight = $(".userinfo").height()+"px";
    //$('.dealsb .lead-detail-coment').css("max-height", "calc(100vh - '+cheight+'px)");
}

$('body').on('click', '.closesb', function(event) {
    $(".dealsb").hide();
    $(".page-content").css("width", "100%");
    $('.card-body').removeClass('alert-success');
});

var tableData = $(".tableData").DataTable({
    "searching": false,
    "lengthChange": false,
    "targets": 'no-sort',
    "bSort": false,
    "order": [],
});
function deleteItem(id){
  Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
    
  if (result.value) {
    ajx("POST", 
        {id:id}
        , `${apiURL}/guide/del`, "json", function(response) {
             if(response.status=='success'){
              getAll();
            Swal.fire(
      'Deleted!',
      'Item has been deleted.',
      'success'
    )}
        });
   
  }
})

}
function getItem(id){
    ajx("POST", {id:id }, `${apiURL}/guide/get`, "json", function(response) {
      
         $('#id').val(response.data.guides.id);
         $('#title').val(response.data.guides.title);
         $('#description').val(response.data.guides.description);
         $('#url').val(response.data.guides.url);
         $('#status').val(response.data.guides.status);
        $('#addNewModel1').modal('show');

    });
}

 function getAll(){
    ajx("POST", {  }, `${apiURL}/guide/get`, "json", function(response) {
        tableData.clear();
        if(response.data && response.data.guides){
            guides = response.data.guides;

            $.each(guides,function(i,guide){
              let button= '';
                    if(response.data.isAdmin){
                     button=  `
                   <span class="btn-group btn-flex">
                    <button type="button" class="btn btn-btn btn-primary btn-sm p-1 px-2 mt-0 btn-sm  " data-toggle="m" data-toggle="tooltip"
                   data-placement="top"  onclick="getItem(${guide.id})"   title="edit"><i class="fas fa-edit"></i>
               </button>
               <button type="button" class="btn btn-btn btn-primary btn-sm p-1 px-2 mt-0 btn-sm assign-to-user" data-toggle="modal" data-target="#OthersModel"
                   data-toggle="tooltip" onclick="deleteItem(${guide.id})" data-placement="top" title="Assign to Others"><i class="fas fa-trash"></i>
               </button></span>
                    `; 
                    }
                let tRow = tableData.row.add([
                    `${i+1}`,
                    `${guide.title}`,
                    ((guide.url)),
                    ((guide.status==1?'Active':'DeActive')),
                      button
                  ,
                ]).draw(false).node();
                $(tRow).attr("data-id", guide.id);
            });
        }
        tableData.draw();
    });
}

$(function () {
    $('input[name="date"]').daterangepicker({
        opens: 'left',
        singleDatePicker: true,
        showDropdowns: true,
        minDate: moment(),
        startDate: moment()
    }, function (start, end, label) {
        $("#date").val(start.format('MM/DD/YYYY'));
    });

    $('#switch1').on('change',function(evt){
        evt.preventDefault();
        $('#addCampaign').find('.assigned-users').toggle();
    });
    $('#switch1').trigger('change');

    $('#addNew').on('click',function(){
        $('#addNewModel').modal('show');
    });

    $('#addGuide').on('submit',function(evt){
        evt.preventDefault();
        var formData = new FormData($(this)[0]); 
// var files = $('#image')[0].files[0];
        // formData.append('file',files);
        $.ajax({
        type:'POST',
            url: `${apiURL}/guide/add`,
            data:formData,
            cache:false,
            contentType: false,
            processData: false, 
        complete: function() {
            loader();
        },
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            Accpet: "applicationjson"
        },
        success: function(response) {
            showMsg(response);
            if(response.status == 'success'){
                $('#addNewModel').modal('hide');
                $('#switch1').trigger('change');
                formReset('#addGuide');
                getAll();
            }
        }
        
    });
        // ajx("POST",  formData, `${apiURL}/guide/add`, "json", function(response) {
        //     showMsg(response);
        //     if(response.status == 'success'){
        //         $('#addNewModel').modal('hide');
        //         $('#switch1').trigger('change');
        //         formReset('#addCampaign');
        //         getAll();
        //     }
        // });
    });
  $('#editGuide').on('submit',function(evt){
        evt.preventDefault();
        var formData = new FormData($(this)[0]); 
        $.ajax({
        type:'POST',
            url: `${apiURL}/guide/update`,
            data:formData,
            cache:false,
            contentType: false,
            processData: false, 
        complete: function() {
            loader();
        },
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            Accpet: "applicationjson"
        },
        success: function(response) {
            showMsg(response);
            if(response.status == 'success'){
                $('#addNewModel1').modal('hide');
                $('#switch1').trigger('change');
                formReset('#editGuide');
                getAll();
            }
        }
        
    });
    });
    $('body').on('click','.assign-to-me',function(evt){
        evt.preventDefault();
        data = {};
        data.user = '';
        data.campaign = $(this).parents('tr').data('id');
        assignToMe(data);
    });

    $('body').on('click','.assign-to-user',function(evt){
        evt.preventDefault();
        $('#assignUser').find('[name="campaign"]').val($(this).parents('tr').data('id'));
        $('#assignUserModel').modal('show');
    });

    $('#assignUser').on('submit',function(evt){
        evt.preventDefault();
        assignToMe($(this).serialize());
    });

    getAll();
 });



  



 
function deleteItem(id){
  Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
    
  if (result.value) {
    ajx("POST", 
        {id:id}
        , `${apiURL}/guide/del`, "json", function(response) {
             if(response.status=='success'){
              getAll();
            Swal.fire(
      'Deleted!',
      'Item has been deleted.',
      'success'
    )}
        });
   
  }
})

}
function showBg(id){
    
    $(".al-boxes").css("display","none");
    const index = listOfGuides.findIndex( (item)   => item.id == id);
    $('#card-img-top').attr('src',listOfGuides[index].image);
    $('#BGdescription').html(listOfGuides[index].description);
    $('#BGtitle').html(listOfGuides[index].title);
    $('#BGupdate').html(listOfGuides[index].msg);
      $("#b-detail").css("display","block"); 
     }
function getItem(id){
    ajx("POST", {id:id }, `${apiURL}/guide/get`, "json", function(response) {
      
         $('#id').val(response.data.guides.id);
         $('#title').val(response.data.guides.title);
//         $('#description').val(response.data.guides.description);
        tinyMCE.activeEditor.setContent(response.data.guides.description)
         $('#url').val(response.data.guides.url);
         $('#status').val(response.data.guides.status);
        $('#addNewModel1').modal('show');

    });
}
 var listOfGuides;
 function getAll(){
 	loader('show');

    ajx("POST", {  }, `${apiURL}/guide/get`, "json", function(response) {
         let htmll='';
           listOfGuides=response.data.guides;
        if(response.data && response.data.guides){
           
        $('#listG').html('');
          
            guides = response.data.guides;
            $.each(guides,function(i,guide){
              let button= '';
                    if(response.data.isAdmin){
                     button=`<a class="dropdown-item" href="#" onclick="deleteItem(${guide.id})" ><i class="fas fa-trash"></i> Delete</a>`; 
                    }
          htmll+=` <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
               <div class="guide-box">
                  <img src="${ (guide.image)}">
                  <div class="text-guide">
                     <h4> ${ (guide.title)}</h4>
                     <div class="update-notify">  ${ (guide.msg)}</div>
                     <p>  ${ (guide.descriptionF)}  </p>
                  <a href="#" class="read-more" class="s-detail" onclick="showBg(${guide.id})">Read more</a>
                  </div>
               </div><div class="set-box" style="">
                     <span class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v dropdown-toggle"></i></span>
                     <div class="dropdown-menu dropdown-menu-right w-50">
                        <a class="dropdown-item" href="#" onclick="getItem(${guide.id})" ><i class="far fa-edit"></i> Edit</a>
                       ${button}
                     </div>
                  </div>
            </div>`;         
               
            });
        }
         $('#listG').append(htmll);loader();
    });
}

$(function () {
    $('input[name="date"]').daterangepicker({
        opens: 'left',
        singleDatePicker: true,
        showDropdowns: true,
        minDate: moment(),
        startDate: moment()
    }, function (start, end, label) {
        $("#date").val(start.format('MM/DD/YYYY'));
    });

     
   

    $('#addNew').on('click',function(){
        $('#addNewModel').modal('show');
    });

    $('#addGuide').on('submit',function(evt){
        evt.preventDefault();
        var formData = new FormData($(this)[0]); 
                formData.set('description',tinyMCE.activeEditor.getContent());

       loader('show');
        $.ajax({
        type:'POST',
            url: `${apiURL}/guide/add`,
            data:formData,
            cache:false,
            contentType: false,
            processData: false, 
        complete: function() {
            loader();
        },
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            Accpet: "applicationjson"
        },
        success: function(response) {
        	 loader();
            showMsg(response);
            if(response.status == 'success'){
                $('#addNewModel').modal('hide');
                $('#switch1').trigger('change');
                formReset('#addGuide');
                getAll();
            }
        }
        
    });
         
    });
  $('#editGuide').on('submit',function(evt){
        evt.preventDefault();
        var formData = new FormData($(this)[0]); 
        formData.set('description',tinyMCE.activeEditor.getContent());
        loader('show');
        $.ajax({
        type:'POST',
            url: `${apiURL}/guide/update`,
            data:formData,
            cache:false,
            contentType: false,
            processData: false, 
        complete: function() {
            loader();
        },
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            Accpet: "applicationjson"
        },
        success: function(response) {
            showMsg(response);
            if(response.status == 'success'){
                $('#addNewModel1').modal('hide');
                $('#switch1').trigger('change');
                formReset('#editGuide');
                getAll();
            }
        }
        
    });
    });
    
     

    

      getAll(); 
 });



  



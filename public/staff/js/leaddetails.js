function showMore() {
    $('#detailsModal').modal('show');
}
$('#addLeads').on('submit',function(evt){
        evt.preventDefault();
        var formData = new FormData($(this)[0]);
        console.log(formData);
       loader('show');
        $.ajax({
        type:'POST',
            url: `${apiURL}/leads/add`,
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
        complete: function() {
            loader();
        },
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            Accpet: "applicationjson"
        },
        success: function(response) {
           loader();
            showMsg(response);
                 // showOkWithReload(response);

            if(response.status == 'success'){
                $('#addNewModel').modal('hide');
                $('#switch1').trigger('change');
                formReset('#addLeads');
                 getAll();
            }
        }

    });

    });
$('#updateStatus').on('submit',function(evt){
        evt.preventDefault();
        var formData = new FormData($(this)[0]);
        console.log(formData);
       loader('show');
        $.ajax({
        type:'POST',
            url: `${apiURL}/deals/status`,
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
        complete: function() {
            loader();
        },
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            Accpet: "applicationjson"
        },
        success: function(response) {
           loader();
                 showOkWithReload(response);
            // showMsg(response);
            if(response.status == 'success'){
                $('#addNewModel1').modal('hide');
                $('#switch1').trigger('change');
                formReset('#updateStatus');
               // location.reload();

            }
        }

    });

    });
$('#sendMessage').on('submit',function(evt){
        evt.preventDefault();
       $('#usercommnts').css('display' , 'block');
        var formData = new FormData($(this)[0]);
       loader('show');
        $.ajax({
        type:'POST',
            url: `${apiURL}/leads/addComments`,
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
        complete: function() {
            loader();
        },
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            Accpet: "applicationjson"
        },
        success: function(response) {
           loader();
                 // showOkWithReload(response);
    getcomments();

//            showMsg(response);
            if(response.status == 'success'){
                // $('#addNewModel1').modal('hide');
                // $('#switch1').trigger('change');
                formReset('#sendMessage');
               // location.reload();

            }
        }

    });

    });
    $("#status").change(function(evt){
        evt.preventDefault();
        var formData = new FormData($('#statuschange')[0]);

       loader('show');
        $.ajax({
        type:'POST',
            url: `${apiURL}/deals/status`,
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
        complete: function() {
            loader();
        },
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            Accpet: "applicationjson"
        },
        success: function(response) {
           loader();
                 // showOkWithReload(response);
            showMsg(response);
            if(response.status == 'success'){
                $('#addNewModel1').modal('hide');
                $('#switch1').trigger('change');
                // formReset('#updateStatus');
            // getAll();
            // selectLeads(response.data.leads[0].id);



               // location.reload();

            }
        }

    });

    });
 function activeLeads(){
    loader('show');

    ajx("POST", { 'lead_id':$('#idd').val() }, `${apiURL}/leads/active`, "json", function(response) {
            showMsg(response);
            if(response.status=='success'){

$("#selecterMove").css("display", "block");
$("#selecterButton").css("display", "none");
            }


    });
}
function changeStatus(id,name,budget,status){
      $('#idd').val(id);
         $('#name').val(name);
         $('#budget').val(budget);
         $('#statuss').val(status);
    $('#addNewModel1').modal('show');
}
function showOkWithReload( res){
    let status = res.status.substr(0,1).toUpperCase()+res.status.substr(1);
Swal.fire({
    icon: res.status,
    title: status,
    text: res.msg,
  confirmButtonColor: '#3085d6',
  confirmButtonText: 'OK!'
}).then((result) => {
  if (result.value) {
   location.reload();
  }
})}
 $('#editLeads').on('submit',function(evt){
        evt.preventDefault();
        var formData = new FormData($(this)[0]);
        loader('show');
        $.ajax({
        type:'POST',
            url: `${apiURL}/leads/update`,
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
        complete: function() {
            loader();
        },
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            Accpet: "applicationjson"
        },
        success: function(response) {
            showOkWithReload(response);
            if(response.status == 'success'){
                $('#editNewModel').modal('hide');
                $('#switch1').trigger('change');
                formReset('#editLeads');
//                getAll();
            }
        }

    });
    });
var  listOfLeads=[];
 function getAll(){
    loader('show');

    ajx("POST", { search:$('#chat-search').val() }, `${apiURL}/leads/get`, "json", function(response) {
         let htmll='';
        console.log( response.data.leads);
        if(response.data && response.data.leads){

  listOfLeads=response.data.leads;
        $('#listG').html('');
            leads = response.data.leads;
            $.each(leads,function(i,leads){
              let button= 'active-chat';

          htmll+=`<a href="javascript:void(0)" onclick="selectLeads(${leads.id})" class="media a${leads.id}">
                        <div class="media-body new-message ">
                            <div>
                                <h6>${leads.name}</h6>
                                <p>Status: <span class="lead-status1">${leads.statusName}</span></p>
                            </div>
                            <div>
                                <span>${leads.created}</span>
                            </div>
                        </div>
                    </a>`;

            });
        }
         $('#listG').append(htmll);
         console.log('lll-',);
         if(response.data.leads.length){
            selectLeads(response.data.leads[0].id);
            }else{
                $('.detail-wrap1').hide();
            }
    });
}

function getcomments(){
    loader('show');

    ajx("POST", { 'lead_id':$('#idd').val() }, `${apiURL}/leads/comments`, "json", function(response) {
         let htmll='';
        console.log( response);
        if(response.data && response.data.comments.length > 0){
        $('#usercommnts').css('display' , 'block');
        $('#usercommnts').html('');
            comments = response.data.comments;
            $.each(comments,function(i,comments){
                let button= 'active-chat';

                var user_image = "/staff/images/user-1.jpg";
                if (comments.image != '' && comments.image != null) {
                    user_image = storage_url+'/avatars/'+comments.image;
                }

                htmll+= `<li class="user-comment">
                            <a href="javascript:;">
                                <img class="rounded-circle" width="40" height="40" src="${user_image}" alt="">
                            </a>
                            <div class="user-comment-right">
                                <div class="commentContent overflow">
                                   <a class="comment_name" href="javascript:;">${comments.userName}</a>
                                    <span class="commentTxt" dir="auto">${comments.message}</span>
                                </div>
                                <span class="title timeago">${comments.created}</span>
                            </div>
                        </li> `;

            });
        }
        else {
            $('#usercommnts').css('display' , 'none');
        }
         $('#usercommnts').append(htmll);
    });
}

function selectLeads(id){
  $('.detail-wrap1').show();
    $('.media').removeClass('active-chat');
    $('.a'+id).addClass('active-chat');
    const index = listOfLeads.findIndex( (item)   => item.id == id);
    $('#lLeadName').html(listOfLeads[index].name);
    $('#lEmail').html(listOfLeads[index].email);
    $('#lPhone').html(listOfLeads[index].phone);
    $('#lPoint').html(listOfLeads[index].point_of_contact);
    $('#lBudget').html(listOfLeads[index].email);
    $('#lSource').html(listOfLeads[index].source);
    $('#lBudget').html('€'+listOfLeads[index].budget);
    $('#userName').html(listOfLeads[index].userName);
    $('#idd').val(listOfLeads[index].id);
    $('#lead_id').val(listOfLeads[index].id);
    $('#status').val(listOfLeads[index].status);
    if(listOfLeads[index].active){

$("#selecterMove").css("display", "block");
$("#selecterButton").css("display", "none");


    }else{
$("#selecterButton").css("display", "block");
$("#selecterMove").css("display", "none");
    }
    getcomments();

     }
         getcomments();
$("body").on("click", ".edit_clone_block", function (evt) {
            evt.preventDefault();
            var total = $(".edit_question_block").length;
            total = total + 1;
            // console.log(total);
            var html =
                `<div class="edit_question_block">
            <div class="form-group mb-2">
               <label for="name" class="font-weight-bold black-text w-100">
                  Question <spna class="edit_q_count">` +
                total +
                `</spna>
                  <a href="javascript:;" class="text-danger float-right edit_remove_block" onclick="remove_edit_fields(this)"><i class="far fa-trash-alt"></i></a>
               </label>
               <input type="text"  required class="form-control heading" name="question[]" aria-describedby="Heading" placeholder="Question">
            </div>
            <div class="form-group">
               <input type="phone" required class="form-control answer"  name="answer[]" aria-describedby="Answer" placeholder="Answer">
            </div>
            <hr style="width: 50%">
         </div>`;

            $("#edit-form").append(html);
            // recountQuestionsBlk();
        });

$('#editLeadsForm').on('submit', function(evt) {
    evt.preventDefault();
    var formData = $(this).serialize();//new FormData($(this)[0]);
    // console.log(formData);
    // formData+='&leadId='+leadId;
    // var leadId=$('#idd').val();
    // console.log(listOfUsers);
    loader('show');
    $.ajax({
        type: 'POST',
        url: `${apiURL}/leads/update-lead`,
        data: formData,
        complete: function() {
            loader();
        },
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            Accpet: "applicationjson"
        },
        success: function(response) {
            loader();
            showMsg(response);
            // showOkWithReload(response);

            if (response.status == 'success') {
                $('#editNewModel').modal('hide');
                $('#switch1').trigger('change');
                location.reload();
                /*formReset('#addLeads');
                getAll();*/
            }
        }

    });

});

function editDeal() {
    $("#editNewModel").modal("show");
}

function editDealNew(id) {
    $("#editNewModel").modal("show");
}

function remove_edit_fields(e) {
    $(e).parent("label").parent("div").parent("div").remove();
    recountQuestionsEditBlk();
}

function recountQuestionsEditBlk() {
    var length = $(".edit_question_block").length;
    if (length > 0) {
        $.each($(".edit_question_block"), function (index, value) {
            var nextCount = parseInt(index) + 1;
            $(this).find(".edit_q_count").html(nextCount);
        });
    }
}

function deleteDeal() {
    ajx("POST", {
        'id': $('#idd').val()
    }, `${apiURL}/leads/del`, "json", function(res) {
        let status = res.status.substr(0,1).toUpperCase()+res.status.substr(1);
        Swal.fire({
            icon: res.status,
            title: status,
            text: res.msg,
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'OK!'
        }).then((result) => {
            if (result.value) {
                // location.reload();
                window.location.replace(appURL+ "/deals");
            }
        });
    });
}

function deleteDealNew(id) {
    ajx("POST", {
        'id': id
    }, `${apiURL}/leads/del`, "json", function(res) {
        let status = res.status.substr(0,1).toUpperCase()+res.status.substr(1);
        Swal.fire({
            icon: res.status,
            title: status,
            text: res.msg,
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'OK!'
        }).then((result) => {
            if (result.value) {
                // location.reload();
                window.location.replace(appURL+ "/deals");
            }
        });
    });
}

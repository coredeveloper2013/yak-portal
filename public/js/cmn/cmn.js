function loader(trigger = "hide") {
    if (trigger == "hide") {
        $("#page-loader").fadeOut("slow");
    } else {
        $("#page-loader").fadeIn("slow");
    }
}
function redirectURLCallBack(response) {
	if (response.status == "success") {
		if (response.data && response.data.redirectURL) {
			setTimeout(function() {
	            document.location = response.data.redirectURL;
			}, 1500);
		}
	}
}
function ajx(method="GET", param={}, path, dataType="json", cb, async = true) {
    $.ajax({
        url: path,
        async: async,
        type: method,
        data: param,
        dataType: dataType,
        beforeSend: function() {},
        complete: function() {
            loader();
        },
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            Accpet: "applicationjson"
        },
        success: function(response) {
            loader();
            redirectURLCallBack(response);
            cb(response);
        },
        error: function(xhr, status, error) {
            if (
                (typeof xhr.responseJSON.msg != "undefined" &&
                xhr.responseJSON.msg != "") || (typeof xhr.responseJSON.message != "undefined" &&
                xhr.responseJSON.message != "")
            ) {
                showMsg({
                    status: "error",
                    msg: `${xhr.responseJSON.message}`
                });
            } else {
                showMsg({
                    status: "error",
                    msg: "<strong>Oh snap!</strong> Something went wrong here. Please refresh the page and try again."
                });
            }
        }
    });
}

function $_redirect(url) {
    document.location = url;
}

function showMsg(res, settings = {}) {
    if (res.status == 'success') {
        var title = 'Success';
    } else {
        var title = 'Error';
    }
    Swal.fire({
        title:title,
        icon: res.status,
        text: res.msg,
        ...settings
    });
}
function showOkWithReload( res){
    Swal.fire({
        icon: res.status,
            text: res.msg,
      confirmButtonColor: '#3085d6',
      confirmButtonText: 'OK!'
    }).then((result) => {
      if (result.value) {
       location.reload();
      }
    })
}
function formReset(id, cb) {
    $(id)[0].reset();
    if (typeof cb != "undefined") {
        cb();
    }
}

function log(obj) {
    console.log(obj);
}

function actionButton(crrButton, disabled = true) {
    if (disabled) {
        $(crrButton)
            .find('[type="submit"]')
            .attr("disabled", "disabled");
    } else {
        $(crrButton)
            .find('[type="submit"]')
            .removeAttr("disabled");
    }
}

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 15);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/views/staff/retention/vue.js":
/*!************************************************!*\
  !*** ./resources/views/staff/retention/vue.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

new Vue({
  el: '#retentionContainer',
  mounted: function mounted() {
    this.getSalesData(false);
  },
  data: {
    fields: [],
    sales: [],
    salesMeta: {},
    isBusy: false,
    dataLoaded: false,
    getSalesDataUrl: App_url + '/leaderboard',
    offset: 0,
    lastOffset: 0,
    totalRecords: 0,
    limit: 0
  },
  watch: {
    'salesMeta.current_page': function salesMetaCurrent_page(val) {
      this.loadPaginatedData();
    }
  },
  methods: {
    loadPaginatedData: function loadPaginatedData() {
      this.getSalesData(this.getSalesDataUrl + '?page=' + this.salesMeta.current_page);
    },
    getSalesData: function getSalesData() {
      var _this = this;

      var url = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
      this.isBusy = true;

      if (url == false) {
        url = this.getSalesDataUrl;
      }

      axios.post(url, {
        offset: this.offset
      }).then(function (response) {
        console.log(response);

        if (response.data.status == 'success') {
          _this.isBusy = false;
          _this.fields = response.data.fields;
          _this.sales = response.data.data;
          _this.dataLoaded = true;
          _this.totalRecords = response.data.total;
          _this.offset = response.data.offset;
          _this.limit = response.data.limit;
          _this.lastOffset = response.data.lastoffset;
        }
      })["catch"](function (error) {
        console.log(error);
        _this.isBusy = false;
        _this.dataLoaded = true;
      });
    },
    setLastOffset: function setLastOffset() {
      this.offset = this.lastOffset;
      this.getSalesData(false);
    },
    numberWithComma: function numberWithComma(x) {
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    },
    Filtered: function Filtered(filteredItems) {
      // Trigger pagination to update the number of buttons/pages due to filtering
      this.landingPagesMeta.totalRows = filteredItems.length;
      this.landingPagesMeta.currentPage = 1;
    }
  }
});

/***/ }),

/***/ 15:
/*!******************************************************!*\
  !*** multi ./resources/views/staff/retention/vue.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\xampp\htdocs\yak-portal\resources\views\staff\retention\vue.js */"./resources/views/staff/retention/vue.js");


/***/ })

/******/ });
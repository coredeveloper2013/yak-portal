/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 9);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/views/staff/tasks/vue.js":
/*!********************************************!*\
  !*** ./resources/views/staff/tasks/vue.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

new Vue({
  el: '#taskContainer',
  mounted: function mounted() {
    this.getTasksStatus('pending'); //this.getTasks(false);
  },
  data: {
    todayTasks: [],
    upcomingTasks: [],
    todayTasksMeta: {},
    upcomingTasksMeta: {},
    currentUser: user_id,
    currentTask: '',
    comment: '',
    filter: {
      allTasks: false,
      myTasks: true,
      status: ''
    },
    showLoader: false,
    avatarBaseUrl: avatarBaseUrl,
    userAvatarBaseUrl: userAvatarBaseUrl,
    getTasksUrl: App_url + '/data',
    postCommentUrl: App_url + '/post/comment',
    updateStatusUrl: App_url + '/status/update',
    updateCheckListStatusUrl: App_url + '/checklist/status/update',
    removeAttachmentUrl: App_url + '/remove/attachment'
  },
  watch: {
    'tasksMeta.current_page': function tasksMetaCurrent_page(val) {
      this.loadPaginatedData();
    },
    currentTask: function currentTask(task) {
      this.updateCurrentCheckbox();
    }
  },
  methods: {
    loadPaginatedData: function loadPaginatedData() {
      this.getTasks(this.getTasksUrl + '?page=' + this.todayTasksMeta.current_page);
    },
    getTasks: function getTasks() {
      var _this = this;

      var url = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
      this.showLoader = true;

      if (url == false) {
        url = this.getTasksUrl;
      }

      axios.post(url, {
        filter: this.filter
      }).then(function (response) {
        if (response.data.status == 'success') {
          _this.todayTasks = response.data.todayTasks; // this.todayTasksMeta = response.data.todayTasks.meta;

          _this.upcomingTasks = response.data.upcomingTasks; // this.upcomingTasksMeta = response.data.upcomingTasks.meta;

          _this.showLoader = false;

          if (_this.todayTasks.length) {
            _this.currentTask = _this.todayTasks[0];

            _this.updateCurrentTask(_this.currentTask);

            setTimeout(function () {
              _this.updateCurrentCheckbox();
            }, 1000);
          } else if (_this.upcomingTasks.length) {
            _this.currentTask = _this.upcomingTasks[0];

            _this.updateCurrentTask(_this.currentTask);

            setTimeout(function () {
              _this.updateCurrentCheckbox();
            }, 1000);
          }
        }
      })["catch"](function (error) {
        _this.showLoader = false;
        console.log(error);
      });
    },
    getMyTasks: function getMyTasks() {
      this.filter = {
        myTasks: true,
        allTasks: false,
        status: ''
      };
      this.getTasks(false);
    },
    getAllTasks: function getAllTasks() {
      this.filter = {
        allTasks: true,
        myTasks: false,
        status: ''
      };
      this.getTasks(false);
    },
    getTasksStatus: function getTasksStatus(status) {
      this.filter.status = status;
      this.getTasks(false);
    },
    resetFilter: function resetFilter() {
      this.filter = {
        allTasks: false,
        myTasks: true,
        status: ''
      };
      this.getTasks(false);
    },
    showAddForm: function showAddForm() {
      window.location = App_url + '/create';
    },
    backToTasks: function backToTasks() {
      window.location = App_url + '/';
    },
    updateCurrentTask: function updateCurrentTask(task) {
      this.currentTask = task;
    },
    updateCurrentCheckbox: function updateCurrentCheckbox() {
      $(".leftsidecheck").removeClass("active");
      $("#customCheck" + this.currentTask.id).addClass("active");
      $(".leftsidecheck").prop("checked", false);
      $("#customCheck" + this.currentTask.id).prop("checked", true);
    },
    postComment: function postComment(task) {
      var _this2 = this;

      this.showLoader = true;
      axios.post(this.postCommentUrl, {
        comment: this.comment,
        taskId: task.id
      }).then(function (response) {
        if (response.data.status == 'success') {
          Swal.fire({
            title: 'Success',
            text: 'Comment Posted.',
            icon: 'success'
          });
          _this2.showLoader = false;
          _this2.currentTask = response.data.data;

          _this2.getTasksStatus(_this2.filter.status); //this.updateCurrentTask(this.currentTask);
          //this.getMyTasks(false);


          _this2.comment = '';
        }
      })["catch"](function (error) {
        _this2.showLoader = false;
        console.log(error);
      });
    },
    markTaskAsCompleted: function markTaskAsCompleted(task) {
      var _this3 = this;

      this.showLoader = true;

      if ($("#taskStatusCheckbox" + task.id).prop("checked") == true) {
        axios.post(this.updateStatusUrl, {
          status: 'completed',
          taskId: task.id
        }).then(function (response) {
          if (response.data.status == 'success') {
            _this3.showLoader = false;
            Swal.fire({
              title: 'Success',
              text: 'Task marked as completed',
              icon: 'success'
            }); // this.currentTask = response.data.data;
            // this.updateCurrentTask(this.currentTask);
          }
        })["catch"](function (error) {
          _this3.showLoader = false;
          console.log(error);
        });
      } else if ($("#taskStatusCheckbox" + task.id).prop("checked") == false) {
        this.showLoader = true;
        axios.post(this.updateStatusUrl, {
          status: 'pending',
          taskId: task.id
        }).then(function (response) {
          if (response.data.status == 'success') {
            _this3.showLoader = false;
            Swal.fire({
              title: 'Success',
              text: 'Task marked as pending.',
              icon: 'success'
            }); // this.currentTask = response.data.data;
            // this.updateCurrentTask(this.currentTask);
          }
        })["catch"](function (error) {
          _this3.showLoader = false;
          console.log(error);
        });
      }
    },
    markSubTaskAsCompleted: function markSubTaskAsCompleted(subTask, taskId) {
      var _this4 = this;

      if ($("#subTaskStatus" + subTask.id).prop("checked") == true) {
        var chkStatus = 'completed';
        var chkMsg = 'Checklist marked as completed.';
      } else {
        var chkStatus = 'pending';
        var chkMsg = 'Checklist marked as pending.';
      } // this.showLoader = true;


      axios.post(this.updateCheckListStatusUrl, {
        status: chkStatus,
        subTaskId: subTask.id,
        taskId: taskId
      }).then(function (response) {
        if (response.data.status == 'success') {
          //this.showLoader = false;
          Swal.fire({
            title: 'Success',
            text: chkMsg,
            icon: 'success',
            onAfterClose: function onAfterClose() {
              _this4.getTasksStatus(_this4.filter.status);
            }
          }); // this.currentTask = response.data.data;
          // this.updateCurrentTask(this.currentTask);
        }
      })["catch"](function (error) {
        _this4.showLoader = false;
        console.log(error);
      });
    },
    removeAttachment: function removeAttachment(taskId) {
      var _this5 = this;

      axios.post(this.removeAttachmentUrl, {
        taskId: taskId
      }).then(function (response) {
        if (response.data.status == 'success') {
          Swal.fire({
            title: 'Success',
            text: 'Attachment Removed Successfully.',
            icon: 'success'
          });

          _this5.getTasks();
        }
      })["catch"](function (error) {
        console.log(error);
      });
    },
    // checkIfMyTask: function(task){
    //create a new array where user id eist in task users array
    // return task.task_users.includes(this.currentUser);
    // },
    Filtered: function Filtered(filteredItems) {
      // Trigger pagination to update the number of buttons/pages due to filtering
      this.todayTasksMeta.totalRows = filteredItems.length;
      this.todayTasksMeta.currentPage = 1;
    }
  }
});

/***/ }),

/***/ 9:
/*!**************************************************!*\
  !*** multi ./resources/views/staff/tasks/vue.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\xampp\htdocs\yak-portal\resources\views\staff\tasks\vue.js */"./resources/views/staff/tasks/vue.js");


/***/ })

/******/ });
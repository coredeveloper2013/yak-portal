/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 16);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/views/staff/agreement/vue.js":
/*!************************************************!*\
  !*** ./resources/views/staff/agreement/vue.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

new Vue({
  el: '#agreements',
  mounted: function mounted() {
    this.getUserAgreements(false);
    var context = this;
    $('input[name="daterange"]').daterangepicker({
      opens: "center",
      maxDate: moment(),
      autoUpdateInput: false,
      orientation: "auto",
      locale: {
        cancelLabel: "Clear"
      }
    });
    $('input[name="daterange"]').on("apply.daterangepicker", function (ev, picker) {
      $(this).val(picker.startDate.format("MM/DD/YYYY") + " - " + picker.endDate.format("MM/DD/YYYY"));
      context.filter.selectedStartDate = picker.startDate.format("MM/DD/YYYY");
      context.filter.selectedEndDate = picker.endDate.format("MM/DD/YYYY");
      context.getUserAgreements(false);
    });
    $('input[name="daterange"]').on("cancel.daterangepicker", function (ev, picker) {
      $(this).val("");
      context.filter.selectedStartDate = "";
      context.filter.selectedEndDate = "";
      context.getUserAgreements(false);
    });
  },
  data: {
    fields: [{
      key: 'id',
      label: '#'
    }, {
      key: 'name',
      label: 'Name'
    }, {
      key: 'user.company',
      label: 'Business Name'
    }, {
      key: 'email',
      label: 'Email'
    }, {
      key: 'campaign',
      label: 'Campaign'
    }, {
      key: 'docu_status',
      label: 'Document Status'
    }, 'action'],
    agreements: [],
    agreementsMeta: {},
    filter: {
      selectedStartDate: '',
      selectedEndDate: '',
      status: ''
    },
    user: session_user,
    isBusy: false,
    markAsSignedUrl: apiURL + '/agreements/signed',
    getAgreementsUrl: apiURL + '/agreements/data',
    resendAgreementUrl: apiURL + '/agreements/resend'
  },
  watch: {
    'agreementsMeta.current_page': function agreementsMetaCurrent_page(val) {
      this.loadPaginatedData();
    }
  },
  methods: {
    loadPaginatedData: function loadPaginatedData() {
      this.getUserAgreements(this.getAgreementsUrl + '?page=' + this.agreementsMeta.current_page);
    },
    getUserAgreements: function getUserAgreements() {
      var _this = this;

      var url = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
      this.isBusy = true;

      if (url == false) {
        url = this.getAgreementsUrl;
      }

      axios.post(url, {
        filter: this.filter
      }).then(function (response) {
        if (response.data.status == 'success') {
          _this.isBusy = false;
          _this.agreements = response.data.data;
          _this.agreementsMeta = response.data.meta;
        }
      })["catch"](function (error) {
        console.log(error);
        _this.isBusy = false;
      });
    },
    resetFilter: function resetFilter() {
      this.filter = {
        selectedStartDate: '',
        selectedEndDate: '',
        status: ''
      };
      $("#daterange").val('');
      this.getUserAgreements(false);
    },
    resendAgreement: function resendAgreement(agreement_id) {
      var _this2 = this;

      console.log(agreement_id); //return false;

      axios.post(this.resendAgreementUrl, {
        id: agreement_id
      }).then(function (response) {
        if (response.data.status == 'success') {
          _this2.getUserAgreements(false);

          var resendTitle = 'Success';
        } else {
          var resendTitle = 'Error';
        }

        Swal.fire({
          title: resendTitle,
          text: response.data.msg,
          icon: response.data.status
        });
      })["catch"](function (error) {
        console.log(error);
        _this2.isBusy = false;
      });
    },
    markAsSigned: function markAsSigned(agreement_id) {
      var _this3 = this;

      console.log(agreement_id); //return false;

      axios.post(this.markAsSignedUrl, {
        id: agreement_id
      }).then(function (response) {
        if (response.data.status == 'success') {
          _this3.getUserAgreements(false);

          var resendTitle = 'Success';
        } else {
          var resendTitle = 'Error';
        }

        Swal.fire({
          title: resendTitle,
          text: response.data.msg,
          icon: response.data.status
        });
      })["catch"](function (error) {
        console.log(error);
        _this3.isBusy = false;
      });
    },
    numberWithComma: function numberWithComma(x) {
      if (x != undefined && x != "" && x != null) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      } else {
        return '0';
      }
    },
    Filtered: function Filtered(filteredItems) {
      // Trigger pagination to update the number of buttons/pages due to filtering
      this.agreementsMeta.totalRows = filteredItems.length;
      this.agreementsMeta.currentPage = 1;
    }
  }
});

/***/ }),

/***/ 16:
/*!******************************************************!*\
  !*** multi ./resources/views/staff/agreement/vue.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\xampp\htdocs\yak-portal\resources\views\staff\agreement\vue.js */"./resources/views/staff/agreement/vue.js");


/***/ })

/******/ });
var leads = [];

$('.changeStatusBtn').hide();

function detailWrapHtml(data){
    let html = ``;
    let modal_html = ``;
    if (typeof(data) !== undefined && typeof(data.lead_questions) !== undefined && data.lead_questions.length > 0) {
        $.each(data.lead_questions, function(index, value) {
            if(index < 2) {
                html += `<div class="row details-custom-row" data-qid="${value.id}">
                            <div class="col-xl-6 col-lg-6 col-md-6 frst-col"><b>${value.headings}</b></div>
                            <div class="col-xl-6 col-lg-6 col-md-6">${value.answers}</div>
                        </div>`;
            }
            modal_html += `<div class="row details-custom-row" data-qid="${value.id}">
                            <div class="col-xl-6 col-lg-6 col-md-6 frst-col"><b>${value.headings}</b></div>
                            <div class="col-xl-6 col-lg-6 col-md-6">${value.answers}</div>
                        </div>`;
        });
    } else {
        html = `<div class="alert alert-custom m-0">No Record Found</div>`;
    }

    if(data.lead_questions.length > 2) {
        $('#btn-details-load').show();
    }
    else {
        $('#btn-details-load').hide();
    }

    $('.detail-wrap').html(html);
    $('#modal-details-wrap').html(modal_html);
}

function loadChat(result = ''){
    let html = '';

    if(result != ''){
        $.each(result,function(i,msg){
            if (msg.userdata.image != null && msg.userdata.image != '') {
                msg.userdata.image = storagePath+'/'+msg.userdata.image;
            } else {
                msg.userdata.image = defaultImage;
            }
            html += `   <li class="user-comment">
                            <a href="javascript:;">
                                <img class="rounded-circle" width="40" height="40" src="${msg.userdata.image}" alt="">
                            </a>
                            <div class="user-comment-right">
                                <div class="commentContent overflow">
                                   <a class="comment_name" href="javascript:;">${msg.userdata.name}</a>
                                    <span class="commentTxt" dir="auto">${msg.message}</span>
                                </div>
                                <span class="title timeago">${msg.created_at_date}</span>
                            </div>
                        </li>`;
        });
    }

    $('.charHistory').html(html);
}

function getDetailWrap(data = { campaign: campaign,lead:lead }, loadLeads){
    ajx("POST", data, `${apiURL}/leads/get-leads`, "json", function(response) {
        $('.edit_lead').hide();
        if(response.data && response.data.leads){
            leads = response.data.leads;
            $('.edit_lead').show();
            $('#sendMessage').find('[name=lead_id]').val(leads.id);

            if (loadLeads) {
                leadSearch();
            }

            detailWrapHtml(leads);
            getComments();

            changeStatusButton(leads);
        }
        else {
            $('.load_more').addClass('d-none');
            $('.no_record').removeClass('d-none');
        }
    },false);
}

function showMore() {
    $('#detailsModal').modal('show');
}

function changeStatusButton(leads){
    $('.changeStatusBtn').show();
    if(leads.status == 'open'){
        $('.changeStatusBtn').html('Mark as Closed');
        $('.changeStatusBtn').data('status','closed');
    }else{
        $('.changeStatusBtn').html('Mark as Open');
        $('.changeStatusBtn').data('status','open');
    }
    $('.changeStatusBtn').data('campaign',leads.campaign);
    $('.changeStatusBtn').data('lead',leads.id);

    $('.delete-lead').data('lead',leads.id);
    $('.delete-lead').data('campaign',leads.campaign);
}

function getComments(){
    ajx("POST", { id: leads.id }, `${apiURL}/leads/get-comments`, "json", function(response) {
        $('.charHistory').html('');
        $('.charHistory').addClass('d-none');
        if(response.data && response.data.comments){
            $('.charHistory').removeClass('d-none');
            comments = response.data.comments;
            loadChat(comments);
        }
    },false);
}

function leadSearch(data = '', append){
    data = `${data}&campaign=${campaign}`;

    if (typeof append == 'undefined' || append == 'undefined' ) {
        append = 'load';
    }

    ajx("POST", data, `${apiURL}/leads/lead-search`, "json", function(response) {
        if (append != 'next') {
            $('#general_chat').html('');
        }

        if(response.status == 'success'){
            $('.lead_detail_block').removeClass('d-none');
            loadLeads(response.data.leads);

            if (append == 'next' && response.data.leads.length <= 5) {
                //$('.load_more').addClass('d-none');
                $('.no_record').addClass('d-none');
            } else if (response.data.leads.length > 0) {
                $('.no_record').addClass('d-none');
                if (response.data.leads.length <= 5) {
                    $('.load_more').addClass('d-none');
                } else {
                    $('.load_more').removeClass('d-none');
                }

            } else {
                $('.no_record').removeClass('d-none');
            }
        } else {
            if (append == 'search') {
                $('.lead_detail_block').addClass('d-none');
            } else {
                $('.lead_detail_block').removeClass('d-none');
            }

            if (append == 'next') {
                $('.no_record').html('<b>No more Leads</b>');
            } else {
                $('.no_record').html('<b>No Record Found</b>');
            }

            $('.load_more').addClass('d-none');
            $('.no_record').removeClass('d-none');
        }
    });
}

function loadLeads(result = ''){
    let html = '';

    if(result != ''){
        $.each(result,function(i,lead){
            let attr = `data-lead="${lead.id}" data-campaign="${lead.campaign}"`;
            let className = 'load-lead';

            if(lead.id == leads.id){
                className = `load-lead active-chat`;
            }
            html += `<a ${attr} href="javascript:void(0)" class="media ${className}">
                        <div class="media-body">
                            <div>
                                <h6>${lead.lead_no}</h6>
                                <p>Status :<span class="lead-status">${lead.status_upper}</span></p>
                            </div>
                            <div>
                                <span>${lead.submitted_on_date}</span>
                            </div>
                        </div>
                    </a>`;
        });
    }


    $('#general_chat').append(html);
}

function recountQuestionsBlk() {
    var length = $('.question_block').length;
    if (length > 0) {
        $.each($('.question_block'), function( index, value ) {
            var nextCount = parseInt(index) + 1;
            $(this).find('.q_count').html(nextCount);
        });
    }
}

$(function(){

    $('.add_lead').on('click',function(evt){
        evt.preventDefault();
        //formReset('#addEditLeadForm');
        $('#addEditLead').find('.modal-title').html('Add Lead');
        $('#addEditLead').find('[name=lead_id]').val('');
        $('#addEditLead').modal('show');
    });

    $('body').on('click', '.load_more button', function(event) {
        event.preventDefault();
        var lastLead = $('#general_chat').find('a:last-child');
        var lastLeadId = lastLead.data('lead');
        leadSearch('last_lead_id='+lastLeadId, 'next');
    });

    $('.clone_block').on('click',function(evt){
        evt.preventDefault();
        var html = $('.question_block:last-child').clone();
        html.find('input').val('');
        html.find('.remove_block').removeClass('d-none');
        $('.append_block').append(html);
        recountQuestionsBlk();
    });

    $('body').on('click', '.remove_block', function(event) {
        event.preventDefault();
        var parent = $(this).parents('.question_block');
        parent.remove();
        recountQuestionsBlk();
    });

    $('#addEditLeadForm').on('submit',function(evt){
        evt.preventDefault();
        //loader('show');
        actionButton('#addEditLeadForm',true);
        ajx("POST", $(this).serialize(), `${apiURL}/leads/save-lead`, "json", function(response) {
            actionButton('#addEditLeadForm',false);
            if(response.status == 'success'){
                Swal.fire({
                    title: 'Success',
                    text: response.msg,
                    icon: response.status,
                    onAfterClose: () => {
                        //window.location = this.redirectUrl;
                        location.reload();
                    }
                });
            } else {
                Swal.fire({
                    title: response.status,
                    text: response.msg,
                    icon: response.status
                });
            }
        });
    });

    $('#sendMessage').on('submit',function(evt){
        evt.preventDefault();
        // loader('show');
        ajx("POST", $(this).serialize(), `${apiURL}/leads/save-comment`, "json", function(response) {
            if(response.status == 'success'){
                showMsg(response);
                formReset('#sendMessage');
                getComments();
                // loader('hide');
            }
        });
    });

    $('#lead-search').on('submit',function(evt){
        evt.preventDefault();
        loader('show');
        leadSearch($(this).serialize(), 'search');
    });

    $('.changeStatusBtn').on('click',function(evt){
        evt.preventDefault();
        let data = $(this).data();

        ajx("POST", data, `${apiURL}/leads/status`, "json", function(response) {
            showMsg(response);
            if(response.status == 'success'){
                changeStatusButton(response.data.leads);
                $('#lead-search').trigger('submit');
            }
        });
    });

    $('body').on('click', '.delete-lead', function(event) {
        event.preventDefault();
        var that = $(this);
        var data = that.data();

        ajx("POST", data, `${apiURL}/leads/delete`, "json", function(response) {
            if (response.status == 'success') {
                var title = 'Success';
            } else {
                var title = 'Error';
            }
            Swal.fire({
                title:title,
                icon: response.status,
                text: response.msg,
                onAfterClose: () => {
                    location.reload();
                }
            });
        });
    });

    $('body').on('change', '#campaign_status', function(event) {
        event.preventDefault();
        var that = $(this);
        var val = that.val();
        if (val == "") {
            return false;
        }

        ajx("POST", {status:val, campaign:campaign}, `${apiURL}/campaigns/status`, "json", function(response) {
            if(response.status == 'success'){
                showMsg(response);
            }
        });
    });

    $('body').on('click','.load-lead',function(evt){
        evt.preventDefault();
        loader('show');
        data = $(this).data();
        $(this).addClass('active-chat').siblings().removeClass('active-chat');
        getDetailWrap(data, false);
    });

    getDetailWrap({campaign: campaign}, true);
});

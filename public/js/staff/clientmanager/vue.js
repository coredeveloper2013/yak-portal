/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 14);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/views/staff/clientmanager/vue.js":
/*!****************************************************!*\
  !*** ./resources/views/staff/clientmanager/vue.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

new Vue({
  el: '#clientManagerContainer',
  mounted: function mounted() {
    this.getClients();
  },
  watch: {},
  data: {
    fieldsForCampaigns: [{
      key: 'id',
      label: 'Campaign ID'
    }, {
      key: 'start_date',
      label: 'Start Date'
    }, {
      key: 'name',
      label: 'Campaign Name'
    }, {
      key: 'landing_page_name',
      label: 'Landing Page'
    }, {
      key: 'amount',
      label: 'Contract Value'
    }, {
      key: 'compaign_amount',
      label: 'Budget'
    }, 'type', 'status', 'Actions'],
    fieldsForOtherProjects: [{
      key: 'id',
      label: 'Order'
    }, {
      key: 'project_name',
      label: 'Name'
    }, {
      key: 'human_start_date',
      label: 'Start Date'
    }, {
      key: 'human_end_date',
      label: 'End Date'
    }, {
      key: 'price',
      label: 'Price'
    }, {
      key: 'budget',
      label: 'Budget'
    }, {
      key: 'cost_type',
      label: 'Cost type'
    }, {
      key: 'status_upper',
      label: 'Status'
    }, 'Actions'],
    fieldsForLogs: [{
      key: 'uri',
      label: 'URL'
    }, {
      key: 'ip_address',
      label: 'IP Address'
    }, {
      key: 'Name',
      label: 'Name'
    }, {
      key: 'date',
      label: 'Date',
      sortable: true
    }, {
      key: 'time',
      label: 'Time',
      sortable: true
    }],
    clients: [],
    currentClient: '',
    campaigns: [],
    landingPages: [],
    otherProjects: [],
    logs: [],
    logFilter: null,
    filter: {
      queryString: '',
      status: ''
    },
    userAvatarBaseUrl: userAvatarBaseUrl,
    getClientUrl: App_url + '/get/clients',
    leadsUrl: main_url + '/staff/campaign-leads',
    campaignDetailsUrl: main_url + '/staff/campaign-detail',
    landingDetailsUrl: main_url + '/staff/landing-page-details',
    removeClientUrl: App_url + '/remove/client',
    exportLogsUrl: App_url + '/export/client'
  },
  computed: {},
  wathc: {
    logFilter: function logFilter(text) {
      console.log(text);
      this.logFilter = text;
    }
  },
  methods: {
    getClients: function getClients() {
      var _this2 = this;

      axios.post(this.getClientUrl, {
        filter: this.filter
      }).then(function (response) {
        if (response.data.status == 'success') {
          _this2.clients = response.data.data;
          _this2.currentClient = response.data.data[0];

          if (_this2.currentClient) {
            _this2.updateCurrentClient(_this2.currentClient);

            _this2.updateLogs(_this2.currentClient);
          }
        }
      })["catch"](function (error) {
        console.log(error);
      });
    },
    updateCurrentClient: function updateCurrentClient(user) {
      this.currentClient = user;
      this.campaigns = user.campaigns;
      this.landingPages = user.landing_pages;
      this.otherProjects = user.other_projects;
    },
    updateLogs: function updateLogs(user) {
      _this = this;
      this.logs.items = [];
      this.logs.perPage = 10;
      this.logs.current = 1;

      if (user.request_logs.length > 0) {
        user.request_logs.forEach(function myFunction(log) {
          var temp = JSON.parse(log.log_details.content);
          var temp_log = [];
          temp_log.uri = App_url + temp.uri;
          temp_log.ip_address = temp.ip_address;
          temp_log.name = _this.currentClient.name;
          temp_log.date = moment(log.log_details.created_at).format("MMMM D YYYY");
          temp_log.time = moment(log.log_details.created_at).format("hh:mm A");

          _this.logs.items.push(temp_log);
        });
      }

      this.logs.totalRows = this.logs.items.length; // this.logs = user.request_logs;
    },
    currentPage: function currentPage() {
      console.log('here');
      return 1;
    },
    ucFirst: function ucFirst(string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    },
    numberWithComma: function numberWithComma(x) {
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    },
    getLeadsUrl: function getLeadsUrl(id) {
      return "".concat(this.leadsUrl, "/").concat(id);
    },
    getCampaignDetailsUrl: function getCampaignDetailsUrl(id) {
      return "".concat(this.campaignDetailsUrl, "/").concat(id);
    },
    getLandingDetailsUrl: function getLandingDetailsUrl(id) {
      return "".concat(this.landingDetailsUrl, "/").concat(id);
    },
    deleteUser: function deleteUser(userId) {
      var _this3 = this;

      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
      }).then(function (result) {
        if (result.value) {
          axios.post(_this3.removeClientUrl, {
            id: userId
          }).then(function (response) {
            if (response.data.status == 'success') {
              Swal.fire({
                title: 'Deleted!',
                text: 'Client Removed Successfully.',
                icon: 'success'
              });

              _this3.getClients();
            } else {
              Swal.fire({
                title: 'Error!',
                text: response.data.msg,
                icon: 'error'
              });
            }
          })["catch"](function (error) {
            console.log(error);
          });
        }
      });
    },
    editClient: function editClient(client) {
      $('.password-blk').addClass('d-none');
      $('.c-password-blk').addClass('d-none');
      $("#userId").val(client.id);
      $("#name").val(client.name);
      $("#job_position").val(client.job_position);
      $("#mobile").val(client.phone);
      $("#email").val(client.email);
      $("#status").val(client.status); // $("#password").val(client.password);
      // $("#password_confirmation").val(client.password);

      if (client.company) {
        $("#company").val(client.company.name);
      } else {
        $("#company").val('');
      }

      if (client.business_info) {
        $("#work_email").val(client.business_info.email_address);
        $("#work_phone").val(client.business_info.phone);
        $("#address").val(client.business_info.address);
        $("#website").val(client.business_info.website);
        $("#facebook_link").val(client.business_info.facebook_link);
        $("#twitter_link").val(client.business_info.twitter_link);
        $("#instagram_link").val(client.business_info.instagram_link);
        $("#notes").val(client.business_info.notes);
      } else {
        $("#work_email").val('');
        $("#work_phone").val('');
        $("#address").val('');
        $("#website").val('');
        $("#facebook_link").val('');
        $("#twitter_link").val('');
        $("#instagram_link").val('');
        $("#notes").val('');
      }

      $("#addNewClientModal").modal('show');
    },
    addNewClient: function addNewClient() {
      $('.password-blk').removeClass('d-none');
      $('.c-password-blk').removeClass('d-none');
      $("#userId").val('');
      $("#name").val('');
      $("#job_position").val('');
      $("#mobile").val('');
      $("#email").val('');
      $("#company").val('');
      $("#work_email").val('');
      $("#work_phone").val('');
      $("#address").val('');
      $("#website").val('');
      $("#facebook_link").val('');
      $("#twitter_link").val('');
      $("#instagram_link").val('');
      $("#notes").val('');
      $("#addNewClientModal").modal('show');
    },
    filterStatus: function filterStatus(status) {
      this.filter.status = status;
      this.getClients();
    },
    resetFilter: function resetFilter() {
      this.filter = {
        queryString: '',
        status: ''
      };
      this.getClients();
    },
    exportlogs: function exportlogs() {
      axios.post(this.exportLogsUrl, {
        filter: this.filter,
        user_id: this.currentClient.id,
        logFilter: this.logFilter
      }).then(function (response) {
        console.log(response.data.data);

        if (response.data.status == 'success') {
          console.log(response.data.data);
          window.open(response.data.data, '_blank');
        }
      })["catch"](function (error) {
        console.log(error);
      });
    }
  }
});

/***/ }),

/***/ 14:
/*!**********************************************************!*\
  !*** multi ./resources/views/staff/clientmanager/vue.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\xampp\htdocs\yak-portal\resources\views\staff\clientmanager\vue.js */"./resources/views/staff/clientmanager/vue.js");


/***/ })

/******/ });
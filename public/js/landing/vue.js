/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/views/landing/vue.js":
/*!****************************************!*\
  !*** ./resources/views/landing/vue.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

new Vue({
  el: '#landingPage',
  mounted: function mounted() {
    this.getUserLandingPages(false);
    var context = this;
    $('input[name="daterange"]').daterangepicker({
      opens: "center",
      maxDate: moment(),
      autoUpdateInput: false,
      orientation: "auto",
      locale: {
        cancelLabel: "Clear"
      }
    });
    $('input[name="daterange"]').on("apply.daterangepicker", function (ev, picker) {
      $(this).val(picker.startDate.format("MM/DD/YYYY") + " - " + picker.endDate.format("MM/DD/YYYY"));
      context.filter.selectedStartDate = picker.startDate.format("MM/DD/YYYY");
      context.filter.selectedEndDate = picker.endDate.format("MM/DD/YYYY");
      context.getUserLandingPages(false);
    });
    $('input[name="daterange"]').on("cancel.daterangepicker", function (ev, picker) {
      $(this).val("");
      context.filter.selectedStartDate = "";
      context.filter.selectedEndDate = "";
      context.getUserLandingPages(false);
    });
  },
  data: {
    landingPages: [],
    landingPagesMeta: {},
    filter: {
      selectedStartDate: '',
      selectedEndDate: '',
      status: ''
    },
    ticketSubject: '',
    isBusy: false,
    disableSubmitBtn: false,
    redirectUrl: appURL + '/support-ticket',
    getLandingPagesUrl: App_url + '/data',
    createTicketUrl: apiURL + '/support-ticket/add/ticket'
  },
  watch: {
    'landingPagesMeta.current_page': function landingPagesMetaCurrent_page(val) {
      this.loadPaginatedData();
    }
  },
  methods: {
    loadPaginatedData: function loadPaginatedData() {
      this.getUserLandingPages(this.getLandingPagesUrl + '?page=' + this.landingPagesMeta.current_page);
    },
    getUserLandingPages: function getUserLandingPages() {
      var _this = this;

      var url = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
      this.isBusy = true;

      if (url == false) {
        url = this.getLandingPagesUrl;
      }

      axios.post(url, {
        filter: this.filter
      }).then(function (response) {
        if (response.data.status == 'success') {
          _this.isBusy = false;
          _this.landingPages = response.data.data;
          _this.landingPagesMeta = response.data.meta;
        }
      })["catch"](function (error) {
        console.log(error);
        _this.isBusy = false;
      });
    },
    resetFilter: function resetFilter() {
      this.filter = {
        selectedStartDate: '',
        selectedEndDate: '',
        status: ''
      };
      $("#daterange").val('');
      this.getUserLandingPages(false);
    },
    showTicketModal: function showTicketModal(landVal) {
      this.ticketSubject = "Edit request of Landing Page (" + landVal + ")";
      $('#addTicketModal').modal('show');
    },
    createTicket: function createTicket() {
      var _this2 = this;

      this.disableSubmitBtn = true;
      var staffIds = [];

      _.forEach(this.staff_id, function (value, key) {
        staffIds.push(value.id);
      });

      console.log(staffIds);
      var formData = new FormData($('#add_ticket')[0]); //formData.append('subject', this.ticketSubject);
      //formData.append('question', this.ticketSubject);
      //formData.append('file', this.ticketSubject);

      var headersArr = {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      };
      axios.post(this.createTicketUrl, formData, headersArr).then(function (response) {
        _this2.disableSubmitBtn = false;
        console.log(response);
        var res = response.data;
        var status = res.status.substr(0, 1).toUpperCase() + res.status.substr(1);

        if (res.status == 'Success') {
          Swal.fire({
            title: status,
            text: res.msg,
            icon: 'success',
            onAfterClose: function onAfterClose() {
              //window.location = this.redirectUrl;
              location.reload();
            }
          });
        } else {
          Swal.fire({
            title: status,
            text: res.msg,
            icon: 'error'
          });
        }
      })["catch"](function (error) {
        console.log(error);
        _this2.disableSubmitBtn = false;
      });
    },
    numberWithComma: function numberWithComma(x) {
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    },
    Filtered: function Filtered(filteredItems) {
      // Trigger pagination to update the number of buttons/pages due to filtering
      this.landingPagesMeta.totalRows = filteredItems.length;
      this.landingPagesMeta.currentPage = 1;
    }
  }
});

/***/ }),

/***/ 4:
/*!**********************************************!*\
  !*** multi ./resources/views/landing/vue.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\xampp\htdocs\yak-portal\resources\views\landing\vue.js */"./resources/views/landing/vue.js");


/***/ })

/******/ });
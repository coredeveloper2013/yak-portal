var tableData = $(".tableData").DataTable({
  "searching": false,
  "lengthChange": false,
  "targets": 'no-sort',
  "bSort": false,
  "order": [],
});
function deleteUser(id){
  Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {

    if (result.value) {
      ajx("POST",
        {id:id}
        , `${apiURL}/staffAccount/del`, "json", function(response) {
         if(response.status=='success'){
          getCampaigns();
          Swal.fire(
            'Deleted!',
            'User has been deleted.',
            'success'
            )}
        });

    }
  })

}
function getEditUser(id){
  ajx("POST", {id:id }, `${apiURL}/staffAccount/get`, "json", function(response) {

   $('#id').val(response.data.users.id);
   $('#name').val(response.data.users.name);
   $('#email').val(response.data.users.email);
   $('#no_of_leave').val(response.data.users.no_of_leave);
 if (response.data.users.type == "staff"){

                $("#noOfleavediv2").css("display", "block");
                } else{

                $("#noOfleavediv2").css("display", "none");
                }
         // $('#username').val(response.data.users.username);
         $('#type').val(response.data.users.type);
         $('#addNewModel1').modal('show');

       });
}
$('.filter').on('click', function() {
  $('.filter').removeClass('Factive');

  $(this).addClass('Factive');
});


function getCampaigns(type='all'){

  ajx("POST", { type:type }, `${apiURL}/staffAccount/get`, "json", function(response) {
    tableData.clear();
    if(response.data && response.data.users){
      users = response.data.users;

      $.each(users,function(i,user){
        let button= '';
        if(response.data.isAdmin){
         button=  `
         <span class="btn-group btn-flex">
         <button type="button" class="btn btn-btn btn-primary btn-sm p-1 px-2 mt-0 btn-sm  " data-toggle="m" data-toggle="tooltip"
         data-placement="top"  onclick="getEditUser(${user.id})"   title="edit"><i class="fas fa-edit"></i>
         </button>
         <button type="button" class="btn btn-btn btn-primary btn-sm p-1 px-2 mt-0 btn-sm assign-to-user" data-toggle="modal" data-target="#OthersModel"
         data-toggle="tooltip" onclick="deleteUser(${user.id})" data-placement="top" title="Assign to Others"><i class="fas fa-trash"></i>
         </button></span>
         `;
       }
       let tRow = tableData.row.add([
        `${i+1}`,
        `${user.name}`,
                    // ((user.username)   ),
                    ((user.email)),
                    ((user.type)),
                    button
                    ,
                    ]).draw(false).node();
       $(tRow).attr("data-id", user.id);
       $(tRow).attr("class", user.type);
     });
    }
    tableData.draw();
  });
}

$(function () {
  $('input[name="date"]').daterangepicker({
    opens: 'left',
    singleDatePicker: true,
    showDropdowns: true,
    minDate: moment(),
    startDate: moment()
  }, function (start, end, label) {
    $("#date").val(start.format('MM/DD/YYYY'));
  });

  $('#switch1').on('change',function(evt){
    evt.preventDefault();
    $('#addCampaign').find('.assigned-users').toggle();
  });
  $('#switch1').trigger('change');

  $('#addNew').on('click',function(){
    $('#addNewModel').modal('show');
  });

  $('#addCampaign1').on('submit',function(evt){
    evt.preventDefault();
    ajx("POST", $(this).serialize(), `${apiURL}/unbounce/add`, "json", function(response) {
      showMsg(response);
      if(response.status == 'success'){
        $('#addNewModel').modal('hide');
        // $('#switch1').trigger('change');
        formReset('#addCampaign1');
          location.reload();
        // getCampaigns();
      }
    });
  });
  $('#editCampaign').on('submit',function(evt){
    evt.preventDefault();
    ajx("POST", $(this).serialize(), `${apiURL}/staffAccount/update`, "json", function(response) {
      showMsg(response);
      if(response.status == 'success'){
        $('#addNewModel1').modal('hide');
        $('#switch1').trigger('change');
         formReset('#editCampaign');
          location.reload();
        // getCampaigns();
      }
    });
  });

  $('body').on('click','.assign-to-me',function(evt){
    evt.preventDefault();
    data = {};
    data.user = '';
    data.campaign = $(this).parents('tr').data('id');
    assignToMe(data);
  });

  $('body').on('click','.assign-to-user',function(evt){
    evt.preventDefault();
    $('#assignUser').find('[name="campaign"]').val($(this).parents('tr').data('id'));
    $('#assignUserModel').modal('show');
  });

  $('#assignUser').on('submit',function(evt){
    evt.preventDefault();
    assignToMe($(this).serialize());
  });

  // getCampaigns();
});



        $("#iType").change(function(evt){
            if ($("#iType").val() == "staff"){

                $("#noOfleavediv").css("display", "block");
                } else{

                $("#noOfleavediv").css("display", "none");
                }

                });


        $("#type").change(function(evt){
            if ($("#type").val() == "staff"){

                $("#noOfleavediv2").css("display", "block");
                } else{

                $("#noOfleavediv2").css("display", "none");
                }

                });

        $("#is_new_bussiness_lead").change(function(evt){
            if (this.checked == false){

                $(".select_compaign_id").css("display", "block");
                } else{

                $(".select_compaign_id").css("display", "none");
                }

                });

function showUserCampaign(user_id)
{
    $('#compaign_id option').each(function() {
        if (user_id == $(this).data('user'))
        {
            $(this).show();
        }
        else
        {
            $(this).hide();
        }
    });


}
function deleteUnbounce(id)
{
    ajx("POST", {id : id}, `${apiURL}/unbounce/delete`, "json", function(response) {
        showMsg(response);
        if(response.status == 'success'){
            location.reload();
        }
    });

}

function showUserUnbounce(user_id) {
    window.location.replace(appURL + "/unbounce/" + user_id);
}


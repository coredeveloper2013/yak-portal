<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes(['verify' => true]);

Route::group(['middleware' => ['staff'], 'prefix' => 'staff', 'as' => '_v2.staff.', 'namespace' => '_v2\Staff'], function () {
    // Home Routes
    Route::get('/home-2', 'HomeController@index')->middleware('permissionStaff')->name('home-2');
    Route::get('/dashboard-data', 'HomeController@dashboardData')->name('dashboardData');
    Route::get('/dashboard-campaign', 'HomeController@dashboardCampaign')->name('dashboardCampaign');

    // Holiday Routes
    Route::get('/holiday/getUserHolidays', 'HolidayController@getUserHolidays');
    Route::post('/holiday/{user_id}/updateUserHoliday', 'HolidayController@updateUserHoliday');
    Route::post('/holiday/user', 'HolidayController@userStore')->name('holiday.user.store');
    Route::resource('/holiday', 'HolidayController')->middleware('permissionStaff');

    // Client Routes
    /*Route::get('/d-d', function () { return view('_v2.pages.staff.report.debit-doctor', ['page_title' => 'D D']); })->name('debit_doctor');
    Route::get('/d', function () { return view('_v2.pages.staff.report.debit', ['page_title' => 'D']); })->name('debit');
    Route::get('/r', function () { return view('_v2.pages.staff.report.report', ['page_title' => 'R']); })->name('report');*/
    Route::post('/client/updatePaymentStatus/{id}', 'ClientController@updatePaymentStatus');
    Route::post('/client/payNow/{id}', 'ClientController@payNow');
    Route::post('/client/updateACampaign/{id}', 'ClientController@updateACampaign');
    Route::get('/client/getIntent/{id}', 'ClientController@getIntent');
    Route::get('/client/getClients', 'ClientController@getClients');
    Route::get('/client/getData', 'ClientController@getInvoiceData');
    Route::post('client/saveInvoice/{id}', 'ClientController@saveInvoice');
    Route::get('/client/{id}/view-landing-page', 'ClientController@viewLandingPage')->name('client.viewLandingPage');
    Route::get('/client/{id}/view-landing-page/{landing_page_id}/details', 'ClientController@detailsLandingPage')->name('client.detailsLandingPage');
    Route::post('/client/{id}/save-lead', 'ClientController@saveLead')->name('client.saveLead');
    Route::get('/client/show-lead/{id}', 'ClientController@showLead')->name('client.showLead');
    Route::post('/client/update-lead/{id}', 'ClientController@updateLead')->name('client.updateLead');
    Route::get('/client/{id}/getLogs', 'ClientController@getLogs');
    Route::get('/client/{id}/logs', 'ClientController@logs')->name('client.logs');
    Route::get('/client/{id}/billing', 'ClientController@billing')->name('client.billing');
    Route::get('/client/{id}/billing/getBilling', 'ClientController@getBilling');
    Route::get('/client/billing/pdf/{id}', 'ClientController@exportPdf');
    Route::get('/client/{id}/overview/viewCustomerLeads', 'ClientController@viewCustomerLeads')->name('client.viewCustomerLeads');
    Route::get('/client/{id}/overview', 'ClientController@overview')->name('client.overview');
    Route::get('/client/{id}/edit_client', 'ClientController@showClient')->name('client.edit_client');
    Route::get('/client/{id}/notification', 'ClientController@showNotification')->name('client.notification');
    Route::post('/client/{id}/notification/store', 'ClientController@storeNotification')->name('client.notification.store');
    Route::post('/client/{id}/notification/delete', 'ClientController@storeNotification')->name('client.notification.delete');
    Route::post('/client/update/{id}', 'ClientController@updateClient');
    Route::get('/client/details/{id}', 'ClientController@details')->name('client.details');
    Route::get('/client/client-campaign/{id}', 'ClientController@staffClientCampaign')->name('staffClientCampaign');
    Route::post('/client/save-note/{id}', 'ClientController@saveNote')->name('saveNote');
    Route::delete('/client/{user_id}/delete-note/{id}', 'ClientController@deleteNote')->name('deleteNote');
    Route::post('/client/save-landing-page-note/{id}', 'ClientController@saveLandingPageNote')->name('saveLandingPageNote');
    Route::delete('/client/{lp_id}/delete-lp-note/{id}', 'ClientController@deleteLandingPageNote')->name('deleteLandingPageNote');
    Route::resource('/client', 'ClientController')->middleware('permissionStaff');;

    // My Staff Routes
    Route::post('/my-staff/saveRoutesPermission/{user_id}', 'MyStaffController@saveRoutesPermission');
    Route::get('/my-staff/getRoutes/{user_id}', 'MyStaffController@getRoutes');
    Route::post('/my-staff/updateMyStaff/{user_id}', 'MyStaffController@updateMyStaff');
    Route::get('/my-staff/getMyStaff', 'MyStaffController@getMyStaff');
    Route::resource('/my-staff', 'MyStaffController')->middleware('permissionStaff');

    // Tasks Routes
    Route::post('/tasks/saveComment/{task_id}', 'TaskController@saveComment');
    Route::post('/tasks/updateStaffMember', 'TaskController@updateStaffMember');
    Route::post('/tasks/clientTaskStore', 'TaskController@clientTaskStore');
    Route::get('/tasks/changeSubTaskStatus/{id}/{status}', 'TaskController@changeSubTaskStatus');
    Route::get('/tasks/changeTaskStatus/{id}/{status}', 'TaskController@changeTaskStatus');
    Route::get('/tasks/getTasks', 'TaskController@getTasks');
    Route::resource('/tasks', 'TaskController')->middleware('permissionStaff');

    // Agreement Routes
    Route::get('/agreement/getAgreement', 'AgreementController@getAgreement');
    Route::post('/agreement/{id}', 'AgreementController@updateAgreementStatus');
    Route::resource('/agreement', 'AgreementController')->middleware('permissionStaff');

    // Campaigns Routes
    Route::get('/campaigns/getCampaign', 'CampaignsController@getCampaign');
    Route::resource('/campaigns', 'CampaignsController');

    // LiveChat Routes
    Route::get('/live_chat', 'LiveChatController@index')->name('live_chat');

    // Appointments List Routes
    Route::get('/appointments_list', function () {
        return view('_v2.pages.appointments.index', ['page_title' => 'Appointments List']);
    })->name('appointments_list');

    // Invoice Routes
    Route::get('/staff-billing', function () {
        return view('_v2.pages.staff.billing.newBill');
    })->name('staff-billing');
    Route::get('/billing/getInvoice', 'BillingController@getInvoice');
    Route::get('/billing/pdf/{id}', 'BillingController@exportPdf');
    Route::resource('/billing', 'BillingController')->middleware('permissionStaff');

    // Unbounce Routes
    Route::get('/unbounce/getAllCampaigns', 'UnbounceController@getAllCampaigns');
    Route::get('/unbounce/getAllCustomers', 'UnbounceController@getAllCustomers');
    Route::get('/unbounce/getUnbounce', 'UnbounceController@getUnbounce');
    Route::resource('/unbounce', 'UnbounceController')->middleware('permissionStaff');

    // Report Routes
    Route::get('/report', 'ReportController@index')->name('report.index')->middleware('permissionStaff');
    Route::get('/report-data', 'ReportController@reportData')->name('reportData');
    Route::get('/billing-sale', 'ReportController@billingTotalSale')->name('billingTotalSale');

    // Leads Routes
    Route::get('/leads/{id}', 'LeadsController@index')->name('leads.index');
    Route::get('/leads/getLead/{lead_id}/{campaign_id}', 'LeadsController@getLead');
    Route::get('/leads/getLeadsByCampaign/{campaign_id}', 'LeadsController@getLeadsByCampaign');
    Route::post('/leads/save-lead/{id}', 'LeadsController@saveLeads');
    //Route::post('/leads/lead-search', 'LeadsController@leadSearch');
    Route::post('/leads/get-comments', 'LeadsController@getComments');
    Route::post('/leads/save-comment/{lead_id}', 'LeadsController@saveComments');
    Route::post('/leads/status/{lead_id}/{campaign_id}', 'LeadsController@status');
   // Route::post('/leads/delete', 'LeadsController@destroy');
});

<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes(['verify' => true]);

Route::group(['middleware' => ['auth', 'customer'], 'as' => '_v2.customer.', 'namespace' => '_v2\Customer'], function () {

    // Lead Routes
    Route::post('/leads/update', 'LeadController@update')->name('lead.update');
    Route::resource('/leads', 'LeadController');

    // Agreement Routes
    Route::get('/agreement/getAgreement', 'AgreementController@getAgreement');
    Route::resource('/agreement', 'AgreementController');

    // Billing Routes
    Route::post('/staff-billing/updatePaymentStatus/{id}', 'BillingController@updatePaymentStatus');
    Route::get('/staff-billing/getIntent/{id}', 'BillingController@getIntent');
    Route::get('/staff-billing/getInvoice', 'BillingController@getInvoice');
    Route::get('/staff-billing/pdf/{id}', 'BillingController@exportPdf');
    Route::resource('/staff-billing', 'BillingController');


    // LiveChat Routes
    Route::get('/live_chat', 'LiveChatController@index')->name('customer.live_chat');


    //  Getting Started
    Route::get('/markAsDone/{user_id}/{getting_started_id}/{action}', "GettingStartedController@markAsDone")->name('getting-started.markAsDone');
    Route::post('/payNow/{id}', 'BillingController@payNow');
    Route::get('/', "GettingStartedController@index")->name('getting-started');

    Route::get('/customer-dashboard-leads-chart', 'CustomerController@customerDashboardLeadsChart')->name('customerDashboardLeadsChart');
    Route::get('/customer-dashboard-data', 'CustomerController@customerDashboard')->name('customerDashboard');
    Route::get('/dashboard', 'CustomerController@index')->name('dashboard');


    //  profile
    Route::resource('my-profile', "ProfileController");


    //  Getting Started History
    Route::resource('getting-started-history', "GettingStartedHistory");

});

<?php

use Illuminate\Support\Facades\Route;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
Route::view('payment', 'auth.payment');
Auth::routes(['verify' => true]);
/*
// v2 all routing
Route::group(['middleware' => ['auth', 'customer'], 'namespace' => 'Customer'], function () {
    Route::get('/', function () {
        return view('_v2.staff.dashboard');
        //return view('dashboard.index', ['page_title' => '']);
    })->name('getting-started');
    Route::get('/dashboard', 'CustomerController@index')->name('dashboard');

    Route::get('/holiday', function () {
        return view('staff.holiday.holiday_index');
    })->name('holiday');

    //Live Chat
    Route::get('/live_chat', 'LiveChatController@index')->name('live_chat');
    //Mudiuddin working
    // leads
    Route::get('/leads', function () {
        return view('_v2.leads.customer_leads', ['page_title' => 'Customer Leads']);
    })->name('leads');

    Route::get('/appointments', 'AppointmentController@index')->name('appointments');

    Route::get('/appointments_list', function () {
        return view('_v2.appointments.index', ['page_title' => 'Appointments List']);
    })->name('appointments_list');


    // notifications
    Route::get('/notifications', function () {
        return view('_v2.notifications.index', ['page_title' => 'All Notifications']);
    })->name('new_notifications');


    //Ekramul working
    Route::resource('my-profile', "ProfileController");
    //tasks
    Route::get('/tasks', function () {
        return view('_v2.staff.task.index', ['page_title' => 'Task']);
    })->name('tasks');

    Route::get('/staff-billing', function () {
        return view('_v2.staff.billing.index');
    })->name('staff-billing');

    // New Billing
    Route::get('/staff-billing-new', function () {
        return view('_v2.staff.billing.newBill');
    })->name('staff-billing-new');

    // Report
    Route::get('/report', function () {

        return view('_v2.staff.report.report');
//        return view('report.index');
    })->name('report');

    Route::get('/clients', function () {
        return view('_v2.customer.clients');
    })->name('clients');

    Route::get('/home-2', function () {
        return view('_v2.customer.home-2');
    })->name('home-2');

    Route::view('debit-report', '_v2.staff.report.debit')->name('debit-report');
    Route::view('debit-doctor', '_v2.staff.report.debit-doctor')->name('debit-doctor');
    Route::view('agreement', '_v2.agreement.index')->name('agreement');
});

//Sabbir Working for staff
Route::group(['middleware' => ['staff'], 'prefix' => 'staff-2'], function () {
    Route::get('/holiday', function () {
        return view('staff.holiday.holiday_index', ['page_title' => 'Agreements']);
    })->name('staff.holiday');

    //Live Chat
    Route::get('/live_chat', 'Customer\LiveChatController@index')->name('staff.live_chat');

    Route::get('/appointments_list', function () {
        return view('_v2.appointments.index', ['page_title' => 'Appointments List']);
    })->name('staff.appointments_list');

    Route::resource('my-profile', "ProfileController");
    //tasks
    Route::get('/tasks', function () {
        return view('_v2.staff.task.index', ['page_title' => 'Task']);
    })->name('staff.tasks');

    Route::get('/staff-billing', function () {
        return view('_v2.staff.billing.index');
    })->name('staff.staff-billing');

    Route::get('/report', function () {
        return view('_v2/staff/report/report');
    })->name('staff.report');

    Route::get('/clients', function () {
        return view('_v2.customer.clients');
    })->name('staff.clients');

    Route::get('/home-2', function () {
        return view('_v2.customer.home-2');
    })->name('staff.home-2');
});*/


//verify email middleware => 'verified'
Route::middleware(['auth'])->group(function () {
    Route::get('/staff/appointment-calender', 'AppointmentController@appointmentCalender')->name('_v2.staff.appointment-calender.index')->middleware('permissionStaff');




    Route::post('/add/appointment', 'AppointmentController@addAppointment')->name('addAppointment');
    Route::resource('appointments', 'AppointmentController');
    //Customer ROutes
    Route::middleware(['customer'])->group(function () {


        //campaing
        Route::get('/add_Campaign', function () {
            return view('_v2.admin.add_Campaign');

        });

        Route::get('/home', function () {
            return view('home.index');
        })->name('home');

        Route::get('/campaigns', function () {
            return view('campaigns.index', ['page_title' => 'Campaigns']);
        })->name('campaigns');


        Route::post('/campaigns/data', 'CampaignController@index')->name('campaigns.data');

        Route::get('/create/campaign', 'CampaignController@create')->name('campaigns.create');

        Route::post('/add/campaign', 'CampaignController@add')->name('campaigns.add');

        Route::get('/campaigns-setup', function () {
            return view('campaigns.setup');
        })->name('campaigns-setup');


        Route::get('/landing-pages', function () {
            return view('landing.index', ['page_title' => 'My Landing Pages']);
        })->name('landing');

        Route::get('/order/landing', 'LandingPageController@index')->name('landing.order');
        Route::post('/order/landing', 'LandingPageController@save')->name('landing.order');
        Route::get('/edit/landing/{id}', 'LandingPageController@editIndex')->name('landing.edit');
        Route::post('/update/landing', 'LandingPageController@edit');
        Route::post('/landing-pages/data', 'LandingPageController@listing')->name('landing.data');


        /*Route::get('/guide', function () {
            return view('guide.index', ['page_title' => 'Guide']);
        })->name('guide');*/

        Route::get('/faq', 'FaqController@faq')->name('faq');
        Route::get('/invoice-detail', function () {
            $stripePub = getenv("STRIPE_KEY");
            return view('invoice.index', ['stripe_pub' => $stripePub]);
        })->name('invoice.index');
        Route::get('other-project', 'OtherProjectController@index')->name('other_project');

        Route::get('/billing', function () {
            return view('billing.index', ['page_title' => 'Billing']);
        })->name('billing');

        Route::get('/billing/data', 'BillingController@getUserInvoices')->name('billing.data');

        Route::get('/billing/invoice-detail/{id}', 'BillingController@invoiceDetail');
        Route::get('/billing/download/{id}', 'BillingController@donloadInvoice')->name('download-invoice');
        Route::get('/billing/user/setup-intent', 'BillingController@getSetupIntent');
        Route::put('/billing/user/subscription', 'BillingController@updateSubscription');
        Route::post('/billing/user/payments', 'BillingController@postPaymentMethods');
        Route::get('/billing/user/payment-methods', 'BillingController@getPaymentMethods');
        Route::post('/billing/user/remove-payment', 'BillingController@removePaymentMethod');
        Route::post('/billing/user/charge/payment', 'BillingController@chargeCard');
        Route::post('/billing/user/update/company', 'BillingController@updateCompany')->name('billing.company.update');

        Route::get('/notifications', 'NotificationController@index')->name('notifications');
        Route::get('/delete-alert/{id}', 'NotificationController@deleteNofificationAlert')->name('deleteNofificationAlert');
        Route::post('/add/sms/alert', 'NotificationController@addSmsAlert')->name('notification.add.sms.alert');


        Route::get('/agreements', function () {
            return view('agreement.index', ['page_title' => 'Agreements']);
        })->name('agreements');

        Route::get('/agreements/create', function () {
            return view('agreement.create', ['page_title' => 'Create Agreements']);
        })->name('agreements.create');

        Route::post('/agreements/add', 'AgreementController@add')->name('agreements.add');

        Route::post('/agreements/data', 'AgreementController@index')->name('agreements.data');

        // Route::get('/support', function() {
        //     return view('support.index');
        // })->name('support');
        Route::get('/support', 'FaqController@index')->name('support');
        Route::get('/guide', 'GuideController@index')->name('guides');

        Route::get('/profile', function () {
            return view('user.profile', ['page_title' => 'Profile']);
        })->name('profile');

        Route::get('/reset/profile/image', 'UserController@resetImage')->name('profile.img.reset');

        // Route::get('/leads/{campain_id}', 'LeadsController@index');

        // Support Tickets
        Route::get('/support-ticket', 'TicketController@index')->name('tickets');

        Route::post('/profile/update', 'UserController@updateProfile')->name('update.profile');
        Route::post('/update/password', 'UserController@updatePassword')->name('update.password');
        Route::post('/profile/company', 'UserController@updateCompany')->name('update.company');
        Route::get('/request-history', 'UserController@requestsHistory')->name('requests');
    });

    // STAFF ROUTES
    Route::group(['middleware' => ['staff'], 'prefix' => 'staff'], function () {

        Route::get('/dashboard', 'Staff\OverviewController@index')->name('staff.dashboard');


        Route::get('/workload', 'Staff\WorkloadController@index')->name('staff.workload');
        Route::get('/order', function () {
            return view('staff.orders.index');
        })->name('staff.order');

        Route::get('/leaddetails', function () {
            return view('staff.newbusinesshub.leaddetails');
        })->name('staff.newbusinesshub.leaddetails');

        //calender routes
        Route::get('/calender', 'Staff\CalenderConrtroller@index')->name('staff.Calender');

        Route::get('/agreements', function () {
            $sessionUser = Auth::user();
            return view('staff.agreement.index', ['page_title' => 'Agreements', 'user' => json_encode($sessionUser)]);
        })->name('staff.agreements');


        //   Route::get('/deals', function () {
        //     return view('staff.newbusinesshub.deals');
        // })->name('staff.newbusinesshub.deals');

        Route::get('/deals/{id?}', 'Staff\DealsController@deals')->name('staff.newbusinesshub.deals');
        Route::get('/deal-detail/{id}', 'Staff\DealsController@dealDetail')
            ->name('staff.newbusinesshub.deal-detail');
        Route::get('/leads', 'Staff\DealsController@leads')->name('staff.newbusinesshub.leads');

        Route::get('/campaign-leads/{campain_id}', 'Staff\LeadsController@index')->name('staff.campaign.leads');
        Route::get('/campaign-detail/{campain_id}', 'Staff\CampaignsController@detail')->name('staff.campaign.detail');
        Route::post('/campaign/comment', 'Staff\CampaignsController@postComment')->name('staff.campaign.comment');

        Route::get('/support-ticket', 'Staff\TicketController@index')->name('staff.supportticket');

        /* Route::get('/unbounce/{id?}', 'Staff\UnbounceController@index')->name('staff.unbounce');
        Route::get('/unbounce/cron', 'Staff\UnbounceController@cron')->name('unbounceCron');
        Route::post('/unbounce/add', 'Staff\UnbounceController@add')->name('unbounceAdd');
        Route::post('/unbounce/delete', 'Staff\UnbounceController@delete')->name('unbounceDelete'); */
        Route::get('/tasknew', function () {
            return view('staff.tasks.new');
        })->name('staff.tasks.new');

        Route::group(['prefix' => 'tasks'], function () {

            Route::get('/', function () {
                return view('staff.tasks.index');
            })->name('staff.tasks');

            Route::post('/data', 'TaskController@index')->name('staff.tasks.data');

            Route::get('/create', 'TaskController@create')->name('staff.tasks.create');

            Route::post('/store', 'TaskController@store')->name('staff.tasks.store');

            Route::get('/get/staff/members', 'TaskController@getStallMembers')->name('staff.tasks.members');
            Route::get('/get/campaigns', 'TaskController@getAllCampaigns')->name('staff.tasks.campaigns');
            Route::get('/get/landing/pages', 'TaskController@getAllLandingPages')->name('staff.tasks.landing');
            Route::get('/get/other/projects', 'TaskController@getOtherProjects')->name('staff.tasks.project');

            Route::post('/post/comment', 'TaskController@postComment')->name('staff.tasks.post.comment');

            Route::post('/status/update', 'TaskController@updateStatus')->name('staff.tasks.update.status');

            Route::post('/checklist/status/update', 'TaskController@updateChecklistStatus')->name('staff.tasks.update.checklist.status');
            Route::post('/remove/attachment', 'TaskController@removeAttachment')->name('staff.tasks.remove.attachment');
        });

        Route::group(['prefix' => 'clients'], function () {
            Route::get('/', function () {
                return view('staff.clientmanager.index');
            })->name('staff.clients');

            Route::post('/add/client', 'ClientController@store')->name('staff.client.store');
            Route::post('/get/clients', 'ClientController@index')->name('staff.clients.get');
            Route::post('/remove/client', 'ClientController@removeClient')->name('staff.client.remove');
            Route::post('/export/client', 'ClientController@exportClient')->name('staff.client.export');
        });

        Route::get('/campaigns', 'Staff\CampaignsController@index')->name('staff.campaigns');

        Route::get('/staffAccount', 'Staff\StaffAccountController@index')->name('staff.staffAccount');
        Route::post('/staffAccount/profile-upload', 'Staff\StaffAccountController@uploadProfile')->name('staff.staffAccount.profile.upload');
        Route::get('/faqs', 'Staff\FaqsController@index')->name('staff.faqs');


        Route::get('/guide', 'Staff\GuideController@guide')->name('staff.guide');
        Route::get('/holiday', 'Staff\HolidayController@index')->name('staff.holiday');

        Route::get('/target', 'Staff\TargetController@index')->name('staff.target');
        Route::get('/landing-pages', 'Staff\LandingController@listing')->name('staff.landing');

        Route::get('/landing-page-details/{id}', 'Staff\LandingController@show')->name('staff.landing-page-details');
        Route::post('/landing-page/comment', 'Staff\LandingController@postComment')->name('staff.landing.comment');

        Route::get('/campaigns-detail', function () {
            return view('staff.campaign.detail');
        })->name('staff.campaignDetail');

        Route::get('/staff/logout', 'Staff\StaffController@logout')->name('staff.logout');

        Route::post('/landing-pages/create', 'Staff\LandingController@create')->name('staff.landing.create');
        Route::post('/landing-pages/update', 'Staff\LandingController@update')->name('staff.landing.update');
        Route::post('/landing-pages/data', 'Staff\LandingController@index')->name('staff.landing.data');
        Route::get('/landing-pages/get/staff/members', 'Staff\LandingController@allStaffMembers')->name('staff.landing.members');
        Route::get('/landing-pages/get/customers', 'Staff\LandingController@allCustomers')->name('staff.landing.customers');

        Route::post('/invoice/send-email', 'Staff\InvoiceController@sendInvoice');
        Route::get('/invoice', function () {
            return view('staff.invoice.index');
        })->name('staff.invoice');
        Route::get('/order', function () {
            return view('staff.orders.index');
        })->name('staff.order');

        Route::get('/sale', function () {
            return view('staff.sales.index');
        })->name('staff.sale');

        Route::post('/sale/leaderboard', 'Staff\DashboardController@leaderBoard')->name('staff.leaderboard');


        Route::get('/retention', function () {
            return view('staff.retention.index');
        })->name('staff.retention');

        Route::post('/retention/leaderboard', 'Staff\DashboardController@retentionLeaderBoard')->name('staff.retention.leaderboard');


        Route::get('/campaigns', 'Staff\CampaignsController@index')->name('staff.campaigns');
        Route::get('/campaigns/add', 'Staff\CampaignsController@add')->name('staff.campaigns.add');
        Route::get('/campaigns/create', 'Staff\CampaignsController@create')->name('staff.campaigns.create');


        Route::get('/landing-pages', function () {
            return view('staff.landing.index');
        })->name('staff.landing');
        Route::post('/landing-pages/create', 'Staff\LandingController@create')->name('staff.landing.create');
        Route::post('/landing-pages/data', 'Staff\LandingController@index')->name('staff.landing.data');
        Route::post('/landing-pages/assign/me', 'Staff\LandingController@assignToMe')->name('staff.landing.assign.me');
        Route::post('/landing-pages/assign/other', 'Staff\LandingController@assignToOther')->name('staff.landing.assign.other');
        Route::get('/landing-pages/get/staff/members', 'Staff\LandingController@allStaffMembers')->name('staff.landing.members');
        Route::get('/other-projects/data', 'Staff\OtherProjectController@index')->name('staff.other_project.listing');
        Route::get('/projects/detail/{id}', 'Staff\OtherProjectController@detail')->name('staff.other_project.detail');

        Route::get('/invoice', 'Staff\InvoiceController@index')->name('staff.invoice');
        Route::get('/invoice/download/{id?}', 'Staff\InvoiceController@donloadInvoice')->name('staff-download-invoice');
    });
});

Route::get('/zoho-code/code', 'ZohoController@createCode')->name('zoho-code-genrate');
Route::get('/zoho-code', 'ZohoController@createToken')->name('zoho-auth-token');
//Route::get('/zoho-code/refresh', 'ZohoController@refreshToken')->name('zoho-refresh-token');
Route::get('/zoho-document/send', 'ZohoController@sendDocument')->name('zoho-send-document');
Route::get('/zoho-document/status', 'ZohoController@getDocumentStatus')->name('zoho-document-status');

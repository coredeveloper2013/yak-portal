<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//USER MODULE
Route::post('/user/register', 'Api\AuthController@register');
Route::post('/user/login', 'Api\AuthController@login');

Route::group(['middleware' => ['auth:api','acceptheader']], function () {

    Route::group(['middleware' => ['customer']], function() {
        //LANDING
        Route::post('/landing-pages/data', 'Api\LandingPageController@listing')->name('landing.data');
        Route::post('/landing-pages/create', 'Api\LandingPageController@create')->name('landing.order');

        //LEADS MODULE
        Route::post('/leads/get-leads', 'Api\LeadsController@getLeads');
        Route::post('/leads/save-lead', 'Api\LeadsController@saveLeads');
        Route::post('/leads/lead-search', 'Api\LeadsController@leadSearch');
        Route::post('/leads/get-comments', 'Api\LeadsController@getComments');
        Route::post('/leads/save-comment', 'Api\LeadsController@saveComments');
        Route::post('/leads/status', 'Api\LeadsController@status');
        Route::get('/leads/{campain_id}', 'LeadsController@index');

        //CAMPAIGNS
        Route::post('/campaigns/data', 'Api\CampaignController@index')->name('campaigns.data');
        Route::post('/campaigns/create/', 'Api\CampaignController@create')->name('campaigns.create');

        Route::get('/billing/data', 'Api\BillingController@getUserInvoices')->name('billing.data');
        Route::get('/billing/invoice-detail/{id}', 'BillingController@invoiceDetail');
        Route::post('/agreements/data', 'AgreementController@index')->name('agreements.data');

        //Other Projects
        Route::post('/other-projects/data', 'Api\OtherProjectController@listing');

        //Support Ticket
        Route::post('/support-ticket', 'Api\TicketController@listing');
        Route::post('/support-ticket/comments', 'Api\TicketController@getComments');
        Route::post('/support-ticket/add/comment', 'Api\TicketController@addComment');
        Route::post('/support-ticket/add/ticket', 'Api\TicketController@create');

        //Customer APIS

        Route::get('/campaigns/data', 'CampaignController@index');

        Route::get('/notifications', 'NotificationController@index');
        Route::post('/notifications', 'NotificationController@updateNotification');
        Route::delete('/notifications/{notify}', 'NotificationController@removeNofificationAlert');
        Route::get('/guides', 'GuideController@index');
        Route::get('/faqs', 'FaqController@faq');
        //User profile
        Route::get('/profile', 'UserController@getProfile');
        Route::patch('/change-password', 'UserController@changePassword');
        Route::post('/change-company', 'UserController@updateCompany');
        Route::post('/profile', 'UserController@updateProfile');
        Route::patch('/reset-image', 'UserController@resetImage');


    });

    Route::group(['middleware' => ['staff'], 'prefix' => 'staff'], function() {
        //COMPANIES MODULE
        Route::get('/get-all-companies', 'Staff\Api\CompanyController@getAllCompanies');
        //CAMPAIGNS MODULE
        Route::post('/campaigns/get-campaigns', 'Staff\Api\CampaignsController@getCampaigns');
        Route::post('/campaigns/add-campaign', 'Staff\Api\CampaignsController@add');
        Route::post('/campaigns/assign', 'Staff\Api\CampaignsController@assign');
        Route::post('/campaigns/status', 'Staff\Api\CampaignsController@status');
        Route::post('/campaigns/edit', 'Staff\Api\CampaignsController@recordById');
        Route::get('campaigns/detail/{campain_id}', 'Staff\CampaignsController@detail');
        Route::get('/campaigns/leads/{campain_id}', 'Staff\LeadsController@index');
        Route::post('/campaign/comment', 'Staff\CampaignsController@postComment');
        Route::post('/campaigns/delete', 'Staff\Api\CampaignsController@destroy');
        Route::post('/campaigns/comment/delete', 'Staff\Api\CampaignsController@destroyComment');

        //LEADS MODULE
        Route::post('/leads/get-leads', 'Staff\Api\LeadsController@getLeads');
        Route::post('/leads/save-lead', 'Staff\Api\LeadsController@saveLeads');
        Route::post('/leads/lead-search', 'Staff\Api\LeadsController@leadSearch');
        Route::post('/leads/get-comments', 'Staff\Api\LeadsController@getComments');
        Route::post('/leads/save-comment', 'Staff\Api\LeadsController@saveComments');
        Route::post('/leads/status', 'Staff\Api\LeadsController@status');
        Route::post('/leads/delete', 'Staff\Api\LeadsController@destroy');

        // Agreements
        Route::post('/agreements/data', 'Staff\Api\AgreementController@index');
        Route::post('/agreements/resend', 'Staff\Api\AgreementController@resend');
        Route::post('/agreements/signed', 'Staff\Api\AgreementController@markAsSigned');

        Route::post('/staffAccount/add', 'Staff\Api\StaffAccountController@add');
        Route::post('/staffAccount/get', 'Staff\Api\StaffAccountController@get');
        Route::post('/staffAccount/del', 'Staff\Api\StaffAccountController@delete');
        Route::post('/staffAccount/update', 'Staff\Api\StaffAccountController@update');
        Route::post('/staffAccount/update_rights', 'Staff\Api\StaffAccountController@update_rights');
        Route::get('/user/get_rights', 'Staff\Api\StaffAccountController@get_rights');

        Route::post('/faqs/add', 'Staff\Api\FaqsController@add');
        Route::post('/faqs/get', 'Staff\Api\FaqsController@get');
        Route::post('/faqs/del', 'Staff\Api\FaqsController@delete');
        Route::post('/faqs/update', 'Staff\Api\FaqsController@update');

        Route::post('/guide/add', 'Staff\Api\GuideController@add');
        Route::post('/guide/get', 'Staff\Api\GuideController@get');
        Route::post('/guide/del', 'Staff\Api\GuideController@delete');
        Route::post('/guide/update', 'Staff\Api\GuideController@update');

        Route::post('/holiday/add', 'Staff\Api\HolidayController@add');
        Route::post('/holiday/leave', 'Staff\Api\HolidayController@leave');

        Route::post('/holiday/get', 'Staff\Api\HolidayController@get');
        Route::post('/holiday/del', 'Staff\Api\HolidayController@delete');
        Route::post('/holiday/update', 'Staff\Api\HolidayController@update');

         Route::post('/target/add', 'Staff\Api\TargetsController@add');
         Route::post('/target/get', 'Staff\Api\TargetsController@get');
         Route::post('/target/del', 'Staff\Api\TargetsController@delete');
         Route::post('/target/update', 'Staff\Api\TargetsController@update');

        Route::get('/sale/leaderboard', 'Staff\DashboardController@leaderBoard')->name('staff.leaderboard');

        //Other Projects
        Route::post('/other-projects/create', 'Staff\Api\OtherProjectController@create');
        Route::post('/other-projects/listing', 'Staff\Api\OtherProjectController@listing');
        Route::get('/other-projects/members', 'Staff\Api\OtherProjectController@allStaffMembers');
        Route::get('/other-projects/customers', 'Staff\Api\OtherProjectController@allCustomers');
        Route::post('/other-projects/chng/status', 'Staff\Api\OtherProjectController@changeStatus');
        Route::post('/other-projects/destroy', 'Staff\Api\OtherProjectController@destroyProject');
        Route::post('/other-projects/edit', 'Staff\Api\OtherProjectController@recordById');
        Route::get('/other-projects/detail/{id}', 'Staff\OtherProjectController@detail');
        Route::post('/other-projects/add/comment', 'Staff\Api\OtherProjectController@addComment');
        Route::post('/other-projects/comment/delete', 'Staff\Api\OtherProjectController@destroyComment');

        //Support Ticket
        Route::post('/support-ticket', 'Staff\Api\TicketController@listing');
        Route::post('/support-ticket/comments', 'Staff\Api\TicketController@getComments');
        Route::post('/support-ticket/assign', 'Staff\Api\TicketController@AssignTicket');
        Route::post('/support-ticket/add/comment', 'Staff\Api\TicketController@addComment');
        Route::post('/support-ticket/delete/ticket', 'Staff\Api\TicketController@destroyTicket');
        Route::post('/support-ticket/complete/ticket', 'Staff\Api\TicketController@completeTicket');


        Route::post('/leads/add', 'Staff\Api\DealsController@add');
        Route::post('/leads/update-lead', 'Staff\Api\DealsController@updateLead');
        Route::post('/leads/get', 'Staff\Api\DealsController@leads');
        Route::post('/leads/del', 'Staff\Api\DealsController@delete');
        Route::post('/leads/update', 'Staff\Api\DealsController@update');
        Route::post('/deals/status', 'Staff\Api\DealsController@status');
        Route::get('/deal-detail/{id}', 'Staff\DealsController@dealDetail');
        Route::post('/leads/addComments', 'Staff\Api\DealsController@addComments');
        Route::post('/leads/comments', 'Staff\Api\DealsController@comments');
        Route::post('/leads/active', 'Staff\Api\DealsController@active');
        Route::post('/leads/assign', 'Staff\Api\DealsController@assign');


        //Calender Module
        Route::post('/calender/save-category', 'Staff\Api\CalenderConrtroller@storeCategory');
        Route::post('/calender/save-event', 'Staff\Api\CalenderConrtroller@storeEvent');
        Route::post('/calender/get-events', 'Staff\Api\CalenderConrtroller@getEvent');

        //Invoices
        Route::post('/invoice', 'Staff\Api\InvoiceController@getAllInvoices');
        Route::get('/invoice/setup-intent', 'Staff\Api\InvoiceController@getSetupIntent');
        Route::post('/invoice/charge', 'Staff\Api\InvoiceController@chargeCard');


        // Landing Pages
        Route::post('/landing-pages/create', 'Staff\Api\LandingController@create');
        Route::post('/landing-pages/update', 'Staff\Api\LandingController@update');
        Route::post('/landing-pages/data', 'Staff\Api\LandingController@index');
        Route::get('/landing-pages/get/staff/members', 'Staff\Api\LandingController@allStaffMembers');
        Route::get('/landing-pages/get/customers', 'Staff\Api\LandingController@allCustomers');
        Route::get('/landing-page-details/{id}', 'Staff\Api\LandingController@show');
        Route::post('/landing-page/note', 'Staff\Api\LandingController@postComment');
        Route::post('/landing-page/comment/delete', 'Staff\Api\LandingController@destroyComment');

        //client manager
        Route::get('/clients', 'ClientController@getAllClients');
        Route::get('/client/{user}', 'ClientController@getClient');
        Route::post('/clients', 'ClientController@updateClient');
        Route::delete('/client/{user_id}', 'ClientController@deleteClient');

        //tasks api

        Route::post('/tasks', 'TaskController@storeTask');
        Route::patch('/task/{task}', 'TaskController@updateTaskStatus');
        Route::patch('/task/subtask/{subtask}', 'TaskController@updateSubTaskStatus');
        Route::patch('/task/removeattachment/{task}', 'TaskController@updateTaskAttachment');
        Route::post('/task/comment', 'TaskController@taskComment');
        Route::get('/tasks', 'TaskController@getTasks');
        Route::get('/task/{task}', 'TaskController@getTask');

        //dashboard
        Route::get('/dashboard/overview', 'Staff\OverviewController@index');
        Route::get('dashboard/sale', 'Staff\DashboardController@leaderBoard');
        Route::get('dashboard/retention', 'Staff\DashboardController@retentionLeaderBoard');
        Route::get('dashboard/workload','Staff\WorkloadController@index');

        //unbounce
        Route::get('/unbounce', 'Staff\UnbounceController@index');
        Route::post('/unbounce/add', 'Staff\UnbounceController@add');

        //invoice
        Route::post('/invoice/resend', 'Staff\InvoiceController@sendInvoice');
        Route::get('/invoice/download/{id?}', 'Staff\InvoiceController@donloadInvoice');

    });
});
// Route::get('/users', 'Admin\Api\UserController@index');
// Route::post('/saveUser', 'Admin\Api\UserController@save');
// Route::post('/updateUser', 'Admin\Api\UserController@update');
// Route::post('/delete', 'Admin\Api\UserController@destroy');
// Route::post('/getuser', 'Admin\Api\UserController@getUser');
Route::post('/guide/get', 'GuideController@get')->name('guide');

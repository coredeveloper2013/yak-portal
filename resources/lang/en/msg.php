<?php

return [

    

    'user' => 'Users',
    'ppc' => 'PPC',
    'users' => 'Users',
    'addUser' => 'Add User',
    'dashboard' => 'Dashboard',
    'editUser' => 'Edit User',
    'name' => 'Name',
    'email' => 'Email',
    'password' => 'Password',
    'username' => 'UserName',
    'update' => 'Update',
    'cancel' => 'Cancel',
    'save' => 'Save',
    'createUser' => 'Create User',
    'footer' => 'Footer',
    'logout' => 'Logout',
    'updated_successfully' => 'Updated Successfully',
    'created_successfully' => 'Created Successfully',
    'deleted_successfully' => 'Deleted Successfully',
    'type' => 'Type',
    'action' => 'Action'

];

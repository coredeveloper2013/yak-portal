@extends('layouts.app')

@section('css')
    <style>
        .invalid-feedback {
            display: block !important;
        }
    </style>

@endsection

@section('content')


    <!-- Page Content-->
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="wlcm-panel">
                        <div class="welcome-text text-center">
                            <p class="text-muted">Whether you are in individual or a Fortune 500 <br> enterprise, there
                                is a plan there is a plan that's right for you

                            </p>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xl-8 col-lg-10 col-md-10 mx-auto mx-flex">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" action="{{route('campaigns.add')}}">
                                @csrf
                                <div class="row">

                                    <div class="col-xl-12">
                                        <div class="form-group">
                                            <label>Campaign Name<span class="req">*</span></label>
                                            <input type="text" name="name" class="form-control" value="{{old('name')}}">
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-xl-6">
                                        <div class="form-group">
                                            <label>Start Date</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="start_date"
                                                       id="start_date" value="{{old('start_date')}}"
                                                       placeholder="Select Date">
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i
                                                            class="dripicons-calendar"></i></span>
                                                </div>
                                            </div>
                                            @error('start_date')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>


                                    <div class="col-xl-6">
                                        <div class="form-group">
                                            <label>End Date</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="end_date" id="end_date"
                                                       value="{{old('end_date')}}" placeholder="Select Date">
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i
                                                            class="dripicons-calendar"></i></span>
                                                </div>
                                            </div>
                                            @error('end_date')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-xl-6">
                                        <div class="form-group">
                                            <label>Landing page </label>
                                            <select class="form-control" name="landing_id">
                                                <option disabled="" selected="">Select landing page</option>
                                                @foreach($landing_pages as $page)
                                                    <option
                                                        value="{{$page->id}}" {{ (old('landing_id') == $page->id ? "selected" : "") }} >{{$page->name}}</option>
                                                @endforeach
                                            </select>
                                            @error('landing_id')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-xl-6">
                                        <div class="form-group">
                                            <label>Campaign Budget<span class="req">*</span></label>
                                            <input type="number" name="amount" class="form-control"
                                                   value="{{old('amount')}}">
                                            @error('amount')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                        </div>
                                    </div>

{{--                                    <div class="col-xl-12">--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label>Do you want to auto repeat this campaign?</label>--}}

{{--                                            <div class="radio radio-primary">--}}
{{--                                                <input type="radio" name="repeat" id="radio13" value="yes"--}}
{{--                                                    {{old('repeat') === 'yes' ? 'checked' : ''}}>--}}
{{--                                                <label for="radio13">--}}
{{--                                                    Yes--}}
{{--                                                </label>--}}
{{--                                            </div>--}}

{{--                                            <div class="radio radio-primary">--}}
{{--                                                <input type="radio" name="repeat" id="radio14" value="no"--}}
{{--                                                    {{old('repeat') === 'no' ? 'checked' : ''}}>--}}
{{--                                                <label for="radio14">--}}
{{--                                                    No--}}
{{--                                                </label>--}}
{{--                                            </div>--}}
{{--                                            @error('repeat')--}}
{{--                                            <span class="invalid-feedback" role="alert">--}}
{{--                                                    <strong>{{$message}}</strong>--}}
{{--                                            </span>--}}
{{--                                            @enderror--}}
{{--                                        </div>--}}
{{--                                    </div>--}}


                                    <div class="col-xl-6">
                                        <div class="form-group">
                                            <label>Status</label>
                                            <select class="form-control" name="status">
                                                <option disabled="" selected="">Select status</option>
                                                <option
                                                    value="completed" {{ (old('status') === "completed" ? "selected" : "") }}>
                                                    Completed
                                                </option>
                                                <option
                                                    value="pending" {{ (old('status') === "pending" ? "selected" : "") }}>
                                                    Pending
                                                </option>
                                            </select>
                                            @error('status')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-xl-6">
                                        <div class="form-group">
                                            <label for="campaign_type">Campaign Type</label>
                                            <select class="form form-control form-control-sm" name="type">
                                                <option
                                                    value="one_off" {{ (old('type') === "one_off" ? "selected" : "") }}>
                                                    One Off
                                                </option>
                                                <option
                                                    value="monthly" {{ (old('type') === "monthly" ? "selected" : "") }}>
                                                    Monthly
                                                </option>
                                            </select>
                                            @error('campaign_id')
                                            <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-xl-12">
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-primary float-right"
                                                   value="Create a campaign">
                                        </div>
                                    </div>

                                </div>

                            </form>

                        </div>
                    </div>
                </div>
            </div>

            <!--end col-->
        </div>
        <!-- container -->
    @include('layouts.footer')
    <!--end footer-->
    </div>



    <!-- end page content -->
@endsection



@push('js')
    <script>
        $(function () {
            $('input[name="start_date"]').daterangepicker({
                opens: 'left',
                singleDatePicker: true,
                showDropdowns: true,
                minDate: moment(),
            }, function (start, end, label) {
                $("#start_date").val(start.format('MM/DD/YYYY'));
            });
            $('input[name="end_date"]').daterangepicker({
                opens: 'left',
                singleDatePicker: true,
                showDropdowns: true,
                minDate: moment(),
            }, function (start, end, label) {
                $("#end_date").val(start.format('MM/DD/YYYY'));
            });
        });
    </script>
@endpush

@extends('layouts.app')

@section('content')


<!-- Page Content-->
<div class="page-content">
    <div class="container-fluid">
       <div class="row">
          <div class="col-lg-12">
             <div class="wlcm-panel">
                <div class="welcome-text text-center">
                   <h3 class="title2 py-3 m-0">
                   Campaign Setup</h6>
                   <p class="text-muted">Whether you are in individual or a Fortune 500 <br> enterprise, there is a plan there is a plan that's right for you

                   </p>
                </div>

             </div>
          </div>
       </div>

        <div class="row">
                   <div class="col-xl-8 col-lg-10 col-md-10 mx-auto mx-flex">
                      <div class="card">
                         <div class="card-body">
                            <div class="wizard">
                               <div class="wizard-inner">
                                  <div class="connecting-line"></div>
                                  <ul class="nav nav-tabs" role="tablist">
                                     <li role="presentation" class="active">
                                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" aria-expanded="true"><span class="round-tab">1 </span> <span class="side-place-text">Basic information</span></a>
                                     </li>
                                     <li role="presentation" class="disabled">
                                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" aria-expanded="false"><span class="round-tab">2</span> <span class="side-place-text">Profile</span></a>
                                     </li>
                                     <li role="presentation" class="disabled">
                                        <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab"><span class="round-tab">3</span><span class="side-place-text">Notifications</span></a>
                                     </li>
                                     <li role="presentation" class="disabled">
                                        <a href="#step4" data-toggle="tab" aria-controls="step4" role="tab"><span class="round-tab">4</span> <span class="side-place-text">Done</span></a>
                                     </li>
                                  </ul>
                               </div>
                               <form role="form" action="index.html" class="login-box">
                                  <div class="tab-content" id="main_form">
                                     <div class="tab-pane active" role="tabpanel" id="step1">
                                        <div class="row">
                                           <div class="col-xl-12 mx-auto">
                                              <!-- <div class="form-group">
                                                 <label>Fill in your basic information to let us know your preferences</label>
                                                 <input class="form-control" type="text" name="name" placeholder="Enter Code">
                                              </div> -->

                                              <div class="form-group">
                                    <label>Fill in your basic information to let us know your preferences</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="far fa-envelope"></i></span>
                                        </div>
                                        <input type="text" id="example-input1-group1" name="example-input1-group1" class="form-control" placeholder="Enter code">
                                    </div>
                                </div>

                                           </div>
                                        </div>
                                        <ul class="list-inline pull-right">
                                           <li><button type="button" class="btn btn-grey btn-sp float-right next-step">Next</button></li>
                                        </ul>
                                     </div>
                                     <div class="tab-pane" role="tabpanel" id="step2">
                                        <div class="row">
                                        </div>
                                        <ul class="list-inline pull-right">
                                        <li><button type="button" class="btn btn-primary btn-sp float-right next-step">Next</button></li>
                                           <li><button type="button" class="btn btn-grey float-right prev-step">Back</button></li>

                                        </ul>
                                     </div>
                                     <div class="tab-pane" role="tabpanel" id="step3">
                                        <div class="row">
                                           <div class="col-md-6">
                                              <div class="form-group">
                                                 <label>Account Name *</label>
                                                 <input class="form-control" type="text" name="name" placeholder="">
                                              </div>
                                           </div>
                                           <div class="col-md-6">
                                              <div class="form-group">
                                                 <label>Demo</label>
                                                 <input class="form-control" type="text" name="name" placeholder="">
                                              </div>
                                           </div>
                                        </div>
                                        <ul class="list-inline pull-right">
                                          <li><button type="button" class="btn btn-primary btn-sp float-right next-step">Next</button></li>
                                           <li><button type="button" class="btn btn-grey float-right prev-step">Back</button></li>

                                        </ul>
                                     </div>
                                     <div class="tab-pane" role="tabpanel" id="step4">
                                     </div>
                                     <div class="clearfix"></div>
                                  </div>
                               </form>
                            </div>
                         </div>
                      </div>
                   </div>
                </div>
       <div class="feature-panel">
          <div class="row py-5">
             <div class="col-xl-8 col-lg-10 col-md-10 mx-auto">
                <div class="row">
                   <div class="col-xl-12">
                      <div class="welcome-text text-center">
                         <h3 class="py-3 m-0">
                         Frequently Asked Questions</h6>
                         <p class="text-muted">Here are the most frequently asked questions you may check before started.
                         </p>
                      </div>
                   </div>
                   <div class="col-xl-6 col-lg-6 col-md-6">
                      <div class="text-block-01">
                         <h4>What happens if rm not satisfied?</h4>
                         <p>If you are still in your free trial period, you can cancel your account at anytime with a single click of a button. If you already paid for your first month, we also offer 30-day money-back guarantee with no questions asked. After first month, you can still cancel your account at any time but we will calculate the amount that corresponds to days you have been using our app for that month and refund only the remaining amount. </p>
                      </div>
                   </div>
                   <div class="col-xl-6 col-lg-6 col-md-6">
                      <div class="text-block-01">
                         <h4>What is the duration of the free trial? </h4>
                         <p>Our app is free to try for 14 days, if you want more, you can provide payment details which will extend your trial to 30 days providing you an extra 16 more days to try our app. </p>
                      </div>
                   </div>
                   <div class="col-xl-6 col-lg-6 col-md-6">
                      <div class="text-block-01">
                         <h4>What is the storage is for? </h4>
                         <p>Since we provide an extremely detailed reporting and analytics tool, they require quite a bit storage space. For average use, you don't have to worry about running out of space since the Personal package limits the projects you can have. For some reason if you run out of space, contact us and we will see what can be done about it and make sure you are not generating unnecessary reports and/or analytics data.</p>
                      </div>
                   </div>
                   <div class="col-xl-6 col-lg-6 col-md-6">
                      <div class="text-block-01">
                         <h4>Are there discounts for non-profits or educational use?</h4>
                         <p>Yes, our Personal and Premium packages are free for non-profits and educational use. E-mail your details to us after starting your Free Trial and we will upgrade your account if you qualify. </p>
                      </div>
                   </div>
                </div>
             </div>
          </div>
       </div>
       <!--end col-->
    </div>
<!-- container -->
@include('layouts.footer')
<!--end footer-->
</div>



<!-- end page content -->
@endsection



@push('js')

      <script type="text/javascript">
         // ------------step-wizard-------------
         $(document).ready(function () {
         $('.nav-tabs > li a[title]').tooltip();

         //Wizard
         $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

         var target = $(e.target);

         if (target.parent().hasClass('disabled')) {
            return false;
         }
         });

         $(".next-step").click(function (e) {

         var active = $('.wizard .nav-tabs li.active');
         active.next().removeClass('disabled');
         nextTab(active);

         });
         $(".prev-step").click(function (e) {

         var active = $('.wizard .nav-tabs li.active');
         prevTab(active);

         });
         });

         function nextTab(elem) {
         $(elem).next().find('a[data-toggle="tab"]').click();
         }
         function prevTab(elem) {
         $(elem).prev().find('a[data-toggle="tab"]').click();
         }


         $('.nav-tabs').on('click', 'li', function() {
         $('.nav-tabs li.active').removeClass('active');
         $(this).addClass('active');
         });



      </script>

@endpush

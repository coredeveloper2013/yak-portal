@extends('layouts.app')
@section('css')
<style>
   .invalid-feedback {
   display: block !important;
   }
</style>
@endsection
@section('content')
<!-- Page Content-->
<div class="page-content" id="campaigns" xmlns="http://www.w3.org/1999/html">
   <div class="container-fluid">
      <div class="row">
         <div class="col-sm-12  custom-space01">
            <div class="page-title-box">
               <div class="float-right">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="javascript:void(0);">Main</a></li>
                     <li class="breadcrumb-item active">Campaigns</li>
                  </ol>
               </div>
               <h4 class="page-title text-left"></h4>
            </div>
            <!--end page-title-box-->
         </div>
         <!--end col-->
      </div>

      <div class="custom-space01 clearfix">
        <div class="form-group filter-f dropdown-arrow float-left mr-3">
            <input type="text" name="daterange" id="daterange" placeholder="Filter by date"
                   class="form-control"/>
        </div>
        <div class="form-group filter-c float-left">
            <select class="form form-control form-control-sm" v-model="filter.status" @change="getUserCampaigns(false)">
               <option value="" disabled>Filter by Status</option>
               <option value="live">Live</option>
               <option value="completed">Completed</option>
               <option value="pending">Pending</option>
            </select>
         </div>
         <div class="form-group reset-c float-left m-0">
            <button type="button" class="btn" @click="resetFilter"><i class="fas fa-undo"></i></button>
         </div>
      </div>

      <div class="row custom-space01 mt-3 loading-icon-resp" v-if="campaigns.length">
         <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12" v-for="row in campaigns">
              <div class="card">

                  <div class="card-body">
                    <div class="card-title border-bottom pb-2 clearfix">
                      <h4 class="m-0 pr-4 float-left">@{{row.name}}</h4>
                      <span class="text-success mt-0 float-right font-small"> #@{{row.id}} </span>

                    </div>
                      <!-- <div class="top-txt">
                          <span class="text-success mt-0 mb-3"> #@{{row.id}} </span>
                          <a :href="getLeadsUrl(row.id)" class="float-right ml-2 toptxt-toltip" title="View Leads">
                             <i class="fa fa-list"></i>
                          </a>
                      </div> -->
                      <!-- <h5 class="txt-black-label">@{{row.name}}</h5> -->
                      <div class="row text-group mb-1">
                          <div class="col-lg-4 pr-0">
                              <span class="txt-black-label text-limit">Landing Page</span>
                          </div>
                          <div class="col-lg-8">
                              <span class="txt-grey text-limit">@{{row.landing_page_name}}</span>
                          </div>
                      </div>
                      <div class="row text-group mb-1">
                          <div class="col-lg-4 pr-0">
                              <span class="txt-black-label text-limit">Order Date</span>
                          </div>
                          <div class="col-lg-8">
                              <span class="txt-grey text-limit">@{{row.created_human}}</span>
                          </div>
                      </div>
                      <div class="row text-group mb-1">
                          <div class="col-lg-4 pr-0">
                              <span class="txt-black-label">Budget</span>
                          </div>
                          <div class="col-lg-8">
                              <span class="txt-grey">&#163; @{{numberWithComma(row.amount)}}</span>
                          </div>
                      </div>
                      <div class="row text-group">
                          <div class="col-lg-4 pr-0">
                              <span class="txt-black-label">Type</span>
                          </div>
                          <div class="col-lg-8">
                              <span class="txt-grey">@{{row.type_text}}</span>
                          </div>
                      </div>
                      <div class="footer-card-panel f-card-panel">
                          <span class="mt-2 badge badge-boxed  badge-soft-success tm-2" v-if="row.customer_status == 'Live'">
                              Live
                          </span>
                          <span class="mt-2 badge badge-boxed  badge-soft-danger tm-2"  v-else-if="row.customer_status == 'Completed'">
                              Completed
                          </span>
                          <span class="mt-2 badge badge-boxed  badge-soft-warning tm-2"  v-else>
                              @{{row.customer_status}}
                          </span>
                           <a :href="getLeadsUrl(row.id)" class="btn grey-btn float-right lead-btn-resp">
                             <i class="fas fa-chart-bar"></i> View Leads
                          </a>

                      </div>
                  </div>
              </div>
         </div>
      </div>

      <div class="custom-space01 text-center" v-else>
          <!-- <i class="far fa-frown"></i> -->
          <p style="color: grey" class="text-c12 white-box">Sorry, no Campaigns found.</p>
      </div>

      <div class="custom-space01">
          <div class="col-xl-6 col-lg-6 col-md-6 float-left">
              <p class="mb-0 p-1 small mt-1 float-left">Showing @{{ campaignsMeta.from }} to @{{
                  campaignsMeta.to }} of @{{ campaignsMeta.total }} records | Per Page: @{{
                  campaignsMeta.per_page }} | Current Page: @{{ campaignsMeta.current_page }}</p>
          </div>
          <div class="col-xl-6 col-lg-6 col-md-6 float-right t012 pager-resp">
              <b-pagination class="float-right  t012" size="sm" :total-rows="campaignsMeta.total"
                            v-model="campaignsMeta.current_page" :per-page="campaignsMeta.per_page"
                            first-text="First" prev-text="Pervious" next-text="Next" last-text="Last"
                            ellipsis-text="More" variant="danger">
              </b-pagination>
          </div>
      </div>{{-- End Row --}}
   </div>
   <!-- container -->
   @include('layouts.footer')
   <!--end footer-->
</div>
<!-- end page content -->
@endsection
@push('js')
<script>
   var main_url = '{!! url('/') !!}';
   var App_url = '{!! url()->current() !!}';
</script>
{{--  Inject vue js files  --}}
<script src="{{ asset('js/app-vue.js') }}"></script>
<script src="{{ asset('js/campaigns/vue.js') }}"></script>
@endpush

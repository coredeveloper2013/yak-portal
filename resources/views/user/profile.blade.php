@extends('layouts.app')


@section('css')
    <style>
        .invalid-feedback {
            display: block !important;
        }
    </style>

    <link href="//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css" rel="stylesheet" />
@endsection

@section('content')
    <!-- Page Content-->
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="float-right">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Main</a></li>
                                <li class="breadcrumb-item active">Edit Profiles</li>
                            </ol>
                        </div>
                        <h4 class="page-title resp-account">Account Settings</h4>


                    </div>
                    <!--end page-title-box-->
                </div>
                <!--end col-->
            </div>
            <!-- Collapse -->
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist"
                                 aria-orientation="vertical">
                                <a @if (!\Session::has('company') && !\Session::has('password'))
                                   class="nav-link waves-effect waves-light active"
                                   @else
                                   class="nav-link waves-effect waves-light" @endif
                                    id="v-pills-home-tab"
                                   data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home"
                                   aria-selected="true"><i class="fas fa-cog"></i> General</a>
                                <a @if (\Session::has('password'))
                                   class="nav-link waves-effect waves-light active"
                                   @else
                                   class="nav-link waves-effect waves-light" @endif
                                    id="v-pills-profile-tab" data-toggle="pill"
                                   href="#v-pills-profile" role="tab" aria-controls="v-pills-profile"
                                   aria-selected="false"><i class="fas fa-unlock-alt"></i> Change Password</a>
                                <a  @if (\Session::has('company'))
                                    class="nav-link waves-effect waves-light active"
                                    @else
                                    class="nav-link waves-effect waves-light" @endif
                                   id="v-pills-settings-tab"
                                   data-toggle="pill" href="#v-pills-settings" role="tab"
                                   aria-controls="v-pills-settings" aria-selected="false"><i
                                        class="fas fa-building"></i> Company Information</a>

                                {{--<a class="nav-link waves-effect waves-light" id="requests" data-toggle="pill" href="#user-requests" role="tab" aria-controls="v-pills-settings" aria-selected="false"><i class="fas fa-user"></i> Requests</a>--}}
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="card">
                                <div class="card-body">
                                    <div class="tab-content mo-mt-2" id="v-pills-tabContent">
                                        <div @if (!\Session::has('company') && !\Session::has('password')) class="tab-pane fade active show" @else class="tab-pane fade" @endif id="v-pills-home" role="tabpanel"
                                             aria-labelledby="v-pills-home-tab">
                                            <form method="POST" action="{{route('update.profile')}}"
                                                  enctype="multipart/form-data" autocomplete="off">
                                                @csrf
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-xl-12">
                                                            <div class="user-profile-image float-left">
                                                                <img
                                                                    @if(!is_null(Auth::user()->image)) src="{{asset('/storage/avatars/'.Auth::user()->image)}}"
                                                                    @else src="{{asset('/images/user.png')}}"
                                                                    @endif  id="preview">
                                                            </div>
                                                            <input type="file" name="img" class="file" accept="image/*">
                                                            <div class="float-left upload-buttons">
                                                                <button type="button" class="browse btn btn-upload">
                                                                    UPLOAD NEW PHOTO
                                                                </button>
                                                                <button type="button" class="btn btn-grey"
                                                                        onclick="resetform()">Reset
                                                                </button>
                                                                <p>Allowed JPG, GIF or PNG Max Size of 800kb</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Username<span
                                                            class="req">*</span></label>
                                                    <input type="text" class="form-control" id="username"
                                                           name="username" placeholder="Enter username"
                                                           aria-describedby="emailHelp"
                                                           @if(Auth::user()->username == null) value=""
                                                           @else value="{{Auth::user()->username}}" @endif>
                                                    @error('username')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Name<span
                                                            class="req">*</span></label>
                                                    <input type="text" class="form-control" id="name" name="name"
                                                           placeholder="Enter name" aria-describedby="emailHelp"
                                                           @if(Auth::user()->name == null) value=""
                                                           @else value="{{Auth::user()->name}}" @endif>
                                                    @error('name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">E-Mail<span
                                                            class="req">*</span></label>
                                                    <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter email" @if(Auth::user()->email == null) value="" @else value="{{Auth::user()->email}}" @endif>
                                                    @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror

                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Company<span
                                                            class="req">*</span></label>
                                                    <input type="text" class="form-control" id="companyName"
                                                           name="companyName" aria-describedby="emailHelp"
                                                           placeholder="Enter company name"
                                                           @if(Auth::user()->company->name == null) value=""
                                                           @else value="{{Auth::user()->company->name}}" @endif>
                                                </div>
                                                @if (\Session::has('general') && \Session::get('general') != '')
                                                    <div class="alert alert-success">
                                                        <small>{!! \Session::get('general') !!}</small>
                                                    </div>
                                                @endif
                                                <div class="form-group float-right mb-0">
                                                    <button type="submit" class="btn btn-primary save-c-margin">Save Changes</button>
                                                    <button type="button" class="btn btn-grey p-btn"
                                                            onclick="redirectToDashboard()">Cancel
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                        <div @if (\Session::has('password')) class="tab-pane fade active show" @else class="tab-pane fade" @endif id="v-pills-profile" role="tabpanel"
                                             aria-labelledby="v-pills-profile-tab">
                                            <form action="{{route('update.password')}}" method="POST"
                                                  autocomplete="off">
                                                @csrf
                                                <h4>Change Password</h4>
                                                <div class="form-group">
                                                    <label for="old_password">Current Password<span
                                                            class="req">*</span></label>
                                                    <input type="password" class="form-control" id="old_password"
                                                           name="old_password" aria-describedby="emailHelp"
                                                           placeholder="Enter your current password">
                                                    @error('old_password')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label for="new_password">New Password<span
                                                            class="req">*</span></label>
                                                    <input type="password" class="form-control" id="new_password"
                                                           name="password" aria-describedby="emailHelp"
                                                           placeholder="Enter your new password">
                                                    @error('password')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                                <div class="form-group ">
                                                    <label for="confirm_new_pw">Confirm New Password<span
                                                            class="req">*</span></label>
                                                    <input type="password" class="form-control" id="confirm_new_pw"
                                                           name="password_confirmation" aria-describedby="emailHelp"
                                                           placeholder="Confirm your new password">
                                                </div>
                                                @if (\Session::has('password') && \Session::get('password') != '')
                                                    <div class="alert alert-success">
                                                        <small>{!! \Session::get('password') !!}</small>
                                                    </div>
                                                @endif
                                                <div class="form-group float-right mb-0">
                                                    <button type="submit" class="btn btn-primary">Save Changes</button>
                                                    <button type="button" class="btn btn-grey p-btn"
                                                            onclick="redirectToDashboard()">Cancel
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                        <div @if (\Session::has('company')) class="tab-pane fade active show" @else class="tab-pane fade" @endif id="v-pills-settings" role="tabpanel"
                                             aria-labelledby="v-pills-settings-tab">
                                            <form method="POST" action="{{route('update.company')}}" autocomplete="off">
                                                @csrf
                                                <div class="form-group">
                                                    <label for="companyName">Business Name<span class="req">*</span></label>
                                                    <input type="text" name="companyName" id="companyName"
                                                           class="form form-control"
                                                           value="{{Auth::user()->company->name}}">
                                                    @error('companyName')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label for="address">Business Address</label>
                                                    <textarea class="form form-control" name="address"
                                                           id="address" rows="5">{{Auth::user()->company->address}}</textarea>
                                                    @error('address')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                                @if (\Session::has('company') && \Session::get('company') != '')
                                                    <div class="alert alert-success">
                                                        <small>{!! \Session::get('company') !!}</small>
                                                    </div>
                                                @endif
                                                <div class="form-group float-right mb-0">
                                                    <button type="submit" class="btn btn-primary">Save Changes</button>
                                                    <button type="button" class="btn btn-grey p-btn">Cancel</button>
                                                </div>
                                            </form>
                                        </div>

                                        {{--<div @if (\Session::has('company')) class="tab-pane fade active show" @else class="tab-pane fade" @endif id="user-requests" role="tabpanel"
                                             aria-labelledby="v-pills-settings-tab">
                                            <div id="requests-content">

                                                <ul id="listRequest"></ul>

                                                <div class="table-responsive">
                                                    <table class="table text-center td-block tableData">
                                                        <thead>
                                                        <tr class="th-head">
                                                            <th># </th>
                                                            <th>IP Adress</th>
                                                            <th>Name</th>
                                                            <th>Date</th>
                                                            <th>Time</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="request-body">

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div> --}}



                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end col-->
            </div>
            <!--end row-->
        </div>
        <!-- container -->
    @include('layouts.footer')
    <!--end footer-->
    </div>
    <!-- end page content -->
@endsection

@push('js')

<script src="//cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js" type="text/javascript"></script>













    <script>

        $(function() {
            getHistory();
        });

        $(document).on("click", ".browse", function () {

            var file = $(this).parents().find(".file");
            file.trigger("click");
        });
        $('input[type="file"]').change(function (e) {
            var fileName = e.target.files[0].name;
            $("#file").val(fileName);

            var reader = new FileReader();
            reader.onload = function (e) {
                // get loaded data and render thumbnail.
                document.getElementById("preview").src = e.target.result;
            };
            // read the image file as a data URL.
            reader.readAsDataURL(this.files[0]);
        });

        function resetform() {
            axios.get('/reset/profile/image')
                .then(response => {
                    if (response.data.status == 'success') {
                        window.location.reload();
                    }
                }).catch(error => {
                console.log(error);
            });
        }

        function getHistory() {
            return '';
            axios.get('request-history')
                .then(response => {
                listResponse(response);
            }).catch(error => {
                    console.log(error);
            });

        }
        function listResponse(response){
            console.log('Resp: ', response);
            var  html="";

            if(response.data){
                var list = response.data.request_list;
                var counter = 0;
                $.each( list, function( key, value ) {
                    counter++;
                    html +="<tr>";
                    html +="<td>"+ counter +"</td>";
                    html +="<td>"+ value.ip_address+"</td>";
                    html +="<td>"+ value.page+"</td>";
                    html +="<td>"+ value.date+"</td>";
                    html +="<td>"+ value.time+"</td>";
                    html+="</tr>";
                });
            }


//            $("#listRequest").html(html);
            $("#request-body").html(html);

            $('.tableData').DataTable({

            } );


//            $("#listRequest").JPaging({
//                pageSize: 15
//            });
        }


        function redirectToDashboard() {
            window.location = '/home';
        }
    </script>
@endpush

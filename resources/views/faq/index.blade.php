@extends('layouts.app')

@section('content')

<!-- Page Content-->
<div class="page-content">
            <div class="container-fluid">
               

               <div class="row py-5 faq-row mx-auto" style="padding-bottom: 0 !important;">
               <div class="col-xl-12">
                  <h4>Most Asked</h4>
               </div>
                  <div class="col-xl-12">
                     <div class="accordion" id="accordionExample">
                        @if(! $faq->isEmpty())
                            @foreach($faq as $fq)
                              @if($fq->isFrequent && $fq->status)
                                 <div class="faq-a">
                                    <a class="collapsed fq-1"  data-toggle="collapse" data-target="#collapseOne{{$fq->id}}" aria-expanded="true" aria-controls="collapseOne">
                                    {{$fq->question}}
                                    <span href="" class="float-right text-info"><i class="fas fa-angle-down"></i></span>
                                    </a>
                                    <div id="collapseOne{{$fq->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                       <div class="faq-body">
                                         {{$fq->answer}}
                                       </div>
                                    </div>
                                 </div>
                                 @endif
                              @endforeach
                        @endif
                        
                     </div>
                  </div>
               </div>

    <div class="row py-5 faq-row mx-auto">
               <div class="col-xl-12">
                  <h4>General Inquiries</h4>
                  <div class="accordion" id="accordionExample">
                        @if(! $faq->isEmpty())
                            @foreach($faq as $fq)
                              @if(!$fq->isFrequent && $fq->status)
                                 <div class="faq-a">
                                    <a class="collapsed fq-1"  data-toggle="collapse" data-target="#collapseOne{{$fq->id}}" aria-expanded="true" aria-controls="collapseOne">
                                    {{$fq->question}}
                                    <span href="" class="float-right text-info"><i class="fas fa-angle-down"></i></span>
                                    </a>
                                    <div id="collapseOne{{$fq->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                       <div class="faq-body">
                                         {{$fq->answer}}
                                       </div>
                                    </div>
                                 </div>
                                 @endif
                              @endforeach
                        @endif
                        
                     </div>
               </div>
                 
               </div>

            </div>
         <!-- container -->
        @include('layouts.footer')
         <!--end footer-->
      </div>
<!-- end page content -->
@endsection

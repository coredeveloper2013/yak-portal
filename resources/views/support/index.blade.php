@extends('layouts.app')

@section('css')
    <style>

    </style>

@endsection


@section('content')
   <div class="page-content">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="wlcm-panel">
                        <div class="welcome-text text-center m-0">
                          <!--  <h2>Contact Us</h2> -->
                           <h3 class="title1 py-3 mb-2 title1-resp">
                              Please choose from the following options: </h3>
                        </div>
                     </div>
                  </div>
                  </div>
                  <div class="row d-flex justify-content-center">
                     <div class="col-sm-4 col-md-3 col-lg-2">
                        <div class="contact-sub-box contact-resp">
                        <a href="javascript:;" data-toggle="modal" data-target="#myModal"><img src="{{asset('images/s1.jpg')}}">
                           <p>Phone Us</p>
                        </a>
                        </div>
                     </div>
{{--                     <div class="col-sm-4 col-md-3 col-lg-2">--}}
{{--                        <div class="contact-sub-box contact-resp">--}}
{{--                        <a href="{{ route('tickets') }}">--}}
{{--                           <img src="{{asset('images/s2.jpg')}}">--}}
{{--                           <p>Submit a Ticket</p>--}}
{{--                        </a>--}}
{{--                        </div>--}}
{{--                     </div> --}}
                     <div class="col-sm-4 col-md-3 col-lg-2">
                        <div class="contact-sub-box contact-resp">
                        <a href="{{ route('faq') }}"><img src="{{asset('images/s3.jpg')}}">
                           <p>FAQ's</p>
                        </a>
                        </div>
                     </div>
                  </div>
                  <div class="row d-flex justify-content-center">
                     <div class="col-md-7 view-guid-box">
                        <h2 class="mb-0">Do you need help closing your leads?</h2>
                        <p class="mt-0">View our guides to get our top tips on how to close your leads, special tatics and more!</p>
                        <button  onclick="window.location = '{{ route('guides') }}'">View Guides</button>
                     </div>
                  </div>

                   <!-- The Modal -->
                  <div class="modal fade pr-0" id="myModal">
                    <div class="modal-dialog modal-lg-1 model-styling-m">
                      <div class="modal-content model-main-box">

                        <!-- Modal Header -->
                        <div class="model-header-style">
                        <button type="button" class="close btn-cross-style" data-dismiss="modal">&times;</button>
                          <h2 class="modal-title text-center">Phone Us</h2>

                        </div>

                        <!-- Modal body -->
                        <!-- <div class="model-body-style">
                          <h4>If your calling is regarding:</h4>
                          <ul>
                             <li>- Account Review (going over leads, billing, etc)</li>
                             <li>- Complaints</li>
                             <li>- Technical</li>
                             <li>- Increasing Budgets</li>
                             <li>- or anything that may require us to prepare before the call</li>
                          </ul>
                          <a href="">Please book a call with your account manager by clicking the link below:</a>
                           <div class="row">
                              <button>Book a Call</button>
                           </div>
                        </div>   -->
                        <div class="row justify-content-center text-center call-number py-3 mx-0">
                           <div class="col-md-12 call-number-1">We are available via phone from 9am to 5pm, Monday to friday.</div>
                           <div class="col-md-12 call-number-2">020 3026 8937</div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <!-- <div class="box-support mx-auto">
                     <div class="box01">
                        <a  href="/support-ticket" >
                           <img src="{{asset('images/s1.jpg')}}">
                           <div class="icon-sub-box">ICON</div>
                           <div class="head03">Phone Us</div>
                           <p>Contact us for more detailed support</p>
                        </a>
                     </div>
                     <div class="box01">
                          <a  href="/faq" >
                        <img src="{{asset('images/s2.jpg')}}">
                        <div class="head03">FAQs</div>
                        <p>Frequently asked question & answers</p>
                     </a>
                     </div>
                     <div class="box01">
                       <a  href="/guide" >
                        <img  src="{{asset('images/s3.jpg')}}">
                        <div class="head03">Guides</div>
                        <p>Article and resources to guide you</p>
                     </a>
                     </div>
                  </div> -->

               <div class="feature-panel">
                  <div class="row py-5 faq-row mx-auto">
                     <div class="col-xl-12">
                        <div class="row">
                           <div class="col-xl-12">
                              <div class="welcome-text text-center">
                                 <h3 class="py-3 m-0">
                                 Most Frequently Asked Questions</h6>
                                 <p class="text-muted">Here are the most frequently asked questions you may check before started.
                                 </p>
                              </div>
                           </div>
                           <div class="col-xl-12">
                              <div class="accordion" id="accordionExample">
                              @if(! $faq->isEmpty())
                               @foreach($faq as $fq)
                                 @if($fq->isFrequent && $fq->status)
                                 <div class="faq-a">
                                    <a class="collapsed fq-1"  data-toggle="collapse" data-target="#collapseOne{{$fq->id}}" aria-expanded="true" aria-controls="collapseOne">
                                    {{$fq->question}}
                                    <span href="" class="float-right text-info"><i class="fas fa-angle-down"></i></span>
                                    </a>
                                    <div id="collapseOne{{$fq->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                       <div class="faq-body">
                                         {{$fq->answer}}
                                       </div>
                                    </div>
                                 </div>
                                  @endif
                                 @endforeach
                                @else
                                 <div class="faq-a">
                                    <a class="collapsed fq-1"  data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseOne">
                                    What's the benefits of the Premium Membership?
                                    <span href="" class="float-right text-info"><i class="fas fa-angle-down"></i></span>
                                    </a>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                       <div class="faq-body">
                                          Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                       </div>
                                    </div>
                                 </div>
                                 <div class="faq-a">
                                    <a class="collapsed fq-1"  data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseOne">
                                    How much time i will need to learn this app?
                                    <span href="" class="float-right text-info"><i class="fas fa-angle-down"></i></span>
                                    </a>
                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                       <div class="faq-body">
                                          Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                       </div>
                                    </div>
                                 </div>
                                 <div class="faq-a">
                                    <a class="collapsed fq-1"  data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseOne">
                                    Are there any free tutorials avalaible?
                                    <span href="" class="float-right text-info"><i class="fas fa-angle-down"></i></span>
                                    </a>
                                    <div id="collapseFour" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                       <div class="faq-body">
                                          Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                       </div>
                                    </div>
                                 </div>
                                 <div class="faq-a">
                                    <a class="collapsed fq-1"  data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseOne">
                                    Is there a month-to-month payment option?
                                    <span href="" class="float-right text-info"><i class="fas fa-angle-down"></i></span>
                                    </a>
                                    <div id="collapseFive" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                       <div class="faq-body">
                                          Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                       </div>
                                    </div>
                                 </div>
                                 @endif
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!--end col-->
            </div>
            <!-- container -->
            @include('layouts.footer')

            <!--end footer-->
         </div>

@endsection
@push('js')
    <script>

    </script>
@endpush


<table width="100%" border="0" cellspacing="0"  cellpadding="0" style="min-width: 320px; font-family:arial;">
    <tr>
        <td align="center">
            <table border="0" cellspacing="0" cellpadding="0" bgcolor="#f1f5f9"  class="table_width_100" width="100%" style="max-width: 500px; min-width: 300px; border: dashed 2px #5368d2;">
                <tr>
                    <td style="text-align:center;">
                        <br>
                        <img src="{{asset('storage/images/logo-dark.png')}}" width="202">
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center; color: #5368d2;">
                        <br>
                        <h2>Welcome to PPC</h2>
                    </td>
                </tr>
                <!--header -->
                <tr>
                    <td align="center">
                        <!-- padding -->
                        <table width="90%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <p>Please confirm your email address by clicking the link below.</p>
                                    <p>We may need to send you critical information about our service and it is important that we have an accurate email address.</p>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <font face="Arial, Helvetica, sans-serif" size="4" color="#57697e" style="font-size: 15px;">
                                        <table width="90%" border="0" cellspacing="10" cellpadding="0" style="text-align:center; ">
                                            <tr>
                                                <td align="center">
                                                    <div style="line-height: 24px; width:100%">
                                                        <a href="{{$url}}" target="_blank" style="background: #5368d2; width: 100%; padding: 8px; border-radius: 5px; color: #fff; text-decoration: none;">
                                                            Confirm email address
                                                        </a>
                                                    </div>
                                                    <div style="height: 60px; line-height: 60px; font-size: 10px;"></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </font>
                                </td>
                            </tr>
                            <!--footer -->
                            <tr>
                                <td class="iage_footer" align="center" bgcolor="#ffffff">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="center" style="padding:20px;flaot:left;width:100%; text-align:center;">
                                                <font face="Arial, Helvetica, sans-serif" size="3" color="#96a5b5" style="font-size: 13px;">
                                    <span style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #7b7b7b;">
                                    2020 © PPC. ALL Rights Reserved.
                                    </span></font>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <!--footer END-->
                            <tr>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

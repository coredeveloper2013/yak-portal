<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="./assets/css/bootstrap.min.css">
    <!-- css -->
    <link rel="stylesheet" href="./assets/css/style.css">


    <title>Foam of YAK Portal</title>

    <style>
        table, th, td {
            border: 3px solid black;
            border-collapse: collapse;
            font-weight: 700;
            padding: 5px 0px 5px 10px;
            /* text-align: start; */
        }
        .form-style{
            border: none;
        }
        .Text-padding{
            padding-left: 10px;
        }

        /* responsive */

        @media only screen and (max-width: 425px) {
            .table-resp{
                width: 400px!important;
            }
        }
        @media only screen and (max-width: 375px) {
            .table-resp {
                width: 350px!important;
            }
            .heading-style{
                font-size: 20px!important;
                font-weight: bold;
            }
        }
        @media only screen and (max-width: 320px) {
            .table-resp {
                width: 300px!important;
            }
        }
    </style>

</head>
<body>
<h2 class="heading-style" style="text-align: center; margin-top: 20px;">New Lead in YAK Portal</h2>
<table class="table-resp" style="width: 500px; margin: 45px auto;">

    @foreach($leadsData as $key =>$value)
        <tr>
            <th>{{$value['heading']}}</th>
            <th>
            {{$value['ans']}}
            </th>
        </tr>
    @endforeach
</table>



</body>

</html>

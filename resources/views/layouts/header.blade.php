<!-- Top Bar Start -->
<div class="topbar">
    <!-- LOGO -->
    <!-- <div class="topbar-left">
        <a href="{{url('/')}}" class="logo">
            <span class="logo-retina">
            <img src="{{asset('storage/images/logo-sm.png')}}" alt="logo-retina">
            </span>
            <span>
            <img src="{{asset('storage/images/logo-dark.png')}}" alt="logo-large" class="logo-side">
            </span>
        </a>
    </div> -->
    <!--end logo-->
    <!-- Navbar -->
    <nav class="navbar-custom custompublic-nav">
        <div class="title-of-page">
            <h1 class="m-0">
                @if (!empty($page_title))
                    {{ $page_title }}
                @else
                    <!-- Welcome, {{ request()->user()->name }}! -->
                @endif
            </h1></div>
        <ul class="list-unstyled topbar-nav float-right mb-0">
            @auth

                <li class="dropdown">
                <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                    <img @if(!is_null(Auth::user()->image)) src="{{asset('storage/avatars/'.Auth::user()->image)}}" @else src="{{asset('/images/user.png')}}" @endif alt="profile-user" class="rounded-circle" />
                    <span class="ml-1 nav-user-name"><i class="mdi mdi-chevron-down"></i> </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="{{route('profile')}}"><i class="dripicons-user text-muted mr-2"></i> Profile</a>
                    <!-- <a class="dropdown-item" href="#"><i class="dripicons-wallet text-muted mr-2"></i> My Wallet</a>
                    <a class="dropdown-item" href="#"><i class="dripicons-gear text-muted mr-2"></i> Settings</a>
                    <a class="dropdown-item" href="#"><i class="dripicons-lock text-muted mr-2"></i> Lock screen</a> -->
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        <i class="dripicons-exit text-muted mr-2"></i> {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
            @else
                <li class="sign-links"><a href="{{route('login')}}"><i class="fas fa-sign-in-alt"></i> Login</a></li>
                <li class="sign-links"><a href="{{route('register')}}"> <i class="far fa-user"></i> Register</a></li>
            @endauth
        </ul>
    <!--end topbar-nav-->
    @auth
        <ul class="list-unstyled topbar-nav mb-0">
           <!--  <div class="text-center main-logo"><a href="{{url('/')}}"><img src="{{url('images/logo-dark.png')}}" alt="logo-large" class="logo-side"></a></div> -->
            <!-- <li>
                <button class="button-menu-mobile nav-link waves-effect waves-light" data-toggle="collapse" data-target=".navbar-collapse">
                    <i class="dripicons-menu nav-icon"></i>
                </button>
            </li> -->
            <!-- <li class="hide-phone app-search">
                <form role="search" class="">
                    <input type="text" placeholder="Search..." class="form-control">
                    <a href=""><i class="fas fa-search"></i></a>
                </form>
            </li> -->
        </ul>
    @endauth
</nav>
    <!-- end navbar-->
</div>
<!-- Top Bar End -->

<footer class="footer text-center text-sm-left">
    &copy; {{ now()->year }} Yak-Portal <span class="text-muted d-none d-sm-inline-block float-right"></span>
</footer>
<!--end footer-->

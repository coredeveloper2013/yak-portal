<!-- Left Sidenav -->
<div class="left-sidenav navbar-collapse">
    <div class="main-icon-menu">
        <div class="site-logo-icon">
            <a href="{{url('/')}}">
                <img class="dashboard-logo" src="{{asset('images/app-icon-bull.png')}}" alt="logo-side">
            </a>
        </div>
    <div class="nav-responsive style-4">
        <nav class="nav">
            <a href="{{route('home')}}" data-divid="Home"  @if(Route::currentRouteName() == 'home') class="nav-link active" @else class="nav-link" @endif data-toggle="tooltip-custom" data-placement="top" title="" data-original-title="Home">
                <div><i class="fa fa-home"></i></div>
                <div class="icon-t">Home</div>
            </a>
            <!--end MetricaAnalytic-->
            <a href="{{route('campaigns')}}" data-divid="Compaign" @if(Route::currentRouteName() == 'campaigns') class="nav-link active" @else class="nav-link" @endif  data-toggle="tooltip-custom" data-placement="top" title="" data-original-title="My Compaign">
                <i class="fas fa-bullhorn"></i>
                <div class="icon-t">My Campaigns</div>

            </a>
            <!--end MetricaCrypto-->
            <a href="{{route('landing')}}" data-divid="Landing"  @if(Route::currentRouteName() == 'landing') class="nav-link active" @else class="nav-link" @endif data-toggle="tooltip-custom" data-placement="top" title="" data-original-title="My Landing pages">
                <i class="far fa-file"></i>
                <div class="icon-t">My Landing pages</div>
            </a>
            <a href="{{route('other_project')}}" data-divid="Landing"  @if(Route::currentRouteName() == 'other_project') class="nav-link active" @else class="nav-link" @endif data-toggle="tooltip-custom" data-placement="top" title="" data-original-title="Other Projects">
                <i class="ti-view-grid"></i>
                <div class="icon-t">Projects</div>
            </a>
            <!--end MetricaProject-->
            <a href="{{route('billing')}}" data-divid="Billing" @if(Route::currentRouteName() == 'billing') class="nav-link active" @else class="nav-link" @endif  data-toggle="tooltip-custom" data-placement="top" title="" data-original-title="Billing">
                <i class="fas fa-receipt"></i>
                <div class="icon-t">Billing</div>


            </a>
            <!--end MetricaEcommerce-->
            {{--<a href="{{route('notifications')}}" data-divid="Notification" @if(Route::currentRouteName() == 'notifications') class="nav-link active" @else class="nav-link" @endif data-toggle="tooltip-custom" data-placement="top" title="" data-original-title="Notification">
                <i class="far fa-bell"></i>
                <div class="icon-t">Notifications</div>
            </a>--}}
            <!--end MetricaCRM-->
            <a href="{{route('support')}}" @if(Route::currentRouteName() == 'support') class="nav-link active" @else class="nav-link" @endif class="nav-link" data-divid="Support" data-toggle="tooltip-custom" data-placement="top" title="" data-original-title="Support">
                <i class="fas fa-headset"></i>
                <div class="icon-t">Support</div>

            </a>
            <!--end MetricaOthers-->
            <a href="{{route('agreements')}}" data-divid="Agreements" @if(Route::currentRouteName() == 'agreements') class="nav-link active" @else class="nav-link" @endif  data-toggle="tooltip-custom" data-placement="top" title="" data-original-title="Agreements">
                <i class="fas fa-file-contract"></i>
                <div class="icon-t">Agreements</div>
            </a>
            <!--end MetricaPages-->
            <a href="{{route('profile')}}" @if(Route::currentRouteName() == 'profile') class="nav-link active" @else class="nav-link" @endif class="nav-link" data-divid="profile" data-toggle="tooltip-custom" data-placement="top" title="" data-original-title="My Profile">
                <i class="fas fa-user-tie"></i>
                <div class="icon-t">My Profile</div>
            </a>
            <!--end MetricaAuthentication-->
            <!--end MetricaPages-->
           <!--  <a href="{{-- route('leads') --}}" @if(Route::currentRouteName() == 'leads') class="nav-link active" @else class="nav-link" @endif class="nav-link" data-divid="leads" data-toggle="tooltip-custom" data-placement="top" title="" data-original-title="My Profile">
                <i class="fas fa-user-tie"></i>
                <div class="icon-t">Leads</div>
            </a> -->
            <!--end MetricaAuthentication-->

            <!--end MetricaPages-->
            <!-- <a href="{{route('tickets')}}" @if(Route::currentRouteName() == 'tickets') class="nav-link active" @else class="nav-link" @endif class="nav-link" data-divid="tickets" data-toggle="tooltip-custom" data-placement="top" title="" data-original-title="My Profile">
                <i class="fas fa-user-tie"></i>
                <div class="icon-t">Tickets</div>
            </a> -->
            <!--end MetricaAuthentication-->

            <!--end MetricaPages-->
{{--           <!--  <a href="{{ route('dashboard') }}" @if(Route::currentRouteName() == 'dashboard') class="nav-link active" @else class="nav-link" @endif class="nav-link" data-divid="dashboard" data-toggle="tooltip-custom" data-placement="top" title="" data-original-title="My Profile">--}}
{{--                <i class="fas fa-user-tie"></i>--}}
{{--                <div class="icon-t">Dashboard</div>--}}
{{--            </a> -->--}}
            <!--end MetricaAuthentication-->

            <!--end MetricaPages-->
         <!--    <a href="{{route('support')}}" @if(Route::currentRouteName() == 'support') class="nav-link active" @else class="nav-link" @endif class="nav-link" data-divid="support" data-toggle="tooltip-custom" data-placement="top" title="" data-original-title="My Profile">
                <i class="fas fa-user-tie"></i>
                <div class="icon-t">Support</div>
            </a> -->
            <!--end MetricaAuthentication-->
        </nav>

        </div>
        <!--end nav-->
    </div>
    <!--end main-icon-menu-->

    <!-- end main-menu-inner-->
</div>
<!-- end left-sidenav-->

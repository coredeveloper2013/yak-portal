<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">


<head>

    <meta charset="utf-8"/>
    <title>YAK Portal</title>
    {{--    <meta name="viewport" content="width=device-width, initial-scale=1.0">--}}
    <meta content="A premium admin dashboard template by Mannatthemes" name="description"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content="Mannatthemes" name="author"/>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('/images/logo-sm.png')}}">
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{asset('css/icons.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('css/metisMenu.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css"/>
    @yield('css')
</head>
<body>
<div>
    @if(Route::currentRouteName() == 'login' || Route::currentRouteName() == 'register' ||
        Route::currentRouteName() == 'verification.notice' || Route::currentRouteName() == 'password.request' ||
        Route::currentRouteName() == 'password.reset' || Route::currentRouteName() == 'password.update')
        @yield('content')
    @else
        @include('layouts.header')
        <div class="page-wrapper">
            @include('layouts.navigation')
            @yield('content')
        </div>
    @endif
</div>

<div class="page-loader" id="page-loader">
    <div class="spinner-border text-light" role="status">
        <span class="sr-only">Loading...</span>
    </div>
</div>

<!-- Scripts -->
<script src="{{asset('js/app.js') }}"></script>
<script src="{{asset('js/metisMenu.min.js')}}"></script>
<script src="{{asset('js/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('js/waves.min.js')}}"></script>
<script>
    $('.nav > a').each(function () {
        if ($(this).hasClass('active')) {
            $("#" + $(this).data('divid')).addClass('active');
        }
    });
    $(window).on('load', function(){
        document.getElementById('page-loader').style.display = 'none';
    });
</script>
@stack('js')
</body>
</html>

@extends('layouts.app')

@section('content')

<!-- Page Content-->
<div class="page-content">
            <div class="container-fluid">
               
  <!-- invoice html -->
<div class="row d-flex justify-content-center mt-5">
    <div class="col-md-5 col-sm-6 col-12 in-resp-change">
        <div class="invoice-box">
            <div class="invoice-box-1">

                <h3>Business Details</h3>
            </div>
            <div class="invoice-box-2">
                <a href="javascript:;" data-toggle="modal" data-target="#EditModal" class="float-right text-info"><i class="fas fa-pencil-alt"></i></a>
                <h5>Business Name</h5>
                <p>{{Auth::user()->company->name}}</p>
                <!-- <div class="invoice-dots"></div> -->
                <h5>Business Address</h5>
                <p>{{Auth::user()->company->address}}</p>
                <!-- <div class="invoice-dots"></div>
                <div class="invoice-dots"></div> -->
                
            </div>
            <div class="invoice-box-3 text-center">
                <span class="m-0">
                    <span>&#163;</span>{{$invoice ?? ''->campaign->amount}}
                 </span>
                <h4>Saved Cards</h4>
                <input id="card-holder-name" type="text" class="input-invoice-styling mb-2">
                <div class="row d-flex justify-content-center">
                    <button class="invoice-card">Add Card</button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5 col-sm-6 col-12">
    <div class="invoice-box">
            <div class="invoice-box-1">
                <h3>Plan details</h3>
            </div>
            <div class="my-4 text-center">
                <h4>Amount Due: £ 384</h4>
                <button class="invoice-card">Download Invoice</button>    
            </div>
            <div class="check-sec">
                <div class="invoice-dots"></div>
                <h4 class="text-center">How would you like to pay ?</h4>
                <div class="custom-control custom-checkbox">
                      <input id="checked" class="custom-control-input" type="checkbox" checked />
                      <label for="checked" class="custom-control-label lable-text">Card</label>
                </div>
                <div class="custom-control custom-checkbox">
                      <input id="unchecked" class="custom-control-input " type="checkbox" />
                      <label for="unchecked" class="custom-control-label lable-text">Bank Transfer</label>
                </div>

                <subscription :amount="currentInvoice.amount"
                              :type="currentInvoice.type"
                              :id="currentInvoice.id"
                              :status="currentInvoice.status"></subscription>
            </div>     
        </div>    
    </div>
</div>

                    <!-- invoice html end -->


<div class="modal fade" id="EditModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Invoice Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{route('billing.company.update')}}">
                    @csrf
                    <div class="row" class="needs-validation was-validated">
                        <div class="col-xl-12">
                            <div class="form-group">
                                <label>Company Name</label>
                                <input type="text" name="name" class="form-control" value="{{Auth::user()->company->name}}">
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-xl-12">
                            <div class="form-group">
                                <label>Address</label>
                                <input type="text" name="address" class="form-control" value="{{Auth::user()->company->address}}">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>




         <!-- container -->
        @include('layouts.footer')
         <!--end footer-->
      </div>
<!-- end page content -->
@endsection

@push('js')
    <script>
        var currentInvoice = '{!! $invoice !!}';
        console.log(currentInvoice);
    </script>
    <script src="{{ asset('js/app-vue.js') }}"></script>
@endpush

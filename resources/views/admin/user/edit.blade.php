@extends('admin.layouts.app')
@section('content')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.10.12/dist/sweetalert2.all.min.js" integrity="sha256-HyVNOA4KTbmvCLxBoFNrj0FLZtj/RCWyg4zfUjIry0k=" crossorigin="anonymous"></script>
<script
  src="https://code.jquery.com/jquery-3.5.0.slim.js"integrity="sha256-sCexhaKpAfuqulKjtSY7V9H7QT0TCN90H+Y5NlmqOUE="
  crossorigin="anonymous"></script>
<style>
    .invalid-feedback{
        display: block !important;
    }
</style>
<script >
$(document).ready(function() {
  var url = $(location).attr("href");
  var id = url.substring(url.lastIndexOf('/') + 1);
console.log(url);
function getRecord(){
  if(id){
     $.ajax({
            type: "get",
            url: "{{ url('api/users') }}", 
            data: { 
            id:id
            }
        }).done(function( response ) {

            if(response.status==='success'){
             
                $('#email').val(response.data.user.email);
                $('#username').val(response.data.user.username);
                $('#name').val(response.data.user.name);
                $('#type').val(response.data.user.type);

            }else{
               Swal.fire({
                  icon: 'error',
                  title: response.msg,
                  //footer: '<a href>Why do I have this issue?</a>'
                });
            }
            $('.spinner-cover').hide();
        });
   }else{

   }
}
getRecord();
  $("#submit").click(function(event){
   var btn = $('#submit');
   btn.attr('disabled', true);

   var name = $('#name').val();
   var email = $('#email').val();
   var password = $('#password').val();
   var username = $('#username').val();
   var type = $('#type').val();
   
 

   if( name!= '' && email != ''  && username !=''){
    event.preventDefault();
     $.ajax({
            type: "post",
            url: "{{ url('api/updateUser') }}", // This is what I have updated
            data: { 
            name:name,
            email:email,
            password:password,
            id:id,
            username:username,
            type:type
            }
        }).done(function( response ) {
            if(response.status=='success'){
                Swal.fire(
                  'Good job!',
                  response.msg,
                  'success'
                );
                $( 'form' ).each(function(){
                    this.reset();
                });
                console.log(response);
            }else{
               Swal.fire({
                  icon: 'error',
                  title: response.msg,
                  //footer: '<a href>Why do I have this issue?</a>'
                });
            }
            
        });
   btn.attr('disabled', false);
   }else{
   btn.attr('disabled', false);

    
   }
  });




});
</script>
<div class="container-fluid">
    @if ($message = Session::get('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ $message }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <div class="row">
        <div class="col-lg-12">
            <div class="wlcm-panel">
                <div class="welcome-text no-sub text-center">
                    <h3 class="title2 py-3 m-0">
                        @lang('msg.editUser')</h6>
                        </p>
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-12 col-lg-10 col-md-10 mx-auto mx-flex">
            <div class="card">
                <div class="card-body">
                    <form method="post"  enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="form-group">
                                    <label>@lang('msg.name')</label>
                                    <input  type="hidden" name="id"  class="form-control">
                                    <input  required type="text" name="name"  value="" class="form-control" id="name">
                                   
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="form-group">
                                    <label>@lang('msg.username')</label>
                                    <input  required type="text" name="username"   class="form-control" id="username">
                                   
                                </div>
                            </div>

                            <div class="col-xl-6">
                                <div class="form-group">
                                    <label>@lang('msg.email')</label>
                                    <input  required type="email" name="email"  value="" class="form-control" id="email">
                                   
                                </div>
                            </div>
                            <input type="hidden" name="" value="" id="id">
                            <div class="col-xl-6">
                                <div class="form-group">
                                    <label>@lang('msg.password')</label>
                                    <input    type="text" name="password"  value="" class="form-control" id="password">
                                    

                                </div>
                            </div>

                            <div class="col-xl-6">
                                <div class="form-group">
                                    <label>Type</label>
                                    <select class="form form-control form-control-sm" name="type" id='type'>
                                        <option value="admin" {{ (old('status') === "admin" ? "selected" : "") }}>Admin</option>
                                        <option value="staff" {{ (old('status') === "staff" ? "selected" : "") }}>Staff</option>
                                        <option value="customer" {{ (old('status') === "customer" ? "selected" : "") }}>Customer</option>
                                    </select>
                                    @error('type')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="col-xl-12">
                                <div class="form-group ft-none">
                                    <input type="button" class="btn btn-grey p-btn float-right"
                                           name="" value="  {{__('msg.cancel')}} "
                                            onclick="history.back();"
                                           >

                                    <input type="submit" class="btn btn-primary float-right" 
                                          id="submit" name="" value="{{__('msg.update')}}"

                                          >


                                </div>

                            </div>

                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>



</div>

@endsection

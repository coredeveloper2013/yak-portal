@extends('admin.layouts.app')
@section('css')

<!--<link rel="stylesheet" href="{{asset('css/daterangepicker.css')}}" />-->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.10.12/dist/sweetalert2.all.min.js" integrity="sha256-HyVNOA4KTbmvCLxBoFNrj0FLZtj/RCWyg4zfUjIry0k=" crossorigin="anonymous"></script>
<script
  src="https://code.jquery.com/jquery-3.5.0.slim.js"integrity="sha256-sCexhaKpAfuqulKjtSY7V9H7QT0TCN90H+Y5NlmqOUE="
  crossorigin="anonymous"></script>
<style>
    .invalid-feedback{
        display: block !important;
    }
</style>
<script >
$(document).ready(function() {
var currentUser={!!Auth::user()->id!!};

      $('tbody').on('click', 'a', function() {
    var id=$(this).data("value");
    if(id!=null){
            $('.spinner-cover').show();
        $.ajax({
            type: "post",
            url: "{{ url('api/delete') }}", // This is what I have updated
            data: { 
            id:id,
           
            }
        }).done(function( response ) {

            if(response.status=='success'){
                Swal.fire(
                  'Good job!',
                  response.msg,
                  'success'
                );

               $('#tr'+id).remove();
               if($('.tableData').length > 0){
                     var tableData = $('.tableData').DataTable();
                 }
                 
                  tableData.clear().draw();
                
                  getUsers();

            }else{
               Swal.fire({
                  icon: 'error',
                  title: response.msg,
                  //footer: '<a href>Why do I have this issue?</a>'
                });
            }
            
        });
            $('.spinner-cover').show();


    }else{
        
    }


});
  
  function getUsers(){
       $.ajax({
            type: "get",
            url: "{{ url('api/users') }}", // This is what I have updated
            data: { }
        }).done(function( response ) {
            console.log(response);
            if (response.data.users) {
                if($('.tableData').length > 0){
                     var tableData = $('.tableData').DataTable();
                 }

// console.log(window.location.replace("https://"));
                    $.each(response.data.users, function(index, row) {
                         let buttons = "";
                        buttons += `<td>
                                        <a href="javascript:;" class="btn grey-btn float-right"
                                        data-value="`+row.id+`">
                                            <i class="fa fa-trash"></i></a>
                                        <a href="{{ url('admin/editUser') }}/`+row.id+`" class="btn grey-btn float-right">
                                            <i class="fa fa-edit"></i></a>
                                    </td>`;
                        if(currentUser!=row.id){
                         tableData.row.add([
                            row.id,
                            row.name,
                            row.username,
                            row.email,
                            row.type,
                            buttons
                        ]).draw(false).node();
                     }
                        //$(tRow).attr('data-enc-id',row.enc_id);
                    });
                }
                tableData.draw();
            $('.spinner-cover').hide();

        });

    }
getUsers();



 
    
});
</script>
@endsection
@section('js')

@endsection
@section('content')
<div class="container-fluid">
    @if ($message = Session::get('success'))

    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ $message }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <div class="row">
        <div class="col-lg-12">
            <div class="wlcm-panel">
                <div class="welcome-text no-sub text-center">
                    <h3 class="title2 py-3 m-0">
                        @lang('msg.user')</h3>
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 mx-auto">
            <div class="card">
                <div class="card-body">
                    <a href="{{ url('admin/addUser') }}" class="float-right btn btn-primary btn-sm space-bt">
                        @lang('msg.createUser')</a>
                    <!--<h4 class="mt-0 header-title">@lang('msg.myProcurement')</h4>-->
                    <div class="table-responsive">
                        <table class="table mb-0 campaign-t myTable tableData" >
                            <thead class="thead-light">
                                <tr>
                                    <th>@lang('msg.id')</th>
                                    <th>@lang('msg.name')</th>
                                    <!--<th>@lang('msg.description')</th>-->
                                    <th>@lang('msg.username')</th>
                                    <th>@lang('msg.email')</th>
                                    <th>@lang('msg.type')</th>

                                    <th>@lang('msg.action')</th>

                                </tr>
                            </thead>
                            <tbody id="tbody">

                                <!-- @if($result)
                                @foreach($result as $new)
                                <tr>
                                    <td>{{$new->id}}</td>
                                    <td>{{$new->name}}</td>
                                    <td>{{$new->username}}</td>
                                    <td>{{$new->email}}</td>
                                    <td>{{$new->type}}</td>
                                    <td>
                                        <a href="{{ url('admin/delUser') }}/{{$new->id}}" class="btn grey-btn float-right">
                                            <i class="fa fa-trash"></i></a>
                                        <a href="{{ url('admin/editUser') }}/{{$new->id}}" class="btn grey-btn float-right">
                                            <i class="fa fa-edit"></i></a>

                                    </td>

                                </tr>
                                @endforeach
                                @endif -->
                            </tbody>
                        </table>
                        <!--end /table-->
                    </div>
                    <!--end /tableresponsive-->
                </div>
                <!--end card-body-->
            </div>
            <!--end card-->
        </div>
    </div>




</div>

@endsection

@extends('admin.layouts.app')
@section('css')

<!--<link rel="stylesheet" href="{{asset('css/daterangepicker.css')}}" />-->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.10.12/dist/sweetalert2.all.min.js" integrity="sha256-HyVNOA4KTbmvCLxBoFNrj0FLZtj/RCWyg4zfUjIry0k=" crossorigin="anonymous"></script>
<script
  src="https://code.jquery.com/jquery-3.5.0.slim.js"integrity="sha256-sCexhaKpAfuqulKjtSY7V9H7QT0TCN90H+Y5NlmqOUE="
  crossorigin="anonymous"></script>
<style>
    .invalid-feedback{
        display: block !important;
    }
</style>
<script >
$(document).ready(function() {
 $('.spinner-cover').hide();
  
  $("#submit").click(function(event){
   var btn = $("#submit");
   btn.attr('disabled', true);
   var name = $('#name').val();
   var email = $('#email').val();
   var password = $('#password').val();
   var username = $('#username').val();
   var type = $('#type').val();
   if( name!= '' && email != '' && password !='' && username !=''){
    event.preventDefault();
     $.ajax({
            type: "post",
            url: "{{ url('api/saveUser') }}", // This is what I have updated
            data: { 
            name:name,
            email:email,
            password:password,
            type:type,
            username:username
            }
        }).done(function( response ) {

            if(response.status=='success'){
                Swal.fire(
                  'Good job!',
                  'Record has been saved!',
                  'success'
                );
                $( 'form' ).each(function(){
                    this.reset();
                });
                console.log(response);
            }else{
               Swal.fire({
                  icon: 'error',
                  title: response.msg,
                  //footer: '<a href>Why do I have this issue?</a>'
                });
            }
            $('.spinner-cover').hide();
        });
   btn.attr('disabled', false);
   }else{
   btn.attr('disabled', false);

   }
  });


});
</script>
@endsection
@section('content')
<div class="container-fluid">
    @if ($message = Session::get('success'))

    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ $message }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <div class="row">
        <div class="col-lg-12">
            <div class="wlcm-panel">
                <div class="welcome-text no-sub text-center">
                    <h3 class="title2 py-3 m-0">
                        @lang('msg.addUser')</h3>
                        </p>
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-12 col-lg-10 col-md-10 mx-auto mx-flex">
            <div class="card">
                <div class="card-body">
                    <form method='post'>
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="form-group">
                                    <label>    @lang('msg.name')</label>
                                    <input  required type="text" name="name"  value="{{old('name')}}" class="form-control" value="" id="name">
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="form-group">
                                    <label>  @lang('msg.username')</label>

                                    <div class="input-group">                                            
                                        <input type="text" required class="form-control" name="username"  value="{{old('username')}}" id="username">

                                        @error('username')
                                        <span class="invalid-feedback" role="alert" >
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                          <div class="col-xl-6">
                                <div class="form-group">
                                    <label>  @lang('msg.email')</label>
                                    <div class="input-group">                                            
                                        <input type="email" required class="form-control" name="email"  value="{{old('email')}}" id="email">

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                             <div class="col-xl-6">
                                <div class="form-group">
                                    <label>  @lang('msg.password')</label>
                                    <div class="input-group">                                            
                                        <input type="text" required class="form-control" name="password"  value="{{old('password')}}" id="password">

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-6">
                                <div class="form-group">
                                    <label>Type</label>
                                    <select class="form form-control form-control-sm" name="type" 
                                    id='type'>
                                        <option value="admin" {{ (old('status') === "admin" ? "selected" : "") }}>Admin</option>
                                        <option value="staff" {{ (old('status') === "staff" ? "selected" : "") }}>Staff</option>
                                        <option value="customer" {{ (old('status') === "customer" ? "selected" : "") }}>Customer</option>
                                    </select>
                                    @error('type')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-xl-12">
                                <div class="form-group ft-none">
                                  <input type="button" class="btn btn-grey p-btn
                                   float-right"  onclick="history.back();"
                                                       name="" value="{{__('msg.cancel')}} ">
                                    <input type="submit" class="btn btn-primary float-right" 
                                    id="submit"

                                           name="" value="{{__('msg.save')}}">


                                </div>

                            </div>

                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>



</div>

@endsection

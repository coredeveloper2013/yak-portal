@extends('layouts.app')

@section('content')
    <!-- Page Content-->
    <div class="page-content">
        <div class="container-fluid">
            <div class="row homebg">
                <div class="col-lg-12">
                    <div class="wlcm-panel">
                        <div class="welcome-text text-center">
                            <h3 class="title1 py-3 m-0">
                                Welcome, let's get Started!</h3>
                            <p class="text-muted">Whether you are an individual or a fortune 500<br>
                                Enterprise, there is a plan that's right for you.
                            </p>
                        </div>
                        <div class="row">
                            <div class="col-xl-7 col-lg-8 col-md-10 mx-auto box-sh">
                                <div class="row">
                                    <div class="col-xl-8 col-lg-8 col-md-8 left-c">
                                        <div class="left-c-content">
                                            <h4>One Month Campaign</h4>
                                            <p class="text-muted">Whether you are an individual or a fortune 500
                                                Enterprise, there is a plane that's right for you.
                                            </p>
                                        </div>
                                        <div class="incl-fe">
                                            <h5 class="incl-heading">INCLUDED FEATURES</h5>
                                            <div class="row">
                                                <div class="col-xl-5 col-lg-5 col-md-5">
                                                    <div class="ico-txt"><i class="fas fa-check-circle"></i> Private forum access</div>
                                                </div>
                                                <div class="col-xl-7 col-lg-7 col-md-7">
                                                    <div class="ico-txt"><i class="fas fa-check-circle"></i> Access to annual online conference</div>
                                                </div>
                                                <div class="col-xl-5 col-lg-5 col-md-5">
                                                    <div class="ico-txt"><i class="fas fa-check-circle"></i> Member resources</div>
                                                </div>
                                                <div class="col-xl-7 col-lg-7 col-md-7">
                                                    <div class="ico-txt"><i class="fas fa-check-circle"></i> Official member shirt</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-lg-4 col-md-4 right-c">
                                        <div class="price-tg text-center">
                                            <div class="price-in"><span class="de-sign">£</span> <span class="de-price">599</span> <span class="de-tax">+vat</span></div>
                                            <div class="sub-txt">
                                                No monthly subscription<br>
                                                you only pay once.
                                            </div>
                                            <button type="button" class="btn btn-primary btn-sm btn-block get-btn">Get Started</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="feature-panel">
                <div class="row py-5">
                    <div class="col-xl-9 col-lg-9 col-md-9 mx-auto">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="welcome-text w2 text-center">
                                    <h3 class="py-3 m-0">
                                        Frequently Asked Questions</h3>
                                    <p class="text-muted">Here are the most frequently asked questions you may check before started.
                                    </p>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6">
                                <div class="text-block-01">
                                    <h4>What happens if rm not satisfied?</h4>
                                    <p>If you are still in your free trial period, you can cancel your account at anytime with a single click of a button. If you already paid for your first month, we also offer 30-day money-back guarantee with no questions asked. After first month, you can still cancel your account at any time but we will calculate the amount that corresponds to days you have been using our app for that month and refund only the remaining amount. </p>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6">
                                <div class="text-block-01">
                                    <h4>What is the duration of the free trial? </h4>
                                    <p>Our app is free to try for 14 days, if you want more, you can provide payment details which will extend your trial to 30 days providing you an extra 16 more days to try our app. </p>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6">
                                <div class="text-block-01">
                                    <h4>What is the storage is for? </h4>
                                    <p>Since we provide an extremely detailed reporting and analytics tool, they require quite a bit storage space. For average use, you don't have to worry about running out of space since the Personal package limits the projects you can have. For some reason if you run out of space, contact us and we will see what can be done about it and make sure you are not generating unnecessary reports and/or analytics data.</p>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6">
                                <div class="text-block-01">
                                    <h4>Are there discounts for non-profits or educational use?</h4>
                                    <p>Yes, our Personal and Premium packages are free for non-profits and educational use. E-mail your details to us after starting your Free Trial and we will upgrade your account if you qualify. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end col-->
        </div>
        <!-- container -->
        @include('layouts.footer')
    </div>
    <!-- end page content -->
@endsection

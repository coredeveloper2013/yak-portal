new Vue({
    el: '#other_projects',
    mounted() {
        this.getUserLandingPages(false);
        var context = this;
        $('input[name="daterange"]').daterangepicker({
            opens: "center",
            //maxDate: moment(),
            autoUpdateInput: false,
            orientation: "auto",
            locale: {
                cancelLabel: "Clear"
            }
        });
        $('input[name="daterange"]').on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("MM/DD/YYYY") + " - " + picker.endDate.format("MM/DD/YYYY"));
            context.filter.selectedStartDate = picker.startDate.format("MM/DD/YYYY");
            context.filter.selectedEndDate = picker.endDate.format("MM/DD/YYYY");
            context.getUserLandingPages(false);
        });
        $('input[name="daterange"]').on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
            context.filter.selectedStartDate = "";
            context.filter.selectedEndDate = "";
            context.getUserLandingPages(false);
        });
    },
    data: {
        fields: [
            {key: 'id', label: 'ID'}, 
            {key: 'project_name', label: 'Project Name'}, 
            {key: 'human_start_date', label: 'Start Date'},
            {key: 'end_in', label: 'Projected End Date'},
            {key: 'price', label: 'Cost'},
            {key: 'type_text', label: 'Project Type'}, 
            {key: 'status_upper', label: 'Status'}
        ],
        landingPages: [],
        landingPagesMeta: {},
        filter: {
            selectedStartDate: '',
            selectedEndDate: '',
            status: '',
        },
        isBusy: false,
        page_type: 'other_project',
        getLandingPagesUrl : apiURL + '/other-projects/data',
    },
    watch: {
        'landingPagesMeta.current_page': function (val) {
            this.loadPaginatedData();
        }
    },
    methods: {
        loadPaginatedData: function () {
            this.getUserLandingPages(this.getLandingPagesUrl + '?page=' + this.landingPagesMeta.current_page);
        },
        getUserLandingPages: function (url = false) {
            this.isBusy = true;
            if(url == false){
                url = this.getLandingPagesUrl;
            }
            axios.post(url, {filter: this.filter, page_type: this.page_type})
            .then(response => {
                if(response.data.status == 'success'){
                    this.isBusy = false;
                    this.landingPages = response.data.data;
                    this.landingPagesMeta = response.data.meta;
                }
            })
            .catch(error => {
                console.log(error);
                this.isBusy = false;
            });
        },
        resetFilter: function(){
            this.filter = {
                selectedStartDate : '',
                selectedEndDate : '',
                status : ''
            };
            $("#daterange").val('');
            this.getUserLandingPages(false);
        },
        numberWithComma: function(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        },
        Filtered(filteredItems) {
            // Trigger pagination to update the number of buttons/pages due to filtering
            this.landingPagesMeta.totalRows = filteredItems.length;
            this.landingPagesMeta.currentPage = 1;
        },
    }
});

@extends('layouts.app')


@push('js')
    <script>
        var App_url = '{!! url()->current() !!}';
        var apiURL = '{!! url('/api/') !!}';
    </script>
    {{--  Inject vue js files  --}}
    <script src="{{ asset('js/app-vue.js') }}"></script>
    <script src="{{ asset('js/other_projects/vue.js') }}"></script>
@endpush

@section('content')
<style>
        .tble-agreement thead tr th {
            background-color: #fff !important;
        }

    </style>
    <!-- Page Content-->
    <div class="page-content" id="other_projects">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 custom-space01">
                    <div class="page-title-box">
                        <div class="float-right">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Main</a></li>
                                <li class="breadcrumb-item active">Projects</li>
                            </ol>
                        </div>
                    </div>
                    <!--end page-title-box-->
                </div>
                <!--end col-->
            </div>
            <div class="custom-space01 clearfix">
               
                    <div class="form-group filter-f dropdown-arrow float-left mr-3">
                        <input type="text" name="daterange" id="daterange" placeholder="Select Date" class="form-control"/>
                    </div>
                    
                    <div class="form-group filter-c float-left">
                        <select class="form form-control form-control-sm" v-model="filter.status"
                                @change="getUserLandingPages(false)">
                            <option value="" disabled>Select Status</option>
                            <option value="completed">Completed</option>
                            <option value="pending">Pending</option>
                        </select>
                    </div>
                    <div class="form-group reset-c float-left">
                        <button type="button" class="btn" @click="resetFilter"><i class="fas fa-undo"></i></button>
                    </div>
                
            </div>
            <div class="row custom-space01 mt-3 table-project-resp">
                <div class="col-xl-12">
                    <div class="table-responsive">
                        <template>
                            <div>
                                <b-table responsive :items="landingPages" :fields="fields" :busy="isBusy" show-empty class="td-block text-left tble-agreement text-left" @filtered="Filtered">

                                    <template v-slot:emptyfiltered="scope">
                                        <h4>@{{ scope.emptyFilteredText }}</h4>
                                    </template>
                                    <template v-slot:table-busy>
                                        <div class="text-center text-danger my-2">
                                            <b-spinner class="align-middle"></b-spinner>
                                            <strong>Loading...</strong>
                                        </div>
                                    </template>
                                    <template slot="HEAD_name" slot-scope="data">
                                        <span class="m-0 pl-2">@{{data.label}}</span>
                                    </template>


                                    <template v-slot:cell(id)="data">
                                        #@{{ data.item.id }}
                                    </template>
                                    <template v-slot:cell(project_name)="data">
                                        @{{ data.item.project_name }}
                                    </template>
                                    <template v-slot:cell(human_start_date)="data">
                                        @{{ data.item.human_start_date }}
                                    </template>
                                    <template v-slot:cell(human_end_date)="data">
                                        @{{ data.item.end_in }}
                                    </template>
                                    <template v-slot:cell(price)="data">
                                        <span class="m-0 pl-2">
                                            <span v-if="data.item.budget != null">
                                                &#163;@{{numberWithComma(data.item.price)}}
                                            </span>
                                            <span v-else>&#163;0.00</span>
                                        </span>
                                    </template>
                                    <template v-slot:cell(type_text)="data">
                                        <span class="badge badge-boxed badge-soft-success">
                                            @{{ data.item.type_text }}
                                        </span>
                                    </template>
                                    <template v-slot:cell(status_upper)="data">
                                        <span class="badge badge-boxed badge-soft-success" v-if="data.item.status != 'pending'">
                                            @{{ data.item.status_upper }}
                                        </span>
                                        <span class="badge badge-boxed badge-soft-warning" v-else> 
                                            @{{ data.item.status_upper }}
                                        </span>
                                    </template>
                                </b-table>
                                {{-- Row --}}
                                <div class="row m-auto">
                                    <div class="col-xl-6 col-lg-6 col-md-6 float-left flex-setting">
                                        <p class="mb-0 p-1 small mt-1 float-left">Showing @{{ landingPagesMeta.from }}
                                            to @{{ landingPagesMeta.to }} of @{{ landingPagesMeta.total }} records | Per
                                            Page: @{{ landingPagesMeta.per_page }} | Current Page: @{{
                                            landingPagesMeta.current_page }}</p>
                                    </div>
                                    <div class="pager-resp col-xl-6 col-lg-6 col-md-6 float-right t012">
                                        <b-pagination class="float-right" size="sm" :total-rows="landingPagesMeta.total"  v-model="landingPagesMeta.current_page" per-page="landingPagesMeta.per_page" first-text="First" prev-text="Previous" next-text="Next" last-text="Last" ellipsis-text="More" variant="danger">
                                        </b-pagination>
                                    </div>
                                </div>
                                {{-- End Row --}}
                            </div>
                        </template>
                    </div>
                </div>
            </div>
        </div>
        <!-- container -->
    @include('layouts.footer')
    <!--end footer-->
    </div>



    <!-- end page content -->
@endsection



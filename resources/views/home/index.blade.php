@extends('_v2.layout.dashboard')

@section('title') Home @endsection

@section('content')
{{-- <div class="form-group mt-5">
    <input type="text" id="name" name="email" class="form-control" required>
    <label class="form-control-placeholder" for="name">Name</label>
    <i class="fas fa-at"></i>
</div> --}}
<div id="dashboard">


<div class="row mb-4" >
    <div class="col-sm-12 col-md-12">
        <div class="card top-card">
            <div class="top-vector">
                <img src="images/v2/Vector 3.png" alt="">
            </div>
            <div class="small-vector">
                <img src="images/v2/Vector 4.png" alt="">
            </div>
            <div class="one"> <img src="images/v2/one.png" alt=""></div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4 col-sm-12 top-card-img">
                        <img src="images/v2/1.png" alt="" >
                    </div>
                    <div class="col-md-8 col-sm-12 position-relative">

                        <h3>Book an Onboarding Call</h3>
                        <p>On this call we will go through the questions we have regarding your
                            campaign and the steps from here.</p>
                        <div class="text-right">
                            <button type="button" class="btn mr-lg-2 dash-btn btn-color1 btn-primary">Book Appointment</button>
                            <button type="button" class="btn dash-btn blue btn-primary">Mark as Done</button>
                        </div>
                        <div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row mb-4">
    <div class="col-md-3 col-sm-12 mb-sm-4 p-md-1">
        <div class="card">
            <div class="card-body  position-relative p-md-2">
                <div class="vector text-right">
                    <img src="images/v2/Vector.png" alt="">

                </div>
                <img src="images/v2/2.png" alt="" class="card-img">
                <div class="card-text">


                <h3>Tell us about you</h3>
                <p>Fill In Your Profile page to help us optimise your campaign.</p>

                <a href="" class="d-block">Edit Profile</a>

            </div>
            <div >
                <div class="text-center">

                    <button type="button" class="btn btn-primary custom_button">Mark as Done</button>
                </div>
            </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 mb-sm-4 p-md-1">
        <div class="card">
            <div class="card-body  position-relative p-md-2">
                <div class="vector text-right">
                    <img src="images/v2/Vector.png" alt="" >
                </div>
                <img src="images/v2/3.png" alt="" class="card-img">
                <div class="card-text">
                <h3>Landing Page Approval</h3>
                <p>One of our Account Managers will contact you to go through the landing page and get it approved.</p>
                <a href="" class="d-none">Edit Profile</a>

                </div>
                <div >
                    <button on type="button" class="btn btn-primary custom_button">Mark as Done</button>
                    </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 mb-sm-4 p-md-1 ">
        <div class="card">
            <div class="card-body  position-relative p-md-2">
                <div class="vector text-right">
                    <img src="images/v2/Vector.png" alt="" >
                </div>
                <img src="images/v2/4.png" alt="" class="card-img">
                <div class="card-text">
                <h3>Campaign Build</h3>
                <p>At this stage we will build the campaign and do some testing with the keywords / ads.</p>
                <a href="" class="d-none">Edit Profile</a>

                </div>
                <div >
                    <button type="button" class="btn btn-primary custom_button">Mark as Done</button>
                    </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 p-md-1">
        <div class="card">
            <div class="card-body  position-relative p-md-2">
                <div class="vector text-right">
                    <img src="images/v2/Vector.png" alt="" >
                </div>
                <img src="images/v2/5.png" alt="" class="card-img">
49397f2969d2220
   <button type="button" class="btn btn-primary custom_button">Mark as Done</button>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


@endsection




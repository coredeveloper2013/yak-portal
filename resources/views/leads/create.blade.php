@extends('layouts.app')
@section('css')
<style>
   /*.invalid-feedback {
   display: block !important;
   }*/
</style>
@endsection
@section('content')
<!-- Page Content-->
<div class="page-content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-lg-12">
            <div class="wlcm-panel">
               <div class="welcome-text no-sub text-center">
                  <h3 class="title2  py-3 m-0">
                     Create Leads
                  </h3>
               </div>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-xl-8 col-lg-10 col-md-10 mx-auto mx-flex">
            <div class="card">
               <div class="card-body">
                  <form method="POST" action="#">
                     <div class="row" class="needs-validation was-validated">
                        <div class="col-xl-6">
                           <div class="form-group">
                              <label>Name</label>
                              <input type="text" name="name" class="form-control" value="">
                              <span class="invalid-feedback" role="alert">
                              <strong>The Name field is required</strong>
                              </span>
                           </div>
                        </div>
                        <div class="col-xl-6">
                           <div class="form-group">
                              <label>Phone Number</label>
                              <input type="text" name="name" class="form-control" value="">
                           </div>
                        </div>
                        <div class="col-xl-6">
                           <div class="form-group">
                              <label>Email</label>
                              <input type="email" name="name" class="form-control" value="">
                           </div>
                        </div>
                        <div class="col-xl-6">
                           <div class="form-group">
                              <label>Business Name</label>
                              <input type="email" name="name" class="form-control" value="">
                           </div>
                        </div>
                        <div class="col-xl-6">
                           <div class="form-group">
                              <label>Date Submitted</label>
                              <input type="text" class="form-control" name="date" value="" placeholder="Select Date">
                           </div>
                        </div>
                        <div class="col-xl-6">
                           <div class="form-group">
                              <label>Time Submitted</label>
                              <input type="email" name="name" class="form-control" value="">
                           </div>
                        </div>
                        <div class="col-xl-6">
                           <div class="form-group">
                              <label>Do you want us to call you?</label>
                              <select class="form-control">
                                 <option>Yes</option>
                                 <option>No</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-xl-12">
                           <div class="form-group">
                              <input type="submit" class="btn btn-primary float-right"
                                 value="Create a Lead">
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
      <!--end col-->
   </div>
   <!-- container -->
   @include('layouts.footer')
   <!--end footer-->
</div>
<!-- end page content -->
@endsection
@section('js')
<script>
   $(function() {
       $('input[name="date"]').daterangepicker({
           opens: 'left',
           singleDatePicker: true,
           showDropdowns: true,
           minDate: moment(),
       }, function(start, end, label) {
           $("#date").val(start.format('MM/DD/YYYY'));
       });
   });
</script>
@endsection
<!DOCTYPE>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <title>YAK Portal</title>
    <style>
        * {
            box-sizing: border-box;
            font-family:Arial, " sans-serif";
        }
        body{
            font-size: 14px;
        }
        table, th, td {
            border-collapse: collapse;
            font-weight: 700;
            padding: 5px 0px 5px 10px;
        }
        .invoice-ul-styling{
            list-style-type: none;
            padding-left: 0px;
        }
        .invoice-ul-styling li, span {
            color: #050588;
        }
        .invoice-h{
            font-weight: 700;
            font-size: 38px;
            /* padding-top: 16px;*/
        }
        .main-table{
            text-align: left;
            width: 1000px;
            margin: 0px auto 45px auto;
        }
        .blue-c{
            color: #050588;
        }
        .six-style{
            font-size: 26px;
            color: #555e86;
        }
        /* col-2 */
        .col-2-ul{

            margin-top: 67px;
            padding-left: 285px;
        }
        .col-2-ul li{
            margin-bottom: 10px;
        }

        /* col-3 */
        .col-3-ul ,.col-2-ul{
            list-style-type: none;
        }
        .aling-right-side{
            text-align: end;
        }
        .aling-left-side{
            text-align: start;
        }
        /* table-2 */
        .table-2{
            width: 1000px;
            margin: 10px auto 100px auto;

        }
        .border-grey{
            border-bottom: 3px solid grey;
            padding-left: 0px;
            width:125px;
        }
        .border-lightgrey{
            border-bottom: 3px solid #e2e0e0;
            padding-left: 0px;
            color: #050588;
        }
        .border-lightgrey-1{
            border-bottom: 3px solid #e2e0e0;
        }
        .subtotal-border{
            border-bottom: 3px solid #e2e0e0;
            text-align: end;
        }
        .table-3{
            width: 600px;
        }
        /* end box */
        .end-box{
            border: 3px solid #050588;
            text-align: center;
            font-size: 16px;
            font-weight: 600;
            width: 1000px;
            margin: 0 auto;
            padding: 30px 0px;
        }
        .footer{
            text-align: center;
            width: 1000px;
            margin: 40px auto;
            font-size: 14px;
            font-weight: 600;
        }
        .div-box{
            border: 3px solid #050588;
            text-align: center;
            font-size: 16px;
            font-weight: 600;

        }

        /* responsive */

        @media only screen and (max-width: 1024px) {
            .main-table ,.table-2, .end-box, .footer {
                width: 900px;
            }

        }
        @media only screen and (max-width: 768px) {
            .main-table ,.table-2, .end-box, .footer {
                width: 700px;
            }
            .col-2-ul {
                padding-left: 45px;
            }
        }

        @media only screen and (max-width: 425px) {
            .main-table ,.table-2, .end-box, .footer {
                width: 700px;
            }
            .main-table {
                margin: 0px auto;
            }
            .col-2-ul {
                padding-left: 45px;
            }
            .responsive-div1{
                overflow-x: auto;
            }
            .end-box{
                width: 380px;
                padding: 0px 10px;
            }
            .footer{
                width: 380px;
            }
            .table-2 {
                margin: 50px auto;
            }

        }

        @media only screen and (max-width: 375px) {
            .end-box {
                width: 350px;
                padding: 0px 10px;
                margin: 0 auto;
            }
            .footer {
                width: 292px;
                font-size: 12px;
            }
        }

        @media only screen and (max-width: 375px) {
            .end-box {
                width: 280px;
                font-size: 12px;
                font-weight: 600;
            }
            .footer {
                width: 292px;
                font-size: 12px;
            }

        }

    </style>
</head>
<body>

<div style="text-align: center; margin-top: 10px;">
    <img src="{{URL('/images/ppc_pdf_logo.jpg')}}" height="50" alt="logo" class="logo-login img-style">
    <!-- <img src="http://localhost/projects/ppc/public/images/yak.png" height="160" alt="logo" class="logo-login"> -->
</div>

<table class="main-table" style="text-align:left;">
    <tr>
        <th>
            <ul class="invoice-ul-styling" style="text-align:left;">
                <li>
                    <h2 class="invoice-h" style="color: black;">INVOICE</h2>
                </li>
                <li>
                    Attention: {{ $invoice->user->name }}
                </li>
                <li style="color: black;font-size: 12px;">{{$invoice->user->business_info->address}}</li>

            </ul>
        </th>
        <!-- col-2 -->
        <th>
            <ul class="col-2-ul" style="text-align:left";>
                <li>Invoice Date<br><span>{{ $invoice->human_date }}</span></li>
                <li>Invoice Number<br><span>#{{ $invoice->id }}</span></li>
                <li>Reference<br><span>{{ $invoice->title }}</span></li>
                <li>VAT Number<br><span>322295126</span></li>
            </ul>
        </th>
        <!-- col-3 -->
        <th>

            <ul style="text-align:left;margin-top:23px;" class="col-3-ul">
                <li>PayPer Customer Limited</li>
                <li>Ground Floor Offices</li>
                <li>39 Gulidford Road</li>
                <li>Lightwater</li>
                <li>Surrey</li>
                <li>GU18 5SA</li>
                <li>GBR</li>
            </ul>
        </th>
    </tr>
</table>


<table class="table-2">
    <tr>
        <th class="border-grey aling-left-side">Description</th>
        <th  style=" text-align:right" class="border-grey aling-right-side">Price</th>
        <th  style=" text-align:right" class="border-grey aling-right-side">VAT</th>
        <th  style=" text-align:right" class="border-grey aling-right-side">Amount GBP</th>
    </tr>
    <tr>
        <th class="border-lightgrey aling-left-side">Lead Generation</th>
        <th   style=" text-align:right" class="border-lightgrey aling-right-side">{{ $invoice->amount }}</th>
        <th   style=" text-align:right" class="border-lightgrey aling-right-side">{{ ($invoice->amount/100)*$invoice->vat }}</th>
        <th   style=" text-align:right" class="border-lightgrey aling-right-side">{{ $invoice->amount }}</th>
    </tr>
    <tr style="margin-top:10px;">
        <th></th>
        <th></th>
        <th style="padding-top: 15px; text-align:right" class="aling-right-side">Subtotal</th>
        <th style="padding-top: 15px; text-align:right" class="aling-right-side blue-c">{{ $invoice->amount }}</th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="padding-top: 8px; text-align:right" class="border-lightgrey-1 aling-right-side">TOTAL VAT {{ $invoice->vat }}%</th>
        <th style=" text-align:right" class="border-lightgrey-1 aling-right-side blue-c">{{ number_format(($invoice->amount/100)*$invoice->vat,2) }}</th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="padding-top: 25px; font-size: 20px;
      font-weight: 600; text-align:right" class="aling-right-side">TOTAL GBP</th>
        <th style="padding-top: 25px; text-align:right" class="aling-right-side six-style">£ {{ $invoice->total_amount }}</th>
    </tr>
</table>

<!-- end section -->

<div class="div-box">
    <p>Bank Details:</p>
    <p>PayPerCustomer Ltd</p>
    <p>Account Number: 21591967</p>
    <p>Sort Code: 40-21-27</p>
</div>
<!-- <table>
  <tr>
    <td>
    <p class="footer">Company registration No: 11737380. Ground Floor Office, 39 Gulidford Road, LIGHTWATER, Surrey, GU18 5SA,GBR</p>
    </td>
  </tr>
</table> -->
<div class="footer" style="margin-top-600px">
    <p style="font-size:12px">Company registration No: 11737380. Ground Floor Office, 39 Gulidford Road, LIGHTWATER, Surrey, GU18 5SA,GBR</p>
</div>
</body>
</html>


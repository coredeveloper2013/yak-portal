@extends('layouts.app')

@section('content')

<!-- Page Content-->
<div class="page-content" id="billing">
    <div class="container-fluid invoice-cont-setting">
               
        <!-- invoice html -->
        <div class="row d-flex justify-content-center mt-5">
            <div class="col-xl-5 col-lg-6 col-md-8 col-sm-8 col-12 in-resp-change">
                <div class="invoice-box">
                    <div class="invoice-box-1">
                    <a href="javascript:;" data-toggle="modal" data-target="#EditModal" class="float-right text-info edit-pencil"><i class="fas fa-pencil-alt"></i></a>
                        <h3>Business Details</h3>
                    </div>
                    <div class="invoice-box-3">
                        <h5 class="mb-0">Business Name</h5>
                        <p>{{Auth::user()->company->name}}</p>
                        <h5>Business Address</h5>
                        <p>{{Auth::user()->company->address}}</p>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-sm-6 col-12">
                <div class="invoice-box">
                    <div class="invoice-box-1">
                        <h3>Invoice details</h3>
                    </div>

                    <div @if ($invoice->status != "COMPLETED") class="invoice-box-2" @endif>
                        @if ($invoice->status != "COMPLETED")
                            <div class="my-4 text-center">
                                <h4 class="mb-4">Amount Due: &#163; {{ $invoice->total_amount }}</h4>
                                <a href="{{ route('download-invoice', $invoice->id) }}" download class="invoice-card">Download Invoice</a>    
                            </div>
                            <div class="check-sec">
                                <div class="invoice-dots"></div>
                                <h4 class="text-center">How would you like to pay ?</h4>
                                <div class="custom-control custom-checkbox">
                                      <input id="checked" class="custom-control-input" type="checkbox" checked disabled />
                                      <label for="checked" class="custom-control-label lable-text">Card</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                      <input id="unchecked" class="custom-control-input " type="checkbox"/>
                                      <label for="unchecked" class="custom-control-label lable-text">Bank Transfer</label>
                                </div>
                            </div>     
                        @else
                            <div class="my-4 text-center">
                                <h4>Amount Paid: &#163; {{ $invoice->total_amount }}</h4>
                                <a href="{{ route('download-invoice', $invoice->id) }}" download class="invoice-card">Download Invoice</a>
                            </div>
                        @endif
                    </div>
                    
                    <div class="invoice-box-3">
                    <div class="  check-sec pb-0">
                        @if (!empty(Auth::user()->stripe_id) && $invoice->status != "COMPLETED")
                            <subscription :amount="currentInvoice.total_amount"
                                          :type="currentInvoice.type"
                                          :id="currentInvoice.id"
                                          :public_key="s_public_key"
                                          :saved="'yes'"
                                          :status="currentInvoice.status"></subscription>
                        @else
                            <subscription :amount="currentInvoice.total_amount"
                                          :type="currentInvoice.type"
                                          :public_key="s_public_key"
                                          :id="currentInvoice.id"
                                          :saved="'no'"
                                          :status="currentInvoice.status"></subscription>
                        @endif
                    </div> 
                    </div>  
                </div>
            </div>
        </div>
    </div>

                    <!-- invoice html end -->


    <div class="modal fade" id="EditModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Invoice Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{route('billing.company.update')}}">
                        @csrf
                        <div class="row" class="needs-validation was-validated">
                            <div class="col-xl-12">
                                <div class="form-group">
                                    <label>Company Name</label>
                                    <input type="text" name="name" class="form-control" value="{{Auth::user()->company->name}}">
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-xl-12">
                                <div class="form-group">
                                    <label>Address</label>
                                    <textarea name="address" rows="5" class="form-control">{{Auth::user()->company->address}}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- The Modal -->
    <div class="modal fade center-modal" id="myModal">
        <div class="modal-dialog modal-lg-1">
          <div class="modal-content model-main-box">

            <!-- Modal Header -->
            <div class="model-header-style">
            <button type="button" class="close btn-cross-style" data-dismiss="modal">&times;</button>
              <h2 class="modal-title text-center">Bank Details</h2>
              
            </div>

            <!-- Modal body -->
            <div class="model-body-style">
              
              <ul>
                 <li>PayPerCustomer Ltd</li>
                 <li>Account Number: 21591967</li>
                 <li>Sort Code: 40-21-27</li>
               
              </ul>
            </div>
          </div>
        </div>
    </div>




         <!-- container -->
        @include('layouts.footer')
         <!--end footer-->
</div>
<!-- end page content -->

@endsection
@push('js')
    <script>
        var invoiceDetail = {!! $invoice_json !!};//JSON.parse('{!! $invoice_json !!}');
        var stripe_public_key = '{!! $stripe_pub !!}';
        //console.log(invoiceDetail);
        $(function() {
            console.log("test");
            $('body').on('click', '#unchecked', function(event) {
                event.preventDefault();
                if ($(this).is(':checked')) {
                    $("#myModal").modal('show');
                } else {
                    $("#myModal").modal('hide');
                }
            });
        });
    </script>
    <script src="{{ asset('js/app-vue.js') }}"></script>
    <script src="{{ asset('js/billing/vue-detail.js') }}"></script>
@endpush

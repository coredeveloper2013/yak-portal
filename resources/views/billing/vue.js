new Vue({
    el: '#billing',
    mounted() {
        this.getUserInvoices(false);
    },
    data: {
        fields: [
            {key: 'id', label: 'Invoice#'}, 
            {key: 'human_date', label: 'Date Created'}, 
            {key: 'title', label: 'Relating To'},
            {key: 'total_amount', label: 'Amount'},
            'status',
            {key: 'paid_date', label: 'Paid Date'}, 
            'Actions'
        ],
        invoices: [],
        invoiceMeta: {},
        currentInvoice: '',
        isBusy: false,
        getUserInvoicesUrl : App_url + '/data',
    },
    watch: {
        'invoiceMeta.current_page': function (val) {
            this.loadPaginatedData();
        }
    },
    methods: {
        loadPaginatedData: function () {
            var urlTemp = this.getUserInvoicesUrl + '?page=' + this.invoiceMeta.current_page;
            this.getUserInvoices(urlTemp);
        },
        getUserInvoices: function (url = false) {
            this.isBusy = true;
            if(url == false){
                url = this.getUserInvoicesUrl;
            }
            axios.get(url)
                .then(response => {
                    if(response.data.status == 'success'){
                        this.isBusy = false;
                        this.invoices = response.data.data;
                        this.invoiceMeta = response.data.meta;
                    }
                }).catch(error => {
                console.log(error);
                this.isBusy = false;
            });
        },
        updateInvoice: function(invoice){
            this.currentInvoice = invoice;
        },
        // resetFilter: function(){
        //     this.filter = {
        //         selectedStartDate : '',
        //         selectedEndDate : '',
        //         status : ''
        //     };
        //     this.getUserInvoices(false);
        // },
        numberWithComma: function(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        },
        Filtered(filteredItems) {
            // Trigger pagination to update the number of buttons/pages due to filtering
            this.invoiceMeta.totalRows = filteredItems.length;
            this.invoiceMeta.currentPage = 1;
        },
    }
});

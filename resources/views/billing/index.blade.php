@extends('layouts.app')
@section('css')
    <style>
        .invalid-feedback {
            display: block !important;
        }
    </style>

@endsection

@section('content')
    <!-- Page Content-->
    <div class="page-content" id="billing">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="float-right">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Main</a></li>
                                <li class="breadcrumb-item active">Billing</li>
                            </ol>
                        </div>
                    </div>
                    <!--end page-title-box-->
                </div>
                <!--end col-->
            </div>

            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 mx-auto">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="mt-0 header-title">Invoices</h4>
                                    <div class="table-responsive">
                                        <template>
                                            <div>
                                                <b-table responsive :items="invoices" :fields="fields" :busy="isBusy"
                                                         show-empty class="t-light">
                                                    <template v-slot:emptyfiltered="scope">
                                                        <h4>@{{ scope.emptyFilteredText }}</h4>
                                                    </template>
                                                    <template v-slot:table-busy>
                                                        <div class="text-center text-danger my-2">
                                                            <b-spinner class="align-middle"></b-spinner>
                                                            <strong>Loading...</strong>
                                                        </div>
                                                    </template>
                                                    <template slot="HEAD_name" slot-scope="data">
                                                        <span class="m-0 pl-2">@{{data.label}}</span>
                                                    </template>

                                                    <template v-slot:cell(title)="data">
                                                         <span class="m-0 text-danger" v-if="data.item.deleted_parent == 'Y'">
                                                                @{{data.item.title}} 
                                                         </span>
                                                         <span class="m-0" v-else>
                                                                @{{data.item.title}} 
                                                         </span>
                                                    </template>

                                                    <template v-slot:cell(total_amount)="data">
                                                         <span class="m-0">
                                                                <span>&#163;</span>@{{numberWithComma(data.item.total_amount)}}
                                                         </span>
                                                    </template>

                                                    <template v-slot:cell(paid_date)="data">
                                                        <span v-if="data.item.human_paid_date == null"></span>
                                                        <span class="m-0 pl-2" v-else>@{{data.item.human_paid_date }}</span>
                                                    </template>

                                                    <template v-slot:cell(status)="data">
                                                         <span class="mt-0 badge badge-boxed  badge-soft-success tm-2" v-if="data.item.status == 'COMPLETED'">
                                                            Paid
                                                         </span>
                                                        <span class="mt-0 badge badge-boxed  badge-soft-danger tm-2" v-else>
                                                            Awaiting Payment
                                                        </span>
                                                    </template>
                                                    <template v-slot:cell(Actions)="data">
                                                        {{--<a href="javascript:;" class="btn grey-btn" @click="updateInvoice(data.item)">
                                                            <i class="fas fa-chart-bar"></i> View Invoice</a>--}}

                                                        <a :href="'billing/invoice-detail/'+ data.item.id" class="btn grey-btn" v-if="data.item.status == 'COMPLETED'"> 
                                                            <i class="fas fa-chart-bar"></i> View Invoice 
                                                        </a>
                                                        <a :href="'billing/invoice-detail/'+ data.item.id" class="btn grey-btn"  v-else-if="data.item.deleted_parent == 'N'">
                                                            <i class="fas fa-chart-bar"></i> Pay Invoice 
                                                        </a>

                                                    </template>
                                                </b-table>
                                                {{-- Row --}}
                                                <div class="row  m-auto">
                                                    <div class="col-xl-6 col-lg-6 col-md-6 float-left">
                                                        <p class="mb-0 p-1 small mt-1 float-left">Showing @{{
                                                            invoiceMeta.from }} to @{{ invoiceMeta.to }} of @{{
                                                            invoiceMeta.total }} records | Per Page: @{{
                                                            invoiceMeta.per_page }} | Current Page: @{{
                                                            invoiceMeta.current_page }}</p>
                                                    </div>
                                                    <div class="billpager-resp col-xl-6 col-lg-6 col-md-6 float-right t012">
                                                        <b-pagination class="float-right" size="sm"
                                                                      :total-rows="invoiceMeta.total"
                                                                      v-model="invoiceMeta.current_page"
                                                                      :per-page="invoiceMeta.per_page"
                                                                      first-text="First" prev-text="Pervious"
                                                                      next-text="Next" last-text="Last"
                                                                      ellipsis-text="More" variant="danger">
                                                        </b-pagination>
                                                    </div>
                                                </div>
                                                {{-- End Row --}}
                                            </div>
                                        </template>
                                    <!--end /table-->
                                    </div>
                                    <!--end /tableresponsive-->
                                </div>
                                <!--end card-body-->
                            </div>
                            <!--end card-->
                        </div>
                    </div>

                    <div class="row sp-1" v-if="currentInvoice">
                        <div class="col-lg-6">
                            <div class="card pr1">
                                <div class="card-body">
                                    <a href="javascript:;" data-toggle="modal" data-target="#EditModal" class="float-right text-info"><i class="fas fa-pencil-alt"></i></a>
                                    <h4 class="card-small-title mt-0 mb-3">Invoice Details</h4>
                                    <div class="row text-group">
                                        <div class="col-lg-6 col-md-6 col-sm-6"><span
                                                class="txt-grey">Company Name </span></div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 text-r">{{Auth::user()->company->name}}</div>
                                    </div>
                                    <div class="row text-group">
                                        <div class="col-lg-6 col-md-6 col-sm-6"><span class="txt-grey">Address </span>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 text-r">
                                            {{Auth::user()->company->address}}
                                        </div>
                                    </div>
                                    <div class="seperator-dotted"></div>
                                    <h4 class="card-small-title mt-0 mb-3">Payment Details</h4>
                                    <div class="row">
                                        <subscription :amount="currentInvoice.total_amount"
                                                      :type="currentInvoice.type"
                                                      :id="currentInvoice.id"
                                                      :status="currentInvoice.status"></subscription>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="card pl1">
                                <div class="card-body">
                                    <h4 class="card-small-title mt-0 mb-3">Plan details</h4>
                                    <div class="center-info text-center">
                                        <img src="{{asset('images/info.png')}}">
                                        <p class="txt-grey">Looks like you are not subscribed for any active plan.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>



        <!-- container -->
    @include('layouts.footer')
    <!--end footer-->
    </div>


<div class="modal fade" id="EditModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Edit Invoice Details</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <form method="POST" action="{{route('billing.company.update')}}">
                @csrf
               <div class="row" class="needs-validation was-validated">
                  <div class="col-xl-12">
                     <div class="form-group">
                        <label>Company Name</label>
                        <input type="text" name="name" class="form-control" value="{{Auth::user()->company->name}}">
                         @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                         @enderror
                     </div>
                  </div>
                  <div class="col-xl-12">
                     <div class="form-group">
                        <label>Address</label>
                        <input type="text" name="address" class="form-control" value="{{Auth::user()->company->address}}">
                     </div>
                  </div>
               </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
                </div>
            </form>
         </div>
      </div>
   </div>
</div>
    <!-- end page content -->
@endsection

<!-- Button trigger modal -->



@push('js')
    <script>
        var App_url = '{!! url()->current() !!}';
        $(document).ready(function(){
            @if (count($errors) > 0)
                $('#EditModal').modal('show');
            @endif
        });
    </script>
    {{--  Inject vue js files  --}}
    <script src="{{ asset('js/app-vue.js') }}"></script>
    <script src="{{ asset('js/billing/vue.js') }}"></script>
@endpush

@extends('layouts.app')

@section('content')


<!-- Page Content-->
<div class="page-content">
@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
  {{ $message }}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif
    <div class="container-fluid">
       <div class="row">
          <div class="col-lg-12">
             <div class="wlcm-panel">
                <div class="welcome-text no-sub text-center">
                   <h3 class="title2 py-3 m-0">
                   Edit Order Of Landing Page</h3>
                </div>

             </div>
          </div>
       </div>

        <div class="row">
                   <div class="col-xl-8 col-lg-10 col-md-10 mx-auto mx-flex">
                      <div class="card">
                         <div class="card-body">
                         <form method="post" action="/update/landing" enctype="multipart/form-data">
        {{ csrf_field() }}
                              <div class="row">
                                  <div class="col-xl-6">
                                      <div class="form-group">
                                          <label>Order ID</label>
                                          <input disabled type="number" name="orderId"  value="{{$result->orderId}}" class="form-control" value="56456">
                                      </div>
                                  </div>

                                  <div class="col-xl-6">
                                      <div class="form-group">
                                          <label>Landing Page Name</label>
                                          <input type="hidden" name="id" value="{{$result->id}}" >
                                          <input type="text" required name="name" value="{{$result->name}}" class="form-control" value="my landing">
                                      </div>
                                  </div>

                                  <div class="col-xl-12">
                                      <div class="form-group">
                                          <label>Description</label>
                                          <textarea required class="form-control" name="description"> {{$result->description}} </textarea>
                                      </div>
                                  </div>

                                  <div class="col-xl-6">
                                      <div class="form-group">
                                          <label>Date</label>
                                           <div class="input-group">
                                        <input type="text" required class="form-control" name="date" id="date"  value="{{date('m-d-Y', strtotime($result->date)) }}">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="dripicons-calendar"></i></span>
                                        </div>
                                    </div>
                                      </div>
                                  </div>


                                  <div class="col-xl-6">
                                      <div class="form-group">
                                          <label>URL</label>
                                          <input type="text" required name="url" class="form-control" value="{{$result->url}}">
                                      </div>
                                  </div>


                                  <div class="col-xl-6">
                                      <div class="form-group">
                                          <label>Cost</label>
                                          <input type="number" min="1" required name="cost" class="form-control" value="{{$result->cost}}">
                                      </div>
                                  </div>

                                  <div class="col-xl-6">
                                      <div class="form-group">
                                          <label>Status</label>
                                           <select class="form-control" name="status" required>
                                              <option disabled=""    >Select status</option>
                                              <option value="completed"{{$result->status=='completed'?'selected':''}}>Completed</option>
                                              <option value="pending" {{$result->status=='pending'?'selected':''}}>Pending</option>

                                          </select>
                                      </div>
                                  </div>
                                  <div class="col-xl-12">
                                    <div class="form-group ft-none">
                                    <input type="submit" class="btn btn-grey p-btn float-right" name="" value="Cancel">

                                    <input type="submit" class="btn btn-primary float-right" name="" value="Update">


                                    </div>

                                  </div>

                              </div>

                          </form>

                         </div>
                      </div>
                   </div>
                </div>

       <!--end col-->
    </div>
<!-- container -->
@include('layouts.footer')
<!--end footer-->
</div>



<!-- end page content -->
@endsection



@push('js')
    <script>
        $(function() {
            $('input[name="date"]').daterangepicker({
                opens: 'left',
                singleDatePicker: true,
                showDropdowns: true,
                maxDate: moment(),
                startDate: moment()
            }, function(start, end, label) {
                $("#date").val(start.format('MM/DD/YYYY'));
            });
        });
    </script>
@endpush

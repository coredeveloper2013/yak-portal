@extends('layouts.app')

@section('css')
    <style>
        .invalid-feedback {
            display: block !important;
        }
    </style>
@endsection

@section('content')


    <!-- Page Content-->
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="wlcm-panel">
                        <div class="welcome-text no-sub text-center">
                        </div>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xl-8 col-lg-10 col-md-10 mx-auto mx-flex">
                    <div class="card">
                        <div class="card-body">

                            <form method="post" action="{{route('landing.order')}}" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-xl-6">
                                        <div class="form-group">
                                            <label>Landing Page Name<span class="req">*</span></label>
                                            <input type="text" name="name" value="{{old('name')}}" class="form-control">
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-xl-6">
                                        <div class="form-group">
                                            <label>Upload Logo<span class="req">*</span></label>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="customFile"
                                                       name="logo">
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                            </div>
                                            @error('logo')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-xl-12">
                                        <div class="form-group">
                                            <label>Description<span class="req">*</span></label>
                                            <textarea class="form-control"
                                                      name="description">{{old('description')}}</textarea>
                                            @error('description')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-xl-6">
                                        <div class="form-group">
                                            <label>Date</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" value="{{old('date')}}"
                                                       name="date" id="date" disabled>
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i
                                                            class="dripicons-calendar"></i></span>
                                                </div>
                                                @error('date')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-xl-6">
                                        <div class="form-group">
                                            <label>URL<span class="req">*</span></label>
                                            <input type="text" name="url" value="{{old('url')}}" class="form-control"
                                                   placeholder="https://www.example.com">
                                            @error('url')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                        </div>
                                    </div>


                                    <div class="col-xl-6">
                                        <div class="form-group">
                                            <label>Cost<span class="req">*</span></label>
                                            <input type="number" min="1" value="{{old('cost')}}" name="cost"
                                                   class="form-control">
                                            @error('cost')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror  </div>
                                    </div>

                                    <div class="col-xl-6">
                                        <div class="form-group">
                                            <label>Status<span class="req">*</span></label>
                                            <select class="form-control" name="status">
                                                <option disabled="" selected>Select status</option>
                                                <option
                                                    value="completed" {{ (old('status') === "completed" ? "selected" : "") }}>
                                                    Completed
                                                </option>
                                                <option
                                                    value="pending" {{ (old('status') === "pending" ? "selected" : "") }}>
                                                    Pending
                                                </option>
                                            </select>
                                            @error('status')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                        </div>
                                    </div>


                                    <div class="col-xl-12">
                                        <div class="form-group">
                                            <label>What is this landing page trying to sell?</label>
                                            <textarea class="form-control" name="trying_to_sell">{{old('trying_to_sell')}}</textarea>
                                            @error('trying_to_sell')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-xl-12">
                                        <div class="form-group">
                                            <label>What will the customer get by filling in the form?</label>
                                            <textarea class="form-control" name="customer_benefit">{{old('customer_benefit')}}</textarea>
                                            @error('customer_benefit')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                        </div>
                                    </div>


                                    <div class="col-xl-12">
                                        <div class="form-group">
                                            <label>What are two questions do you want to ask the leads?</label>

                                            <input type="text" class="form-control ques" placeholder="Question 1"
                                                   name="leads_questions[]" value="{{old('leads_questions.0')}}">
                                            <input type="text" class="form-control" placeholder="Question 2" name="leads_questions[]"
                                                   value="{{old('leads_questions.1')}}">
                                            @error('leads_questions')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-xl-12">
                                        <div class="form-group">
                                            <label>What contact details do you want to ask the leads for?</label>

                                            <div class="checkbox checkbox-primary">
                                                <input id="checkbox1" type="checkbox" value="name"
                                                       name="leads_contact_details[]"
                                                    {{ (is_array(old('leads_contact_details')) and
                                                    in_array('name', old('leads_contact_details'))) ? 'checked' : '' }}>
                                                <label for="checkbox1">Name</label>
                                            </div>

                                            <div class="checkbox checkbox-primary">
                                                <input id="checkbox2" type="checkbox" value="postcode"
                                                       name="leads_contact_details[]"
                                                    {{ (is_array(old('leads_contact_details')) and
                                                        in_array('postcode', old('leads_contact_details'))) ? 'checked' : '' }}>
                                                <label for="checkbox2">Postcode</label>
                                            </div>

                                            <div class="checkbox checkbox-primary">
                                                <input id="checkbox3" type="checkbox" value="phone_number"
                                                       name="leads_contact_details[]"
                                                    {{ (is_array(old('leads_contact_details')) and
                                                            in_array('phone_number', old('leads_contact_details'))) ? 'checked' : '' }}>
                                                <label for="checkbox3">Phone Number</label>
                                            </div>

                                            <div class="checkbox checkbox-primary">
                                                <input id="checkbox4" type="checkbox"
                                                       value="email_address"
                                                       name="leads_contact_details[]"
                                                    {{ (is_array(old('leads_contact_details')) and
                                                            in_array('email_address', old('leads_contact_details'))) ? 'checked' : '' }}>
                                                <label for="checkbox4">Email Address</label>
                                            </div>
                                            @error('leads_contact_details')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror

                                        </div>
                                    </div>


                                    <div class="col-xl-12">
                                        <div class="form-group">
                                            <label>Do you have any review pages?</label>
                                            <div class="row review_social">
                                                <div class="checkbox checkbox-primary col-xl-4">
                                                    <input id="checkbox5" type="checkbox" class="review_check"
                                                           value="google"
                                                        {{ (is_array(old('review_pages')) and
                                                                in_array('google', old('review_pages'))) ? 'checked' : '' }}>
                                                    <label for="checkbox5">Google</label>
                                                </div>
                                                <div class="col-xl-8 review_input_block d-none">
                                                    <div class="form-group">
                                                        <input type="text" name="review_pages[google]" class="form-control review_text">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row review_social">
                                                <div class="checkbox checkbox-primary col-xl-4">
                                                    <input id="checkbox6" type="checkbox" class="review_check"
                                                           value="trustpilot"
                                                        {{ (is_array(old('review_pages')) and
                                                                    in_array('trustpilot', old('review_pages'))) ? 'checked' : '' }}>
                                                    <label for="checkbox6">Trustpilot</label>
                                                </div>
                                                <div class="col-xl-8 review_input_block d-none">
                                                    <div class="form-group">
                                                        <input type="text" name="review_pages[trustpilot]" class="form-control review_text">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row review_social">
                                                <div class="checkbox checkbox-primary col-xl-4">
                                                    <input id="checkbox7" type="checkbox" class="review_check"
                                                           value="facebook"
                                                        {{ (is_array(old('review_pages')) and
                                                                        in_array('facebook', old('review_pages'))) ? 'checked' : '' }}>
                                                    <label for="checkbox7">Facebook</label>
                                                </div>
                                                <div class="col-xl-8 review_input_block d-none">
                                                    <div class="form-group">
                                                        <input type="text" name="review_pages[facebook]" class="form-control review_text">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row review_social">
                                                <div class="checkbox checkbox-primary col-xl-4">
                                                    <input id="checkbox8"
                                                           type="checkbox" class="review_check"
                                                           value="mybuilder"
                                                        {{ (is_array(old('review_pages')) and
                                                                            in_array('mybuilder', old('review_pages'))) ? 'checked' : '' }}>
                                                    <label for="checkbox8">Mybuilder</label>
                                                </div>
                                                <div class="col-xl-8 review_input_block d-none">
                                                    <div class="form-group">
                                                        <input type="text" name="review_pages[mybuilder]" class="form-control review_text">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row review_social">
                                                <div class="checkbox checkbox-primary col-xl-4">
                                                    <input id="checkbox9" type="checkbox" class="review_check"
                                                           value="yell"
                                                        {{ (is_array(old('review_pages')) and
                                                                                in_array('yell', old('review_pages'))) ? 'checked' : '' }}>
                                                    <label for="checkbox9">Yell</label>
                                                </div>
                                                <div class="col-xl-8 review_input_block d-none">
                                                    <div class="form-group">
                                                        <input type="text" name="review_pages[yell]" class="form-control review_text">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row review_social">
                                                <div class="checkbox checkbox-primary col-xl-4">
                                                    <input id="checkbox10" type="checkbox" class="review_check"
                                                           value="others"
                                                        {{ (is_array(old('review_pages')) and
                                                                                in_array('others', old('review_pages'))) ? 'checked' : '' }}>
                                                    <label for="checkbox10">Others</label>
                                                </div>
                                                <div class="col-xl-8 review_input_block d-none">
                                                    <div class="form-group">
                                                        <input type="text" name="review_pages[others]" class="form-control review_text">
                                                    </div>
                                                </div>
                                            </div>

                                            @error('review_pages')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                        </div>
                                    </div>


                                    <div class="col-xl-12">
                                        <div class="form-group">
                                            <label>What are 3 reasons someone should choose you?</label>

                                            <input type="text" class="form-control ques" placeholder="Reason 1" name="reasons[]"
                                            value="{{old('reasons.0')}}">
                                            @error('reasons.0')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror

                                            <input type="text" class="form-control ques" placeholder="Reason 2" name="reasons[]"
                                                   value="{{old('reasons.1')}}">
                                            @error('reasons.1')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror

                                            <input type="text" class="form-control" placeholder="Reason 3" name="reasons[]"
                                                   value="{{old('reasons.2')}}">
                                            @error('reasons.2')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror

                                        </div>
                                    </div>


                                    <div class="col-xl-12">
                                        <div class="form-group">
                                            <label>What are 2 reviews we can add to the landing page?</label>

                                            <input type="text" class="form-control ques" placeholder="Review 1" name="reviews[]"
                                            value="{{old('reviews.0')}}">
                                            @error('reviews.0')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror

                                            <input type="text" class="form-control ques" placeholder="Review 2" name="reviews[]"
                                                   value="{{old('reviews.1')}}">

                                            @error('reviews.1')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror

                                        </div>
                                    </div>


                                    <div class="col-xl-12">
                                        <div class="form-group">
                                            <label>What are 4 frequently asked questions?</label>

                                            <input type="text" class="form-control ques" placeholder="Question 1"
                                                   name="landing_page[0][questions]"
                                            value="{{old('landing_page.0.questions')}}">
                                            @error('landing_page.0.questions')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror

                                            <input type="text" class="form-control ques" placeholder="Answer" name="landing_page[0][answers]"
                                                   value="{{old('landing_page.0.answers')}}">
                                            @error('landing_page.0.answers')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                            <hr>

                                            <input type="text" class="form-control ques" placeholder="Question 2"
                                                   name="landing_page[1][questions]" value="{{old('landing_page.1.questions')}}">
                                            @error('landing_page.1.questions')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror

                                            <input type="text" class="form-control ques" placeholder="Answer" name="landing_page[1][answers]"
                                                   value="{{old('landing_page.1.answers')}}">
                                            @error('landing_page.1.answers')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                            <hr>


                                            <input type="text" class="form-control ques" placeholder="Question 3"
                                                   name="landing_page[2][questions]" value="{{old('landing_page.2.questions')}}">

                                            @error('landing_page.2.questions')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror

                                            <input type="text" class="form-control ques" placeholder="Answer" name="landing_page[2][answers]"
                                                   value="{{old('landing_page.2.answers')}}">
                                            @error('landing_page.2.answers')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                            <hr>

                                            <input type="text" class="form-control ques" placeholder="Question 4"
                                                   name="landing_page[3][questions]" value="{{old('landing_page.3.questions')}}">
                                            @error('landing_page.3.questions')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror

                                            <input type="text" class="form-control ques" placeholder="Answer" name="landing_page[3][answers]"
                                                   value="{{old('landing_page.3.answers')}}">
                                            @error('landing_page.3.answers')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                            <hr>
                                        </div>
                                    </div>


                                    <div class="col-xl-12">
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-primary float-right" value="Order Now">
                                        </div>

                                    </div>

                                </div>

                            </form>

                        </div>
                    </div>
                </div>
            </div>

            <!--end col-->
        </div>
        <!-- container -->
    @include('layouts.footer')
    <!--end footer-->
    </div>



    <!-- end page content -->
@endsection



@push('js')
    <script>
        $(function () {
            $('input[name="date"]').daterangepicker({
                opens: 'left',
                singleDatePicker: true,
                showDropdowns: true,
                maxDate: moment(),
                minDate: moment(),
                startDate: moment()
            }, function (start, end, label) {
                $("#date").val(start.format('MM/DD/YYYY'));
            });

            $('body').on('click', '.review_check', function(event) {
                var that = $(this);
                var parent = that.parents('.review_social');
                var input = parent.find('.review_text');
                var inputBlock = input.parents('div.review_input_block');
                if (that.is(":checked")) {
                    inputBlock.removeClass('d-none');
                } else {
                    input.val('');
                    inputBlock.addClass('d-none');
                }
            });
        });

    </script>
@endpush

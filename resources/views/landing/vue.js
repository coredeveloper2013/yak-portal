new Vue({
    el: '#landingPage',
    mounted() {
        this.getUserLandingPages(false);
        var context = this;
        $('input[name="daterange"]').daterangepicker({
            opens: "center",
            maxDate: moment(),
            autoUpdateInput: false,
            orientation: "auto",
            locale: {
                cancelLabel: "Clear"
            }
        });
        $('input[name="daterange"]').on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("MM/DD/YYYY") + " - " + picker.endDate.format("MM/DD/YYYY"));
            context.filter.selectedStartDate = picker.startDate.format("MM/DD/YYYY");
            context.filter.selectedEndDate = picker.endDate.format("MM/DD/YYYY");
            context.getUserLandingPages(false);
        });
        $('input[name="daterange"]').on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
            context.filter.selectedStartDate = "";
            context.filter.selectedEndDate = "";
            context.getUserLandingPages(false);
        });

    },
    data: {
        landingPages: [],
        landingPagesMeta: {},
        filter: {
            selectedStartDate: '',
            selectedEndDate: '',
            status: '',
        },
        ticketSubject: '',
        isBusy: false,
        disableSubmitBtn: false,
        redirectUrl : appURL + '/support-ticket',
        getLandingPagesUrl : App_url + '/data',
        createTicketUrl : apiURL+'/support-ticket/add/ticket',
    },
    watch: {
        'landingPagesMeta.current_page': function (val) {
            this.loadPaginatedData();
        }
    },
    methods: {
        loadPaginatedData: function () {
            this.getUserLandingPages(this.getLandingPagesUrl + '?page=' + this.landingPagesMeta.current_page);
        },
        getUserLandingPages: function (url = false) {
            this.isBusy = true;
            if(url == false){
                url = this.getLandingPagesUrl;
            }
            axios.post(url, {filter: this.filter})
                .then(response => {
                    if(response.data.status == 'success'){
                        this.isBusy = false;
                        this.landingPages = response.data.data;
                        this.landingPagesMeta = response.data.meta;
                    }
                }).catch(error => {
                console.log(error);
                this.isBusy = false;
            });
        },
        resetFilter: function(){
            this.filter = {
                selectedStartDate : '',
                selectedEndDate : '',
                status : ''
            };
            $("#daterange").val('');
            this.getUserLandingPages(false);
        },
        showTicketModal: function(landVal) {
            this.ticketSubject = "Edit request of Landing Page ("+landVal+")";
            $('#addTicketModal').modal('show');
        },
        createTicket: function() {
            this.disableSubmitBtn = true;
            let staffIds = [];
            _.forEach(this.staff_id, function(value, key){
                staffIds.push(value.id);
            });
            console.log(staffIds);
            let formData = new FormData($('#add_ticket')[0]);
            //formData.append('subject', this.ticketSubject);
            //formData.append('question', this.ticketSubject);
            //formData.append('file', this.ticketSubject);

            let headersArr = {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            };

            axios.post( this.createTicketUrl,
                formData,
                headersArr
            ).then(response => {
                this.disableSubmitBtn = false;
                console.log(response);
                let res = response.data;
                var status = res.status.substr(0,1).toUpperCase()+res.status.substr(1);
                if (res.status == 'Success') {
                    Swal.fire({
                        title: status,
                        text: res.msg,
                        icon: 'success',
                        onAfterClose: () => {
                            //window.location = this.redirectUrl;
                            location.reload();
                        }
                    });
                } else {
                    Swal.fire({
                        title: status,
                        text: res.msg,
                        icon: 'error'
                    });
                }
            }).catch(error => {
                console.log(error);
                this.disableSubmitBtn = false;
            });
        },
        numberWithComma: function(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        },
        Filtered(filteredItems) {
            // Trigger pagination to update the number of buttons/pages due to filtering
            this.landingPagesMeta.totalRows = filteredItems.length;
            this.landingPagesMeta.currentPage = 1;
        },
    }
});

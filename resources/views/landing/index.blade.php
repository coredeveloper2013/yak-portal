@extends('layouts.app')


@push('js')
    <script>
        var App_url = '{!! url()->current() !!}';
        var appURL = '{!! url('') !!}';
        var apiURL = '{!! url('/api/') !!}';
    </script>
    {{--  Inject vue js files  --}}
    <script src="{{ asset('js/app-vue.js') }}"></script>
    <script src="{{ asset('js/landing/vue.js') }}"></script>
    <script>
        $( document ).ready(function() {
            $(function () {
              $('[data-toggle="tooltip"]').tooltip()
            })
        })
    </script>
@endpush

@section('content')
    <!-- Page Content-->
    <div class="page-content" id="landingPage">

        <div class="container-fluid">
            <div class="row" style="">
                <div class="col-sm-12 custom-space01">
                    <div class="page-title-box">
                        <div class="float-right">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Main</a></li>
                                <li class="breadcrumb-item active">My Landing Pages</li>
                            </ol>
                        </div>
                    </div>
                    <!--end page-title-box-->
                </div>
                <!--end col-->
            </div>

            <div class="custom-space01 clearfix">
              <div class="form-group filter-f dropdown-arrow float-left mr-3">
                  <input type="text" name="daterange" id="daterange" placeholder="Filter by date"
                         class="form-control"/>
              </div>
              <div class="form-group filter-c float-left">
                        <select class="form form-control form-control-sm" v-model="filter.status"
                                @change="getUserLandingPages(false)">
                            <option value="" disabled>Select Status</option>
                            <option value="completed">Completed</option>
                            <option value="pending">Pending</option>
                        </select>
                     </div>
                     <div class="form-group reset-c float-left m-0">
                         <button type="button" class="btn" @click="resetFilter"><i class="fas fa-undo"></i></button>
                     </div>
            </div>

            <div class="row custom-space01 mt-3" v-if="landingPages.length">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12" v-for="row in landingPages">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-title border-bottom pb-2 clearfix">
                              <h4 class="m-0 pr-4 float-left">@{{row.name}}</h4>
                              <span class="text-success mt-0 float-right font-small"> #@{{row.id}} </span>

                            </div>

                            <p class="txt-grey text-limit">@{{row.description}}</p>
                            <div class="row text-group mb-1">
                                <div class="col-lg-4 pr-0">
                                    <span class="txt-black-label text-limit">URL</span>
                                </div>
                                <div class="col-lg-8">
                                    <span class="txt-grey text-limit">@{{row.url}}</span>
                                </div>
                            </div>

                            <div class="footer-card-panel request-landing landding-btn-resp">
                                <span class="mt-0 mt-2 badge badge-boxed  badge-soft-success tm-2 landing-btn-s" v-if="row.status == 'COMPLETED'">
                                    @{{row.status}}
                                </span>
                                <span class="mt-0 mt-2 badge badge-boxed badge-resp badge-soft-danger tm-2 landding-btn-m"  v-else>
                                    @{{row.status}}
                                </span>
                                <span class="edit-btn-style float-right btn grey-btn"  @click="showTicketModal(row.name+' #'+row.id)"><i class="fas fa-chart-bar"></i>  Request Landing Page Edit</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="custom-space01 text-center" v-else>
                <!-- <i class="far fa-frown"></i> -->
                <p style="color: grey" class="text-c12 white-box">Sorry, no landing pages found.</p>
            </div>
            <div class="custom-space01">
                <div class="col-xl-6 col-lg-6 col-md-6 float-left">
                    <p class="mb-0 p-1 small mt-1 float-left">Showing @{{ landingPagesMeta.from }} to @{{
                        landingPagesMeta.to }} of @{{ landingPagesMeta.total }} records | Per Page: @{{
                        landingPagesMeta.per_page }} | Current Page: @{{ landingPagesMeta.current_page }}</p>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 float-right landing-edit t012">
                    <b-pagination class="float-right  t012" size="sm" :total-rows="landingPagesMeta.total"
                                  v-model="landingPagesMeta.current_page" :per-page="landingPagesMeta.per_page"
                                  first-text="First" prev-text="Pervious" next-text="Next" last-text="Last"
                                  ellipsis-text="More" variant="danger">
                    </b-pagination>
                </div>
            </div>{{-- End Row --}}
        </div>
        <!-- container -->
        <div class="modal fade" id="addTicketModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4>Request Landing Page Edit </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: red">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form enctype="multipart/form-data" id="add_ticket">
                            @csrf
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="form-group">
                                        <label>Subject <span class="req">*</span></label>
                                        <input type="text" class="form-control disabled" placeholder="Enter Subject" name="subject" v-model="ticketSubject" readonly>
                                    </div>
                                </div>

                                <div class="col-xl-12">
                                    <div class="form-group">
                                        <label>What would you like us to change? <span class="req">*</span></label>
                                        <textarea rows="6" class="form-control" name="question"></textarea>
                                    </div>
                                </div>

                                <div class="col-xl-12">
                                    <div class="form-group">
                                        <label>Upload File<span class="req"></span></label>
                                        <div class="uploadOuter">
                                            <span class="dragBox" >
                                                <input type="file" class="up-file" id="customFile" name="file">
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xl-12">
                                    <div class="form-group">
                                        <input type="button" @click="createTicket()" class="btn btn-primary float-right" :disabled="disableSubmitBtn" value="Submit Request">
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        @include('layouts.footer')
        <!--end footer-->
    </div>



    <!-- end page content -->
@endsection



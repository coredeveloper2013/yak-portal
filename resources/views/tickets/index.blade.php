@extends('layouts.app')
@section('css')
<link href='https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>
<style>
   .paginate_button{
   padding:0px !important;
   }
   .card-body {
    min-height: calc(100vh - 200px);
   }
   .inbox-details {
    max-height: 452px;
    height: auto;
   }
   .inbox-msg {
    max-height: calc(100vh - 244px);
    height: auto;
   }
   .add-msg {
    position: absolute;
    bottom: 10px;
    left: 1.25rem;
    right: 1.25rem;
   }

</style>
@endsection
@section('content')
<div class="page-content grey-bg">
   <div class="container-fluid">

      <div class="row">
         <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4">
            <div class="btns-support-r mt-3">
               <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#ticket_modal">
               <i class="fas fa-plus"></i> Create Ticket
               </button>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-xl-3 col-lg-4 col-md-4 col-sm-12">
            <div class="side-inbox">
               <div class="head-inbox">
                  <i class="fas fa-bars"></i> INBOX
                  <span class="msg-count">1-10 of 45</span>
               </div>
               <div class="inbox-msg">
                  <ul class="tickets-body">
                      <span class="text-center d-block">No Ticket found.</span>
                  </ul>
               </div>
            </div>
         </div>
         <div class="col-xl-9 col-lg-8 col-md-8 col-sm-12">
            <div class="card">
               <div class="card-body">
                  <div class="inbox-details">
                     <ul class="comments-body">
                         <span class="text-center d-block">No Chat found.</span>
                     </ul>
                  </div>
                  <div class="add-msg">
                     <div class="form-group">
                        <textarea class="form-control" placeholder="Type your message here" id="comment_text"></textarea>
                     </div>
                     <div class="form-group">
                        <input type="button" name="" id="send_comment" class="btn btn-darkblue" value="Send">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      @include('layouts.footer')
   </div>
</div>

<div class="modal fade" id="ticket_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog" role="document">
       <div class="modal-content">
           <div class="modal-header">
               <h4>Create Ticket</h4>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: red">
                   <span aria-hidden="true">&times;</span>
               </button>
           </div>
           <div class="modal-body">
	           	<form method="post" action="" enctype="multipart/form-data" id="add_ticket">
	           		@csrf
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="form-group">
                                <label>Subject <span class="req">*</span></label>
                                <input type="text" class="form-control" placeholder="Enter Subject" name="subject" value="">
                            </div>
                        </div>

                        <div class="col-xl-12">
                            <div class="form-group">
                                <label>Question <span class="req">*</span></label>
                                <textarea class="form-control" rows="6" name="question"></textarea>
                            </div>
                        </div>

                        <div class="col-xl-12">
                            <div class="form-group">
                                <label>Upload File<span class="req"></span></label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="customFile"
                                           name="file">
                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-12">
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary float-right" value="Create">
                            </div>

                        </div>

                    </div>
	            </form>
           </div>
       </div>
   </div>
</div>
@endsection

@push('js')
	<script type="text/javascript">
      var App_url = '{!! url()->current() !!}';
      var appUrl = '{!! url('') !!}';
      var apiURL = '{!! url('/api/') !!}';
      var user_id = '{!! Auth::user()->id !!}';
      var atcive_ticket = 0;

    $(function() {
        loadTickets(0, '');

        $('body').on('click', '.filter', function(event) {
            event.preventDefault();
            var that = $(this);
            var status = that.data('status');
            loadTickets(0, status);
        });

        $('body').on('click', '.load_comments', function(event) {
            event.preventDefault();
            var that = $(this);
            var ticketId = that.data('id');
            atcive_ticket = ticketId;
            $('.load_comments').removeClass('active');
            that.addClass('active');
            loadComments(ticketId);
        });



        var flag = false;
        $('body').on('click', '#send_comment', function(event) {
            event.preventDefault();
            var that = $(this);
            var text = $('#comment_text').val();
            if (text == '' || flag == true) {
               return  false;
            }

            flag = true;

            var ticketId = atcive_ticket;
            var csrf = $('meta[name="csrf-token"]').attr("content");
            $.ajax({
               url: apiURL+'/support-ticket/add/comment',
               type: 'POST',
               dataType: 'json',
               data: {
                  id: ticketId,
                  text: text
               },
               headers: {
                  "X-CSRF-TOKEN": csrf,
                  Accpet: "applicationjson"
               },
            }).always(function(res) {
                  flag = false;
                let status = res.status.substr(0,1).toUpperCase()+res.status.substr(1);
                  Swal.fire({
                      title: status,
                      text: res.msg,
                      icon: res.status,
                  });

                  if (res.status == 'Success' ) {
                     $('#comment_text').val('');
                     var html = makeComments(res.data);
                     $('.comments-body').append(html);
                  }
            });
        });

        var flag1 = false;
        $('body').on('submit', '#add_ticket', function(event) {
            event.preventDefault();
            var that = $(this);

            if (flag1 == true) {
               return  false;
            }

            var data = new FormData(that[0]);
            var csrf = $('meta[name="csrf-token"]').attr("content");
            $.ajax({
              url: apiURL+'/support-ticket/add/ticket',
              type: 'POST',
              dataType: 'json',
              data: data,
              processData: false,
              contentType: false,
               headers: {
                  "X-CSRF-TOKEN": csrf,
                  Accpet: "applicationjson"
               },
            }).always(function(res) {
                  flag1 = false;
                let status = res.status.substr(0,1).toUpperCase()+res.status.substr(1);
                  Swal.fire({
                      title: status,
                      text: res.msg,
                      icon: res.status,
                      onAfterClose: () => {
                         if (res.status == 'Success' ) {
                            that[0].reset();
                           location.reload();
                         }
                      }
                  });
            });
        });

      });

    function loadTickets(offset, status) {
        if (typeof offset == 'undefined') {
            offset = 0;
        }

        if (typeof status == 'undefined') {
            status = '';
        }
        var csrf = $('meta[name="csrf-token"]').attr("content");
        $.ajax({
              url: apiURL+'/support-ticket',
              type: 'POST',
              dataType: 'json',
              data: {
               offset: offset,
               status: status
            },
              headers: {
                  "X-CSRF-TOKEN": csrf,
                  Accpet: "applicationjson"
              },
        }).always(function(res) {
               if (res.status == 'success' ) {
                  if (res.data.show_comments == 'Y') {
                     var html = ``;
                     $.each(res.data.tickets, function( index, value ) {
                        html += makeTickets(value);
                     });
                     $('.tickets-body').html(html);

                     setTimeout(function() {
                        var fisrtTicketBlock = $('.tickets-body').find('li:nth-child(1) a');
                        fisrtTicketBlock.trigger('click');
                     }, 500)
                  } else {
                     $('.comments-body').html('<span class="text-center d-block">No Chat found.</span>');
                     $('.tickets-body').html('<span class="text-center d-block">No Ticket found.</span>');
                  }
               } else {
                  $('.tickets-body').html('<span class="text-center d-block">No Ticket found.</span>');
               }
        });
    }

	function makeTickets(tickets) {
         var html =  `<li>
                        <a href="javascript:;" class="load_comments ticket_`+tickets.id+`"  data-id="`+tickets.id+`" data-un_assigned="`+tickets.unassigned+`">
                           <div class="name-time1-o">`+tickets.customer_data.name+`  <span class="time1-o">`+tickets.human_date+`</span></div>
                           <div class="sub-msg"></div>
                           <p>`+tickets.subject+`</p>
                        </a>
                     </li>`;
         return html;
	}

	function loadComments(tId) {

         var un_assigned = $('.ticket_'+tId).data('un_assigned');
         if (un_assigned == 'Y') {
            $('.assign_block').removeClass('d-none');
         } else {
            $('.assign_block').addClass('d-none');
         }

         var csrf = $('meta[name="csrf-token"]').attr("content");
         $.ajax({
              url: apiURL+'/support-ticket/comments',
              type: 'POST',
              dataType: 'json',
              data: {id: tId},
              headers: {
                  "X-CSRF-TOKEN": csrf,
                  Accpet: "applicationjson"
              },
          }).always(function(res) {
               if (res.status == 'success' ) {
                  if (res.data.show_comments == 'Y') {
                     var html = ``;
                     $.each(res.data.comments, function( index, value ) {
                        html += makeComments(value);
                     });
                     $('.comments-body').html(html);
                  } else {
                     $('.comments-body').html('<span class="text-center d-block">No Chat found.</span>');
                  }
               } else {
                  $('.comments-body').html('<span class="text-center d-block">No Chat found.</span>');
               }
          });
	}

	function makeComments(comment) {
         var html =  `<li>
                        <div class="media">
                           <div class="media-left">`;
                              if (comment.user_image != '' && comment.user_image != null) {
                                 html += `<img src="{{asset('storage/avatars/')}}/`+comment.user_image+`" alt="user" class="rounded-circle thumb-md">`
                              } else {
                                 html += `<img src="{{asset('staff/images/user-1.jpg')}}" alt="user" class="rounded-circle thumb-md">`
                              }
                              html += `<span class="round-10 bg-success"></span>
                           </div>
                           <div class="media-body">
                              <div>`;
                                 if (comment.user_id == user_id) {
                                    html += `<h6>You</h6>`;
                                 } else {
                                    html += `<h6>`+comment.user_name+`</h6>`;
                                 }
                                 html += `<div class="dropdown">
                                    <a class="dropdown-toggle" type="button">
                                       `+comment.human_date+`
                                    </a>
                                 </div>
                              </div>
                        </div>
                     </div>
                     <p>
                        `+comment.chat_text+`
                     </p>
                  </li>`;
         return html;
	}
   </script>
@endpush

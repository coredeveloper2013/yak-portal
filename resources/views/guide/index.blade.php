@extends('layouts.app')

@section('content')

<!-- Page Content-->
<div class="page-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="float-right">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Main</a></li>
                            <li class="breadcrumb-item active">Guide</li>
                        </ol>
                    </div>
                </div><!--end page-title-box-->
            </div><!--end col-->
        </div>
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 p-20 contain-box">
            <div class="">
                <div class="row al-boxes" id="listG" >
                   <!--  @if(! $guides->isEmpty())
                        @foreach($guides as $guide)
                            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                                <div class="guide-box">
                                    <img src="{{asset($guide->image)}}">
                                    <div class="text-guide">
                                        <h4>{{$guide->title}}</h4>
                                        <div class="update-notify">
                                            {{$guide->msg?:''}}
                                        </div>
                                        <p> {{$guide->descriptionF}}</p>
                                        <a href="javascript:;" onclick="showBg({!! $guide->id!!})" class="read-more">Read more</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="col-md-12 text-center">
                            <div class="alert alert-custom" role="alert">
                                No Record Found
                            </div>
                        </div>
                    @endif -->
                   

                </div> <!-- Modal -->
                    <div class="modal fade" id="guideModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body p-0">
                                    <div class="guide-box m-0">
                                        <img  id="card-img-tops" src="{{asset('/images/b2.jpg')}}">
                                        <div class="text-guide">
                                           <h4 id="BGtitle">Most beauituful place to see</h4>
                                           <div class="update-notify" id="BGupdate">Updated 20 seconds ago</div>
                                           <p  id="BGdescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod .</p>
                                        </div>
                                     </div>
                                </div>
                           </div>
                        </div>
                    </div>
                    <!-- Modal -->
            </div>
        </div>
    </div>
    <!-- container -->
    @include('layouts.footer')
    <!--end footer-->
 </div>
<!-- end page content -->
@endsection
 
<script>
    var GUIDES='{!! $guides !!}';
   var appURL = '{!! url('/api/') !!}';
    
</script>
{{--  Inject vue js files  --}} 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="{{ asset('js/cmn/cmn.js') }}"></script>
<script src="{{ asset('staff/js/cguide.js') }}"></script>
 


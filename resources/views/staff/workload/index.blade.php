@extends('staff.layouts.app')

@section('staff_css')

<style>



</style>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
@endsection

@section('staff_content')


   <div class="page-content compaign-sec-bg compaign-sec-bg-1">
      <div class="container-fluid workload-container">
         <div class="row">
            <div class="col-sm-12">
               <div class="page-title-box">
                  <h4 class="page-title">Campaign Overviews</h4>
               </div> 
               <!--end page-title-box-->
            </div>
         </div>
         <div class="row">
            <div class="col-12 col-sm-6 col-md-4 col-lg-4 pl-0 responsive-compaign">
               <div class="comapaign-box-main compaign-orange">Pending Agreement / Payment</div>
                  <div class="row mt-2">
                     <div class="col-6 col-md-6 col-sm-6 compaign-number">
                        <div class="border-r-left comapaign-sub-box compaign-orange">
                           {{isset($count_camp['pending']) ? $count_camp['pending'] : '0' }}
                        </div>
                     </div>

                     <div class="col-6 col-md-6 col-sm-6 compaign-value">
                        <div class="border-r-right comapaign-sub-box compaign-orange">
                           £
                           {{isset($sum_camp['pending']) ? floatval($sum_camp['pending']) : '0' }}
                        </div>
                     </div>
                  </div>
            </div>
            <!-- <div class="col-12 col-sm-6 col-md-4 col-lg-2 pl-0">
               <div class="comapaign-box-main compaign-orange">Pending Payment</div>
               <div class="row mt-2">
                  <div class="col-6 col-md-6 col-sm-6 compaign-number">
                     <div class="border-r-left comapaign-sub-box compaign-orange">
                        {{--{{isset($count_camp['pending']) ? $count_camp['pending'] : '0' }}--}}
                     </div>
                  </div>

                  <div class="col-6 col-md-6 col-sm-6 compaign-value">
                     <div class="border-r-right comapaign-sub-box compaign-orange">
                        {{--£--}}
                        {{--{{isset($sum_camp['pending']) ? floatval($sum_camp['pending']) : '0' }}--}}
                     </div>
                  </div>
               </div>
            </div> -->


            <div class="col-12 col-sm-6 col-md-4 col-lg-4 pl-0 responsive-compaign">
               <div class="comapaign-box-main compaign-blue">Awaiting Build</div>
                  <div class="row mt-2">
                     <div class="col-6 col-md-6 col-sm-6 compaign-number">
                        <div class="border-r-left comapaign-sub-box compaign-blue">
                           {{isset($count_camp['awaiting']) ? $count_camp['awaiting'] : '0' }}
                        </div>
                     </div>
                     <div class="col-6 col-md-6 col-sm-6 compaign-value">
                        <div class="border-r-right comapaign-sub-box compaign-blue">
                           £ {{isset($sum_camp['awaiting']) ? floatval($sum_camp['awaiting'] ): '0' }}
                        </div>
                     </div>
               </div>
            </div>

            <div class="col-12 col-sm-6 col-md-4 col-lg-4 pl-0 responsive-compaign">
               <div class="comapaign-box-main compaign-blue">Billing Details</div>
               <div class="row mt-2">
                  <div class="col-6 col-md-6 col-sm-6 compaign-number">
                     <div class="border-r-left comapaign-sub-box compaign-blue">
                        {{isset($count_camp['billing_detail']) ? $count_camp['billing_detail'] : '0' }}
                     </div>
                  </div>
                  <div class="col-6 col-md-6 col-sm-6 compaign-value">
                     <div class="border-r-right comapaign-sub-box compaign-blue">
                        £ {{isset($sum_camp['billing_detail']) ? floatval($sum_camp['billing_detail'] ): '0' }}
                     </div>
                  </div>
               </div>
            </div>
                     
            <div class="col-12 col-sm-6 col-md-4 col-lg-4 pl-0 responsive-compaign">
               <div class="comapaign-box-main compaign-blue">Live</div>
                  <div class="row mt-2">
                     <div class="col-6 col-md-6 col-sm-6 compaign-number">
                        <div class="border-r-left comapaign-sub-box compaign-blue">
                           {{isset($count_camp['live']) ? $count_camp['live'] : '0' }}
                        </div>
                     </div>
                     <div class="col-6 col-md-6 col-sm-6 compaign-value">
                        <div class="border-r-right comapaign-sub-box compaign-blue">
                           £ {{isset($sum_camp['live']) ? floatval( $sum_camp['live']) : '0' }}
                        </div>
                     </div>
                  </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-4 pl-0 responsive-compaign">
               <div class="comapaign-box-main compaign-blue">Completed</div>
                  <div class="row mt-2">
                     <div class="col-6 col-md-6 col-sm-6 compaign-number">
                        <div class="border-r-left comapaign-sub-box compaign-blue">
                           {{isset($count_camp['completed']) ? $count_camp['completed'] : '0' }}
                        </div>
                     </div>
                     <div class="col-6 col-md-6 col-sm-6 compaign-value">
                        <div class="border-r-right comapaign-sub-box compaign-blue">
                           £ {{isset($sum_camp['completed']) ? floatval($sum_camp['completed']) : '0' }}
                           </div>
                     </div>
                  </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-4 pl-0 responsive-compaign">
               <div class="comapaign-box-main compaign-green">Followed up</div>
                  <div class="row mt-2">
                     <div class="col-6 col-md-6 col-sm-6 compaign-number">
                        <div class="border-r-left comapaign-sub-box compaign-green">
                           {{isset($count_camp['followed_up']) ? $count_camp['followed_up'] : '0' }}
                        </div>
                     </div>
                     <div class="col-6 col-md-6 col-sm-6 compaign-value">
                        <div class="border-r-right comapaign-sub-box compaign-green">
                           £ {{isset($sum_camp['followed_up']) ? floatval($sum_camp['followed_up'] ): '0' }}
                        </div>
                     </div>
                  </div>
            </div>
         </div>
         <div class="row my-2">
            <div class="col-sm-12">
               <div class="page-title-box text-center">
                  <h4 class="assigned-main-heading">Assigned Staff</h4>
               </div>
            </div>
         </div>
         <div class="row">
            @foreach($staff_camp as $key => $value)

            <div class="col-md-2 pl-0 mb-2">
               <div class="assign-staff-box">
                  <p class="assign-name">{{$value->staff_name}}</p>
                  <p class="assign-number">{{$value->total}}</p>
               </div>
            </div>
            @endforeach

         </div>




               <!--end page-title-box-->
            </div>
         </div>



         <!-- start -->
         <!-- <div class="row"> -->
           <!-- <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
             <div class="card mt-3">
               <div class="card-body card-border">
                  <div class="single-line-w">
                     <h5 class="card-title graph-title">
                       New order (this week)
                     </h5>
                     <h4 class="ret-o">3,137 <span class="text-danger">0.7%&nbsp;<i class="fas fa-car"></i>&nbsp;</span><span class="lightgray">than last week</span></h4>
                     <div id="chart_div1"></div>
                  </div>
               </div>
             </div>
           </div> -->
           <!-- <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
             <div class="card mt-3">
               <div class="card-body card-border">
                  <div class="single-line-w">
                     <h5 class="card-title graph-title">
                       AVG. ORDER VALUE
                     </h5>
                     <h4 class="ret-o">3,137 <span class="text-danger">0.7%&nbsp;<i class="fas fa-car"></i>&nbsp;</span><span class="lightgray">than last week</span></h4>
                     <div id="chart_div2"></div>
                  </div>
               </div>
             </div>
           </div> -->
           <!-- <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
             <div class="card mt-3">
               <div class="card-body card-border">
                  <div class="single-line-w">
                     <h5 class="card-title graph-title">
                       Total Sales (this week)
                     </h5>
                     <h4 class="ret-o">3,137 <span class="text-success">0.7%&nbsp;<i class="fas fa-car"></i>&nbsp;</span><span class="lightgray">than last week</span></h4>
                     <div id="chart_div3"></div>
                  </div>
               </div>
             </div>
           </div>
         </div>  -->
         <!-- end -->
<!--
      </div> -->
    </div>







@endsection

@push('staff_js')

@endpush

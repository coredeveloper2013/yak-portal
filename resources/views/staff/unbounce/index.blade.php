@extends('staff.layouts.app')

@section('staff_content')
  <div class="page-content grey-bg compaign-sec-bg-1">
      <div class="container-fluid">
         <div class="row">
            <div class="col-sm-12">
               <div class="page-title-box">
                  <!-- <div class="float-right">
                     <ol class="breadcrumb">
                        <button type="button" class="btn btn-primary text-uppercase">
                           <i class="fas fa-file-pdf"></i>&nbsp;&nbsp; Generate PDF Report
                        </button>
                     </ol>
                  </div> -->
                  <h4 class="page-title"> </h4>
               </div>
               <!--end page-title-box-->
            </div>
         </div>
         <!--end col-->
         <div class="row top-search01">
            <div class="col-xl-9">
               <!-- <div class="float-left">
                  <div class="btn-group custom-btn-group" role="group" aria-label="Basic example">
                     <a href="#" class="btn btn-camp filter Factive " onclick="getCampaigns('all')">All Users</a>
                     <a href="#" class="btn btn-camp filter" onclick="getCampaigns('staff')">Staff Users </a>
                     <a href="#" class="btn btn-camp filter" onclick="getCampaigns('admin')">Admin Users</a>
                  </div>
               </div> -->
               <div class="float-left">
                <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#addNewModel">
                   Add New  </button>
               </div>
            </div>
            <div class="col-xl-3">
                @if(Auth::user()->type=="admin")
                    <div class="form-group">
                        <select class="form-control" name="user_search_id" onchange="showUserUnbounce(this.value)">
                            <option value="-1" {{$user_search_id == -1 ? 'selected' : ''}}  >All Users</option>
                            @foreach($members as $user)
                                <option {{$user_search_id == $user->id ? 'selected' : ''}} value="{{$user->id}}">{{$user->name}}</option>
                            @endforeach

                        </select>
                    </div>
                @else
                    <input type="hidden" class="form-control"  value="{{Auth::user()->id}}" name="user_search_id">
                @endif
            </div>
         </div>
         <div class="row">
            <div class="col-xl-12">
               <div class="table-responsive unbounce-resp-overflow">
                  <table class="table text-center td-block tableData">
                     <thead>
                        <tr class="th-head">
                           <th># </th>
                           <th>Campaign Name</th>
                           <!-- <th>UserName</th> -->
                           <th>Unbounce ID</th>
                           <th>Created on</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                       @foreach($unbounce as $key=>$value)
                       <tr>
                           <td>{{($key+1)}}</td>
                           <td>{{$value->name ? $value->name : 'New Bussiness Lead'}}</td>
                           <td>{{$value->unbounce_id}}</td>
                           <td>{{$value->created}}</td>
                           <td>
                               <a href="javascript:;" onclick="deleteUnbounce({{$value->id}})" class="text-danger float-right remove_block">
                                   <i class="far fa-trash-alt"></i>
                               </a>
                           </td>
                      </tr>
                      @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>


   <div class="modal fade" id="addNewModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h5>Link Campaigns</h5>
               <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <form id="addCampaign1">
               <div class="modal-body">
                  <div class="form-group">
                     <label for="name">Unbounce Page ID:</label>
                     <input type="text" class="form-control"   name="unbounce_id">
                  </div>
                  <div class="form-group">
                     <label for="is_new_bussiness_lead">Add in New Business Hub</label>
                     <div class="custom-control custom-switch dp-inline pl-5">
                         <input type="checkbox" name="is_new_bussiness_lead" value="1" class="custom-control-input" id="is_new_bussiness_lead">
                         <label class="custom-control-label" for="is_new_bussiness_lead"></label>
                     </div>
                  </div>
                   @if(Auth::user()->type=="admin")
                       <div class="form-group">
                           <label for="userId" class="">User:</label>
                           <select class="form-control" name="user_id" onchange="showUserCampaign(this.value)">
                                   <option value="" disabled selected >Select </option>
                                   @foreach($members as $user)
                                       <option value="{{$user->id}}">{{$user->name}}</option>
                                   @endforeach

                               </select>
                       </div>
                   @else
                       <input type="hidden" class="form-control"  value="{{Auth::user()->id}}" name="user_id">
                   @endif
                  <div class="form-group select_compaign_id">
                     <label for="landing_id">Campaigns:</label>
                     <select class="form-control" name="campaign" id="compaign_id" required="">
                          <option disabled="" selected="">Select Campaigns</option>
                            @foreach($campaigns as $value)
                            <option {{(Auth::user()->type=="admin") ? 'style="display: none;"' : ''}}  value="{{$value->id}}" data-user="{{$value->assigned_id}}">{{$value->name}}</option>
                            @endforeach
                    </select>
                  </div>
               </div>
              <div class="modal-footer">
                 <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                 <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
              </div>
            </form>
         </div>
      </div>
   </div>

 @include('layouts.footer')
    <!--end footer-->
    </div>
    <!-- end page content -->
@endsection
@push('staff_js')
    <script>
        var main_url = '{!! url('/') !!}';
        var App_url = '{!! url()->current() !!}';

   var appURL = '{!! url('/staff/') !!}';
   var apiURL = '{!! url('/staff/') !!}';
    </script>
    {{--  Inject vue js files  --}}

<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('js/cmn/cmn.js') }}"></script>
<script src="{{ asset('js/unbounce.js') }}"></script>
@endpush

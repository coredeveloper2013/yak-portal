import Multiselect from "vue-multiselect";
new Vue({
    el: '#AddtaskContainer',
    components: {Multiselect},
    mounted() {
        this.getStaffMembers();
        this.getCampaigns();
        this.getLandingPages();
        this.getOtherProjects();
        var context = this;
        $('input[name="due_date"]').daterangepicker({
            opens: 'left',
            singleDatePicker: true,
            showDropdowns: true,
            timePicker: true,
            locale: {
                format: 'MM-DD-YYYY hh:mm:ss A'
            },
            minDate: moment(),
        }, function (start, end, label) {
            context.dueDate = start.format('YYYY-MM-DD hh:mm:ss A');
        });
    },
    watch: {
        campaignId: function(val){
            var flag = false;

            if (this.projectId != null && this.projectId.length > 0) {
                flag = true;
            } else if (this.landingId != null && this.landingId.length > 0) {
                flag = true;
            } else if (this.projectId != null && this.projectId.length == undefined) {
                flag = true;
            } else if (this.landingId != null && this.landingId.length == undefined) {
                flag = true;
            }

            if(flag){
                Swal.fire({
                    title: 'Sorry',
                    text: 'You can only link to one campaign, landing or project.',
                    icon: 'error'
                });
                this.campaignId = '';
                return false;
            }
        },
        landingId: function(val){
            var flag = false;

            if (this.campaignId != null && this.campaignId.length > 0) {
                flag = true;
            } else if (this.projectId != null && this.projectId.length > 0) {
                flag = true;
            } else if (this.campaignId != null && this.campaignId.length == undefined) {
                flag = true;
            } else if (this.projectId != null && this.projectId.length == undefined) {
                flag = true;
            }

            if(flag){
                Swal.fire({
                    title: 'Sorry',
                    text: 'You can only link to one campaign, landing or project.',
                    icon: 'error'
                });
                this.landingId = '';
                return false;
            }
        },
        projectId: function(val){
            var flag = false;

            if (this.campaignId != null && this.campaignId.length > 0) {
                flag = true;
            } else if (this.landingId != null && this.landingId.length > 0) {
                flag = true;
            } else if (this.campaignId != null && this.campaignId.length == undefined) {
                flag = true;
            } else if (this.landingId != null && this.landingId.length == undefined) {
                flag = true;
            }

            if(flag){
                Swal.fire({
                    title: 'Sorry',
                    text: 'You can only link to one campaign, landing or project.',
                    icon: 'error'
                });
                this.projectId = '';
                return false;
            }
        },
    },
    data: {
        taskUrl: tasksUrl,
        title: '',
        description: '',
        assignToOthers: true,
        staffMembers: [],
        campaigns: [],
        landingPages: [],
        otherProjects: [],
        users: '',
        LinkedModuleId: '',
        // assigneeId: user_id,
        status: 'pending',
        priority: 'low',
        getStaffMembersUrl : getStaffMembersUrl,
        getCampaignsUrl : getCampaignsUrl,
        getLandingPagesUrl : getLandingPagesUrl,
        getOtherProjectsUrl : getOtherProjectsUrl,
        submitFormUrl: storeTaskUrl,
        checklists: [
            {
                name: ''
            }
        ],
        file: '',
        campaignId: campaignData,
        landingId: landingData,
        projectId: projectData,
        dueDate: moment().format("YYYY-MM-DD hh:mm:ss A"),
        disableSubmitBtn: false,
    },
    methods: {
        getStaffMembers: function(){
          axios.get(this.getStaffMembersUrl)
              .then(response => {
                  if(response.data.status == 'success'){
                        this.staffMembers =  response.data.data;
                  }
              })
        },
        getCampaigns: function(){
          axios.get(this.getCampaignsUrl)
              .then(response => {
                  if(response.data.status == 'success'){
                        this.campaigns =  response.data.data;
                  }
              })
        },
        getLandingPages: function(){
          axios.get(this.getLandingPagesUrl)
              .then(response => {
                  if(response.data.status == 'success'){
                        this.landingPages =  response.data.data;
                  }
              })
        },
        getOtherProjects: function(){
          axios.get(this.getOtherProjectsUrl)
              .then(response => {
                  if(response.data.status == 'success'){
                        this.otherProjects =  response.data.data;
                  }
              })
        },
        backToTasks: function(){
            window.location = this.taskUrl;
        },
        add(index) {
            this.checklists.push({ name: '' });
        },
        remove(index) {
            this.checklists.splice(index, 1);
        },
        handleFileUpload(){
            this.file = this.$refs.file.files[0];
        },
        submitForm(){
            if(this.title == ''){
                Swal.fire({
                    title: 'Sorry',
                    text: 'Title is required.',
                    icon: 'error'
                });
                return false;
            }

            if(this.users == ''){
                Swal.fire({
                    title: 'Sorry',
                    text: 'Assign task to some staff member.',
                    icon: 'error'
                });
                return false;
            }
            
            if(this.description == ''){
                Swal.fire({
                    title: 'Sorry',
                    text: 'Description is required.',
                    icon: 'error'
                });
                return false;
            }

            var validFileTypes = ["image/gif", "image/jpeg", "image/png", "application/pdf",
                "application/vnd.openxmlformats-officedocument.wordprocessingml.document"];
            if (this.file != '' && $.inArray(this.file.type, validFileTypes) < 0) {

                Swal.fire({
                    title: 'Sorry',
                    text: 'You can only upload Image, Pdf or Docx file.',
                    icon: 'error'
                });
                return false;
            }
            let temp = [];
            _.forEach(this.checklists, function(value){
                temp.push(value.name);
            });
            let userIds = [];
            _.forEach(this.users, function(value){
                userIds.push(value.id);
            });
            this.disableSubmitBtn = true;
            let formData = new FormData();
            formData.append('file', this.file);
            formData.append('title', this.title);
            formData.append('description', this.description);
            formData.append('status', this.status);
            formData.append('priority', this.priority);
            formData.append('priority', this.priority);
            formData.append('checklists', temp);
            formData.append('userIds', userIds);
            formData.append('dueDate', this.dueDate);
            if(this.campaignId != ''){
                formData.append('moduleId', this.campaignId.id);
                formData.append('type', 'campaign');
            }

            if(this.landingId != ''){
                formData.append('moduleId', this.landingId.id);
                formData.append('type', 'landing_page');
            }
            if(this.projectId != ''){
                formData.append('moduleId', this.projectId.id);
                formData.append('type', 'other_project');
            }
            // formData.append('landingPageId', this.landingId);
            // formData.append('otherProjectId', this.projectId);
            axios.post( this.submitFormUrl,
                formData,
                {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }
            ).then(response => {
                console.log(response);
                if(response.data.status == 'success'){
                    this.backToTasks();
                    this.disableSubmitBtn = false;
                }
            }).catch(error => {
                console.log(error);
                this.disableSubmitBtn = false;
            });
        },

    }
});

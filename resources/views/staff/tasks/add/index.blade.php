@extends('staff.layouts.app')

@section('staff_content')

    <div class="page-content grey-bg" id="AddtaskContainer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="float-right">
                            <ol class="breadcrumb">
                                <button type="button" class="btn btn-primary btn-sm text-uppercase" @click="backToTasks">
                                    <i class="fas fa-backward"></i>&nbsp;&nbsp; Back to Tasks
                                </button>
                            </ol>
                        </div>
                        <h4 class="page-title">Create Task</h4>
                    </div>
                    <!--end page-title-box-->
                </div>
            </div>
            <!--end col-->
            <div class="row top-search01">
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <form>
                        <div class="form-row">
                            <div class="form-group col-md-4 col-sm-6">
                                <label for="usr">Title:</label>
                                <input type="text" class="form-control" v-model="title">
                            </div>
                            <div class="form-group col-md-4 col-sm-6">
                                <label for="usr">Assign to Staff Members</label>
                                {{--<div class="custom-control custom-switch dp-inline pl-5">
                                    <input type="checkbox" class="custom-control-input" id="ct-switch1" v-model="assignToOthers">
                                    <label class="custom-control-label" for="ct-switch1"></label>
                                </div>--}}
                                <div class="form-group">
                                    <multiselect v-model="users" :options="staffMembers" :multiple="true" :disabled="!assignToOthers" :close-on-select="false" :clear-on-select="false" :preserve-search="true" placeholder="Assign task to staff" label="name" track-by="id" :preselect-first="true">
                                        <template slot="noResult" slot-scope="data">
                                            <strong>Sorry, couldn't find staff member with this name.</strong>
                                        </template>
                                    </multiselect>
                                </div>
                            </div>
                            <div class="form-group col-md-4 col-sm-6">
                                <label for="usr">Status</label>
                                <select name="assign" class="custom-select" v-model="status" >
                                    <option value="pending">Pending</option>
                                    <option value="in-progress">In Progress</option>
                                    <option value="completed">Completed</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4 col-sm-6">
                                <label for="usr">Priority</label>
                                <select name="assign" class="custom-select" v-model="priority">
                                    <option value="low">Low</option>
                                    <option value="medium">Medium</option>
                                    <option value="high">High</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4 col-sm-6">
                                <label for="usr">Due Date</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="due_date"
                                           id="due_date"
                                           placeholder="Due Date">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="dripicons-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-4"></div>
                            <div class="form-group col-md-4 col-sm-6">
                                <label for="campaign">Link to Campaign</label>
{{--                                <select name="campaign" class="custom-select" v-model="campaignId">--}}
{{--                                    <option value=""></option>--}}
{{--                                    <option :value="campaign.id" v-for="campaign in campaigns">@{{ campaign.name }}</option>--}}
{{--                                </select>--}}
                                <multiselect :options="campaigns" v-model="campaignId"
                                             :show-labels="false"
                                             placeholder="Link to campaign" label="name" track-by="id">
                                    <template slot="noResult" slot-scope="data">
                                        <strong>Sorry, couldn't find campaign with this name.</strong>
                                    </template>
                                </multiselect>
                            </div>
                            <div class="form-group col-md-4 col-sm-6">
                                <label for="landing">Link to Landing Pages</label>
{{--                                <select name="landing" class="custom-select" v-model="landingId">--}}
{{--                                    <option value=""></option>--}}
{{--                                    <option :value="landing.id" v-for="landing in landingPages">@{{ landing.name }}</option>--}}
{{--                                </select>--}}
                                <multiselect :options="landingPages" v-model="landingId"
                                             :show-labels="false"
                                             placeholder="Link to landing page" label="name" track-by="id">
                                    <template slot="noResult" slot-scope="data">
                                        <strong>Sorry, couldn't find landing page with this name.</strong>
                                    </template>
                                </multiselect>
                            </div>
                            <div class="form-group col-md-4 col-sm-6">
                                <label for="landing">Link to Other Projects</label>
{{--                                <select name="landing" class="custom-select" v-model="projectId">--}}
{{--                                    <option value=""></option>--}}
{{--                                    <option :value="project.id" v-for="project in otherProjects">@{{ project.project_name }}</option>--}}
{{--                                </select>--}}
                                <multiselect :options="otherProjects" v-model="projectId"
                                             :show-labels="false"
                                             placeholder="Link to other projects" label="project_name" track-by="id">
                                    <template slot="noResult" slot-scope="data">
                                        <strong>Sorry, couldn't find project with this name.</strong>
                                    </template>
                                </multiselect>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="exampleFormControlTextarea1">Description</label>
                                <textarea name="description" id="description" v-model="description"
                                          class="form-control"  rows="4" cols="50"></textarea>
                            </div>
                            <div class="form-group col-md-3 col-sm-6" id="checklist-gp" v-for="(input,k) in checklists" :key="k">
                                <label>Checklist</label>
                                <input type="text" class="form-control form-control-sm" id="usr" v-model="input.name">
                                <span>
                                    <i class="fa fa-plus" @click="add(k)" v-show="k == checklists.length-1"></i>
                                    <i class="fa fa-trash" style="color: red" @click="remove(k)" v-show="k || ( !k && checklists.length > 1)"></i>
                                </span>
                            </div>
                            <div class="form-group col-md-12">
                                <label>Attachment</label>
                                <div class="uploadOuter">
                              <span class="dragBox" >
{{--                                 <div class="drag-t">Drag and Drop</div>--}}
{{--                                 <span class="drag-h">--}}
{{--                                 Drag your files into this area</span>--}}
{{--                                 <button type="button" class="btn btn-light mode-uploadbtn">Upload</button>--}}
                                 <input type="file" class="up-file" id="file" ref="file" v-on:change="handleFileUpload()" />
                              </span>
                                </div>
                                <div id="preview" class="d-flex align-content-start flex-wrap"></div>
                            </div>
                            <div class="form-group col-md-12">
                                <button type="button" class="btn btn-primary" @click="submitForm" :disabled="disableSubmitBtn">Create Task</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- end page content -->
    </div>


@endsection
@push('staff_js')
    <script>
        var tasksUrl = '{!! route('staff.tasks') !!}';
        var getStaffMembersUrl = '{!! route('staff.tasks.members') !!}';
        var getCampaignsUrl = '{!! route('staff.tasks.campaigns') !!}';
        var getLandingPagesUrl = '{!! route('staff.tasks.landing') !!}';
        var getOtherProjectsUrl = '{!! route('staff.tasks.project') !!}';
        var storeTaskUrl = '{!! route('staff.tasks.store') !!}';
        var App_url = '{!! url()->current() !!}';
        var user_id = '{!! Auth::user()->id !!}';

        var campaignData = @if (!empty($campaignData)) {!! $campaignData !!} @else [] @endif;
        var landingData = @if (!empty($landingData)) {!! $landingData !!} @else [] @endif;
        var projectData = @if (!empty($projectData)) {!! $projectData !!} @else [] @endif;
    </script>
    {{--  Inject vue js files  --}}
    <script src="{{ asset('js/app-vue.js') }}"></script>
    <script src="{{ asset('js/staff/tasks/add_vue.js') }}"></script>
@endpush

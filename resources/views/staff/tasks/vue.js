new Vue({
    el: '#taskContainer',
    mounted() {
        this.getTasksStatus('pending');
        //this.getTasks(false);
    },
    data: {
        todayTasks: [],
        upcomingTasks: [],
        todayTasksMeta: {},
        upcomingTasksMeta: {},
        currentUser: user_id,
        currentTask: '',
        comment: '',
        filter: {
            allTasks: false,
            myTasks: true,
            status: '',
        },
        showLoader: false,
        avatarBaseUrl: avatarBaseUrl,
        userAvatarBaseUrl: userAvatarBaseUrl,
        getTasksUrl: App_url + '/data',
        postCommentUrl : App_url + '/post/comment',
        updateStatusUrl: App_url + '/status/update',
        updateCheckListStatusUrl: App_url + '/checklist/status/update',
        removeAttachmentUrl: App_url + '/remove/attachment',
    },
    watch: {
        'tasksMeta.current_page': function (val) {
            this.loadPaginatedData();
        },
        currentTask: function(task) {
            this.updateCurrentCheckbox();
        }
    },
    methods: {
        loadPaginatedData: function () {
            this.getTasks(this.getTasksUrl + '?page=' + this.todayTasksMeta.current_page);
        },
        getTasks: function (url = false) {
            this.showLoader = true;
            if (url == false) {
                url = this.getTasksUrl;
            }
            axios.post(url, {filter: this.filter})
                .then(response => {

                    if (response.data.status == 'success') {
                        this.todayTasks = response.data.todayTasks;
                        // this.todayTasksMeta = response.data.todayTasks.meta;
                        this.upcomingTasks = response.data.upcomingTasks;
                        // this.upcomingTasksMeta = response.data.upcomingTasks.meta;
                        this.showLoader = false;
                        if (this.todayTasks.length) {
                            this.currentTask = this.todayTasks[0];
                            this.updateCurrentTask(this.currentTask);
                            setTimeout(() => {
                                this.updateCurrentCheckbox();
                            }, 1000);
                        }else if (this.upcomingTasks.length){
                            this.currentTask = this.upcomingTasks[0];
                            this.updateCurrentTask(this.currentTask);
                            setTimeout(() => {
                                this.updateCurrentCheckbox();
                            }, 1000);
                        }
                    }
                }).catch(error => {
                this.showLoader = false;
                console.log(error);
            });
        },
        getMyTasks: function () {

            this.filter = {
                myTasks: true,
                allTasks: false,
                status: ''
            };
            this.getTasks(false);
        },
        getAllTasks: function () {
            this.filter = {
                allTasks: true,
                myTasks: false,
                status: ''
            };

            this.getTasks(false);
        },
        getTasksStatus: function (status) {
            this.filter.status = status;

            this.getTasks(false);
        },
        resetFilter: function () {
            this.filter = {
                allTasks: false,
                myTasks: true,
                status: '',
            };
            this.getTasks(false);
        },
        showAddForm: function () {
            window.location = App_url + '/create';
        },
        backToTasks: function () {
            window.location = App_url + '/';
        },
        updateCurrentTask: function(task){
            this.currentTask = task;
        },
        updateCurrentCheckbox(){
            $(".leftsidecheck").removeClass("active");
            $("#customCheck"+this.currentTask.id).addClass("active");
            $(".leftsidecheck").prop("checked", false);
            $("#customCheck"+this.currentTask.id).prop("checked", true);
        },
        postComment: function(task){
            this.showLoader = true;
            axios.post(this.postCommentUrl, {comment: this.comment, taskId: task.id})
                .then(response => {
                    if(response.data.status == 'success'){
                        Swal.fire({
                            title: 'Success',
                            text: 'Comment Posted.',
                            icon: 'success'
                        });
                        this.showLoader = false;
                        this.currentTask = response.data.data;
                        this.getTasksStatus(this.filter.status);
                        //this.updateCurrentTask(this.currentTask);
                        //this.getMyTasks(false);
                        this.comment = '';
                    }
                }).catch(error => {
                this.showLoader = false;
                    console.log(error);
            });
        },
        markTaskAsCompleted: function(task){
            this.showLoader = true;
            if($("#taskStatusCheckbox"+task.id).prop("checked") == true){
                axios.post(this.updateStatusUrl, {status: 'completed', taskId: task.id})
                    .then(response => {
                        if(response.data.status == 'success'){
                            this.showLoader = false;
                            Swal.fire({
                                title: 'Success',
                                text: 'Task marked as completed',
                                icon: 'success'
                            });
                            // this.currentTask = response.data.data;
                            // this.updateCurrentTask(this.currentTask);
                        }
                    }).catch(error => {
                        this.showLoader = false;
                        console.log(error);
                    });
            }
            else if($("#taskStatusCheckbox"+task.id).prop("checked") == false){
                this.showLoader = true;
                axios.post(this.updateStatusUrl, {status: 'pending', taskId: task.id})
                    .then(response => {
                        if(response.data.status == 'success'){
                            this.showLoader = false;
                            Swal.fire({
                                title: 'Success',
                                text: 'Task marked as pending.',
                                icon: 'success'
                            });
                            // this.currentTask = response.data.data;
                            // this.updateCurrentTask(this.currentTask);
                        }
                    }).catch(error => {
                    this.showLoader = false;
                    console.log(error);
                });
            }
        },

        markSubTaskAsCompleted: function(subTask, taskId){
            if($("#subTaskStatus"+subTask.id).prop("checked") == true){
                var chkStatus = 'completed';
                var chkMsg = 'Checklist marked as completed.';
            } else {
                var chkStatus = 'pending';
                var chkMsg = 'Checklist marked as pending.';
            }
           // this.showLoader = true;
            axios.post(this.updateCheckListStatusUrl, {status: chkStatus, subTaskId: subTask.id, taskId: taskId})
            .then(response => {
                if(response.data.status == 'success'){
                    //this.showLoader = false;
                    Swal.fire({
                        title: 'Success',
                        text: chkMsg,
                        icon: 'success',
                        onAfterClose: () => {
                            this.getTasksStatus(this.filter.status);
                        }
                    });
                    // this.currentTask = response.data.data;
                    // this.updateCurrentTask(this.currentTask);
                }
            }).catch(error => {
                this.showLoader = false;
                console.log(error);
            });
        },
        removeAttachment(taskId){
            axios.post(this.removeAttachmentUrl, {taskId: taskId})
                .then(response => {
                    if(response.data.status == 'success'){
                        Swal.fire({
                            title: 'Success',
                            text: 'Attachment Removed Successfully.',
                            icon: 'success'
                        });
                        this.getTasks();
                    }
                }).catch(error => {
                    console.log(error);
            });
        },
        // checkIfMyTask: function(task){
            //create a new array where user id eist in task users array
            // return task.task_users.includes(this.currentUser);
        // },
        Filtered(filteredItems) {
            // Trigger pagination to update the number of buttons/pages due to filtering
            this.todayTasksMeta.totalRows = filteredItems.length;
            this.todayTasksMeta.currentPage = 1;
        },
    }
});

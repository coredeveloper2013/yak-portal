@extends('staff.layouts.app')
@section('staff_content')

    <div class="page-content grey-bg compaign-sec-bg-1" id="taskContainer">
            <div class="page-loader" v-if="showLoader">
                <div class="spinner-border text-primary" role="status">
              <span class="sr-only">Loading...</span>
            </div>
            </div>



        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Tasks</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-7 col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xl-4 col-md-12 col-sm-12">
                                    <div class="btn-group light-btn">
                                        <button type="button" class="btn btn-light"
                                                :class="{ active: filter.myTasks }"
                                                @click="getMyTasks"> My Tasks </button>
                                        <button type="button" class="btn btn-light" :class="{ active: filter.allTasks }"
                                                @click="getAllTasks"> All Tasks </button>
                                    </div>
                                </div>
                                <div class="col-xl-8 col-md-12 col-sm-12 pd-0">
                                    <div class="btn-right-m">
                                        <div class="btn-group light-btn task-btn-resp">
                                            <button type="button" class="btn btn-light"
                                                    :class="{ active: filter.status == 'pending' }"
                                                    @click="getTasksStatus('pending')"> Pending </button>
                                            <button type="button" class="btn btn-light"
                                                    :class="{ active: filter.status == 'in-progress' }"
                                                    @click="getTasksStatus('in-progress')"> In Progress </button>
                                            <button type="button" class="btn btn-light"
                                                    :class="{ active: filter.status == 'completed' }"
                                                    @click="getTasksStatus('completed')"> Completed </button>
                                        </div>
                                        <button type="button" class="btn" @click="resetFilter"><i class="fas fa-undo"></i></button>
                                        <button type="button" class="btn btn-primary ad-new-task task-btn-resp-1" @click="showAddForm"><i class="fas fa-plus"></i> Add New </button>
                                    </div>
                                </div>
                            </div>
                            <div class="row anti-flex">
                                <div class="task-list">
                                    <div class="task-list-o" data-toggle="collapse" href="#collapse01">
                                        <i class="fas fa-angle-down"></i> Today <span>(@{{ todayTasks.length }})</span>
                                    </div>
                                    <div class="collapse show task-c" id="collapse01">
                                        <div class="table-responsive">
                                            <table class="table m-100" v-if="todayTasks.length">
                                                <tr v-for="task in todayTasks">
                                                    <td class="check-decs">
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input leftsidecheck"
                                                                   :id="`${'customCheck'+task.id}`" name="example1"
                                                                   @click="updateCurrentTask(task)">
                                                            <label class="custom-control-label" :for="`${'customCheck'+task.id}`">
                                                                @{{ task.name }}
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <img src="{{asset('staff/images/no-image.jpg')}}" alt="user" class="attachment-o" v-if="task.attachments == null">
                                                        <img :src="avatarBaseUrl  +  task.attachments"
                                                             alt="user" class="attachment-o"
                                                             v-if="(task.file_type == 'image/jpg' || task.file_type == 'image/jpeg' ||
                                                            task.file_type == 'image/gif' || task.file_type == 'image/bmp' ||
                                                             task.file_type == 'image/png') && (task.attachments != null)">
                                                        <img src="{{asset('staff/images/pdf.png')}}"
                                                             alt="user" class="attachment-o"
                                                        v-if="(task.file_type == 'application/pdf') && (task.attachments != null)">
                                                        <img src="{{asset('staff/images/file.png')}}"
                                                             alt="user" class="attachment-o"
                                                        v-if="(task.file_type == 'application/msword' ||
                                                         task.file_type == 'application/vnd.openxmlformats-officedoc') && (task.attachments != null)">
                                                    </td>
                                                    <td class="utilities">
                                                        <span><i class="fas fa-calendar-alt"></i> @{{ task.human_due_date }} </span>
                                                        <span><i class="fas fa-list"></i>
                                                            @{{ task.sub_tasks_count }}/@{{ task.sub_tasks.length }}
                                                        </span>
                                                        <span><i class="far fa-comment-dots"></i> @{{ task.comments.length }} </span>
                                                        <span class="badge badge-low" v-if="task.priority == 'low'">@{{ task.priority }}</span>
                                                        <span class="badge badge-medium" v-if="task.priority == 'medium'">@{{ task.priority }}</span>
                                                        <span class="badge badge-high" v-if="task.priority == 'high'">@{{ task.priority }}</span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="task-list-o" data-toggle="collapse" href="#collapse02">
                                        <i class="fas fa-angle-down"></i> upcoming <span>(@{{ upcomingTasks.length }})</span>
                                    </div>
                                    <div class="collapse show task-c" id="collapse02">
                                        <div class="table-responsive">
                                            <table class="table m-100" v-if="upcomingTasks.length">
                                                <tr v-for="upcomingTask in upcomingTasks">
                                                    <td class="check-decs">
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input leftsidecheck"
                                                                   :id="`${'customCheck'+upcomingTask.id}`" name="example1"
                                                                   @click="updateCurrentTask(upcomingTask)">
                                                            <label class="custom-control-label"
                                                                   :for="`${'customCheck'+upcomingTask.id}`">
                                                                @{{ upcomingTask.name }}
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <img src="{{asset('staff/images/no-image.jpg')}}" alt="user" class="attachment-o" v-if="upcomingTask.attachments == null">
                                                        <img :src="avatarBaseUrl  +  upcomingTask.attachments"
                                                             alt="user" class="attachment-o"
                                                             v-if="(upcomingTask.file_type == 'image/jpg' || upcomingTask.file_type == 'image/jpeg' ||
                                                            upcomingTask.file_type == 'image/gif' || upcomingTask.file_type == 'image/bmp' ||
                                                             upcomingTask.file_type == 'image/png') && (upcomingTask.attachments != null)">
                                                        <img src="{{asset('staff/images/pdf.png')}}"
                                                             alt="user" class="attachment-o"
                                                             v-if="(upcomingTask.file_type == 'application/pdf') && (upcomingTask.attachments != null)">
                                                        <img src="{{asset('staff/images/file.png')}}"
                                                             alt="user" class="attachment-o"
                                                             v-if="(upcomingTask.file_type == 'application/msword' ||
                                                         upcomingTask.file_type == 'application/vnd.openxmlformats-officedoc') && (upcomingTask.attachments != null)">
                                                    </td>
                                                    <td class="utilities">
                                                        <span><i class="fas fa-calendar-alt"></i> @{{ upcomingTask.human_due_date }} </span>
                                                        <span><i class="fas fa-list"></i>
                                                            @{{ upcomingTask.sub_tasks_count }}/@{{ upcomingTask.sub_tasks.length }}
                                                        </span>
                                                        <span><i class="far fa-comment-dots"></i> @{{ upcomingTask.comments.length }} </span>
                                                        <span class="badge badge-low" v-if="upcomingTask.priority == 'low'">@{{ upcomingTask.priority }}</span>
                                                        <span class="badge badge-medium" v-if="upcomingTask.priority == 'medium'">@{{ upcomingTask.priority }}</span>
                                                        <span class="badge badge-high" v-if="upcomingTask.priority == 'high'">@{{ upcomingTask.priority }}</span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-5 col-sm-12" v-if="currentTask">
                    <div class="card">
                        <div class="card-body">
                            <div class="sidetask-head">
                                <div class="row">
                                    <div class="col-9">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input"
                                                   :id="`${'taskStatusCheckbox'+currentTask.id}`"
                                                   name="example1"
                                                   @change="markTaskAsCompleted(currentTask)"
                                                   :checked="currentTask.status =='completed'">
                                            <label class="custom-control-label"
                                                   :for="`${'taskStatusCheckbox'+currentTask.id}`">
                                                Mark as completed
                                            </label>
                                        </div>
                                    </div>
{{--                                    <div class="col-3">--}}
{{--                                        <div class="dropdown dropleft float-right">--}}
{{--                              <span class=" dropdown-toggle " type="button" id="dropdownMenu2" data-toggle="dropdown"--}}
{{--                                    aria-haspopup="true" aria-expanded="false">--}}
{{--                              <i class="fas fa-ellipsis-h"></i>--}}
{{--                              </span>--}}
{{--                                            <div class="dropdown-menu mt-3" aria-labelledby="dropdownMenu2">--}}
{{--                                                <button class="dropdown-item" type="button">Action</button>--}}
{{--                                                <button class="dropdown-item" type="button">Another action</button>--}}
{{--                                                <button class="dropdown-item" type="button">Something else here</button>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
                                </div>
                            </div>
                            <h4 class="task-title-head">@{{ currentTask.name }}</h4>
                            <div class="sidetask-assign">
                                <div class="row assign-head">
                                    <div class="col-6">
                                        Assigned to
                                    </div>
                                    <div class="col-6">
                                        Due Date
                                    </div>
                                </div>
                                <div class="row assign-list" v-if="currentTask.task_users.length <= 0">
                                    <div class="col-6">
                                        <p>No Assigne found.</p>
                                    </div>
                                    <div class="col-6">
                                        @{{currentTask.human_due_date  }}
                                    </div>
                                </div>
                                <div class="row assign-list" v-for="user in currentTask.task_users" v-else>
                                    <div class="col-6">
                                        <a href="" class="media">
                                            <div class="media-left">
                                                <img :src="userAvatarBaseUrl + user.image" class="rounded-circle thumb-xm"
                                                     alt="User Profile" v-if="user.image != null">
                                                <img src="{{asset('images/user.jpg')}}" class="rounded-circle thumb-xm"
                                                     alt="User Profile" v-else>
                                            </div>
                                            <!-- media-left -->
                                            <div class="media-body">
                                                <div>
                                                    <h6>@{{ user.name }}</h6>
                                                </div>
                                            </div>
                                            <!-- end media-body -->
                                        </a>
                                    </div>
                                    <div class="col-6">
                                        <span class="timel-o"><i class="fas fa-calendar-alt text-success"></i> <span>@{{ currentTask.human_due_date }}</span></span>
                                    </div>
                                </div>
                            </div>

                            <div class="decs-subtask">
                                <p>
                                    @{{ currentTask.description }}
                                </p>
                            </div>

                            <div v-if="currentTask.link_type != null">
                                <h5>Linked With @{{ currentTask.module_name }} (@{{ currentTask.module_type }})</h5>
                            </div>

                            <div class="task-checklist" v-if="currentTask.sub_tasks.length > 0">
                                <h4 class="heading-sub-o">Checklists</h4>
                                <div class="custom-control custom-checkbox" v-for="subTask in currentTask.sub_tasks">
                                    <input type="checkbox" class="custom-control-input"
                                           :id="`${'subTaskStatus'+subTask.id}`"
                                           @change="markSubTaskAsCompleted(subTask, currentTask.id)"
                                           :checked="subTask.status =='completed'">
                                    <label class="custom-control-label" :for="`${'subTaskStatus'+subTask.id}`">@{{ subTask.name }}</label>
                                </div>
                            </div>
                            <div class="task-checklist" v-else>
                                <h4 class="heading-sub-o">Checklists</h4>
                                <div class="custom-control custom-checkbox">
                                    <p>No checklist found.</p>
                                </div>
                            </div>


                            <div class="task-attachment" v-if="currentTask.attachments != null">
                                <h4 class="heading-sub-o">Attachments</h4>
                                <ul>
                                    <li>
                                            <div class="media">
                                            <div class="media-left" v-if="currentTask.file_type == 'image/jpg' || currentTask.file_type == 'image/jpeg' ||
                                                            currentTask.file_type == 'image/gif' || currentTask.file_type == 'image/bmp' ||
                                                             currentTask.file_type == 'image/png'">
                                                <img :src="avatarBaseUrl + currentTask.attachments" alt="attachment" class="thumb-sm">
                                            </div>
                                            <div class="media-left" v-if="currentTask.file_type == 'application/pdf'">
                                                <img src="{{asset('staff/images/pdf.png')}}"
                                                     alt="user" class="attachment-o">
                                            </div>
                                            <div class="media-left" v-if="currentTask.file_type == 'application/msword'
                                             || currentTask.file_type == 'application/vnd.openxmlformats-officedoc'">
                                                <img src="{{asset('staff/images/file.png')}}"
                                                     alt="user" class="attachment-o">
                                            </div>
                                            <!-- media-left -->
                                            <div class="media-body">
                                                <div>
                                                    <span class="file-name-o">@{{ currentTask.attachments }}</span>
                                                </div>
                                            </div>
                                            </div>
                                            <!-- end media-body -->
                                        <span class="float-right top-padding">
                                            <a :href="avatarBaseUrl + currentTask.attachments" target="_blank"><i class="fas fa-cloud-download-alt"></i></a>
                                            <a href="javascript:;" @click="removeAttachment(currentTask.id)"><i class="fas fa-times pl-1 pr-1 text-danger"></i></a>
                                        </span>
                                    </li>
                                </ul>
                            </div>
                            <div class="task-attachment" v-else>
                                <h4 class="heading-sub-o">No attachments found.</h4>
                            </div>

                            <div class="task-comment-o" v-if="currentTask.comments.length > 0">
                                <div class="comment-listing">
                                    <ul>
                                        <li v-for="comment in currentTask.comments">
                                            <span class="time-com-o">@{{ comment.human_created_at }}</span>
                                            <div class="media float-left">
                                                <div class="media-left">
                                                    <img src="{{asset('/images/user.jpg')}}" class="comment-o" alt="User Profile Image" v-if="comment.comment_by.image == null">
                                                    <img :src="userAvatarBaseUrl + comment.comment_by.image" class="comment-o"
                                                         alt="User Profile Image" v-else>
                                                </div>
                                                <!-- media-left -->
                                                <div class="media-body">
                                                    <div>
                                                        <h6>@{{ comment.comment_by.name }}</h6>
                                                        <p>@{{ comment.description }}</p>
                                                    </div>
                                                </div>
                                                <!-- end media-body -->
                                            </div>
                                        </li>
                                    </ul>
                                </div>


                                <div class="card">
                                    <div class="card-body no-padding">
                                        <textarea class="form-control" id="" rows="4" cols="10"
                                                  v-model="comment"
                                                  v-on:keydown.ctrl.enter="postComment(currentTask)"></textarea>
                                    </div>
                                    <div class="card-footer">
                                        <div class="float-right">
                                            <button type="button" class="btn btn-success btn-sm send-btn"
                                                    @click="postComment(currentTask)">
                                                <i class="fab fa-telegram-plane"></i> Submit
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div v-else>
                                <div class="card">
                                    <div class="card-body no-padding">
                                        <textarea class="form-control" id="" rows="4" cols="10"
                                                  v-model="comment"
                                                  v-on:keydown.ctrl.enter="postComment(currentTask)"></textarea>
                                    </div>
                                    <div class="card-footer">
                                        <div class="float-right">
                                            <button type="button" class="btn btn-success btn-sm send-btn"
                                                    @click="postComment(currentTask)">
                                                <i class="fab fa-telegram-plane"></i> Submit
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>




                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('staff_js')
    <script>
        var avatarBaseUrl = '{!! asset('storage/tasks')  !!}' + '/';
        var userAvatarBaseUrl = '{!! asset('/storage/avatars/')  !!}' + '/';
        var App_url = '{!! url()->current() !!}';
        var user_id = '{!! Auth::user()->id !!}';
    </script>
    {{--  Inject vue js files  --}}
    <script src="{{ asset('js/app-vue.js') }}"></script>
    <script src="{{ asset('js/staff/tasks/vue.js') }}"></script>
@endpush

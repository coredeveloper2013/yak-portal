@extends('staff.layouts.app')
@section('staff_css')
<style>
  html {
    background-color: #f8f8f8;
  }
</style>
@endsection
@section('staff_content')
<div class="page-content grey-bg project">
   <div class="container-fluid">
      <div class="row">
         <div class="col-sm-12">
            <div class="page-title-box">
               <h4 class="page-title">Project Details</h4>
            </div>
            <!--end page-title-box-->
         </div>
         <!--end col-->
      </div>
      <div class="row">
         <div class="col-xl-4 col-lg-5 col-md-5">
         <div class="detail-scroll">
            <div class="row">
               <div class="col-12">
                  <div class="blue-badge text-center">
                     <div>Status</div>
                     <h3>
                        @if (strtolower($projects->status) == 'pending')
                          IN PROGRESS
                        @else
                          {{ strtoupper($projects->status) }}
                        @endif
                    </h3>
                  </div>
               </div>
               <div class="col-12">
                  <a href="javascript:;" class="blue-plane text-center">Order:<span>#{{ $projects->id }}</span></a>
               </div>
               <div class="col-12">
                  <div class="purple-badge text-center">
                     <h3>{{ $projects->customer_name }}</h3>
                     <a href="javascript:;" class="btn btn-light-o" data-toggle="modal" data-target="#contact_detail">View Contact Details</a>
                  </div>
               </div>
               <div class="col-6">
                  <div class="blue-badge-p">
                     <div>Contract Value</div>
                     <h3>&#163; {{ $projects->price }}</h3>
                  </div>
               </div>
               <div class="col-6">
                  <div class="blue-badge-p">
                     <div>Budget</div>
                     <h3>&#163; {{ $projects->budget }}</h3>
                  </div>
               </div>
               {{--<div class="col-6">
                  <a href="javascript:;" class="green-plane text-center">Contract</a>
               </div>
               <div class="col-6">
                  <a href="javascript:;" class="green-plane text-center">Payment</a>
               </div>--}}
            </div>
            <div class="row">
               <div class="card task-detail">
                  <div class="card-header">
                     Task Details
                     <a href="{{ route('staff.tasks.create') }}" class="btn btn-sm btn-primary float-right"><i class="fa fa-plus"></i> Add Task</a>
                  </div>
                  <ul class="list-group">
                    @forelse ($projects->tasks as $task)
                        <li class="list-group-item">
                           @if (!empty($task->attachments))
                              <img src="{{ asset('/storage/tasks/'.$task->attachments) }}" class="mr-3">
                           @endif
                           <span class="title-list">{{$task->name}}</span>
                           @if ($task->status == 'completed')
                              <span class="badge badge-success float-right">Completed</span>
                           @else
                              <span class="badge badge-warning float-right">Pending</span>
                           @endif
                        </li>
                    @empty
                         <span class="text-center">No task detail found.</span>
                    @endforelse
                  </ul>
               </div>
            </div>
            <div class="card">
               <div class="card-body">
                  <h3 class="card-heading">Description</h3>
                  <p class="decs-grey">
                     {{ $projects->description }}
                  </p>
               </div>
            </div>
            <div class="row">
               <div class="card asign-detail">
                  <div class="card-header">
                     Assign Details
                  </div>
                  <ul class="list-group">
                     @forelse ($projects->assigned_to as $assign)
                        <li class="list-group-item">
                           @if (!empty($assign->image))
                              <img src="{{ asset('storage/avatars/'.$assign->image) }}" class="mr-3">
                           @else
                              <img src="{{asset('staff/images/user-1.jpg')}}" class="mr-3">
                           @endif
                           <span class="title-list">{{$assign->name}}</span>
                           <span class="badge badge-pill badge-grey float-right">{{ ucfirst($assign->type) }}</span>
                        </li>
                     @empty
                         <span class="text-center">Not Assigned to any one.</span>
                     @endforelse
                  </ul>
               </div>
            </div>


            @if (!empty($projects->installments) && count($projects->installments) > 0 && $projects->with_installment == 'Y')
                <div class="row">
                    <div class="card asign-detail">
                        <div class="card-header">
                            Installments
                        </div>
                        <ul class="list-group">
                            <li class="list-group-item">
                                <span class="">Amount</span>
                                <span class="float-right">Date</span>
                            </li>
                            @foreach ($projects->installments as $installment)
                                <li class="list-group-item">
                                    <span class="title-list">£ {{$installment->amount}}</span>
                                    <span class="float-right">{{$installment->installment_date}}</span>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif

	</div>
         </div>
         <div class="col-xl-8 col-lg-7 col-md-7">
            
            <div class="card m-0">
               <div class="card-body">
                  <h3 class="card-heading">Notes</h3>
                  <div class="notes01">
                     <ul class="note-listing">
                        @forelse ($projects->comments as $comment)
                           <li class="media comment-blk">
                              @if (!empty($assign->image))
                                 <img src="{{ asset('storage/avatars/'.$assign->image) }}" class="mr-3">
                              @else
                                 <img src="{{asset('staff/images/user-1.jpg')}}" class="mr-3">
                              @endif
                              <div class="media-body">
                                 <div class="name-time-o">
                                    {{$comment->name}}
                                    <span><i class="mdi mdi-clock-outline"></i> </span>
                                    <span>{{$comment->date}} | </span>
                                    <span>{{$comment->time}}</span>
                                    <a href="javascript:;" class="del-o del-cmnt" data-comment_id="{{$comment->id}}" data-parent_id="{{$projects->id}}"><i class="far fa-trash-alt"></i></a>
                                 </div>
                                 <p class="msg-o">
                                    {!! $comment->comment_text !!}
                                 </p>
                                 
                              </div>
                           </li>
                        @empty
                            <span class="text-center d-block">Not Notes Found.</span>
                        @endforelse
                     </ul>
                     <div class="add-notes">
                        <div class="form-group">
                           <textarea class="form-control" placeholder="Type your note here" id="comment_text"></textarea>
                        </div>
                        <div class="form-group m-0">
                           <input type="button" name="" id="send_comment" class="btn btn-darkblue" value="Add note">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="card float-right">
               <div class="card-body">
                  <div class="btn-group dropup">
                     <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     <i class="fas fa-info"></i> Change Status
                     </button>
                     <div class="dropdown-menu">
                        @if ($projects->status == 'completed')
                           <a class="dropdown-item disabled" href="javascript:;">Completed</a>
                           <a class="dropdown-item chng_status" href="javascript:;" data-status="pending">Pending</a>
                        @else
                           <a class="dropdown-item chng_status" href="javascript:;" data-status="completed">Completed</a>
                           <a class="dropdown-item disabled" href="javascript:;">Pending</a>
                        @endif
                     </div>
                  </div>
                  <button class="btn btn-danger delete_project"> <i class="fas fa-trash"></i> Remove</button>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<!-- Modal -->
<div class="modal fade" id="contact_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
     <div class="modal-content">
        <div class="modal-header">
           <h5>Customer Contact Detail</h5>
           <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
           </button>
        </div>
        <form id="addCampaign">
            <input type="hidden" value="" id="edit_id" name="edit_id">
            <div class="modal-body">
                <div class="form-group">
                    <p><i class="far fa-user float-left"></i>&nbsp; {{ $projects->user->name }}</p>
                </div>

                @if(!empty($projects->user->phone))
                    <div class="form-group">
                        <p><i class="fa fa-phone float-left"></i>&nbsp; {{ $projects->user->phone }}</p>
                    </div>
                @endif

                <div class="form-group">
                    <p><i class="far fa-envelope float-left"></i>&nbsp; {{ $projects->user->email }}</p>
                </div>

                @if(!empty($projects->user->business_info))
                    @if(!empty($projects->user->business_info->email_address))
                        <div class="form-group">
                            <p><i class="far fa-envelope float-left"></i>&nbsp; {{ $projects->user->business_info->email_address }}</p>
                        </div>
                    @endif

                    @if(!empty($projects->user->business_info->phone))
                        <div class="form-group">
                            <p><i class="fa fa-phone float-left"></i>&nbsp; {{ $projects->user->business_info->phone }}</p>
                        </div>
                    @endif

                    @if(!empty($projects->user->business_info->website))
                        <div class="form-group">
                            <p><i class="fa fa-globe float-left"></i>&nbsp; {{ $projects->user->business_info->website }}</p>
                        </div>
                    @endif
                @endif

                <div class="form-group">
                    <p><i class="fa fa-check float-left"></i>&nbsp; {{ ucfirst($projects->user->status) }} Client</p>
                </div>

                @if(!empty($projects->user->business_info))
                    @if(!empty($projects->user->business_info->notes))
                        <div class="form-group">
                            <p><i class="fa fa-comments float-left"></i>&nbsp; {{ $projects->user->business_info->notes }} </p>
                        </div>
                    @endif
                @endif
           </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
            </div>
        </form>
     </div>
  </div>
</div>


@endsection
@push('staff_js')
<script>
   var appURL = '{!! url('/staff/') !!}';
   var apiURL = '{!! url('/api/staff/') !!}';
   var UnqId = '{{ $projects->id }}';
</script>
{{--  Inject vue js files  --}}
<script type="text/javascript">
   $(function() {
      var csrf = $('meta[name="csrf-token"]').attr("content");

      var flag1 = false;
      $('body').on('click', '.chng_status', function(event) {
         event.preventDefault();
         if (flag1) {
            return false;
         }
         flag1 = true;

         var that = $(this);
         var status = that.data('status');

         $.ajax({
            url: apiURL+'/other-projects/chng/status',
            type: 'POST',
            dataType: 'json',
            data: {status: status, id: UnqId},
            headers: {
               "X-CSRF-TOKEN": csrf,
               Accpet: "applicationjson"
           },
         }).always(function(res) {
            flag1 = false;
            if (res.status == 'success') {
                let status = res.status.substr(0,1).toUpperCase()+res.status.substr(1);
               Swal.fire({
                   title: status,
                   text: res.msg,
                   icon: res.status,
                   onAfterClose: () => {
                       location.reload();
                   }
               });
            } else {
                let status = res.status.substr(0,1).toUpperCase()+res.status.substr(1);
               Swal.fire({
                   title: status,
                   text: res.msg,
                   icon: res.status
               });
            }
         });
      });

      var flag2 = false;
      $('body').on('click', '#send_comment', function(event) {
         event.preventDefault();
         if (flag2) {
            return false;
         }
         flag2 = true;

         var text = $('#comment_text').val();
         text = text.trim();
         if (text == '') {
            Swal.fire({
                title: 'error',
                text: 'Please enter some text in note area',
                icon: 'error'
            });
            flag2 = false;
            return false
         }

         $.ajax({
            url: apiURL+'/other-projects/add/comment',
            type: 'POST',
            dataType: 'json',
            data: {id: UnqId, text:text},
            headers: {
               "X-CSRF-TOKEN": csrf,
               Accpet: "applicationjson"
           },
         }).always(function(res) {
            flag2 = false;
            if (res.status == 'success') {
                let status = res.status.substr(0,1).toUpperCase()+res.status.substr(1);
               Swal.fire({
                   title: status,
                   text: res.msg,
                   icon: res.status,
                   onAfterClose: () => {
                       location.reload();
                   }
               });
            } else {
                let status = res.status.substr(0,1).toUpperCase()+res.status.substr(1);
               Swal.fire({
                   title: status,
                   text: res.msg,
                   icon: res.status
               });
            }
         });

      });

      var flag4 = false;
      $('body').on('click', '.del-cmnt', function(event) {
          event.preventDefault();
          if (flag4) {
              return false;
          }
          flag4 = true;

          var that = $(this);
          var id = that.data('comment_id');
          var pid = that.data('parent_id');

          $.ajax({
              url: apiURL+'/other-projects/comment/delete',
              type: 'POST',
              dataType: 'json',
              data: {comment_id: id, parent_id:pid},
              headers: {
                  "X-CSRF-TOKEN": csrf,
                  Accpet: "applicationjson"
              },
          }).always(function(res) {
              flag4 = false;
              let status = res.status.substr(0,1).toUpperCase()+res.status.substr(1);
              if (res.status == 'success') {
                  Swal.fire({
                      title: status,
                      text: res.msg,
                      icon: res.status,
                      onAfterClose: () => {
                          that.parents('.comment-blk').remove();
                      }
                  });
              } else {
                  Swal.fire({
                      title: status,
                      text: res.msg,
                      icon: res.status
                  });
              }
          });
      });

      var flag3 = false;
      $('body').on('click', '.delete_project', function(event) {
         event.preventDefault();
         if (flag3) {
            return false;
         }
         flag3 = true;

         var that = $(this);
         var status = that.data('status');

         $.ajax({
            url: apiURL+'/other-projects/destroy',
            type: 'POST',
            dataType: 'json',
            data: {id: UnqId},
            headers: {
               "X-CSRF-TOKEN": csrf,
               Accpet: "applicationjson"
           },
         }).always(function(res) {
            flag3 = false;
            if (res.status == 'success') {
                let status = res.status.substr(0,1).toUpperCase()+res.status.substr(1);
               Swal.fire({
                   title: status,
                   text: res.msg,
                   icon: res.status,
                   onAfterClose: () => {
                       window.location = "{!! route('staff.other_project.listing') !!}";
                   }
               });
            } else {
                let status = res.status.substr(0,1).toUpperCase()+res.status.substr(1);
               Swal.fire({
                   title: status,
                   text: res.msg,
                   icon: res.status
               });
            }
         });

      });
   });
</script>
@endpush

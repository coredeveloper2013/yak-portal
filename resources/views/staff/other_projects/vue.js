import Multiselect from "vue-multiselect";
var otherProjectsVue = new Vue({
    el: '#StaffLandingPage',
    components: {Multiselect},
    mounted() {
        this.getStaffLandingPages(false);
        this.getStaffMembers();
        this.getCustomers();
        var context = this;
         $('#start_date').daterangepicker({
            opens: 'left',
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'YYYY-MM-DD'
            },
            minDate: moment(),
        }, function (start, end, label) {
            context.start_date = start.format('YYYY-MM-DD');
        });

        $('#end_date').daterangepicker({
            opens: 'left',
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'YYYY-MM-DD'
            },
            minDate: moment(),
        }, function (start, end, label) {
            context.end_date = start.format('YYYY-MM-DD');
        });

        $('.datepicker').daterangepicker({
            opens: 'left',
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'YYYY-MM-DD'
            },
            minDate: moment(),
        }, function (start, end, label) {
            //context.installment[0].installment_date = start.format('YYYY-MM-DD');
            context.installment[1].installment_date = start.format('YYYY-MM-DD');
        });
    },
    data: {
        fields: [
            {key: 'id', label: 'Order'},
            {key: 'project_name', label: 'Name'},
            {key: 'description', label: 'Description'},
            {key: 'human_start_date', label: 'Start Date'},
            {key: 'end_in', label: 'Ends In'},
            {key: 'price', label: 'Price'},
            {key: 'budget', label: 'Budget'},
            {key: 'type_text', label: 'Cost type'},
            {key: 'status_upper', label: 'Status'},
            {key: 'actions', label: 'Actions'}
        ],
        landingPages: [],
        landingPagesMeta: {},
        customers: [],
        staffMembers: [],
        currentUser: user_id,
        otherAssigneId: "",
        landingPageId: "",
        filter: {
            status: '',
        },
        costTypeArr: {
            one_off: 'One Off',
            monthly: 'Monthly'
        },
        costMonthlyArr: [1,2,3,4,5,6,7,8,9,10,11,12],
        isBusy: false,
        redirectUrl : redirect,
        submitFormUrl : apiURL+'/staff/other-projects/create',
        getStaffMembersUrl : apiURL + '/staff/other-projects/members',
        getCustomersUrl : apiURL + '/staff/other-projects/customers',
        getLandingPagesUrl : apiURL + '/staff/other-projects/listing',
        getProjectDetail : apiURL+'/staff/other-projects/edit',
        edit_id: '',
        project_name: '',
        cost_type: '',
        start_date: moment().format('YYYY-MM-DD'),
        end_date: moment().format('YYYY-MM-DD'),
        ends_in: '',
        description: '',
        price: '',
        budget: '',
        project_status: '',
        customer_id: '',
        staff_id: '',
        enable_installment:false,
        disableSubmitBtn:false,
        addButton:'d-none',
        installmentBlk:'d-none',
        installmentBlkM:'d-none',
        installment:[{installment_amount : 0, installment_date : moment().format('YYYY-MM-DD'), disabled: true, readonly: true, display: 'd-none'}, {installment_amount : 0, installment_date : moment().format('YYYY-MM-DD'), disabled: false, readonly: false, display: 'd-none'}],
    },
    watch: {
        'landingPagesMeta.current_page': function (val) {
            this.loadPaginatedData();
        },
        cost_type: function (val) {
            if (this.cost_type == 'monthly') {
                $('.end_date').addClass('d-none');
                $('.no_of_months').removeClass('d-none');
            } else {
                $('.no_of_months').addClass('d-none');
                $('.end_date').removeClass('d-none');
            }

            if (this.cost_type == 'one_off' && this.edit_id == '') {
                this.installmentBlkM = '';
            } else {
                this.enable_installment = false
                this.installmentBlkM = 'd-none';
            }

            if (this.edit_id != '') {
                $('.end_date').addClass('d-none');
                $('.no_of_months').addClass('d-none');
            }
        },
        enable_installment: function (val) {
            if (this.enable_installment && this.edit_id == '') {
                this.addButton = '';
                this.installmentBlk = '';
            } else {
                this.addButton = 'd-none';
                this.installmentBlk = 'd-none';
            }
        },
        installment: function (val) {
            if (this.enable_installment && this.edit_id == '') {
                this.installmentBlk = '';
            } else {
                this.installmentBlk = 'd-none';
            }
        },
    },
    methods: {
        loadPaginatedData: function () {
            this.getStaffLandingPages(this.getLandingPagesUrl + '?page=' + this.landingPagesMeta.current_page);
        },
        assignDatePicker: function () {
            let block = $('.installments').find('.datepicker');
            let length = block.length;
            let that = this;
            if (length > 0) {
                $.each(block, function( index, value ) {
                    $(this).daterangepicker({
                        opens: 'left',
                        singleDatePicker: true,
                        showDropdowns: true,
                        locale: {
                            format: 'YYYY-MM-DD'
                        },
                        minDate: moment(),
                    }, function (start, end, label) {
                        that.installment[index].installment_date = start.format('YYYY-MM-DD');
                    });
                });
            }
        },
        cloneBlock: function () {
            var obj = {
                installment_amount : 0, 
                installment_date:moment().format('YYYY-MM-DD'), 
                disabled: false, 
                readonly: false, 
                display: ''
            }
            if (this.installment.length < 3) {
                this.installment.push(obj);
            }

            if (this.installment.length >= 3) {
                this.addButton = 'd-none';
            } else {
                this.addButton = '';
            }
            let that  = this;
            setTimeout(function(){
                that.assignDatePicker();
            }, 500);
        },
        removeCloneBlock: function (key) {
            if (key > 0) {
                this.installment.splice(key, 1);
            }

            if (this.installment.length >= 3) {
                this.addButton = 'd-none';
            } else {
                this.addButton = '';
            }
        },
        getStaffLandingPages: function (url = false) {
            this.isBusy = true;
            if(url == false){
                url = this.getLandingPagesUrl;
            }
            axios.post(url, {filter: this.filter})
                .then(response => {
                    if(response.data.status == 'success'){
                        this.isBusy = false;
                        this.landingPages = response.data.data;
                        this.landingPagesMeta = response.data.meta;
                    }
                }).catch(error => {
                console.log(error);
                this.isBusy = false;
            });
        },
        getStaffMembers: function(){
          axios.get(this.getStaffMembersUrl)
                .then(response => {
                    if(response.data.status == 'success'){
                        this.staffMembers = response.data.data;
                    }
                }).catch(error => {
                    console.log(error);
          });
        },
        getCustomers: function(){
          axios.get(this.getCustomersUrl)
              .then(response => {
                  if(response.data.status == 'success'){
                      this.customers = response.data.data;
                  }
              }).catch(error => {
                  console.log(error);
          });
        },
        populateForm: function(id){
            if (id <= 0) {
                $('#staff_block').removeClass('d-none');
                $('#staff_block > select').attr('disabled', false);

                $('#customer_block').removeClass('d-none');
                $('#customer_block > select').attr('disabled', false);

                $('#price_block').removeClass('d-none');
                $('#price_block > input').attr('disabled', false);

                $('.disbale_edit').removeClass('d-none');
                $('.disbale_edit > select').attr('disabled', false);
                $('.disbale_edit > input').attr('disabled', false);

                setTimeout(function() {
                    $('.no_of_months').addClass('d-none');
                }, 500)

                this.edit_id = '';
                this.project_name = '';
                this.cost_type = '';
                this.start_date = moment().format('YYYY-MM-DD');
                this.end_date = moment().format('YYYY-MM-DD');
                this.ends_in = '';
                this.price = '';
                this.budget = '';
                this.description = '';
                this.project_status = '';
                this.customer_id = '';
                this.staff_id = '';

                $('#addLandingModal').modal('show');
            } else {
                axios.post(this.getProjectDetail, {id: id})
                .then(response => {
                    console.log(response);
                    let res = response.data;
                    if(res.status == 'success'){

                        $('#staff_block').addClass('d-none');
                        $('#staff_block > select').attr('disabled', true);

                        $('#customer_block').addClass('d-none');
                        $('#customer_block > select').attr('disabled', true);

                        $('#price_block').addClass('d-none');
                        $('#price_block > input').attr('disabled', true);

                        this.edit_id = id;
                        this.project_name = res.data.project_name;
                        this.cost_type = res.data.cost_type;
                        this.start_date = res.data.start_date;
                        this.end_date = res.data.end_date;
                        this.ends_in = res.data.no_of_months;
                        this.price = res.data.price;
                        this.budget = res.data.budget;
                        this.description = res.data.description;

                        this.enable_installment = false
                        this.installmentBlkM = 'd-none';

                        $('.disbale_edit').addClass('d-none');
                        $('.disbale_edit > select').attr('disabled', true);
                        $('.disbale_edit > input').attr('disabled', true);

                        //this.project_status = res.data.other_status;

                        /*$('#start_date').data('daterangepicker').setStartDate(res.data.picker_start_date);
                        $('#start_date').data('daterangepicker').setEndDate(res.data.picker_start_date);
                        $('#end_date').data('daterangepicker').setStartDate(res.data.picker_end_date);
                        $('#end_date').data('daterangepicker').setEndDate(res.data.picker_end_date);*/

                        $('#addLandingModal').modal('show');
                    } else {
                        let status = res.status.substr(0,1).toUpperCase()+res.status.substr(1);
                        Swal.fire({
                            title: status,
                            text: res.msg,
                            icon: res.status,
                            onAfterClose: () => {
                                //$('.add_project').trigger('click');
                            }
                        });
                    }
                }).catch(error => {
                    let status = res.status.substr(0,1).toUpperCase()+res.status.substr(1);
                    Swal.fire({
                        title: status,
                        text: res.msg,
                        icon: res.status,
                        onAfterClose: () => {
                            //$('.add_project').trigger('click');
                        }
                    });
                });
            }
        },
        submitForm(){
            this.disableSubmitBtn = true;
            let staffIds = [];
            let installment_date = [];
            let installment_amount = [];

            var enable_installment = 'N';
            if (this.enable_installment && this.cost_type == 'one_off') {
                enable_installment = 'Y';
            }

            _.forEach(this.staff_id, function(value, key) {
                staffIds.push(value.id);
            });

            _.forEach(this.installment, function(value, key) {
                installment_date.push(value.installment_date);
                installment_amount.push(value.installment_amount);
            });

            var customerId = '';
            if (this.customer_id != null && this.customer_id != '') {
                customerId = this.customer_id.id;
            }

            let formData = new FormData();
            formData.append('edit_id', this.edit_id);
            formData.append('project_name', this.project_name);
            formData.append('cost_type', this.cost_type);
            formData.append('start_date', this.start_date);
            formData.append('end_date', this.end_date);
            formData.append('ends_in', this.ends_in);
            formData.append('price', this.price);
            formData.append('description', this.description);
            formData.append('project_status', this.project_status);
            formData.append('budget', this.budget);
            formData.append('customer_id', customerId);
            formData.append('staff_id', staffIds);
            formData.append('installment_date', installment_date);
            formData.append('installment_amount', installment_amount);
            formData.append('enable_installment', enable_installment);

            let headersArr = {
                headers: {
                    //"X-CSRF-TOKEN": csrf,
                    //'Content-Type': 'multipart/form-data'
                }
            };

            axios.post( this.submitFormUrl,
                formData,
                headersArr
            ).then(response => {
                this.disableSubmitBtn = false;
                console.log(response);
                let res = response.data;
                if (res.status == 'success') {
                    let status = res.status.substr(0,1).toUpperCase()+res.status.substr(1);
                    Swal.fire({
                        title: status,
                        text: res.msg,
                        icon: res.status,
                        onAfterClose: () => {
                            location.reload();
                            this.getStaffLandingPages(false);
                            $('#addLandingModal').modal('hide');
                        }
                    });
                } else {
                    let status = res.status.substr(0,1).toUpperCase()+res.status.substr(1);
                    Swal.fire({
                        title: status,
                        text: res.msg,
                        icon: res.status
                    });
                }
            }).catch(error => {
                console.log(error);
                this.disableSubmitBtn = false;
            });
        },
        resetFilter: function(){
            this.filter = {
                status : ''
            };
            this.getStaffLandingPages(false);
        },
        numberWithComma: function(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        },
        Filtered(filteredItems) {
            // Trigger pagination to update the number of buttons/pages due to filtering
            this.landingPagesMeta.totalRows = filteredItems.length;
            this.landingPagesMeta.currentPage = 1;
        },
    }
});

@extends('staff.layouts.app')

@section('staff_css')
    <style>
        .invalid-feedback {
            display: block !important;
        }
        .daterangepicker {
            top: 260px !important;
        }
    </style>
@endsection

@section('staff_content')
    <div class="page-content grey-bg compaign-sec-bg-1" id="StaffLandingPage">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box resp-main-heading">
                        <!-- <div class="float-right">
                            <ol class="breadcrumb">
                                <button type="button" class="btn btn-primary text-uppercase">
                                    <i class="fas fa-file-pdf"></i>&nbsp;&nbsp; Generate PDF Report
                                </button>
                            </ol>
                        </div> -->
                        <h4 class="page-title overview-flot">Projects</h4>
                    </div>
                    <!--end page-title-box-->
                </div>
            </div>
            <div class="row top-search01">
                <div class="col-xl-12">
                    <!-- <div class="float-left">
                        <div class="btn-group custom-btn-group" role="group" aria-label="Basic example">
                            <a href="javascript:;" class="btn btn-camp" @click="getLandingByStatus('awaiting_build')">
                                Awaiting Build
                            </a>

                        </div>

                    </div> -->
                    <div class="float-left">
                        <a href="javascript:;" class="btn btn-add01 add_project" @click="populateForm(0)">
                            <i class="fa fa-plus"></i> Add New </a>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div class="table-responsive">
                        <template>
                            <div>
                                <b-table responsive :items="landingPages" :fields="fields" :busy="isBusy" show-empty class="t-light td-block text-left" @filtered="Filtered">

                                    <template v-slot:emptyfiltered="scope">
                                        <h4>@{{ scope.emptyFilteredText }}</h4>
                                    </template>
                                    <template v-slot:table-busy>
                                        <div class="text-center text-danger my-2">
                                            <b-spinner class="align-middle"></b-spinner>
                                            <strong>Loading...</strong>
                                        </div>
                                    </template>
                                    <template slot="HEAD_name" slot-scope="data">
                                        <span class="m-0 pl-2">@{{data.label}}</span>
                                    </template>


                                    <template v-slot:cell(id)="data">
                                        #@{{ data.item.id }}
                                    </template>
                                    <template v-slot:cell(project_name)="data">
                                        @{{ data.item.project_name }}
                                    </template>
                                    <template v-slot:cell(description)="data">
                                        <span v-if="data.item.description.length <  40 "> 
                                            @{{ data.item.description }}
                                        </span>
                                        <span v-else>
                                            @{{ data.item.description.substring(0,40)+".." }}
                                        </span>
                                    </template>
                                    <template v-slot:cell(human_start_date)="data">
                                        @{{ data.item.human_start_date }}
                                    </template>
                                    <template v-slot:cell(human_end_date)="data">
                                        @{{ data.item.human_end_date }}
                                    </template>
                                    <template v-slot:cell(price)="data">
                                        <span class="m-0 pl-2">
                                            <span v-if="data.item.budget != null">
                                                &#163;@{{numberWithComma(data.item.price)}}
                                            </span>
                                            <span v-else>&#163;0.00</span>
                                        </span>
                                    </template>
                                    <template v-slot:cell(budget)="data">
                                        <span class="m-0 pl-2">
                                            <span v-if="data.item.budget != null">
                                                &#163;@{{numberWithComma(data.item.budget)}}
                                            </span>
                                            <span v-else>&#163;0.00</span>
                                        </span>
                                    </template>
                                    <template v-slot:cell(type_text)="data">
                                        <span class="badge badge-boxed badge-soft-success">
                                            @{{ data.item.type_text }}
                                        </span>
                                    </template>
                                    <template v-slot:cell(status_upper)="data">
                                        <span class="badge badge-boxed badge-soft-success" v-if="data.item.status != 'pending'">
                                            @{{ data.item.status_upper }}
                                        </span>
                                        <span class="badge badge-boxed badge-soft-warning" v-else> 
                                            @{{ data.item.status_upper }}
                                        </span>
                                    </template>
                                    <template v-slot:cell(actions)="data">
                                        <span class="btn-group btn-flex">
                                            <b-button :data-edit_id='data.item.id' size="sm" title="View Detail" variant="btn btn-primary btn-sm p-1 px-2 mt-0 edit_project"  @click="populateForm(data.item.id)">
                                                <i class="fa fa-edit"></i>
                                            </b-button>
                                            <b-button :href="'/staff/projects/detail/' + data.item.id" size="sm" title="View Detail" variant="btn btn-primary btn-sm p-1 px-2 mt-0">
                                                <i class="fa fa-eye"></i>
                                            </b-button>
                                        </span>
                                    </template>
                                </b-table>
                                {{-- Row --}}
                                <div class="row  m-auto">
                                    <div class="col-xl-6 col-lg-6 col-md-6 float-left">
                                        <p class="mb-0 p-1 small mt-1 float-left">Showing @{{ landingPagesMeta.from }}
                                            to @{{ landingPagesMeta.to }} of @{{ landingPagesMeta.total }} records | Per
                                            Page: @{{ landingPagesMeta.per_page }} | Current Page: @{{
                                            landingPagesMeta.current_page }}</p>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-6 float-right t012">
                                        <b-pagination class="float-right" size="sm" :total-rows="landingPagesMeta.total"  v-model="landingPagesMeta.current_page" per-page="landingPagesMeta.per_page" first-text="First" prev-text="Previous" next-text="Next" last-text="Last" ellipsis-text="More" variant="danger">
                                        </b-pagination>
                                    </div>
                                </div>
                                {{-- End Row --}}
                            </div>
                        </template>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="addLandingModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4>Create Project</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: red">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="add_project">
                            <input type="hidden" value="" id="edit_id" name="edit_id" v-model="edit_id">
                            <div class="form-row">
                                <div class="form-group col-md-6 col-sm-6">
                                    <label for="usr">Project Name</label>
                                    <input type="text" class="form-control" name="project_name" id="project_name" v-model="project_name">
                                </div>

                                <div class="form-group col-md-6 col-sm-6 disbale_edit">
                                   <label for="usr">Cost type</label>
                                   <select class="custom-select" name="cost_type" @change="" id="cost_type" v-model="cost_type">
                                        <option value="" selected>Select Cost Type</option>
                                        <option v-for="(value, key) in costTypeArr" :key="key" :value="key">
                                            @{{ value }}
                                        </option>
                                   </select>
                                </div>

                                <div class="form-group col-md-6 col-sm-6 disbale_edit">
                                    <label for="usr">Start Date</label> 
                                    <div class="input-group">
                                        <input type="text" name="start_date" id="start_date" placeholder="Start Date" class="form-control" v-model="start_date"> 
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="dripicons-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-md-6 col-sm-6 end_date disbale_edit">
                                    <label for="usr">End Date</label> 
                                    <div class="input-group">
                                        <input type="text" name="end_date" id="end_date" placeholder="End Date" class="form-control" v-model="end_date"> 
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="dripicons-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-md-6 col-sm-6 no_of_months disbale_edit d-none">
                                    <label for="usr">No. of months</label> 
                                    <select class="custom-select" name="ends_in" id="ends_in" v-model="ends_in">
                                        <option value="" selected>Select No. of Months</option>
                                        <option v-for="(value, key) in costMonthlyArr" :key="value" :value="value">
                                            @{{ value }}
                                        </option>
                                   </select>
                                </div>
                                
                                <div class="form-group col-md-6 col-sm-6" id="price_block" >
                                   <label for="usr">Price</label>
                                   <input type="text" class="form-control" name="price" id="price" v-model="price">
                                </div>

                                <div class="form-group col-md-6 col-sm-6">
                                   <label for="usr">Budget</label>
                                   <input type="text" class="form-control" id="budget" name="budget" v-model="budget">
                                </div>

                                {{--<div class="form-group col-md-12 col-sm-6" >
                                   <label for="usr"> Project Status</label>
                                   <input type="text" class="form-control" name="project_status" id="project_status" v-model="project_status">
                                </div>--}}

                                <div class="form-group col-md-12">
                                   <label for="exampleFormControlTextarea1">Description</label>
                                   <textarea class="form-control" name="description" id="description" v-model="description" rows="3"></textarea>
                                </div>

                                <div class="form-group col-md-12" id="customer_block">
                                   <label for="usr">Customer</label>
                                   <multiselect name="customer_id" v-model="customer_id" :options="customers" :show-labels="false" placeholder="Select Customer" label="name" track-by="id">
                                        <template slot="noResult" slot-scope="data">
                                            <strong>Sorry, couldn't find customer with this name.</strong>
                                        </template>
                                    </multiselect>
                                </div>

                                <div class="form-group col-md-12" id="staff_block">
                                   <label for="staff_id">Staff Members</label>
                                    <multiselect name="staff_id" v-model="staff_id" id="staff_id" :options="staffMembers" :multiple="true" :close-on-select="false" :clear-on-select="false" :preserve-search="true" placeholder="Assign task to staff" label="name" track-by="id" :preselect-first="true">
                                        <template slot="noResult" slot-scope="data">
                                            <strong>Sorry, couldn't find staff member with this name.</strong>
                                        </template>
                                    </multiselect>
                                </div>

                                <div class="insatllment_blk col" :class="installmentBlkM">
                                    <div class="form-group">
                                        <div class="custom-control custom-switch">
                                            <input type="checkbox" v-model="enable_installment" class="custom-control-input" id="enable_installment">
                                            <label class="custom-control-label" for="enable_installment">Enable Installment</label>
                                            <button type="button" class="btn btn-xs btn-primary m-0 float-right" @click="cloneBlock()" :class="addButton"><i class="fas fa-plus"></i></button>
                                        </div>
                                    </div>
                                
                                    <div class="installments" :class="installmentBlk">
                                        <div class="form-group clone-blk" v-for="(item, index) in installment">
                                            <div class="row">
                                                <div class="col">
                                                    <label>Installment Amount <span class="count">@{{ index+1 }}</span>:</label>
                                                    <input type="text" class="form-control" v-model="installment[index].installment_amount" >
                                                </div>
                                                <div class="col">
                                                    <label>Installment Date <span class="count">@{{ index+1 }}</span>:</label>
                                                    <a href="javascript:;" class="text-danger float-right" :class="item.display"  @click="removeCloneBlock(index)">
                                                        <i class="fa fa-trash-alt"></i>
                                                    </a>
                                                    <input type="text" class="form-control datepicker date-input" v-model="installment[index].installment_date" :readonly="item.readonly" :disabled="item.disabled" @call>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-md-12">
                                   <button type="button" class="btn btn-primary" @click="submitForm" :disabled="disableSubmitBtn">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('staff_js')
    
    <script>
        var App_url = '{!! url()->current() !!}';
        var appUrl = '{!! url('') !!}';
        var apiURL = '{!! url('/api/') !!}';
        var redirect = "{!! route('staff.other_project.listing') !!}";
        var user_id = '{!! Auth::user()->id !!}';
    </script>
    {{--  Inject vue js files  --}}
    <script src="{{ asset('js/app-vue.js') }}"></script>
    <script src="{{ asset('js/staff/other_projects/vue.js') }}"></script>
    <script>
        $(function () {
            var csrf = $('meta[name="csrf-token"]').attr("content");
        });
    </script>
@endpush

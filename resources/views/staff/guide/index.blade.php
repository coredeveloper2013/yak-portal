@extends('staff.layouts.app')

@section('staff_css')
<link href='https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>
<style>
   .paginate_button{
      padding:0px !important;
   }
</style>
@endsection

@section('staff_content')
   <div class="page-content grey-bg">
      <div class="container-fluid">
         <div class="row">
            <div class="col-sm-12">
               <div class="page-title-box">
                  <!-- <div class="float-right">
                     <ol class="breadcrumb">
                        <button type="button" class="btn btn-primary text-uppercase">
                           <i class="fas fa-file-pdf"></i>&nbsp;&nbsp; Generate PDF Report
                        </button>
                     </ol>  
                  </div> -->
                  <h4 class="page-title">Guides</h4>
               </div>
               <!--end page-title-box-->
            </div>
         </div>
         <!--end col-->
         <div class="row top-search01">
            <div class="col-xl-12">
               <div class="float-left">
                  <div class="btn-group custom-btn-group" role="group" aria-label="Basic example">
                 <!--     <a href="#" class="btn btn-camp">Admin Users</a>
                     <a href="#" class="btn btn-camp">Staff Users </a>
                     <a href="#" class="btn btn-camp">Customer Users</a> 
                  --> </div>
               </div>
             @can('isAdmin')
               <div class="float-left">
                  <a href="javascript:void(0)" class="btn btn-add01" id="addNew"> <i class="fa fa-plus"></i> Add New </a>
               </div>
               @endcan
            </div>
         </div>
         <div class="row">
            <div class="col-xl-12">
               <div class="table-responsive">
                  <table class="table text-center td-block tableData">
                     <thead>
                        <tr class="th-head">
                           <th># </th>
                           <th>Title</th>
                           <th>Url</th>
                           <th>Status</th>
                           <th></th>
                        </tr>
                     </thead>
                     <tbody>
                       
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
      <!-- end page content -->
   </div>

   <!-- Modal -->
   <div class="modal fade" id="addNewModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h5>Add Guide</h5>
               <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <form id="addGuide" >
               <div class="modal-body">
                  <div class="form-group">
                     <label for="name">Title:</label>
                     <input type="text" class="form-control"   name="title">
                  </div>
                  <div class="form-group">
                     <label for="UserName">Description:</label>
                     <input type="text" class="form-control"   name="description">
                  </div>
                  <div class="form-group">
                     <label for="url">Url:</label>
                     <input type="text" class="form-control"   name="url">
                  </div>
                  <div class="form-group">
                     <label for="Password">Image:</label>
                     <input type="file" class="form-control" id="image"  name="image">
                  </div>
                  <div class="form-group">
                     <label for="landing_id">Status:</label>
                     <select class="form-control" name="status" required="">
                        <option disabled="" selected="">Select </option>
                          <option value="1">Active</option>
                            <option value="0">DeActive</option> 
                                   
                  </select>
                  </div>
                 
                  
               </div>
              <div class="modal-footer">
                 <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                 <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
              </div>
            </form>
         </div>
      </div>
   </div>
 <div class="modal fade" id="addNewModel1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h5>Edit Guide</h5>
               <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <form id="editGuide">
               <div class="modal-body">
                  <div class="form-group">
                     <label for="name">Title:</label>
                     <input type="hidden" class="form-control" id="id" name="id">
                     <input type="text" class="form-control" id="title" name="title">
                  </div>
                  <div class="form-group">
                     <label for="UserName">Description:</label>
                     <input type="text" class="form-control" id="description" name="description">
                  </div>
                  <div class="form-group">
                     <label for="email">Url:</label>
                     <input type="text" class="form-control" id="url" name="url">
                  </div>
                  <div class="form-group">
                     <label for="Password">Image:</label>
                     <input type="file" class="form-control" id="image" name="image">
                  </div>
                  <div class="form-group">
                     <label for="landing_id">Status:</label>
                     <select class="form-control" id="status" name="status" required="">
                        <option disabled="" selected="">Select Status</option>
                          <option value="1">Active</option>
                            <option value="0">DeActive</option> 
                                   
                  </select>
                  </div>
                 
                  
               </div>
              <div class="modal-footer">
                 <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                 <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
              </div>
            </form>
         </div>
      </div>
   </div>
    
@endsection

@push('staff_js')
<script>
   var appURL = '{!! url('/staff/') !!}';
   var apiURL = '{!! url('/api/staff/') !!}';
</script>
{{--  Inject vue js files  --}}
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('js/cmn/cmn.js') }}"></script>
<script src="{{ asset('staff/js/guide.js') }}"></script>
@endpush
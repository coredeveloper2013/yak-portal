@extends('staff.layouts.app')
@section('staff_content')
<!-- Page Content-->
<div class="page-content grey-bg compaign-sec-bg-1">
<div class="container-fluid">
   <div class="row">
      <div class="col-sm-12">
         <div class="page-title-box resp-main-heading">
            <div class="float-right">
               <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                      <button type="button" id="addNew" class="btn btn-primary text-uppercase">
                     Add Guide
                     </button>
                  </li>
               </ol>
            </div>
            <h4 class="page-title overview-flot">Guides</h4>
         </div>
         <!--end page-title-box-->
      </div>
      <!--end col-->
   </div>
   <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 p-20 contain-box">
      <div>
         <div class="card row" id="b-detail" >
            <div>
               <img class="card-img-top" id="card-img-top" src="https://img-prod-cms-rt-microsoft-com.akamaized.net/cms/api/am/imageFileData/RWptHL?ver=3e39&q=90&m=2&h=768&w=1024&b=%23FFFFFFFF&aim=true" >
               <span id="cl-detail"><i class="fas fa-times" ></i></span>
            </div>
            <div class="card-body">
               <h5 class="card-title" id="BGtitle">Card title</h5>
               <h6 class="card-subtitle mb-2 text-muted" id="BGupdate">Card subtitle</h6>
               <p class="" id="BGdescription">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
              <!--  <a href="#" class="card-link">Card link</a>
               <a href="#" class="card-link">Another link</a> -->
            </div>
         </div>
         <div class="row al-boxes" id="listG">





         </div>
      </div>
   </div>
   <!-- container -->
   @include('layouts.footer')
   <!--end footer-->
</div>
</div>
<!-- Modal -->
   <div class="modal fade" id="addNewModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h5>Add Guide</h5>
               <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <form id="addGuide" >
               <div class="modal-body">
                  <div class="form-group">
                     <label for="name">Title:</label>
                     <input type="text" class="form-control"   name="title">
                  </div>
                  <div class="form-group">
                     <label for="UserName">Description:</label>
                     <!--<input type="text" class="form-control"   name="description">-->
                       <textarea  id="description1" class="form-control"  rows="4" cols="50" name="description"></textarea>
                  </div>
                  <div class="form-group">
                     <label for="Password">Image:</label>
                     <input type="file" class="form-control" id="image"  name="image">
                  </div>
                  <div class="form-group">
                     <label for="landing_id">Status:</label>
                     <select class="form-control" name="status" required="">
                        <option disabled="" selected="">Select </option>
                          <option value="1">Active</option>
                            <option value="0">DeActive</option>

                  </select>
                  </div>


               </div>
              <div class="modal-footer">
                 <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                 <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
              </div>
            </form>
         </div>
      </div>
   </div>
 <div class="modal fade" id="addNewModel1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h5>Edit Guide</h5>
               <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <form id="editGuide">
               <div class="modal-body">
                  <div class="form-group">
                     <label for="name">Title:</label>
                     <input type="hidden" class="form-control" id="id" name="id">
                     <input type="text" class="form-control" id="title" name="title">
                  </div>
                  <div class="form-group">
                     <label for="UserName">Description:</label>
                     <!--<input type="text" class="form-control" id="description" name="description">-->
                   <textarea    class="form-control"  id="description"  rows="4" cols="50" name="description"></textarea>
                  </div>
                  <div class="form-group">
                     <label for="Password">Image:</label>
                     <input type="file" class="form-control" id="image" name="image">
                  </div>
                  <div class="form-group">
                     <label for="landing_id">Status:</label>
                     <select class="form-control" id="status" name="status" required="">
                        <option disabled="" selected="">Select Status</option>
                          <option value="1">Active</option>
                            <option value="0">DeActive</option>

                  </select>
                  </div>


               </div>
              <div class="modal-footer">
                 <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                 <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
              </div>
            </form>
         </div>
      </div>
   </div>
<!-- end page content -->
@endsection
@push('staff_js')
<script>
    var apiURL = '{!! url('/api/') !!}';
   var appURL = '{!! url('/staff/') !!}';
   var apiURL = '{!! url('/api/staff/') !!}';
    var GUIDES={!! $guides !!};

   $(document).ready(function(){
     // $(".s-detail").click(function(){
     //   $("#b-detail").css("display","block");
     //   $(".al-boxes").css("display","none");
     // });
     $("#cl-detail").click(function(){
       $("#b-detail").css("display","none");
       $(".al-boxes").css("display","flex");
     });
   });
</script>

{{--  Inject vue js files  --}}
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
<script src="{{ asset('js/cmn/cmn.js') }}"></script>
<script src="{{ asset('staff/js/guideList.js') }}"></script>
<script src="{{ asset('js/plugins/tinymce/tinymce.js') }}"></script>
 <!--<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>-->

       <script>
tinymce.init({
  selector: '#description',
   setup: function (editor) {
    editor.on('change', function () {
        tinymce.triggerSave();
     });


    },
});
tinymce.init({
  selector: '#description1',
   setup: function (editor) {
    editor.on('change', function () {
        tinymce.triggerSave();
     });


    },
});
    </script>
@endpush

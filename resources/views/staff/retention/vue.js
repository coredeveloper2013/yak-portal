new Vue({
    el: '#retentionContainer',
    mounted() {
        this.getSalesData(false);
    },
    data: {
        fields: [],
        sales: [],
        salesMeta: {},
        isBusy: false,
        dataLoaded: false,
        getSalesDataUrl : App_url + '/leaderboard',
        offset: 0,
        lastOffset: 0,
        totalRecords: 0,
        limit: 0,
    },
    watch: {
        'salesMeta.current_page': function (val) {
            this.loadPaginatedData();
        }
    },
    methods: {
        loadPaginatedData: function () {
            this.getSalesData(this.getSalesDataUrl + '?page=' + this.salesMeta.current_page);
        },
        getSalesData: function (url = false) {
            this.isBusy = true;
            if(url == false){
                url = this.getSalesDataUrl;
            }
            axios.post(url, {offset: this.offset})
                .then(response => {
                    console.log(response);
                    if(response.data.status == 'success'){
                        this.isBusy = false;
                        this.fields = response.data.fields;
                        this.sales = response.data.data;
                        this.dataLoaded = true;
                        this.totalRecords = response.data.total;
                        this.offset = response.data.offset;
                        this.limit = response.data.limit;
                        this.lastOffset = response.data.lastoffset;
                    }
                }).catch(error => {
                console.log(error);
                this.isBusy = false;
                this.dataLoaded = true;
            });
        },
        setLastOffset: function(){
            this.offset = this.lastOffset;
            this.getSalesData(false);
        },
        numberWithComma: function(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        },
        Filtered(filteredItems) {
            // Trigger pagination to update the number of buttons/pages due to filtering
            this.landingPagesMeta.totalRows = filteredItems.length;
            this.landingPagesMeta.currentPage = 1;
        },
    }
});

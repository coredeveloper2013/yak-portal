@extends('staff.layouts.app')

@section('staff_content')
    <!-- Page Content-->
    <div class="page-content" id="retentionContainer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box resp-main-heading">
                        <h4 class="page-title">Retention Overview</h4>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <div class="card mt-3">
                        <div class="card-body card-border">

                            <div class="float-left">

                                <h2 class="leaderboard-h">Leaderboard</h2>
                                <div style="color:#adacac">Your retention and refferal earnings over the last 30 days</div>

                            </div>
                            <div class="card-text table-responsive">
                                <table class="table bg-white align-center text-center mt-2 sale-table">
                                    <thead>
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col" class="font-weight-bold" v-for="field in fields">@{{ field }}
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="text-center font-weight-bold" v-for="sale in sales">
                                        <td v-if="sale.user_name != null">@{{ sale.user_name }}</td>
                                        <td v-else style="color: red">N/A</td>
                                        <td v-for="value in sale.reports" v-if="sale.reports"> @{{ value}}</td>
                                        <td v-else> 0</td>
                                    </tr>
                                    {{-- <tr>
                                        <a href="javascript:;" @click="getSalesData(false)" v-if="offset < totalRecords"><button type="button" class="btn btn-primary"> <i class="fas fa-arrow-right"></i> Load More</button></a>
                                        <a href="javascript:;" @click="setLastOffset" v-if="lastOffset < offset && limit != offset">
                                            <button type="button" class="btn btn-primary"><i class="fas fa-arrow-left"></i> Back</button></a>
                                    </tr> --}}
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12" v-for="(sale, index) in sales" :key="index">
                    <div class="card">
                        <div class="card-body card-border">
                            <h5 class="card-title graph-title">
                                @{{ sale.user_name }} - Retention
                            </h5>
                            {{--<p class="font-14 lightgray">Number of customers who have active subscription with you.</p>--}}
                            <div class="mb-3">
                                <div class="graph-container">
                                    <chart :labels="fields" :dashboard="'Retention'" :values="sale.chartValues" :uniqueid="index" v-if="dataLoaded">
                                    </chart>
                                </div>
                            </div>
                            <div>
                                <div class="float-left w-50 pa-5">
                                    <h4 class="ret-o">&#163;@{{ numberWithComma(sale.lastMonthSale) }}</h4>

                                    <div class="text-primary font-weight-bold"> Retention (last month)</div>
                                    {{--<div class="font-12 mt-2 lightgray">
                                        Customers who have upgraded the level of your product and services.
                                    </div>--}}
                                </div>
                                <div class="float-right w-50 pa-5">
                                    <h4 class="ret-o">&#163;@{{ numberWithComma(sale.currentMonthSale) }}</h4>

                                    <div class="text-danger font-weight-bold"> Retention (this month)</div>
                                    {{--<div class="font-12 mt-2 lightgray">
                                        Customers who have upgraded the level of your product and services.
                                    </div>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- container -->
            @include('staff.layouts.footer')
        </div>
        <!-- end page content -->
    </div>
    <!-- end page content -->
@endsection
@push('staff_js')
    <script>
        var App_url = '{!! url()->current() !!}';
    </script>
    {{--  Inject vue js files  --}}
    <script src="{{ asset('js/app-vue.js') }}"></script>
    <script src="{{ asset('js/staff/retention/vue.js') }}"></script>
@endpush

@extends('staff.layouts.app')

@section('staff_css')
<link href='https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>
<style>
   .paginate_button{
      padding:0px !important;
   }
</style>
@endsection

@section('staff_content')
   <div class="page-content grey-bg compaign-sec-bg-1">
      <div class="container-fluid">
         <div class="row">
            <div class="col-sm-12">
               <div class="page-title-box resp-main-heading">
                  <!-- <div class="float-right">
                     <ol class="breadcrumb">
                        <button type="button" class="btn btn-primary text-uppercase">
                           <i class="fas fa-file-pdf"></i>&nbsp;&nbsp; Generate PDF Report
                        </button>
                     </ol>  
                  </div> -->
                  <h4 class="page-title overview-flot">Faqs</h4>
               </div>
               <!--end page-title-box-->
            </div>
         </div>
         <!--end col-->
         <div class="row top-search01">
            <div class="col-xl-12">
               <div class="float-left">
                  <div class="btn-group custom-btn-group" role="group" aria-label="Basic example">
                  </div>
               </div>
               <div class="float-left">
                  <a href="javascript:void(0)" class="btn btn-add01" id="addNew"> <i class="fa fa-plus"></i> Add New </a>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-xl-12">
               <div class="table-responsive overflow-table">
                  <table class="table text-center td-block tableData">
                     <thead>
                        <tr class="th-head">
                           <th># </th>
                           <th>Question</th>
                           <th>Category</th>
                           <th>Status</th>
                           <th></th>
                        </tr>
                     </thead>
                     <tbody>
                       
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
      <!-- end page content -->
   </div>

   <!-- Modal -->
   <div class="modal fade" id="addNewModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h5>Add Faq</h5>
               <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <form id="addFaqs">
               <div class="modal-body">
                  <div class="form-group">
                     <label for="name">Question:</label>
                     <input type="text" class="form-control"   name="question">
                  </div>
                  <div class="form-group">
                     <label for="answer1">Answer:</label>
                     <!-- <input type="text" class="form-control"   name="answer"> -->
                 </div>
                  <div class="form-group">
                        <textarea  id="answer1" class="form-control"  rows="4" cols="50" name="answer"></textarea>
                      </div>
              <div class="form-group">
                <label for="isFrequent">Category</label>
                <select class="form-control" id='isFrequent' name="isFrequent" >
                  <option value='1'>Frequentlly</option>
                  <option value='0'>General</option>
                </select>
             </div>
             <div class="form-group">
                <label for="status">Status</label>
                <select class="form-control"  id='status' name='status'>
                  <option value='1'>Active</option>
                  <option value='0'>DeActive</option>
                </select>
             </div>
               </div>
              <div class="modal-footer">
                 <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                 <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
              </div>
            </form>
         </div>
      </div>
   </div>
 <div class="modal fade" id="addNewModel1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h5>Edit Faqs</h5>
               <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <form id="editFaqs">
               <div class="modal-body">
                  <div class="form-group">
                     <label for="name">Question:</label>
                     <input type="hidden" class="form-control" id="id" name="id">
                     <input type="text" class="form-control" id="question" name="question">
                  </div>
                  <div class="form-group">
                     <label for="answer">Answer:</label>
                     <!-- <input type="text" class="form-control" id="username" > -->
                  </div>
                  <div class="form-group">
                     <textarea class="form-control" id="answer" rows="4" cols="50" name="answer">
                     </textarea>
                   </div>
                  <div class="form-group">
              
                <label for="EisFrequent">Category</label>
                <select class="form-control" id='EisFrequent' name="isFrequent" >
                  <option value='1'>Frequentlly</option>
                  <option value='0'>General</option>
                </select>
             </div>
             <div class="form-group">
                <label for="Estatus">Status</label>
                <select class="form-control"  id='Estatus' name='status'>
                  <option value='1'>Active</option>
                  <option value='0'>DeActive</option>
                </select>
             </div>
               </div>
              <div class="modal-footer">
                 <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                 <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
              </div>
            </form>
         </div>
      </div>
   </div>
    
@endsection

@push('staff_js')
<script>
   var appURL = '{!! url('/staff/') !!}';
   var apiURL = '{!! url('/api/staff/') !!}';
</script>
{{--  Inject vue js files  --}}
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('js/cmn/cmn.js') }}"></script>
<script src="{{ asset('staff/js/faqs.js') }}"></script>
@endpush
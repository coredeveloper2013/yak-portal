new Vue({
    el: '#agreements',
    mounted() {
        this.getUserAgreements(false);
        var context = this;
        $('input[name="daterange"]').daterangepicker({
            opens: "center",
            maxDate: moment(),
            autoUpdateInput: false,
            orientation: "auto",
            locale: {
                cancelLabel: "Clear"
            }
        });
        $('input[name="daterange"]').on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("MM/DD/YYYY") + " - " + picker.endDate.format("MM/DD/YYYY"));
            context.filter.selectedStartDate = picker.startDate.format("MM/DD/YYYY");
            context.filter.selectedEndDate = picker.endDate.format("MM/DD/YYYY");
            context.getUserAgreements(false);
        });
        $('input[name="daterange"]').on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
            context.filter.selectedStartDate = "";
            context.filter.selectedEndDate = "";
            context.getUserAgreements(false);
        });
    },
    data: {
        fields: [
            {key: 'id', label: '#'},
            {key: 'name', label: 'Name'},
            {key: 'user.company', label: 'Business Name'},
            {key: 'email', label: 'Email'},
            {key: 'campaign', label: 'Campaign'},
            {key: 'docu_status', label: 'Document Status'},
            'action'
        ],
        agreements: [],
        agreementsMeta: {},
        filter: {
            selectedStartDate: '',
            selectedEndDate: '',
            status: '',
        },
        user: session_user,
        isBusy: false,
        markAsSignedUrl: apiURL + '/agreements/signed',
        getAgreementsUrl: apiURL + '/agreements/data',
        resendAgreementUrl: apiURL + '/agreements/resend'
    },
    watch: {
        'agreementsMeta.current_page': function (val) {
            this.loadPaginatedData();
        }
    },
    methods: {
        loadPaginatedData: function () {
            this.getUserAgreements(this.getAgreementsUrl + '?page=' + this.agreementsMeta.current_page);
        },
        getUserAgreements: function (url = false) {
            this.isBusy = true;
            if (url == false) {
                url = this.getAgreementsUrl;
            }
            axios.post(url, {filter: this.filter})
                .then(response => {
                    if (response.data.status == 'success') {
                        this.isBusy = false;
                        this.agreements = response.data.data;
                        this.agreementsMeta = response.data.meta;
                    }
                }).catch(error => {
                console.log(error);
                this.isBusy = false;
            });
        },
        resetFilter: function () {
            this.filter = {
                selectedStartDate: '',
                selectedEndDate: '',
                status: ''
            };
            $("#daterange").val('');
            this.getUserAgreements(false);
        },
        resendAgreement: function (agreement_id) {
            console.log(agreement_id);
            //return false;
            axios.post(this.resendAgreementUrl, {id: agreement_id})
                .then(response => {
                    if (response.data.status == 'success') {
                        this.getUserAgreements(false);
                        var resendTitle = 'Success';
                    } else {
                        var resendTitle = 'Error';
                    }
                    Swal.fire({
                        title: resendTitle,
                        text: response.data.msg,
                        icon: response.data.status
                    });
                }).catch(error => {
                console.log(error);
                this.isBusy = false;
            });
        },
        markAsSigned: function (agreement_id) {
            console.log(agreement_id);
            //return false;
            axios.post(this.markAsSignedUrl, {id: agreement_id})
                .then(response => {
                    if (response.data.status == 'success') {
                        this.getUserAgreements(false);
                        var resendTitle = 'Success';
                    } else {
                        var resendTitle = 'Error';
                    }
                    Swal.fire({
                        title: resendTitle,
                        text: response.data.msg,
                        icon: response.data.status
                    });
                }).catch(error => {
                console.log(error);
                this.isBusy = false;
            });
        },
        numberWithComma: function (x) {
            if (x != undefined &&x != "" && x != null) {
                return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            } else {
                return '0';
            }
        },
        Filtered(filteredItems) {
            // Trigger pagination to update the number of buttons/pages due to filtering
            this.agreementsMeta.totalRows = filteredItems.length;
            this.agreementsMeta.currentPage = 1;
        },
    }
});

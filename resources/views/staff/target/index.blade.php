@extends('staff.layouts.app')
@section('staff_css')
<!-- <link href='https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'> -->
<style>
    .paginate_button{
    padding:0px !important;
    }
</style>
@endsection
@section('staff_content')
<div class="page-content grey-bg compaign-sec-bg-1">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box resp-main-heading">
                    <div class="float-right">
                        <div class="form-group float-left w-55 mr-2">
                            <select class="form-control" name="year" id="businessyeard" required="">
                                <option disabled="" selected="">Select Business Year </option>
                                <option value="2020" selected >2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                                <option value="2026">2026</option>
                                <option value="2027">2027</option>
                                <option value="2028">2028</option>
                                <option value="2029">2029</option>
                                <option value="2030">2030</option>
                                <option value="2031">2031</option>
                            </select>
                        </div>
                    </div>
                    <h4 class="page-title target-style">Targets</h4>
                </div>
                <!--end page-title-box-->
            </div>
            <!--end col-->
        </div>
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mt-5 g-list" id="listG">
                {{--<div class="card mb-0">
                    <div class="">
                        <div class="head-t card-body border-bottom">
                            <div class="float-left">
                                <h4 class="page-title text-uppercase userName1">HAYLIE ORIOT</h4>
                            </div>
                            <div class="float-right">
                                <button type="button" onclick="addTarget(2)" class="btn btn-primary" id="addNew"><i class="fas fa-plus"></i> Add New</button> 
                            </div>
                        </div>
                        <div class="w-100 col-xl-8 col-lg-8 col-md-8 col-sm-8">
                            <div class="card-body">
                                <div class="table-responsive tble-business-holid">
                                    <table class="table text-center table-bordered holi-table td-block">
                                        <tbody>
                                            <tr class="first-border tr-font">
                                                <th></th>
                                                <th>New Business</th>
                                                <th>Retention</th>
                                                <th></th>
                                            </tr>
                                            <tr class="tr-font">
                                                <td class="text-left">JANUARY</td>
                                                <td>£451</td>
                                                <td>£45</td>
                                                <td>
                                                    <a href="#" class="edit" onclick="getItem(5)" data-toggle="m" data-placement="top" title="Edit">Edit</a>
                                                </td>
                                            </tr>
                                            <tr class="tr-font">
                                                <td class="text-left">FEBRUARY</td>
                                                <td>£451</td>
                                                <td>£45</td>
                                                <td>
                                                    <a href="#" class="edit" onclick="getItem(5)" data-toggle="m" data-placement="top" title="Edit">Edit</a>
                                                </td>
                                            </tr>
                                            <tr class="tr-font">
                                                <td class="text-left">MARCH</td>
                                                <td>£451</td>
                                                <td>£45</td>
                                                <td>
                                                    <a href="#" class="edit" onclick="getItem(5)" data-toggle="m" data-placement="top" title="Edit">Edit</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--}}
            </div>
            <!-- container -->
            <footer class="footer text-center text-sm-left">
                &copy; 2020 Yak-Portal <span class="text-muted d-none d-sm-inline-block float-right"></span>
            </footer>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="addNewModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5>Add Targets</h5>
                <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="addTarget" >
                <div class="modal-body">
                    <div class="form-group">
                        <label for="year">Business year:</label>
                        <select class="form-control" name="year" required="">
                            <option disabled="" selected="">Select </option>
                            <option value="2020">2020</option>
                            <option value="2021">2021</option>
                            <option value="2022">2022</option>
                            <option value="2023">2023</option>
                            <option value="2024">2024</option>
                            <option value="2025">2025</option>
                            <option value="2026">2026</option>
                            <option value="2027">2027</option>
                            <option value="2028">2028</option>
                            <option value="2029">2029</option>
                            <option value="2030">2030</option>
                            <option value="2031">2031</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="number_of_days">Month:</label>
                        <select class="form-control" name="month" required="">
                            <option disabled="" selected="">Select </option>
                            <option value="january">January</option>
                            <option value="feburary">February</option>
                            <option value="march">March</option>
                            <option value="april">April</option>
                            <option value="may">May</option>
                            <option value="june">June</option>
                            <option value="july">July</option>
                            <option value="august">August</option>
                            <option value="september">September</option>
                            <option value="october">October</option>
                            <option value="november">November</option>
                            <option value="december">December</option>
                            <!--      <option value="4">Day 4</option> 
                                <option value="5">Day 5</option> 
                                <option value="6">Day 6</option>  -->
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="new_business">New Business:</label>
                        <input type="number" class="form-control" min='0' name="new_business"
                            placeholder="£">
                    </div>
                    <div class="form-group">
                        <label for="reason">Retention:</label>
                        <input type="number" class="form-control" min='0' name="retention" 
                            placeholder="£">
                    </div>
                    <div class="form-group">
                        <input type="hidden" class="form-control"  id="userId" name="user"  >
                    </div>
                    <div class="form-group">
                        <label for="project">Project:</label> 
                        <input type="number" class="form-control" min='0' name="project" 
                            placeholder="£">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="addNewModel1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5>Edit Targets</h5>
                <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="editTarget" >
                <div class="modal-body">
                    <div class="form-group">
                        <label for="year">Business year:</label>
                        <select class="form-control" name="year" required="" id="year">
                            <option disabled="" selected="">Select </option>
                            <option value="2020">2020</option>
                            <option value="2021">2021</option>
                            <option value="2022">2022</option>
                            <option value="2023">2023</option>
                            <option value="2024">2024</option>
                            <option value="2025">2025</option>
                            <option value="2026">2026</option>
                            <option value="2027">2027</option>
                            <option value="2028">2028</option>
                            <option value="2029">2029</option>
                            <option value="2030">2030</option>
                            <option value="2031">2031</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="number_of_days">Month:</label>
                        <select class="form-control" id="month" name="month" required="">
                            <option disabled="" selected="">Select </option>
                            <option value="january">January</option>
                            <option value="feburary">February</option>
                            <option value="march">March</option>
                            <option value="april">April</option>
                            <option value="may">May</option>
                            <option value="june">June</option>
                            <option value="july">July</option>
                            <option value="august">August</option>
                            <option value="september">September</option>
                            <option value="october">October</option>
                            <option value="november">November</option>
                            <option value="december">December</option>
                            <!--      <option value="4">Day 4</option> 
                                <option value="5">Day 5</option> 
                                <option value="6">Day 6</option>  -->
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="new_business">New Business:</label>
                        <input type="hidden" id='idT' class="form-control"   name="id">
                        <input type="number" id="new_business" class="form-control" min='0' name="new_business"
                            placeholder="£">
                    </div>
                    <div class="form-group">
                        <label for="reason">Retention:</label>
                        <input type="number" class="form-control" id="retention"  name="retention" 
                            placeholder="£">
                    </div>
                    <div class="form-group">
                        <input type="hidden" class="form-control"  id="userId1" name="user" 
                            >
                    </div>
                    <div class="form-group">
                        <label for="project">Project:</label> 
                        <input type="number" class="form-control" id="project" min='0' name="project" 
                            placeholder="£">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm">Update changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@push('staff_js')
<script>
    var appURL = '{!! url('/staff/') !!}';
    var apiURL = '{!! url('/api/staff/') !!}';
</script>
{{--  Inject vue js files  --}}
<!-- 
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script> -->
<script src="{{ asset('js/cmn/cmn.js') }}"></script>
<script src="{{ asset('staff/js/targets.js') }}"></script>
@endpush
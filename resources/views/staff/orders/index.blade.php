@extends('staff.layouts.app')

@section('staff_content')
    <!-- Page Content-->
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">

                    <div class="page-title-box">
                        <!-- <div class="float-right">
                            <button type="button" class="btn btn-primary text-uppercase"><i class="fas fa-file-pdf"></i> Generate PDF Report</button>
                        </div> -->

                        <h4 class="page-title">Other Orders</h4>
                    </div>
                    <!--end page-title-box-->
                </div>
                <!--end col-->
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <div class="float-right">
                            <button type="button" class="btn btn-pink"><i class="fas fa-plus"></i> Create Project</button>
                        </div>
                        <h4 class="page-title">
                            <button type="button" class="btn btn-primary">All</button>
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-light">Ongoing</button>
                                <button type="button" class="btn btn-light">Finished</button>
                            </div>
                        </h4>
                    </div>
                    <!--end page-title-box-->
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="card card-border" >
                        <div class="card-body">
                            <h5 class="card-title">
                                <div class="float-left">App Design & Develop </div>
                                <div class="float-right"><span class="badge badge-success">Completed</span> </div>
                            </h5>
                            <p class="card-text text-limit">To achieve this. it would be necessary to have common words</p>
                            <div class="d-flex flex-row bd-highlight mb-3">
                                <div class="p-2 bd-highlight">
                                    <img src="{{asset('staff/images/user.jpg')}}" class="rounded-circle" alt="Cinque Terre" width="35" height="35">
                                </div>
                                <div class="p-2 bd-highlight">
                                    <img src="{{asset('staff/images/user.jpg')}}" class="rounded-circle" alt="Cinque Terre" width="35" height="35">
                                </div>
                                <div class="p-2 bd-highlight">
                                    <img src="{{asset('staff/images/user.jpg')}}" class="rounded-circle" alt="Cinque Terre" width="35" height="35">
                                </div>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="d-flex justify-content-between">
                                <div class="align-middle">
                                    <span>Created</span><br/>
                                    <div class="pt-1 float-left">
                                        <i class="fas fa-calendar-alt float-left .mt-1"></i>
                                    </div>
                                    <div class="float-left pl-1">13 Oct</div>
                                </div>
                                <div class="align-middle">
                                    <span>Due Date</span><br/>
                                    <div class="pt-1 float-left">
                                        <i class="fas fa-calendar-alt float-left .mt-1"></i>
                                    </div>
                                    <div class="float-left pl-1">13 Oct</div>
                                </div>
                                <div class="align-middle">
                                    <h4>$400</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="card card-border" >
                        <div class="card-body">
                            <h5 class="card-title">
                                <div class="float-left">App Design & Develop </div>
                                <div class="float-right"><span class="badge badge-success">Completed</span> </div>
                            </h5>
                            <p class="card-text text-limit">To achieve this. it would be necessary to have common words</p>
                            <div class="d-flex flex-row bd-highlight mb-3">
                                <div class="p-2 bd-highlight">
                                    <img src="{{asset('staff/images/user.jpg')}}" class="rounded-circle" alt="Cinque Terre" width="35" height="35">
                                </div>
                                <div class="p-2 bd-highlight">
                                    <img src="{{asset('staff/images/user.jpg')}}" class="rounded-circle" alt="Cinque Terre" width="35" height="35">
                                </div>
                                <div class="p-2 bd-highlight">
                                    <img src="{{asset('staff/images/user.jpg')}}" class="rounded-circle" alt="Cinque Terre" width="35" height="35">
                                </div>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="d-flex justify-content-between">
                                <div class="align-middle">
                                    <span>Created</span><br/>
                                    <div class="pt-1 float-left">
                                        <i class="fas fa-calendar-alt float-left .mt-1"></i>
                                    </div>
                                    <div class="float-left pl-1">13 Oct</div>
                                </div>
                                <div class="align-middle">
                                    <span>Due Date</span><br/>
                                    <div class="pt-1 float-left">
                                        <i class="fas fa-calendar-alt float-left .mt-1"></i>
                                    </div>
                                    <div class="float-left pl-1">13 Oct</div>
                                </div>
                                <div class="align-middle">
                                    <h4>$400</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="card card-border" >
                        <div class="card-body">
                            <h5 class="card-title">
                                <div class="float-left">App Design & Develop </div>
                                <div class="float-right"><span class="badge badge-success">Completed</span> </div>
                            </h5>
                            <p class="card-text text-limit">To achieve this. it would be necessary to have common words</p>
                            <div class="d-flex flex-row bd-highlight mb-3">
                                <div class="p-2 bd-highlight">
                                    <img src="{{asset('staff/images/user.jpg')}}" class="rounded-circle" alt="Cinque Terre" width="35" height="35">
                                </div>
                                <div class="p-2 bd-highlight">
                                    <img src="{{asset('staff/images/user.jpg')}}" class="rounded-circle" alt="Cinque Terre" width="35" height="35">
                                </div>
                                <div class="p-2 bd-highlight">
                                    <img src="{{asset('staff/images/user.jpg')}}" class="rounded-circle" alt="Cinque Terre" width="35" height="35">
                                </div>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="d-flex justify-content-between">
                                <div class="align-middle">
                                    <span>Created</span><br/>
                                    <div class="pt-1 float-left">
                                        <i class="fas fa-calendar-alt float-left .mt-1"></i>
                                    </div>
                                    <div class="float-left pl-1">13 Oct</div>
                                </div>
                                <div class="align-middle">
                                    <span>Due Date</span><br/>
                                    <div class="pt-1 float-left">
                                        <i class="fas fa-calendar-alt float-left .mt-1"></i>
                                    </div>
                                    <div class="float-left pl-1">13 Oct</div>
                                </div>
                                <div class="align-middle">
                                    <h4>$400</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="card card-border" >
                        <div class="card-body">
                            <h5 class="card-title">
                                <div class="float-left">App Design & Develop </div>
                                <div class="float-right"><span class="badge badge-success">Completed</span> </div>
                            </h5>
                            <p class="card-text text-limit">To achieve this. it would be necessary to have common words</p>
                            <div class="d-flex flex-row bd-highlight mb-3">
                                <div class="p-2 bd-highlight">
                                    <img src="{{asset('staff/images/user.jpg')}}" class="rounded-circle" alt="Cinque Terre" width="35" height="35">
                                </div>
                                <div class="p-2 bd-highlight">
                                    <img src="{{asset('staff/images/user.jpg')}}" class="rounded-circle" alt="Cinque Terre" width="35" height="35">
                                </div>
                                <div class="p-2 bd-highlight">
                                    <img src="{{asset('staff/images/user.jpg')}}" class="rounded-circle" alt="Cinque Terre" width="35" height="35">
                                </div>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="d-flex justify-content-between">
                                <div class="align-middle">
                                    <span>Created</span><br/>
                                    <div class="pt-1 float-left">
                                        <i class="fas fa-calendar-alt float-left .mt-1"></i>
                                    </div>
                                    <div class="float-left pl-1">13 Oct</div>
                                </div>
                                <div class="align-middle">
                                    <span>Due Date</span><br/>
                                    <div class="pt-1 float-left">
                                        <i class="fas fa-calendar-alt float-left .mt-1"></i>
                                    </div>
                                    <div class="float-left pl-1">13 Oct</div>
                                </div>
                                <div class="align-middle">
                                    <h4>$400</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="card card-border" >
                        <div class="card-body">
                            <h5 class="card-title">
                                <div class="float-left">App Design & Develop </div>
                                <div class="float-right"><span class="badge badge-success">Completed</span> </div>
                            </h5>
                            <p class="card-text text-limit">To achieve this. it would be necessary to have common words</p>
                            <div class="d-flex flex-row bd-highlight mb-3">
                                <div class="p-2 bd-highlight">
                                    <img src="{{asset('staff/images/user.jpg')}}" class="rounded-circle" alt="Cinque Terre" width="35" height="35">
                                </div>
                                <div class="p-2 bd-highlight">
                                    <img src="{{asset('staff/images/user.jpg')}}" class="rounded-circle" alt="Cinque Terre" width="35" height="35">
                                </div>
                                <div class="p-2 bd-highlight">
                                    <img src="{{asset('staff/images/user.jpg')}}" class="rounded-circle" alt="Cinque Terre" width="35" height="35">
                                </div>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="d-flex justify-content-between">
                                <div class="align-middle">
                                    <span>Created</span><br/>
                                    <div class="pt-1 float-left">
                                        <i class="fas fa-calendar-alt float-left .mt-1"></i>
                                    </div>
                                    <div class="float-left pl-1">13 Oct</div>
                                </div>
                                <div class="align-middle">
                                    <span>Due Date</span><br/>
                                    <div class="pt-1 float-left">
                                        <i class="fas fa-calendar-alt float-left .mt-1"></i>
                                    </div>
                                    <div class="float-left pl-1">13 Oct</div>
                                </div>
                                <div class="align-middle">
                                    <h4>$400</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="card card-border" >
                        <div class="card-body">
                            <h5 class="card-title">
                                <div class="float-left">App Design & Develop </div>
                                <div class="float-right"><span class="badge badge-success">Completed</span> </div>
                            </h5>
                            <p class="card-text text-limit">To achieve this. it would be necessary to have common words</p>
                            <div class="d-flex flex-row bd-highlight mb-3">
                                <div class="p-2 bd-highlight">
                                    <img src="{{asset('staff/images/user.jpg')}}" class="rounded-circle" alt="Cinque Terre" width="35" height="35">
                                </div>
                                <div class="p-2 bd-highlight">
                                    <img src="{{asset('staff/images/user.jpg')}}" class="rounded-circle" alt="Cinque Terre" width="35" height="35">
                                </div>
                                <div class="p-2 bd-highlight">
                                    <img src="{{asset('staff/images/user.jpg')}}" class="rounded-circle" alt="Cinque Terre" width="35" height="35">
                                </div>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="d-flex justify-content-between">
                                <div class="align-middle">
                                    <span>Created</span><br/>
                                    <div class="pt-1 float-left">
                                        <i class="fas fa-calendar-alt float-left .mt-1"></i>
                                    </div>
                                    <div class="float-left pl-1">13 Oct</div>
                                </div>
                                <div class="align-middle">
                                    <span>Due Date</span><br/>
                                    <div class="pt-1 float-left">
                                        <i class="fas fa-calendar-alt float-left .mt-1"></i>
                                    </div>
                                    <div class="float-left pl-1">13 Oct</div>
                                </div>
                                <div class="align-middle">
                                    <h4>$400</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page content -->
@endsection
@push('staff_js')
@endpush

@extends('staff.layouts.app')
@section('staff_css')
<style>
   .edit_lead {
      display: none;
   }
   .chat-body {
       padding: 12px 0 10px 0 !important;
   }

  .chat-box-left-staff .active-chat .new-message::before {
    display: none;
  }

</style>
@endsection
@section('staff_content')

<div class="page-content grey-body-bg lead-bg-resp" >
   <div class="row">
      <div class="col-12">
         <div class="chat-box-left chat-box-left-staff">
            <h4 class="chat-head clearfix">
               <span class="mt-2 float-left">My Leads </span>
               <button data-toggle="modal" data-target="#addNewModel" class="btn btn-xs btn-primary add_lead m-0 float-right">Add Lead</button>
            </h4>

            <div class="chat-search">
               <div class="form-group">
                  <div class="input-group chat-search">
                     <input type="text" id="chat-search" name="search" class="form-control chat-search-input"
                        placeholder="Search">
                     <span class="input-group-append">
                     <button type="button" onclick="getAll()"class="btn btn-grey1 shadow-none"><i class="fas fa-search"></i></button>
                     </span>
                  </div>
               </div>
               <div class="form-group mb-2 ml-2 mr-2">
                    @if($session_user->type=="admin")
                        <select class="form-control" id="user-id" onchange="getAll('yes')">
                            <option selected value="-1">All</option>
                            <option value="0">Unassigned</option>
                            @foreach($users as $user)
                                <option value="{{$user->id}}">{{$user->name}}</option>
                            @endforeach
                        </select>
                    @else
                        <input type="hidden" class="form-control"  value="{{$session_user->id}}" id="user-id">
                    @endif
                </div>

                @if($session_user->type != "admin")
                    <div class="form-group mb-2 ml-1 mr-1">
                        <div class="row">
                          <div class="col-md-6">
                              <button class="btn btn-grey btn-block linked yes active" data-linked='yes'> Assigned</button>
                          </div>

                          <div class="col-md-6">
                              <button class="btn btn-grey btn-block linked no" data-linked='no'> Unassigned</button>
                          </div>
                        </div>
                    </div>
                @endif
            </div>
            <!--end chat-search-->
            @if($session_user->type=="admin")
                <div class="tab-content chat-list chat-list-search" id="">
                   <div class="tab-pane fade show active" id="listG">
                   </div>
                </div>
            @else
                <div class="tab-content chat-list chat-list-search" id="">
                   <div class="tab-pane fade show active" id="listG">
                   </div>
                </div>
            @endif
            <!--end tab-content-->
         </div>
         <!--end chat-box-left -->
         <div class="chat-box-right">
            <div class="text-right p-2 pt-3">
               <div class="media-body overflow">
                  <div class="chat-features">
                     <div class="d-sm-inline-block form-inline">
                        <div class="">
                           <span id="selecterButton" style="display: none">
                              @if($session_user->type != "admin")
                                  <select class="form-control" id="assign_to" onchange="assign()">
                                      <option value="">Select Staff Member</option>
                                      @foreach($users as $user)
                                          <option value="{{$user->id}}">{{$user->name}}</option>
                                      @endforeach
                                  </select>
                              @endif
                             <button type="button" onclick="editDeal()" class="btn btn-success">Edit Lead</button>
                             <button type="button" onclick="activeLeads()" class="btn btn-primary move_to_deals">Move to Deals</button>
                             <button type="button" onclick="deleteDeal()" class="btn btn-danger">Delete Lead</button>
                           </span>
                           <span id="selecterMove" style="display: none">
                              <form id="statuschange">
                                 <input type="hidden" class="form-control"    id="idd" name="id">
                                 <select class="form-control" id="status" name="status">
                                    <option disabled="" selected="">Move to</option>
                                    @foreach($listStatus as $deal_key => $deal_val)
                                    <option value="{{$deal_key}}">{{$deal_val}}</option>
                                    @endforeach
                                 </select>
                              </form>
                           </span>
                        </div>
                     </div>
                  </div>
                  <h6 class="m-0 p-0 chat-title-name" id="lLeadName">Leads Name</h6>
               </div>
            </div>
            <!-- end chat-header -->
            <div class="chat-body ">
               <div class="detail-wrap1 d-none mt-0 mb-2">
                  <div class="row bt-1 question-body">
                  </div>
                   <!-- <a href="javascript:void(0)" id="btn-details-load" onclick="showMore()" class="float-right mt-2 font-weight-bold">View more </a> -->
               </div>
               <div class="text-right mb-2">
               <a href="javascript:void(0)" id="btn-details-load" style="display: none;" onclick="showMore()" class=" font-weight-bold pr-2">View more </a>
             </div>
               <!--end card-body-->

                   <div class="card-body bg-white" id="detail-wrap2">
                     <div class="detail-wrap">
                        <div class="row" style="height: 300px ">
                           <div class="col-12 text-center pt-4 mt-4">
                              <h2 class="chat-detail-head mb-0">Doesn't have any leads yet!</h2>
                              <p class="subtext">If you have received a lead notification but the lead isn't here please let us know as soon as you can.</p>

                           </div>
                        </div>
                     </div>

                   </div>

               <!--end card-body-->

               <ul class="user-comment-sec charHistory lead-detail-coment" id="usercommnts">
               </ul>
               <!--end card-->
            </div>
            <!-- end chat-body -->
            <div class="chat-footer chat-footer-custom footer-custom-resp">
               <form id="sendMessage" style="display: none">
                  <div class="">
                     <div class="txt-area-sec">
                        <textarea class="chat-input" name="message"></textarea>
                        <input type="hidden" id="lead_id" name="lead_id">
                     </div>
                     <input type="submit" class="btn btn-send" value="SEND">
                  </div>

               </form>
               <!-- end row -->
            </div>
            <!-- end chat-footer -->
         </div>
         <!--end chat-box-right -->
      </div>
      <!-- end col -->
   </div>


   <div class="modal fade" id="addNewModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h5>Add Leads</h5>
               <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <form id="addLeads" >
               <div class="modal-body">
                  <div class="row">
                     <div class="col-xl-12">
                        <div class="form-group">
                           @if($session_user->type=="admin")
                             <label for="userId">User:</label>
                             <select class="form-control" name="user_id" required="">
                                <option disabled="" selected="">Select </option>
                                @foreach($users as $user)
                                <option value="{{$user->id}}">{{$user->name}}</option>
                                @endforeach
                             </select>
                           @else
                             <input type="hidden" class="form-control"  value="{{$session_user->id}}" name="user_id">
                           @endif
                        </div>
                     </div>

                     <div class="col-xl-12">
                        <div class="form-group">
                          <label for="name" class="font-weight-bold black-text w-100">
                              Lead Name
                           </label>
                           <input type="text" class="form-control heading" name="lead_name" aria-describedby="Heading" placeholder="Lead Name">
                        </div>
                     </div>
                  </div>

                  <div class="append_block">
                     <div class="question_block">
                        <div class="form-group mb-2">
                           <label for="name" class="font-weight-bold black-text w-100">
                              Question <spna class="q_count">1</spna>
                              <a href="javascript:;" class="text-danger float-right remove_block d-none"><i class="far fa-trash-alt"></i></a>
                           </label>
                           <input type="text" class="form-control heading" name="question[]" aria-describedby="Heading" placeholder="Question">
                        </div>
                        <div class="form-group">
                           <input type="phone" class="form-control answer" name="answer[]" aria-describedby="Answer" placeholder="Answer">
                        </div>
                        <hr style="width: 50%">
                     </div>
                  </div>
                  <div class="form-group clearfix text-center">
                     <span class="clone_block btn btn-xs btn-primary m-0"><i class="fas fa-plus"></i> Add Question</span>
                  </div>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
               </div>
            </form>
         </div>
      </div>
   </div>

   <div class="modal fade" id="editNewModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
   aria-hidden="true">
     <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
           <div class="modal-header">
              <h5>Edit Lead</h5>
              <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              </button>
           </div>
           <form id="editLeadsForm" >
              <input type="hidden" class="form-control" id="leadid" name="lead_id">
              <div class="modal-body">
                 <div class="row" id="edit-select"></div>
                 <div id="edit-form"></div>
                 <div id="edit-form-add-q"></div>
              </div>
              <div class="modal-footer">
                 <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                 <button type="submit" class="btn btn-primary btn-sm">Update changes</button>
              </div>
           </form>
        </div>
     </div>
  </div>
    <div class="modal" id="detailsModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="addEditLeadForm">
                    <div class="modal-header">
                        <h5 class="modal-title">Lead Details</h5>
                        <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <div class="detail-wrap detail-box-style detail-over-flow" id="modal-details-wrap"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @include('layouts.footer')
</div>

@endsection
@push('staff_js')
<script>
   var appURL = '{!! url('/staff/') !!}';
   var apiURL = '{!! url('/api/staff/') !!}';
   var userType = '{!! $session_user->type !!}';
   var userId = '{!! $session_user->id !!}';

   // console.log(userType);
</script>
<script src="{{ asset('js/cmn/cmn.js') }}"></script>
<script src="{{ asset('staff/js/leads.js') }}"></script>
@endpush

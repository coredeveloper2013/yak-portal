@extends('staff.layouts.app')
@section('staff_css')

@endsection
@section('staff_content')

<div class="page-content">
   <div class="container-fluid">
   <!-- table -->
   <div class="section-filter-table">
          <!-- <div class="row d-flex justify-content-between">
              <button class="lead-btn">Add Leads</button>
              <div class="form-group filter-c float-right mb-0 filter-style">
                  <select class="form form-control form-control-sm mt-3 option-text">
                     <option value="" >Everyone</option>
                     <option value="">Completed</option>
                     <option value="">Pending</option>
                  </select>
               </div>
          </div> -->
          <div class="table-responsive">
            <div class="page-title-box text-center">
                <h4 class="page-title">Leads</h4>
            </div>
               
                   <!-- The Modal -->
                   
 <div class="modal fade" id="addNewModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h5>Add Leads</h5>
               <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <form id="addLeads" >
               <div class="modal-body">
                  <div class="form-group">
                     <label for="name">Name:</label>
                     <input type="text" class="form-control"   name="name">
                  </div>
                  <div class="form-group">
                     <label for="budget">Budget:</label>
                     <input type="number" min="0" class="form-control"   name="budget">
                  </div>
                  <div class="form-group">
                     <label for="source">Source:</label>
                     <input type="text" class="form-control"   name="source">
                  </div>
                   @if(Auth::user()->type=="admin")
                  <div class="form-group">
                     <label for="userId">User:</label>
                     <select class="form-control" name="user_id" required="">
                        <option disabled="" selected="">Select </option>
                          @foreach($users as $user)
                          <option value="{{$user->id}}">{{$user->name}}</option>
                          @endforeach
                                   
                  </select>
                  </div>
                 @else
                      <input type="hidden" class="form-control"  value="{{Auth::user()->id}}" name="user_id">
                @endif
                 
                  
               </div>
              <div class="modal-footer">
                 <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                 <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
              </div>
            </form>
         </div>
      </div>
   </div>
<div class="modal fade" id="addNewModel1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h5>Edit Leads</h5>
               <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <form id="editLeads" >
               <div class="modal-body">
                  <div class="form-group">
                     <label for="name">Name:</label>
                     <input type="text" class="form-control"   name="name">
                  </div>
                  <div class="form-group">
                     <label for="budget">Budget:</label>
                     <input type="number" min="0" class="form-control"   name="budget">
                  </div>
                  <div class="form-group">
                     <label for="source">Source:</label>
                     <input type="text" class="form-control"   name="source">
                  </div>
                   
                 
                  
               </div>
              <div class="modal-footer">
                 <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                 <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
              </div>
            </form>
         </div>
      </div>
   </div>

          <table class="table">
              <thead>
                  <tr class="head-table-2">
                      <th><button class="lead-btn" data-toggle="modal" data-target="#addNewModel">Add Leads</button></th>
                      <th></th>
                      <th></th>
                      <!-- <th>
                        <div class="row d-flex justify-content-between">
                            <!-- <button class="lead-btn">Add Leads</button> -->
                            <!-- <div class="form-group filter-c float-right mb-0 filter-style">
                                <select class="form form-control form-control-sm mt-3 option-text"> <i class="fas fa-wifi signal-style"></i>
                                   <option value="" >Everyone</option>
                                   <option value="">Completed</option>
                                   <option value="">Pending</option>
                                </select>
                             </div>
                        </div>
                      </th> -->
                  </tr>
                  <tr class="head-table-1">
                      <th>Name</th>
                      <th>Budget</th>
                      <th>Source</th>
                      <th>Created</th>
                  </tr>
              </thead>
              <tbody>
              	@foreach($leads_data as $ld_key => $ld_val)
					<tr class="table-body-content">
						<td>
							<!-- <i class="fas fa-circle font-a-color"></i> -->
              {{$ld_val['name']}}
						</td>
						<td>
							£{{$ld_val['budget']}}
						</td>
						<td>
							{{$ld_val['source']}}
						</td>
						<td>
							{{ $ld_val['created_at']->format('d F Y') }}
						</td>
					</tr>
              	@endforeach            
              </tbody>
          </table>
      </div>
      </div>
        <!-- table end -->
   </div>
</div>



@endsection
@push('staff_js')
<script>
   var appURL = '{!! url('/staff/') !!}';
   var apiURL = '{!! url('/api/staff/') !!}';
</script>
<script src="{{ asset('js/cmn/cmn.js') }}"></script>
<script src="{{ asset('staff/js/leads.js') }}"></script>
@endpush
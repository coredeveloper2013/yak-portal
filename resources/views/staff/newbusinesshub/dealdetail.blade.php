@extends('staff.layouts.app')
@section('staff_css')
<style>
    .edit_lead {
    display: none;
    }
    .chat-body {
    padding: 0 0 10px 0px !important;
    }
     .chat-box-right .chat-footer {
            left: 4px;
         }
</style>
@endsection
@section('staff_content')
<div class="page-content grey-body-bg">
<div class="row">
    <div class="col-12">
        <!--end chat-box-left -->
        <div class="chat-box-right ml-0 pl-1">
            <div class="chat-header p-2" style="min-height: auto">
                <div class="media-body overflow">
                    <div class="chat-features float-right">
                        <div class="d-sm-inline-block">
                            <div class="form-group float-right mb-0 form-inline">
                                <span id="">
                                    <form id="statuschange">
                                        <input type="hidden" class="form-control"  value="{{$leads[0]->id}}"  id="idd" name="id">
                                        <button type="button" onclick="editDeal()" class="btn btn-success">Edit Deal</button>
                                        <button type="button" onclick="deleteDeal()" class="btn btn-danger">Delete Deal</button>
                                        <select class="form-control" id="status" value="{{ ($leads[0]->status)}}" name="status">
                                            @foreach($list as $deal_key => $deal_val)
                                                <option value="{{$deal_key}}" {{$deal_key==$leads[0]->status?'selected':''}}>{{$deal_val}}</option>
                                            @endforeach
                                        </select>
                                    </form>
                                </span>
                            </div>
                        </div>
                    </div>
                    <h4 class="m-0 p-0 chat-title-name float-left mt-2">
                      @if(!empty($leads[0]->name))
                          {{ $leads[0]->name }}
                      @else
                        Lead No #{{ @$leads[0]->id }}
                      @endif
                    </h4>
                </div>
            </div>
            <!-- end chat-header -->
            <div class="chat-body ">

                @if ($leads[0]->questions)
                <div class="detail-wrap1 detail-wrap-box ">
                    <div class="row bt-1">
                        @php
                          $i = 0;
                          $modal_html = '';
                        @endphp
                        @foreach($leads[0]->questions as $key => $val)
                            @if($i < 3)
                            <div class="col-xl-4 col-lg-4 leads-col">
                                <div class="heading-title-leads">{{ $val->question }} </div>
                                <span id="">{{ $val->answer }}</span>
                            </div>
                            @endif
                            @php  $modal_html = $modal_html . '<div class="col-xl-12 col-lg-12 lead-col-b">
                                <div class="heading-title-leads">'.$val->question.'</div>
                                <span id="">'.$val->answer.'</span>
                            </div>';

                                $i++;
                              @endphp
                        @endforeach

                    </div>
                </div>
                @if(count($leads[0]->questions) > 3)
                        <div class="text-right mb-2 mt-1">
                            <a href="javascript:void(0)" id="btn-details-load" onclick="showMore()" class="mt-2 font-weight-bold">View more </a>
                        </div>
                        @endif
                @endif
                <!--end card-body-->
                <ul class="user-comment-sec charHistory lead-detail-coment mt-3" id="usercommnts">
                </ul>
                <!--end card-->
            </div>
            <!-- end chat-body -->
            <div class="chat-footer chat-footer-custom">
                <form id="sendMessage">
                    <div class="">
                       <div class="txt-area-sec">
                          <textarea class="chat-input chat-input-1" name="message"></textarea>
                         <input type="hidden" id="lead_id"  value="{{($leads[0]->id)}}" name="lead_id">
                       </div>
                       <input type="submit" class="btn btn-send" value="SEND">
                    </div>

                </form>

                <!-- end row -->
            </div>
            <!-- end chat-footer -->
        </div>
        <!--end chat-box-right -->
    </div>
    <!-- end col -->
</div>
    <div class="modal" id="detailsModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="addEditLeadForm">
                    <div class="modal-header">
                        <h5 class="modal-title">Lead Details</h5>
                        <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <div class="detail-wrap detail-box-style" id="modal-details-wrap">
                            {!! @$modal_html !!}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<div class="modal fade" id="editNewModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
aria-hidden="true">
<div class="modal-dialog modal-dialog-centered" role="document">
   <div class="modal-content">
      <div class="modal-header">
         <h5>Edit Lead</h5>
         <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
         <span aria-hidden="true">&times;</span>
         </button>
      </div>
      <form id="editLeadsForm">
        <input type="hidden" class="form-control" id="leadid" value="{{$leads[0]->id}}" name="lead_id">
         <div class="modal-body">
            <div class="row">
               <div class="col-xl-12">
                  <div class="form-group">
                     @if(Auth::user()->type=="admin")
                     <label for="userId">User:</label>
                     <select class="form-control" name="user_id" required="">
                        <option disabled="">Select</option>
                        @foreach($users as $user)
                        <option @if($leads[0]->user_id==$user->id) selected @endif value="{{$user->id}}">{{$user->name}}</option>
                        @endforeach
                     </select>
                     @else
                     <input type="hidden" class="form-control"  value="{{$leads[0]->userid}}" name="user_id">
                     @endif
                  </div>
               </div>
            </div>

            <div class="row">
               <div class="col-xl-12">
                  <div class="form-group">
                     <label for="userId">Lead Name:</label>
                     <input type="text" class="form-control"  value="{{$leads[0]->name}}" name="lead_name">
                  </div>
               </div>
            </div>

            <div class="append_block">
                <div id="edit-form">
                @foreach($leads[0]->questions as $key => $val)
                <div class="edit_question_block">
                    <div class="form-group mb-2">
                       <label for="name" class="font-weight-bold black-text w-100">
                       Question <spna class="edit_q_count">{{$loop->iteration}}</spna>
                       @if(!$loop->first)
                          <a href="javascript:;" class="text-danger float-right edit_remove_block" onclick="remove_edit_fields(this);"><i class="far fa-trash-alt"></i></a>
                          @endif
                       </label>
                       <input type="text" class="form-control heading" value="{{ $val->question }}" name="question[]" aria-describedby="Heading" placeholder="Question">
                    </div>
                    <div class="form-group">
                       <input type="phone" class="form-control answer" value="{{ $val->answer }}" name="answer[]" aria-describedby="Answer" placeholder="Answer">
                    </div>
                    <hr style="width: 50%">
                 </div>
                @endforeach
            </div>
            </div>
            <div class="form-group clearfix text-center">
               <span class="edit_clone_block btn btn-xs btn-primary m-0"><i class="fas fa-plus"></i> Add Question</span>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary btn-sm">Update Lead</button>
         </div>
      </form>
   </div>
</div>
</div>
     @include('layouts.footer')
<!-- end row -->
<!-- container -->
<!--end footer-->
@endsection
@push('staff_js')
<script>
    var appURL = '{!! url('/staff/') !!}';
    var apiURL = '{!! url('/api/staff/') !!}';
    var userType = '{!! Auth::user()->type !!}';
    var userId = '{!! Auth::user()->id !!}';
</script>
<script src="{{ asset('js/cmn/cmn.js') }}"></script>
<script src="{{ asset('staff/js/leaddetails.js') }}"></script>
@endpush

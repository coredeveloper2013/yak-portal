@extends('staff.layouts.app')
@section('staff_css')
<style>

    html {
        background: #f8f8f8;
     }
</style>
@endsection
@section('staff_content')

<div class="page-content grey-bg pb-0">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-9">
                <div class="page-title-box">
                    <h4 class="page-title">Deals</h4>
                </div>

            </div>
            <div class="col-sm-3">
                <input type="hidden" class="form-control" id="idd">
                @if(Auth::user()->type=="admin")
                    <div class="form-group row mb-0 mt-3 mb-2">
                        <label for="userId" class="col-sm-2 col-form-label text-left">User:</label>
                        <div class="col-sm-10">
                        <select class="form-control" name="user_id" onchange="getUserDeals(this.value)">
                            <option value="-1" {{($searchID == -1) ? 'selected' : ''}} >All </option>
                            @foreach($users as $user)
                                <option {{($searchID == $user->id) ? 'selected' : ''}} value="{{$user->id}}">{{$user->name}}</option>
                            @endforeach

                        </select>
                    </div>
                    </div>
                @else
                    <input type="hidden" class="form-control"  value="{{Auth::user()->id}}" name="user_id">
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-xl-12">
                <div class="panel-reponsive panel-height">
                    @foreach($deals_data as $deal_key => $deal_val)
                        <div class="panel kaban-board">
                            <div class="panel-heading">
                                <h3 class="panel-head">
                                    {{$deal_val['name']}}
                                </h3>
                                <span class="panel-sub">
                                   &nbsp;
                                </span>
                            </div>
                            <div class="panel-body">
                                @foreach($deal_val['deals_data'] as $key => $val)
                                    <a href="javascript:;" data-deal_id="{{ $val['id'] }}" class="load_deal">
                                        <div class="card" >
                                            <div class="card-body">
                                                <h4 class="card-head">
                                                    @if(!empty($val['name']))
                                                        {{ $val['name'] }}
                                                    @else
                                                        {{ $val['title'] }}
                                                    @endif
                                                </h4>
                                                <div class="task-ammount">
                                                    <i class="fas fa-user"></i> <span>{{$val['userName']}}</span> 
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

<div class="dealsb">
<a href="javascript:;" class="closesb">x</a>
    <a href="javascript:;" class="load_detail"><i class="fa fa-eye"></i></a>
    
    <div class="infochat">
        <div class="userinfo">
        </div>

        <div class="chat-body ">
            <ul class="user-comment-sec charHistory lead-detail-coment mt-3" id="usercommnts" style="display: none;">
            </ul>
        </div>
         
        <div class="chat-footer chat-footer-custom">
            <form id="sendMessage">
                <div class="">
                   <div class="txt-area-sec">
                      <textarea class="chat-input chat-input-1" name="message" spellcheck="false"></textarea>
                     <input type="hidden" id="lead_id" value="1" name="lead_id">
                   </div>
                   <input type="submit" class="btn btn-send" value="SEND">
                </div>

            </form>
            <!-- end row -->
        </div>
    
    </div>
</div>

@endsection
@push('staff_js')
<script>
    var appURL = '{!! url('/staff/')!!}';
    var apiURL = '{!! url('/api/staff/')!!}';
	
/*
	
$("h4").click(function(){
  $(".dealsb").show();
  $(".page-content").css("width", "calc(100% - 300px)");
  var cheight = $(".userinfo").height();
 
	//$('.dealsb .lead-detail-coment').css("max-height", "calc(100vh - '+cheight+'px)");


	$('.shopping-cart-table-container').css( { height: `calc(100% - ${cheight}px)` } );

});

$(".closesb").click(function(){
  $(".dealsb").hide();
  $(".page-content").css("width", "100%");

});

*/
    $(".dealsb").hide();
    $("body").on('click', '.load_deal', function(event) {
        event.preventDefault();
        let that = $(this);
        let d_id = that.data('deal_id');
        let url = appURL+'/deal-detail/'+d_id;
        $('.load_detail').attr('href', url);
        $('.card-body').removeClass('alert-success');
        that.find('.card-body').addClass('alert-success');
        loadDeals(d_id);
        showDealSec();
		
		    //var pos = $('div.panel-height').position().left;
			//alert(pos);
    		//$('div.panel-height').scrollLeft(pos);
    });


    

</script>
<script src="{{ asset('js/cmn/cmn.js') }}"></script>
<script src="{{ asset('staff/js/leads.js') }}"></script>
@endpush

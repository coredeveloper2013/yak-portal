@extends('staff.layouts.app')
@section('staff_css')
<style>
    .invalid-feedback {
        display: block !important;
    }
</style>
@endsection
@section('staff_content')
<div class="page-content" id="clientManagerContainer">
<div class="container-fluid">
   <div class="row">
      <div class="col-sm-12">
         <div class="page-title-box">
            <h4 class="page-title">Client Manager</h4>
         </div>
         <!--end page-title-box-->
      </div>
      <!--end col-->
   </div>
   <div class="row">
      <div class="col-xl-12 client-m-resp">
         <div role="group" aria-label="Basic example" class="btn-group client-btn-group client-bar-resp">
             <a href="javascript:;" class="btn btn-camp" :class="{ active: filter.status == '' }" @click="resetFilter"><i class=""></i> All</a>
            <a href="javascript:;" class="btn btn-camp" :class="{ active: filter.status == 'recurring' }" @click="filterStatus('recurring')"> <i class="mdi mdi-account-search-outline"></i> Recurring Clients</a>
{{--            <a href="javascript:;" class="btn btn-camp" @click="addNewClient"> <i class="mdi mdi-account-multiple-plus"></i> New Clients</a>--}}
            <a href="javascript:;" class="btn btn-camp" :class="{ active: filter.status == 'new' }" @click="filterStatus('new')"> <i class="mdi mdi-account-multiple-plus"></i> New Clients</a>
            <a href="javascript:;" class="btn btn-camp" :class="{ active: filter.status == 'gold' }" @click="filterStatus('gold')"> <i class="mdi mdi-account-star"></i> Gold Clients</a>
            <a href="javascript:;" class="btn btn-camp" :class="{ active: filter.status == 'lost' }" @click="filterStatus('lost')"> <i class="mdi mdi-account-multiple-minus"></i> Lost Clients</a>
         </div>
         @if (\Session::has('success'))
          <div class="alert alert-success" id="client_created_msg">
             <strong>{!! \Session::get('success') !!}</strong>
          </div>
         @endif
      </div>
      <div class="col-xl-12">
         <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12 no-r-padding">
               <div class="chat-inbox">
                  <div class="search-msg">
                     <div class="form-group">
                        <div class="input-group">
                           <i class="fas fa-search custom-search-icon"></i>
                            <input type="text" id="chat-search"
                                   v-model="filter.queryString"
                                   @keyup="getClients"
                                   name="chat-search" class="form-control msg01" placeholder="Search Contacts">
                            <span class="input-group-append">
                            <button type="button" class="btn btn-primary shadow-none msg01-btn" @click="addNewClient">
                                <i class="mdi mdi-account-plus-outline"></i>
                           </button>
                           </span>
                        </div>
                     </div>
                  </div>
                  <div class="chat-group-wrap">
                     <div class="chat-group pt-2">
{{--                        <span class="group-head">A</span>--}}
                        <a href="javascript:;" class="media"
                           :class="{ active: currentClient.id == user.id }"
                           v-for="user in clients" :key="user.id" @click="updateCurrentClient(user) ; updateLogs(user); ">
                           <div class="media-left">
{{--                              <img src="{{asset('staff/images/user-1.jpg')}}" alt="user" class="rounded-circle thumb-md">--}}
                               <img :src="userAvatarBaseUrl + user.image" class="rounded-circle thumb-md"
                                    alt="User Profile" v-if="user.image != null">
                               <img src="{{asset('staff/images/user-1.jpg')}}" class="rounded-circle thumb-md"
                                    alt="User Profile" v-if="user.image == null">
                              <span class="round-10 bg-success"></span>
                           </div>
                           <!-- media-left -->
                           <div class="media-body">
                              <div>
                                 <h6>@{{ user.company != null ? user.company.name : 'N/A' }}</h6>
                                 <p>@{{ user.name }}</p>
                              </div>
                           </div>
                           <!-- end media-body -->
                        </a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="right-panel05 grey-bg col-xl-9 col-lg-9 col-md-8 col-sm-12" v-if="currentClient">
               <ul class="nav nav-tabs" id="myTab" role="tablist">
                  <li class="nav-item">
                     <a class="nav-link active" id="home-tab" data-toggle="tab" href="#tab001" role="tab" aria-controls="tab001" aria-selected="true">Contact information</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" id="profile-tab" data-toggle="tab" href="#tab002" role="tab" aria-controls="tab002" aria-selected="false">Campaigns</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" id="contact-tab" data-toggle="tab" href="#tab003" role="tab" aria-controls="tab003" aria-selected="false">Landing Pages</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" id="contact-tab" data-toggle="tab" href="#tab004" role="tab" aria-controls="tab004" aria-selected="false">Projects</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" id="contact-tab" data-toggle="tab" href="#tab005" role="tab" aria-controls="tab005" aria-selected="false">Logs</a>
                  </li>
               </ul>
               <div class="tab-content" id="myTabContent">
                  <div class="tab-pane fade show active" id="tab001" role="tabpanel" aria-labelledby="home-tab">
                     <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                           <h4 class="mt-0 header-title" v-if="currentClient.company && currentClient.company.name">@{{ currentClient.company.name }}</h4>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                           <span class="side-btn-light">
                           <a href="javascript:;" @click="editClient(currentClient)" class="btn btn-light"><i class="fa fa-edit"></i> Edit</a>
                           @if (Auth::user()->type == 'admin')
                              <a href="javascript:;" class="btn btn-light" @click="deleteUser(currentClient.id)"><i class="fa fa-trash"></i> Delete</a>
                           @endif
                           </span>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xl-12">
                           <h4 class="heading085">Point of Contact</h4>
                        </div>
                        <div class="col-xl-3 col-lg-5 col-md-5 col-sm-12">
                           <div class="text-group">
                              <label>Name</label>
                              <div class="bk0">@{{ ucFirst(currentClient.name) }}</div>
                           </div>
                        </div>
                        <div class="col-xl-9 col-lg-7 col-md-7 col-sm-12">
                           <div class="text-group">
                              <label>Job Position</label>
                              <div class="bk0" v-if="currentClient.job_position">@{{ ucFirst(currentClient.job_position) }}</div>
                              <div class="bk0" v-else>N/A</div>
                           </div>
                        </div>
                        <div class="col-xl-3 col-lg-5 col-md-5 col-sm-12">
                           <div class="text-group">
                              <label>Mobile Phone</label>
                              <div v-if="currentClient.phone">@{{ currentClient.phone }}</div>
                              <div v-else>N/A</div>
                           </div>
                        </div>
                        <div class="col-xl-9 col-lg-7 col-md-7 col-sm-12">
                           <div class="text-group">
                              <label>Email Address</label>
                              <div>@{{ currentClient.email }}</div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xl-12">
                           <h4 class="heading085">Business Details</h4>
                        </div>
                        <div class="col-xl-3 col-lg-5 col-md-5 col-sm-12">
                           <div class="text-group">
                              <label>Email Address</label>
                              <div v-if="currentClient.business_info && currentClient.business_info.email_address">@{{ currentClient.business_info.email_address }}</div>
                              <div v-else>N/A</div>
                           </div>
                        </div>
                        <div class="col-xl-9 col-lg-7 col-md-7 col-sm-12">
                           <div class="text-group">
                              <label>Work Phone</label>
                              <div v-if="currentClient.business_info != null && currentClient.business_info.phone">@{{ currentClient.business_info.phone }}</div>
                              <div v-else>N/A</div>
                           </div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                           <div class="text-group">
                              <label>Address</label>
                              <div class="bk0" v-if="currentClient.business_info != null && currentClient.business_info.address">
                                  @{{ currentClient.business_info.address }}
                              </div>
                               <div class="bk0" v-else>
                                  N/A
                              </div>
                           </div>
                        </div>
                        <div class="col-xl-3 col-lg-5 col-md-5 col-sm-12">
                           <div class="text-group">
                              <label>Website</label>
                              <div v-if="currentClient.business_info != null && currentClient.business_info.website">@{{ currentClient.business_info.website }}</div>
                              <div v-else>N/A</div>
                           </div>
                        </div>
                        <div class="col-xl-9 col-lg-7 col-md-7 col-sm-12">
                           <div class="text-group">
                              <label>Client Status</label>
                              <div v-if="currentClient.status == 'new'">New Client</div>
                              <div v-else-if="currentClient.status == 'recurring'">Recurring Client</div>
                              <div v-else-if="currentClient.status == 'gold'">Gold Client</div>
                              <div v-else-if="currentClient.status == 'lost'">Lost Client</div>
                              <div v-else>@{{ currentClient.status  }}</div>
                           </div>
                        </div>
                        <div class="col-xl-9 col-lg-10 col-md-10 col-sm-12">
                           <div class="text-group">
                              <label>Note</label>
                              <div class="bk0" v-if="currentClient.business_info != null && currentClient.business_info.notes">
                                 <p>
                                     @{{ currentClient.business_info.notes  }}
                                 </p>
                              </div>
                               <div class="bk0" v-else>
                                 <p>
                                     None.
                                 </p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="tab-pane fade" id="tab002" role="tabpanel" aria-labelledby="profile-tab">
                     <div class="row">
                        <div class="col-lg-12">
                            <h4 class="mt-0 header-title" v-if="currentClient.company && currentClient.company.name">@{{ currentClient.company.name }}</h4>
                           <div class="">
                              <div class="">
                                 <div class="table-responsive">
                                     <template>
                                         <div>
                                             <b-table responsive :items="campaigns" :fields="fieldsForCampaigns" show-empty class="t-light td-block text-left table-responsive">
                                                 <template v-slot:emptyfiltered="scope">
                                                     <h4>@{{ scope.emptyFilteredText }}</h4>
                                                 </template>
                                                 <template v-slot:table-busy>
                                                     <div class="text-center text-danger my-2">
                                                         <b-spinner class="align-middle"></b-spinner>
                                                         <strong>Loading...</strong>
                                                     </div>
                                                 </template>
                                                 <template slot="HEAD_name" slot-scope="data">
                                                     <span class="m-0 pl-2">@{{data.label}}</span>
                                                 </template>
                                                <template v-slot:cell(amount)="data">
                                                     <span class="m-0 pl-2">
                                                        <span>&#163;</span>@{{numberWithComma(data.item.amount)}}
                                                     </span>
                                                </template>
                                                <template v-slot:cell(compaign_amount)="data">
                                                     <span class="m-0 pl-2">
                                                        <span>&#163;</span>@{{numberWithComma(data.item.compaign_amount)}}
                                                     </span>
                                                </template>
                                                <template v-slot:cell(date)="data">
                                                     <span class="m-0 pl-2" v-if="data.item.human_date != null">@{{data.item.human_date}}</span>
                                                     <span class="m-0 pl-2 badge badge-soft-danger badge-boxed" v-else>N/A</span>
                                                </template>
                                                <template v-slot:cell(type)="data">
                                                     <span class="m-0 pl-2">
                                                         <strong>@{{ data.item.type }}</strong>
                                                     </span>
                                                </template>
                                                <template v-slot:cell(status)="data">
                                                    <span class="mt-0 mb-3">
                                                        @{{data.item.status}}
                                                    </span>
                                                </template>
                                                <template v-slot:cell(Actions)="data">
                                                  <span class="btn-group btn-flex">
                                                   <a title="View Leads" :href="getLeadsUrl(data.item.id)" class="btn btn-primary btn-sm mr-1">
                                                        <i class="fa fa-eye m-0"></i>
                                                    </a>
                                                   <a title="View Details"  :href="getCampaignDetailsUrl(data.item.id)"  class="btn btn-primary btn-sm">
                                                         <i class="fa fa-info-circle m-0"></i>
                                                   </a>
                                                 </span>
                                                </template>
                                            </b-table>
                                        </div>
                                     </template>
                                    <!--end /table-->
                                 </div>
                                 <!--end /tableresponsive-->
                              </div>
                              <!--end card-body-->
                           </div>
                           <!--end card-->
                        </div>
                     </div>
                  </div>
                  <div class="tab-pane fade" id="tab003" role="tabpanel" aria-labelledby="contact-tab">
                     <div class="row">
                        <div class="col-xl-12">
                            <h4 class="mt-0 header-title" v-if="currentClient.company && currentClient.company.name">@{{ currentClient.company.name }}</h4>
                        </div>
                        <div class="col-xl-6 col-lg-12 col-md-12 col-sm-6" v-if="landingPages.length != 0" v-for="row in landingPages">
                           <div class="card">
                              <div class="card-body">
                                 <div class="top-txt">
                                    <span class="text-success mt-0 mb-3">  #@{{row.id}}  </span>
                                    <span class="float-right">@{{row.date}}</span>
                                 </div>
                                 <h5 class="txt-black-label">@{{row.name}}</h5>
                                 <p class="txt-grey">@{{row.description}}</p>
                                 <div class="row text-group">
                                    <div class="col-lg-3">
                                       <span class="txt-black-label">URL</span>
                                    </div>
                                    <div class="col-lg-9">
                                       <span class="txt-grey">@{{row.url}}</span>
                                    </div>
                                 </div>
                                 <div class="row text-group">
                                    <div class="col-lg-3">
                                       <span class="txt-black-label">COST</span>
                                    </div>
                                    <div class="col-lg-9">
                                       <span class="txt-grey">&#163;@{{numberWithComma(row.cost)}} per month</span>
                                    </div>
                                 </div>
                                 <div class="footer-card-panel pt-3">
                                    <span class="mt-0  badge badge-boxed  badge-soft-success tm-2"
                                          v-if="row.status == 'COMPLETED'">
                                            @{{row.status}}
                                     </span>
                                     <span class="mt-0 badge badge-boxed  badge-soft-danger tm-2"
                                           v-else>
                                            @{{row.status}}
                                    </span>
                                    <div class="float-right">
                                     <a :href="getLandingDetailsUrl(row.id)"  class="mt-0 badge badge-boxed  tm-2 pull-right btn-primary">
                                           <i class="fa fa-eye"></i>
                                     </a>
                                   </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                         <div class="col-xl-12" v-if="landingPages.length == 0">
                             <div class="text-center my-2 alert-client">Sorry, no landing pages found.</div>
                         </div>
                     </div>
                  </div>
                  <div class="tab-pane fade" id="tab004" role="tabpanel" aria-labelledby="contact-tab">
                      <div class="row">
                          <div class="col-xl-12">
                              <h4 class="mt-0 header-title" v-if="currentClient.company && currentClient.company.name">@{{ currentClient.company.name }}</h4>
                          </div>
                      </div>
                          <div class="row">
                              <div class="col-xl-12">
                                  <div class="table-responsive">
                                      <template>
                                          <div>
                                              <b-table responsive :items="otherProjects" :fields="fieldsForOtherProjects" show-empty
                                                       class="t-light td-block text-left">

                                                  <template v-slot:emptyfiltered="scope">
                                                      <h4>@{{ scope.emptyFilteredText }}</h4>
                                                  </template>
                                                  <template v-slot:table-busy>
                                                      <div class="text-center text-danger my-2">
                                                          <b-spinner class="align-middle"></b-spinner>
                                                          <strong>Loading...</strong>
                                                      </div>
                                                  </template>
                                                  <template slot="HEAD_name" slot-scope="data">
                                                      <span class="m-0 pl-2">@{{data.label}}</span>
                                                  </template>


                                                  <template v-slot:cell(id)="data">
                                                      #@{{ data.item.id }}
                                                  </template>
                                                  <template v-slot:cell(project_name)="data">
                                                      @{{ data.item.project_name }}
                                                  </template>
                                                  <template v-slot:cell(description)="data">
                                                        <span v-if="data.item.description.length <  40 ">
                                                            @{{ data.item.description }}
                                                        </span>
                                                        <span v-else>
                                                            @{{ data.item.description.substring(0,40)+".." }}
                                                        </span>
                                                  </template>
                                                  <template v-slot:cell(human_start_date)="data">
                                                      @{{ data.item.human_start_date }}
                                                  </template>
                                                  <template v-slot:cell(human_end_date)="data">
                                                      @{{ data.item.human_end_date }}
                                                  </template>
                                                  <template v-slot:cell(price)="data">
                                                    <span class="m-0 pl-2">
                                                        <span v-if="data.item.budget != null">
                                                            &#163;@{{numberWithComma(data.item.price)}}
                                                        </span>
                                                        <span v-else>&#163;0.00</span>
                                                    </span>
                                                  </template>
                                                  <template v-slot:cell(budget)="data">
                                                    <span class="m-0 pl-2">
                                                        <span v-if="data.item.budget != null">
                                                            &#163;@{{numberWithComma(data.item.budget)}}
                                                        </span>
                                                        <span v-else>&#163;0.00</span>
                                                    </span>
                                                  </template>
                                                  <template v-slot:cell(cost_type)="data">
                                                    <span class="badge badge-boxed badge-soft-success">
                                                        @{{ data.item.cost_type }}
                                                    </span>
                                                  </template>
                                                  <template v-slot:cell(status_upper)="data">
                                                    <span class="badge badge-boxed badge-soft-success" v-if="data.item.status != 'pending'">
                                                        @{{ data.item.status_upper }}
                                                    </span>
                                                                  <span class="badge badge-boxed badge-soft-warning" v-else>
                                                        @{{ data.item.status_upper }}
                                                    </span>
                                                  </template>

                                                  <template v-slot:cell(actions)="data">
                                                    <span class="btn-group btn-flex">
                                                        <b-button :href="'/staff/projects/detail/' + data.item.id" size="sm" title="View Detail" variant="btn btn-primary btn-sm p-1 px-2 mt-0">
                                                            <i class="fa fa-eye"></i>
                                                        </b-button>
                                                    </span>
                                                  </template>
                                              </b-table>
                                          </div>
                                      </template>
                                  </div>
                              </div>
                          </div>

               </div>
                  <div class="tab-pane fade" id="tab005" role="tabpanel" aria-labelledby="contact-tab">
                     <div class="row">
                        <div class="col-xl-6">
                            <h4 class="mt-0 header-title" v-if="currentClient.company && currentClient.company.name">@{{ currentClient.company.name }}</h4>
                        </div>
                        <div class="col-xl-2">
                            <a href="javascript:;" @click="exportlogs()" class="btn btn-light"><i class="fa fa-excel"></i> Export
                            </a>
                        </div>
                        <div class="col-xl-4">
                            <div class="form-group">

                              <input type="text" name="searchLog" v-model="logFilter" class="form-control" placeholder="Search">

                            </div>
                        </div>
                      </div>
                                                  <div class="row">
                              <div class="col-xl-12">
                                  <div class="table-responsive">
                                      <template>
                                          <div>
                                              <b-table responsive :items="logs.items" :fields="fieldsForLogs" show-empty
                                                       sort-icon-left class="t-light td-block text-left"
                                                       :per-page="logs.perPage"
                                                       :current-page="logs.current"
                                                       id="log-table"
                                                       :filter="logFilter"
                                                       >

                                                  <template v-slot:emptyfiltered="scope">
                                                      <h4>@{{ scope.emptyFilteredText }}</h4>
                                                  </template>
                                                  <template v-slot:table-busy>
                                                      <div class="text-center text-danger my-2">
                                                          <b-spinner class="align-middle"></b-spinner>
                                                          <strong>Loading...</strong>
                                                      </div>
                                                  </template>
                                                  <template slot="HEAD_name" slot-scope="data">
                                                      <span class="m-0 pl-2">@{{data.label}}</span>
                                                  </template>


                                                  <template v-slot:cell(uri)="data">
                                                      #@{{ data.item.uri }}
                                                  </template>
                                                  <template v-slot:cell(ip_address)="data">
                                                      @{{ data.item.ip_address }}
                                                  </template>
                                                  <template v-slot:cell(name)="data">
                                                        @{{ data.item.name }}
                                                  </template>
                                                  <template v-slot:cell(date)="data">
                                                      @{{ data.item.date }}
                                                  </template>
                                                  <template v-slot:cell(time)="data">
                                                      @{{ data.item.time }}
                                                  </template>

                                              </b-table>
                                              <b-pagination
                                                v-model="logs.current"
                                                :total-rows="logs.totalRows"
                                                :per-page="logs.perPage"
                                                aria-controls="log-table"
                                              ></b-pagination>
                                          </div>
                                      </template>
                                  </div>
                              </div>
                          </div>
                     </div>
                  </div>
            </div>
         </div>
      </div>
   </div>
</div>
    <!-- Modal -->
    <div class="modal fade" id="addNewClientModal" tabindex="-1" role="dialog" aria-labelledby="addNewClient" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Add New Client</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('staff.client.store')}}" method="POST">
                        @csrf
                        <input type="hidden" name="userId" id="userId" value="{{old('userId')}}">
                        <div class="">
                            <h5>Point Of Contact</h5>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">Name<span style="color: red"> *</span></label>
                                    <input type="text" name="name" value="{{old('name')}}" id="name" class="form form-control form-control-sm">
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="job-position">Job Position<span style="color: red"> *</span></label>
                                    <input type="text" name="job_position" value="{{old('job_position')}}" id="job_position" class="form form-control form-control-sm">
                                    @error('job_position')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="mobile">Mobile Phone<span style="color: red"> *</span></label>
                                    <input type="text" name="mobile" value="{{old('mobile')}}" id="mobile" class="form form-control form-control-sm">
                                    @error('mobile')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email">Email Address<span style="color: red"> *</span></label>
                                    <input type="text" name="email" value="{{old('email')}}" id="email" class="form form-control form-control-sm">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6 password-blk">
                                <div class="form-group">
                                    <label for="password">Password<span style="color: red"> *</span></label>
                                    <input type="password" name="password" value="{{old('password')}}" id="password" class="form form-control form-control-sm">
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6 c-password-blk">
                                <div class="form-group">
                                    <label for="confirm_password">Confirm Password<span style="color: red"> *</span></label>
                                    <input type="password" name="password_confirmation" value="{{old('password_confirmation')}}" id="password_confirmation" class="form form-control form-control-sm">
                                    @error('password_confirmation')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <select name="status" id="status" class="form form-control form-control-sm">
                                        <option value="new" {{ (old('status') === "new" ? "selected" : "") }}>New Client</option>
                                        <option value="recurring" {{ (old('status') === "completed" ? "recurring" : "") }}>Recurring Client</option>
                                        <option value="gold" {{ (old('status') === "gold" ? "selected" : "") }}>Gold Client</option>
                                        <option value="lost" {{ (old('status') === "lost" ? "selected" : "") }}>Lost Client</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="">
                            <h5>Business Details</h5>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="company">Company Name<span style="color: red"> *</span></label>
                                    <input type="text" name="company" value="{{old('company')}}" id="company" class="form form-control form-control-sm">
                                    @error('company')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="work_email">Work Email Address</label>
                                    <input type="text" name="work_email" value="{{old('work_email')}}" id=work_email class="form form-control form-control-sm">
                                    @error('work_email')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="work_phone">Work Phone</label>
                                    <input type="text" name="work_phone" value="{{old('work_phone')}}" id="work_phone" class="form form-control form-control-sm">
                                    @error('work_phone')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="address">Address</label>
                                    <textarea name="address" id="address" class="w-100 p-2" cols="50" rows="4">{{old('address')}}</textarea>
                                    @error('address')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="website">Website</label>
                                    <input type="text" name="website" value="{{old('website')}}" id="website" class="form form-control form-control-sm">
                                    @error('website')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row" style="display: none;">
                            <div class="col-xl-6 col-lg-6 col-md-6">
                                <label class="sr-only" for="facebook_link">Social Profile</label>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="mdi mdi-facebook"></i></div>
                                    </div>
                                    <input type="text" class="form form-control form-control-sm" value="{{old('facebook_link')}}" id="facebook_link" name="facebook_link" placeholder="Facebook Profile Link">
                                    @error('facebook_link')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6">
                                <label class="sr-only" for="twitter_link">Social Profile</label>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="mdi mdi-twitter"></i></div>
                                    </div>
                                    <input type="text" class="form form-control form-control-sm" value="{{old('twitter_link')}}" id="twitter_link" name="twitter_link" placeholder="Twitter Profile Link">
                                    @error('twitter_link')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                                <div class="col-xl-6 col-lg-6 col-md-6">
                                    <label class="sr-only" for="Instagram_link">Social Profile</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="mdi mdi-instagram"></i></div>
                                        </div>
                                        <input type="text" class="form form-control form-control-sm" value="{{old('instagram_link')}}" id="Instagram_link" name="Instagram_link" placeholder="Instagram Profile Link">
                                        @error('instagram_link')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="notes">Notes</label>
                                    <textarea name="notes" id="notes" class="w-100 p-2" cols="50" rows="4">{{old('notes')}}</textarea>
                                    @error('notes')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('staff_js')
<script>
    var App_url = '{!! url()->current() !!}';
    var main_url = '{!! url('/') !!}';
    var userAvatarBaseUrl = '{!! asset('/storage/avatars/')  !!}' + '/';
    $(document).ready(function(){
        @if (count($errors) > 0)
            $('#addNewClientModal').modal('show');
        @endif
        setTimeout(function () {
            $("#client_created_msg").fadeOut(1000);
        },1000);
    });

</script>
        {{--  Inject vue js files  --}}
        <script src="{{ asset('js/app-vue.js') }}"></script>
        <script src="{{ asset('js/staff/clientmanager/vue.js') }}"></script>

        <!-- //export table
        <script src="{{ asset('vendor/tableExport/libs/es6-promise/es6-promise.auto.min.js') }}"></script>
         <script src="{{ asset('vendor/tableExport/libs/FileSaver/FileSaver.min.js') }}"></script>
         <script src="{{ asset('vendor/tableExport/libs/html2canvas/html2canvas.min.js') }}"></script>

         <script src="{{ asset('vendor/tableExport/libs/html2canvas/html2canvas.min.js') }}"></script>

         <script src="{{ asset('vendor/tableExport/libs/js-xlsx/xlsx.core.min.js') }}"></script>
         <script type="text/javascript">
           $('#log-table').tableExport({type:'excel'});
         </script> -->
@endpush

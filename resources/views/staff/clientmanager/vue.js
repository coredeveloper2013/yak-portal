new Vue({
    el: '#clientManagerContainer',
    mounted() {
        this.getClients();
    },
    watch: {
    },
    data: {
        fieldsForCampaigns: [
            {key: 'id', label: 'Campaign ID'},
            {key: 'start_date', label: 'Start Date'},
            {key: 'name', label: 'Campaign Name'},
            {key: 'landing_page_name', label: 'Landing Page'},
            {key: 'amount', label: 'Contract Value'},
            {key: 'compaign_amount', label: 'Budget'},
            'type',
            'status',
            'Actions'
        ],
        fieldsForOtherProjects: [
            {key: 'id', label: 'Order'},
            {key: 'project_name', label: 'Name'},
            {key: 'human_start_date', label: 'Start Date'},
            {key: 'human_end_date', label: 'End Date'},
            {key: 'price', label: 'Price'},
            {key: 'budget', label: 'Budget'},
            {key: 'cost_type', label: 'Cost type'},
            {key: 'status_upper', label: 'Status'},
            'Actions'
        ],
        fieldsForLogs: [
            {key: 'uri', label: 'URL' },
            {key: 'ip_address', label: 'IP Address' },
            {key: 'Name', label: 'Name' },
            {key: 'date', label: 'Date' , sortable: true },
            {key: 'time', label: 'Time' , sortable: true },

        ],
        clients: [],
        currentClient: '',
        campaigns: [],
        landingPages: [],
        otherProjects: [],
        logs: [],
        logFilter:null,
        filter: {
            queryString: '',
            status: ''
        },
        userAvatarBaseUrl: userAvatarBaseUrl,
        getClientUrl: App_url + '/get/clients',
        leadsUrl : main_url + '/staff/campaign-leads',
        campaignDetailsUrl : main_url + '/staff/campaign-detail',
        landingDetailsUrl : main_url + '/staff/landing-page-details',
        removeClientUrl: App_url + '/remove/client',
        exportLogsUrl: App_url + '/export/client',
    },
    computed: {

    },
    wathc:{
        logFilter: function(text)
        {
            console.log(text);
            this.logFilter = text;
        }
    },
    methods: {
        getClients: function(){
          axios.post(this.getClientUrl, {filter: this.filter})
              .then(response => {
                  if(response.data.status == 'success'){
                      this.clients = response.data.data;
                      this.currentClient = response.data.data[0];
                      if(this.currentClient)
                      {
                          this.updateCurrentClient(this.currentClient);
                          this.updateLogs(this.currentClient);
                      }
                  }
              }).catch(error => {
                  console.log(error);
          });
        },
        updateCurrentClient: function(user){
            this.currentClient = user;
            this.campaigns = user.campaigns;
            this.landingPages = user.landing_pages;
            this.otherProjects = user.other_projects;

        },
        updateLogs: function (user){
            _this = this;
            this.logs.items = [];
            this.logs.perPage = 10;
            this.logs.current = 1;
            if (user.request_logs.length > 0) {
                user.request_logs.forEach(function myFunction(log) {
                            let temp = JSON.parse(log.log_details.content);
                            let temp_log = [];
                            temp_log.uri  =  App_url + temp.uri;
                            temp_log.ip_address =  temp.ip_address;
                            temp_log.name = _this.currentClient.name;
                            temp_log.date = moment(log.log_details.created_at).format("MMMM D YYYY");
                            temp_log.time = moment(log.log_details.created_at).format("hh:mm A");
                            _this.logs.items.push(temp_log);
                          });
                    }
            this.logs.totalRows = this.logs.items.length;
           // this.logs = user.request_logs;
        },
        currentPage: function()
        {
            console.log('here');
            return 1;

        },
        ucFirst(string){
            return string.charAt(0).toUpperCase() + string.slice(1);
        },
        numberWithComma: function(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        },
        getLeadsUrl: function(id){
            return `${this.leadsUrl}/${id}` ;
        },
        getCampaignDetailsUrl: function(id){
            return `${this.campaignDetailsUrl}/${id}` ;
        },
        getLandingDetailsUrl: function(id)
        {
            return `${this.landingDetailsUrl}/${id}` ;
        },
        deleteUser: function(userId){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    axios.post(this.removeClientUrl, {id: userId})
                        .then(response => {
                            if(response.data.status == 'success'){
                                Swal.fire({
                                    title: 'Deleted!',
                                    text: 'Client Removed Successfully.',
                                    icon: 'success'
                                });
                                this.getClients();
                            } else {
                                Swal.fire({
                                    title: 'Error!',
                                    text: response.data.msg,
                                    icon: 'error'
                                });
                            }
                        }).catch(error => {
                            console.log(error);
                    });
                }
            })
        },
        editClient: function(client){
            $('.password-blk').addClass('d-none');
            $('.c-password-blk').addClass('d-none');
            $("#userId").val(client.id);
            $("#name").val(client.name);
            $("#job_position").val(client.job_position);
            $("#mobile").val(client.phone);
            $("#email").val(client.email);
            $("#status").val(client.status);
            // $("#password").val(client.password);
            // $("#password_confirmation").val(client.password);
            if(client.company){
                $("#company").val(client.company.name);
            }else{
                $("#company").val('');
            }
            if(client.business_info){
                $("#work_email").val(client.business_info.email_address);
                $("#work_phone").val(client.business_info.phone);
                $("#address").val(client.business_info.address);
                $("#website").val(client.business_info.website);
                $("#facebook_link").val(client.business_info.facebook_link);
                $("#twitter_link").val(client.business_info.twitter_link);
                $("#instagram_link").val(client.business_info.instagram_link);
                $("#notes").val(client.business_info.notes);
            }else{
                $("#work_email").val('');
                $("#work_phone").val('');
                $("#address").val('');
                $("#website").val('');
                $("#facebook_link").val('');
                $("#twitter_link").val('');
                $("#instagram_link").val('');
                $("#notes").val('');
            }
            $("#addNewClientModal").modal('show');
        },
        addNewClient: function(){
            $('.password-blk').removeClass('d-none');
            $('.c-password-blk').removeClass('d-none');
            $("#userId").val('');
            $("#name").val('');
            $("#job_position").val('');
            $("#mobile").val('');
            $("#email").val('');
            $("#company").val('');
            $("#work_email").val('');
            $("#work_phone").val('');
            $("#address").val('');
            $("#website").val('');
            $("#facebook_link").val('');
            $("#twitter_link").val('');
            $("#instagram_link").val('');
            $("#notes").val('');
            $("#addNewClientModal").modal('show');
        },
        filterStatus: function(status){
            this.filter.status = status;
            this.getClients();
        },
        resetFilter: function(){
            this.filter = {
                queryString: '',
                    status: ''
            };
            this.getClients();
        },
        exportlogs: function () {
            axios.post(this.exportLogsUrl, {filter: this.filter , user_id: this.currentClient.id, logFilter: this.logFilter})
                .then(response => {
                    console.log(response.data.data);
                     if(response.data.status == 'success'){
                        console.log(response.data.data);
                        window.open(response.data.data, '_blank');
                    }
                }).catch(error => {
                console.log(error);
            });
        }
    }
});

@extends('staff.layouts.app')
@section('staff_css')
<!-- <link href='{{ asset('staff/css/calender.css') }}' rel='stylesheet' type='text/css'>
   -->
<link href="{{ asset('staff/packages/core/main.css') }}" rel='stylesheet' type='text/css' />
<link href="{{ asset('staff/packages/daygrid/main.css') }}" rel='stylesheet' type='text/css' />
<link href="{{ asset('staff/packages/timegrid/main.css') }}" rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="{{ asset('css/bootstrap-select.min.css') }}">

<style>
</style>
@endsection
@section('staff_content')
<div class="page-content grey-bg">
   <div class="container-fluid">
      <div class="row">
         <div class="col-xl-3 col-lg-3 col-md-3">
            <div class="page-title-box">
               <h4 class="page-title">Calender</h4>
            </div>
            <button type="button" class="btn btn-addevent" data-toggle="modal" data-target="#modal-view-event-add">
            ADD EVENT</button>
            <div class="calendar-checks">
               <span>Category</span>
               <a href="#" class="float-right" data-toggle="modal" data-target="#addcat-event"><i class="fa fa-plus"></i></a>
               <ul class="calendar-list">
                @if($categories)
                @foreach($categories as $category)
                  @if( count($category->assignedmodule) || \Auth::user()->type == 'admin')
                  <li>
                     <div class="checkbox {{$category->color}}">
                        <input id="category-{{$category->id}}" value="{{$category->id}}" type="checkbox" checked="" name="category_id">
                        <label for="category-{{$category->id}}">
                          {{$category->name}}
                        </label>
                     </div>
                  </li> 
                  @endif
                @endforeach
                @endif
               </ul>
            </div>
         </div>
         <div class="col-xl-9 col-lg-9 col-md-9">
            <div id='calendar'></div>
            <!--end page-title-box-->
         </div>
      </div>
   </div>
</div>
<!-- calendar modal -->
<div id="addcat-event" class="modal modal-top fade">
   <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
         <div class="modal-body">
               <h3>Add Category</h3>
                 <div class="form-group">
                  <label>Category name</label>
                  <input type="text" class="form-control" name="category_name" id="category_name">
               </div>

               <div class="form-group">
                  <label>Color</label>
                  <select class="form-control" name="category_color" id="category_color">
                     <option value="checkbox-orange">Orange</option>
                     <option value="checkbox-purple">Purple</option>
                     <option value="checkbox-pink">Pink</option>
                     <option value="checkbox-green">Green</option>
                  </select>
               </div>
               <div class="form-group">
                  <label>Staff Members</label> 
                  <select class="selectpicker form-control" name="staff_id" id="staff_id" multiple="" data-live-search="true" data-actions-box="true">
                     @foreach($users as $user)
                      <option value="{{$user->id}}">{{$user->name}}</option>
                     @endforeach
                  </select>
               </div>
         </div>
         <div class="modal-footer">
                         <button type="submit" id="saveCategory" class="btn btn-primary" >Save</button>

            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div id="modal-view-event-add" class="modal modal-top fade">
   <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
         <form id="">
            <div class="modal-body">
               <h3>Add Event Detail</h3>
               <div class="form-group">
                  <label>Event name</label>
                  <input type="text" class="form-control" name="name" id="name">
               </div>
               <div class="form-group">
                  <label>Event Description</label>
                  <textarea class="form-control" name="edesc" id="desc"></textarea>
               </div>
               <div class="form-group">
                  <label>Date & Time</label>
                  <input type='text' value="" class="datetimepicker form-control" name="edate" id="event_time">
               </div>
               <div class="form-group">
                  <label>Invitees</label>
                  <a class="extra-fields-customer float-right" href="#"><i class="fa fa-plus"></i> Add more</a>
                  <div class="customer_records">
                     <div class="row">
                        <div class="col-6">
                           <div class="form-group">
                              <input name="invitee_name[]" multiple="" class="form-control" type="text" value="" placeholder="Name">
                           </div>
                        </div>
                        <div class="col-6">
                           <div class="form-group">
                              <input  name="invitee_phone[]" multiple="" class="form-control"  type="number" value="" placeholder="Phone Number">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="customer_records_dynamic"></div>
               </div>
               <div class="form-group">
                  <label>Category</label>
                  <select class="form-control" name="category_id" id="category_id">
                    @if($categories)
                      @foreach($categories as $category)
                        @if( count($category->assignedmodule) || \Auth::user()->type == 'admin')
                         <option value="{{$category->id}}"> {{$category->name}}</option>
                        @endif
                      @endforeach
                    @endif
                   </select>
               </div>
               <p>Do you want to send a sms 20 mins before to attendees?</p>
               <div class="form-group padding-left-o">
                  <div class="radio radio-info form-check-inline">
                     <input type="radio" id="inlineRadio1" value="1" name="is_send_sms">
                     <label for="inlineRadio1"> Yes </label>
                  </div>
                  <div class="radio radio-info form-check-inline">
                     <input type="radio" id="inlineRadio2" value="0" name="is_send_sms" checked="">
                     <label for="inlineRadio2"> No </label>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary" id="saveEvent">Save</button>
               <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>        
            </div>
         </form>
      </div>
   </div>
</div>
@endsection
@push('staff_js')
<script>
   var appURL = '{!! url('/staff/') !!}';
   var apiURL = '{!! url('/api/staff/') !!}';
</script>
{{--  Inject vue js files  --}}
<script src="{{ asset('js/cmn/cmn.js') }}"></script>
<!-- <script src="{{ asset('staff/js/campaigns.js') }}"></script> -->
<!-- <script src="{{ asset('staff/js/calender.js') }}"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.js"></script> -->

 <script src="{{ asset('js/calender/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('staff/packages/core/main.js') }}"></script>
<script src="{{ asset('staff/packages/core/locales-all.js') }}"></script>
<script src="{{ asset('staff/packages/interaction/main.js') }}"></script>
<script src="{{ asset('staff/packages/daygrid/main.js') }}"></script>
<script src="{{ asset('staff/packages/timegrid/main.js') }}"></script>
<script>
  let current_event_source;
  var calendar;
   document.addEventListener('DOMContentLoaded', function() {
     $('#staff_id').selectpicker('selectAll');
      $('input[name="edate"]').daterangepicker({
            opens: 'left',
            singleDatePicker: true,
            showDropdowns: true,
            timePicker: true,
            startDate: moment().startOf('hour'),
            endDate: moment().startOf('hour').add(32, 'hour'),
            locale: {
                format: 'MM/DD/YYYY HH:mm'
            },
            minDate: moment(),
            startDate: moment()
        }, function (start, end, label) {
            // $("#date").val(start.format('MM/DD/YYYY'));
        });
     var initialLocaleCode = 'en';
     var localeSelectorEl = document.getElementById('locale-selector');
     var calendarEl = document.getElementById('calendar');
   
      calendar = new FullCalendar.Calendar(calendarEl, {
       plugins: [ 'interaction', 'dayGrid', 'timeGrid' , 'dayGridWeek' , 'dayGrid' , 'dayGridMonth'],
       header: {
         left: 'title prev,next',
         right: 'timeGrid,dayGridWeek,dayGridMonth'
       },
       buttonText :  {
            timeGrid:    'Day',
            dayGridMonth:    'Month',
            dayGridWeek:     'Week',
          },
       defaultDate: moment().format('YYYY-MM-DD'),
       defaultView: 'dayGridWeek',
       locale: initialLocaleCode,
       buttonIcons: true, // show the prev/next text
       weekNumbers: false,
       navLinks: true, // can click day/week names to navigate views
       editable: false,
       allDaySlot: false,
       eventLimit: true, // allow "more" link when too many events
       eventSources : 
         [{
                id: '1',
                events :  function(info, successCallback, failureCallback) {
              //console.log(info.startStr);
               var categories = $('input[name^=category_id]:checked').map(function(idx, elem) {
                    return $(elem).val();
                  }).get();
                  $.ajax({              
                      
                      url: '{{URL("/api/staff/calender/get-events")}}',
                      type: 'POST',
                      async: false,
                      data : {
                        start : info.startStr,
                        end : info.endStr,
                        categories : categories
                      },
                      headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                        Accpet: "applicationjson"
                      },
                      error: function() {
                        alert('there was an error while fetching events!');
                      },
                      success:function(events)
                      {
                        successCallback(JSON.parse(events));
                      }

                    });
                }  
        }]

     });
   
     calendar.render();
   
     // build the locale selector's options
     // calendar.getAvailableLocaleCodes().forEach(function(localeCode) {
     //   var optionEl = document.createElement('option');
     //   optionEl.value = localeCode;
     //   optionEl.selected = localeCode == initialLocaleCode;
     //   optionEl.innerText = localeCode;
     //   localeSelectorEl.appendChild(optionEl);
     // });
   
     // when the selected option changes, dynamically change the calendar option
     // localeSelectorEl.addEventListener('change', function() {
     //   if (this.value) {
     //     calendar.setOption('locale', this.value);
     //   }
     // });
   
   });

   $(document).on('change', 'input[name^=category_id]', function(e) {
     
      if (current_event_source) {
          let source_id = calendar.getEventSourceById(current_event_source);
          source_id.remove();
      }

      if (calendar) {

         let event_source_id = Math.floor(Math.random() * 10000) + 100;
        current_event_source = event_source_id;

        var new_event_source =  {
                  id: event_source_id,
                  events : function(info, successCallback, failureCallback)  {
              //console.log(info.startStr);
               var categories = $('input[name^=category_id]:checked').map(function(idx, elem) {
                    return $(elem).val();
                  }).get();
                  $.ajax({              
                      
                      url: '{{URL("/api/staff/calender/get-events")}}',
                      type: 'POST',
                      async: false,
                      data : {
                        start : info.startStr,
                        end : info.endStr,
                        categories : categories
                      },
                      headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                        Accpet: "applicationjson"
                      },
                      error: function() {
                        alert('there was an error while fetching events!');
                      },
                      success:function(events)
                      {
                        successCallback(JSON.parse(events));
                      }
                    });
                }
                 
          };

          calendar.removeAllEvents();
          calendar.addEventSource(new_event_source);
          let event_source_default = calendar.getEventSourceById(1);
          if (event_source_default)
              event_source_default.remove();
          let event_source = calendar.getEventSourceById(event_source_id);
          //console.log(calendar.getEventSources());

          event_source.refetch();

      }
      e.preventDefault();
   });



   
</script>
<script type="text/javascript">
   $('.extra-fields-customer').click(function() {
   $('.customer_records').clone().appendTo('.customer_records_dynamic');
   $('.customer_records_dynamic .customer_records').addClass('single remove');
   $('.single .extra-fields-customer').remove();
   $('.single').append('<a href="#" class="remove-field btn-remove-customer"><i class="fas fa-times text-danger"></i></a>');
   $('.customer_records_dynamic > .single').attr("class", "remove-row");
   
   // $('.customer_records_dynamic input').each(function() {
   //   var count = 0;
   //   var fieldname = $(this).attr("name");
   //   // $(this).attr('name', fieldname + count);
   //   // count++;
   // });
   
   });
   
   $(document).on('click', '.remove-field', function(e) {
   $(this).parent('.remove-row').remove();
   e.preventDefault();
   });

   //save categories 
   $(document).on('click', '#saveCategory', function(e) {
     loader('show');
     var form_data = {
      name  :  $('#category_name').val(),
      color :  $('#category_color').val(),
      staff_id :  $('#staff_id').val(),
     };
     $.ajax({
        url: "{{URL('/api/staff/calender/save-category')}}",
        data: form_data,
        cache: false,
        type: "POST",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            Accpet: "applicationjson"
        },
        success: function(response) {
          console.log(response);
          showOkWithReload( response);
          loader();
        },
        error: function(xhr) {
          loader();
        }
    });
     e.preventDefault();
   });

     //save events 

   $(document).on('click', '#saveEvent', function(e) {
     loader('show');
     var invitee_names = $('input[name^=invitee_name]').map(function(idx, elem) {
        return $(elem).val();
      }).get();

      var invitee_phones = $('input[name^=invitee_phone]').map(function(idx, elem) {
        return $(elem).val();
      }).get();


     var form_data = {
      name            : $('#name').val(),
      description     : $('#desc').val(),
      event_time      : $('#event_time').val(),
      invitee_names   : invitee_names,
      invitee_phones  : invitee_phones,
      category_id     : $('#category_id').val(),
      is_send_sms     : $('input[name=is_send_sms]:checked').val()
     };
     console.log(form_data);
     $.ajax({
        url: "{{URL('/api/staff/calender/save-event')}}",
        data: form_data,
        cache: false,
        type: "POST",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            Accpet: "applicationjson"
        },
        success: function(response) {
          console.log(response);
          showOkWithReload( response);
          loader();
        },
        error: function(xhr) {
          loader();
        }
    });
     e.preventDefault();
   });


</script>
@endpush
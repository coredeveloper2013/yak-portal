import Multiselect from "vue-multiselect";
var otherProjectsVue = new Vue({
    el: '#staff_invoices',
    components: {Multiselect},
    mounted() {
        this.getStaffInvoices(false);
    },
    data: {
        invoices: [],
        invoicesMeta: {},
        currentUser: user_id,
        filter: {
            status: '',
        },
        isBusy:false,
        showMsg: '',
        currentInvoice: '',
        donwloadUrl : donwload,
        s_public_key: stripe_public_key,
        getInvoiceUrl : apiURL + '/staff/invoice',
    },
    watch: {
        'invoicesMeta.current_page': function (val) {
            this.loadPaginatedData();
        },
        cost_type: function (val) {
            if (this.cost_type == 'monthly') {
                $('.end_date').addClass('d-none');
                $('.no_of_months').removeClass('d-none');
            } else {
                $('.no_of_months').addClass('d-none');
                $('.end_date').removeClass('d-none');
            }
        }
    },
    methods: {
        loadPaginatedData: function () {
            this.getStaffInvoices(this.getInvoiceUrl + '?page=' + this.invoicesMeta.current_page);
        },
        getStaffInvoices: function (url = false) {
            this.isBusy = true;
            this.showMsg = 'd-none';
            if(url == false){
                url = this.getInvoiceUrl;
            }
            axios.post(url, {filter: this.filter})
                .then(response => {
                    if(response.data.status == 'success'){
                        this.isBusy = false;
                        this.showMsg = '';
                        this.invoices = response.data.data;
                        this.invoicesMeta = response.data.meta;
                    }
                }).catch(error => {
            console.log(error);
                this.isBusy = false;
                this.showMsg = '';
            });
        },
        updateInvoice: function(invoice){
            this.currentInvoice = invoice;
            $('#pay_model').modal('show');
        },
        sendInvoice: function(id){
            let url = appUrl + '/staff/invoice/send-email'
            axios.post(url, {id: id})
                .then(response => {
                    let res = response.data;
                    let status = res.status.substr(0,1).toUpperCase()+res.status.substr(1);
                    Swal.fire({
                        title: status,
                        text: res.msg,
                        icon: res.status
                    });

                }).catch(error => {
                console.log(error);
                this.isBusy = false;
                this.showMsg = '';
            });
        },
        viewCustomer: function(invoice){
            this.currentInvoice = invoice;
            $('#client_detail').modal('show');
        },
        resetFilter: function(){
            this.filter = {
                status : ''
            };
            this.getStaffInvoices(false);
        },
        numberWithComma: function(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        },
        Filtered(filteredItems) {
            // Trigger pagination to update the number of buttons/pages due to filtering
            this.invoicesMeta.totalRows = filteredItems.length;
            this.invoicesMeta.currentPage = 1;
        },
    }
});

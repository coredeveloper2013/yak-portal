Dear {{$invoice->user->name}}!
<br>
This is a reminder email from "YAK Portal" for a pending invoice. <br/><br/>
Kindly check the attached invoice.

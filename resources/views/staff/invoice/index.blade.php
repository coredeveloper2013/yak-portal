@extends('staff.layouts.app')

@section('staff_content')
    <!-- Page Content-->
    <div class="page-content" id="staff_invoices">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box resp-main-heading">
                        <!-- <div class="float-right">
                            <ol class="breadcrumb">
                                <button type="button" class="btn btn-primary text-uppercase">
                                    <i class="fas fa-file-pdf"></i>&nbsp;&nbsp; Generate PDF Report
                                </button>
                            </ol>
                        </div> -->
                        <h4 class="page-title overview-flot">Invoice</h4>
                    </div>
                    <!--end page-title-box-->
                </div>
            </div>

    <!-- loader html -->

    <!-- <div class="loader"></div> -->

    <!-- loader html -->

            <div class="row" :class="showMsg">
                <div class="col-xl-12">
                    <div class="table-responsive" :busy="isBusy">
                        <table class="table bg-white align-center" v-if="invoices.length">
                            <tr  v-for="row in invoices">
                                <td>
                                    <span class="status-pay paid" v-if="row.status == 'COMPLETED'">
                                        <span v-if="row.paid_as == 'marked'">Marked as Paid</span>
                                        <span v-else>Paid</span>
                                    </span>
                                    <span class="status-pay sent" v-else>Sent</span>
                                </td>
                                <td>
                                    <span class="inv-b">Invoice #@{{row.id}}</span>
                                    <span class="grey-o">@{{row.human_date}}</span>
                                </td>
                                <td>
                                    <span class="inv-o" v-if="row.user.company !== null">
                                        <span class="text-danger" v-if="row.user.deleted_at !== null">
                                            @{{row.user.company.name}}
                                        </span>
                                        <span v-else>
                                            @{{row.user.company.name}}
                                        </span>
                                    </span>
                                    <span class="inv-o" v-else>
                                        <span class="text-danger" v-if="row.user.deleted_at !== null">
                                            @{{row.user.name}}
                                        </span>
                                        <span v-else>
                                            @{{row.user.name}}
                                        </span>
                                    </span>

                                    <span class="grey-o" v-if="row.landing_page !== null">
                                        <span class="text-danger" v-if="row.deleted_parent == 'Y'">
                                            @{{row.landing_page.name}} #@{{row.landing_page.id}}
                                        </span>
                                        <span v-else>
                                            @{{row.landing_page.name}} #@{{row.landing_page.id}}
                                        </span>
                                    </span>
                                    <span class="grey-o" v-else-if="row.other_project !== null">
                                        <span class="text-danger" v-if="row.deleted_parent == 'Y'">
                                            @{{row.other_project.project_name}} #@{{row.other_project.id}}
                                        </span>
                                        <span v-else>
                                            @{{row.other_project.project_name}} #@{{row.other_project.id}}
                                        </span>
                                    </span>
                                    <span class="grey-o" v-else-if="row.campaign !== null">
                                        <span class="text-danger" v-if="row.deleted_parent == 'Y'">
                                            @{{row.campaign.name}} #@{{row.campaign.id}}
                                        </span>
                                        <span v-else>
                                            @{{row.campaign.name}} #@{{row.campaign.id}}
                                        </span>
                                    </span>
                                </td>
                                <td class="text-right">
                                    <span class="inv-o">GBP @{{row.total_amount}}</span>
                                    <span class="grey-o">@{{row.human_paid_date}}</span>
                                </td>
                                <td class="text-right">
                                    <a href="javascript:;" class="btn btn-grey" v-if="row.status != 'COMPLETED' && row.deleted_parent == 'N' && row.user.deleted_at == null" @click="updateInvoice(row)">Pay</a>
                                    <a href="javascript:;" class="btn btn-grey" v-if="row.status != 'COMPLETED' && row.deleted_parent == 'N' && row.user.deleted_at == null" @click="sendInvoice(row.id)">Send</a>
                                    <a href="javascript:;" class="btn btn-grey" @click="viewCustomer(row)">Client</a>
                                    <a :href="donwloadUrl+'/'+row.id" download class="btn btn-grey">Download Invoice</a>
                                </td>
                            </tr>
                        </table>
                        <table class="table bg-white align-center" v-else>
                            <tr>
                                <td>Sorry, no invoices found.</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="custom-space01">
                <div class="col-xl-6 col-lg-6 col-md-6 float-left">
                    <p class="mb-0 p-1 small mt-1 float-left">Showing @{{ invoicesMeta.from }} to @{{
                        invoicesMeta.to }} of @{{ invoicesMeta.total }} records | Per Page: @{{
                        invoicesMeta.per_page }} | Current Page: @{{ invoicesMeta.current_page }}</p>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 float-right t012">
                    <b-pagination class="float-right  t012" size="sm" :total-rows="invoicesMeta.total"
                                  v-model="invoicesMeta.current_page" :per-page="invoicesMeta.per_page"
                                  first-text="First" prev-text="Pervious" next-text="Next" last-text="Last"
                                  ellipsis-text="More" variant="danger">
                    </b-pagination>
                </div>
            </div>{{-- End Row --}}
        </div>
        <!-- end page content -->


        <div class="modal fade" id="pay_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4>Pay Invoice</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: red">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row" v-if="currentInvoice">
                            <div class="col-md-12" v-if="currentInvoice.status != 'COMPLETED' && ( currentInvoice.user.card_brand == '' || currentInvoice.user.card_brand == null) ">
                                <staff_subscription :amount="currentInvoice.total_amount"
                                          :type="currentInvoice.type"
                                          :id="currentInvoice.id"
                                          :public_key="s_public_key"
                                          :saved="'no'"
                                          :status="currentInvoice.status"></staff_subscription>
                            </div>

                            <div class="col-md-12" v-else>
                                <staff_subscription :amount="currentInvoice.total_amount"
                                          :type="currentInvoice.type"
                                          :id="currentInvoice.id"
                                          :public_key="s_public_key"
                                          :saved="'yes'"
                                          :status="currentInvoice.status"></staff_subscription>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="client_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4>Client Detail</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: red">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row" v-if="currentInvoice">
                            <div class="col-md-12">
                                <label>Name:</label>
                                <p>@{{ currentInvoice.user.name }}</p>
                            </div>
                            <div class="col-md-12">
                                <label>Email:</label>
                                <p>@{{ currentInvoice.user.email }}</p>
                            </div>

                            <div class="col-md-12" v-if="currentInvoice.user.company != null">
                                <label>Company Name:</label>
                                <p>@{{ currentInvoice.user.company.name }}</p>
                            </div>

                            <div class="col-md-12" v-if="currentInvoice.user.company != null && currentInvoice.user.address != null && currentInvoice.user.address != ''">
                                <label>Company Address:</label>
                                <p>@{{ currentInvoice.user.company.address }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- end page-wrapper -->
@endsection

@push('staff_js')
    <script>
        var App_url = '{!! url()->current() !!}';
        var appUrl = '{!! url('') !!}';
        var apiURL = '{!! url('/api/') !!}';
        var donwload = "{!! route('staff-download-invoice') !!}";
        var user_id = '{!! Auth::user()->id !!}';
        var stripe_public_key = '{!! $stripe_pub !!}';
    </script>
    {{--  Inject vue js files  --}}
    <script src="{{ asset('js/app-vue.js') }}"></script>
    <script src="{{ asset('js/staff/invoice/vue.js') }}"></script>
    <script>
        $( document ).ready(function() {
            $(function () {
              $('[data-toggle="tooltip"]').tooltip()
            })
        })
    </script>
@endpush

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8" />
    <title>PPC</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A premium admin dashboard template by Mannatthemes" name="description" />
    <meta content="Mannatthemes" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('/images/logo-sm.png')}}">
    <!-- App css -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{asset('css/icons.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('css/metisMenu.min.css')}}" rel="stylesheet" type="text/css"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{asset('staff/css/main.css')}}" rel="stylesheet" type="text/css" />
{{--    <link rel="stylesheet" href="{{ asset('/css/_v2/main.css') }}">--}}
    @yield('staff_css')

    <script type="text/javascript">
        var storage_url = "{{asset('storage')}}";
    </script>

</head>
<body>
@include('staff.layouts.header')
<div class="page-wrapper">
    @include('staff.layouts.navigation')
        @yield('staff_content')
</div>

    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{asset('js/metisMenu.min.js')}}"></script>
    <script src="{{asset('js/waves.min.js')}}"></script>
    <script src="{{asset('js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('js/Sidebar-toggle.js') }}"></script>
    @stack('staff_js')
    <script>

        $(document).on("click", ".browse", function () {

            var file = $(".profile-image");
            file.trigger("click");
        });
        $(document).on("change", ".profile-image", function () {
            $('#profile-image-form').submit();
        });

    </script>
</body>
</html>

<!-- Left Sidenav -->
@php

 $rights = array();

 if(\Auth::user()->rights != null)
 {
    $rights = explode(',' , \Auth::user()->rights );
 }

@endphp
<div class="left-sidenav ad-sidenav">
    <!--end main-icon-menu-->
    <div class="main-menu-inner nav-responsive style-4">
        <div class="profile-side-sectio">
            <div class="top-notify">
                <span class="float-left side-account logo-sm">
                    <img src="{{asset('staff/images/logo-sm.png')}}" alt="logo">
                </span>
                <span class="float-right side-account">
                    <a href="#">
                        <i class="dripicons-bell noti-icon"></i>
                    </a>
                    <a href="{!! route('staff.logout') !!}" title="Logout">
                        <i class="dripicons-exit noti-icon"></i>
                    </a>
                </span>
            </div>
            <div class="user-name">
                <form method="post" id="profile-image-form" enctype="multipart/form-data" action="{{route('staff.staffAccount.profile.upload')}}">
                    @csrf
                    <input type="file" name="img" class="file profile-image" accept="image/*">
                </form>
                <img @if(!is_null(Auth::user()->image)) src="{{asset('storage/avatars/'.Auth::user()->image)}}"
                     @else src="{{asset('/images/user.png')}}" @endif alt="profile-user" class="browse rounded-circle"/>
                <div class="user-side-info">
                    <h5>{{Auth::user()->name}}</h5>
                    <div>{{Auth::user()->email}}</div>
                </div>
            </div>
        </div>
        <div class="menu-body ">
            <div class="main-icon-menu-pane">
                <div class="title-box">
                    <h6 class="menu-title">Dashboard</h6>
                </div>
                <ul class="nav">

                    @if(in_array('overview' , $rights) || \Auth::user()->type == 'admin')

                    <li class="nav-item"><a class="nav-link" href="{{route('staff.dashboard')}}">
                            <i class="dripicons-meter" aria-hidden="true"></i>
                            Overview</a></li>
                    @endif

                    @if(in_array('new_business' , $rights) || \Auth::user()->type == 'admin')
                    <li class="nav-item"><a class="nav-link" href="{{route('staff.sale')}}"> <i
                                class="far fa-chart-bar"></i> New Business</a></li>

                    @endif

                    @if(in_array('retention' , $rights) || \Auth::user()->type == 'admin')
                    <li class="nav-item"><a class="nav-link" href="{{route('staff.retention')}}"><i
                                class="far fa-user-circle"
                                aria-hidden="true"></i>
                            Retention</a></li>
                    @endif
                    @if(in_array('work_load' , $rights) || \Auth::user()->type == 'admin')
                    <li class="nav-item"><a class="nav-link" href="{{route('staff.workload')}}"><i class="ti-harddrives"></i>
                        Workload</a></li>
                    @endif
                </ul>
            </div>
            <!-- end Analytic -->
            <div class="main-icon-menu-pane">
                <div class="title-box">
                    <h6 class="menu-title">My Orders</h6>
                </div>
                <ul class="nav">
                    @if(in_array('campaigns' , $rights) || \Auth::user()->type == 'admin')
                    <li class="nav-item"><a class="nav-link" href="{{route('staff.campaigns')}}"> <i
                                class="ti-announcement"></i> Campaigns</a>
                    </li>
                    @endif
                    @if(in_array('landing_pages' , $rights) || \Auth::user()->type == 'admin')
                    <li class="nav-item"><a class="nav-link" href="{{route('staff.landing')}}"> <i
                                class="fa fa-file"></i> Landing Pages</a></li>
                    @endif
                    @if(in_array('projects' , $rights) || \Auth::user()->type == 'admin')
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('staff.other_project.listing')}}">
                            <i class="ti-view-grid"></i> Projects
                        </a>
                    </li>
                    @endif

                </ul>
            </div>

            <div class="main-icon-menu-pane">
                <div class="title-box">
                    <h6 class="menu-title">New Business hub</h6>
                </div>
                <ul class="nav">
                    @if(in_array('leads' , $rights) || \Auth::user()->type == 'admin')
                    <li class="nav-item"><a class="nav-link" href="{{route('staff.newbusinesshub.leads')}}"> <i
                                class="fa fa-file"></i>Leads</a></li> @endif
                    @if(in_array('deals' , $rights) || \Auth::user()->type == 'admin')
                    <li class="nav-item"><a class="nav-link" href="{{route('staff.newbusinesshub.deals')}}"> <i
                                class="ti-tag"></i>Deals</a>
                    </li>
                    @endif


                </ul>
            </div>
            <!-- end Crypto -->
            <div class="main-icon-menu-pane">
                <div class="title-box">
                    <h6 class="menu-title">Applications</h6>
                </div>
                <ul class="nav">
                    @if(in_array('clients_manager' , $rights) || \Auth::user()->type == 'admin')
                    <li class="nav-item"><a class="nav-link" href="{{route('staff.clients')}}"><i
                                class="dripicons-user"></i> Client Manager</a>
                    </li>
                     @endif
                    {{-- @if(in_array('support_ticket' , $rights) || \Auth::user()->type == 'admin')
                    <li class="nav-item"><a class="nav-link" href="{{route('staff.supportticket')}}"><i
                                class="ti-ticket"></i> Support Tickets</a></li>
                    @endif --}}
                    @if(in_array('invoice' , $rights) || \Auth::user()->type == 'admin')
                    <li class="nav-item"><a class="nav-link" href="{{route('staff.invoice')}}"> <i class="ti-receipt" aria-hidden="true"></i>Invoices
                        </a></li>
                    @endif
                    @if(in_array('agreements' , $rights) || \Auth::user()->type == 'admin')
                    <li class="nav-item"><a class="nav-link" href="{{route('staff.agreements')}}"> <i class="far fa-file-alt"></i> Agreements</a>
                    </li>
                    @endif
                </ul>
            </div>
            <!-- end  Project-->
            <div class="main-icon-menu-pane">
                <div class="title-box">
                    <h6 class="menu-title">My Applications</h6>
                </div>
                <ul class="nav">
                    <!-- <li class="nav-item"><a class="nav-link" href="#"> <i class="far fa-envelope-open"></i> Mailbox</a>
                    </li> -->
                    <!-- <li class="nav-item"><a class="nav-link" href="{{route('staff.Calender')}}"> <i
                                class="fas fa-calendar-alt"></i> Calender</a>
                    </li> -->
                    @if(in_array('tasks' , $rights) || \Auth::user()->type == 'admin')
                    <li class="nav-item"><a class="nav-link" href="{{route('staff.tasks')}}"> <i
                                class="fas fa-list-alt"></i> Tasks</a></li>
                    @endif
                    @if(in_array('holiday' , $rights) || \Auth::user()->type == 'admin')
                    <li class="nav-item"><a class="nav-link" href="{{route('staff.holiday')}}"> <i
                                class="far fa-clock"></i> Holidays and Time
                            Off</a></li>
                    @endif

                </ul>
            </div>
            <!-- end Ecommerce -->
            <div class="main-icon-menu-pane">
                <div class="title-box">
                    <h6 class="menu-title">Admin Tools</h6>
                </div>
                <ul class="nav">
                    <!-- <li class="nav-item"><a class="nav-link" href="#"><i class="ti-shortcode"></i>Campaigns Code</a>
                    </li> -->
                    @if(in_array('unbounce' , $rights) || \Auth::user()->type == 'admin')
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('staff.unbounce')}}">
                                <i class="fa fa-file"></i> Unbounce
                            </a>
                        </li>
                    @endif
                    {{-- @if(in_array('guide' , $rights) || \Auth::user()->type == 'admin')
                    <li class="nav-item"><a class="nav-link" href="{{route('staff.guide')}}"> <i class="ti-book"></i>
                            Guides</a></li>@endif
                    @if(in_array('faqs' , $rights) || \Auth::user()->type == 'admin')
                    <li class="nav-item"><a class="nav-link" href="{{route('staff.faqs')}}"><i
                                class="dripicons-question"></i>Faqs</a></li>@endif --}}
                    @if(in_array('staff_account' , $rights) || \Auth::user()->type == 'admin')
                    <li class="nav-item"><a class="nav-link" href="{{route('staff.staffAccount')}}">
                            <i class="dripicons-user-group"
                               aria-hidden="true"></i> Staff Account</a>
                    </li>
                    @endif

                     {{-- @if(in_array('target' , $rights) || \Auth::user()->type == 'admin')
                    <li class="nav-item"><a class="nav-link" href="{{route('staff.target')}}"> <i
                                class="dripicons-graph-line"></i>Targets</a></li> @endif --}}
                </ul>
            </div>
            <!-- end CRM -->
            <!-- end Authentication-->
        </div>
        <!--end menu-body-->
    </div>
    <!-- end main-menu-inner-->
</div>
<!-- end left-sidenav-->


@extends('staff.layouts.app')

@section('staff_css')
<link href='https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>
<style>
   .paginate_button{
      padding:0px !important;
   }
</style>
@endsection

@section('staff_content')
   <div class="page-content grey-bg compaign-sec-bg-1">
      <div class="container-fluid">
         <div class="row">
            <div class="col-sm-12">
               <div class="page-title-box resp-main-heading">
                  <!-- <div class="float-right">
                     <ol class="breadcrumb">
                        <button type="button" class="btn btn-primary text-uppercase">
                           <i class="fas fa-file-pdf"></i>&nbsp;&nbsp; Generate PDF Report
                        </button>
                     </ol>  
                  </div> -->
                  <h4 class="page-title overview-flot">Users</h4>
               </div>
               <!--end page-title-box-->
            </div>
         </div>
         <!--end col-->
         <div class="row top-search01">
            <div class="col-xl-12">
               <div class="float-left">
                  <div class="btn-group custom-btn-group" role="group" aria-label="Basic example">
                     <a href="#" class="btn btn-camp filter Factive " onclick="getCampaigns('all')">All Users</a>
                     <a href="#" class="btn btn-camp filter" onclick="getCampaigns('staff')">Staff Users </a>
                     <a href="#" class="btn btn-camp filter" onclick="getCampaigns('admin')">Admin Users</a> 
                  </div>
               </div>
             @can('isAdmin')
               <div class="float-left">
                  <a href="javascript:void(0)" class="btn btn-add01" id="addNew"> <i class="fa fa-plus"></i> Add New </a>
               </div>
               @endcan
            </div>
         </div>
         <div class="row">
            <div class="col-xl-12">
               <div class="table-responsive resp-overfolw">
                  <table class="table text-center td-block tableData">
                     <thead>
                        <tr class="th-head">
                           <th># </th>
                           <th>Name</th>
                           <!-- <th>UserName</th> -->
                           <th>Email</th>
                           <th>User Type</th>
                           <th></th>
                        </tr>
                     </thead>
                     <tbody>
                       
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
      <!-- end page content -->
   </div>

   <!-- Modal -->
   <div class="modal fade" id="addNewModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h5>Add User</h5>
               <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <form id="addCampaign">
               <div class="modal-body">
                  <div class="form-group">
                     <label for="name">Name:</label>
                     <input type="text" class="form-control"   name="name">
                  </div>
                  <!-- <div class="form-group">
                     <label for="UserName">UserName:</label>
                     <input type="text" class="form-control"   name="username">
                  </div> -->
                  <div class="form-group">
                     <label for="email">Email:</label>
                     <input type="text" class="form-control"   name="email">
                  </div>
                  <div class="form-group">
                     <label for="Password">Password:</label>
                     <input type="password" class="form-control"   name="password">
                  </div>
                  <div class="form-group">
                     <label for="landing_id">User Type:</label>
                     <select class="form-control" name="type" id="iType" required="">
                        <option disabled="" selected="">Select Type</option>
                        <option value="admin" >Admin</option>
                        <option value="staff">Staff</option>        
                  </select>
                  </div>
                  <div class="form-group" id="noOfleavediv" style="display: none">
                     <label for="number">No Of Leave:</label>
                     <input type="number" min="0" class="form-control" value="15"  name="no_of_leave" required="">
                  </div>
                 
                  
               </div>
              <div class="modal-footer">
                 <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                 <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
              </div>
            </form>
         </div>
      </div>
   </div>
 <div class="modal fade" id="addNewModel1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h5>Edit User</h5>
               <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <form id="editCampaign">
               <div class="modal-body">
                  <div class="form-group">
                     <label for="name">Name:</label>
                     <input type="hidden" class="form-control" id="id" name="id">
                     <input type="text" class="form-control" id="name" name="name">
                  </div>
                  <!-- <div class="form-group">
                     <label for="UserName">UserName:</label>
                     <input type="text" class="form-control" id="username" name="username">
                  </div> -->
                  <div class="form-group">
                     <label for="email">Email:</label>
                     <input type="email" class="form-control" id="email" name="email">
                  </div>
                  <div class="form-group">
                     <label for="Password">Password:</label>
                     <input type="password" class="form-control" id="password" name="password">
                  </div>
                  <div class="form-group">
                     <label for="landing_id">User Type:</label>
                     <select class="form-control" id="type" name="type" required="">
                        <option disabled="" selected="">Select Type</option>
                          <option value="admin">Admin</option>
                            <option value="staff">Staff</option>
                            <option value="customer">Customer</option>
                                   
                  </select>
                  </div>
                  <div class="form-group" id="noOfleavediv2" style="display: none">
                     <label for="number">No Of Leave:</label>
                     <input type="number" min="0" class="form-control"   name="no_of_leave"  id="no_of_leave" required="">
                  </div>
                  
               </div>
              <div class="modal-footer">
                 <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                 <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
              </div>
            </form>
         </div>
      </div>
   </div>
<div class="modal fade" id="rightsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h5>Edit User Rights</h5>
               <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <form id="rightForm">
               <div class="modal-body">
                  <div class="form-row">
                   <div class="form-group col-md-4 col-sm-6 label-button">
                       <label for="overview">Overview</label>
                       <div class="custom-control custom-switch dp-inline pl-5">
                           <input type="checkbox" name="right[]" value="overview" class="custom-control-input" id="overview">
                           <label class="custom-control-label" for="overview"></label>
                       </div>
                    </div>
                    <div class="form-group col-md-4 col-sm-6 label-button">
                       <label for="new_business">New Business</label>
                       <div class="custom-control custom-switch dp-inline pl-5">
                           <input type="checkbox" name="right[]" value="new_business" class="custom-control-input" id="new_business">
                           <label class="custom-control-label" for="new_business"></label>
                       </div>
                    </div>
                    <div class="form-group col-md-4 col-sm-6 label-button">
                       <label for="retention">Retention</label>
                       <div class="custom-control custom-switch dp-inline pl-5">
                           <input type="checkbox" name="right[]" value="retention" class="custom-control-input" id="retention">
                           <label class="custom-control-label" for="retention"></label>
                       </div>
                    </div>
                    <div class="form-group col-md-4 col-sm-6 label-button">
                       <label for="work_load">Work Load</label>
                       <div class="custom-control custom-switch dp-inline pl-5">
                           <input type="checkbox" name="right[]" value="work_load" class="custom-control-input" id="work_load">
                           <label class="custom-control-label" for="work_load"></label>
                       </div>
                    </div>
                    <div class="form-group col-md-4 col-sm-6 label-button">
                       <label for="campaigns">Campaigns</label>
                       <div class="custom-control custom-switch dp-inline pl-5">
                           <input type="checkbox" name="right[]" value="campaigns" class="custom-control-input" id="campaigns">
                           <label class="custom-control-label" for="campaigns"></label>
                       </div>
                    </div>
                    <div class="form-group col-md-4 col-sm-6 label-button">
                       <label for="landing_pages">Landing Pages</label>
                       <div class="custom-control custom-switch dp-inline pl-5">
                           <input type="checkbox" name="right[]" value="landing_pages" class="custom-control-input" id="landing_pages">
                           <label class="custom-control-label" for="landing_pages"></label>
                       </div>
                    </div>
                    <div class="form-group col-md-4 col-sm-6 label-button">
                       <label for="projects">Projects</label>
                       <div class="custom-control custom-switch dp-inline pl-5">
                           <input type="checkbox" name="right[]" value="projects" class="custom-control-input" id="projects">
                           <label class="custom-control-label" for="projects"></label>
                       </div>
                    </div>
                    <div class="form-group col-md-4 col-sm-6 label-button">
                       <label for="unbounce">Unbounce</label>
                       <div class="custom-control custom-switch dp-inline pl-5">
                           <input type="checkbox" name="right[]" value="unbounce" class="custom-control-input" id="unbounce">
                           <label class="custom-control-label" for="unbounce"></label>
                       </div>
                    </div>
                    <div class="form-group col-md-4 col-sm-6 label-button">
                       <label for="leads">Leads</label>
                       <div class="custom-control custom-switch dp-inline pl-5">
                           <input type="checkbox" name="right[]" value="leads" class="custom-control-input" id="leads">
                           <label class="custom-control-label" for="leads"></label>
                       </div>
                    </div>
                    <div class="form-group col-md-4 col-sm-6 label-button">
                       <label for="deals">Deals</label>
                       <div class="custom-control custom-switch dp-inline pl-5">
                           <input type="checkbox" name="right[]" value="deals" class="custom-control-input" id="deals">
                           <label class="custom-control-label" for="deals"></label>
                       </div>
                    </div>
                    <div class="form-group col-md-4 col-sm-6 label-button">
                       <label for="clients_manager">Clients Manager</label>
                       <div class="custom-control custom-switch dp-inline pl-5">
                           <input type="checkbox" name="right[]" value="clients_manager" class="custom-control-input" id="clients_manager">
                           <label class="custom-control-label" for="clients_manager"></label>
                       </div>
                    </div>
                    <div class="form-group col-md-4 col-sm-6 label-button">
                       <label for="support_ticket">Support Ticket</label>
                       <div class="custom-control custom-switch dp-inline pl-5">
                           <input type="checkbox" name="right[]" value="support_ticket" class="custom-control-input" id="support_ticket">
                           <label class="custom-control-label" for="support_ticket"></label>
                       </div>
                    </div>
                    <div class="form-group col-md-4 col-sm-6 label-button">
                       <label for="invoice">Invoice</label>
                       <div class="custom-control custom-switch dp-inline pl-5">
                           <input type="checkbox" name="right[]" value="invoice" class="custom-control-input" id="invoice">
                           <label class="custom-control-label" for="invoice"></label>
                       </div>
                    </div>
                    <div class="form-group col-md-4 col-sm-6 label-button">
                       <label for="agreements">Agreements</label>
                       <div class="custom-control custom-switch dp-inline pl-5">
                           <input type="checkbox" name="right[]" value="agreements" class="custom-control-input" id="agreements">
                           <label class="custom-control-label" for="agreements"></label>
                       </div>
                    </div>
                    <div class="form-group col-md-4 col-sm-6 label-button">
                       <label for="tasks">Tasks</label>
                       <div class="custom-control custom-switch dp-inline pl-5">
                           <input type="checkbox" name="right[]" value="tasks" class="custom-control-input" id="tasks">
                           <label class="custom-control-label" for="tasks"></label>
                       </div>
                    </div>
                    <div class="form-group col-md-4 col-sm-6 label-button">
                       <label for="holiday">Holidays & Time Off</label>
                       <div class="custom-control custom-switch dp-inline pl-5">
                           <input type="checkbox" name="right[]" value="holiday" class="custom-control-input" id="holiday">
                           <label class="custom-control-label" for="holiday"></label>
                       </div>
                    </div>
                    <div class="form-group col-md-4 col-sm-6 label-button">
                       <label for="guide">Guide</label>
                       <div class="custom-control custom-switch dp-inline pl-5">
                           <input type="checkbox" name="right[]" value="guide" class="custom-control-input" id="guide">
                           <label class="custom-control-label" for="guide"></label>
                       </div>
                    </div>
                    <div class="form-group col-md-4 col-sm-6 label-button">
                       <label for="faqs">Faqs</label>
                       <div class="custom-control custom-switch dp-inline pl-5">
                           <input type="checkbox" name="right[]" value="faqs" class="custom-control-input" id="faqs">
                           <label class="custom-control-label" for="faqs"></label>
                       </div>
                    </div>
                    <div class="form-group col-md-4 col-sm-6 label-button">
                       <label for="staff_account">Staff Account</label>
                       <div class="custom-control custom-switch dp-inline pl-5">
                           <input type="checkbox" name="right[]" value="staff_account" class="custom-control-input" id="staff_account">
                           <label class="custom-control-label" for="staff_account"></label>
                       </div>
                    </div>
                    <div class="form-group col-md-4 col-sm-6 label-button">
                       <label for="target">Target</label>
                       <div class="custom-control custom-switch dp-inline pl-5">
                           <input type="checkbox" name="right[]" value="target" class="custom-control-input" id="target">
                           <label class="custom-control-label" for="target"></label>
                       </div>
                    </div>
                 </div>
               </div>
              <div class="modal-footer">
               <input type="hidden" name="rights_user_id" id="rights_user_id">
                 <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                 <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
              </div>
            </form>
         </div>
      </div>
   </div>   
@endsection

@push('staff_js')
<script>
   var appURL = '{!! url('/staff/') !!}';
   var apiURL = '{!! url('/api/staff/') !!}';
</script>
{{--  Inject vue js files  --}}
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('js/cmn/cmn.js') }}"></script>
<script src="{{ asset('staff/js/staffaccount.js') }}"></script>
@endpush
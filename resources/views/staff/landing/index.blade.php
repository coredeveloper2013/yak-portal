@extends('staff.layouts.app')

@section('staff_css')
    <style>
        .invalid-feedback {
            display: block !important;
        }
    </style>
@endsection

@section('staff_content')
    <div class="page-content grey-bg compaign-sec-bg-1" id="StaffLandingPage">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box resp-main-heading">
                        <h4 class="page-title">Landing Pages</h4>
                    </div>
                </div>
            </div>
            <div class="row top-search01">
                <div class="col-xl-12">
                    <div class="float-left">
                        <div class="btn-group custom-btn-group" role="group" aria-label="Basic example">
                            <a href="javascript:;" class="btn btn-camp" :class="{ active: filter.status == 'awaiting_build' }"
                               @click="getLandingByStatus('awaiting_build')">Awaiting
                                Build</a>
                            <a href="javascript:;" class="btn btn-camp"
                               :class="{ active: filter.status == 'business_review' }"
                               @click="getLandingByStatus('business_review')">Business
                                Review</a>
                            <a href="javascript:;" :class="{ active: filter.status == 'customer_review' }"
                               class="btn btn-camp" @click="getLandingByStatus('customer_review')">Customer
                                Review</a>
                            <a href="javascript:;" :class="{ active: filter.status == 'awaiting_changes' }"
                               class="btn btn-camp" @click="getLandingByStatus('awaiting_changes')">Awaiting
                                Changes</a>
                            <a href="javascript:;" :class="{ active: filter.status == 'completed' }"
                               class="btn btn-camp" @click="getLandingByStatus('completed')">Completed</a>
                        </div>

                    </div>
                    <div class="float-left">
                        <span class="btn btn-add01" @click="getStaffLandingPagesById(0)">
                            <i class="fa fa-plus"></i> Add New 
                        </span>
                    </div>


                </div>

                {{-- <a data-toggle="modal" data-target="#viewLandingModal" href="javascript:void(0)" class="btn btn-btn btn-primary btn-sm p-1 px-2 mt-0 btn-sm"><i class="fa fa-eye"></i></a> --}}

            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div class="table-responsive">
                        <template>
                            <div>
                                <b-table responsive :items="landingPages" :fields="fields" :busy="isBusy" show-empty
                                         class="t-light td-block text-left" @filtered="Filtered">
                                    <template v-slot:emptyfiltered="scope">
                                        <h4>@{{ scope.emptyFilteredText }}</h4>
                                    </template>
                                    <template v-slot:table-busy>
                                        <div class="text-center text-danger my-2">
                                            <b-spinner class="align-middle"></b-spinner>
                                            <strong>Loading...</strong>
                                        </div>
                                    </template>
                                    <template slot="HEAD_name" slot-scope="data">
                                        <span class="m-0 pl-2">@{{data.label}}</span>
                                    </template>
                                    <template v-slot:cell(id)="data">
                                        #@{{ data.item.id }}
                                    </template>
                                    <template v-slot:cell(date)="data">
                                        @{{ data.item.human_date }}
                                    </template>
                                    <template v-slot:cell(description)="data">
                                        <span v-if="data.item.description.length <  40 "> @{{ data.item.description }}</span>
                                        <span v-else>@{{ data.item.description.substring(0,40)+".." }}</span>
                                    </template>
                                    {{-- <template v-slot:cell(url)="data">
                                        <span v-if="data.item.url.length <  30 "> @{{ data.item.url }}</span>
                                        <span v-else>@{{ data.item.url.substring(0,30)+".." }}</span>
                                    </template> --}}
                                    <template v-slot:cell(cost)="data">
                                        <span class="m-0 pl-2">
                                            <span>&#163;</span>@{{numberWithComma(data.item.cost)}}
                                        </span>
                                    </template>

                                    {{-- <template v-slot:cell(Budget)="data">
                                        <span class="m-0 pl-2">
                                           <span></span>
                                            <span v-if="data.item.budget != null">&#163;@{{numberWithComma(data.item.budget)}}</span>
                                            <span v-else>&#163;0.00</span>
                                        </span>
                                    </template> --}}
                                    <template v-slot:cell(Assignee)="data">
                                        <span class="m-0 pl-2">
                                            <strong v-if="data.item.assigned_to != null" class="badge badge-boxed badge-soft-success">@{{ findAssignee(data.item.assigned_to) }}</strong>
                                            <strong v-else class="badge badge-boxed badge-soft-danger">None</strong>
                                        </span>
                                    </template>
                                    {{-- <template v-slot:cell(created_by)="data">
                                        <strong v-if="data.item.created_by != null" class="badge badge-boxed badge-soft-success">@{{ findAssignee(data.item.created_by) }}</strong>
                                        <strong v-else class="badge badge-boxed badge-soft-danger">None</strong>
                                    </template> --}}
                                    {{-- <template v-slot:cell(status)="data">
                                        <span class="badge badge-boxed badge-soft-success" v-if="data.item.status != 'PENDING'">
                                            @{{data.item.status}}
                                        </span>
                                        <span class="badge badge-boxed badge-soft-danger" v-else> @{{ data.item.status }}</span>
                                    </template> --}}
                                    <template v-slot:cell(actions)="data">
                                        <span class="btn-group btn-flex"><!-- <b-button size="sm" title="Assign to me" variant="btn btn-primary btn-sm p-1 px-2 mt-0" @click="assignTome(data.item)">
                                            <i class="fa fa-user"></i>
                                        </b-button> -->
                                        <b-button size="sm" title="Edit" @click="getStaffLandingPagesById(data.item.id)" variant="btn btn-primary btn-sm p-1 px-2 mt-0">
                                            <i class="fa fa-edit"></i>
                                        </b-button>
                                        <b-button :href="'/staff/landing-page-details/' + data.item.id" size="sm" title="View Details" variant="btn btn-primary btn-sm p-1 px-2 mt-0">
                                                <i class="fa fa-eye"></i>
                                        </b-button>
                                    </span>
                                    </template>
                                </b-table>
                                {{-- Row --}}
                                <div class="row  m-auto">
                                    <div class="col-xl-6 col-lg-6 col-md-6 float-left">
                                        <p class="mb-0 p-1 small mt-1 float-left">Showing @{{ landingPagesMeta.from }}
                                            to @{{ landingPagesMeta.to }} of @{{ landingPagesMeta.total }} records | Per
                                            Page: @{{ landingPagesMeta.per_page }} | Current Page: @{{
                                            landingPagesMeta.current_page }}</p>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-6 float-right t012">
                                        <b-pagination class="float-right" size="sm" :total-rows="landingPagesMeta.total"
                                                      v-model="landingPagesMeta.current_page"
                                                      :per-page="landingPagesMeta.per_page" first-text="First"
                                                      prev-text="Previous" next-text="Next" last-text="Last"
                                                      ellipsis-text="More" variant="danger">
                                        </b-pagination>
                                    </div>
                                </div>
                                {{-- End Row --}}
                            </div>
                        </template>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="editLandingModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4>Edit Landing Page For Customer </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: red">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="{{route('staff.landing.update')}}" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" v-model="lPages.id"  name="id">
                            <div class="row">
                                <div class="col-xl-6">
                                    <div class="form-group">
                                        <label>Landing Page Name<span class="req">*</span></label>
                                        
                                        <input type="text" v-model="lPages.name"  name="name" class="form-control">
                                    </div>
                                </div>

                                <div class="col-xl-6">
                                    <div class="form-group">
                                        <label>Status</label>
                                        <select class="form-control" v-model="lPages.status" name="status">
                                            <option disabled="" selected>Select status</option>
                                            <option value="awaiting_build">
                                                Awaiting Build
                                            </option>
                                            <option value="business_review">
                                                Business Review
                                            </option>
                                            <option value="customer_review">
                                                Customer Review
                                            </option>
                                            <option value="awaiting_changes">
                                                Awaiting Changes
                                            </option>
                                            <option value="completed">
                                                Completed
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="form-group">
                                        <label>Description<span class="req">*</span></label>
                                        <textarea class="form-control" v-model="lPages.description" name="description"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xl-6">
                                    <div class="form-group">
                                        <label>Date</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" v-model="lPages.date" name="date" id="date">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="dripicons-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-xl-6">
                                    <div class="form-group">
                                        <label>URL<span class="req">*</span></label>
                                        <input type="text" v-model="lPages.url" name="url" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Staff Member<span class="req">*</span></label>
                                        <multiselect v-model="lPages.staff_id" :options="staffMembers" :show-labels="false" placeholder="Select Staff Member" label="name" track-by="id">
                                            <template slot="noResult" slot-scope="data">
                                                <strong>Sorry, couldn't find staff member with this name.</strong>
                                            </template>
                                        </multiselect>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Customers<span class="req">*</span></label>
                                        <multiselect v-model="lPages.customer_id" :options="customers" :show-labels="false" placeholder="Select Customer" label="name" track-by="id">
                                            <template slot="noResult" slot-scope="data">
                                                <strong>Sorry, couldn't find customer with this name.</strong>
                                            </template>
                                        </multiselect>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                                <button type="button" @click="createUpdateLandingPage()" class="btn btn-primary btn-sm" :disabled="disableSubmitBtn">Save changes</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page content -->
    </div>

@endsection

@push('staff_js')
    <script>
        //var App_url = '{!! url()->current() !!}';
        var App_url = '{!! url('/api/') !!}/staff/landing-pages';
        var user_id = '{!! Auth::user()->id !!}';
    </script>
    {{--  Inject vue js files  --}}
    <script src="{{ asset('js/app-vue.js') }}"></script>
    <script src="{{ asset('js/staff/landing/vue.js') }}"></script>
@endpush

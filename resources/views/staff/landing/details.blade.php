@extends('staff.layouts.app')
@section('staff_css')
    <style>
        .invalid-feedback {
        display: block !important;
        }
        .chat-body {
        padding: 12px 0 10px 12px !important;
        }
    </style>
@endsection

@section('staff_content')
    <div class="page-content grey-bg">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Landing Page Details</h4>
                    </div>
                    <!--end page-title-box-->
                </div>
                <!--end col-->
            </div>
            <div class="row">
                <div class="col-xl-4 col-lg-5 col-md-5">
                <div class="detail-scroll">
                    <div class="row">
                        <div class="col-12">
                            <div class="blue-badge text-center">
                                <div>Landing Page Name</div>
                                <h3 class="capital-text">{{$landing_page->name}}</h3>
                            </div>
                        </div>
                        <div class="col-6">
                            <a href="javascript:;" class="blue-plane text-center"><span> {{$landing_page->status}}</span></a>
                        </div>
                        <div class="col-6">
                            <a href="javascript:;" class="blue-plane text-center">{{$landing_page->date}}</a>
                        </div>
                        <div class="col-12">
                            <div class="purple-badge text-center">
                                <h3>{{@$landing_page->user->name}}</h3>
                                <a href="javascript:;" class="btn btn-light-o"  data-toggle="modal" data-target="#contact_detail">View Contact Details</a>
                            </div>
                        </div>
                        
                                            
                        <!-- <div class="col-6">
                            <a href="javascript:;" class="green-plane text-center">Contract</a>
                            </div>
                            <div class="col-6">
                            <a href="javascript:;" class="green-plane text-center">Payment</a>
                            </div> -->
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <h3 class="card-heading">Description</h3>
                            <p class="decs-grey">
                                {{ $landing_page->description }}
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="card asign-detail">
                            <div class="card-header">
                                Assign Details
                            </div>
                            <ul class="list-group">
                                @if($landing_page->assigned)
                                <li class="list-group-item">
                                    <img @if(!is_null($landing_page->assigned->image)) src="{{asset('/storage/avatars/'.$landing_page->assigned->image)}}"
                                    @else src="{{asset('/images/user.png')}}"
                                    @endif
                                    class="mr-3">
                                    <span class="title-list">{{$landing_page->assigned->name}}</span>
                                    <span class="badge badge-pill badge-grey float-right">{{$landing_page->assigned->type}}</span>
                                </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-xl-8 col-lg-7 col-md-7">

                    <div class="card">
                        <div class="card-body">
                            <h3 class="card-heading">Notes</h3>
                            <div class="notes01">
                                <ul class="note-listing">
                                    @forelse ($landing_page->comments as $comment)
                                    <li class="media comment-blk">
                                        <img src="{{asset('/staff/images/user-1.jpg')}}" alt="img" class="mr-3">
                                        <!-- <img src="http://127.0.0.1:8000/staff/images/user-1.jpg" class="mr-3"> -->
                                        <div class="media-body">
                                            <div class="name-time-o">
                                                {{$comment->user->name}}
                                                <span><i class="mdi mdi-clock-outline"></i> </span>
                                                <span>{{$comment->created_at->format('d.m.Y')}} | </span>
                                                <span>{{$comment->created_at->format('g:i:s A')}}</span>
                                                <a href="javascript:;" class="del-o del-cmnt" data-comment_id="{{$comment->id}}" data-parent_id="{{$landing_page->id}}"><i class="far fa-trash-alt"></i></a>
                                            </div>
                                            <p class="msg-o">
                                                {!! $comment->description_br !!}
                                            </p>
                                            
                                        </div>
                                    </li>
                                    @empty
                                    <span class="text-center d-block">Not Notes Found.</span>
                                    @endforelse
                                </ul>
                                <div class="add-notes">
                                    <div class="form-group">
                                        <textarea class="form-control" placeholder="Type your note here" id="comment_text"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <input type="button" name="" id="send_comment" class="btn btn-darkblue" value="Add note">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="contact_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h5>Customer Contact Detail</h5>
               <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
            </div>

            <div class="modal-body">
                <div class="form-group">
                    <p><i class="far fa-user float-left"></i>&nbsp; {{ $landing_page->user->name }}</p>
                </div>

                @if(!empty($landing_page->user->phone))
                    <div class="form-group">
                        <p><i class="fa fa-phone float-left"></i>&nbsp; {{ $landing_page->user->phone }}</p>

                    </div>
                @endif

                <div class="form-group">

                    <p><i class="far fa-envelope float-left"></i>&nbsp; {{ $landing_page->user->email }}</p>
                </div>

                @if(!empty($landing_page->user->business_info))
                    @if(!empty($landing_page->user->business_info->email_address))
                        <div class="form-group">
                            <p><i class="far fa-envelope float-left"></i>&nbsp; {{ $landing_page->user->business_info->email_address }}</p>

                        </div>
                    @endif

                    @if(!empty($landing_page->user->business_info->phone))
                        <div class="form-group">
                            <p><i class="fa fa-phone float-left"></i>&nbsp; {{ $landing_page->user->business_info->phone }}</p>

                        </div>
                    @endif

                    @if(!empty($landing_page->user->business_info->website))
                        <div class="form-group">
                            <p><i class="fa fa-globe float-left"></i>&nbsp; {{ $landing_page->user->business_info->website }}</p>

                        </div>
                    @endif
                @endif
                <div class="form-group">

                    <p><i class="fa fa-check float-left"></i>&nbsp; {{ ucfirst($landing_page->user->status) }} Client</p>
                </div>
                @if(!empty($landing_page->user->business_info))
                    @if(!empty($landing_page->user->business_info->notes))
                <div class="form-group">

                    <p><i class="fa fa-comments float-left"></i>&nbsp; {{ $landing_page->user->business_info->notes }} </p>
                </div>
                    @endif
                @endif
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
            </div>
         </div>
      </div>
    </div>
@endsection

@push('staff_js')
    <script>
        var appURL = '{!! url('/staff/') !!}';
        var apiURL = '{!! url('/api/staff/') !!}';
        var UnqId = '{{ $landing_page->id }}';
    </script>
    <script type="text/javascript">
        $(function() {
            var csrf = $('meta[name="csrf-token"]').attr("content");
        
            var flag2 = false;
            $('body').on('click', '#send_comment', function(event) {
                event.preventDefault();
                if (flag2) {
                    return false;
                }
                flag2 = true;
        
                var text = $('#comment_text').val();
        
                text = text.trim();
                //console.log('Hello : ', text);
        
                if (text == '') {
                    Swal.fire({
                        title: 'error',
                        text: 'Please enter some text in note area',
                        icon: 'error'
                    });
                    flag2 = false;
                    return false
                }
        
                $.ajax({
                    url: appURL+'/landing-page/comment',
        
                    type: 'POST',
                    dataType: 'json',
                    data: {id: UnqId, text:text},
                    headers: {
                        "X-CSRF-TOKEN": csrf,
                        Accpet: "applicationjson"
                    },
                }).always(function(res) {
                    flag2 = false;
                    if (res.status == 'success') {
                        let status = res.status.substr(0,1).toUpperCase()+res.status.substr(1);
                        Swal.fire({
                                title: status,
                                text: res.msg,
                                icon: res.status,
                                onAfterClose: () => {
                                location.reload();
                    }
                    });
                    } else {
                        let status = res.status.substr(0,1).toUpperCase()+res.status.substr(1);
                        Swal.fire({
                            title: status,
                            text: res.msg,
                            icon: res.status
                        });
                    }
                });
        
            });

            var flag3 = false;
        $('body').on('click', '.del-cmnt', function(event) {
            event.preventDefault();
            if (flag3) {
                return false;
            }
            flag3 = true;

            var that = $(this);
            var id = that.data('comment_id');
            var pid = that.data('parent_id');

            $.ajax({
                url: apiURL+'/landing-page/comment/delete',
                type: 'POST',
                dataType: 'json',
                data: {comment_id: id, parent_id:pid},
                headers: {
                    "X-CSRF-TOKEN": csrf,
                    Accpet: "applicationjson"
                },
            }).always(function(res) {
                flag3 = false;
                let status = res.status.substr(0,1).toUpperCase()+res.status.substr(1);
                if (res.status == 'success') {
                    Swal.fire({
                        title: status,
                        text: res.msg,
                        icon: res.status,
                        onAfterClose: () => {
                            that.parents('.comment-blk').remove();
                        }
                    });
                } else {
                    Swal.fire({
                        title: status,
                        text: res.msg,
                        icon: res.status
                    });
                }
            });
        });
        
        
        });
    </script>
@endpush
import Multiselect from "vue-multiselect";
new Vue({
    el: '#StaffLandingPage',
    components: {Multiselect},
    mounted() {
        this.filter.status = 'awaiting_build';
        this.getStaffLandingPages(false);
        this.getStaffMembers();
        this.getCustomers();

        var context = this;
        $('#date').daterangepicker({
            opens: 'left',
            singleDatePicker: true,
            showDropdowns: true,
            maxDate: moment(),
            minDate: moment(),
            startDate: moment()
        }, function (start, end, label) {
            context.lPages.date = start.format('MM/DD/YYYY');
        });
    },
    data: {
        fields: [
            {key: 'id', label: 'Order'}, 
            {key: 'name', label: 'Name'}, 
            {key: 'description', label: 'Description'},
            {key: 'customer', label: 'Customer'},
            'date', 
            {key: 'assigned', label: 'Assigned To'},
            {key: 'actions', label: 'Actions'}
        ],
        landingPages: [],
        lPages:{
            id:'',
            status:'',
            name:"",
            description:"",
            url:"",
            date:moment().format('MM/DD/YYYY'),
            staff_id:[],
            customer_id:[]
        },
        landingPagesMeta: {},
        staffMembers: [],
        customers: [],
        currentUser: user_id,
        otherAssigneId: "",
        landingPageId: "",
        filter: {
            status: '',
        },
        isBusy: false,
        disableSubmitBtn: false,
        getStaffMembersUrl : App_url + '/get/staff/members',
        getCustomersUrl : App_url + '/get/customers',
        getLandingPagesUrl : App_url + '/data',
        getLandingPagesCreateUrl : App_url + '/create',
        getLandingPagesUpdateUrl : App_url + '/update',
        getLandingPagesUrlById : App_url + '/get',
        assignToMeUrl: App_url + '/assign/me',
        assignToOtherUrl: App_url + '/assign/other',
    },
    watch: {
        'landingPagesMeta.current_page': function (val) {
            this.loadPaginatedData();
        } 
        
    },
    methods: {
        loadPaginatedData: function () {
            this.getStaffLandingPages(this.getLandingPagesUrl + '?page=' + this.landingPagesMeta.current_page);
        },
        getStaffLandingPages: function (url = false) {
            this.isBusy = true;
            if(url == false){
                url = this.getLandingPagesUrl;
            }
            axios.post(url, {filter: this.filter})
                .then(response => {
                    if(response.data.status == 'success'){
                        this.isBusy = false;
                        this.landingPages = response.data.data;
                        this.landingPagesMeta = response.data.meta;
                    }
                }).catch(error => {
                console.log(error);
                this.isBusy = false;
            });
        },
        getStaffLandingPagesById: function (idd) {
            if (idd > 0) {
                axios.post(this.getLandingPagesUrl, {id: idd})
                    .then(response => {
                        if(response.data.status == 'success'){
                            this.isBusy = false;
                            var landingData = response.data.data[0];
                            this.lPages = {
                                id:landingData.id,
                                status:landingData.statuss,
                                name:landingData.name,
                                description:landingData.description,
                                date:landingData.date,
                                url:landingData.url,
                                customer_id:{id:landingData.user_id, name:landingData.customer},
                                staff_id:{id:landingData.assigned_to, name:landingData.assigned}
                            };
                            $('#editLandingModal').modal('show');
                        }
                    }).catch(error => {
                    console.log(error);
                    this.isBusy = false;
                });
            } else {
                this.lPages = {
                    id:'',
                    status:'',
                    name:"",
                    description:"",
                    url:"",
                    date:"",
                    customer_id:[],
                    staff_id:[]
                };
                $('#editLandingModal').modal('show');
            }
        },
        createUpdateLandingPage:function () {
            this.disableSubmitBtn = true;
            let formData = new FormData();
            formData.append('id', this.lPages.id);
            formData.append('name', this.lPages.name);
            formData.append('status', this.lPages.status);
            formData.append('description', this.lPages.description);
            formData.append('url', this.lPages.url);
            formData.append('date', this.lPages.date);
            formData.append('customer_id', this.lPages.customer_id.id);
            formData.append('staff_id', this.lPages.staff_id.id);

            axios.post( this.getLandingPagesUpdateUrl, formData).then(response => {
                this.disableSubmitBtn = false;
                let res = response.data;
                let status = res.status.substr(0,1).toUpperCase()+res.status.substr(1);
                if (res.status == 'success') {
                    Swal.fire({
                        title: status,
                        text: res.msg,
                        icon: res.status,
                        onAfterClose: () => {
                            this.filter.status = this.lPages.status;
                            this.getStaffLandingPages(false);
                             $('#editLandingModal').modal('hide');
                            //location.reload();
                        }
                    });
                } else {
                    Swal.fire({
                        title: status,
                        text: res.msg,
                        icon: res.status
                    });
                }
            }).catch(error => {
                this.disableSubmitBtn = false;
            });
        },
        getStaffMembers: function(){
          axios.get(this.getStaffMembersUrl)
              .then(response => {
                  if(response.data.status == 'success'){
                      this.staffMembers = response.data.data;
                  }
              }).catch(error => {
                  console.log(error);
          });
        },
        getCustomers: function(){
          axios.get(this.getCustomersUrl)
              .then(response => {
                  if(response.data.status == 'success'){
                      this.customers = response.data.data;
                  }
              }).catch(error => {
                  console.log(error);
          });
        },
        assignTome: function(landing_page){
            if(landing_page.staff_id != null){
                Swal.fire({
                    title: 'Sorry!',
                    text: 'You cannot change the already assigned staff member.',
                    icon: 'warning',
                });
                return false;
            }
            axios.post(this.assignToMeUrl, {id: landing_page.id})
                .then(response => {
                    if(response.data.status == 'success'){
                        Swal.fire({
                            title: 'Success!',
                            text: 'Order #'+landing_page.id+' assigned to you.',
                            icon: 'success',
                        });
                        this.getStaffLandingPages(false);
                    }
                })
        },
        showAssignToOtherModal: function(landing_page){
            this.landingPageId = landing_page.id;
            $("#AssignToOthersModal").modal("show");
        },
        assignToOther: function(){
          axios.post(this.assignToOtherUrl, {id: this.landingPageId, assigned_to_id: this.otherAssigneId})
              .then(response => {
                  if(response.data.status == 'success'){
                      Swal.fire({
                          title: 'Success!',
                          text: 'Order #'+this.landingPageId+' assigned to '+this.findAssignee(this.otherAssigneId)+'.',
                          icon: 'success',
                      });
                      this.getStaffLandingPages(false);
                      $("#AssignToOthersModal").modal("hide");
                  }
              });
        },
        getLandingByStatus: function(status){
            this.filter.status = status;
            this.getStaffLandingPages(false);
        },
        findAssignee: function(id){
            let assigneName = '';
            this.staffMembers.forEach(function (item) {
                if (item.id === id) {
                    assigneName = item.name;
                }
            });
            return assigneName;
        },
        resetFilter: function(){
            this.filter = {
                status : ''
            };
            this.getStaffLandingPages(false);
        },
        numberWithComma: function(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        },
        Filtered(filteredItems) {
            // Trigger pagination to update the number of buttons/pages due to filtering
            this.landingPagesMeta.totalRows = filteredItems.length;
            this.landingPagesMeta.currentPage = 1;
        },
    }
});

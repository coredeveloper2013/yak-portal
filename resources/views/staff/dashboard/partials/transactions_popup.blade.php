<div class="modal" id="orders">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Biggest Orders (this month)</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
       <div class="three-col-w">
                         <!-- for add scroler add class name append "scrollclass" with three-col-w-center -->
                        <div class="three-col-w-center ">

                          <!-- only repeat in this loop -->
                          @foreach($this_month_biggest_order as $order)
                          @php
                            $text_class = 'text-primary';
                            if($order->status ==  'completed')
                            {
                              $text_class = 'text-success';
                            }
                          @endphp
                          <div class="three-col-w-center-single three-col-w-center-single-1">
                            <div class="row">
                              <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9">
                                  <div class="mr-8 float-left">
                                    <a href="#">
                                      <img alt="Benjamin Boyce Thumb" class="rounded-circle float-left" src="https://uat-xcarta-pr.s3.amazonaws.com/uploads/user/image/1272/thumb_Benjamin_Boyce_pic.jpg" width="40" height="40">
                                    </a>
                                  </div>
                                  <div class="author-forum-l">
                                          <a href="#" class="black-text">
                                            <h4>{{$order->name}}</h4>
                                          </a>
                                          <span class="font-12 lightgray">{{$order->created_at}}</span>
                                  </div>
                              </div>
                              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
                                <div class="time-name">
                                 <span class="font-weight-bold">£{{$order->total_amount}}</span>
                                 <span class="{{$text_class}}">{{$order->status}}</span>
                               </div>
                              </div>
                              
                            </div>
                          </div>
                          @endforeach
                          <!-- end -->
                        </div>
                        
                     </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
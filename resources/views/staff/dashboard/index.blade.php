@extends('staff.layouts.app')

@section('staff_css')

<style>



</style>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
@endsection

@section('staff_content')


   <div class="page-content">
      <div class="container-fluid">
         <div class="row">
            <div class="col-sm-12">
               <div class="page-title-box overview-resp-p-h">
                  <!-- <div class="float-right">
                     <ol class="breadcrumb">
                        <button type="button" class="btn btn-primary text-uppercase">
                           <i class="fas fa-file-pdf"></i>&nbsp;&nbsp; Generate PDF Report
                        </button>
                     </ol>
                  </div> -->
                  <h4 class="page-title overview-flot">Overview</h4>
               </div>
               <!--end page-title-box-->
            </div>
         </div>


         <!-- start -->
         <div class="row">
           <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
             <div class="card mt-3">
               <div class="card-body card-border">
                  <div class="single-line-w">
                     <h5 class="card-title graph-title">
                       New Orders (this week)
                     </h5>
                     <h4 class="ret-o">{{number_format($totals_this_week[0]->total_orders)}}

                      <!-- <span class="@php echo $change_percentage['total_orders'] <= 0 ? 'text-danger' : 'text-success'; @endphp">{{number_format($change_percentage['total_orders'])}}%&nbsp;<i class="@php echo $change_percentage['total_orders'] <= 0 ? 'fas fa-arrow-down' : 'fas fa-arrow-up'; @endphp"></i>&nbsp;</span>

                      <span class="lightgray">than last week</span> --></h4>
{{--                     <div id="chart_div1"></div>--}}
                  </div>
               </div>
             </div>
           </div>
           <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
             <div class="card mt-3">
               <div class="card-body card-border">
                  <div class="single-line-w">
                     <h5 class="card-title graph-title">
                       AVG Order Value
                     </h5>
                     <h4 class="ret-o">£{{number_format($totals_this_week[0]->avg_order_value)}}

                       <!-- <span class="@php echo $change_percentage['avg_order_value'] <= 0 ? 'text-danger' : 'text-success'; @endphp">{{number_format($change_percentage['avg_order_value'])}}%&nbsp;<i class="@php echo $change_percentage['avg_order_value'] <= 0 ? 'fas fa-arrow-down' : 'fas fa-arrow-up'; @endphp"></i>&nbsp;</span>

                      <span class="lightgray">than last week</span> --></h4>
{{--                     <div id="chart_div2"></div>--}}
                  </div>
               </div>
             </div>
           </div>
           <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
             <div class="card mt-3">
               <div class="card-body card-border">
                  <div class="single-line-w">
                     <h5 class="card-title graph-title">
                       Total Sales (this week)
                     </h5>
                     <h4 class="ret-o">£{{number_format($totals_this_week[0]->total_sales)}}

                       <!-- <span class="@php echo $change_percentage['total_sales'] <= 0 ? 'text-danger' : 'text-success'; @endphp">{{number_format($change_percentage['total_sales'])}}%&nbsp;<i class="@php echo $change_percentage['total_sales'] <= 0 ? 'fas fa-arrow-down' : 'fas fa-arrow-up'; @endphp"></i>&nbsp;</span>

                      <span class="lightgray">than last week</span> --></h4>
{{--                     <div id="chart_div3"></div>--}}
                  </div>
               </div>
             </div>
           </div>
         </div>
         <!-- end -->
          <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
              <div class="card">
                <div class="card-body card-border">
                    <h5 class="card-title graph-title">
                     New Business
                    </h5>
                    <p class="font-14 lightgray">This is the total New Business sales across all orders.</p>
                  <div class="mb-3">
                     <div class="graph-container">
                        <div id="newBussinessChart"></div>
                      <!-- implement chart component code here -->
                     </div>
                  </div>
                  <div>
                    <div class="float-left w-50 pa-5">
                      <h4 class="ret-o">£{{number_format($last_month_total_new[0]->total_sales)}}</h4>
                      <div class="text-primary font-weight-bold text-uppercase">  New business (LAST MONTH)</div>
{{--                      <div class="font-12 mt-2 lightgray">Customers who have upgraded the level of your product and services.--}}
{{--                      </div>--}}
                    </div>
                    <div class="float-right w-50 pa-5">
                      <h4 class="ret-o">£{{number_format($this_month_total_new[0]->total_sales)}}</h4>
                      <div class="text-danger font-weight-bold text-uppercase">  New business (THIS Month)</div>
{{--                      <div class="font-12 mt-2 lightgray">Customers who have upgraded the level of your product and services.--}}
{{--                      </div>--}}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
              <div class="card">
                <div class="card-body card-border">
                    <h5 class="card-title graph-title">
                     Account Retention
                    </h5>
                    <p class="font-14 lightgray">This is the total Account Retention sales across all orders.</p>
                  <div class="mb-3">
                     <div class="graph-container">
                      <div id="retentionChart"></div>
                      <!-- implement chart component code here -->
                     </div>
                  </div>
                  <div>
                    <div class="float-left w-50 pa-5">
                      <h4 class="ret-o">£{{number_format($last_month_total_retention[0]->total_sales)}}</h4>
                      <div class="text-primary font-weight-bold text-uppercase">  Recurred Spend (LAST MONTH)</div>
{{--                      <div class="font-12 mt-2 lightgray">Customers who have upgraded the level of your product and services.--}}
{{--                      </div>--}}
                    </div>
                    <div class="float-right w-50 pa-5">
                      <h4 class="ret-o">£{{number_format($this_month_total_retention[0]->total_sales)}}</h4>
                      <div class="text-danger font-weight-bold text-uppercase">  Recurred Spend (THIS Month)</div>
{{--                      <div class="font-12 mt-2 lightgray">Customers who have upgraded the level of your product and services.--}}
{{--                      </div>--}}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- end two column -->
          <!-- start three column -->
            <div class="row">
              <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12">
                <div class="card mt-3">
                  <div class="card-border">
                     <div class="three-col-w">
                        <h5 class="card-title graph-title m-0">
                         Biggest Orders (this month)
                        </h5>
                        <!-- for add scroler add class name append "scrollclass" with three-col-w-center -->
                        <div class="three-col-w-center ">

                          <!-- only repeat in this loop -->
                          @php $i = 1 @endphp
                          @foreach($this_month_biggest_order as $order)
                          @php
                            $text_class = 'text-primary';
                            if($order->status ==  'completed')
                            {
                              $text_class = 'text-success';
                            }
                          @endphp
                          <div class="three-col-w-center-single three-col-w-center-single-1">
                            <div class="row">
                              <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9">
                                  <div class="mr-8 float-left">
                                    <a href="#">
                                      <img alt="Benjamin Boyce Thumb" class="rounded-circle float-left"
                                           @if(!is_null($order->image)) src="{{asset('/storage/avatars/'.$order->image)}}"
                                           @else src="{{asset('/images/user.png')}}"
                                           @endif
                                           width="40" height="40">
                                    </a>
                                  </div>
                                  <div class="author-forum-l">
                                          <a href="#" class="black-text">
                                            <h4>{{$order->name}}</h4>
                                          </a>
                                          <span class="font-12 lightgray">{{$order->created_at}}</span>
                                  </div>
                              </div>
                              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
                                <div class="time-name">
                                 <span class="font-weight-bold">£{{$order->total_amount}}</span>
                                 <!-- <span class="{{$text_class}}">{{$order->status}}</span> -->
                               </div>
                              </div>

                            </div>
                          </div>
                          @php
                            if($i == 5)
                               break;
                            $i++;
                          @endphp
                          @endforeach
                          <!-- end -->
                        </div>
                        <!-- <div class="three-col-w-bottom">
                          <a href="#" onclick="showModal('#orders')" class="lightgray">View all transaction <i class="fas fa-car"></i></a>
                        </div> -->
                     </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12">
                <div class="card mt-3">
                  <div class="card-border">
                     <div class="three-col-w">
                        <h5 class="card-title graph-title m-0">
                         Latest Customers
                        </h5>
                        <!-- for add scroler add class name append "scrollclass" with three-col-w-center -->
                        <div class="three-col-w-center ">

                          <!-- only repeat in this loop -->
                           @php $i = 1 @endphp
                          @foreach($this_month_customers as $customer)
                          <div class="three-col-w-center-single three-col-w-center-single-1">
                            <div class="row">
                              <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">
                                  <div class="mr-8 float-left">
                                    <a href="javascript:;">
                                      <img alt="Benjamin Boyce Thumb" class="rounded-circle float-left"
                                           @if(!is_null($customer->image))
                                           src="{{asset('/storage/avatars/'.$customer->image)}}"
                                           @else src="{{asset('/images/user.png')}}"
                                           @endif
                                           width="40" height="40">
                                    </a>
                                  </div>
                                  <div class="author-forum-l">
                                          <a href="#" class="black-text">
                                            <h4>{{$customer->name}}</h4>
                                          </a>
                                          <span class="font-12 lightgray">{{$customer->created_at}}</span>
                                  </div>
                              </div>
                              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12">
                                <div class="time-name text-right time-cloud">
                                 <span class="font-weight-bold"><!-- <i class="fas fa-cloud"></i> --></span>

                               </div>
                              </div>

                            </div>
                          </div>
                           @php
                            if($i == 5)
                               break;
                            $i++;
                          @endphp
                          @endforeach
                          <!-- end -->
                        </div>
                        <!-- <div class="three-col-w-bottom">
                          <a href="#"  onclick="showModal('#latest_customer')" class="lightgray">View More Customers <i class="fas fa-arrow-down"></i></a>
                        </div> -->
                     </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12">
                <div class="card mt-3">
                  <div class="card-border">
                     <div class="support-track-w">
                        <h5 class="card-title graph-title m-0">
                          Support Tracker
                        </h5>
                       <ul>
                        <li>
                         Open Tickets
                         <span class="font-weight-bold">{{$activeTickets[0]->total_tickets}}</span>
                       </li>
                       <li>
                         Closed Tickets
                         <span class="font-weight-bold">{{$closedTickets[0]->total_tickets}}</span>
                       </li>

                       </ul>
                        <div class="support-track-w-bottom">
                          <span class="text-danger">Completed Tickets</span>
                          <span class="font-weight-bold">{{$percentSolved}}%</span>
                        </div>
                     </div>
                  </div>
                </div>
              </div>
            </div>
          <!-- end three column -->
            <div class="row">
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12">
                <div class="card mt-3">
                  <div class="card-body card-border">
                     <div class="pie-chart-w">
                        <h5 class="card-title graph-title">
                         Customers
                        </h5>
                         <div id="piechart"></div>
                        <ul>
                          @foreach($customers_by_status as $cust)
                          @php

                            $textClass = ($cust->status == 'new' ?'text-primary' : ($cust->status == 'gold' ? 'text-warning' : 'text-danger') );
                          @endphp
                          <li>
                            <i class="fas fa-cloud {{$textClass}}"></i>&nbsp;{{ucfirst($cust->status)}} <span class="float-right">{{$cust->total_customers}}</span>
                          </li>
                          @endforeach
                        </ul>
                     </div>
                  </div>
                </div>
              </div>

               <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">
                <div class="card mt-3">
                  <div class="card-border">
                    <div class="card-body ">
                      <h5 class="card-title graph-title">Employee Performance (this month) </h5>

                    </div>
                    <div class="table-responsive employe-per-tbl">
                    <table class="table">
                      <thead>
                        <tr>
                          <th scope="col"></th>
                          <th scope="col">NB Count</th>
                          <th scope="col">NB Total Sales</th>
                          <th scope="col">R Count</th>
                          <th scope="col">R Total Sales</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($employePerformance as $data)
                        <tr>
                          <td>{{$data->name}}</td>
                          <td>{{$data->total_orders ? $data->total_orders : 0}}</td>
                          <td>£{{$data->total_sales ? $data->total_sales : 0 }}</td>
                          <td>{{$data->retention_total_orders ? $data->retention_total_orders : 0}}</td>
                          <td>£{{$data->retention_total_sales ? $data->retention_total_sales  : 0}}</td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                  </div>
                </div>
              </div>
            </div>
          <!-- start pie chart and table -->
          <!-- end -->
      </div>
    </div>

@extends('staff.dashboard.partials.transactions_popup')
@extends('staff.dashboard.partials.latest_customers_popup')

<script type="text/javascript">

  {{--google.charts.load('current', {packages: ['corechart', 'line']});--}}
  {{--google.charts.setOnLoadCallback(drawBasic);--}}

  {{--function drawBasic() {--}}


  {{--      var options = {--}}

  {{--                      hAxis: { textPosition: 'none',baselineColor: '#fff',gridlineColor: '#fff' },--}}
  {{--                      vAxis: { textPosition: 'none',baselineColor: '#fff',gridlineColor: '#fff' },--}}
  {{--                      legend: { position: 'none' },--}}
  {{--                      tooltip: { isHtml: true },--}}
  {{--                      enableInteractivity: true,--}}

  {{--                      colors: ['#000']--}}
  {{--                  };--}}


  {{--      var chart = new google.visualization.LineChart(document.getElementById('chart_div1'));--}}

  {{--      var data = new google.visualization.DataTable();--}}
  {{--      data.addColumn('number', 'X');--}}
  {{--      data.addColumn('number', '');--}}
  {{--      data.addColumn({type: 'string', role: 'tooltip'});--}}
  {{--      data.addRows({!! json_encode($total_orders_graph) !!});--}}
  {{--      chart.draw(data,options);--}}


  {{--      var chart2 = new google.visualization.LineChart(document.getElementById('chart_div2'));--}}
  {{--      var data = new google.visualization.DataTable();--}}
  {{--      data.addColumn('number', 'X');--}}
  {{--      data.addColumn('number', '');--}}
  {{--      data.addColumn({type: 'string', role: 'tooltip'});--}}
  {{--      data.addRows({!! json_encode($total_avg_amount_graph) !!});--}}
  {{--      chart2.draw(data,options);--}}

  {{--      var chart3 = new google.visualization.LineChart(document.getElementById('chart_div3'));--}}
  {{--      var data = new google.visualization.DataTable();--}}
  {{--      data.addColumn('number', 'X');--}}
  {{--      data.addColumn('number', '');--}}
  {{--      data.addColumn({type: 'string', role: 'tooltip'});--}}
  {{--      data.addRows({!! json_encode($total_sales_graph) !!});--}}
  {{--      chart3.draw(data,options);--}}
  {{--    }--}}



 // for pie chart
   google.charts.load('current', {'packages':['corechart']});
   google.charts.setOnLoadCallback(drawChart);

   function drawChart() {

     var data = google.visualization.arrayToDataTable({!! json_encode($customers_by_status_graph) !!});

     var options = {
       //title: 'My Daily Activities',
       legend:{
                 position: 'none',

               },
               pieSliceText: "none",
               'tooltip' : {
                 trigger: 'none'
               },
               enableInteractivity: false,
               colors: ['#4d79f6','#f3c74d' , '#dc3545']
     };

     var chart = new google.visualization.PieChart(document.getElementById('piechart'));

     chart.draw(data, options);
   }


//for line charts on bussiness
google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawAnnotations);

function drawAnnotations() {
      //   var data = google.visualization.arrayToDataTable([
      //    ['Month', 'Sales'],
      //    ['Copper', 8.94],            // RGB value
      //    ['Silver', 10.49],            // English color name
      //    ['Gold', 19.30],
      //    ['Platinum', 21.45], // CSS-style declaration
      // ]);

      var options = {
        title: '',
        annotations: {
          alwaysOutside: false,
          textStyle: {
            fontSize: 14,
            color: '#000',
            auraColor: 'none'
          }
        },
        legend: { position : 'none'},
        hAxis: { title: '' },
        vAxis: { position: 'none' }
      };
      //new business graph
      var data = google.visualization.arrayToDataTable({!! json_encode($total_new_business_graph) !!});


      var chart = new google.visualization.ColumnChart(document.getElementById('newBussinessChart'));
      chart.draw(data, options);


    //retention graph
      var data = google.visualization.arrayToDataTable({!! json_encode($this_year_retention_graph) !!});


      var chart = new google.visualization.ColumnChart(document.getElementById('retentionChart'));
      chart.draw(data, options);
    }

   function showModal(id) {
     $(id).modal('show');
   }
 </script>




@endsection

@push('staff_js')

@endpush

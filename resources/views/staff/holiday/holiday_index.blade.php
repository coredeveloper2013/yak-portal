@extends('_v2.layout.dashboard')

@section('title', $page_title)
@section('top-bar-title', $page_title)

@section('styles')
@endsection
<style>
    /* @media screen and (max-width: 796px) {

         .col-12.col-lg-6.p8 {
             padding-bottom: 19px;
         }
     }*/


</style>
@section('content')
    <div id="holiday_2">
        <div class="row justify-content-between page-header">
            <div class="col-md-6">
                <h1 class="main-title">Holiday & Time Off</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header bg-white border-0">
                        <div class="row">
                            <div class="col-md-5">
                                <h5>Ben Bozzoni</h5>
                            </div>
                            <div class="col-md-7">
                                <form class="form-inline holiday-form">
                                    <div class="form-group">
                                        <label for="inputPassword6">Total Leaves</label>
                                        <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline">
                                        <!-- Button trigger modal -->
                                       <button class="btn btn-primary" type="button" data-toggle="modal" data-target=".bd-example-modal-lg">
                                           <i class="fas fa-plus-circle"></i>
                                       </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-header bg-primary text-white font-weight-bold">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Add</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form>
                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-4 col-form-label">Number of Days</label>
                                            <div class="col-sm-8">
                                                <input type="number" name="number_of_days" class="form-control" id="number_of_days" value="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-4 col-form-label">Date of Holiday</label>
                                            <div class="col-sm-8">
                                                <div class="input-group date">
                                                    <input type="text" name="date_of_holiday" class="form-control" id="datepicker" value="{{ date('Y-m-d') }}">
                                                    <div class="input-group-addon">
                                                        <span class="glyphicon glyphicon-th"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-4 col-form-label">Description</label>
                                            <div class="col-sm-8">
                                                <textarea type="text" name="description" class="form-control text-muted" value="" placeholder="Description"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-4 col-form-label">Reason</label>
                                            <div class="col-sm-8">
                                                <input type="text" name="reason" class="form-control" value="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-4 col-form-label">Availed</label>
                                            <div class="col-sm-8">
                                                <select class="form-control" name="availed" id="exampleInputEmail1">
                                                    <option value="">---</option>
                                                    <option value="1">Yes</option>
                                                    <option value="0">No</option>
                                                    {{--@foreach()
                                                        <option value=""></option>
                                                    @endforeach--}}
                                                </select>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal End -->




                    <div class="card-body">
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th>Day</th>
                                <th>Notes</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>23rd May 2020</td>
                                <td>Camping Holiday</td>
{{--                                <td>{{$holidays->date_of_holiday}}</td>--}}
{{--                                <td>{{$holidays->reason}}</td>--}}
                                <td><button class="btn btn-danger btn-sm float-right"> <i data-feather="trash" class=""></i></button></td>
                            </tr>
                            <tr>
                                <td>28th June 2020</td>
                                <td>Running Holiday</td>
                                <td><button class="btn btn-danger btn-sm float-right"> <i data-feather="trash" class=""></i></button></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

            <div class="col-md-6">
                <div class="card">
                    <div class="card-header bg-white border-0">
                        <div class="row">
                            <div class="col-md-5">
                                <h5>Ben Bozzoni</h5>
                            </div>
                            <div class="col-md-7">
                                <form class="form-inline holiday-form">
                                    <div class="form-group">
                                        <label for="inputPassword6">Total Leaves</label>
                                        <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline">
                                        <button class="btn btn-primary"><i class="fas fa-plus-circle"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th>Day</th>
                                <th>Notes</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>23rd May 2020</td>
                                <td>Camping Holiday</td>
                                <td><button class="btn btn-danger btn-sm float-right"> <i data-feather="trash" class=""></i></button></td>
                            </tr>
                            <tr>
                                <td>28th June 2020</td>
                                <td>Running Holiday</td>
                                <td><button class="btn btn-danger btn-sm float-right"> <i data-feather="trash" class=""></i></button></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

            <div class="col-md-6">
                <div class="card">
                    <div class="card-header bg-white border-0">
                        <div class="row">
                            <div class="col-md-5">
                                <h5>Ben Bozzoni</h5>
                            </div>
                            <div class="col-md-7">
                                <form class="form-inline holiday-form">
                                    <div class="form-group">
                                        <label for="inputPassword6">Total Leaves</label>
                                        <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline">
                                        <button class="btn btn-primary"><i class="fas fa-plus-circle"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th>Day</th>
                                <th>Notes</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>23rd May 2020</td>
                                <td>Camping Holiday</td>
                                <td><button class="btn btn-danger btn-sm float-right"> <i data-feather="trash" class=""></i></button></td>
                            </tr>
                            <tr>
                                <td>28th June 2020</td>
                                <td>Running Holiday</td>
                                <td><button class="btn btn-danger btn-sm float-right"> <i data-feather="trash" class=""></i></button></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

            <div class="col-md-6">
                <div class="card">
                    <div class="card-header bg-white border-0">
                        <div class="row">
                            <div class="col-md-5">
                                <h5>Ben Bozzoni</h5>
                            </div>
                            <div class="col-md-7">
                                <form class="form-inline holiday-form">
                                    <div class="form-group">
                                        <label for="inputPassword6">Total Leaves</label>
                                        <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline">
                                        <button class="btn btn-primary"><i class="fas fa-plus-circle"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th>Day</th>
                                <th>Notes</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>23rd May 2020</td>
                                <td>Camping Holiday</td>
                                <td><button class="btn btn-danger btn-sm float-right"> <i data-feather="trash" class=""></i></button></td>
                            </tr>
                            <tr>
                                <td>28th June 2020</td>
                                <td>Running Holiday</td>
                                <td><button class="btn btn-danger btn-sm float-right"> <i data-feather="trash" class=""></i></button></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

            <div class="col-md-6">
                <div class="card">
                    <div class="card-header bg-white border-0">
                        <div class="row">
                            <div class="col-md-5">
                                <h5>Ben Bozzoni</h5>
                            </div>
                            <div class="col-md-7">
                                <form class="form-inline holiday-form">
                                    <div class="form-group">
                                        <label for="inputPassword6">Total Leaves</label>
                                        <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline">
                                        <button class="btn btn-primary"><i class="fas fa-plus-circle"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th>Day</th>
                                <th>Notes</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>23rd May 2020</td>
                                <td>Camping Holiday</td>
                                <td><button class="btn btn-danger btn-sm float-right"> <i data-feather="trash" class=""></i></button></td>
                            </tr>
                            <tr>
                                <td>28th June 2020</td>
                                <td>Running Holiday</td>
                                <td><button class="btn btn-danger btn-sm float-right"> <i data-feather="trash" class=""></i></button></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

            <div class="col-md-6">
                <div class="card">
                    <div class="card-header bg-white border-0">
                        <div class="row">
                            <div class="col-md-5">
                                <h5>Ben Bozzoni</h5>
                            </div>
                            <div class="col-md-7">
                                <form class="form-inline holiday-form">
                                    <div class="form-group">
                                        <label for="inputPassword6">Total Leaves</label>
                                        <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline">
                                        <button class="btn btn-primary"><i class="fas fa-plus-circle"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th>Day</th>
                                <th>Notes</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>23rd May 2020</td>
                                <td>Camping Holiday</td>
                                <td><button class="btn btn-danger btn-sm float-right"> <i data-feather="trash" class=""></i></button></td>
                            </tr>
                            <tr>
                                <td>28th June 2020</td>
                                <td>Running Holiday</td>
                                <td><button class="btn btn-danger btn-sm float-right"> <i data-feather="trash" class=""></i></button></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

        </div>
    </div>


@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('sbVendor/bootstrap-datepicker-1.9.0-dist/js/bootstrap-datepicker.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('sbVendor/bootstrap-datepicker-1.9.0-dist/css/bootstrap-datepicker.min.css') }}">
    <script>
        $("#datepicker").datepicker({
            format: 'yyyy-mm-dd'
        }).on('changeDate', function(ev){
            $('#datepicker').datepicker('hide');
        });
    </script>

@endsection


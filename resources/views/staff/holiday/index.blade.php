@extends('staff.layouts.app')

@section('staff_css')
<link href='https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>
<style>
   .paginate_button{
      padding:0px !important;
   }
   .footer {
    bottom: 0;
   }
</style>
@endsection

@section('staff_content')
    <!-- end left-sidenav-->         <!-- Page Content-->
         <div class="page-content grey-bg">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-12">
                     <div class="page-title-box page-title-box-mobile">
                      <h4 class="page-title float-left holiday-h-resp">Holidays & Time Off</h4>
                        <div class="float-right">
                           <div class="form-group float-left w-55 mr-2 holiday-form-resp">
                              <select class="form-control" id="userId" name="userid">
                                 <option>Select Staff Member</option>
                                 @foreach($users as $user)
                                 <option value="{{$user->id}}">{{$user->name}}</option>
                                 @endforeach
                              </select>
                           </div>
                        </div>
                        @can('isAdmin')
                        <div class="float-right form-inline mr-3">
                          <div class="form-group">
                             <p class="mb-0 mr-2">Total leaves</p>
                             <input disabled="" type="text" name="no_of_leave" class="form-control mr-3" value="" id="noOfLeave">
                             <a title="Edit Leaves" href="javascript:void(0)"  data-toggle="modal" data-target="#editLeaveModel" id="" class="btn btn-btn btn-primary btn-sm p-1 px-2 mt-0 btn-sm"><i class="fa fa-edit"></i></a>
                          </div>
                        </div>
                        @endcan
                        
                     </div>
                     <!--end page-title-box-->
                  </div>
                  <!--end col-->
               </div>
               <div class="row">
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                     <div class="card">
                        <div class="">
                           <div class="head-t card-body border-bottom">
                              <div class="float-left">
                                 <h4 class="page-title text-uppercase" id="userName"></h4>
                              </div>
                              <div class="float-right">
                                 <div class="form-group float-left w-55 mr-2">
                                    <p class="holiday-p-top mb-0 mt-2" id="daysleft">29 DAYS LEFT</p>
                                    <!-- <input type="email" disabled="" class="form-control center" id="daysleft" aria-describedby="emailHelp" placeholder="" value=""> -->
                                 </div>
                                 <button type="button" id="addNew" class="btn btn-primary"><i class="fas fa-plus"></i> Add New</button>
                              </div>
                           </div>
                           <div class="w-100 col-xl-8 col-lg-8 col-md-8 col-sm-8">
                            <div class="card-body card-b-resp">
                              <div class="table-responsive m-0 tble-bold-td">
                                 <table class="table text-center holi-table table-bordered t-border-color  td-block ">                                   
                                
                                 </table>
                              </div>
                            </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- container -->
                 
               </div>
               <!-- end page content -->
            </div>
         </div>
         <!-- end page content -->
 <footer class="footer text-center text-sm-left">
   &copy; 2020 Yak-Portal <span class="text-muted d-none d-sm-inline-block float-right"></span>
</footer>
<!--end footer-->
   <!-- Modal -->
   <div class="modal fade" id="addNewModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h5>Add Holiday</h5>
               <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <form id="addHoliday" >
               <div class="modal-body">
                  <!-- <div class="form-group">
                     <label for="number_of_days">Days:</label>
                     <select class="form-control" name="days" required="">
                        <option disabled="" selected="">Select </option>
                          <option value="1">Day 1</option>
                            <option value="2">Day 2</option> 
                            <option value="3">Day 3</option> 
                  </select>
                  </div> -->
                  <div class="form-group">
                     <label for="date">Date:</label>
                     <input type="text" class="form-control"   name="date">
                  </div>
                  <div class="form-group">
                     <label for="reason">Reason:</label>
                     <input type="text" class="form-control"  max="15" name="reason">
                  </div>
                    <div class="form-group">
                     <label for="reason">Description:</label>
                     <input type="text" class="form-control"   name="description">
                  </div>
                  @if(Auth::user()->type=="admin")
                  <div class="form-group">
                     <label for="userId">User:</label>
                     <select class="form-control" name="user" required="">
                        <option disabled="" selected="">Select </option>
                          @foreach($users as $user)
                          <option value="{{$user->id}}">{{$user->name}}</option>
                          @endforeach
                                   
                  </select>
                  </div>
                 @else
                      <input type="hidden" class="form-control"  value="{{Auth::user()->id}}" name="user">
                @endif
                  
               </div>
              <div class="modal-footer">
                 <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                 <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
              </div>
            </form>
         </div>
      </div>
   </div>
 <div class="modal fade" id="addNewModel1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h5>Edit Holiday</h5>
               <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <form id="editHoliday">
                 <div class="modal-body">
                 <!--  <div class="form-group">
                     <label for="number_of_days">Days:</label>
                     <select class="form-control" name="number_of_days" id="number_of_days">
                        <option disabled="" selected="">Select </option>
                          <option value="1">Day 1</option>
                            <option value="2">Day 2</option> 
                            <option value="3">Day 3</option> 
                  </select>
                  </div> -->
                  <div class="form-group">
                     <label for="date">Date:</label>
                     <input type="date" class="form-control" id="date"  name="date">
                  </div>
                  <div class="form-group">
                     <label for="reason">Reason:</label>
                     <input type="text" class="form-control" id="reason"  name="reason">
                  </div>
                   <div class="form-group">
                     <label for="reason">Description:</label>
                     <input type="text" class="form-control" id="description"  name="description">
                  </div>
                   
                  
                  <!-- <div class="form-group">
                     <label for="userId">User:</label>
                     <select class="form-control" name="userId" id="userid" required="">
                        <option disabled="" selected="">Select </option>
                          @foreach($users as $user)
                          <option value="{{$user->id}}">{{$user->name}}</option>
                          @endforeach
                                   
                  </select>
                  </div> -->
                 
                  
               </div>
              <div class="modal-footer">
                 <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                 <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
              </div>
            </form>
         </div>
      </div>
   </div>
<!-- edit leave modal -->
  <div class="modal fade" id="editLeaveModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
       aria-hidden="true">
       <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
             <div class="modal-header">
                <h5>Edit Leaves</h5>
                <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
                </button>
             </div>
             <form id="updateLeave">
                  <div class="modal-body">
                  
                   <div class="form-group">
                      <label for="reason">No. of leaves</label>
                      <input type="number" min="0" class="form-control" id="no_of_leave"  name="no_of_leave">
                      <input type="hidden" class="form-control" name="id">
                   </div>
                   
                  
                  
                   
                </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
               </div>
             </form>
          </div>
       </div>
    </div>
<!-- end -->
@endsection

@push('staff_js')
<script>
   var appURL = '{!! url('/staff/') !!}';
   var apiURL = '{!! url('/api/staff/') !!}';
 var USERID ='{!! Auth::user()->id !!}';

</script>
{{--  Inject vue js files  --}}
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('js/cmn/cmn.js') }}"></script>
<script src="{{ asset('staff/js/holiday.js') }}"></script>
@endpush
@extends('staff.layouts.app')
@section('staff_css')
<link href='https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>
<style>
   .paginate_button{
   padding:0px !important;
   }
</style>
@endsection
@section('staff_content')
<div class="page-content grey-bg support-bg-1">
   <div class="container-fluid">
      <div class="row">
         <div class="col-sm-12">
            <div class="page-title-box support-resp-h">
               <h4 class="page-title">Support ticket</h4>
            </div>
            <!--end page-title-box-->
         </div>
      </div>
      <div class="row">
         <div class="col-xl-10 col-lg-9 col-md-8 col-sm-8">
            <div class="btns-support-l">
               <a href="javascript:;" class="btn btn-camp unassigned filter active" data-status="unassigned"> <i class="ti-ticket"></i> Unassigned Tickets</a>
               <a href="javascript:;" class="btn btn-camp my-tickets filter" data-status="my-tickets"> <i class="ti-ticket"></i> My Tickets</a>
               <a href="javascript:;" class="btn btn-camp completed filter" data-status="completed"><i class="ti-check"></i> Completed Tickets</a>
               <a href="javascript:;" class="btn btn-camp trash-ticket"> <i class="ti-trash"></i> Trash</a>
            </div>
         </div>
         {{--<div class="col-xl-2 col-lg-3 col-md-4 col-sm-4">
            <div class="btns-support-r">
               <button type="button" class="btn btn-primary btn-block">
                 <i class="fas fa-tick"></i> Compose
               </button>
            </div>
         </div>--}}
      </div>
      <div class="row">
         <div class="col-xl-3 col-lg-4 col-md-4 col-sm-12">
            <div class="side-inbox">
               <div class="head-inbox">
                  <i class="fas fa-bars"></i> INBOX
                  {{--<span class="msg-count">1-10 of 45</span>--}}
               </div>
               <div class="inbox-msg">
                  <ul class="tickets-body">
                      <span class="text-center d-block">No Ticket found.</span>
                  </ul>
               </div>
            </div>
         </div>
         <div class="col-xl-9 col-lg-8 col-md-8 col-sm-12">
            <div class="add-r assign_block top-bar-btn01 bg-style-support">
              <div class="btn-group float-right">
                <select class="custom-select" name="staff_members" id="staff_members">
                    <option value="">Select Staff Member</option>
                    @foreach ($staff_members as $staff)
                        <option value="{{$staff->id}}">{{$staff->name}}</option>
                    @endforeach
                </select>
               <a title="Mark as completed" href="javascript:;" class="active-btn btn btn-reply complete-ticket"><i class="fa fa-check"></i></a>
               {{--<a href="javascript:;" class="btn btn-reply assign-me"><i class="fas fa-user"></i></a>--}}
             </div>
            </div>
            <div class="card">
               <div class="card-body">
                  <div class="inbox-details">
                     <ul class="comments-body">
                         <span class="text-center d-block">No Chat found.</span>
                     </ul>
                  </div>
                  <div class="add-msg">
                     <div class="form-group">
                        <textarea class="form-control" placeholder="Type your message here" id="comment_text"></textarea>
                     </div>
                     <div class="form-group">
                        <input type="button" name="" id="send_comment" class="btn btn-darkblue" value="Send">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="assign_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog" role="document">
       <div class="modal-content">
           <div class="modal-header">
               <h4>Staff Members List</h4>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: red">
                   <span aria-hidden="true">&times;</span>
               </button>
           </div>
           <div class="modal-body">
                <div class="form-row">
                    <div class="form-group col-md-6 col-sm-6">
                       <label for="usr">Staff Members:</label>
                       <select class="custom-select" name="staff_member" id="staff_member">
                           <option value="">Select Staff Member</option>
                           @foreach ($staff_members as $staff)
                               <option value="{{$staff->id}}">{{$staff->name}}</option>
                           @endforeach
                       </select>
                    </div>

                    <div class="form-group col-md-12">
                       <button type="botton" class="btn btn-primary assign_others">Submit</button>
                    </div>
                </div>
           </div>
       </div>
   </div>
</div>
@endsection
@push('staff_js')

   <script type="text/javascript">
      var App_url = '{!! url()->current() !!}';
      var appUrl = '{!! url('') !!}';
      var apiURL = '{!! url('/api/') !!}';
      var user_id = '{!! Auth::user()->id !!}';
      var atcive_ticket = 0;

      $(function() {
         loadTickets(0, 'unassigned');

         $('body').on('click', '.filter', function(event) {
            event.preventDefault();
            var that = $(this);
            var status = that.data('status');
            if (status == 'completed') {
              $('.complete-ticket').addClass('d-none');
            } else {
              $('.complete-ticket').removeClass('d-none');
            }
            loadTickets(0, status);
         });

         $('body').on('click', '.load_comments', function(event) {
            event.preventDefault();
            var that = $(this);
            var ticketId = that.data('id');
            atcive_ticket = ticketId;
            $('.load_comments').removeClass('active');
            that.addClass('active');
            loadComments(ticketId);
         });

        $('body').on('change', '#staff_members', function(event) {
            event.preventDefault();
            var that = $(this);
            if (that.val() == '') {
              return false;
            }
            var staff_id = that.val();
            var ticketId = atcive_ticket;

            var csrf = $('meta[name="csrf-token"]').attr("content");
            $.ajax({
               url: apiURL+'/staff/support-ticket/assign',
               type: 'POST',
               dataType: 'json',
               data: {
                  id: ticketId,
                  staff_id: staff_id
               },
               headers: {
                  "X-CSRF-TOKEN": csrf,
                  Accpet: "applicationjson"
               },
            }).always(function(res) {
                let status = res.status.substr(0,1).toUpperCase()+res.status.substr(1);
                  Swal.fire({
                      title: status,
                      text: res.msg,
                      icon: res.status,
                  });

                  if (res.status == 'success' ) {
                    $('.ticket_'+ticketId).data('un_assigned', 'N');
                    $('.ticket_'+ticketId).data('assigned', staff_id);
                    loadTickets(0, 'unassigned');
                    //$('.assign_block').addClass('d-none');
                  }
            });
        });

        /*$('body').on('click', '.assign_others, .assign-me', function(event) {
            event.preventDefault();
            var that = $(this);
            if (that.hasClass('assign_others')) {
               var staff_id = $('#staff_member').val();
            } else {
               var staff_id = user_id;
            }
            var ticketId = atcive_ticket;
            var csrf = $('meta[name="csrf-token"]').attr("content");
            $.ajax({
               url: apiURL+'/staff/support-ticket/assign',
               type: 'POST',
               dataType: 'json',
               data: {
                  id: ticketId,
                  staff_id: staff_id
               },
               headers: {
                  "X-CSRF-TOKEN": csrf,
                  Accpet: "applicationjson"
               },
            }).always(function(res) {
                  Swal.fire({
                      title: res.status,
                      text: res.msg,
                      icon: res.status,
                  });

                  if (res.status == 'success' ) {
                     $('.ticket_'+ticketId).data('un_assigned', 'N');
                     $('.assign_block').addClass('d-none');
                  }
            });
        });*/

         var flag = false;
         $('body').on('click', '#send_comment', function(event) {
            event.preventDefault();
            var that = $(this);
            var text = $('#comment_text').val();
            if (text == '' || flag == true) {
               return  false;
            }

            flag = true;

            var ticketId = atcive_ticket;
            var csrf = $('meta[name="csrf-token"]').attr("content");
            $.ajax({
               url: apiURL+'/staff/support-ticket/add/comment',
               type: 'POST',
               dataType: 'json',
               data: {
                  id: ticketId,
                  text: text
               },
               headers: {
                  "X-CSRF-TOKEN": csrf,
                  Accpet: "applicationjson"
               },
            }).always(function(res) {
                  flag = false;
                let status = res.status.substr(0,1).toUpperCase()+res.status.substr(1);
                  Swal.fire({
                      title: status,
                      text: res.msg,
                      icon: res.status,
                  });

                  if (res.status == 'Success' ) {
                     $('#comment_text').val('');
                     var html = makeComments(res.data);
                     $('.comments-body').append(html);
                  }
            });
         });

         var flag1 = false;
         $('body').on('click', '.trash-ticket', function(event) {
            event.preventDefault();
            var that = $(this);
            if (flag1 == true) {
               return  false;
            }

            flag1 = true;

            var ticketId = atcive_ticket;
            var csrf = $('meta[name="csrf-token"]').attr("content");
            $.ajax({
               url: apiURL+'/staff/support-ticket/delete/ticket',
               type: 'POST',
               dataType: 'json',
               data: {
                  id: ticketId
               },
               headers: {
                  "X-CSRF-TOKEN": csrf,
                  Accpet: "applicationjson"
               },
            }).always(function(res) {
                  flag1 = false;
                let status = res.status.substr(0,1).toUpperCase()+res.status.substr(1);
                  Swal.fire({
                      title: status,
                      text: res.msg,
                      icon: res.status,
                      onAfterClose: () => {
                           if (res.status == 'success' ) {
                             location.reload();
                           }
                      }
                  });

            });
         });

         var flag2 = false;
         $('body').on('click', '.complete-ticket', function(event) {
            event.preventDefault();
            var that = $(this);
            if (flag2 == true) {
               return  false;
            }

            flag2 = true;

            var ticketId = atcive_ticket;
            var csrf = $('meta[name="csrf-token"]').attr("content");
            $.ajax({
               url: apiURL+'/staff/support-ticket/complete/ticket',
               type: 'POST',
               dataType: 'json',
               data: {
                  id: ticketId
               },
               headers: {
                  "X-CSRF-TOKEN": csrf,
                  Accpet: "applicationjson"
               },
            }).always(function(res) {
                  flag2 = false;
                let status = res.status.substr(0,1).toUpperCase()+res.status.substr(1);
                  Swal.fire({
                      title: status,
                      text: res.msg,
                      icon: res.status,
                      onAfterClose: () => {
                           if (res.status == 'success' ) {
                             location.reload();
                           }
                      }
                  });

            });
         });
      });

      function loadTickets(offset, status) {
         if (typeof offset == 'undefined') {
            offset = 0;
         }

         if (typeof status == 'undefined') {
            status = '';
         }

         if (status != '') {
            $('.filter').removeClass('active');
            $('.'+status).addClass('active');
         }

         var csrf = $('meta[name="csrf-token"]').attr("content");
         $.ajax({
              url: apiURL+'/staff/support-ticket',
              type: 'POST',
              dataType: 'json',
              data: {
               offset: offset,
               status: status
            },
              headers: {
                  "X-CSRF-TOKEN": csrf,
                  Accpet: "applicationjson"
              },
         }).always(function(res) {
               if (res.status == 'success' ) {
                  if (res.data.show_comments == 'Y') {
                     var html = ``;
                     $.each(res.data.tickets, function( index, value ) {
                        html += makeTickets(value);
                     });
                     $('.tickets-body').html(html);

                     setTimeout(function() {
                        var fisrtTicketBlock = $('.tickets-body').find('li:nth-child(1) a');
                        fisrtTicketBlock.trigger('click');
                     }, 500)
                  } else {
                     $('.comments-body').html('<span class="text-center d-block">No Chat found.</span>');
                     $('.tickets-body').html('<span class="text-center d-block">No Ticket found.</span>');
                  }
               } else {
                  $('.tickets-body').html('<span class="text-center d-block">No Ticket found.</span>');
               }
          });
      }

      function makeTickets(tickets) {
         var html =  `<li>
                        <a href="javascript:;" class="load_comments ticket_`+tickets.id+`"  data-id="`+tickets.id+`" data-assigned="`+tickets.staff_id+`" data-un_assigned="`+tickets.unassigned+`">
                           <div class="name-time1-o">`+tickets.customer_data.name+`  <span class="time1-o">`+tickets.human_date+`</span></div>
                           <div class="sub-msg"></div>
                           <p>`+tickets.subject+`</p>
                        </a>
                     </li>`;
         return html;
      }

      function loadComments(tId) {
         var assigned = $('.ticket_'+tId).data('assigned');
         var un_assigned = $('.ticket_'+tId).data('un_assigned');

         if (un_assigned == 'Y') {
            $('.assign_block #staff_members').attr('disabled', false);
            $('.assign_block #staff_members').val('');
            //$('.assign_block').removeClass('d-none');
         } else {
            $('.assign_block #staff_members').attr('disabled', true);
            $('.assign_block #staff_members').val(assigned);
            //$('.assign_block').addClass('d-none');
         }

         var csrf = $('meta[name="csrf-token"]').attr("content");
         $.ajax({
              url: apiURL+'/staff/support-ticket/comments',
              type: 'POST',
              dataType: 'json',
              data: {id: tId},
              headers: {
                  "X-CSRF-TOKEN": csrf,
                  Accpet: "applicationjson"
              },
          }).always(function(res) {
               if (res.status == 'success' ) {
                  if (res.data.show_comments == 'Y') {
                     var html = ``;
                     $.each(res.data.comments, function( index, value ) {
                        html += makeComments(value);
                     });
                     $('.comments-body').html(html);
                  } else {
                     $('.comments-body').html('<span class="text-center d-block">No Chat found.</span>');
                  }
               } else {
                  $('.comments-body').html('<span class="text-center d-block">No Chat found.</span>');
               }
          });
      }

      function makeComments(comment) {
         var html =  `<li>
                        <div class="media">
                           <div class="media-left">`;
                              if (comment.user_image != '' && comment.user_image != null) {
                                 html += `<img src="{{asset('storage/avatars/')}}/`+comment.user_image+`" alt="user" class="rounded-circle thumb-md">`
                              } else {
                                 html += `<img src="{{asset('staff/images/user-1.jpg')}}" alt="user" class="rounded-circle thumb-md">`
                              }
                              html += `<span class="round-10 bg-success"></span>
                           </div>
                           <div class="media-body">
                              <div>`;
                                 if (comment.user_id == user_id) {
                                    html += `<h6>You</h6>`;
                                 } else {
                                    html += `<h6>`+comment.user_name+`</h6>`;
                                 }
                                 html += `<div class="dropdown">
                                    <a class="dropdown-toggle" type="button">
                                       `+comment.human_date+`
                                    </a>
                                 </div>
                              </div>
                        </div>
                     </div>
                     <p>
                        `+comment.chat_text+`
                     </p>
                  </li>`;
         return html;
      }
   </script>

@endpush

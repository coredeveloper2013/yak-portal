@extends('staff.layouts.app')

@section('staff_content')
    <div class="page-content grey-bg">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <!-- <div class="float-right">
                           <ol class="breadcrumb">
                              <button type="button" class="btn btn-primary text-uppercase">
                                 <i class="fas fa-file-pdf"></i>&nbsp;&nbsp; Generate PDF Report
                              </button>
                           </ol>
                        </div> -->
                        <h4 class="page-title">Campaigns</h4>
                    </div>
                    <!--end page-title-box-->
                </div>
            </div>
            <!--end col-->
            <div class="row top-search01">
            </div>
            <div class="row">
                <div class="col-xl-12" style="display: inline-block;">
                    <form id="addCampaign">
                        <div class="row">
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="name">Campaign Name:</label>
                                    <input type="text" class="form-control" id="name" name="name">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="date">Date:</label>
                                    <input type="text" class="form-control" name="date" id="date" value=""
                                           placeholder="Select Date">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="landing_id">Landing page:</label>
                                    <select class="form-control" name="landing_id">
                                        <option disabled="" selected="">Select landing page</option>
                                        @foreach($landing_pages as $page)
                                            <option value="{{$page->id}}">{{$page->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="amount">Amount:</label>
                                    <input type="text" class="form-control" id="amount" name="amount">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="compaign_amount">Campaign Amount:</label>
                                    <input type="text" class="form-control" id="compaign_amount" name="compaign_amount">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="max_add_amount">Max Add Amount:</label>
                                    <input type="text" class="form-control" id="max_add_amount" name="max_add_amount">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="type">Campaign Type</label>
                                    <select class="form form-control form-control-sm" name="type">
                                        <option value="one_off">One Off</option>
                                        <option value="monthly">Monthly</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <select class="form form-control form-control-sm" name="status">
                                        <option value="completed">Completed</option>
                                        <option value="pending">Pending</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" value='yes' name="assigned_opt"
                                               class="custom-control-input" id="switch1">
                                        <label class="custom-control-label" for="switch1">Assign to other user</label>
                                    </div>
                                </div>

                                <div class="form-group assigned-users">
                                    <label for="assigned_id">Assigned User:</label>
                                    <select class="form-control" name="assigned_id">
                                        @foreach($users as $user)
                                            <option value="{{$user->id}}">{{$user->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary float-right" ="Create a campaign">
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            <!-- end page content -->
        </div>
    </div>
@endsection

@push('staff_js')
    <script>
        var appURL = '{!! url('/staff/') !!}';
        var apiURL = '{!! url('/api/staff/') !!}';
    </script>
    {{--  Inject vue js files  --}}
    <script src="{{ asset('js/cmn/cmn.js') }}"></script>
    <script src="{{ asset('staff/js/campaigns.js') }}"></script>
@endpush

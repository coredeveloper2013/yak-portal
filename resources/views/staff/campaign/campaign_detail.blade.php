@extends('staff.layouts.app')
@section('staff_css')
<style>
    .invalid-feedback {
    display: block !important;
    }
    .chat-body {
    padding: 12px 0 10px 0 !important;
    }
    html {
        background: #f8f8f8;
    }
</style>
@endsection
@section('staff_content')
<div class="page-content grey-bg">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title float-left">{{$campaign->name}}</h4>
                    <a href="{{ route('staff.tasks.create') }}?type=campaign&id={{ $campaign->id }}" class="blue-badge text-center float-left mb-3 float-right">Add Tasks</a>
                </div>
                <!--end page-title-box-->
            </div>
            <!--end col-->
        </div>
        <div class="row">
            <div class="col-xl-4 col-lg-5 col-md-5">
            <div class="detail-scroll">
                <div class="row">
                    <div class="col-12">
                        <div class="blue-badge text-center">
                            <div>Status</div>
                            <h3 class="capital-text">{{$campaign->status}} Campaign</h3>
                        </div>
                    </div>
                    <div class="col-6">
                        <a href="javascript:;" class="blue-plane text-center"><span> Order: #{{$campaign->id}}</span></a>
                    </div>
                    <div class="col-6">
                        <a href="javascript:;" class="blue-plane text-center">{{ $is_retention > 0 ? 'Retention' : 'New Business' }}</a>
                    </div>
                    <div class="col-12">
                        <div class="purple-badge text-center">
                            <h3 class="">{{($campaign->user->company) ? $campaign->user->company->name : 'N/A'}}</h3>
                            <a href="javascript:;" class="btn btn-light-o"  data-toggle="modal" data-target="#contact_detail">View Contact Details</a>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="blue-badge-p">
                            <div>Contract Value</div>
                            <h3>£ {{ str_replace('.00', '', $campaign->amount)}}</h3>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="blue-badge-p">
                            <div>Budget</div>
                            <h3>£ {{ str_replace('.00', '', $campaign->compaign_amount) }}</h3>
                        </div>
                    </div>
                    <div class="col-6">
                        <a href="javascript:;" class="{{!empty($campaign->agreement->status) && strtolower($campaign->agreement->status) == 'completed' ? 'green-plane' : 'red-plane' }} text-center">Contract</a>
                    </div>
                    <div class="col-6">
                        <a href="javascript:;" class="{{!empty($campaign->invoice->status) && strtolower($campaign->invoice->status) == 'completed' ? 'green-plane' : 'red-plane' }} text-center">Payment</a>
                    </div>
                    {{--<div class="col-12">
                        <a href="javascript:;" class="red-plane text-center">Order Completed</a>
                    </div>--}}
                </div>
                {{-- <div class="row">
                    <div class="card asign-detail pb-0">
                        <div class="card-header">
                            Landing Url: <a class="ellipsis" href="{{$campaign->landing_pages->url}}"> {{$campaign->landing_pages->url}}</a>
                        </div>
                    </div>
                </div> --}}
                <div class="row">
                    <div class="card asign-detail">
                        <div class="card-header">
                            Assign Details
                        </div>
                        <ul class="list-group">
                            @if($campaign->campaignSpecialist)
                            <li class="list-group-item">
                                <img @if(!is_null($campaign->campaignSpecialist->image)) src="{{asset('/storage/avatars/'.$campaign->campaignSpecialist->image)}}"
                                @else src="{{asset('/images/user.png')}}"
                                @endif
                                class="mr-3">
                                <span class="title-list">{{$campaign->campaignSpecialist->name}}</span>
                                <span class="badge badge-pill badge-grey float-right">Campaign Specialist</span>
                            </li>
                            @endif
                            @if($campaign->lpSpecialist)
                            <li class="list-group-item">
                                <img @if(!is_null($campaign->lpSpecialist->image)) src="{{asset('/storage/avatars/'.$campaign->lpSpecialist->image)}}"
                                @else src="{{asset('/images/user.png')}}"
                                @endif
                                class="mr-3">
                                <span class="title-list">{{$campaign->lpSpecialist->name}}</span>
                                <span class="badge badge-pill badge-grey float-right">LP Specialist</span>
                            </li>
                            @endif
                            @if($campaign->accounManager)
                            <li class="list-group-item">
                                <img @if(!is_null($campaign->accounManager->image)) src="{{asset('/storage/avatars/'.$campaign->accounManager->image)}}"
                                @else src="{{asset('/images/user.png')}}"
                                @endif
                                class="mr-3">
                                <span class="title-list">{{$campaign->accounManager->name}}</span>
                                <span class="badge badge-pill badge-grey float-right">Account Manager</span>
                            </li>
                            @endif
                            @if($campaign->salesPerson)
                            <li class="list-group-item">
                                <img @if(!is_null($campaign->salesPerson->image)) src="{{asset('/storage/avatars/'.$campaign->salesPerson->image)}}"
                                @else src="{{asset('/images/user.png')}}"
                                @endif
                                class="mr-3">
                                <span class="title-list">{{$campaign->salesPerson->name}}</span>
                                <span class="badge badge-pill badge-grey float-right">Sales Person</span>
                            </li>
                            @endif
                        </ul>
                    </div>
                </div>

                @if (!empty($campaign->installments) && count($campaign->installments) > 0 && $campaign->with_installment == 'Y')
                    <div class="row">
                        <div class="card asign-detail">
                            <div class="card-header">
                                Installments
                            </div>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <span class="">Amount</span>
                                    <span class="float-right">Date</span>
                                </li>
                                @foreach ($campaign->installments as $installment)
                                    <li class="list-group-item">
                                        <span class="title-list">£ {{$installment->amount}}</span>
                                        <span class="float-right">{{$installment->installment_date}}</span>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif
			</div>
            </div>
            <div class="col-xl-8 col-lg-7 col-md-7">
                <div class="card d-none">
                    <div class="card-body">
                        <h3 class="card-heading">Description</h3>
                        <p class="decs-grey">
                            {{ $campaign->description }}
                        </p>
                    </div>
                </div>

                <div class="card m-0">
                    <div class="card-body">
                        <h3 class="card-heading">Notes</h3>
                        <div class="notes01">
                            <ul class="note-listing eas">
                                @forelse ($campaign->campaignComments as $comment)
                                    <li class="media comment-blk">
                                        <img src="{{asset('/staff/images/user-1.jpg')}}" alt="img" class="mr-3">
                                        <!-- <img src="http://127.0.0.1:8000/staff/images/user-1.jpg" class="mr-3"> -->
                                        <div class="media-body">
                                            <div class="name-time-o">
                                                {{$comment->user->name}}
                                                <span><i class="mdi mdi-clock-outline"></i> </span>
                                                <span>{{$comment->created_at->format('d.m.Y')}} | </span>
                                                <span>{{$comment->created_at->format('g:i:s A')}}</span>
                                                <a href="javascript:;" class="del-o del-cmnt" data-comment_id="{{$comment->id}}" data-parent_id="{{$campaign->id}}"><i class="far fa-trash-alt"></i></a>
                                            </div>
                                            <p class="msg-o">
                                                {!! $comment->description_br !!}
                                            </p>

                                        </div>
                                    </li>
                                @empty
                                    <span class="text-center d-block">Not Notes Found.</span>
                                @endforelse
                            </ul>
                            <div class="add-notes">
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Type your note here" id="comment_text"></textarea>
                                </div>
                                <div class="form-group m-0">
                                    <input type="button" name="" id="send_comment" class="btn btn-darkblue" value="Add note">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" style="display:none">
                    <div class="card-body">
                        <a href="{{ route('staff.tasks.create') }}?type=campaign&id={{ $campaign->id }}" class="blue-badge text-center float-left m-0">Add Tasks</a>
                        @if ($campaign->status != "Pending")
                          <div class="chat-features float-right">
                             <div class="d-sm-inline-block">
                                <select class="custom-select" name="campaign_status" id="campaign_status">
                                   <option value="">Select Status</option>
                                   <option value="awaiting" disabled {{ 'Awaiting' == $campaign->status ? 'selected' : '' }}>Awaiting</option>
                                   <option value="billing_detail" {{ 'Billing detail' == $campaign->status ? 'selected' : '' }}>Billing Detail</option>
                                   <option value="live" {{ 'Live' == $campaign->status ? 'selected' : '' }}>Live</option>
                                   <option value="followed_up" {{ 'Followed up' == $campaign->status ? 'selected' : '' }}>Followed Up</option>
                                   <option value="completed" {{ 'Completed' == $campaign->status ? 'selected' : '' }}>Completed</option>
                               </select>
                             </div>
                          </div>
                       @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="contact_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
     <div class="modal-content">
        <div class="modal-header">
           <h5>Customer Contact Detail</h5>
           <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
           </button>
        </div>
        <form id="addCampaign">
            <input type="hidden" value="" id="edit_id" name="edit_id">
            <div class="modal-body">
                <div class="form-group">
                    <p><i class="far fa-user float-left"></i>&nbsp; {{ $campaign->user->name }}</p>
                </div>

                @if(!empty($campaign->user->phone))
                    <div class="form-group">
                        <p><i class="fa fa-phone float-left"></i>&nbsp; {{ $campaign->user->phone }}</p>

                    </div>
                @endif

                <div class="form-group">

                    <p><i class="far fa-envelope float-left"></i>&nbsp; {{ $campaign->user->email }}</p>
                </div>

                @if(!empty($campaign->user->business_info))
                    @if(!empty($campaign->user->business_info->email_address))
                        <div class="form-group">
                            <p><i class="far fa-envelope float-left"></i>&nbsp; {{ $campaign->user->business_info->email_address }}</p>

                        </div>
                    @endif

                    @if(!empty($campaign->user->business_info->phone))
                        <div class="form-group">
                            <p><i class="fa fa-phone float-left"></i>&nbsp; {{ $campaign->user->business_info->phone }}</p>

                        </div>
                    @endif

                    @if(!empty($campaign->user->business_info->website))
                        <div class="form-group">
                            <p><i class="fa fa-globe float-left"></i>&nbsp; {{ $campaign->user->business_info->website }}</p>

                        </div>
                    @endif
                @endif
                <div class="form-group">

                    <p><i class="fa fa-check float-left"></i>&nbsp; {{ ucfirst($campaign->user->status) }} Client</p>
                </div>
                @if(!empty($campaign->user->business_info))
                    @if(!empty($campaign->user->business_info->notes))
                <div class="form-group">

                    <p><i class="fa fa-comments float-left"></i>&nbsp; {{ $campaign->user->business_info->notes }} </p>
                </div>
                    @endif
                @endif
           </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
            </div>
        </form>
     </div>
  </div>
</div>

@endsection
@push('staff_js')
<script>
    var appURL = '{!! url('/staff/') !!}';
    var apiURL = '{!! url('/api/staff/') !!}';
    var UnqId = '{{ $campaign->id }}';
    var campaign = '{{ $campaign->id }}';
</script>
<script src="{{ asset('js/cmn/cmn.js') }}"></script>
<script type="text/javascript">
    $(function() {
        var csrf = $('meta[name="csrf-token"]').attr("content");

        var flag2 = false;
        $('body').on('click', '#send_comment', function(event) {
            event.preventDefault();
            if (flag2) {
                return false;
            }
            flag2 = true;

            var text = $('#comment_text').val();

            text = text.trim();
            console.log('Hello : ', text);

            if (text == '') {
                Swal.fire({
                    title: 'error',
                    text: 'Please enter some text in note area',
                    icon: 'error'
                });
                flag2 = false;
                return false
            }

            $.ajax({
                url: appURL+'/campaign/comment',

                type: 'POST',
                dataType: 'json',
                data: {id: UnqId, text:text},
                headers: {
                    "X-CSRF-TOKEN": csrf,
                    Accpet: "applicationjson"
                },
            }).always(function(res) {
                flag2 = false;
                if (res.status == 'success') {
                    let status = res.status.substr(0,1).toUpperCase()+res.status.substr(1);
                    Swal.fire({
                            title: status,
                            text: res.msg,
                            icon: res.status,
                            onAfterClose: () => {
                            location.reload();
                }
                });
                } else {
                    let status = res.status.substr(0,1).toUpperCase()+res.status.substr(1);
                    Swal.fire({
                        title: status,
                        text: res.msg,
                        icon: res.status
                    });
                }
            });
        });

        var flag3 = false;
        $('body').on('click', '.del-cmnt', function(event) {
            event.preventDefault();
            if (flag3) {
                return false;
            }
            flag3 = true;

            var that = $(this);
            var id = that.data('comment_id');
            var pid = that.data('parent_id');

            $.ajax({
                url: apiURL+'/campaigns/comment/delete',
                type: 'POST',
                dataType: 'json',
                data: {comment_id: id, parent_id:pid},
                headers: {
                    "X-CSRF-TOKEN": csrf,
                    Accpet: "applicationjson"
                },
            }).always(function(res) {
                flag3 = false;
                let status = res.status.substr(0,1).toUpperCase()+res.status.substr(1);
                if (res.status == 'success') {
                    Swal.fire({
                        title: status,
                        text: res.msg,
                        icon: res.status,
                        onAfterClose: () => {
                            that.parents('.comment-blk').remove();
                        }
                    });
                } else {
                    Swal.fire({
                        title: status,
                        text: res.msg,
                        icon: res.status
                    });
                }
            });
        });

        $('body').on('change', '#campaign_status', function(event) {
            event.preventDefault();
            var that = $(this);
            var val = that.val();
            if (val == "") {
                return false;
            }

            ajx("POST", {status:val, campaign:campaign}, `${apiURL}/campaigns/status`, "json", function(response) {
                if(response.status == 'success'){
                    showMsg(response);
                    location.reload();
                }
            });
        });
    });
</script>
@endpush

@extends('staff.layouts.app')

@section('staff_css')
    <style>
        .invalid-feedback {
            display: block !important;
        }
        .daterangepicker {
            top: 260px !important;
        }
    </style>
    <link rel="stylesheet" href="{{ asset('css/bootstrap-select.min.css') }}">
@endsection

@section('staff_content')
    <div class="page-content grey-bg compaign-sec-bg-1" id="campaignPage">
      <div class="container-fluid">
         <div class="row">
            <div class="col-sm-12">
               <div class="page-title-box resp-main-heading">
                  <!-- <div class="float-right">
                     <ol class="breadcrumb">
                        <button type="button" class="btn btn-primary text-uppercase">
                           <i class="fas fa-file-pdf"></i>&nbsp;&nbsp; Generate PDF Report
                        </button>
                     </ol>
                  </div> -->
                  <h4 class="page-title overview-flot">Campaigns</h4>
               </div>
               <!--end page-title-box-->
            </div>
         </div>
         <!--end col-->
         <div class="row top-search01">
            <div class="col-xl-12">
               <div class="float-left">
                  <div class="btn-group custom-btn-group" role="group" aria-label="Basic example">
                     <a href="javascript:;" class="btn btn-camp" :class="{ active: filter.status == 'pending' }" @click="getCampaignByStatus('pending')">PENDING PAYMENT / AGREEMENT</a>
                     <a href="javascript:;" class="btn btn-camp" :class="{ active: filter.status == 'awaiting' }" @click="getCampaignByStatus('awaiting')">AWAITING BUILD</a>
                     <a href="javascript:;" class="btn btn-camp" :class="{ active: filter.status == 'billing_detail' }" @click="getCampaignByStatus('billing_detail')">BILLING DETAILS</a>
                     <a href="javascript:;" class="btn btn-camp" :class="{ active: filter.status == 'live' }" @click="getCampaignByStatus('live')">LIVE</a>
                     <a href="javascript:;" class="btn btn-camp" :class="{ active: filter.status == 'followed_up' }" @click="getCampaignByStatus('followed_up')">FOLLOWED UP</a>
                     <a href="javascript:;" class="btn btn-camp" :class="{ active: filter.status == 'completed' }" @click="getCampaignByStatus('completed')">COMPLETED</a>
                  </div>
               </div>
               <div class="float-left">
                   <a href="{{ route('staff.campaigns.create') }}" class="btn btn-add01" id="addNew"> <i class="fa fa-plus"></i> Add New </a>
{{--                   <a href="javascript:;" class="btn btn-add01" @click="populateForm(0)" id="addNew"> <i class="fa fa-plus"></i> Add New </a>--}}
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-xl-12">
               {{-- <div class="table-responsive">
                  <table class="table text-center td-block tableData">
                     <thead>
                        <tr class="th-head">
                           <th>Order </th>
                           <th>Business Name</th>
                           <th>Sales Person</th>
                           <th>Contract Value </th>
                           <th>Campaign Budget</th>
                           <th>Max Ad Spend (to date)</th>
                           <th></th>
                        </tr>
                     </thead>
                     <tbody>

                     </tbody>
                  </table>
               </div> --}}
                <div class="table-responsive">
                    <template>
                        <div>
                            <b-table responsive :items="landingPages" :fields="fields" :busy="isBusy" show-empty
                                        class="t-light td-block text-left" @filtered="Filtered">

                                <template v-slot:emptyfiltered="scope">
                                    <h4>@{{ scope.emptyFilteredText }}</h4>
                                </template>
                                <template v-slot:table-busy>
                                    <div class="text-center text-danger my-2">
                                        <b-spinner class="align-middle"></b-spinner>
                                        <strong>Loading...</strong>
                                    </div>
                                </template>
                                <template slot="HEAD_name" slot-scope="data">
                                    <span class="m-0 pl-2">@{{data.label}}</span>
                                </template>


                                <template v-slot:cell(id)="data">
                                    #@{{ data.item.id }}
                                </template>
                                <template v-slot:cell(user)="data">
                                    <span v-if="data.item.user ">
                                        @{{ data.item.user.name }}
                                    </span>
                                    <span v-else>
                                        -
                                    </span>
                                </template>

                                {{-- <template v-slot:cell(user)="data">
                                    <span v-if="data.item.company ">
                                        @{{ data.item.company.company_name }}
                                    </span>
                                    <span v-else>
                                        -
                                    </span>
                                </template> --}}

                                <template v-slot:cell(assigned)="data">
                                    <span v-if="data.item.assigned ">
                                        @{{ data.item.assigned.name }}
                                    </span>
                                    <span v-else>
                                        -
                                    </span>
                                </template>

                                <template v-slot:cell(recurring_option)="data">
                                    @{{ data.item.recurring_option ? data.item.recurring_option : "--" }}
                                </template>
                                <template v-slot:cell(recurring_way)="data">
                                    @{{ data.item.recurring_way ? data.item.recurring_way : "--" }}
                                </template>

                                <template v-slot:cell(amount)="data">
                                    &#163;@{{ data.item.amount }}
                                </template>
                                <template v-slot:cell(compaign_amount)="data">
                                    &#163;@{{ data.item.compaign_amount }}
                                </template>
                                <template v-slot:cell(max_add_amount)="data">
                                    &#163;@{{ data.item.max_add_amount }}
                                </template>
                                <template v-slot:cell(actions)="data">
                                    <span class="btn-group btn-flex" :data-id='data.item.id'>
                                        <b-button size="sm" title="Edit" variant="btn btn-primary btn-sm p-1 px-2 mt-0" @click="populateForm(data.item.id)" >
                                            <i class="fa fa-edit"></i>
                                        </b-button>

                                        <b-button :href="'/staff/campaign-leads/' + data.item.id" size="sm" title="View Leads" variant="btn btn-primary btn-sm p-1 px-2 mt-0 mr-1">
                                            <i class="fa fa-eye"></i>
                                        </b-button>

                                        <b-button :href="'/staff/campaign-detail/' + data.item.id" size="sm" title="View Detail" variant="btn btn-primary btn-sm p-1 px-2 mt-0 mr-1">
                                            <i class="fa fa-info-circle"></i>
                                        </b-button>

                                        <b-button size="sm" title="Delete Campaign" variant="btn btn-primary btn-sm p-1 px-2 mt-0" @click="deleteCampaign(data.item.id)" v-if="userType == 'admin'">
                                            <i class="fa fa-trash"></i>
                                        </b-button>
                                    </span>
                                </template>
                            </b-table>
                            <div class="row  m-auto">
                                <div class="col-xl-6 col-lg-6 col-md-6 float-left">
                                    <p class="mb-0 p-1 small mt-1 float-left">Showing @{{ landingPagesMeta.count }} of @{{ landingPagesMeta.total }} records | Per
                                        Page: @{{ landingPagesMeta.per_page }} | Current Page: @{{
                                        landingPagesMeta.current_page }}</p>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 float-right t012">
                                    <b-pagination class="float-right" size="sm" :total-rows="landingPagesMeta.total"  v-model="landingPagesMeta.current_page" :per-page="landingPagesMeta.per_page" first-text="First" prev-text="Previous" next-text="Next" last-text="Last" ellipsis-text="More" variant="danger">
                                    </b-pagination>
                                </div>
                            </div>
                            {{-- End Row --}}
                        </div>
                    </template>
                </div>
            </div>
         </div>
      </div>
      <!-- end page content -->

       <!-- Modal -->
       <div class="modal fade" id="addNewModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
          aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered modal-lg" style="max-width: 80%;" role="document">
             <div class="modal-content">
                <div class="modal-header">
                   <h5>Add Campaign</h5>
                   <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                   </button>
                </div>
                <form id="addCampaign">
                    <input type="hidden" value="" id="edit_id" name="edit_id">
                    <div class="modal-body">




                        <div class="row">
                            <div class="col-md-6" style="border-right: 1px solid #DBE0EC">
                                <input type="hidden" name="userId" id="userId" value="{{old('userId')}}">
                                <div class="">
                                    <h5>Point Of Contact</h5>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name">Name<span style="color: red"> *</span></label>
                                            <input type="text" name="user_name" value="{{old('name')}}" id="user_name" class="form form-control form-control-sm">
                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="job-position">Job Position<span style="color: red"> *</span></label>
                                            <input type="text" name="job_position" value="{{old('job_position')}}" id="job_position" class="form form-control form-control-sm">
                                            @error('job_position')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="mobile">Mobile Phone<span style="color: red"> *</span></label>
                                            <input type="text" name="mobile" value="{{old('mobile')}}" id="mobile" class="form form-control form-control-sm">
                                            @error('mobile')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="email">Email Address<span style="color: red"> *</span></label>
                                            <input type="text" name="email" value="{{old('email')}}" id="email" class="form form-control form-control-sm">
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6 password-blk">
                                        <div class="form-group">
                                            <label for="password">Password<span style="color: red"> *</span></label>
                                            <input type="password" name="password" value="{{old('password')}}" id="password" class="form form-control form-control-sm">
                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6 c-password-blk">
                                        <div class="form-group">
                                            <label for="confirm_password">Confirm Password<span style="color: red"> *</span></label>
                                            <input type="password" name="password_confirmation" value="{{old('password_confirmation')}}" id="password_confirmation" class="form form-control form-control-sm">
                                            @error('password_confirmation')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="status">Status</label>
                                            <select name="status" id="status" class="form form-control form-control-sm">
                                                <option value="new" {{ (old('status') === "new" ? "selected" : "") }}>New Client</option>
                                                <option value="recurring" {{ (old('status') === "completed" ? "recurring" : "") }}>Recurring Client</option>
                                                <option value="gold" {{ (old('status') === "gold" ? "selected" : "") }}>Gold Client</option>
                                                <option value="lost" {{ (old('status') === "lost" ? "selected" : "") }}>Lost Client</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="">
                                    <h5>Business Details</h5>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="company">Company Name<span style="color: red"> *</span></label>
                                            <input type="text" name="company" value="{{old('company')}}" id="company" class="form form-control form-control-sm">
                                            @error('company')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="work_email">Work Email Address</label>
                                            <input type="text" name="work_email" value="{{old('work_email')}}" id=work_email class="form form-control form-control-sm">
                                            @error('work_email')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="work_phone">Work Phone</label>
                                            <input type="text" name="work_phone" value="{{old('work_phone')}}" id="work_phone" class="form form-control form-control-sm">
                                            @error('work_phone')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="address">Address</label>
                                            <textarea name="address" id="address" class="w-100 p-2" cols="50" rows="4">{{old('address')}}</textarea>
                                            @error('address')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="website">Website</label>
                                            <input type="text" name="website" value="{{old('website')}}" id="website" class="form form-control form-control-sm">
                                            @error('website')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="display: none;">
                                    <div class="col-xl-6 col-lg-6 col-md-6">
                                        <label class="sr-only" for="facebook_link">Social Profile</label>
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text"><i class="mdi mdi-facebook"></i></div>
                                            </div>
                                            <input type="text" class="form form-control form-control-sm" value="{{old('facebook_link')}}" id="facebook_link" name="facebook_link" placeholder="Facebook Profile Link">
                                            @error('facebook_link')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-6">
                                        <label class="sr-only" for="twitter_link">Social Profile</label>
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text"><i class="mdi mdi-twitter"></i></div>
                                            </div>
                                            <input type="text" class="form form-control form-control-sm" value="{{old('twitter_link')}}" id="twitter_link" name="twitter_link" placeholder="Twitter Profile Link">
                                            @error('twitter_link')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                        <div class="col-xl-6 col-lg-6 col-md-6">
                                            <label class="sr-only" for="Instagram_link">Social Profile</label>
                                            <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text"><i class="mdi mdi-instagram"></i></div>
                                                </div>
                                                <input type="text" class="form form-control form-control-sm" value="{{old('instagram_link')}}" id="Instagram_link" name="Instagram_link" placeholder="Instagram Profile Link">
                                                @error('instagram_link')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="notes">Notes</label>
                                            <textarea name="notes" id="notes" class="w-100 p-2" cols="50" rows="4">{{old('notes')}}</textarea>
                                            @error('notes')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="col-md-6">
                                <div class="">
                                    <h5>Campaing Info</h5>
                                </div>
                                <div class="form-group">
                                    <label for="name">Campaign Name:</label>
                                    <input type="text" class="form-control" id="name" name="name">
                                </div>
                                <div class="form-group">
                                    <label for="date">Date:</label>
                                    <input type="text" class="form-control datepicker1" name="date" id="date" placeholder="Select Date">
                                </div>
                                {{-- <div class="form-group">
                                    <label for="user_id">Comapanies:</label>
                                    <multiselect name="user_id" v-model="user_id"
                                    :options="companies" :show-labels="false"
                                    placeholder="Select Company" label="name" track-by="id">
                                        <template slot="noResult" slot-scope="data">
                                            <strong>Sorry, couldn't find companty with this name.</strong>
                                        </template>
                                    </multiselect>
                                </div> --}}
                                <div class="form-group">
                                    <label for="amount">Select recurring option:</label>
                                    <br>
                                    <span @click="recurring_way = ''">
                                        <p-radio class="p-icon p-curve p-tada" name="recurring_option" v-model="recurringOption" value="daily" color="primary-o">
                                            <i slot="extra" class="icon mdi mdi-check"></i>
                                            Daily
                                        </p-radio>
                                        <p-radio class="p-icon p-curve p-tada" name="recurring_option" v-model="recurringOption" value="weekly" color="primary-o">
                                            <i slot="extra" class="icon mdi mdi-check"></i>
                                            Weekly
                                        </p-radio>
                                        <p-radio class="p-icon p-curve p-tada" name="recurring_option" v-model="recurringOption" value="monthly" color="primary-o">
                                            <i slot="extra" class="icon mdi mdi-check"></i>
                                            Monthly
                                        </p-radio>
                                    </span>
                                </div>
                                <div class="form-group" v-if="recurringOption == 'weekly' || recurringOption == 'monthly'">
                                    <label for="recurring_way">Select @{{recurringOption == 'weekly' ? 'day' : 'date'}}:</label>
                                    <multiselect name="recurring_way" v-model="recurring_way" :options="recurringOption == 'weekly' ? weekDays : get30days" :show-labels="false"
                                    :placeholder="recurringOption == 'weekly' ? 'day' : 'date'" label="label" track-by="label">
                                        <template slot="noResult" slot-scope="data">
                                            <strong>Sorry, couldn't find day with this name.</strong>
                                        </template>
                                    </multiselect>
                                </div>
                                {{-- <div class="form-group" v-if="recurringOption == 'monthly'">
                                    <label for="day">Select day:</label>
                                    <multiselect name="day" v-model="recurring_way" :options="get30days" :show-labels="false" placeholder="Select day" label="id" track-by="id">
                                        <template slot="noResult" slot-scope="data">
                                            <strong>Sorry, couldn't find day with this name.</strong>
                                        </template>
                                    </multiselect>
                                </div> --}}
                                <div class="form-group" id="contract_block">
                                    <label for="amount">Contract Value:</label>
                                    <input type="number" class="form-control" id="amount" name="amount">
                                </div>
                                <div class="form-group">
                                    <label for="compaign_amount">Campaign Budget:</label>
                                    <input type="number" class="form-control" id="compaign_amount" name="compaign_amount">
                                </div>
                               {{--<div class="form-group">
                                   <label for="type">Campaign Type</label>
                                   <select class="form form-control form-control-sm" name="type" id="campaign_type">
                                       <option value="">Select Type</option>
                                       <option value="one_off">One Off</option>
                                       <option value="monthly">Monthly</option>
                                   </select>
                               </div>--}}
                                <div class="form-group">
                                    <label for="campaign_specialist">Campaign Specialist:</label>
                                    <multiselect name="campaign_specialist" v-model="campaign_specialist" :options="staffMembers" :show-labels="false" placeholder="Select Campaign Specialist" label="name" track-by="id">
                                        <template slot="noResult" slot-scope="data">
                                            <strong>Sorry, couldn't find campaign specialist with this name.</strong>
                                        </template>
                                    </multiselect>
                                </div>

                                <div class="form-group">
                                    <label for="lp_specialist">LP Specialist:</label>
                                    <multiselect name="lp_specialist" v-model="lp_specialist" :value="lp_specialist" :options="staffMembers" :show-labels="false" placeholder="Select LP Specialist" label="name" track-by="id">
                                        <template slot="noResult" slot-scope="data">
                                            <strong>Sorry, couldn't find lp specialist with this name.</strong>
                                        </template>
                                    </multiselect>
                                </div>

                                <div class="form-group">
                                    <label for="account_manager">Account Manager:</label>
                                    <multiselect name="account_manager" v-model="account_manager" :options="staffMembers" :show-labels="false" placeholder="Select Account Manager" label="name" track-by="id">
                                        <template slot="noResult" slot-scope="data">
                                            <strong>Sorry, couldn't find account manager with this name.</strong>
                                        </template>
                                    </multiselect>
                                </div>
                                <div>
                                    <div class="form-group assigned-users">
                                        <label for="assigned_id">Sales Person:</label>
                                        <multiselect name="assigned_id" v-model="assigned_id" :options="staffMembers" :show-labels="false" placeholder="Select Sales Person" label="name" track-by="id">
                                            <template slot="noResult" slot-scope="data">
                                                <strong>Sorry, couldn't find sales person with this name.</strong>
                                            </template>
                                        </multiselect>
                                    </div>
                                </div>

                                <div class="insatllment_blk">
                                    <div class="form-group">
                                        <div class="custom-control custom-switch">
                                            <input type="checkbox" name="enable_installment" class="custom-control-input" id="enable_installment" value='Y'>
                                            <label class="custom-control-label" for="enable_installment">Enable Installment</label>
                                            <button class="add_installment btn btn-xs btn-primary m-0 float-right"><i class="fa fa-plus"></i></button>
                                        </div>
                                    </div>

                                    <div class="installments">
                                        <div class="appent_block">
                                            <div class="form-group clone-blk">
                                                <div class="row">
                                                    <div class="col">
                                                        <label>Installment Amount <span class="count">1</span>:</label>
                                                        <input type="text" class="form-control installment_amount" name="installment_amount[]">
                                                    </div>
                                                    <div class="col">
                                                        <label>Installment Date <span class="count">1</span>:</label>
                                                        <a href="javascript:;" class="text-danger float-right remove_block d-none"><i class="fa fa-trash-alt"></i></a>
                                                        <input type="text" class="form-control date-input datepicker installment_date" name="installment_date[]" readonly disabled>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group clone-blk">
                                                <div class="row">
                                                    <div class="col">
                                                        <label>Installment Amount <span class="count">2</span>:</label>
                                                        <input type="text" class="form-control installment_amount" name="installment_amount[]">
                                                    </div>
                                                    <div class="col">
                                                        <label>Installment Date <span class="count">2</span>:</label>
                                                        <a href="javascript:;" class="text-danger float-right remove_block d-none"><i class="fa fa-trash-alt"></i></a>
                                                        <input type="text" class="form-control date-input datepicker installment_date" name="installment_date[]">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>


                        </div>














                   </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-sm" :disabled="submiting">Save changes</button>
                    </div>
                </form>
             </div>
          </div>
       </div>
    </div>
@endsection

@push('staff_js')

<script>

    var App_url = '{!! url()->current() !!}';
    var appUrl = '{!! url('') !!}';
    var apiURL = '{!! url('/api/') !!}';
    var user_id = '{!! Auth::user()->id !!}';
    var user_type = '{!! Auth::user()->type !!}';
    var landingPages = {!! $landing_pages !!};
</script>

<script src="{{ asset('js/cmn/cmn.js') }}"></script>
<script src="{{ asset('js/app-vue.js') }}"></script>
<script src="{{ asset('js/staff/campaign/vue.js') }}"></script>
<script src="{{ asset('js/multi-select.js') }}"></script>
<script src="{{ asset('js/calender/bootstrap-select.min.js') }}"></script>
    <script>
        $('.selectpicker').selectpicker();

    </script>
@endpush

import Multiselect from "vue-multiselect";
import PrettyRadio from 'pretty-checkbox-vue/radio';
Vue.component('p-radio', PrettyRadio);
new Vue({
    el: '#campaignPage',
    components: { Multiselect },
    mounted() {
        this.filter.status = 'pending';
        this.getStaffMembers();
        this.getStaffLandingPages(false);

        let self = this;
        $('.datepicker1').daterangepicker({
            opens: 'left',
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'YYYY-MM-DD'
            },
            minDate: moment(),
            startDate: moment()
        }, function (start, end, label) {
            $(".datepicker1").val(start.format('YYYY-MM-DD'));
        });

        this.datePickerInit();

        /*$('.datepicker').daterangepicker({
            opens: 'left',
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'YYYY-MM-DD'
            },
            minDate: moment(),
            startDate: moment()
        }, function (start, end, label) {
            $(".datepicker").val(start.format('YYYY-MM-DD'));
        });*/

        $('#enable_installment').on('change', function (evt) {
            evt.preventDefault();
            if ($(this).is(':checked')) {
                $('.installments').removeClass('d-none');
                $('.add_installment').removeClass('d-none');
            } else {
                $('.installments').addClass('d-none');
                $('.add_installment').addClass('d-none');
            }
        });

        $('#enable_installment').trigger('change');

        $('#addCampaign').on('submit', function (evt) {
            self.submiting = true;
            evt.preventDefault();
            $('.clone-blk:first-child').find('.datepicker').attr({
                disabled: false,
                readonly: false
            });
            var data = $(this).serializeArray();
            for (var index = 0; index < data.length; index++) {
                if (data[index].name == "campaign_specialist" && self.campaign_specialist.id != undefined) {
                    data[index].value = self.campaign_specialist.id;
                }

                if (data[index].name == "lp_specialist" && self.lp_specialist.id != undefined) {
                    data[index].value = self.lp_specialist.id;
                }

                if (data[index].name == "account_manager" && self.account_manager.id != undefined) {
                    data[index].value = self.account_manager.id;
                }

                if (data[index].name == "assigned_id" && self.assigned_id.id != undefined) {
                    data[index].value = self.assigned_id.id;
                }

                // if (data[index].name == "user_id") {
                //     data[index].value = 6;
                // }
                if (data[index].name == "recurring_way" && self.recurring_way.label != undefined) {
                    data[index].value = self.recurring_way.label;
                }

            }
            data = jQuery.param(data);
            ajx("POST", data, `${apiURL}/staff/campaigns/add-campaign`, "json", function (response) {
                self.submiting = false;
                $('.clone-blk:first-child').find('.datepicker').attr({
                    disabled: true,
                    readonly: true
                });
                console.log(response)
                showMsg(response);
                if (response.status == 'success') {
                    location.reload();
                    self.loadPaginatedData();
                    $('#addNewModel').modal('hide');
                    $('#switch1').trigger('change');
                    formReset('#addCampaign');
                }
            });
        });

        $('body').on('click', '.assign-to-me', function (evt) {
            evt.preventDefault();
            data = {};
            data.user = '';
            data.campaign = $(this).parents('span').data('id');
            self.assignToMe(data);
        });

        $('body').on('click', '.assign-to-user', function (evt) {
        });

        $('.add_installment').on('click', function (evt) {
            evt.preventDefault();
            var length = $('.clone-blk').length;
            if (length >= 3) {
                return false;
            }
            var html = $('.clone-blk:last-child').clone();
            html.find('input').val('');
            html.find('.remove_block').removeClass('d-none');
            html.find('.installment_date').attr({
                disabled: false,
                readonly: false
            });

            html.find('.datepicker').daterangepicker({
                opens: 'left',
                singleDatePicker: true,
                showDropdowns: true,
                locale: {
                    format: 'YYYY-MM-DD'
                },
                minDate: moment(),
                startDate: moment()
            }, function (start, end, label) {
                html.find('.datepicker').val(start.format('YYYY-MM-DD'));
            });

            $('.appent_block').append(html);
            if ($('.clone-blk').length >= 3) {
                $('.add_installment').addClass('d-none');
            }
            self.recountInstallmentBlk();
        });

        $('body').on('click', '.remove_block', function (event) {
            event.preventDefault();
            var parent = $(this).parents('.clone-blk');
            parent.remove();
            if ($('.clone-blk').length < 3) {
                $('.add_installment').removeClass('d-none');
            }
            self.recountInstallmentBlk();
        });

        $('#assignUser').on('submit', function (evt) {
            evt.preventDefault();
            self.assignToMe($(this).serialize());
        });
    },
    data: {
        fields: [
            { key: 'id', label: 'Order' },
            { key: 'user', label: 'Client Name' },
            // { key: 'name', label: 'Company Name' },
            { key: 'name', label: 'Campaign Name' },
            { key: 'assigned', label: 'Sales Person' },
            { key: 'recurring_option', label: 'Recurring option' },
            { key: 'recurring_way', label: 'Recurring way' },
            { key: 'amount', label: 'Contract Value' },
            { key: 'compaign_amount', label: 'Campaign Budget' },
            { key: 'max_add_amount', label: 'Max Ad Spend (to date)' },
            { key: 'actions', label: 'Actions' }
        ],
        weekDays: [
            { key: 1, label: 'Saturday' },
            { key: 2, label: 'Sunday' },
            { key: 3, label: 'Monday' },
            { key: 4, label: 'Tuesday' },
            { key: 5, label: 'Wednesday' },
            { key: 6, label: 'Thursday' },
            { key: 7, label: 'Friday' }
        ],
        landingPages: [],
        landingPagesMeta: {},
        filter: {
            status: '',
        },
        isBusy: false,
        edit_id: '',
        name: '',
        date: '',
        landing_id: [],
        companies: [],
        user_id: null,
        dayName: [],
        recurringOption: "daily",
        recurring_way: "",
        amount: '',
        compaign_amount: '',
        type: '',
        assigned_opt: 'no',
        assigned_id: '',
        typeArr: {
            one_off: 'One Off',
            monthly: 'Monthly'
        },
        deleteCampaignUrl: apiURL + '/staff/campaigns/delete',
        getCampaignDetail: apiURL + '/staff/campaigns/edit',
        getStaffMembersUrl: apiURL + '/staff/other-projects/members',
        getLandingPagesUrl: apiURL + '/staff/campaigns/get-campaigns',
        staffMembers: [],
        campaign_specialist: [],
        lp_specialist: [],
        account_manager: [],
        assigned_id: [],
        userType: user_type,
        landing_pages_arr: landingPages,
        submiting: false,
        enable_installment: false,
    },
    watch: {
        'landingPagesMeta.current_page': function (val) {
            this.loadPaginatedData();
        }
    },
    computed: {
        get30days() {
            return Array(30).fill().map((element, index) => { return { label: index + 1 } });
        }
    },
    created() {
        //this.get30days;
        this.getAllCompanies();
    },
    methods: {
        getAllCompanies() {
            axios.get('/api/staff/get-all-companies')
                .then((response) => {
                    this.companies = response.data
                    //console.log(response);
                })
                .catch((error) => {
                    console.log(error);
                });

        },
        recountInstallmentBlk: function () {
            var length = $('.clone-blk').length;
            if (length > 0) {
                $.each($('.clone-blk'), function (index, value) {
                    var nextCount = parseInt(index) + 1;
                    $(this).find('.count').html(nextCount);
                });
            }
        },
        datePickerInit: function () {
            var length = $('.clone-blk').length;
            if (length > 0) {
                $.each($('.clone-blk'), function (index, value) {
                    var nextCount = parseInt(index) + 1;
                    let pick = $(this).find('.datepicker');

                    pick.daterangepicker({
                        opens: 'left',

                        singleDatePicker: true,
                        showDropdowns: true,
                        locale: {
                            format: 'YYYY-MM-DD'
                        },
                        minDate: moment(),
                        startDate: moment()
                    }, function (start, end, label) {
                        pick.val(start.format('YYYY-MM-DD'));
                    });
                });
            }
        },
        loadPaginatedData: function () {
            this.getStaffLandingPages(this.getLandingPagesUrl + '?page=' + this.landingPagesMeta.current_page);
        },
        getStaffLandingPages: function (url = false) {
            this.isBusy = true;
            if (url == false) {
                url = this.getLandingPagesUrl;
            }
            console.log('landing url:' + url)
            axios.post(url, { filter: this.filter })
                .then(response => {
                    if (response.data.status == 'success') {
                        this.isBusy = false;
                        console.log(response.data.data.data)
                        this.landingPages = response.data.data.data;
                        this.landingPagesMeta = response.data.data.meta;
                    }
                }).catch(error => {
                    this.isBusy = false;
                });
        },
        resetFilter: function () {
            this.filter = {
                status: ''
            };
            this.getStaffLandingPages(false);
        },
        getCampaignByStatus: function (status) {
            this.filter.status = status;
            this.getStaffLandingPages(false);
        },
        deleteCampaign: function (id) {
            axios.post(this.deleteCampaignUrl, { campaign_id: id })
                .then(response => {
                    if (response.data.status == 'success') {
                        var title = 'Success';
                    } else {
                        var title = 'Error';
                    }
                    Swal.fire({
                        title: title,
                        icon: response.data.status,
                        text: response.data.msg,
                        onAfterClose: () => {
                            if (response.data.status == 'success') {
                                location.reload();
                            }
                        }
                    });
                }).catch(error => {
                    console.log(error);
                });
        },
        getStaffMembers: function () {
            axios.get(this.getStaffMembersUrl,)
                .then(response => {
                    if (response.data.status == 'success') {
                        this.staffMembers = response.data.data;
                    }
                }).catch(error => {
                    console.log(error);
                });
        },
        assignToMe: function (data) {
        },
        Filtered(filteredItems) {
            this.landingPagesMeta.totalRows = filteredItems.length;
            this.landingPagesMeta.currentPage = 1;
        },
        populateForm: function (id) {
            if (id <= 0) {
                $('#assign_block').removeClass('d-none');
                $('#assign_block input').attr('disabled', false);
                $('#assign_block select').attr('disabled', false);

                $('#contract_block').removeClass('d-none');
                $('#contract_block > input').attr('disabled', false);

                $('#edit_id').val('');
                $('#name').val('');
                $('#date').val(moment().format('YYYY-MM-DD'));
                $('#amount').val('');
                $('#compaign_amount').val('');
                $('#campaign_type').val('');
                this.campaign_specialist = [];
                this.lp_specialist = [];
                this.account_manager = [];
                this.landing_id = [];
                this.assigned_id = [];

                $("#enable_installment").prop("checked", false);
                $("#enable_installment").trigger('change');
                $('.insatllment_blk').removeClass('d-none');

                $('#addNewModel').modal('show');
            } else {
                axios.post(this.getCampaignDetail, { id: id })
                    .then(response => {
                        console.log(response);
                        let res = response.data;
                        if (res.status == 'success') {
                            /*$('#assign_block').addClass('d-none');
                            $('#assign_block input').attr('disabled', true);
                            $('#assign_block select').attr('disabled', true);*/

                            $('.password-blk').addClass('d-none');
                            $('.c-password-blk').addClass('d-none');

                            $("#user_name").val(res.data.user_details_obj.name);
                            $("#job_position").val(res.data.user_details_obj.job_position);
                            $("#mobile").val(res.data.user_details_obj.phone);
                            $("#email").val(res.data.user_details_obj.email);
                            $("#status").val(res.data.user_details_obj.status);

                            if (res.data.company_details_obj) {
                                $("#company").val(res.data.company_details_obj.name);
                            } else {
                                $("#company").val('');
                            }
                            if (res.data.business_details_obj) {
                                $("#work_email").val(res.data.business_details_obj.email_address);
                                $("#work_phone").val(res.data.business_details_obj.phone);
                                $("#address").val(res.data.business_details_obj.address);
                                $("#website").val(res.data.business_details_obj.website);
                                $("#facebook_link").val(res.data.business_details_obj.facebook_link);
                                $("#twitter_link").val(res.data.business_details_obj.twitter_link);
                                $("#instagram_link").val(res.data.business_details_obj.instagram_link);
                                $("#notes").val(res.data.business_details_obj.notes);
                            } else {
                                $("#work_email").val('');
                                $("#work_phone").val('');
                                $("#address").val('');
                                $("#website").val('');
                                $("#facebook_link").val('');
                                $("#twitter_link").val('');
                                $("#instagram_link").val('');
                                $("#notes").val('');
                            }










                            $("#enable_installment").prop("checked", false);
                            $("#enable_installment").trigger('change');
                            $('.insatllment_blk').addClass('d-none');

                            $('#contract_block').addClass('d-none');
                            $('#contract_block > input').attr('disabled', true);

                            $('#edit_id').val(id);
                            $('#name').val(res.data.name);
                            $('#date').val(res.data.given_date);
                            $('#compaign_amount').val(res.data.compaign_amount);
                            $('#campaign_type').val(res.data.type);

                            this.recurringOption = res.data.recurring_option;


                            this.user_id = this.companies.find(company => {
                                if (company.user_id == res.data.user_id) return company
                            })

                            // console.log(typeOf, res.data.recurring_way);

                            if (res.data.recurring_option == 'weekly') {
                                this.recurring_way = this.weekDays.find(day => {
                                    if (day.label == res.data.recurring_way) return day
                                })
                            }

                            if (res.data.recurring_option == 'monthly') {
                                this.recurring_way = this.get30days.find(day => {
                                    if (day.label == res.data.recurring_way) return day
                                })
                            }




                            //console.log('asfsafd: '+res.data.recurring_option);

                            this.landing_id = { id: res.data.landing_pages_id, name: res.data.landing_page_name };
                            this.campaign_specialist = res.data.campaign_specialist_obj;
                            this.lp_specialist = res.data.lp_specialist_obj;
                            this.account_manager = res.data.account_manager_obj;

                            if (res.data.sales_person_obj != null) {
                                if (res.data.sales_person_obj.type == 'admin') {
                                    $('#switch1').prop('checked', false);
                                    this.assigned_id = [];
                                } else {
                                    $('#switch1').prop('checked', true);
                                    this.assigned_id = res.data.sales_person_obj;
                                }
                            } else {
                                $('#switch1').prop('checked', false);
                                this.assigned_id = [];
                            }
                            $('#switch1').trigger('change')

                            $('#addNewModel').modal('show');
                        } else {
                            let status = res.status.substr(0, 1).toUpperCase() + res.status.substr(1);
                            Swal.fire({
                                title: status,
                                text: res.msg,
                                icon: res.status,
                                onAfterClose: () => {
                                    //$('.add_project').trigger('click');
                                }
                            });
                        }
                    }).catch(error => {
                        let status = res.status.substr(0, 1).toUpperCase() + res.status.substr(1);
                        Swal.fire({
                            title: status,
                            text: res.msg,
                            icon: res.status,
                            onAfterClose: () => {
                                //$('.add_project').trigger('click');
                            }
                        });
                    });
            }
        },
    }
});

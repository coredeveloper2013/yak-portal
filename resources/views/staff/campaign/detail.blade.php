@extends('staff.layouts.app')

@section('staff_css')
    <style>
        .invalid-feedback {
            display: block !important;
        }
        .chat-body {
            padding: 12px 0 10px 0 !important;
         }

    </style>
@endsection

@section('staff_content')
   <div class="page-content grey-body-bg">
      <div class="">
         <div class="row">
            <div class="col-12">
               <div class="chat-box-left chat-box-left-staff">
                  <h4 class="chat-head clearfix">
                     <span class="mt-2 float-left">My Leads </span>
                     <button class="btn btn-xs btn-primary add_lead m-0 float-right">Add Lead</button>
                  </h4>
                  <div class="chat-search">
                     <form id="lead-search" method="post">
                        <div class="form-group">
                           <div class="input-group chat-search">
                              <input type="text" id="chat-search" name="search" class="form-control chat-search-input"
                                 placeholder="Search">
                              <span class="input-group-append">
                                 <button type="submit" class="btn btn-grey shadow-none"><i class="fas fa-search"></i></button>
                              </span>
                           </div>
                        </div>
                     </form>
                  </div>
                  <!--end chat-search-->
                  <div class="tab-content chat-list chat-list-search mt-3 slimscroll" id="pills-tabContent">
                     <div class="tab-pane fade show active" id="general_chat">

                     </div>
                     <div class="no_record text-center d-none">
                        <b>No Record Found</b>
                     </div>
                     <div class="load_more clearfix text-center p-2 ml-1 mr-1">
                        <button class="btn btn-xs btn-block btn-grey mt-2">Load More</button>
                     </div>
                     <!--end general chat-->
                  </div>
                  <!--end tab-content-->
               </div>

               <!--end chat-box-left -->
               <div class="chat-box-right">
                     <div class="chat-header p-3" style="min-height: auto;">
                        <div class="media-body overflow">
                            <h6 class="m-0 p-0 chat-title-name float-left chat-title-m">{{$campaign->name}}</h6>

                            @if (!empty($leads))
                              <div class="float-right">
                                  <div class="chat-features float-right">
                                     <div class="d-sm-inline-block">
                                        <a href="javascript:void(0)" data-status="open" class="btn btn-mark changeStatusBtn"></a>
                                     </div>

                                    @if (Auth::user()->type == 'admin')
                                      <div class="d-sm-inline-block">
                                        <a href="javascript:void(0)" class="btn btn-mark btn-danger text-white delete-lead">Delete</a>
                                      </div>
                                    @endif
                                  </div>
                               </div>
                            @endif

                        @if ($campaign->status != "Pending")
                           <div class="chat-features mr-2 float-right">
                              <div class="d-sm-inline-block">
                                 <select class="custom-select" name="campaign_status" id="campaign_status">
                                    <option value="">Select Status</option>
                                    <option value="awaiting" disabled {{ 'Awaiting' == $campaign->status ? 'selected' : '' }}>Awaiting</option>
                                    <option value="billing_detail" {{ 'Billing detail' == $campaign->status ? 'selected' : '' }}>Billing Detail</option>
                                    <option value="live" {{ 'Live' == $campaign->status ? 'selected' : '' }}>Live</option>
                                    <option value="followed_up" {{ 'Followed up' == $campaign->status ? 'selected' : '' }}>Followed Up</option>
                                    <option value="completed" {{ 'Completed' == $campaign->status ? 'selected' : '' }}>Completed</option>
                                </select>
                              </div>
                           </div>
                        @endif
                        </div>


                     </div>

                  <!-- end chat-header -->

                  <div class="lead_detail_block">
                     <div class="chat-body ">
                        <div class="card w-100">
                           <div class="card-body">
                              <h4>Lead Details</h4>
                              <div class="detail-wrap">
                                 <div class="row" style="height: 300px;">
                                    <div class="col-12 text-center pt-4 mt-4">
                                       <h2 class="chat-detail-head mb-0">This campaign doesn't have any leads yet!</h2>
                                       <p class="subtext">If you have received a lead notification but the lead isn't here please let us know as soon as you can.</p>
                                    </div>
                                 </div>
                              </div>
                               <a href="javascript:void(0)" style="display: none;" id="btn-details-load" onclick="showMore()" class="float-right mt-2 font-weight-bold">View more </a>
                           </div>
                           <!--end card-body-->
                        </div>
                        <ul class="user-comment-sec scrollclass charHistory d-none">
                        </ul>
                        <!--end card-->
                     </div>
                     <!-- end chat-body -->

                     <div class="chat-footer chat-footer-custom">
                        <form id="sendMessage">
                           <div class="">
                              <div class="txt-area-sec">
                                 <textarea class="chat-input chat-input-1" name="message"></textarea>
                                 <input type="hidden" name="lead_id">
                              </div>
                              <input type="submit" class="btn btn-send" value="SEND">
                           </div>

                        </form>

                        <!-- end row -->
                     </div>
                     <!-- end chat-footer -->
                  </div>
               </div>
               <!--end chat-box-right -->
            </div>
            <!-- end col -->
         </div>
      </div>
      <!-- end row -->
      <!-- container -->
      @include('layouts.footer')
      <!--end footer-->

      <div class="modal" id="addEditLead" tabindex="-1" role="dialog">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <form id="addEditLeadForm">
                  <div class="modal-header">
                     <h5 class="modal-title">Add Lead</h5>
                     <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                     </button>
                  </div>

                  <div class="modal-body">
                     <div class="append_block">
                        <div class="question_block">
                           <div class="form-group mb-2">
                              <label for="name" class="font-weight-bold black-text w-100">
                                 Question <span class="q_count">1</span>
                                 <a href="javascript:;" class="text-danger float-right remove_block d-none"><i class="far fa-trash-alt"></i></a>
                              </label>
                              <input type="text" class="form-control heading" name="heading[]" aria-describedby="Heading" placeholder="Question">
                           </div>
                           <div class="form-group">
                              <input type="phone" class="form-control answer" name="answer[]" aria-describedby="Answer" placeholder="Answer">
                           </div>
                           <hr style="width: 50%">
                        </div>
                     </div>
                     <div class="form-group clearfix text-center">
                        <button class="clone_block btn btn-xs btn-primary add_lead m-0"><i class="fas fa-plus"></i> Add Question</button>
                     </div>

                     <input type="hidden" name="lead_id" value="">
                     <input type="hidden" name="campaign_id" value="{!! $campaign->id !!}">
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                     <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
   <div class="modal" id="detailsModal" tabindex="-1" role="dialog">
       <div class="modal-dialog" role="document">
           <div class="modal-content">
               <form id="addEditLeadForm">
                   <div class="modal-header">
                       <h5 class="modal-title">Lead Details</h5>
                       <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                           <span aria-hidden="true">&times;</span>
                       </button>
                   </div>

                   <div class="modal-body">
                       <div class="detail-wrap" id="modal-details-wrap"></div>
                   </div>
                   <div class="modal-footer">
                       <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                   </div>
               </form>
           </div>
       </div>
   </div>
@endsection

@push('staff_js')
   <script>
      var appURL = '{!! url('/') !!}';
      var apiURL = '{!! url('/api/staff/') !!}';
      var campaign = '{!! $campaign->id !!}';
      @empty($leads)
         var lead = '';
      @else
         var lead = '{!! $leads->id !!}';
      @endempty


       var storagePath = "{!! asset('storage/avatars/') !!}";
       var defaultImage = "{!! asset('staff/images/user-1.jpg') !!}";
   </script>
   {{--  Inject vue js files  --}}
   <script src="{{ asset('js/cmn/cmn.js') }}"></script>
   <script src="{{ asset('js/staff/leads/index.js') }}"></script>
@endpush


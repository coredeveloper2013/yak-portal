@extends('layouts.app')
@section('css')
    <style>
        .invalid-feedback {
            display: block !important;
        }
    </style>
@endsection
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.10.12/dist/sweetalert2.all.min.js" integrity="sha256-HyVNOA4KTbmvCLxBoFNrj0FLZtj/RCWyg4zfUjIry0k=" crossorigin="anonymous"></script>
@section('content')
    <!-- Page Content-->
    <div class="page-content" id="noti-mobile">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="float-right">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Main</a></li>
                                <li class="breadcrumb-item active">Campaigns</li>
                            </ol>
                        </div>
                    </div>
                    <!--end page-title-box-->
                </div>
                <!--end col-->
            </div>
            @if (\Session::has('success'))
                <div class="alert alert-success" id="alert_created_success">
                    <strong>{!! \Session::get('success') !!}</strong>
                </div>
            @endif

            @if (\Session::has('error'))
                <div class="alert alert-danger" id="alert_created_success">
                    <strong>{!! \Session::get('error') !!}</strong>
                </div>
            @endif
            <div class="row">
            <div class="col-xl-11 col-lg-10 col-md-11 mx-auto">
            <div class="row">
                <div class="col-xl-12">
                    <h3>My Business</h3>
                </div>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body notification-resp">
                            <a href="javascript:;" class="float-right btn btn-primary btn-xs space-bt phone-btn"
                               onclick="showAddModal('sms')">Add Alert</a>
                            <h5 class="mt-0">SMS Notifications</h5>
                            <div class="table-responsive">
                            @if(sizeof($businessSms) > 0)
                                <table class="table mb-0">
                                    <thead class="thead-light">
                                    <tr>
                                        <th>Date Created</th>
                                        <th>Name</th>
                                        <th>Phone Number</th>

                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($businessSms as $sms)
                                    <tr>
                                        <td>{{$sms->human_date}}</td>
                                        <td>{{$sms->name}}</td>
                                        <td>
                                            {{$sms->send_on}}
                                        </td>

                                        <td>
                                            <div class="btn-group btn-flex">
                                                <a href="javascript:;" onclick="showEditSmsAlert('{{$sms->name}}', '{{$sms->send_on}}', '{{$sms->id}}', 'sms')" class="btn btn-primary btn-sm p-1 px-2 mt-0 mr-1">
                                                    <i class="fas fa-edit"></i></a>
                                                <a href="{{url('delete-alert/'.$sms->id)}}"  class="btn btn-primary btn-sm p-1 px-2 mt-0" onclick="confirmation(event)" >
                                                    <i class="fas fa-trash"></i></a>
                                            </div>

                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            @else
                                <h5 class="text-center req">No records found.</h5>
                            @endif
                                <!--end /table-->
                            </div>
                            <!--end /tableresponsive-->
                        </div>
                        <!--end card-body-->
                    </div>
                    <!--end card-->
                </div>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <a href="javascript:;" class="float-right btn btn-primary btn-xs space-bt phone-btn" onclick="showAddModal('email')">Add Alert</a>
                            <h5 class="mt-0">Email Notifications</h5>
                            <div class="table-responsive">
                                @if(sizeof($businessEmail) > 0)
                                    <table class="table mb-0">
                                        <thead class="thead-light">
                                        <tr>
                                            <th>Date Created</th>
                                            <th>Name</th>
                                            <th>Email</th>

                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($businessEmail as $sms)
                                            <tr>
                                                <td>{{$sms->human_date}}</td>
                                                <td>{{$sms->name}}</td>
                                                <td>
                                                    {{$sms->send_on}}
                                                </td>

                                                <td>
                                                    <div class="btn-group btn-flex">
                                                        <a href="javascript:;"
                                                           onclick="showEditSmsAlert('{{$sms->name}}', '{{$sms->send_on}}', '{{$sms->id}}', 'email')" class="btn btn-primary btn-sm p-1 px-2 mt-0 mr-1">
                                                            <i class="fas fa-edit"></i></a>

                                                        <a href="{{url('delete-alert/'.$sms->id)}}"  class="btn btn-primary btn-sm p-1 px-2 mt-0" onclick="confirmation(event)" >
                                                            <i class="fas fa-trash"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>

                                @else
                                    <h5 class="text-center req">No records found.</h5>
                                @endif
                                <!--end /table-->
                            </div>
                            <!--end /tableresponsive-->
                        </div>
                        <!--end card-body-->
                    </div>
                    <!--end card-->
                </div>
            </div>
            </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="addAlertModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add Alert</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="{{route('notification.add.sms.alert')}}">
                                @csrf
                                <input type="hidden" name="id" id="id"  value="{{old('id')}}">
                                <input type="hidden" name="type" id="type"  value="{{old('type')}}">
                            <div class="container">
                                <div class="row mb-3">
                                    <label for="name">Name</label>
                                    <input type="text" name="name" id="name" class="form form-control" value="{{old('name')}}">
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="row mb-3">
                                    <label for="send_on" class="alert_title">Phone Number</label>
                                    <input type="text" name="send_on" id="send_on" class="form form-control" value="{{old('send_on')}}" placeholder="e.g. john.bishop@gmail.com">
                                    @error('send_on')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- container -->
        @include('layouts.footer')
        <!--end footer-->
    </div>
    <!-- end page content -->
@endsection
@push('js')
    <script>
        $(document).ready(function(){
            @if (count($errors) > 0)
                $('#addAlertModal').modal('show');
            @endif
            setTimeout(function () {
                $("#alert_created_success").fadeOut(5000);
            },1000);
        });

        function showAddModal(type){
            $("#name").val('');
            $("#send_on").val('');
            $("#id").val('');
            $("#type").val(type);
            if (type == 'sms') {
                $("#addAlertModal").find('.alert_title').html('Phone Number');
                $("#addAlertModal").find('#send_on').attr('placeholder', 'e.g. +447397909437');
            } else {
                $("#addAlertModal").find('.alert_title').html('Email');
                $("#addAlertModal").find('#send_on').attr('placeholder', 'e.g. john.bishop@gmail.com');
            }
            $("#addAlertModal").modal('show');
        }
        function showEditSmsAlert(name, number, id, type){
            $("#name").val(name);
            $("#send_on").val(number);
            $("#id").val(id);
            $("#type").val(type);
            if (type == 'sms') {
                $("#addAlertModal").find('.alert_title').html('Phone Number');
                $("#addAlertModal").find('#send_on').attr('placeholder', 'e.g. +447397909437');
            } else {
                $("#addAlertModal").find('.alert_title').html('Email');
                $("#addAlertModal").find('#send_on').attr('placeholder', 'e.g. john.bishop@gmail.com');
            }
            $("#addAlertModal").modal('show');
        }


        function confirmation(ev) {
            ev.preventDefault();
            var urlToRedirect = ev.currentTarget.getAttribute('href'); //use currentTarget because the click may be on the nested i tag and not a tag causing the href to be empty
            Swal.fire({
                title: 'Are you sure?',
                text: 'Once deleted, this will remove this alert.',
                icon: 'warning',
                showCancelButton: true,

            }).then(function(res) {
                if (res.value) {
                    window.location.href = urlToRedirect;
                }
            });
        }

    </script>

@endpush

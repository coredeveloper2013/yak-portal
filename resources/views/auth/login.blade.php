{{--
@extends('layouts.app')

@section('css')
    <style>
        .invalid-feedback{
            display: block !important;
        }
        body{
            background: url(../public/images/vuexy-login-bg.jpg) no-repeat center center;
            background-size: cover;
        }
    </style>
@endsection
@section('content')

<div class="container-fluid signin-page">


        <section class="row flexbox-container">


          <div class="col-xl-8 col-11 ">
            <div class="text-center mt-4">
              <img src="{{asset('/images/yak.png')}}" height="160" alt="logo" class="logo-login">
            </div>
            <div class="d-flex justify-content-center">

              <div class="card bg-authentication rounded-0 mb-0">

                  <div class="row m-0">

                      <div class="col-lg-6 d-lg-block d-none text-center align-self-center px-3 pr-3 py-0">


                              <img src="{{asset('/images/login.png')}}" alt="logo" class="">



                      </div>


                      <div class="col-lg-6 col-12 p-0">


                          <div class="card rounded-0 mb-0 px-4">

                                            <div class="mb-3 pl-3 pr-3">


                                                 <div class="card-title pt-4">
                                                     <h4 class="m-0">Login</h4>
                                                 </div>

                                             <p>Welcome back, please login to your account.</p>
                                --}}
{{--                <div class="sign-in-link">Doesn't have an account? <a href="{{route('register')}}"> Sign up</a></div>--}}{{--

                                            </div>

                                            <div class="row">
                                                <div class="col-xl-12 col-lg-12 col-md-12 pl-4 pr-4">
                                                    <form class="needs-validation" method="POST" action="{{ route('login') }}" novalidate>
                                                        @csrf
                                                        <!--end col-->

                                                        <div class="form-group mb-4">

                                                            <!-- <label for="validationCustom02">Email Address</label> -->
                                                            <input type="text" class="form-control" id="validationCustom02" placeholder="Email" name="email" value="{{old('email')}}" required>
                                                            <span class=""><i class="far fa-user"></i></span>
                                                            @error('email')
                                                            <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                        <!--end col-->
                                                        <div class="form-group mb-4">
                                                            <!-- <label for="validationCustom03">Password</label> -->
                                                            <input type="password" class="form-control" id="validationCustom03" placeholder="Password" name="password" value="{{old('password')}}" required>
                                                            <span class=""><i class="fas fa-lock"></i></span>
                                                            @error('password')
                                                            <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>

                                                        <!--end col-->
                                                        <!--end col-->
                                                        <div class="pb-3 clearfix">
                                                            <div class="form-check no-padding float-left">
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" id="horizontalCheckbox" name="remember" data-parsley-multiple="groups" data-parsley-mincheck="2">
                                                                    <label class="custom-control-label" for="horizontalCheckbox">Remember me</label>
                                                                </div>
                                                            </div>
                                                            @if (Route::has('password.request'))
                                                                <a class="float-right text-primary"  href="{{ route('password.request') }}">
                                                                    {{ __('Forgot Password?') }}
                                                                </a>
                                                            @endif
                                                        </div>
                                                        <!--end form-group-->
                                                        <div style="display: flex; justify-content: space-between">
                                                            <a href="{{ route('register') }}" style="font-weight: bold">Create New</a>
                                                            <button class="btn btn-primary float-right btn-inline waves-effect waves-light mb-4" type="submit">Sign In</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                          </div>


                      </div>


                  </div>


              </div>
            </div>

          </div>


        </section>

</div>



@endsection
--}}


@extends('_v2.layout.auth')

@section('title') Login @endsection

@section('css')
    <style>
        body {
            overflow: hidden;
        }

        .form-group .form-control {

            border-radius: 0px !important;
        }
    </style>
@endsection

@section('content')

    <div class="row login">


        <div class="col-md-5 bg-white">


            <form class="form-sign-in" method="POST" action="{{ route('login') }}">
                @csrf

                <img class="logo" src="{{ asset('images/logo/image-23.png') }}">

                <h3 class="text-dark font-weight-bolder pt-5 pb-4">Log in to your account </h3>
                <br>
                <div class="form-group-floating">
                    <input type="text" id="email" name="email" class="form-control" required>
                    <label class="form-control-placeholder" for="email">Email</label>
                </div>
                <div class="form-group-floating">
                    <input type="password" id="password" name="password" class="form-control" required>
                    <label class="form-control-placeholder" for="password">Password</label>
                </div>
                <p class="text-right forget-password">
                    <a href="{{ url('/password/reset') }}" class="text-right">Forgot Password?</a>
                </p>

                <button class="btn btn-lg btn-primary rounded btn-block font-weFight-bold" type="submit">LOGIN</button>
                {{--<p class="mt-5 mb-3 register text-muted">Need to create a PayPerCustomer account? <a href="{{ url('/payment') }}">Create an account</a></p>--}}
            </form>
        </div>
        <div class="col-md-7 bg-login-gradient d-none d-md-block position-relative" style="height: 100vh;">
            <img src="{{ asset('/images/v2/frame.png') }}" class="card-img-top" alt="...">
            <div class="card main-card">
                <img src="{{ asset('/images/v2/frame_image.png') }}" alt="Card image cap">
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <h5 class="card-title text-dark">Windows Business</h5>
                        <div>
                            <span class="fa fa-star text-warning"></span>
                            <span class="fa fa-star text-warning"></span>
                            <span class="fa fa-star text-warning"></span>
                            <span class="fa fa-star text-warning"></span>
                            <span class="fa fa-star text-warning"></span>
                        </div>
                    </div>
                    <small class="card-text text-dark font-weight-normal">“Thank you PayPerCustomer for all of the help
                        and continued support. I cannot express how grateful I am with what you do for us, and how our
                        business has changed since starting. I will continue to use your services as long as you keep
                        delivering as you are!”</small>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.app')

@section('css')
<style>
    .invalid-feedback{
        display: block !important;
    }
</style>

@endsection

@section('content')
    <div class="container-fluid">
        <div class="row login-wrapper flex-row-reverse">

            <div class="col-xl-5 col-lg-5 col-md-5 form-bg">
                <div class="login-head">
                   
                    <h2>Sign up</h2>
                    <div class="sign-in-link">Already have an account? <a href="{{route('login')}}">Sign in</a></div>
                </div>

                <div class="row">
                    <div class="col-xl-6 col-lg-8 col-md-8">
                        <form class="needs-validation" method="POST" action="{{ route('register') }}" autocomplete="off" novalidate>
                            @csrf
                            <div class="form-group">
                                <label for="validationCustom01">Full name<span class="req">*</span></label>
                                <input type="text" class="form-control" id="validationCustom01" name="name" value="{{old('name')}}" autocomplete="off" required>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <!--end col-->
                            <div class="form-group">
                                <label for="validationCustom02">Email Address<span class="req">*</span></label>
                                <input type="text" class="form-control" id="validationCustom02" name="email" value="{{old('email')}}" autocomplete="off" required>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                            <!--end col-->
                            <div class="form-group">
                                <label for="validationCustom03">Password<span class="req">*</span></label>
                                <input type="password" class="form-control" id="validationCustom03" name="password" value="{{old('password')}}" autocomplete="off" required>
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <!--end col-->
                            <div class="form-group">
                                <label for="validationCustom10">Confirm Password<span class="req">*</span></label>
                                <input type="password" class="form-control" id="validationCustom10" name="password_confirmation" autocomplete="off" required>
                            </div>
                            <!--end col-->
                            <div class="form-group">
                                <label for="validationCustom04">Company<span class="req">*</span></label>
                                <input type="text" class="form-control" id="validationCustom04" name="companyName" value="{{old('companyName')}}" autocomplete="off" required>
                                @error('companyName')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                            <!--end col-->
                            <div class="form-group">
                                <div class="form-check no-padding">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="horizontalCheckbox" data-parsley-multiple="groups" data-parsley-mincheck="2" name="terms">
{{--                                        <label class="custom-control-label" for="horizontalCheckbox">I Agree to <a href="#">Terms & Condition</a></label>--}}
                                        <label class="custom-control-label" for="horizontalCheckbox">I Agree to Terms & Condition</label>
                                    </div>
                                    @error('terms')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <!--end form-group-->
                            <button class="btn btn-primary btn-block" type="submit">Create your free account </button>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-xl-7 col-lg-7 col-md-7 bg-login">
                <div class="login-txt my-auto">
                    <a href="{{url('/')}}"><img src="{{asset('/images/app-icon-bull.png')}}" alt="logo"></a>
                   <!--  <h2>High Quality <br>Bespoke <br>Lead Generation</h2>
                    <p>Treo helps developers to build organized and well-coded admin <br> dashboards full of beautiful and feature rich modules.</p> -->
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row login-wrapper flex-row-reverse">

        <div class="col-xl-5 col-lg-5 col-md-5 form-bg signin-bg">
            <div class="login-head">
               
                <h2>{{ __('Reset Password') }}</h2>
            </div>
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6">
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group">
                            <label for="email" class="col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                        <div class="form-group mb-0">
                                <button type="submit" class="btn btn-primary btn-block">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                        </div>
                    </form>
                </div>
                
            </div>

        </div>
        <div class="col-xl-7 col-lg-7 col-md-7 bg-login">
            <div class="login-txt my-auto">
                <a href="{{url('/')}}"><img src="{{asset('/images/app-icon-bull.png')}}" alt="logo" class="signin-logo"></a>
            </div>
        </div>
    </div>
</div>

@endsection

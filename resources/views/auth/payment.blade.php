@extends('_v2.layout.auth')
@section('title') Payment @endsection
@section('content')
    <div class="payment bg-white">
        <div class="d-flex justify-content-center">
            <img class="logo" src="{{ asset('images/logo/image-23.png') }}">
        </div>
        <form class="form-payment" method="POST" action="{{ route('login') }}">
            <div class="row">
                @csrf
                <div class="col-md-6 pl-5 pr-5 left-side">
                    <h3 class="font-weight-bolder pt-5">Your Details </h3>
                    <br>
                    <div class="form-group-floating">
                        <input type="text" id="business_name" name="business_name" class="form-control" required>
                        <label class="form-control-placeholder required" for="business_name">Business Name</label>
                    </div>
                    <div class="form-group-floating">
                        <input type="text" id="point_of_contact" name="point_of_contact" class="form-control" required>
                        <label class="form-control-placeholder" for="point_of_contact">Point of Contact</label>
                    </div>
                    <div class="form-group-floating">
                        <input type="text" id="email" name="email" class="form-control" required>
                        <label class="form-control-placeholder" for="email">Email</label>
                    </div>
                    <div class="form-group-floating">
                        <input type="text" id="phone" name="phone" class="form-control" required>
                        <label class="form-control-placeholder" for="phone">Phone Number</label>
                    </div>
                </div>
                <div class="col-md-6 pl-5 pr-5">
                    <h3 class="font-weight-bolder pt-5">Payment Details </h3>
                    <br>
                    <div class="form-group-floating">
                        <input type="text" id="cardholder_name" name="cardholder_name" class="form-control" required>
                        <label class="form-control-placeholder required" for="cardholder_name">Cardholder Name</label>
                        <i class="fas fa-user"></i>
                    </div>
                    <div class="form-group-floating">
                        <input type="text" id="card_number" name="card_number" class="form-control" required>
                        <label class="form-control-placeholder required" for="card_number">Card Number</label>
                        <i class="fas fa-id-card"></i>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group-floating">
                                <input type="text" id="expiry_date" name="expiry_date" class="form-control" required>
                                <label class="form-control-placeholder required" for="expiry_date">Expiry Date</label>
                                <i class="fas fa-calendar-alt"></i>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group-floating">
                                <input type="text" id="cvc" name="cvc" class="form-control" required>
                                <label class="form-control-placeholder required" for="cvc">CVC</label>
                                <i class="fas fa-lock"></i>
                            </div>
                        </div>
                    </div>
                    <div class="form-group-floating">
                        <input type="text" id="postcode" name="postcode" class="form-control" required>
                        <label class="form-control-placeholder required" for="postcode">Billing Postcode</label>
                        <i class="fas fa-map-marker-alt"></i>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-center form-bottom">
                <div class="amount-due">Amount Due Today : <span class="total-amount">£500 + VAT = £600</span></div>

            </div>
            <div class="d-flex justify-content-center">
                <button class="btn btn-success" type="submit">PAY NOW</button>
            </div>
        </form>
    </div>

@endsection

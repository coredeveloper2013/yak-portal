<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('/images/logo-sm.png')}}">
    <link rel="stylesheet" href="{{ asset('/css/_v2/main.css') }}">

    <link rel="stylesheet" href="{{ asset('sbVendor/select2/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('sbVendor/toastr/toastr.min.css') }}">
    {{--bootstrap-datepicker--}}
    <link rel="stylesheet"
          href="{{ asset('sbVendor/bootstrap-datepicker-1.9.0-dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css">
    <style>
        .select2-selection__rendered, .select2-selection__arrow {
            margin-top: 4px;
        }

        .select2-selection.select2-selection--single {
            height: 38px;
        }

        .select2.select2-container {
            width: 100% !important;
        }

        .select2-container--default .select2-selection--single, .select2-container--default.select2-container--focus .select2-selection--multiple, .select2-container--default .select2-selection--multiple {
            border: 1px solid #d1d3e2;
        }
    </style>

    <title>@yield('title') - {{ config('app.name') }}</title>
    <style>
        .invalid-feedback {
            display: block !important;
        }
    </style>
    @yield('css')
</head>

<body id="page-top">


<!-- Page Wrapper -->
<div id="wrapper">
@include('_v2.layout.partials.sidebar-'.Auth()->user()->type)
{{--@include('_v2.layout.partials.sidebar')--}}

<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">


        @include('_v2.layout.partials.topbar')



        <!-- Begin Page Content -->
            {{--            <div class="@yield('layout','text-success')">Hello world</div>--}}

            <div id="app" class="@yield('layout','container')">

                @yield('content')

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        {{--        @include('_v2.layout.partials.footer')--}}

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <button class="btn btn-primary" type="submit">Logout</button>
                </form>
                {{--                <a class="btn btn-primary" href="{{ route('logout') }}">Logout</a>--}}
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/_v2/main.js') }}"></script>

<script src="{{ asset('sbVendor/select2/select2.min.js') }}"></script>
<script src="{{ asset('sbVendor/toastr/toastr.min.js') }}"></script>

{{--bootstrap-datepicker--}}
<script type="text/javascript"
        src="{{ asset('sbVendor/bootstrap-datepicker-1.9.0-dist/js/bootstrap-datepicker.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"
        integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ=="
        crossorigin="anonymous"></script>

<script src="https://unpkg.com/feather-icons"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.min.js"></script>
@yield('scripts')
@stack('scripts')
<script>
    $('.datepicker_jquery').datetimepicker({
        timepicker:false,
        format:'Y-m-d'
    });
    $('.timepicker').datetimepicker({
        datepicker:false,
        format:'H:i'
    });
    // $('.datepicker').datepicker ({
    //     format: 'yyyy-dd-mm',
    //     todayHighlight: 'TRUE',
    //     autoclose: true,
    // });

    function dateFormat(date) {
        return moment(date).format('DD-MMM-YYYY');
    }

    function timeFormat(time) {
        return moment(time, "HH:mm:ss").format("hh:mm A")
    }

    feather.replace();
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    $('.select2').select2();
    // toastr["success"]("My name is Inigo Montoya. You killed my father. Prepare to die!")

    @if(Session::has('success'))
        toastr["success"]("{{ Session::get('success') }}")
    @elseif(Session::has('warning'))
        toastr["warning"]("{{ Session::get('warning') }}")
    @elseif(Session::has('error'))
        toastr["error"]("{{ Session::get('error') }}")
    @endif
</script>
</body>

</html>

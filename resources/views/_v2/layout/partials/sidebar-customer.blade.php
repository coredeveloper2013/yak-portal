
<ul class="navbar-nav  sidebar sidebar-dark accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ url('/dashboard') }}">
        <div class="sidebar-brand-text">
            <img src="{{ asset('images/v2/image-23.png') }}" alt="" class="logo">
        </div>
    </a>
    @php
        $id                = Auth()->id();
        $gettingStarteds   = \App\Models\GettingStarted::count();
        $myGettingStarteds = \App\Models\GettingStartedHistory::where('user_id', $id)->count();
    @endphp
     {{-- Divider  --}}
    <hr class="sidebar-divider my-0">
    @if($gettingStarteds != $myGettingStarteds)
        <li class="nav-item {{ Route::currentRouteName() == '_v2.customer.getting-started' ? 'menu-active' : '' }}">
            <a class="nav-link" href="{{ url('/') }}">
                <i data-feather="pocket"></i>
                <span>Getting Started</span>
            </a>
        </li>
    {{--@else
        <script>
            window.location = "{{url('/dashboard')}}"
        </script>--}}
    @endif


    <li class="nav-item  {{ Route::currentRouteName() == '_v2.customer.dashboard' ? 'menu-active' : '' }}">
        <a class="nav-link" href="{{ url('/dashboard') }}">
            <i data-feather="menu"></i>
            <span>Dashboard</span>
        </a>
    </li>

    <li class="nav-item   {{ Route::currentRouteName() == '_v2.customer.leads.index' ? 'menu-active' : '' }}">
        <a class="nav-link" href="{{ url('/leads') }}">
            <i data-feather="file-text"></i>
            <span>Leads</span>
        </a>
    </li>

    <li class="nav-item   {{ Route::currentRouteName() == 'notifications' ? 'menu-active' : '' }}">
        <a class="nav-link" href="{{ url('/notifications') }}">
            <i data-feather="bell"></i>
            <span>Notifications</span>
        </a>
    </li>
    <li class="nav-item  {{ Route::currentRouteName() == 'appointments.index' ? 'menu-active' : '' }}">
        <a class="nav-link" href="{{ url('/appointments') }}">
            <i data-feather="calendar"></i>
            <span>Appointments</span>
        </a>
    </li>

    {{--<li class="nav-item  {{ Route::currentRouteName() == '_v2.customer.customer.live_chat' ? 'menu-active' : '' }}">
        <a class="nav-link" href="{{ url('/live_chat') }}">
            <i data-feather="message-circle"></i>
            <span>Live Chat</span>
        </a>
    </li>--}}

    <li class="nav-item {{ Route::currentRouteName() == '_v2.customer.staff-billing.index' ? 'menu-active' : '' }}">
        <a class="nav-link" href="{{ url('/staff-billing') }}">
            <i data-feather="dollar-sign"></i>
            <span>Billing</span>
        </a>
    </li>

    <li class="nav-item {{ Route::currentRouteName() == '_v2.customer.agreement.index' ? 'menu-active' : '' }}">
        <a class="nav-link" href="{{ url('agreement') }}">
            <i data-feather="minimize-2" class=""></i>
            <span class="">Agreements</span>
        </a>
    </li>

    <li class="nav-item {{ Route::currentRouteName() == '_v2.customer.my-profile.index' ? 'menu-active' : '' }}">
        <a class="nav-link" href="{{ url('/my-profile') }}">
            <i data-feather="user"></i>
            <span>My Profile</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

     {{-- Sidebar Toggler (Sidebar)  --}}
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>


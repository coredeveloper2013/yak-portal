<!-- Topbar -->
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow" id="topbar">
    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
    </button>
    <div class="title">
        <h3>@yield('top-bar-title')</h3>
    </div>

    <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">

    </form>

{{--<form class="mx-auto">
    <div class="form-group d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 search-input">
        <i data-feather="search" class="ml-3"></i>
        <input type="text" class="form-control" id="search-input" placeholder="Search">
    </div>
</form>--}}
{{--    <form  class="mx-auto">
        <div class="form-group search-input d-sm-inline-block my-auto">
            <i data-feather="search" class="ml-3 inline-block"></i>
            <input type="email" class="form-control inline-block" placeholder="Search" id="search-input">
        </div>

    </form>--}}

<!-- Topbar Navbar -->
    <ul class="navbar-nav  dropdown no-arrow ml-auto">
{{--        <li class="nav-item">--}}
{{--            <form class="mx-auto" style="margin-top: 10px;">--}}
{{--                <div class="form-group d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 search-input">--}}
{{--                    <i data-feather="search" class="ml-3"></i>--}}
{{--                    <input type="text" class="form-control" id="search-input" placeholder="Search">--}}
{{--                </div>--}}
{{--            </form>--}}
{{--        </li>--}}
        <!-- Nav Item - Alerts -->
        <li class="nav-item dropdown no-arrow ml-auto">
{{--            <a class="nav-link dropdown-toggle " href="#" id="alertsDropdown" role="button"--}}
{{--               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
{{--                --}}{{-- <i class="fas fa-bell fa-1x alert"></i> --}}
{{--                <i class="fas fa-bell bell"></i>--}}
{{--                <!-- Counter - Alerts -->--}}
{{--                --}}{{-- <span class="badge badge-danger badge-counter">3+</span> --}}
{{--            </a>--}}
            <!-- Dropdown - Alerts -->
            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                 aria-labelledby="alertsDropdown">
                <h6 class="dropdown-header">
                    Alerts Center
                </h6>

                <a class="dropdown-item d-flex align-items-center" href="#">
                    <div class="mr-3">
                        <div class="icon-circle bg-warning">
                            <i class="fas fa-exclamation-triangle text-white"></i>
                        </div>
                    </div>
                    <div>
                        <div class="small text-gray-500">December 2, 2019</div>
                        Spending Alert: We've noticed unusually high spending for your account.
                    </div>
                </a>
                <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
            </div>
        </li>

        <!-- Nav Item - Messages -->


        <!-- Nav Item - User Information -->
        <li class="nav-item ">


            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{--                <img class="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60">--}}
                <img src="{{ asset('images/user.jpg') }}" alt="" class="img-profile rounded-circle">
                <span class="ml-2 mr-1 d-none d-lg-inline text-gray-600 auth-user">{{ Auth::user()->name }}</span>
                <i class="fas fa-caret-down"></i>
            </a>
            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                 aria-labelledby="userDropdown">
                @if(Auth()->user()->type == 'customer')
                    <a class="dropdown-item" href="{{route('_v2.customer.my-profile.index')}}">
                        <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                        Profile
                    </a>
                    <div class="dropdown-divider"></div>
                @endif
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                    Logout
                </a>
            </div>
        </li>
    </ul>
</nav>
<!-- End of Topbar -->

<ul class="navbar-nav  sidebar sidebar-dark accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ url('/staff/home-2') }}">
        <div class="sidebar-brand-text">
            <img src="{{ asset('images/v2/image-23.png') }}" alt="" class="logo">
        </div>
    </a>
     {{-- Divider  --}}
    <hr class="sidebar-divider my-0">
    {{--<li class="nav-item {{ Route::currentRouteName() == '_v2.staff.dashboard' ? 'menu-active' : '' }}">
        <a class="nav-link" href="{{ url('/staff/dashboard') }}">
            <i data-feather="pocket"></i>
            <span>Getting Started</span></a>
    </li>--}}

    <li class="nav-item {{ Route::currentRouteName() == '_v2.staff.home-2' ? 'menu-active' : '' }}">
        <a class="nav-link" href="{{ url('/staff/home-2') }}">
            <i data-feather="home"></i>
            <span>Home</span>
        </a>
    </li>

    <li class="nav-item
        {{ Route::is([
                '_v2.staff.client.index',
                '_v2.staff.client.overview',
                '_v2.staff.client.billing',
                '_v2.staff.client.logs',
                '_v2.staff.client.viewLandingPage'
            ]) ? 'menu-active' : ''
        }}">
        <a class="nav-link" href="{{ url('/staff/client') }}">
            <i data-feather="users"></i>
            <span>Clients</span>
        </a>
    </li>

    <li class="nav-item {{ Route::currentRouteName() == '_v2.staff.tasks.index' ? 'menu-active' : '' }}">
        <a class="nav-link" href="{{ url('/staff/tasks') }}">
            <i data-feather="briefcase"></i>
            <span>Tasks</span>
        </a>
    </li>

    <li class="nav-item {{ Route::currentRouteName() == '_v2.staff.appointment-calender.index' ? 'menu-active' : '' }}">
        <a class="nav-link" href="{{ url('/staff/appointment-calender') }}">
            <i data-feather="calendar"></i>
            <span>Appointments</span>
        </a>
    </li>

    {{--<li class="nav-item  {{ Route::currentRouteName() == '_v2.staff.live_chat' ? 'menu-active' : '' }}">
        <a class="nav-link" href="{{ url('/staff/live_chat') }}">
            <i data-feather="message-circle"></i>
            <span>Live Chat</span>
        </a>
    </li>--}}

    <li class="nav-item {{ Route::currentRouteName() == '_v2.staff.billing.index' ? 'menu-active' : '' }}">
        <a class="nav-link" href="{{ url('/staff/billing') }}">
            <i data-feather="dollar-sign"></i>
            <span>Invoices</span>
        </a>
    </li>

    <li class="nav-item {{ Route::currentRouteName() == '_v2.staff.holiday.index' ? 'menu-active' : '' }}">
        <a class="nav-link" href="{{ url('/staff/holiday') }}">
            <i data-feather="coffee"></i>
            <span>Holidays</span>
        </a>
    </li>

    <li class="nav-item {{ Route::currentRouteName() == '_v2.staff.agreement.index' ? 'menu-active' : '' }}">
        <a class="nav-link" href="{{ url('/staff/agreement') }}">
            <i class="" data-feather="minimize-2"></i>
            <span class="">Agreements</span>
        </a>
    </li>

    <li class="nav-item {{ Route::currentRouteName() == '_v2.staff.report.index' ? 'menu-active' : '' }}">
        <a class="nav-link" href="{{ url('/staff/report') }}">
            <i data-feather="file-text"></i>
            <span>Reports</span>
        </a>
    </li>

    <li class="nav-item {{ Route::currentRouteName() == '_v2.staff.unbounce.index' ? 'menu-active' : '' }}">
        <a class="nav-link" href="{{ url('/staff/unbounce') }}">
            <i  data-feather="activity"></i>
            <span >Unbounce</span>
        </a>
    </li>

    {{--<li class="nav-item {{ Route::is(['_v2.staff.campaigns.index', '_v2.staff.leads.index']) ? 'menu-active' : '' }}">
        <a class="nav-link" href="{{ url('/staff/campaigns') }}">
            <i data-feather="hexagon"></i>
            <span>Campaign</span>
        </a>
    </li>--}}

    <li class="nav-item {{ Route::currentRouteName() == '_v2.staff.my-staff.index' ? 'menu-active' : '' }}">
        <a class="nav-link" href="{{ url('/staff/my-staff') }}">
            <i class="" data-feather="shield"></i>
            <span class="">Staff</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

     {{-- Sidebar Toggler (Sidebar)  --}}
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>


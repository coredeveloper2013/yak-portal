
<ul class="navbar-nav  sidebar sidebar-dark accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-text">
            <img src="images/v2/image-23.png" alt="" class="logo">
        </div>
    </a>
     {{-- Divider  --}}
    <hr class="sidebar-divider my-0">
    <li class="nav-item {{ Route::currentRouteName() == 'getting-started' ? 'menu-active' : '' }}">
        <a class="nav-link" href="{{ url('/') }}">
            <i data-feather="pocket"></i>
            <span>Getting Started</span></a>
    </li>


    <li class="nav-item  {{ Route::currentRouteName() == 'dashboard' ? 'menu-active' : '' }}">
        <a class="nav-link" href="{{ url('/dashboard') }}">
            <i data-feather="menu"></i>
            <span>Dashboard</span></a>
    </li>

    <li class="nav-item   {{ Route::currentRouteName() == 'leads' ? 'menu-active' : '' }}">
        <a class="nav-link" href="{{ url('/leads') }}">
            <i data-feather="file-text"></i>
            <span>Leads</span></a>
    </li>

    <li class="nav-item   {{ Route::currentRouteName() == 'new_notifications' ? 'menu-active' : '' }}">
        <a class="nav-link" href="{{ url('/notifications') }}">

            <i data-feather="bell"></i>
            <span>Notifications</span></a>
    </li>

    <li class="nav-item  {{ Route::currentRouteName() == 'appointments_list' ? 'menu-active' : '' }}">
        <a class="nav-link" href="{{ url('/appointments_list') }}">
            <i data-feather="calendar"></i>
            <span>Appointments</span></a>
    </li>

    <li class="nav-item  {{ Route::currentRouteName() == 'live_chat' ? 'menu-active' : '' }}">
        <a class="nav-link" href="{{ url('/live_chat') }}">
            <i data-feather="message-circle"></i>
            <span>Live Chat</span></a>
    </li>

    <li class="nav-item {{ Route::currentRouteName() == 'staff-billing' ? 'menu-active' : '' }}">
        <a class="nav-link" href="{{ url('/staff-billing') }}">
            <i data-feather="dollar-sign"></i>
            <span>Billing</span></a>
    </li>

    <li class="nav-item {{ Route::currentRouteName() == 'agreement.index' ? 'menu-active' : '' }}">
        <a class="nav-link" href="{{ url('#') }}">
            <i data-feather="minimize-2"></i>
            <span>Agreements</span></a>
    </li>

    <li class="nav-item {{ Route::currentRouteName() == 'my-profile.index' ? 'menu-active' : '' }}">
        <a class="nav-link" href="{{ url('/my-profile') }}">
            <i data-feather="user"></i>
            <span>My Profile</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

     {{-- Sidebar Toggler (Sidebar)  --}}
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>


<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Invoice</title>
    <style>
        .invoice-box {
            max-width: 800px;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, .15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-box table tr td:nth-child(2) {
            text-align: right;
        }

        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }

        .date{
            font-size: 25px;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 40px;
        }

        .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
        }

        .invoice-box table tr.details td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.item td{
            border-bottom: 1px solid #eee;
        }

        .invoice-box table tr.item.last td {
            border-bottom: none;
        }

        .invoice-box table tr.total td:nth-child(2) {
            border-top: 2px solid #eee;
            font-weight: bold;
        }

        @media only screen and (max-width: 600px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }
        }

        /** RTL **/
        .rtl {
            direction: rtl;
            font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        }

        .rtl table {
            text-align: right;
        }

        .rtl table tr td:nth-child(2) {
            text-align: left;
        }
    </style>
</head>

<body>
<div class="invoice-box">
    <table>
        <tr style="text-align: center">
            <td>
            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQwAAABKCAYAAABO+mpWAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAACzFSURBVHhe7Z13vBVFtu/v//e+d++8OzOOo2DAgGBETKMiKjqKCgYccxzJOQfJQUQFUXLOUZGggiRJgiCSRYJEQaKASBAQp976rt61T+0+tcNJ4HB6fT7rc053VXVXV631W6Gqe/+HiSiiiCLKkCLAiCiiiDKmCDAiiiiijCkCjIgiiihjygYYe/cdNqM+WGiGjp5fOHlUxBEXbh45fqFZu25nDBESKRtgrFi9zdx0T2tT8tamZ4WvjrGvrKD5qpubmOKlG0cccaHlEjc3NTeWbWmGCHD4KBtgfL1ii7n6tmbmkuvqK18aY3vssi0Llyc775b7zsOp2sGpypJxumtecm09c9HVdc31d7QwZcp3MH9/rEvEERcqvl/47kc6mRJiOIuL4ew/9PMYIiRSdsBYucVc97fmokQoklUoP1/K37jSxdgeC2u5hzmftK393+F4fR+H2zjHFiiSto0xYHHe5TVMlboDzNgJX5qvlm06A7w5B+xrn7+8xOHwsT2Xv+x7zmTsa//75vD4ueyrn8Vn59m//Oo7M2XaMvPI011NyVuamv7DIsBIykUFMP54aXXTot04s0w8rGPHT5wBPpkD9rX/d2ffcyZjX/tzlc/Os/985Bezedte81zl3jkEDFGYBMBIwekUMVl5SgDwcE7rW7bt0rW1gNGywwdmzdrvYyMRUUSFh3799bT5fud+83yVHAPGVgGMFqJkDQoNX3R1PfOnS2uY19t/YFaHAONf//qXckGQvba9fvg4U8pNmzNB+dUve538ut6Zpkz7fTafEcDYtn2feT7nHsZWc60AxiWiSIWFiwpg/FEAo0X78QoY7sRFHHFhYABj67Z95rnXepsStzQTwJgTQ4REigBDOAKMiAs7R4CRA44AI+LCznkCjCCHkZg0TM5uPkCOY6sS6dltl4x97UJ8na9u7JyzSpKc3RxGABgu2QH1kTvgmdZx6yU771K4TrhesvOW3PJwnWTnw+TWC9dNVQaFy906qcos5aTcR255butkWuYrh5LVCZ932VKy82FKVy/ZeUsuYJSMACMVR4ARPh8mt164bqoyKFzu1klVZikn5T5yy3NbJ9MyXzmUrE74vMuWkp0PU7p6yc5byj1grBTAuF0AA0U8x/gSh93zF10jgFFMAKND9lUSKDzY9th3zv0/2THknnPZUvgYcuulYreuJbfcZV9ZJudSHVtyy8McLk93HGZLvjIf++q651xy67gcLkt1bClcDrnn0nG4fqpjS+FyKNU5CMCIr5LcGgGGcgQY2dlXlsm5VMeW3PIwh8vTHYfZkq/Mx7667jmX3Douh8tSHVsKl0PuuXQcrp/q2FK4HEp1Dso9YMQ2bnndd4+L726Acs9nxKlChrShRAYcun6yvrI1/E+XVk8ISdwBTsWW3GO3PMyWfGUFwZZ8ZfnJOb1HuH74OL+5oK8Pn+17WPKVpWIoATB0H0YEGEn7GgFG3jmn9wjXDx/nNxf09eGzfQ9LvrJUDOUJMK7NcGv4ucLxd0miZdWICylnJT17mRI5fZckAgz/oEYc8bnKEWDkgCPAiLiwc54AI2kOI8z5kWc4U5yir+Echm9AI474XOYswLA5jEwBg+9h3B4CDFE2LPHF1/BlqoAvjrE9tucuuSaxHX8DSy7l0t62y8aUSZ142wxY+yRti5asay4sUdtceFXARUvW0f5YD0LrJwMMOX/RNQIYxQQwOkSAEXHh5DwBRiYhiVU4e3yxAAX7GRQ8SgYg4NYHEPiboKwOKxjBovww13Pbh5n7ABS0u+yGhubK2PcI+S7hZdc31PaEGtTxtXdZQxIBjBZJACNTCrc7k5wJ+doVNGdCydr5zuc3W/KVFSaOA0blnIYkHsBAkdngVOKWJubGu1qZ0mWz8/V3vm6K39xYle+8y2qavxavFXgdMaC47IZG5gapQ90bQ1yqzOv6HdFi1zcw519Ry/zl8pqmSIk62vZipx94IAosco/zr6hpzr+yln6894En3jJV6g40jVqOMjUaDDYVnu6q3+ekD3+RehZYaG+v5XI6wIB/+w3+LfY3kbPq/JbQ5kxxVr9+p/2D5d6nHU7XF8h3Pj/ZHbfTp9P36VzmfAWMC6+qY66+tZmp0XComT3vG7Pk603Z+IvFG8xns1aZ4WMXmE7vTDL3PfqmWvq/ilIXE7f/jr+3M+MnLTGLlmzM1pbvCX4+b62ZMPkr07P/TFOlzkBzg4DIBRJeJACGKD3nAK+Kz3QzXd792Hw6fYVZ/c33Ztv3+82OnQfMdvn77fqdZtacNaZbr2mm0kvvqwdCO4DPfS7L6QBj05Y95rOZq8yHk74yH00R/nip8uRPvzbTZ682y1duNQcPHlGhC7c9E7x9x4/ajw9l/Nz+TZL+TZuxSj87uG//YfPr6dPe9gXJPx74Wed42Oj55v1+0827Mie9+s8wYycs0nzZqVP+PkG+8/nJW7btNbPmfqNyN2/henPs2AlvvcLAeQCM7K+3kxfAYrd9Y4LZs/cnc+LEqWx8/PhJ89PhY2bXnkNm3YYfzLiJS8xTr/RQZUXRy1V4Qz9/x6SE2/4ifOTIL2b/jz+brdv3m8VLvzOtO30oINNewQql5hp4HXgotRsPl4leYzZu2m0OHTqqQufSaVGMwz8fN5u37jVzFnxrGrw+ytxWrl0QLsVDpazn09fbi9VIChgAw8vV+ikIlq/0tvBbyg89+bZ59Nl3zQtV+ph2b34k4LXS/LD7UKwXZ87NBWxfqz3Q3FcxsX/lpX8Vnwn617LjB2aCgAngYsl3rfxgLPXRo7+Y+YvWmbff/0Tvf8/Dncyt5dqYm+9pbW67r525X8by5Rr9Ta+Bs8zyVdvM4cPHC7RPPp42c6V4pEPNA493MU3bjFVQPdN9+L1wFmCQw8jhuyTX3i6AIVY8YDyM2uaGO1437btMNAdFQbnByZO/6v/bRMGx6jt/OGAO/XQs5tr9S8DjuBk4fI5+wpyQ4N4Knc2Gjbu0YwgUHzj9fueP2hYhZrJ++eVkrBfSj+WbJcQYrYATJDUDsGjQfKT54ssN2gfo6NET5oddB823AlKrxNPAu9gh1+WjppYWi4XjPZHbxcsBgAAfQCN4l8QmPXmXxA8YfHL9xrtamj9cVFVDHLymC4rX1pCI8Ou8y2poouiFqn3NmA+/1HufSff2A/F8bivX1vxP0armPAnn6Fe8f3IMF7+psXlSvK2Bw+cqmBaUN3RK5nefAP/H05aJRzrE3HR3Kxkv6YuMGyHnlTc21sQ4oSf9u+3+dqZZ27Hiua6Ny9aZ4hFjvzBlH+qkK2SPP99dZRjy1T3XOQEwcvryWTLAaNflo/ik4g0slDDk/b7TTe8BM1UQZ36+RkGAm1Nn0+Y9atkuK9VQJwalpgywoXMASu+BM02fwbPF3V9iVoilOSoeCDHuqVO/iku9zNz98BuqkHgGNRsN1dDHPiRWiU+xDx0537TpPME0ajXatBLPZIC4UwuXbFAAox60fPVW07z9WAEg8ShiOY1MAWPwyLliFduqkF9ZupE+ywNiwcuJRcdaogQoKGD05Mvvm2UyhidPnoq3BzzgE3IObwqlcgHFkj2259xjl13i+KOPvzZ3le+ouZ/LSzUydz7Y3jwo/cMj+pv0r/hNTbRvhGX8/sTipZvUA7DXs7kO5oX+nZSxTwV4lvifeoAPXh5/Dx46YmZKOPjgE2+pRwjAkoy+r2JnU6XuIFOv2QjzUrW+Mq+dFED+LKEgc1u9wRAzX8ICxs3e25J77zBbCp/nGsjQL7/IeIeexxLg/vfHg34+88+e4h0e1PPudeDgWqcDjzjWP/d6Lluyx9RD5oO29CN7G1vO+GcC5LY/PFu6NmEK2gZt3GcocMBYt/EH0/6tieZ/L6mmwkidW+5trQCxd99P2gE69IEAwb0Sjtwu4YUFDMIScgz80hieA8p2hYDKky++J97DenMkJsyrvtkuIDDG/NdfXzNly3cwn0xfoe3tQ0785GsJe3oavmmBFUVhEFCSouQ4xk34UgcG4u/chetMpRfeU8VHSHMCGLeKBacdYEGIsnHzbvPNtzs0BubnCW6+u7Xe/3oZp7YSnmDFbXvGgU+5f7dlj47Bnr2HEp7Dkj2259xjl11CAAGMMgIYWErGecTYBWbDpl3av7kL1kn/xqsHQv9YTSJE2yHW1F7PgsWW7fu0f9t37Nf+uvd02RL/82zHxTMkFP1Znpl8xQtV+5g/C8j/Wcb0NglD2N+yVDxGPC9CV+SDcavbdIS5LJboBnQJNb+Xe1sFsOTeO8yWwufxYH/YdUDHYMcPP2ofbZmlTAHDPt868ZA3bd0jRi25B2nJHlOPPA76smnLXgVktz6Md71BwuvNW/clyE0yxhPfJX39Zu0ODbsxnL56cJh0XHYfUD1280e5BwzPTk8UmhyGBQyIPAUhigWMIiXriOWuY+577E0zQ8AACwZNlTjxCQEC4ld+r9ECBlbIAkYAGrUEqJqbNqJsu2VyIMKVnv1miKtdxbQTD4J7Qgjeoq82msfEjbzyxkY64Ylc21wuocxD/3jHzJm/VjyNo+q1bJLBJemGhwGo2NCkqABGqqTn4BECGPe2ETCqpc9HjoWJR/jxYrbIdbGehE+X39DIvFqzv1paVgdImI4c94V4R8PM4y90N4881VUtbNcen5oVq7ebGeKVvfXeJ6bj25PMgOGf6w8ptREvqW2nCeLWLzc/xbwkywgXY9ei7TjTXBjQ7DN4loIycwGgTZu1Kt4/2tOHhi1G6ViR+GXcEDQIpeIa9STUe+LF7qa8jBmhS0MBFcAewWTOIK63RsK+t9//1HSQ/o4Yt1D7X7f5cNO644fqEeIxEv4AGDcJiHbrOdVs+G63GoHAugYvOjFuK8Tra9p6jLlKPKD/LlJZDE4b02/IbL3P5KnL5B4TTeduUzSZzvNYIke1X5SQe7WUOeMvXh3jQxleaKeukzSH9sATXUyll95T40Yoe+DgEbN2/Q7T6Z3J5ulXeyjAIwu33tvW1BIPdqQ803diDEgQM4eEVq06faCKVEGMEKELyf9BIhNrBIzc/NkeAcLJMgatxWj2GjBDwZN6VesN0nl/9Ll31QNGfhgPQAgvu7LITsVn3zWVRE/qyzx88tkK6SeGORG8Doock7trL3qIoWR1sOKz3UxN6Q/XWS+gxPNbYo77DJxlmrYZo7+Xypi+23uqeVFCZ6IBEvX22nHAyI+t4T7AWC9oi5AjGDaZiJXnd1k/nPKVWhsIgXr61V7ZAAMLw1Kqti1ZT61MMbH4TAbWDyKmRFmLXd9QV0NsjoSBaNhylLra9M0qvmWOAbErxD2vXHuA6d5nmhkkIdN7faarS4wrTJ04YLBKkmJruAsY9z/eRYRzi/QuEb1biZBcI8+Dt/PMP3tJyHbYfCmuf8e3JokV66KhAmAGWHH/v0nsXktABCEtU769KSNhRC2xsMPGzjdlH+6oXz1rLCEWiWK3L/z2LT+HULx0E83JsPqAsNzzSAAY/GUly/bPtnunx1Rd1i5yVV0VNBLGeHAAKL92Ff99TRlTvBCWqwlrunT/2Kxas13DlMOHj4nyrtbnAQwQcn7Hgj0vj8n/GJDajYeKt1ZdPT08CPJH1ssLE5Z7CQluGbuXq/cV0BphRolgIx8o+O33t5XQpaPp1muqKpglZAhj8rQoDWOOdwDo4dbP/eJb6cMwBR+eg9/pvVy811JlWqoHQ71ZMj73C/BfI3KOYSGngofDSiD5FABnw3e7dGzwevk+DPuKipRgX08dNQz3yjg3azNW5RvQAAwBGnSC+uTuqtYbrCEgc48HzjI/AEVujh/+fk/C+TIPdlA5xWgiG/Thsee66wrXnr2Hde4IITCiA8XqPyfjzdYG+s1cXSFGoIQYXkI85GWBeOjME8QvmtmNWIDVUxIu/01Ca4zamA8Xmb1ifKx85AEwsq+SXFC8jj5o2MPo4IQkPCzKh1WfJ64/k05HQEtWEnCJ167PCklmClLSsSJ4J7AMWKk7W6pFYSUGYgK69/5MwwHQHMHDpcJS3CgCgMIH983qK4zSXny1AIcwA0uylIGiDzeVba1ucBbAJP4uie+HjIaMnKd9CAOGDrT06ccDR9Qil5AJZDJeqdlPXWF+GIl+0g5lJBv/j5d7qLXBol0lSk+oxy5VJp69JCw74yUQ26OwLPnJbZR5fsbzwUpvm/88/5/al+mfrzbjJy7WNmHAoH94Vngl5Hiuua25gvPjIpBfLN5o3hbP5nYBLhThLhHcynUGmvrNRqqHVPahjqr09O8d8SgIp/BWJonClRLgIdkLUADmV5RqbGo0GKICy7MRGgH+uPx25cFH9A9lIFFtl9uRK1xuxpP5wVshxHOT2MgQXl25Cp31mctV7GxGiwKwStdMrCkeC4CKxUapK734vgICikYfZ4uxqt1kmAIfCoocAaZPSLjaS7wVvOLeg2apzNtwCYWkXPf43P66GioUFc9ypXiKeEX0vYl4TBgfZIo5ZXXuoSffMQ+LXlwr48/12LN036OdNT+HMj/8j65xmUCX/vfiarrqhZzzrHhFY+X5eF7C6NLyHKw8NRCvsVr9ISpXeI8YIp6LZXTAeMGi9Qpc9EcNlbS9SsaT3BZe2wHxoCxxn6zX23MUkmQGGBvFzcSVppwJAlUR8De7T4nvSQDxh49ZoELJEqmbw5i74Fu1AoQlWAEUGoFFaIgREaaFIkRYC1B64+Y9el+Ukz0GuJFMSrAjNKuvsH7PU/8PdoQyuWTqYZ7Feha2fhZgBB6GS/RjsAMY98pEY8WYRGJTkrwoMULBfa4TT6xRq1FqDcgnoHRYN1YMps1YqS49ytFDQi2WQdnV+leETwQHC8s1a4mVxuow5ljtkycDC4ZC9Bk0W2NMci4d3pmkuRSsJpZKQ5LyHdUttv3bScgh44XV4vkR/joiVO/L/cuJ4DAeD0gcD+jg0WHJt4r1JjRiZYu8x90CQkPEIloXHeBF8OkjdTqIF4Xn0anrZJnr9lrGvhvm0nXZM6EARE6bJqLoV4rSI4uEDyy7W0KGsIYAJ2PH2I+fuES9OpaVeSaM1OSpyzUXg6zVEjkiEcsYrxUwB8gGyByhhBirShIuMveM2xh5dp6ZUBgrXq3+YB0fPDLmjjkpKwDCeDIGjVuO0XHG68bo8Pz0i7Ho1nOahM/fmc8lNAYE8dSRE1aI0JnOMmaAN+DwhvwfgHENBSMWEtA3PDw8IsL2Ox7ooEDPggJhv65IifxVrjNAvR9W8HqI58Lq5UK57kMCRhfK/QCiO0VG6rcYacZ9tFjk9kBCmFfggPHTz8fUiiKMuE/EVmvFUmCFsGpMPOXEzsTNWD4LGPDuvYd03wIWa6ooEsJFKALIoBz8HTX+C83yP/78exqHQ3v3HTajP1ikA5AJYFimnjL/x88H9TMBDFZJAAys9POVeymQ4XLiMZDwBPkRgnICKLibz0odvJsSNzcVQRlt9op7ifDi2vNsCCx7RJhE0B/QRZjxoFjuu+OB9urCPinWceOmXZqUXLpss1pyfjj6suvrC1As1dUqBQwBCsJDBO1JidlZUaoq1o9YF2DGuiDg7IMYOma+xOS9VNjvFk+CpC6rU8TtjD3zgyDOEO8F0CMx3PD1kbo5jr0LNoGKVSQBjJJh0Vgxw1LT7wpPdxNvbYeOX04oL4DBmGJRkdd7K7xpBgydIwDIb4ee1D7u3HVQ98mwusA1kCO8DADjmVd7KviTByBJjJIB9g1ajIgZMZk7mQPmDuDuKx4Iv3Z+vowp4SXhNoCBV2kBg1CK0IY2x46d1BzHsxKuMp4k+V+tRa7rqJajvNyndpPhmixGvrtKGEluhhwVy9KENlyT3Eew0kbO5jf1bpAlZPH/FnnNPCNhBccWMAgR8bL7SJ9JuOtcS3t3bjjOHWCw0/P25ro6Yn/YGER0cxg6qXIDbozAkoBiEHloyoLM8BHd2XeXCCQTgiXCZbMdZfCZRNrzl/0UdicigzBfwhpNJIrLi0tpk3RYEdAeJUPRVfGln0wCbAHC9h2mXpwFvHSLuFue5uUzAAOhABB4iU6TmzLhxW4IwAYFRoFY2nyj22QzX1xBLBDLmCgViM4zudfkmLgUlxL3GQEiv8IYYj1wSfGEyFOQrMLy89xsxuK6jz7bTQGaZT4A4y4JSegfbXBNES76yfPavRh4SeRpyCvgDdJ3wKSmeDTkOAL+VJncBcJfumxLvd9L1ftqfgsQ4TrE5K+JVSMXYlcM+g6eLYDUJgBOmW92wIaf28cucUyYQqKOcIQVOwsYtj4yxC5N9SakH4wJY4xs1hGFw4u6SMLRcgIaLLWz45dEMR6U2x/ieAsYTwtgsEpCvWcl7mdO2W0MQFqZd3mxeA3VGwxWI4IckGhmW0DrNz7U56fvn81epXphn4uEJECO7KgBlpCesZNS5a2isOQ1AByuwXOzg9eGOeRkXqnRz3QRDyNrvqYqsBB6sRkOLxPvCGNiAeMPF1UxFZ7paj6dsUL7EH4WmDGNr5JoDiO3gCHMpIQBgwflJqCjRUjiTFwzVjDIXBPvFRNLiBAjQNbDoD0Th7ArS1usLyADqEwRV7Ju0+G6WQqhJi9Cpp12h8WzmS6TEwhFkLi0YKGAYVmAgDLq8H+whNpAlckChb74JnXTA8ZcAYy26u5hqUnWMh5YUyw2MfTL1fvJM89SK7F0+RY9z6S/JmEWe0XC14RJDNskGftDeGasBRaQcI/n1xi+8wRN8nUWMOKeeC2du05R8OQ6CAc5BwSNZyJhiUASK7NxirFnUxmKg+vLMxI/MzbEtCyH48n5mNwL4IBCzZn/rSoUgEFISLy8Wyy2fZ7h4hkhrH8WZSMZieeYyTIhYMiOXZLaPx85LsbkVAAYcm/GJiVgiDHD0xgrgEHuAwUDNPCMyK0Q8jIXJP8AQhKayCorWHHAEFfeAgbb6x8WkGfuKAMQfVvq8XhZ8aAeoQJeIftbyBXhXTD2ADNzaYl2hATkG3QFSeYDL8Fec5eM5ZBR8/WZuC6JX7x3PFm8R2QXmcB4sf8H1nmSY4Af7xcZZX54NuaaZyG0IjFMWGTvFeYEwBD5yQFghPdhSEjiAYwjR4/roNGJuV+sUxcIBGPTFDF4aRHUS0RhaQsCu4DBBPwows7kkSClPdlmrATCgVXGRWfgUGwGAKU7ceKkeiYrV2/TJBLKi3VwAUFZw4wgCcvqBu4xS4UsiREL4rXEQUPqY4XTAYZu3JLJQGGJh0n4ouzEk0wyS2V4Swg/oGEBgyU1QDh8TRiA5RqMrQUM8jsIEYJCZhthrijAO0lCv5eq9ZH+1pcwopMmNu3mKwAD7wahYoWLXBD9Y+nzTfEUSNoiPLv3/qRe4XIZPzw/8ick4gA83PryDnPMPDwhY8aeFsaGnNJ0sZoIJ+FdHekvQk4fIACCpUyWqAGvLt0/0f0F7jO7jCzs2/+TJiFZwp0ybbnu1zguxsMCBs+D4gCubjsAg2QfYZYCxoTFWkY4wzWw1K/U6K+rGSgo9QB6xhjFIDSM78MIAcYjT72jigcgJQeMvXFvAsAYNmaBvhPV5o0JKrckWDEcbNayRDvyGHgKyEf33tkBY9joBQLQdeKAgfeIDJEEJ4QkhMWj4plh+sgrCg9Lnx8TeUFmXq01QD0eVkw4zw5gvCZ01N4rzImAkZschigSjEJdIA8QzmEEKxjT9OI33tVaB8hmnBFc2lgLDzICGGx8oWN4EwsWrVM3unTZ1qZUmVaaLMSNVndM2K660B5mHZn4C0Ixh4qSomhMDvexAKAswkwMCujwghpgw6YYXEaSkQy8bWMBw32XJExZqyQ1Y6skW2MlWWQHnlh+5ZptgvpB8g+lQxms224JQSEpWbnOIBVoMuouYOBRNWs7Tp8dxXns2e46L+RFXha3lHidsYSCpGdHcUerx1dJpDdaFib6t27jTk24sjoCkLJkjWJwT5cJAzdv3SNzvUfzSwDOJ58tN7dI2OEDjGBlaLyOEwKOMBNOBTstg/GB+Mt4HDx4VJOr9Jl5RIkAUPIOLG+ScMQ9bi7/s1Ro23M9+ks7xpj7jBXl5x4AgeYahPFYF4lb3qT1WHOzyBmJYgzIRwIKeDI+wGD14GkBPfIIeDez53+jYUVC34UJz8g3sFqEFz1tpoQkq7er15FTwLBkAQPdATBIJgPC9ZqNNP9dpIp6E/2HztFnX7/R8i453qWrWJuFmastovj0WUMSAX4XMFyiP/a5kKW8JT3TAAbKz+T+ScCBl7cUHBI4UF4vYIhS2H0YtA02UREa8H/AVvn5/7zLa0kM3U8RHEIoSH62FjRnmRSAYuKIJ8klEO/hErMZiUQdlhghIsvdov04tQjsvSgIwPj1199UuUBqro3Q1202QndPkvexBOix2oMQFBXFBdxcwECxcfFxrXFxATkEEWBghYVxtJPtAgYhAfsMkgEG7dgAxNIp/SNH0nPAjHhoaZndmGzEYpMWPFPmi7lPBRj0nc1m91R4I1haFNBnr8So8YvMfgE4qxyABQlGlAPvhnkgeYgnM0+8TZSMMCxIvtfXJB5L7YSxEDmvT0WRkEkUGyuK8tNnQgPe/eG1BTw4+kSuiP09yChLkhgbQiUfYGio0XF8sJlQvOPqDYeo4ltw5lnJ2bFHg7EjwYtsrPn2e02Y8s5SfgIGniurJcgeS9isRAHi7lwRpmMU3xKPDg9n8tSvdXzPCmDAyQCDB8L9LEICMgYUugLhtAUQkgEGsRJts9oDEtIOjrXnfHD/FrprEFeUh0R4vlm3UxNtJEcffOJtXb5lffvFakG8jmIDLtRnkwqrLmS2dYVFruveIylgSNvsgBGEGC7ZwYcRxmGOstO2caus3Xa8Ndq111TdORgAcrCs6gIG12GCeZZgHV2ESACRTXCEgS5lAwwZ23D/LHE+cMcX6bIq9yfXxDPOWbBWBY/dsX0Hz9KlSfaRsBOR/R5YLcIGEps+wKDfWDfyVywlI/R4jYQ4rECxjMlGM7L1vB3KfgDGnoQsfSEHhFcAoFCPd3UwADeVbaX7c/BGeFau/3zlPjp3AAYbzyjjpUNWO1jSxY1nDwtb5FlWZTMd4St7HT6aslTBxAcYhGx4hCzl/89FVTWfhyfB5ji24LMag6GkHKODDLcWcNu956B4ZbtM83bjUwIG2/KvEMAgh/FuGsBA+Ulss6+CPB66xJgQtuPRIIck2FmV412c0ne11MUFwioM6pkBDJKe7k5PiZnDOQwIV6iTDBzWPe4RoIC2XYwJLS4VwNBVEgsYEpLwduLVbCQCKByPIoHl3gAQdVBychEIHd+9sA9K5puXllgiAzz4HgeJOQTXhgH79rP0uFQzzPZ+7nc2gu9hJH+9nTCGZJPNQBMjcx4K14XxJMh441YjOAjA5RJyIGTshyD/ApiQkMPtJanIikaNRkNVYK0QYe0ASTww6hC6kEvAeljifggIOZb/c+FruhzLSoa9RrhvMC719zt+VCVkfwygUaZ8BwGnPqaaCB7vgpATQWjx4LBczDdJSRRWl07FI2BvCW8Ku9fGM9ooisM3UcgXMfd4D4RVdz7QQQGbvmJlkR3uzc5Tcg68zWxp+aqtunRNeEk+gXFkZYcQj1CEPrDsypywlMr8E6bxLRUMFOPNWONR4p0yznhpbHhauWa7Jj7ZBs61ACU2ZZGwh1B8DA7ACODTV8YDgCdfwC5cwjl2irJH4yuRB2SahD2GQT1cKSOXhQdgifEBZJE5DDBJWJ0nOQ8zloNHzNPQ6f/JczVvN07CjT3qUbFSh/xcEduAxhIquULN04hu0UeYbeIAFUvJCxZtMPc/2sX8519eNf94qUcQquqtEuUBRi9tDqPAtoZnAwwPu4CxXibCAgZWMhPA4C/LoFiHoiXr6a64IaPmqTUgwcj1fIQHwooDngUbmV4UJSD+B/zC90n5AR25FgktBIa6CCyeC2VQQt0YWyJrjiXFmvAMCB9eAgpAyETmHgFnjZzvqDZsOToBMPh/+uw1ppSAFct8uPq8l+JuhuJ+eC1Ya3aMsgmLsU0FGJbIObzV/WMFDcaXsaGPzDeKSnLz3Z7TFAAgPAxeACwjis9SMO71LrGs4evrVua9hxTcyc4jOwCDvjck9ygqf1FqVhLYJzJKlN39TgeEjOA14OVQFy8UAMPLYzXgGfG0HhVAQEkIZfAKWJFgleQ58T6KCxirzCjYBvcDFAhZkAvGZ9zExZpQvlKAhPcreG8Gov+AdVcBa3Z4Aj7sRmbeYOQVpUX52cRnCaDhOy7IOysX6FIYMJq2HWOuF/BiDAktXcAgdALEiomcImvtukzU5Coyjt5hEDGawYepmKegPzwnIQ4rMMgvXirEhjHG6AKRuVcENAn3glslzhdcIIBBCGIBg0QLFioTwGCyCElIomH1QXc+rpIpYCiLAAMYDA5uMm4hu+5YjvMR+zpQiF4DZqmyB1Y6WFEJczrAII7sKorDm7i95XrkIyiDEurG2BKWnDdAx01YbOo0GaFuNK4xIRSbnBCweSJwXXt+qqsauNDB69PBNRA2VpHYKPVHCTfYtEVf3HvwP7kZroc17dF/hgJzJoDB/6xS4JE1aT1aP2hD/7BavBuD8vHinm2DorEZi92LfDAIz8aW+5i5ZkMTOQPen2BvCQnbKnUGGRKZgwX48XRYDqW+SxzzTRW8R1ZMXqjSV/MhJIl5VvoBSPCCGqGMBXGe+0sZI6w3eRos64tV++kLW/MWfhu/F4xLT3iE608feRZ7b5i5YPUOWassfQfg6Qf7ZfBoABU7zhAbswjZAA0+HET+DPCxxDXJW3UU74v9E4QILmAAyISEtGf82diI12T7w7UIMwD5V2sOME+93FM9Jj5NSYi5T8IQu88EQt/6Dv5cPV1CHZKlwa2y5shyHgBjq7pc7gd6UXr2A5BcIqnDcl0DQTNiR6wGyaSs+ixrZrXVMrEubILhhTFQk4QWrhwrA5Rnbx9jaeceo9zU5S8uHy5vnabDVID4nsaUacvMRxJ6sLTLCkNFQWNcVwAr+DK5XJ9rhq4LYAXLqv4fYybexaPBCu7eE3xxjEHOhKjLagghkq5ECCNo7NZDeMl3cG3CrCB+DyYQAlDefHeKWP5g1ah7n8+0XpjwRLgeQMZfwoIMu6feCoKKcJNhp3/85VkJQVhStAQAYL3oL1uL6S+ClmosABmEni+p8dxcm7/sK8H7IxntKp1LCD/jQ9+CVZxd8bHD5eb+unojc4K3aUnnS9x7fR5pg1vPuHEt9160YeUH0KIvYW+V52LnMvfgvvQBZi5JWrt7LCAUmrdMuR73D8sJ/xNecD3KGXe3nLFmvwntGX+Mc9ib5NnYsZr1bLt1bOkPdd3r0T+SxYwfc4BcJCOePZc5jOy/3o6C8pdtx7jPxHZ8uJcNRCihLfcxZTAxOm1wz+HrJIbTjVaxcl9bH1MXBce15S/eEO9RkBfgVV9AjFe9SboBBnhH1oPxXQ/Gy7Hfw/C9fHY2COUkF8GyJxtvWE0g0YUCRhRRflMCYORoH4YARnjjFqsWAANWjhhX41xRWBI/VuET62cBgWWUFuW17RPDg+Ae7upIwnV854QD4AgyysS2lskTBEDh9A92r6PXCpjruB/QgUBql11Kdh5KVuaeT1VuCfeYb0MQK/M8uOW81WnJvY7bzlK68jBlUtetE67nO5cbcq/vu6bvHOTW99XxncsrJbumPZ/T+yVrl+y8pVRlPnKvZ9tZwAhCkpyukrA13FW0MLu5Bfec5XBZTth3jXTXTnY+zO51nDYpcxhniXE52UXKa+uEXrzkR4zqqxtxxHllACPIYeTmZwYiwDjrzE5E4m4SmJu27Nb4m0n11Y044rxy/gJGWCHdY1cBU3Gy9uHjcH1f3XTs1s+gTbpP9J0N5q1C3eJ8gg+9nop/MiDiiAuC8wAY2ZOeZ52vddhXnkfOSnr6v7gVUUTnOtkcRq6Snr87wChgdgHDt6waUUTnOuUeMFZsMdfpxq2sVYTccpZnIK5/nLPOJ4QNueSsa2XncJk9TqzPakvdhC9u+Vy2MEO+82eaXfKdd89lyrlt93vnMPnqFARneq9M6/k4GfnqhhmKhySvCWDk7OWzCDB8gxpmyHf+TLNLvvPuuUw5t+1+7xwmX52C4EzvlWk9HycjX90wQ/kAGH4FzQln7d70n3fP5ZZTXStcZo/D9QPAqJ4jwPi9sEu+8+65TDm37f4d2CVf+dnk/OyTJV9ZmKFEwCDpmYN9GLwIZT95F1cye+yyLXPL3XMuh8uTtXc5VRnsK3fP2f+THcc4/Ik+KNnAWvKVh9lXL9k5yHc+1bE9Z8l3Pnwu2XFOyL1GuL2vDLbkK0vG4frusaVwORQ+hnznIHs+U3Ypk3Pusf3fd5zpOUvuOR9bSnfOno8Dhm4NzxFghJKe1nWX/7NYLLMqoOPma/1AGbPcfYdjbbPqJ1r58LWV7bVsW+UsRQ9fW9k9Z/93jt3722uEd3qGB9NSsvNQsvMFQW4/kt0zVZ1wWbjcR+nqpyqD0pX7KK/X89Vxz7nnw5SuTrr2YcqP67nXSFc/XXmYEpKeeXqXBGWDUbg4BwodVz5Y68eU0NazbeHYuaz6KKvDtk3s2r8XwLCDHj6XjMOUqk6qMku+Oj52Ka/lYfLVh11KVe4r83GYUtVJVWbJV8fHLuW13EeZtElXDoXrhNmlVOW+sggw3P+dY/f+9hoRYGS/r0u++rBLqcp9ZT4OU6o6qcos+er42KW8lvsokzbpyqFwnTC7lKrcV5Z7wPB8DyM1W6X0lfnYVz9de9smXb1knKptDDBSJD2h8LlkHKZUdVKVWfLV8bFL6cotpSu35NZz2aVU5b4yH4cpVZ1UZZZ8dXzsUl7LfZRJm3TlULhOmMOUrDx8Ho4AI4FTtc0OGJA7mOcqnevPF1HmlAfAkJBEP6DjuO/nOAMYf2QfhoQk0dbwiAojRYCRA7aAwWfR+Fx8RBEVNgIw+BIYn5Es4JDk359141axGqZ+8xH6iX0+QXcuMh+5ddlXxzLlCBBfZfeVR3xuMZ8F/Gr5JlPppfdjb6vmADD4gaGE1YpznPlOKF8Q48ds+dAuH/stzMwHfvkxZn6qD6/LVyfic4v5ke66zYab2+5roz9/0X9Ihq+38/VlfjOB38AobFy8dCP9cWK+XVqYGYHhF9v4hH80HoWDmWe4eOmGGmEMGj43hgiJlA0w+JEXfgyHr20XNgYo2YMScQsRmoB9ZRGfu8zPifCzFvx+i4+yAUZEEUUUUTKKACOiiCLKmCLAiCiiiDKmCDAiiiiijCkCjIgiiihDMub/A5/TmMUvYy3KAAAAAElFTkSuQmCC" style="url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQwAAABKCAYAAABO+mpWAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAACzFSURBVHhe7Z13vBVFtu/v//e+d++8OzOOo2DAgGBETKMiKjqKCgYccxzJOQfJQUQFUXLOUZGggiRJgiCSRYJEQaKASBAQp976rt61T+0+tcNJ4HB6fT7rc053VXVXV631W6Gqe/+HiSiiiCLKkCLAiCiiiDKmCDAiiiiijCkCjIgiiihjygYYe/cdNqM+WGiGjp5fOHlUxBEXbh45fqFZu25nDBESKRtgrFi9zdx0T2tT8tamZ4WvjrGvrKD5qpubmOKlG0cccaHlEjc3NTeWbWmGCHD4KBtgfL1ii7n6tmbmkuvqK18aY3vssi0Llyc775b7zsOp2sGpypJxumtecm09c9HVdc31d7QwZcp3MH9/rEvEERcqvl/47kc6mRJiOIuL4ew/9PMYIiRSdsBYucVc97fmokQoklUoP1/K37jSxdgeC2u5hzmftK393+F4fR+H2zjHFiiSto0xYHHe5TVMlboDzNgJX5qvlm06A7w5B+xrn7+8xOHwsT2Xv+x7zmTsa//75vD4ueyrn8Vn59m//Oo7M2XaMvPI011NyVuamv7DIsBIykUFMP54aXXTot04s0w8rGPHT5wBPpkD9rX/d2ffcyZjX/tzlc/Os/985Bezedte81zl3jkEDFGYBMBIwekUMVl5SgDwcE7rW7bt0rW1gNGywwdmzdrvYyMRUUSFh3799bT5fud+83yVHAPGVgGMFqJkDQoNX3R1PfOnS2uY19t/YFaHAONf//qXckGQvba9fvg4U8pNmzNB+dUve538ut6Zpkz7fTafEcDYtn2feT7nHsZWc60AxiWiSIWFiwpg/FEAo0X78QoY7sRFHHFhYABj67Z95rnXepsStzQTwJgTQ4REigBDOAKMiAs7R4CRA44AI+LCznkCjCCHkZg0TM5uPkCOY6sS6dltl4x97UJ8na9u7JyzSpKc3RxGABgu2QH1kTvgmdZx6yU771K4TrhesvOW3PJwnWTnw+TWC9dNVQaFy906qcos5aTcR255butkWuYrh5LVCZ932VKy82FKVy/ZeUsuYJSMACMVR4ARPh8mt164bqoyKFzu1klVZikn5T5yy3NbJ9MyXzmUrE74vMuWkp0PU7p6yc5byj1grBTAuF0AA0U8x/gSh93zF10jgFFMAKND9lUSKDzY9th3zv0/2THknnPZUvgYcuulYreuJbfcZV9ZJudSHVtyy8McLk93HGZLvjIf++q651xy67gcLkt1bClcDrnn0nG4fqpjS+FyKNU5CMCIr5LcGgGGcgQY2dlXlsm5VMeW3PIwh8vTHYfZkq/Mx7667jmX3Douh8tSHVsKl0PuuXQcrp/q2FK4HEp1Dso9YMQ2bnndd4+L726Acs9nxKlChrShRAYcun6yvrI1/E+XVk8ISdwBTsWW3GO3PMyWfGUFwZZ8ZfnJOb1HuH74OL+5oK8Pn+17WPKVpWIoATB0H0YEGEn7GgFG3jmn9wjXDx/nNxf09eGzfQ9LvrJUDOUJMK7NcGv4ucLxd0miZdWICylnJT17mRI5fZckAgz/oEYc8bnKEWDkgCPAiLiwc54AI2kOI8z5kWc4U5yir+Echm9AI474XOYswLA5jEwBg+9h3B4CDFE2LPHF1/BlqoAvjrE9tucuuSaxHX8DSy7l0t62y8aUSZ142wxY+yRti5asay4sUdtceFXARUvW0f5YD0LrJwMMOX/RNQIYxQQwOkSAEXHh5DwBRiYhiVU4e3yxAAX7GRQ8SgYg4NYHEPiboKwOKxjBovww13Pbh5n7ABS0u+yGhubK2PcI+S7hZdc31PaEGtTxtXdZQxIBjBZJACNTCrc7k5wJ+doVNGdCydr5zuc3W/KVFSaOA0blnIYkHsBAkdngVOKWJubGu1qZ0mWz8/V3vm6K39xYle+8y2qavxavFXgdMaC47IZG5gapQ90bQ1yqzOv6HdFi1zcw519Ry/zl8pqmSIk62vZipx94IAosco/zr6hpzr+yln6894En3jJV6g40jVqOMjUaDDYVnu6q3+ekD3+RehZYaG+v5XI6wIB/+w3+LfY3kbPq/JbQ5kxxVr9+p/2D5d6nHU7XF8h3Pj/ZHbfTp9P36VzmfAWMC6+qY66+tZmp0XComT3vG7Pk603Z+IvFG8xns1aZ4WMXmE7vTDL3PfqmWvq/ilIXE7f/jr+3M+MnLTGLlmzM1pbvCX4+b62ZMPkr07P/TFOlzkBzg4DIBRJeJACGKD3nAK+Kz3QzXd792Hw6fYVZ/c33Ztv3+82OnQfMdvn77fqdZtacNaZbr2mm0kvvqwdCO4DPfS7L6QBj05Y95rOZq8yHk74yH00R/nip8uRPvzbTZ682y1duNQcPHlGhC7c9E7x9x4/ajw9l/Nz+TZL+TZuxSj87uG//YfPr6dPe9gXJPx74Wed42Oj55v1+0827Mie9+s8wYycs0nzZqVP+PkG+8/nJW7btNbPmfqNyN2/henPs2AlvvcLAeQCM7K+3kxfAYrd9Y4LZs/cnc+LEqWx8/PhJ89PhY2bXnkNm3YYfzLiJS8xTr/RQZUXRy1V4Qz9/x6SE2/4ifOTIL2b/jz+brdv3m8VLvzOtO30oINNewQql5hp4HXgotRsPl4leYzZu2m0OHTqqQufSaVGMwz8fN5u37jVzFnxrGrw+ytxWrl0QLsVDpazn09fbi9VIChgAw8vV+ikIlq/0tvBbyg89+bZ59Nl3zQtV+ph2b34k4LXS/LD7UKwXZ87NBWxfqz3Q3FcxsX/lpX8Vnwn617LjB2aCgAngYsl3rfxgLPXRo7+Y+YvWmbff/0Tvf8/Dncyt5dqYm+9pbW67r525X8by5Rr9Ta+Bs8zyVdvM4cPHC7RPPp42c6V4pEPNA493MU3bjFVQPdN9+L1wFmCQw8jhuyTX3i6AIVY8YDyM2uaGO1437btMNAdFQbnByZO/6v/bRMGx6jt/OGAO/XQs5tr9S8DjuBk4fI5+wpyQ4N4Knc2Gjbu0YwgUHzj9fueP2hYhZrJ++eVkrBfSj+WbJcQYrYATJDUDsGjQfKT54ssN2gfo6NET5oddB823AlKrxNPAu9gh1+WjppYWi4XjPZHbxcsBgAAfQCN4l8QmPXmXxA8YfHL9xrtamj9cVFVDHLymC4rX1pCI8Ou8y2poouiFqn3NmA+/1HufSff2A/F8bivX1vxP0armPAnn6Fe8f3IMF7+psXlSvK2Bw+cqmBaUN3RK5nefAP/H05aJRzrE3HR3Kxkv6YuMGyHnlTc21sQ4oSf9u+3+dqZZ27Hiua6Ny9aZ4hFjvzBlH+qkK2SPP99dZRjy1T3XOQEwcvryWTLAaNflo/ik4g0slDDk/b7TTe8BM1UQZ36+RkGAm1Nn0+Y9atkuK9VQJwalpgywoXMASu+BM02fwbPF3V9iVoilOSoeCDHuqVO/iku9zNz98BuqkHgGNRsN1dDHPiRWiU+xDx0537TpPME0ajXatBLPZIC4UwuXbFAAox60fPVW07z9WAEg8ShiOY1MAWPwyLliFduqkF9ZupE+ywNiwcuJRcdaogQoKGD05Mvvm2UyhidPnoq3BzzgE3IObwqlcgHFkj2259xjl13i+KOPvzZ3le+ouZ/LSzUydz7Y3jwo/cMj+pv0r/hNTbRvhGX8/sTipZvUA7DXs7kO5oX+nZSxTwV4lvifeoAPXh5/Dx46YmZKOPjgE2+pRwjAkoy+r2JnU6XuIFOv2QjzUrW+Mq+dFED+LKEgc1u9wRAzX8ICxs3e25J77zBbCp/nGsjQL7/IeIeexxLg/vfHg34+88+e4h0e1PPudeDgWqcDjzjWP/d6Lluyx9RD5oO29CN7G1vO+GcC5LY/PFu6NmEK2gZt3GcocMBYt/EH0/6tieZ/L6mmwkidW+5trQCxd99P2gE69IEAwb0Sjtwu4YUFDMIScgz80hieA8p2hYDKky++J97DenMkJsyrvtkuIDDG/NdfXzNly3cwn0xfoe3tQ0785GsJe3oavmmBFUVhEFCSouQ4xk34UgcG4u/chetMpRfeU8VHSHMCGLeKBacdYEGIsnHzbvPNtzs0BubnCW6+u7Xe/3oZp7YSnmDFbXvGgU+5f7dlj47Bnr2HEp7Dkj2259xjl11CAAGMMgIYWErGecTYBWbDpl3av7kL1kn/xqsHQv9YTSJE2yHW1F7PgsWW7fu0f9t37Nf+uvd02RL/82zHxTMkFP1Znpl8xQtV+5g/C8j/Wcb0NglD2N+yVDxGPC9CV+SDcavbdIS5LJboBnQJNb+Xe1sFsOTeO8yWwufxYH/YdUDHYMcPP2ofbZmlTAHDPt868ZA3bd0jRi25B2nJHlOPPA76smnLXgVktz6Md71BwuvNW/clyE0yxhPfJX39Zu0ODbsxnL56cJh0XHYfUD1280e5BwzPTk8UmhyGBQyIPAUhigWMIiXriOWuY+577E0zQ8AACwZNlTjxCQEC4ld+r9ECBlbIAkYAGrUEqJqbNqJsu2VyIMKVnv1miKtdxbQTD4J7Qgjeoq82msfEjbzyxkY64Ylc21wuocxD/3jHzJm/VjyNo+q1bJLBJemGhwGo2NCkqABGqqTn4BECGPe2ETCqpc9HjoWJR/jxYrbIdbGehE+X39DIvFqzv1paVgdImI4c94V4R8PM4y90N4881VUtbNcen5oVq7ebGeKVvfXeJ6bj25PMgOGf6w8ptREvqW2nCeLWLzc/xbwkywgXY9ei7TjTXBjQ7DN4loIycwGgTZu1Kt4/2tOHhi1G6ViR+GXcEDQIpeIa9STUe+LF7qa8jBmhS0MBFcAewWTOIK63RsK+t9//1HSQ/o4Yt1D7X7f5cNO644fqEeIxEv4AGDcJiHbrOdVs+G63GoHAugYvOjFuK8Tra9p6jLlKPKD/LlJZDE4b02/IbL3P5KnL5B4TTeduUzSZzvNYIke1X5SQe7WUOeMvXh3jQxleaKeukzSH9sATXUyll95T40Yoe+DgEbN2/Q7T6Z3J5ulXeyjAIwu33tvW1BIPdqQ803diDEgQM4eEVq06faCKVEGMEKELyf9BIhNrBIzc/NkeAcLJMgatxWj2GjBDwZN6VesN0nl/9Ll31QNGfhgPQAgvu7LITsVn3zWVRE/qyzx88tkK6SeGORG8Doock7trL3qIoWR1sOKz3UxN6Q/XWS+gxPNbYo77DJxlmrYZo7+Xypi+23uqeVFCZ6IBEvX22nHAyI+t4T7AWC9oi5AjGDaZiJXnd1k/nPKVWhsIgXr61V7ZAAMLw1Kqti1ZT61MMbH4TAbWDyKmRFmLXd9QV0NsjoSBaNhylLra9M0qvmWOAbErxD2vXHuA6d5nmhkkIdN7faarS4wrTJ04YLBKkmJruAsY9z/eRYRzi/QuEb1biZBcI8+Dt/PMP3tJyHbYfCmuf8e3JokV66KhAmAGWHH/v0nsXktABCEtU769KSNhRC2xsMPGzjdlH+6oXz1rLCEWiWK3L/z2LT+HULx0E83JsPqAsNzzSAAY/GUly/bPtnunx1Rd1i5yVV0VNBLGeHAAKL92Ff99TRlTvBCWqwlrunT/2Kxas13DlMOHj4nyrtbnAQwQcn7Hgj0vj8n/GJDajYeKt1ZdPT08CPJH1ssLE5Z7CQluGbuXq/cV0BphRolgIx8o+O33t5XQpaPp1muqKpglZAhj8rQoDWOOdwDo4dbP/eJb6cMwBR+eg9/pvVy811JlWqoHQ71ZMj73C/BfI3KOYSGngofDSiD5FABnw3e7dGzwevk+DPuKipRgX08dNQz3yjg3azNW5RvQAAwBGnSC+uTuqtYbrCEgc48HzjI/AEVujh/+fk/C+TIPdlA5xWgiG/Thsee66wrXnr2Hde4IITCiA8XqPyfjzdYG+s1cXSFGoIQYXkI85GWBeOjME8QvmtmNWIDVUxIu/01Ca4zamA8Xmb1ifKx85AEwsq+SXFC8jj5o2MPo4IQkPCzKh1WfJ64/k05HQEtWEnCJ167PCklmClLSsSJ4J7AMWKk7W6pFYSUGYgK69/5MwwHQHMHDpcJS3CgCgMIH983qK4zSXny1AIcwA0uylIGiDzeVba1ucBbAJP4uie+HjIaMnKd9CAOGDrT06ccDR9Qil5AJZDJeqdlPXWF+GIl+0g5lJBv/j5d7qLXBol0lSk+oxy5VJp69JCw74yUQ26OwLPnJbZR5fsbzwUpvm/88/5/al+mfrzbjJy7WNmHAoH94Vngl5Hiuua25gvPjIpBfLN5o3hbP5nYBLhThLhHcynUGmvrNRqqHVPahjqr09O8d8SgIp/BWJonClRLgIdkLUADmV5RqbGo0GKICy7MRGgH+uPx25cFH9A9lIFFtl9uRK1xuxpP5wVshxHOT2MgQXl25Cp31mctV7GxGiwKwStdMrCkeC4CKxUapK734vgICikYfZ4uxqt1kmAIfCoocAaZPSLjaS7wVvOLeg2apzNtwCYWkXPf43P66GioUFc9ypXiKeEX0vYl4TBgfZIo5ZXXuoSffMQ+LXlwr48/12LN036OdNT+HMj/8j65xmUCX/vfiarrqhZzzrHhFY+X5eF7C6NLyHKw8NRCvsVr9ISpXeI8YIp6LZXTAeMGi9Qpc9EcNlbS9SsaT3BZe2wHxoCxxn6zX23MUkmQGGBvFzcSVppwJAlUR8De7T4nvSQDxh49ZoELJEqmbw5i74Fu1AoQlWAEUGoFFaIgREaaFIkRYC1B64+Y9el+Ukz0GuJFMSrAjNKuvsH7PU/8PdoQyuWTqYZ7Feha2fhZgBB6GS/RjsAMY98pEY8WYRGJTkrwoMULBfa4TT6xRq1FqDcgnoHRYN1YMps1YqS49ytFDQi2WQdnV+leETwQHC8s1a4mVxuow5ljtkycDC4ZC9Bk0W2NMci4d3pmkuRSsJpZKQ5LyHdUttv3bScgh44XV4vkR/joiVO/L/cuJ4DAeD0gcD+jg0WHJt4r1JjRiZYu8x90CQkPEIloXHeBF8OkjdTqIF4Xn0anrZJnr9lrGvhvm0nXZM6EARE6bJqLoV4rSI4uEDyy7W0KGsIYAJ2PH2I+fuES9OpaVeSaM1OSpyzUXg6zVEjkiEcsYrxUwB8gGyByhhBirShIuMveM2xh5dp6ZUBgrXq3+YB0fPDLmjjkpKwDCeDIGjVuO0XHG68bo8Pz0i7Ho1nOahM/fmc8lNAYE8dSRE1aI0JnOMmaAN+DwhvwfgHENBSMWEtA3PDw8IsL2Ox7ooEDPggJhv65IifxVrjNAvR9W8HqI58Lq5UK57kMCRhfK/QCiO0VG6rcYacZ9tFjk9kBCmFfggPHTz8fUiiKMuE/EVmvFUmCFsGpMPOXEzsTNWD4LGPDuvYd03wIWa6ooEsJFKALIoBz8HTX+C83yP/78exqHQ3v3HTajP1ikA5AJYFimnjL/x88H9TMBDFZJAAys9POVeymQ4XLiMZDwBPkRgnICKLibz0odvJsSNzcVQRlt9op7ifDi2vNsCCx7RJhE0B/QRZjxoFjuu+OB9urCPinWceOmXZqUXLpss1pyfjj6suvrC1As1dUqBQwBCsJDBO1JidlZUaoq1o9YF2DGuiDg7IMYOma+xOS9VNjvFk+CpC6rU8TtjD3zgyDOEO8F0CMx3PD1kbo5jr0LNoGKVSQBjJJh0Vgxw1LT7wpPdxNvbYeOX04oL4DBmGJRkdd7K7xpBgydIwDIb4ee1D7u3HVQ98mwusA1kCO8DADjmVd7KviTByBJjJIB9g1ajIgZMZk7mQPmDuDuKx4Iv3Z+vowp4SXhNoCBV2kBg1CK0IY2x46d1BzHsxKuMp4k+V+tRa7rqJajvNyndpPhmixGvrtKGEluhhwVy9KENlyT3Eew0kbO5jf1bpAlZPH/FnnNPCNhBccWMAgR8bL7SJ9JuOtcS3t3bjjOHWCw0/P25ro6Yn/YGER0cxg6qXIDbozAkoBiEHloyoLM8BHd2XeXCCQTgiXCZbMdZfCZRNrzl/0UdicigzBfwhpNJIrLi0tpk3RYEdAeJUPRVfGln0wCbAHC9h2mXpwFvHSLuFue5uUzAAOhABB4iU6TmzLhxW4IwAYFRoFY2nyj22QzX1xBLBDLmCgViM4zudfkmLgUlxL3GQEiv8IYYj1wSfGEyFOQrMLy89xsxuK6jz7bTQGaZT4A4y4JSegfbXBNES76yfPavRh4SeRpyCvgDdJ3wKSmeDTkOAL+VJncBcJfumxLvd9L1ftqfgsQ4TrE5K+JVSMXYlcM+g6eLYDUJgBOmW92wIaf28cucUyYQqKOcIQVOwsYtj4yxC5N9SakH4wJY4xs1hGFw4u6SMLRcgIaLLWz45dEMR6U2x/ieAsYTwtgsEpCvWcl7mdO2W0MQFqZd3mxeA3VGwxWI4IckGhmW0DrNz7U56fvn81epXphn4uEJECO7KgBlpCesZNS5a2isOQ1AByuwXOzg9eGOeRkXqnRz3QRDyNrvqYqsBB6sRkOLxPvCGNiAeMPF1UxFZ7paj6dsUL7EH4WmDGNr5JoDiO3gCHMpIQBgwflJqCjRUjiTFwzVjDIXBPvFRNLiBAjQNbDoD0Th7ArS1usLyADqEwRV7Ju0+G6WQqhJi9Cpp12h8WzmS6TEwhFkLi0YKGAYVmAgDLq8H+whNpAlckChb74JnXTA8ZcAYy26u5hqUnWMh5YUyw2MfTL1fvJM89SK7F0+RY9z6S/JmEWe0XC14RJDNskGftDeGasBRaQcI/n1xi+8wRN8nUWMOKeeC2du05R8OQ6CAc5BwSNZyJhiUASK7NxirFnUxmKg+vLMxI/MzbEtCyH48n5mNwL4IBCzZn/rSoUgEFISLy8Wyy2fZ7h4hkhrH8WZSMZieeYyTIhYMiOXZLaPx85LsbkVAAYcm/GJiVgiDHD0xgrgEHuAwUDNPCMyK0Q8jIXJP8AQhKayCorWHHAEFfeAgbb6x8WkGfuKAMQfVvq8XhZ8aAeoQJeIftbyBXhXTD2ADNzaYl2hATkG3QFSeYDL8Fec5eM5ZBR8/WZuC6JX7x3PFm8R2QXmcB4sf8H1nmSY4Af7xcZZX54NuaaZyG0IjFMWGTvFeYEwBD5yQFghPdhSEjiAYwjR4/roNGJuV+sUxcIBGPTFDF4aRHUS0RhaQsCu4DBBPwows7kkSClPdlmrATCgVXGRWfgUGwGAKU7ceKkeiYrV2/TJBLKi3VwAUFZw4wgCcvqBu4xS4UsiREL4rXEQUPqY4XTAYZu3JLJQGGJh0n4ouzEk0wyS2V4Swg/oGEBgyU1QDh8TRiA5RqMrQUM8jsIEYJCZhthrijAO0lCv5eq9ZH+1pcwopMmNu3mKwAD7wahYoWLXBD9Y+nzTfEUSNoiPLv3/qRe4XIZPzw/8ick4gA83PryDnPMPDwhY8aeFsaGnNJ0sZoIJ+FdHekvQk4fIACCpUyWqAGvLt0/0f0F7jO7jCzs2/+TJiFZwp0ybbnu1zguxsMCBs+D4gCubjsAg2QfYZYCxoTFWkY4wzWw1K/U6K+rGSgo9QB6xhjFIDSM78MIAcYjT72jigcgJQeMvXFvAsAYNmaBvhPV5o0JKrckWDEcbNayRDvyGHgKyEf33tkBY9joBQLQdeKAgfeIDJEEJ4QkhMWj4plh+sgrCg9Lnx8TeUFmXq01QD0eVkw4zw5gvCZ01N4rzImAkZschigSjEJdIA8QzmEEKxjT9OI33tVaB8hmnBFc2lgLDzICGGx8oWN4EwsWrVM3unTZ1qZUmVaaLMSNVndM2K660B5mHZn4C0Ixh4qSomhMDvexAKAswkwMCujwghpgw6YYXEaSkQy8bWMBw32XJExZqyQ1Y6skW2MlWWQHnlh+5ZptgvpB8g+lQxms224JQSEpWbnOIBVoMuouYOBRNWs7Tp8dxXns2e46L+RFXha3lHidsYSCpGdHcUerx1dJpDdaFib6t27jTk24sjoCkLJkjWJwT5cJAzdv3SNzvUfzSwDOJ58tN7dI2OEDjGBlaLyOEwKOMBNOBTstg/GB+Mt4HDx4VJOr9Jl5RIkAUPIOLG+ScMQ9bi7/s1Ro23M9+ks7xpj7jBXl5x4AgeYahPFYF4lb3qT1WHOzyBmJYgzIRwIKeDI+wGD14GkBPfIIeDez53+jYUVC34UJz8g3sFqEFz1tpoQkq7er15FTwLBkAQPdATBIJgPC9ZqNNP9dpIp6E/2HztFnX7/R8i453qWrWJuFmastovj0WUMSAX4XMFyiP/a5kKW8JT3TAAbKz+T+ScCBl7cUHBI4UF4vYIhS2H0YtA02UREa8H/AVvn5/7zLa0kM3U8RHEIoSH62FjRnmRSAYuKIJ8klEO/hErMZiUQdlhghIsvdov04tQjsvSgIwPj1199UuUBqro3Q1202QndPkvexBOix2oMQFBXFBdxcwECxcfFxrXFxATkEEWBghYVxtJPtAgYhAfsMkgEG7dgAxNIp/SNH0nPAjHhoaZndmGzEYpMWPFPmi7lPBRj0nc1m91R4I1haFNBnr8So8YvMfgE4qxyABQlGlAPvhnkgeYgnM0+8TZSMMCxIvtfXJB5L7YSxEDmvT0WRkEkUGyuK8tNnQgPe/eG1BTw4+kSuiP09yChLkhgbQiUfYGio0XF8sJlQvOPqDYeo4ltw5lnJ2bFHg7EjwYtsrPn2e02Y8s5SfgIGniurJcgeS9isRAHi7lwRpmMU3xKPDg9n8tSvdXzPCmDAyQCDB8L9LEICMgYUugLhtAUQkgEGsRJts9oDEtIOjrXnfHD/FrprEFeUh0R4vlm3UxNtJEcffOJtXb5lffvFakG8jmIDLtRnkwqrLmS2dYVFruveIylgSNvsgBGEGC7ZwYcRxmGOstO2caus3Xa8Ndq111TdORgAcrCs6gIG12GCeZZgHV2ESACRTXCEgS5lAwwZ23D/LHE+cMcX6bIq9yfXxDPOWbBWBY/dsX0Hz9KlSfaRsBOR/R5YLcIGEps+wKDfWDfyVywlI/R4jYQ4rECxjMlGM7L1vB3KfgDGnoQsfSEHhFcAoFCPd3UwADeVbaX7c/BGeFau/3zlPjp3AAYbzyjjpUNWO1jSxY1nDwtb5FlWZTMd4St7HT6aslTBxAcYhGx4hCzl/89FVTWfhyfB5ji24LMag6GkHKODDLcWcNu956B4ZbtM83bjUwIG2/KvEMAgh/FuGsBA+Ulss6+CPB66xJgQtuPRIIck2FmV412c0ne11MUFwioM6pkBDJKe7k5PiZnDOQwIV6iTDBzWPe4RoIC2XYwJLS4VwNBVEgsYEpLwduLVbCQCKByPIoHl3gAQdVBychEIHd+9sA9K5puXllgiAzz4HgeJOQTXhgH79rP0uFQzzPZ+7nc2gu9hJH+9nTCGZJPNQBMjcx4K14XxJMh441YjOAjA5RJyIGTshyD/ApiQkMPtJanIikaNRkNVYK0QYe0ASTww6hC6kEvAeljifggIOZb/c+FruhzLSoa9RrhvMC719zt+VCVkfwygUaZ8BwGnPqaaCB7vgpATQWjx4LBczDdJSRRWl07FI2BvCW8Ku9fGM9ooisM3UcgXMfd4D4RVdz7QQQGbvmJlkR3uzc5Tcg68zWxp+aqtunRNeEk+gXFkZYcQj1CEPrDsypywlMr8E6bxLRUMFOPNWONR4p0yznhpbHhauWa7Jj7ZBs61ACU2ZZGwh1B8DA7ACODTV8YDgCdfwC5cwjl2irJH4yuRB2SahD2GQT1cKSOXhQdgifEBZJE5DDBJWJ0nOQ8zloNHzNPQ6f/JczVvN07CjT3qUbFSh/xcEduAxhIquULN04hu0UeYbeIAFUvJCxZtMPc/2sX8519eNf94qUcQquqtEuUBRi9tDqPAtoZnAwwPu4CxXibCAgZWMhPA4C/LoFiHoiXr6a64IaPmqTUgwcj1fIQHwooDngUbmV4UJSD+B/zC90n5AR25FgktBIa6CCyeC2VQQt0YWyJrjiXFmvAMCB9eAgpAyETmHgFnjZzvqDZsOToBMPh/+uw1ppSAFct8uPq8l+JuhuJ+eC1Ya3aMsgmLsU0FGJbIObzV/WMFDcaXsaGPzDeKSnLz3Z7TFAAgPAxeACwjis9SMO71LrGs4evrVua9hxTcyc4jOwCDvjck9ygqf1FqVhLYJzJKlN39TgeEjOA14OVQFy8UAMPLYzXgGfG0HhVAQEkIZfAKWJFgleQ58T6KCxirzCjYBvcDFAhZkAvGZ9zExZpQvlKAhPcreG8Gov+AdVcBa3Z4Aj7sRmbeYOQVpUX52cRnCaDhOy7IOysX6FIYMJq2HWOuF/BiDAktXcAgdALEiomcImvtukzU5Coyjt5hEDGawYepmKegPzwnIQ4rMMgvXirEhjHG6AKRuVcENAn3glslzhdcIIBBCGIBg0QLFioTwGCyCElIomH1QXc+rpIpYCiLAAMYDA5uMm4hu+5YjvMR+zpQiF4DZqmyB1Y6WFEJczrAII7sKorDm7i95XrkIyiDEurG2BKWnDdAx01YbOo0GaFuNK4xIRSbnBCweSJwXXt+qqsauNDB69PBNRA2VpHYKPVHCTfYtEVf3HvwP7kZroc17dF/hgJzJoDB/6xS4JE1aT1aP2hD/7BavBuD8vHinm2DorEZi92LfDAIz8aW+5i5ZkMTOQPen2BvCQnbKnUGGRKZgwX48XRYDqW+SxzzTRW8R1ZMXqjSV/MhJIl5VvoBSPCCGqGMBXGe+0sZI6w3eRos64tV++kLW/MWfhu/F4xLT3iE608feRZ7b5i5YPUOWassfQfg6Qf7ZfBoABU7zhAbswjZAA0+HET+DPCxxDXJW3UU74v9E4QILmAAyISEtGf82diI12T7w7UIMwD5V2sOME+93FM9Jj5NSYi5T8IQu88EQt/6Dv5cPV1CHZKlwa2y5shyHgBjq7pc7gd6UXr2A5BcIqnDcl0DQTNiR6wGyaSs+ixrZrXVMrEubILhhTFQk4QWrhwrA5Rnbx9jaeceo9zU5S8uHy5vnabDVID4nsaUacvMRxJ6sLTLCkNFQWNcVwAr+DK5XJ9rhq4LYAXLqv4fYybexaPBCu7eE3xxjEHOhKjLagghkq5ECCNo7NZDeMl3cG3CrCB+DyYQAlDefHeKWP5g1ah7n8+0XpjwRLgeQMZfwoIMu6feCoKKcJNhp3/85VkJQVhStAQAYL3oL1uL6S+ClmosABmEni+p8dxcm7/sK8H7IxntKp1LCD/jQ9+CVZxd8bHD5eb+unojc4K3aUnnS9x7fR5pg1vPuHEt9160YeUH0KIvYW+V52LnMvfgvvQBZi5JWrt7LCAUmrdMuR73D8sJ/xNecD3KGXe3nLFmvwntGX+Mc9ib5NnYsZr1bLt1bOkPdd3r0T+SxYwfc4BcJCOePZc5jOy/3o6C8pdtx7jPxHZ8uJcNRCihLfcxZTAxOm1wz+HrJIbTjVaxcl9bH1MXBce15S/eEO9RkBfgVV9AjFe9SboBBnhH1oPxXQ/Gy7Hfw/C9fHY2COUkF8GyJxtvWE0g0YUCRhRRflMCYORoH4YARnjjFqsWAANWjhhX41xRWBI/VuET62cBgWWUFuW17RPDg+Ae7upIwnV854QD4AgyysS2lskTBEDh9A92r6PXCpjruB/QgUBql11Kdh5KVuaeT1VuCfeYb0MQK/M8uOW81WnJvY7bzlK68jBlUtetE67nO5cbcq/vu6bvHOTW99XxncsrJbumPZ/T+yVrl+y8pVRlPnKvZ9tZwAhCkpyukrA13FW0MLu5Bfec5XBZTth3jXTXTnY+zO51nDYpcxhniXE52UXKa+uEXrzkR4zqqxtxxHllACPIYeTmZwYiwDjrzE5E4m4SmJu27Nb4m0n11Y044rxy/gJGWCHdY1cBU3Gy9uHjcH1f3XTs1s+gTbpP9J0N5q1C3eJ8gg+9nop/MiDiiAuC8wAY2ZOeZ52vddhXnkfOSnr6v7gVUUTnOtkcRq6Snr87wChgdgHDt6waUUTnOuUeMFZsMdfpxq2sVYTccpZnIK5/nLPOJ4QNueSsa2XncJk9TqzPakvdhC9u+Vy2MEO+82eaXfKdd89lyrlt93vnMPnqFARneq9M6/k4GfnqhhmKhySvCWDk7OWzCDB8gxpmyHf+TLNLvvPuuUw5t+1+7xwmX52C4EzvlWk9HycjX90wQ/kAGH4FzQln7d70n3fP5ZZTXStcZo/D9QPAqJ4jwPi9sEu+8+65TDm37f4d2CVf+dnk/OyTJV9ZmKFEwCDpmYN9GLwIZT95F1cye+yyLXPL3XMuh8uTtXc5VRnsK3fP2f+THcc4/Ik+KNnAWvKVh9lXL9k5yHc+1bE9Z8l3Pnwu2XFOyL1GuL2vDLbkK0vG4frusaVwORQ+hnznIHs+U3Ypk3Pusf3fd5zpOUvuOR9bSnfOno8Dhm4NzxFghJKe1nWX/7NYLLMqoOPma/1AGbPcfYdjbbPqJ1r58LWV7bVsW+UsRQ9fW9k9Z/93jt3722uEd3qGB9NSsvNQsvMFQW4/kt0zVZ1wWbjcR+nqpyqD0pX7KK/X89Vxz7nnw5SuTrr2YcqP67nXSFc/XXmYEpKeeXqXBGWDUbg4BwodVz5Y68eU0NazbeHYuaz6KKvDtk3s2r8XwLCDHj6XjMOUqk6qMku+Oj52Ka/lYfLVh11KVe4r83GYUtVJVWbJV8fHLuW13EeZtElXDoXrhNmlVOW+sggw3P+dY/f+9hoRYGS/r0u++rBLqcp9ZT4OU6o6qcos+er42KW8lvsokzbpyqFwnTC7lKrcV5Z7wPB8DyM1W6X0lfnYVz9de9smXb1knKptDDBSJD2h8LlkHKZUdVKVWfLV8bFL6cotpSu35NZz2aVU5b4yH4cpVZ1UZZZ8dXzsUl7LfZRJm3TlULhOmMOUrDx8Ho4AI4FTtc0OGJA7mOcqnevPF1HmlAfAkJBEP6DjuO/nOAMYf2QfhoQk0dbwiAojRYCRA7aAwWfR+Fx8RBEVNgIw+BIYn5Es4JDk359141axGqZ+8xH6iX0+QXcuMh+5ddlXxzLlCBBfZfeVR3xuMZ8F/Gr5JlPppfdjb6vmADD4gaGE1YpznPlOKF8Q48ds+dAuH/stzMwHfvkxZn6qD6/LVyfic4v5ke66zYab2+5roz9/0X9Ihq+38/VlfjOB38AobFy8dCP9cWK+XVqYGYHhF9v4hH80HoWDmWe4eOmGGmEMGj43hgiJlA0w+JEXfgyHr20XNgYo2YMScQsRmoB9ZRGfu8zPifCzFvx+i4+yAUZEEUUUUTKKACOiiCLKmCLAiCiiiDKmCDAiiiiijCkCjIgiiihDMub/A5/TmMUvYy3KAAAAAElFTkSuQmCC')">
            </td>
            </tr>

        <tr style="text-align: center">
            <td>
                Ground Floor offices, 39 Guildford Road, Lightwater, Surrey, GU18 5SA<br>
                VAT Number:
            </td>
        </tr>
    </table>

    <table>
        <tr>
            <td></td>
            <td></td>
            <td style="text-align: right">DATE</td >
            <td style="text-align: right">INVOICE</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td style="text-align: right"><strong class="date">{{ $invoice->created_at ? date_format( $invoice->created_at,"d M Y") : ''}}</strong></td >
            <td style="text-align: right"><strong class="date">{{  str_pad($invoice->id,5,0,STR_PAD_LEFT) }}</strong></td>
        </tr>
        <tr>
            <td>INVOICE TO:</td>
            <td colspan="3" style="text-align: left; color: #0a0a0a ; font-size: 22px"><strong>Address</strong></td>
        </tr>
        <tr>
            <td><strong>Business Name : {{ $invoice->user ? $invoice->user->company->name : ''}}</strong></td>
            <td colspan="3" style="text-align: left"><strong>Address : {{ $invoice->user ? $invoice->user->company->address : ''}}</strong></td>
        </tr>
        <tr>
            <td><strong>Name : {{ $invoice->user ? $invoice->user->name : ''}}</strong></td>
            <td colspan="3" style="text-align: left"><strong>Town :</strong></td>
        </tr>
        <tr>
            <td><strong>Phone  : {{ $invoice->user ? $invoice->user->phone : ''}}</strong></td>
            <td colspan="3" style="text-align: left"><strong>Country :</strong></td>
        </tr>
        <tr>
            <td><strong>Email  : {{ $invoice->user ? $invoice->user->email : ''}}</strong></td>
            <td colspan="3" style="text-align: left"><strong>Postcode :</strong></td>
        </tr>
        <tr>
            <td><strong></strong></td>
            <td colspan="3" style="text-align: left"><strong>Country :</strong></td>
        </tr>

    </table>
    <table style="margin-top: 25px">
        <tr >
            <td>
                DESCRIPTION
            </td>

            <td>
                AMOUNT (GBP)
            </td>
        </tr>
        <tr class="details item" style="color: #0a0a0a">
            <td>
                {{--{{ json_decode($invoice->response) ? ucwords(json_decode($invoice->response)->payment_method_details->type) : '' }}--}}
                Lead Generation Service
            </td>

            <td>
                {{ $invoice->amount }}
            </td>
        </tr>
        </table>

       {{-- <tr class="heading">
            <td>
                Item
            </td>

            <td>
                Price
            </td>
        </tr>--}}
    <table>
        <tr class="">
            <td></td>
            <td>
                Subtotal
            </td>

            <td style="text-align: right">
                {{ $invoice->amount }}
            </td>
        </tr>

        <tr class="item last">
            <td></td>
            <td>
                VAT
            </td>

            <td style="text-align: right">
                {{ $invoice->total_amount -  $invoice->amount }}
            </td>
        </tr>

        <tr class="item">
            <td></td>
            <td>
                Total
            </td>

            <td style="text-align: right">
                {{ $invoice->total_amount }}
            </td>
        </tr>

        <tr>
            <td></td>
            <td><strong style="color: #0a0a0a">AMOUNT DUE </strong>/ (GBP)</td>
            <td style="font-size: 35px; color: #2289ec ; text-align: right">
               £{{ $invoice->total_amount }}
            </td>
        </tr>
    </table>
</div>
</body>
</html>

@extends('_v2.layout.dashboard')

@section('title',"Tasks")
@section('top-bar-title',"Tasks")

@section('content')
    <staff-task-index
        tasks="{{json_encode($tasks)}}"
        users="{{json_encode($users)}}"
    ></staff-task-index>
@endsection

@section('scripts')
    {{--<script src="https://momentjs.com/downloads/moment.min.js"></script>
    <script>
        function dateFormat(date){
            return moment(date).format('DD-MMM-YYYY');
        }
        $(document).ready(function () {
            let initText = 'Choose file';
            $("#taskSearchBar").on("keyup", function () {
                var value = $(this).val().toLowerCase();
                $('div[data-role="taskSearch"]').filter(function () {
                    $(this).toggle($(this).find('p').text().toLowerCase().indexOf(value) > -1)
                });
            });
            $('#dropdownMenuList >a').click(function () {
                if ($(this).data("value") == 'all') {
                    $("div[data-custom-type]").show();
                } else {
                    $("div[data-custom-type]").not("[data-custom-type*=" + $(this).data("value") + "]").hide();
                    $("div[data-custom-type=" + $(this).data("value") + "]").show();
                }
                $('#sort').val($(this).data('value'));
                $('#ordering').text($(this).text());
            });
            $("[name=attachments]").on('change', function () {
                let fileName = $(this).val().split('\\').pop();
                if (fileName.length > 0) $(this).next().text(fileName);
                else $(this).next().text(initText);
            });
            $("[type=reset]").on('click', function () {
                $("[name=attachments]").next().text(initText);
            });
        });
        $("#radiobuttonsave").click(function(event){
            event.preventDefault(); //prevent default action
            var post_url = $(this).attr("action"); //get form action url
            var request_method = $(this).attr("method");//get form GET/POST method
            console.log('hello');
            /*var form_data = $(this).serialize(); *///Encode form elements for submission
            $.ajax({
               /* url : /staff/tasks ,*/
                type: post,
                /*data : form_data*/
            }).done(function(response){ //
                alert('Task Status Has Changed');
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            let id = '', url = "{{route('_v2.staff.tasks.show', '')}}", user_tasks = sub_tasks = [],
                aUser = aSubTask = '';
            $('#name').text('');
            $('#description').text('');
            $('#assigned_to').text('');
            $('#due_date').text('');
            $('#attachments').attr('href', '');
            $('#file_type').text('');
            $('#status').text('');
            $('#priority').text('');
            $('#link_type').text('');
            $('.view-target').click(function () {
                id = $(this).data('id');
                $.ajax({
                    url: `${url}/${id}`,
                    success: function (result) {
                        $('#assign-list, #task-checklist-container').html('');
                        user_tasks = result.user_tasks;
                        sub_tasks = result.sub_tasks;

                        _.each(user_tasks, (element) => {
                            aUser = `
                                <a href="#" class="text-decoration-none">
                                    <img class="rounded-circle mr-1" src="${element.user.image_url}" alt="img" style="height: 20px">
                                    <strong class="pr-3">${element.user.name}</strong>
                                </a>
                            `;
                            $('#assign-list').append(aUser);
                        });

                        _.each(sub_tasks, (element) => {
                            aSubTask = `
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" id="subTaskStatus_${element.id}" class="custom-control-input">
                                <label for="subTaskStatus_${element.id}" class="custom-control-label pb-2">${element.name}</label>
                            </div>
                            `;
                            $('#task-checklist-container').append(aSubTask);
                        });

                        $('#task-checklist-container .custom-control-input').click(function () {
                            console.log($(this), 'aaaa');
                        });

                        $('#name').text(result.name);
                        $('#description').text(result.description);
                        $('#assigned_to').text(result.user.name);
                        if (result.attachments == '') {
                            $(".hidden-message").show();
                            $("#message").text("No file has been uploaded");
                            $("#attachments").hide();
                        } else {
                            $('#attachments').attr('href', result.attachments);
                        }
                        $('#due_date').text(result.due_date);

                        $('#due_date_modal').text(dateFormat(result.due_date));

                        // $('#due_date_modal').moment().subtract(6, 'days').calendar();
                        /*let myTime = result.due_date;
                        myTime.moment().startOf('day').fromNow();
                        $('#due_date_modal').text(myTime);
                        console.log(result.due_date);*/
                        /*$('#attachments').attr('href', result.attachments);*/
                        $('#file_type').text(result.file_type);
                        if (result.status == 'pending') {
                            $('#selecttasktypeFromRadio').attr('checked', true);
                        }
                        else if(result.status == 'in-progress') {
                            $('#selecttasktypeFromRadio1').attr('checked', true);
                        }
                        else if(result.status == 'completed') {
                            $('#selecttasktypeFromRadio2').attr('checked', true);
                        }
                        $('#status').text(result.status.charAt(0).toUpperCase() + result.status.slice(1));
                        $('#priority').text(result.priority.charAt(0).toUpperCase() + result.priority.slice(1));
                        $('#link_type').text((result.link_type.charAt(0).toUpperCase() + result.link_type.slice(1)).replace(/_/g, ' '));
                    }
                });

            });
        });
    </script>--}}
@endsection

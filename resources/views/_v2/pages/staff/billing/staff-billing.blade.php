@extends('_v2.layout.dashboard')

@section('title',"Billing")
@section('top-bar-title',"Billing")

@section('content')
    <staff-billing-index
        invoices="{{json_encode($invoices)}}"
    >
    </staff-billing-index>
@endsection

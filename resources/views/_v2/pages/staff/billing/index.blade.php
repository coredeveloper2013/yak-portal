@extends('_v2.layout.dashboard')

@section('title') Invocie @endsection

@section('content')
    <customer-billing-index
        invoices="{{json_encode($invoices)}}"
        user="{{json_encode(Auth()->user())}}"
    ></customer-billing-index>

@endsection

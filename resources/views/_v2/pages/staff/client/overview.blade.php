@extends('_v2.layout.dashboard')
@section('title',"Debit Report")
@section('top-bar-title',"Debit Report")
@section('content')
    <div class="debit-doctor" id="report">
        @include('_v2.pages.staff.client.tabs', ['id' => request()->route('id'), 'client' => $client])
        <staff-client-details
            :client_id="{{ request()->route('id') }}"
            users="{{json_encode($users)}}"
            staffs="{{ json_encode( $staffs ) }}"
        ></staff-client-details>
    </div>
@endsection
@section('scripts')
@endsection

@extends('_v2.layout.dashboard')

@section('title') Home @endsection

@section('content')
    @include('_v2.pages.staff.client.tabs', ['id' => request()->route('id'), 'client' => $client])
    <div id="dashboard">
        <div class="row justify-content-between page-header">
            <div class="col-md-6">
                <h1 class="main-title">SMS Notifications</h1>
            </div>
        </div>

        <form action="{{ route('_v2.staff.client.notification',$client->id) }}" method="get">
            <div class="form-row filter-bar">
                <div class="col-md-2">
                    <a href="javascript:void(0)" class="btn btn-primary btn-block" onclick="showAddModal('sms')">+ Add Alert</a>
                </div>
                <div class="col-md-2">
                    <div class="dropdown">
                        <button class="btn btn-white btn-block bg-white dropdown-toggle" type="button"
                                id="dropdownMenuButton" data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false">
                            Sort By : <span class="text-primary font-weight-bolder" id="ordering">{{ request()->sort == 'asc' ? 'Ascending' : 'Descending' }}</span>
                        </button>
                        <input type="hidden" name="sort" id="sort" value="desc">
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" id="dropdownMenuButton">
                            <a class="dropdown-item" href="#" data-value="asc">Ascending</a>
                            <a class="dropdown-item" href="#" data-value="desc">Descending</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="search-form">
                        <input type="text" placeholder="Search" class="form-control" name="search"
                               value="{{ Request::get('search') }}">
                        <i data-feather="search" class="ml-3"></i>
                    </div>
                </div>
                <div class="col-md-4">
                    <button class="btn btn-outline-primary bg-white text-primary float-right pl-5 pr-5" type="submit">
                        <i class="fas fa-sliders-h fa-rotate-90"></i>
                        Filter
                    </button>
                </div>
            </div>
        </form>
        <div class="row mb-3">
            <div class="col-md-12 bg-white rounded table-responsive">
                @if(sizeof($businessSms) > 0)
                    <table class="table p-4 table-borderless table-rounded my-3">
                        <thead>
                        <tr>
                            <th>Date Created</th>
                            <th>Name</th>
                            <th class="text-left">Phone Number</th>
                           {{-- <th></th>--}}
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($businessSms as $sms)
                            <tr>
                                <td>{{$sms->human_date}}</td>
                                <td>{{$sms->name}}</td>
                                <td class="text-left">{{$sms->send_on}} </td>


                                {{--<td>
                                    <a href="javascript:void(0)"
                                       onclick="showEditSmsAlert('{{$sms->name}}', '{{$sms->send_on}}', '{{$sms->id}}', 'sms')"
                                       class="btn btn-outline-primary btn-small-text fixed-width-25">
                                        Edit
                                    </a>
                                    <a href="{{url('delete-alert/'.$sms->id)}}"
                                       class="btn btn-outline-primary btn-small-text fixed-width-25"
                                       onclick="confirmation(event)">
                                        Delete </a>
                                </td>--}}
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <h5 class="text-center req mt-3 alert alert-primary text-primary">No notification found</h5>
                @endif
                @if($businessSms->lastPage() > 1)
                    <div class="d-flex justify-content-between">
                        <nav aria-label="Page navigation example" style="float: left">
                            <select name="" id="per-page-items" class="form-control">
                                <option value="1" {{ request()->items1 == '1' ? 'selected':'' }}>1</option>
                                <option value="5" {{ request()->items1 == '5' ? 'selected':'' }}>5</option>
                                <option
                                    value="20" {{ request()->filled('items1') ? (request()->items1 == '20' ? 'selected':''):'selected' }}>
                                    20
                                </option>
                                <option value="50" {{ request()->items1 == '50' ? 'selected':'' }}>50</option>
                                <option value="100" {{ request()->items1 == '100' ? 'selected':'' }}>100</option>
                            </select>
                        </nav>
                        {{ $businessSms->appends([
                                'items1' => request()->items1 ? request()->items1:20,
                                'page1' => $businessSms->currentPage(),
                                'items2' => request()->items2 ? request()->items2:20,
                                'page2' => $businessEmail->currentPage()
                            ])->links() }}
                    </div>
                @endif
            </div>
        </div>


        <div class="row justify-content-between page-header">
            <div class="col-md-6 mt-5">
                <h1 class="main-title">Email Notifications</h1>
            </div>
        </div>


        <form  action="{{ route('_v2.staff.client.notification',$client->id) }}" method="get">
            <div class="form-row filter-bar">
                <div class="col-md-2">
                    <a href="javascript:void(0)" class="btn btn-primary btn-block" onclick="showAddModal('email')">+ Add Alert</a>
                </div>
                <div class="col-md-2">
                    <div class="dropdown">
                        <button class="btn btn-white btn-block bg-white dropdown-toggle" type="button"
                                id="dropdownMenuButton2" data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false">
                            Sort By : <span class="text-primary font-weight-bolder" id="ordering2">{{ request()->sort2 == 'asc' ? 'Ascending' : 'Descending' }}</span>
                        </button>
                        <input type="hidden" name="sort2" id="sort2" value="desc">
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton2" id="dropdownMenuButton2">
                            <a class="dropdown-item" href="#" data-value="asc">Ascending</a>
                            <a class="dropdown-item" href="#" data-value="desc">Descending</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="search-form">
                        <input type="text" placeholder="Search" class="form-control" name="search2"
                               value="{{ Request::get('search2') }}">
                        <i data-feather="search" class="ml-3"></i>
                    </div>
                </div>
                <div class="col-md-4">
                    <button class="btn btn-outline-primary bg-white text-primary float-right pl-5 pr-5" type="submit">
                        <i class="fas fa-sliders-h fa-rotate-90"></i>
                        Filter
                    </button>
                </div>
            </div>
        </form>

        <div class="row mb-3">
            <div class="col-md-12 bg-white rounded table-responsive">
                @if(sizeof($businessEmail) > 0)
                    <table class="table p-4 table-borderless table-rounded my-3">
                        <thead>
                        <tr>
                            <th>Date Created</th>
                            <th>Name</th>
                            <th class="text-left">Email</th>
                            {{--<th></th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($businessEmail as $sms)
                            <tr>
                                <td>{{$sms->human_date}}</td>
                                <td>{{$sms->name}}</td>
                                <td class="text-left">{{$sms->send_on}} </td>

                                {{--<td>
                                    <a href="javascript:;"
                                       onclick="showEditSmsAlert('{{$sms->name}}', '{{$sms->send_on}}', '{{$sms->id}}', 'email')"
                                       class="btn btn-outline-primary btn-small-text fixed-width-25">
                                        Edit
                                    </a>
                                    <a href="{{url('delete-alert/'.$sms->id)}}"
                                       class="btn btn-outline-primary btn-small-text fixed-width-25"
                                       onclick="confirmation(event)">
                                        Delete </a>
                                </td>--}}
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                @else
                    <h5 class="text-center req mt-3 alert alert-primary text-primary">No notification found</h5>
                @endif
                @if($businessEmail->lastPage() > 1)
                    <div class="d-flex justify-content-between">
                        <nav aria-label="Page navigation example" style="float: left">
                            <select name="" id="per-page-items2" class="form-control">
                                <option value="1" {{ request()->items2 == '1' ? 'selected':'' }}>1</option>
                                <option value="5" {{ request()->items2 == '5' ? 'selected':'' }}>5</option>
                                <option
                                    value="20" {{ request()->filled('items2') ? (request()->items2 == '20' ? 'selected':''):'selected' }}>
                                    20
                                </option>
                                <option value="50" {{ request()->items2 == '50' ? 'selected':'' }}>50</option>
                                <option value="100" {{ request()->items2 == '100' ? 'selected':'' }}>100</option>
                            </select>
                        </nav>

                        {{ $businessEmail->appends([
                                'items1' => request()->items1 ? request()->items1:20,
                                'page1' => $businessSms->currentPage(),
                                'items2' => request()->items2 ? request()->items2:20,
                                'page2' => $businessEmail->currentPage()
                            ])->links() }}
                        {{-- $businessEmail->appends(['items2' => request()->items2 ? request()->items2:20, 'page2' => $businessEmail->currentPage()])->links() --}}
                    </div>
                @endif
            </div>
        </div>


    </div>

    <!--Create SMS Notifications Modal -->
    <div id="addAlertModal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary text-white font-weight-bold">
                    <h5 class="modal-title" id="exampleModalLongTitle">Add Alert</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{route('_v2.staff.client.notification.store',$client->id)}}">
                        @csrf
                        <input type="hidden" name="id" id="id" value="{{old('id')}}">
                        <input type="hidden" name="type" id="type" value="{{old('type')}}">

                        <div class="form-group row">
                            <label for="name" class="col-sm-4 col-form-label">Name</label>
                            <div class="col-sm-8">
                                <input type="text" name="name" id="name" class="form form-control"
                                       value="{{old('name')}}">
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="send_on" class="col-sm-4 col-form-label alert_title">Phone Number</label>
                            <div class="col-sm-8">
                                <input type="text" name="send_on" id="send_on" class="form form-control"
                                       value="{{old('send_on')}}" placeholder="e.g. john.bishop@gmail.com">
                                @error('send_on')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="reset" class="btn btn-light">Reset</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal End -->
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            @if (count($errors) > 0)
            $('#addAlertModal').modal('show');
            @endif
            setTimeout(function () {
                $("#alert_created_success").fadeOut(5000);
            }, 1000);
        });

        function showAddModal(type) {
            $("#name").val('');
            $("#send_on").val('');
            $("#id").val('');
            $("#type").val(type);
            if (type == 'sms') {
                $("#addAlertModal").find('.alert_title').html('Phone Number');
                $("#addAlertModal").find('#send_on').attr('placeholder', 'e.g. +447397909437');
            } else {
                $("#addAlertModal").find('.alert_title').html('Email');
                $("#addAlertModal").find('#send_on').attr('placeholder', 'e.g. john.bishop@gmail.com');
            }
            $("#addAlertModal").modal('show');
        }

        function showEditSmsAlert(name, number, id, type) {
            $("#name").val(name);
            $("#send_on").val(number);
            $("#id").val(id);
            $("#type").val(type);
            if (type == 'sms') {
                $("#addAlertModal").find('.alert_title').html('Phone Number');
                $("#addAlertModal").find('#send_on').attr('placeholder', 'e.g. +447397909437');
            } else {
                $("#addAlertModal").find('.alert_title').html('Email');
                $("#addAlertModal").find('#send_on').attr('placeholder', 'e.g. john.bishop@gmail.com');
            }
            $("#addAlertModal").modal('show');
        }


        function confirmation(ev) {
            ev.preventDefault();
            var urlToRedirect = ev.currentTarget.getAttribute('href'); //use currentTarget because the click may be on the nested i tag and not a tag causing the href to be empty
            Swal.fire({
                title: 'Are you sure?',
                text: 'Once deleted, this will remove this alert.',
                icon: 'warning',
                showCancelButton: true,

            }).then(function (res) {
                if (res.value) {
                    window.location.href = urlToRedirect;
                }
            });
        }

        let url = "{{ action('NotificationController@index') }}";

        let items1 = $('#per-page-items').val();
        let items2 = $('#per-page-items2').val();
        let page1 = "{{request()->page1}}";
        let page2 = "{{request()->page2}}";

        $('#per-page-items').on('change', function () {
            items1 = $('#per-page-items').val();
            items2 = $('#per-page-items2').val();
            page1 = "1";
            page2 = "{{request()->page2}}";

            paginationUrl = `${url}?items1=${items1}&page1=${page1}&items2=${items2}&page2=${page2}`;
            window.location = paginationUrl;
        });
        $('#per-page-items2').on('change', function () {
            items1 = $('#per-page-items').val();
            items2 = $('#per-page-items2').val();
            page1 = "{{request()->page1}}";
            page2 = "1";

            paginationUrl2 = `${url}?items1=${items1}&page1=${page1}&items2=${items2}&page2=${page2}`;
            window.location = paginationUrl2;
        });

        $('#dropdownMenuButton > a').click(function () {
            $('#sort').val($(this).data('value'));
            $('#ordering').text($(this).text());
        });
        $('#dropdownMenuButton2 > a').click(function () {
            $('#sort2').val($(this).data('value'));
            $('#ordering2').text($(this).text());
        });
    </script>
@endsection


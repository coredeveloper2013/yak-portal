@extends('_v2.layout.dashboard')

@section('title') Getting Started @endsection
@section('top') Home 2 @endsection

@section('content')
    {{--@dd($view_landing_pages)--}}
    <div class="debit-report" id="report">
        @include('_v2.pages.staff.client.tabs', ['id' => request()->route('id'), 'client' => $client ?? ''])
        <div class="row">
            @if($view_landing_pages->count() <= 0)
                <div class="col-12">
                    <h4 class="alert alert-primary text-primary w-100 text-center">No Landing Pages Found</h4>
                </div>
            @endif
            @foreach($view_landing_pages as $view_landing_page)
                <div class="col-12 col-md-6 mb-3">
                    <div class="card p-0">
                        <div class="card-body">
                            <div class="top-txt">
                                <span class="text-success mt-0 mb-3">  #{{ $view_landing_page->id }}  </span>
                                <span class="float-right">{{ $view_landing_page->date }}</span>
                            </div>
                            <h5 class="txt-black-label">{{ $view_landing_page->name }}</h5>
                            <p class="txt-grey">{{ $view_landing_page->description }}</p>
                            <div class="row text-group">
                                <div class="col-lg-3">
                                    <span class="txt-black-label">URL</span>
                                </div>
                                <div class="col-lg-9">
                                    <span class="txt-grey">
                                        <a href="{{ $view_landing_page->url }}">{{ $view_landing_page->url }}</a>
                                    </span>
                                </div>
                            </div>
                            <div class="row text-group">
                                <div class="col-lg-3">
                                    <span class="txt-black-label">COST</span>
                                </div>
                                <div class="col-lg-9">
                                    <span class="txt-grey">£{{ $view_landing_page->cost }} per month</span>
                                </div>
                            </div>
                            <div class="footer-card-panel pt-3">
                                <small class="alert alert-danger text-danger px-1 py-0 rounded text-capitalize"><i class="fa fa-dot-circle fa-sm"></i> {{ str_replace('_', ' ', $view_landing_page->status) }}</small>
                                <div class="float-right">
                                    <a href="{{ route('_v2.staff.client.detailsLandingPage', [$client->id, $view_landing_page->id]) }}" class="mt-0 badge badge-boxed  tm-2 pull-right btn-primary">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection

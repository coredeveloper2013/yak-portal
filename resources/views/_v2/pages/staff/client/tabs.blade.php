<div class="form-row justify-content-between page-header">
    <div class="col-md-3">
        <h1 class="main-title">{{$client->name}}</h1>
    </div>

    <div class="col-md-9">
        <div class="form-row">
            <div class="col-lg-2 col-md-3 col-sm-3 px-1">
                <a href="{{ route('_v2.staff.client.overview', $client->id) }}"
                   class="btn {{ Route::is(['_v2.staff.client.overview']) ? 'btn-primary':'btn-secondary' }} btn-block"
                >Overview</a>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-3 px-1">
                <a href="{{ route('_v2.staff.client.billing', $client->id) }}"
                   class="btn {{ Route::is(['_v2.staff.client.billing']) ? 'btn-primary':'btn-secondary' }} btn-block"
                >Billing</a>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-3 px-1">
                <a href="{{ route('_v2.staff.client.logs', $client->id) }}"
                   class="btn {{ Route::is(['_v2.staff.client.logs']) ? 'btn-primary':'btn-secondary' }} btn-block"
                >Logs</a>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-3 px-1">
                <a href="{{ route('_v2.staff.client.viewLandingPage', $client->id) }}"
                   class="btn {{ Route::is(['_v2.staff.client.viewLandingPage']) ? 'btn-primary':'btn-secondary' }} btn-block"
                >View LP</a>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-3 px-1">
                <a href="{{ route('_v2.staff.client.notification', $client->id) }}"
                   class="btn {{ Route::is(['_v2.staff.client.notification']) ? 'btn-primary':'btn-secondary' }} btn-block"
                >Notification</a>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-3 px-1">
                <a href="{{ route('_v2.staff.client.edit_client', $client->id) }}"
                   class="btn {{ Route::is(['_v2.staff.client.edit_client']) ? 'btn-primary':'btn-secondary' }} btn-block"
                >Edit Client</a>
            </div>
        </div>
    </div>
</div>

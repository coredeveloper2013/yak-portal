@extends('_v2.layout.dashboard')

@section('title', 'Clients')
@section('top-bar-title',"Clients")

@section('content')
    @include('_v2.pages.staff.client.tabs', ['id' => request()->route('id'), 'client' => $clients])
    @php
        $aClient               = new stdClass();
        $aClient->id           = $clients->id;
        $aClient->name         = $clients->name;
        $aClient->email        = $clients->email;
        $aClient->phone        = $clients->phone;

        $aClient->company_id   = $clients->company->id;
        $aClient->company_name = $clients->company->name;

        $aClient->business_info_id           = $clients->business_info->id;
        $aClient->business_info_trading_name = $clients->business_info->trading_name;
        $aClient->business_info_website      = $clients->business_info->website;

        $aClient->landing_page_id               = $clients->user_landing_page ? $clients->user_landing_page->id:'';
        $aClient->landing_page_name             = $clients->user_landing_page ? $clients->user_landing_page->name:'';
        $aClient->landing_page_status           = $clients->user_landing_page ? str_replace(' ', '_', strtolower($clients->user_landing_page->status)):'';
        //$aClient->landing_page_status           = $clients->user_landing_page ? $clients->user_landing_page->status:'';

        $aClient->landing_page_description      = $clients->user_landing_page ? $clients->user_landing_page->description:'';
        $aClient->landing_page_customer_benefit = $clients->user_landing_page ? $clients->user_landing_page->customer_benefit:'';
        $aClient->landing_page_other_query      = $clients->user_landing_page ? $clients->user_landing_page->other_query:'';
        $aClient->landing_page_target_location  = $clients->user_landing_page ? $clients->user_landing_page->target_location:'';
        $aClient->landing_page_reviews          = $clients->user_landing_page ? $clients->user_landing_page->reviews:'';


        $aClient->landing_page_q1 = $aClient->landing_page_a1 = $aClient->landing_page_q2 = $aClient->landing_page_a2 = $aClient->landing_page_q3 = $aClient->landing_page_a3 = $aClient->landing_page_q4 = $aClient->landing_page_a4 = '';

        if($clients->user_landing_page && $clients->user_landing_page->landing_question_answers){
            foreach ($clients->user_landing_page->landing_question_answers as $k => $question_answer) {
                $number = (int)$k + 1;
                $aClient->{'landing_page_q' . $number} = $question_answer->question;
                $aClient->{'landing_page_a' . $number} = $question_answer->answer;
            }
        }
    @endphp
    <staff-client-edit
        clients="{{ json_encode( $clients ) }}"
        a_client="{{ json_encode( $aClient ) }}"
        landing_pages="{{json_encode($landingPages)}}"
    >
    </staff-client-edit>
@endsection

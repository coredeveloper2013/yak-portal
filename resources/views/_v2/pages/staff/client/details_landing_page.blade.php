@extends('_v2.layout.dashboard')
@section('title', 'Clients')
@section('top-bar-title',"Clients")
@section('layout') container-fluid appointment @endsection
@section('content')
    <staff-client-landing-page
        landing_pages = "{{json_encode($landingPages)}}"
        landing_comments = "{{json_encode($landingComments)}}"
    ></staff-client-landing-page>
@endsection


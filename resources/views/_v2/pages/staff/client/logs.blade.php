@extends('_v2.layout.dashboard')
@section('title',"Debit Report")
@section('top-bar-title',"Debit Report")
@section('content')
    <div class="debit-doctor" id="report">
        @include('_v2.pages.staff.client.tabs', ['id' => request()->route('id'), 'client' => $client])
        <staff-client-logs
            :client_id="{{ request()->route('id') }}"
            client_logs="{{ json_encode($logs) }}"
        ></staff-client-logs>
    </div>
@endsection
@section('scripts')
@endsection

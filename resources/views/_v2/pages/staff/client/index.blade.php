@extends('_v2.layout.dashboard')

@section('title', 'Clients')
@section('top-bar-title',"Clients")

@section('content')
    <staff-client-index
        clients="{{ json_encode( $clients ) }}"
        staffs="{{ json_encode( $staffs ) }}"
    >
    </staff-client-index>
@endsection

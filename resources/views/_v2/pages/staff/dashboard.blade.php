@extends('_v2.layout.dashboard')

@section('title') Home @endsection
@section('top') Home 2 @endsection

@section('content')
    {{-- <div class="form-group mt-5">
        <input type="text" id="name" name="email" class="form-control" required>
        <label class="form-control-placeholder" for="name">Name</label>
        <i class="fas fa-at"></i>
    </div> --}}
    <div id="dashboard">


        <div class="row mb-4">
            <div class="col-sm-12 col-md-12">
                <div class="card top-card mt-4">
                    <div class="ribon">
                        <div class="top-vector">
                            <img src="{{ asset('images/v2/Vector 3.png') }}" alt="">
                            <p>1</p>
                        </div>
                        <div class="small-vector">
                            <img src="{{ asset('images/v2/Vector 4.png') }}" alt="">
                        </div>

                    </div>

                    <div class="one"><img src="images/v2/one.png" alt=""></div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4 col-sm-12 top-card-img">
                                <img src="images/v2/1.png" alt="">
                            </div>
                            <div class="col-md-8 col-sm-12 position-relative">

                                <h1 class="main-title">Book an Onboarding Call</h1>
                                <p class="sub-title" style="margin-top: 29px">On this call we will go through the questions we have <br> regarding your
                                    campaign and the steps from here.</p>
                                <div class="text-right" style="margin-top: 53px">
                                    <a href="{{ url('/appointments') }}" class="btn w-19px mr-lg-2 btn-info">Book Appointment</a>
                                     <button type="button" class="btn w-19px dash-btn blue btn-primary">Mark as Done</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mb-4">

            <div class="col-sm-12 col-md-12">
                <div class="row">
                    <div class="col-md-3 col-sm-12 mb-sm-4 ">
                        <div class="card">
                            <div class="card-body  position-relative p-md-2">
                                <div class="vector text-right">
                                    <img src="{{ asset('images/v2/svg/Vector-ok.svg') }}" alt="">
                                </div>
                                <img src="{{ asset('images/v2/svg/2.svg') }}" alt="" class="card-img">
                                <div class="card-text">
                                    <h3 class="text-center">Tell us about you</h3>
                                    <p class="text-center">Fill In Your Profile page to help us optimise your campaign. <br> <a href="{{ url('/my-profile') }}" class="d-block text-uppercase">Edit Profile</a></p>
                                </div>
                            </div>

                            <div class="card-footer border-0">
                                <div class="text-center">
                                    <button type="button" class="btn btn-primary custom_button">Mark as Done</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12 mb-sm-4 ">
                        <div class="card">
                            <div class="card-body  position-relative p-md-2">
                                <div class="vector text-right">
                                    <img src="{{ asset('images/v2/svg/Vector-ok.svg') }}" alt="">
                                </div>
                                <img src="{{ asset('images/v2/svg/3.svg') }}" alt="" class="card-img">
                                <div class="card-text">
                                    <h3 class="text-center">Landing Page Approval</h3>
                                    <p class="text-center">One of our Account Managers will contact you to go through the landing page and get it approved.</p>
                                    <a href="{{ url('/my-profile') }}" class="d-none">Edit Profile</a>

                                </div>
                            </div>

                            <div class="card-footer border-0">
                                <div class="text-center">
                                    <button type="button" class="btn btn-primary custom_button">Mark as Done</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12 mb-sm-4  ">
                        <div class="card">
                            <div class="card-body  position-relative p-md-2">
                                <div class="vector text-right">
                                    <img src="{{ asset('images/v2/svg/Vector.svg') }}" alt="">
                                </div>
                                <img src="{{ asset('images/v2/svg/4.svg') }}" alt="" class="card-img">
                                <div class="card-text">
                                    <h3 class="text-center">Campaign Build</h3>
                                    <p class="text-center">At this stage we will build the campaign and do some testing with the keywords / ads.</p>
                                </div>
                            </div>
                            <div class="card-footer border-0">
                                <div class="text-center">
                                    <button type="button" class="btn btn-primary custom_button">Mark as Done</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12 ">
                        <div class="card">
                            <div class="card-body  position-relative p-md-2">
                                <div class="vector text-right">
                                    <img src="{{ asset('images/v2/svg/Vector.svg') }}" alt="">
                                </div>
                                <img src="{{ asset('images/v2/svg/5.svg') }}" alt="" class="card-img">
                                <div class="card-text">
                                    <h3 class="text-center">Campaign is Live!</h3>
                                    <p class="text-center">Your ads will be out there for the world to see. Leads should start coming through shortly.</p>
                                </div>
                            </div>
                            <div class="card-footer border-0">
                                <div class="text-center">
                                    <button type="button" class="btn btn-primary custom_button">Mark as Done</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection

@extends('_v2.layout.dashboard')

@section('title',"Unbounce")
@section('top-bar-title',"Unbounce")

@section('content')

    <staff-unbounce-index
        unbounce="{{json_encode($unbounce)}}"
        users="{{json_encode($users)}}"
    ></staff-unbounce-index>
@endsection

@section('scripts')

@endsection

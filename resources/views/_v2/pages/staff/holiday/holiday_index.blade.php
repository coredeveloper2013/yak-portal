@extends('_v2.layout.dashboard')

@section('title', $page_title)
@section('top-bar-title', $page_title)

@section('styles')
@endsection
<style>
    /* @media screen and (max-width: 796px) {

         .col-12.col-lg-6.p8 {
             padding-bottom: 19px;
         }
     }*/


</style>

@section('content')

    <staff-holiday-index
        holidayusers="{{ json_encode($holidays) }}"
        user_holidays="{{ json_encode($userHolidays) }}"
        users="{{ json_encode($users) }}"
        allusers="{{ json_encode($allUsers) }}"
        route_hoiliday_create="{{ route('_v2.staff.holiday.index') }}"
        route_hoiliday_create_user="{{ route('_v2.staff.holiday.user.store') }}"
    ></staff-holiday-index>

@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('sbVendor/bootstrap-datepicker-1.9.0-dist/js/bootstrap-datepicker.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('sbVendor/bootstrap-datepicker-1.9.0-dist/css/bootstrap-datepicker.min.css') }}">
    <script>
        $("#datepicker").datepicker({
            format: 'yyyy-mm-dd'
        }).on('changeDate', function(ev){
            $('#datepicker').datepicker('hide');
        });
    </script>

@endsection


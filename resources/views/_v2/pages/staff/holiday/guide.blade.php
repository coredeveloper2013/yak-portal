@extends('staff.layouts.app')
@section('staff_content')
<!-- Page Content-->
<div class="page-content grey-bg">
            <div class="container-fluid">

            <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title-box">
                                <div class="float-right">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item">
                                          <a href="/staff/guide/add"><button type="button" class="btn btn-primary text-uppercase">
                           Guides Listing
                        </button></a></li>

                                    </ol>
                                </div>
                                <h4 class="page-title">Guide</h4>
                            </div><!--end page-title-box-->
                        </div><!--end col-->
                    </div>
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 p-20">
                    <div class="row">
                    @if(! $guides->isEmpty())
                               @foreach($guides as $guide)
                       <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                          <div class="guide-box">
                             <img src="{{asset($guide->image)}}">
                             <div class="text-guide">
                                <h4>{{$guide->title}}</h4>
                                <div class="update-notify">  {{$guide->msg?:''}}</div>
                                <p> {{$guide->description}}</p>
                                <a href="{{$guide->url}}" class="read-more">Read more</a>
                             </div>
                          </div>
                       </div>
@endforeach
 @else
                       <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                          <div class="guide-box">
                             <img src="{{asset('/images/b2.jpg')}}">
                             <div class="text-guide">
                                <h4>Most beauituful place to see</h4>
                                <div class="update-notify">Updated 20 seconds ago</div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod .</p>
                                <a href="#" class="read-more">Read more</a>
                             </div>
                          </div>
                       </div>


                       <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                          <div class="guide-box">
                             <img src="{{asset('/images/b3.jpg')}}">
                             <div class="text-guide">
                                <h4>Most beauituful place to see</h4>
                                <div class="update-notify">Updated 20 seconds ago</div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod .</p>
                                <a href="#" class="read-more">Read more</a>
                             </div>
                          </div>
                       </div>


                       <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                          <div class="guide-box">
                             <img src="{{asset('/images/b1.jpg')}}">
                             <div class="text-guide">
                                <h4>Most beauituful place to see</h4>
                                <div class="update-notify">Updated 20 seconds ago</div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod .</p>
                                <a href="#" class="read-more">Read more</a>
                             </div>
                          </div>
                       </div>


                       <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                          <div class="guide-box">
                             <img src="{{asset('/images/b2.jpg')}}">
                             <div class="text-guide">
                                <h4>Most beauituful place to see</h4>
                                <div class="update-notify">Updated 20 seconds ago</div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod .</p>
                                <a href="#" class="read-more">Read more</a>
                             </div>
                          </div>
                       </div>


                       <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                          <div class="guide-box">
                             <img src="{{asset('/images/b3.jpg')}}">
                             <div class="text-guide">
                                <h4>Most beauituful place to see</h4>
                                <div class="update-notify">Updated 20 seconds ago</div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod .</p>
                                <a href="#" class="read-more">Read more</a>
                             </div>
                          </div>
                       </div>


                       <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                          <div class="guide-box">
                             <img src="{{asset('/images/b1.jpg')}}" alt="">
                             <div class="text-guide">
                                <h4>Most beauituful place to see</h4>
                                <div class="update-notify">Updated 20 seconds ago</div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod .</p>
                                <a href="#" class="read-more">Read more</a>
                             </div>
                          </div>
                       </div>


                       <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                          <div class="guide-box">
                             <img src="{{asset('/images/b2.jpg')}}">
                             <div class="text-guide">
                                <h4>Most beauituful place to see</h4>
                                <div class="update-notify">Updated 20 seconds ago</div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod .</p>
                                <a href="#" class="read-more">Read more</a>
                             </div>
                          </div>
                       </div>

@endif

                    </div>
                    </div>
              </div>
            <!-- container -->
            @include('layouts.footer')
            <!--end footer-->
         </div>
<!-- end page content -->
@endsection

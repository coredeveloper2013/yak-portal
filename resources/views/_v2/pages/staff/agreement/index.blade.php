@extends('_v2.layout.dashboard')

@section('title') Agreements @endsection

@section('content')
    {{--@dd($agreements)--}}
    <staff-agreements-index
        agreements="{{ json_encode($agreements) }}"
    ></staff-agreements-index>
@endsection

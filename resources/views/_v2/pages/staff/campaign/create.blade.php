{{--@extends('staff.layouts.app')--}}
@extends('_v2.layout.dashboard')
@section('title') Add Campaign @endsection
@section('layout') container-fluid appointment @endsection

{{--@section('css')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-select.min.css') }}">
@endsection--}}

@section('content')

    <staff-campaign-create  users="{{json_encode($users)}}">

    </staff-campaign-create>
@endsection


@extends('_v2.layout.dashboard')
@section('title', 'Clients')
@section('top-bar-title',"Clients")
@section('layout') container-fluid appointment @endsection
@section('content')
    {{--@dd(json_encode($campaignAmount))--}}
    <staff-campaign-index
        campaigns="{{json_encode($campaigns)}}"
        campaignamount="{{json_encode($campaignAmount)}}"
    ></staff-campaign-index>
@endsection


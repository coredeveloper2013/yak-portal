@extends('_v2.layout.dashboard')

@section('title',"Staff")
@section('top-bar-title',"Staff")

@section('content')
    <staff-my-staff-index
        staff="{{ json_encode($staff) }}"
    ></staff-my-staff-index>
@endsection

@section('scripts')
@endsection

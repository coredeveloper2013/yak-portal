@extends('_v2.layout.dashboard')
@section('title',"Debit Report")
@section('top-bar-title',"Debit Report")
@section('content')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css"/>
    <div class="debit-doctor" id="report">
        <div class="form-row justify-content-between page-header border-0">
            <div class="col-md-3">
                <h1 class="main-title">DEBIT DOCTOR LTD.</h1>
            </div>
            <div class="col-md-9">
                <div class="form-row">
                    <div class="col-lg-2 col-md-3 col-sm-3 px-1 mb-1">
                        <button class="btn btn-primary btn-block">Overview</button>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-3 px-1 mb-1">
                        <button class="btn btn-secondary btn-block">Billing</button>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-3 px-1 mb-1">
                        <button class="btn btn-secondary btn-block">Logs</button>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-3 px-1 mb-1">
                        <button class="btn btn-secondary btn-block">View LP</button>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-3 px-1 mb-1">
                        <button class="btn btn-secondary btn-block">Notification</button>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-3 px-1 mb-1">
                        <button class="btn btn-secondary btn-block">Edit Client</button>
                    </div>
                </div>
            </div>
        </div>


        <div class="form-row mt-3 mb-3 report-total-summary">
            <div class="col mb-2">
                <div class="card rounded border-0">
                    <p class="card-subtitle mt-1 text-muted text-center">Balance</p>
                    <h1 class="text-primary text-center">-£785.56</h1>
                </div>
            </div>
            <div class="col mb-2">
                <div class="card rounded border-0">
                    <p class="card-subtitle mt-1 text-muted text-center">Total Payment</p>
                    <h1 class="text-primary text-center">£120.30</h1>
                </div>
            </div>
            <div class="col mb-2">
                <div class="card rounded border-0">
                    <p class="card-subtitle mt-1 text-muted text-center">Max. Ad Speed</p>
                    <h1 class="text-primary text-center">£220</h1>
                </div>
            </div>
            <div class="col mb-2">
                <div class="card rounded border-0">
                    <p class="card-subtitle mt-1 text-muted text-center">No. Of Top Up</p>
                    <h1 class="text-primary text-center">3</h1>
                </div>
            </div>
            <div class="col mb-2">
                <div class="card rounded border-0">
                    <p class="card-subtitle mt-1 text-muted text-center">Last Top Up</p>
                    <h1 class="text-primary text-center">8 days ago</h1>
                </div>
            </div>

        </div>
        <div class="form-row mt-4 mb-4">
            <div class="col-md-12 chart-section">
                <div class="bg-white rounded pl-4 pr-4 pb-4 pt-1 min-h-30">
                    <div class="form-row justify-content-between page-header mb-0 border-0">
                        <div class="col-12 col-sm-12 col-md-4 col-lg-7">
                            <h1 class="main-title">Campaign Report</h1>
                        </div>
                        <div class="col-12 col-sm-12 col-md-8 col-lg-5">
                            <div class="form-row">
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-5">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <button class="btn btn-outline-secondary m-0" type="button"><i
                                                    class="fa fa-calendar"></i></button>
                                        </div>
                                        <input type="text" class="form-control datePicker" placeholder="10-Dec-2020"
                                               aria-label="dd-mm-yyyy" aria-describedby="basic-addon2">
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    to
                                </div>
                                <div class="col-md-5">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <button class="btn btn-outline-secondary m-0" type="button"><i
                                                    class="fa fa-calendar"></i></button>
                                        </div>
                                        <input type="text" class="form-control datePicker" placeholder="10-Dec-2020"
                                               aria-label="dd-mm-yyyy" aria-describedby="basic-addon2">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-lg-12 col-md-12 col-sm-12 my-auto">
                            <div id="barchart_values"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row mt-4 mb-4 justify-content-between page-header note-section">
            <div class="col-md-12">
                <h1 class="main-title">Note</h1>
            </div>
            <div class="col-12 col-sm-12 col-md-8 col-lg-8">
                <div class="rounded note-section-left">

                </div>
            </div>

            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                <div class="bg-white rounded p-2 note-section-right">
                    <ul>
                        <li>
                            <span>Contract Signed :</span>
                            <h3>Yes</h3>
                        </li>
                        <li>
                            <span>Campagn Manager :</span>
                            <h3>Dimas Ariyanto</h3>
                        </li>
                        <li>
                            <span>Sales Person :</span>
                            <h3>Salung Prastyo</h3>
                        </li>
                        <li>
                            <span>Account Manger :</span>
                            <h3>Andrian Dico P.</h3>
                        </li>

                    </ul>
                </div>
            </div>

        </div>
        <div class="form-row mt-4 mb-4 add-note-section">
            <div class="col-md-8">
                <div class="input-group">
                    <input type="text" class="form-control input-redius" placeholder="Type here..."
                           aria-label="Type here..." aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-primary add-note-btn pl-4 pr-4 btn-redius" type="button">Add Note
                        </button>
                    </div>
                </div>
            </div>

        </div>
        <div class="form-row mb-4 mt-4 task-app-section">
            <div class="col-sm-12 col-md-12 col-lg-6 task-section">
                <div class="bg-white rounded">
                    <div class="form-row p-4 justify-content-between page-header mb-0 border-0">
                        <div class="col-6 col-sm-6 col-md-3">
                            <h1 class="main-title">Tasks</h1>
                        </div>
                        <div class="col-6 col-sm-6 col-md-9 text-right">
                            <button class="btn btn-primary btn-sm">Add Task</button>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table p-4 table-borderless table-rounded">
                            <tbody class="">
                            <tr>
                                <td scope="row" class="task-app-title">Quote accepted by jonathan
                                    <br> <small>The following is the photo that I attached... </small>
                                </td>
                                <td class="time-ago-warning">Due 1 day ago</td>
                                <td class="text-right">
                                    <button type="button" class="btn btn-outline-primary btn-small-text fixed-width-25">
                                        View Details
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row" class="task-app-title">Nordian Dwi
                                    <br> <small>The following is the photo that I attached... </small>
                                </td>
                                <td class="time-ago-warning">Due 1 day ago</td>
                                <td class="text-right">
                                    <button type="button" class="btn btn-outline-primary btn-small-text fixed-width-25">
                                        View Details
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row" class="task-app-title">Dinda Ariyanti
                                    <br> <small>The following is the photo that I attached... </small>
                                </td>
                                <td class="time-ago-success">Due 1 day ago</td>
                                <td class="text-right">
                                    <button type="button" class="btn btn-outline-primary btn-small-text fixed-width-25">
                                        View Details
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row" class="task-app-title">Quote accepted by jonathan
                                    <br> <small>The following is the photo that I attached... </small>
                                </td>
                                <td class="time-ago-success">Due 1 day ago</td>
                                <td class="text-right">
                                    <button type="button" class="btn btn-outline-primary btn-small-text fixed-width-25">
                                        View Details
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <div class="col-sm-12 col-md-12 col-lg-6 appointment-section">
                <div class="bg-white rounded">
                    <div class="form-row p-4 justify-content-between page-header mb-0 border-0">
                        <div class="col-6 col-sm-6 col-md-3">
                            <h1 class="main-title">Appointments</h1>
                        </div>
                        <div class="col-6 col-sm-6 col-md-9 text-right">
                            <button class="btn btn-primary btn-sm">Add Appointment</button>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table p-4 table-borderless table-rounded">
                            <tbody class="">
                            <tr>
                                <td scope="row" class="date">20/08/2020</td>
                                <td class="task-app-title">Meeting with Officials
                                    <br> <small>XYZ person</small>
                                </td>
                                <td class="time">14:20</td>
                                <td class="text-right">
                                    <button type="button" class="btn btn-outline-primary btn-small-text fixed-width-25">
                                        View Details
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row" class="date">20/08/2020</td>
                                <td class="task-app-title">Product Presentation
                                    <br> <small>With Client</small>
                                </td>
                                <td class="time">09:20</td>
                                <td class="text-right">
                                    <button type="button" class="btn btn-outline-primary btn-small-text fixed-width-25">
                                        View Details
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row" class="date">20/08/2020</td>
                                <td class="task-app-title">Plant Visit
                                    <br> <small>View Details</small>
                                </td>
                                <td class="time">16:20</td>
                                <td class="text-right">
                                    <button type="button" class="btn btn-outline-primary btn-small-text fixed-width-25">
                                        View Details
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row" class="date">20/08/2020</td>
                                <td class="task-app-title">Meeting Clients
                                    <br> <small>View Details</small>
                                </td>
                                <td class="time">12:20</td>
                                <td class="text-right">
                                    <button type="button" class="btn btn-outline-primary btn-small-text fixed-width-25">
                                        View Details
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.datePicker').datepicker({
                format: 'dd-mm-yyyy'
            })
        })
    </script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load("current", {packages: ["corechart"]});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Genre', 'Pending', 'Dont Buy', 'Booked', 'Sold', 'Unresponsive',
                    'Spam', 'Literature', {role: 'annotation'}],
                // ['client 03', 10, 24, 20, 32, 18, 5, ''],
                // ['client 02', 16, 22, 23, 30, 16, 9, ''],
                ['client 01', 28, 19, 29, 30, 12, 13, 565, '']
            ]);
            var view = new google.visualization.DataView(data);
            view.setColumns([0, 1, 2, 3, 4, 5,
                {
                    calc: "stringify",
                    sourceColumn: 1,
                    type: "string",
                    role: "annotation"
                },
                2]);
            var options = {
                colors: [
                    '#f9b69c',
                    '#8bd6c1',
                    '#b8abf3',
                    '#fdeb89',
                    '#edcdf7',
                    '#d7d7d7'],
                height: 100,
                legend: {position: 'bottom', alignment: 'start', maxLines: 3},
                bar: {groupWidth: '30%'},

                isStacked: true
            };
            var chart = new google.visualization.BarChart(document.getElementById("barchart_values"));
            chart.draw(view, options);
        }
    </script>
    <script type="text/javascript">
        google.charts.load('current', {'packages': ['corechart']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Pending', 'Hours per Day'],
                ['Pending 50.45%', 50.45],
                ['Dont Buy 34.00%', 34.00],
                ['Booked 10.23%', 10.23],
                ['Sold', 5.98],
                ['Unresponsive 0.00%', 0.00],
                ['Spam 0.00%', 0.00]
            ]);
            var options = {
                pieHole: 0.6,
                colors: [
                    '#f9b69c',
                    '#8bd6c1',
                    '#b8abf3',
                    '#fdeb89',
                ],
                //
                height: 240,
                width: '100%',
                pieSliceText: 'label',

            };
            var chart = new google.visualization.PieChart(document.getElementById('piechart'));
            chart.draw(data, options);
        }
    </script>
@endsection




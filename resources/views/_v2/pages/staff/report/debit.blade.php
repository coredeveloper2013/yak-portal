@extends('_v2.layout.dashboard')
@section('title',"Debit Report")
@section('top-bar-title',"Debit Report")
@section('content')
    <div class="debit-report" id="report">
        <div class="row justify-content-between page-header border-0">
            <div class="col-md-3">
                <h1 class="main-title">Debit Doctor LTD.</h1>
            </div>
            <div class="col-md-9">
                <div class="form-row">
                    <div class="col-lg-2 col-md-3 col-sm-3 px-1 mb-1">
                        <button class="btn btn-secondary btn-block">Overview</button>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-3 px-1 mb-1">
                        <button class="btn btn-primary btn-block">Billing</button>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-3 px-1 mb-1">
                        <button class="btn btn-secondary btn-block">Logs</button>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-3 px-1 mb-1">
                        <button class="btn btn-secondary btn-block">View LP</button>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-3 px-1 mb-1">
                        <button class="btn btn-secondary btn-block">Notification</button>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-3 px-1 mb-1">
                        <button class="btn btn-secondary btn-block">Edit Client</button>
                    </div>
                </div>

            </div>

        </div>
        <div class="form-row report-total-summary">
            <div class="col mb-2">
                <div class="card rounded border-0 ">
                    <h1 class="text-primary text-center">-£785.56</h1>
                    <p class="card-subtitle mt-1 text-muted text-center">Balance</p>
                </div>
            </div>
            <div class="col mb-2">
                <div class="card rounded border-0 ">
                    <h1 class="text-primary text-center">£120.30</h1>
                    <p class="card-subtitle mt-1 text-muted text-center">Total Payment</p>
                </div>
            </div>
            <div class="col mb-2">
                <div class="card rounded border-0 ">
                    <h1 class="text-primary text-center">£220</h1>
                    <p class="card-subtitle mt-1 text-muted text-center">Max. Ad Speed</p>
                </div>
            </div>
            <div class="col mb-2">
                <div class="card rounded border-0 ">
                    <h1 class="text-primary text-center">3</h1>
                    <p class="card-subtitle mt-1 text-muted text-center">No. Of Top Up</p>
                </div>
            </div>
            <div class="col mb-2">
                <div class="card rounded border-0  px-0">
                    <h1 class="text-primary text-center">8 days ago</h1>
                    <p class="card-subtitle mt-1 text-muted text-center">Last Top Up</p>
                </div>
            </div>

        </div>
        <div class="row mt-4 mb-4">
            <div class="col-md-12">
                <div class="bg-white rounded table-responsive p-2">
                    <br>
                    <table class="table p-4 table-borderless table-rounded">
                        <thead>
                        <tr>
                            <th scope="col">Status</th>
                            <th scope="col">Payment Method</th>
                            <th scope="col">Next Renewal</th>
                            <th scope="col">Renewal Amount</th>
                            <th scope="col">Number of Renewal</th>
                        </tr>
                        </thead>
                        <tbody class="">
                        <tr>
                            <td scope="row"><span class="badge badge-success">Weekly</span></td>
                            <td>Card Payment</td>
                            <td>27 August 2020</td>
                            <td>27 August 2020</td>
                            <td class="text-center"> 2</td>

                        </tr>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="4">
                            </th>
                            <th colspan="1" class="text-right">
                                <button class="btn btn-primary btn-small-text table-footer-btn mt-4 px-5"><i class="fa fa-edit"></i> Edit
                                </button>
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
        <div class="row mt-4 mb-4">
            <div class="col-md-12">
                <div class="d-flex padding-34 bg-white rounded pr-2">
                    <div class="col">
                        <h6 class="card-title text-primary font-weight-bold">Billing Details</h6>
                        <img src="{{ asset('/images/mastercard.png')}}" alt=""
                             style="height: 40px !important; width: 56px !important;">
                        <h6 class="card-subtitle mt-2 text-muted">**** **** **** 2347</h6>
                        <h6 class="card-subtitle mt-1 ">Expires 09/25</h6>
                    </div>

                    <div class="col">
                        <h6 class="card-title text-primary font-weight-bold">Billing Address</h6>
                        <h6 class="card-subtitle mb-2 text-muted">Dhaka, Bangladesh </h6>
                        <h6 class="card-title text-primary font-weight-bold">Phone</h6>
                        <h6 class="card-subtitle mb-2 text-muted">+2454958998596</h6>
                        <button class="btn btn-primary btn-small-text mt-3 float-right px-5"><i class="fa fa-edit"></i> Edit
                        </button>
                    </div>

                </div>
            </div>
        </div>
        <div class="row mt-4 mb-4">
            <div class="col-md-12 invoice">
                <div class="bg-white rounded table-responsive p-2">
                    <br>
                    <table class="table p-4 table-borderless table-rounded">
                        <thead>
                        <tr>
                            <th scope="col" class="text-nowrap">Status</th>
                            <th scope="col" class="text-nowrap">Business Name</th>
                            <th scope="col" class="text-nowrap">Full Name</th>
                            <th scope="col" class="text-nowrap">Issued On</th>
                            <th scope="col" class="text-nowrap">Paid On</th>
                            <th scope="col" class="text-nowrap">Method</th>
                            <th scope="col" class="text-nowrap" colspan="3">Type</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="">
                            <td>
                                <span class="badge badge-success">Paid</span>
                            </td>
                            <td class="text-nowrap mute">
                                Abc Company
                            </td>
                            <td class="text-nowrap mute">
                                Mr. John
                            </td>
                            <td class="text-nowrap">
                                13 Aug 2020
                            </td>
                            <td class="text-nowrap">
                                27 Nov 2020
                            </td>
                            <td class="text-nowrap">
                                Bank Transfer
                            </td>
                            <td class="text-nowrap">
                                Recurrning
                            </td>

                            <td class="pl-0 pl-0 text-right">
                                <span class="badge badge-primary">PDF</span>
                            </td>
                            <td class="pl-0 text-left">
                                {{--  <button type="button"  class="btn btn-primary btn-small-text fixed-width-15">Pay</button>--}}
                            </td>
                        </tr>
                        <tr class="">
                            <td>
                                <span class="badge badge-success">Paid</span>
                            </td>
                            <td class="text-nowrap mute">
                                Abc Company
                            </td>
                            <td class="text-nowrap mute">
                                Mr. Mike
                            </td>
                            <td class="text-nowrap">
                                13 Aug 2020
                            </td>
                            <td class="text-nowrap">
                                27 Nov 2020
                            </td>
                            <td class="text-nowrap">
                                Bank Transfer
                            </td>
                            <td class="text-nowrap">
                                Recurrning
                            </td>

                            <td class="pl-0 text-right">
                                <span class="badge badge-primary">PDF</span>
                            </td>
                            <td class="pl-0 text-left">
                                {{--    <button type="button"  class="btn btn-primary btn-small-text fixed-width-15">Pay</button>--}}
                            </td>
                        </tr>
                        <tr class="">
                            <td>
                                <span class="badge badge-success">Paid</span>
                            </td>
                            <td class="text-nowrap mute">
                                Abc Company
                            </td>
                            <td class="text-nowrap mute">
                                Mr. David
                            </td>
                            <td class="text-nowrap">
                                13 Aug 2020
                            </td>
                            <td class="text-nowrap">
                                27 Nov 2020
                            </td>
                            <td class="text-nowrap">
                                Bank Transfer
                            </td>
                            <td class="text-nowrap">
                                Recurrning
                            </td>

                            <td class="pl-0 text-right">
                                <span class="badge badge-primary">PDF</span>
                            </td>
                            <td class="pl-0 text-left">
                                {{--    <button type="button"  class="btn btn-primary btn-small-text fixed-width-15">Pay</button>--}}
                            </td>
                        </tr>
                        <tr class="due-text">
                            <td>
                                <span class="badge badge-danger">Due</span>
                            </td>
                            <td class="text-nowrap mute">
                                Abc Company
                            </td>
                            <td class="text-nowrap mute">
                                Mr. Deny
                            </td>
                            <td class="text-nowrap">
                                13 Aug 2020
                            </td>
                            <td class="text-nowrap">
                                27 Nov 2020
                            </td>
                            <td class="text-nowrap">
                                Bank Transfer
                            </td>
                            <td class="text-nowrap">
                                Recurrning
                            </td>

                            <td class="pl-0 text-right">
                                <span class="badge badge-primary">PDF</span>
                            </td>
                            <td class="pl-0 text-left">
                                <button type="button" class="btn btn-primary btn-small-text fixed-width-15">Pay</button>
                            </td>
                        </tr>

                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="7">
                            </th>
                            <th colspan="3" class="text-right">
                                <button class="btn btn-primary mt-4">Create Invoice</button>
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
@endsection





@extends('_v2.layout.dashboard')
@section('title', 'Lead')
@section('top-bar-title',"Lead")
@section('layout') container-fluid appointment @endsection
@section('content')
    <staff-lead-index
        leads="{{json_encode($leads)}}"
        campaign="{{json_encode($campaign)}}"
    ></staff-lead-index>
@endsection



@extends('_v2.layout.dashboard')
@section('title',"Home 2")
@section('top-bar-title',"Home 2")
@section('content')

    <staff-home-index
        home="{{ json_encode( $home ) }}"
    >
    </staff-home-index>

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css"/>
    <div class="home-2" id="report">
        <div class="form-row justify-content-between page-header border-0">
            <div class="col-md-3">
                <h1 class="main-title">Hello, <span> {{ Auth()->user()->name }}</span></h1>
            </div>
        </div>
        <div class="mt-4 mb-4 report-total-summary">
            <div class="form-row ">
                <div class="col mb-2">
                    <div class="card rounded border-0">
                        <h1 class="text-primary font-weight-bold">Onboarding</h1>
                        <h1 class="text-secondary mb-0 font-weight-bold">13</h1>
                        <p class="card-subtitle text-muted">Campaign</p>
                    </div>
                </div>
                <div class="col mb-2">
                    <div class="card rounded border-0">
                        <h1 class="text-primary font-weight-bold">LP Build / Change</h1>
                        <h1 class="text-secondary mb-0 font-weight-bold">4</h1>
                        <p class="card-subtitle text-muted">Campaign</p>
                    </div>
                </div>
                <div class="col mb-2">
                    <div class="card rounded border-0">
                        <h1 class="text-primary font-weight-bold">LP IR</h1>
                        <h1 class="text-secondary mb-0 font-weight-bold">6</h1>
                        <p class="card-subtitle text-muted">Campaign</p>
                    </div>
                </div>
                <div class="col mb-2">
                    <div class="card rounded border-0">
                        <h1 class="text-primary font-weight-bold">LP ER</h1>
                        <h1 class="text-secondary mb-0 font-weight-bold">3</h1>
                        <p class="card-subtitle text-muted">Campaign</p>
                    </div>
                </div>
                <div class="col mb-2">
                    <div class="card rounded border-0">
                        <h1 class="text-primary font-weight-bold">Campaign Build</h1>
                        <h1 class="text-secondary mb-0 font-weight-bold">10</h1>
                        <p class="card-subtitle text-muted">Campaign</p>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col mb-2">
                    <div class="card rounded border-0">
                        <h1 class="text-primary font-weight-bold">Internal Approval</h1>
                        <h1 class="text-secondary mb-0 font-weight-bold">3</h1>
                        <p class="card-subtitle text-muted">Campaign</p>
                    </div>
                </div>
                <div class="col mb-2">
                    <div class="card rounded border-0">
                        <h1 class="text-primary font-weight-bold">Live</h1>
                        <h1 class="text-secondary mb-0 font-weight-bold">3</h1>
                        <p class="card-subtitle text-muted">Campaign</p>
                    </div>
                </div>
                <div class="col mb-2">
                    <div class="card rounded border-0">
                        <h1 class="text-primary font-weight-bold">Paused</h1>
                        <h1 class="text-secondary mb-0 font-weight-bold">1</h1>
                        <p class="card-subtitle text-muted">Campaign</p>
                    </div>
                </div>
                <div class="col mb-2">
                    <div class="card rounded border-0">
                        <h1 class="text-primary font-weight-bold">Lost</h1>
                        <h1 class="text-secondary mb-0 font-weight-bold">1</h1>
                        <p class="card-subtitle text-muted">Campaign</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-row mt-4 mb-4 chart-section">
            <div class="col-12 col-sm-12 col-md-12">
                <h1 class="main-title-1 mb-3">Campaign Report</h1>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-white border-0">
                        <div class="row justify-content-between">
                            <div class="col">
                                <h5 class="title-small">LIVE CAMPAIGNS</h5>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <div id="absolute-filter" class="absolute-filter">
                            <div class="form-group mb-0">
                                <label for="" class="text-sm">From</label>
                                <input type="text" placeholder="yyyy-dd-mm" class="form-control form-control-sm"></div>
                            <div class="form-group">
                                <label for="" class="text-sm">To</label>
                                <input type="text" placeholder="yyyy-dd-mm" class="form-control form-control-sm">
                            </div>
                        </div>

                        <div id="barchart_values"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-row mb-4 mt-4 task-app-section">
            <div class="col-sm-12 col-md-12 col-lg-6 task-section">
                <div class="bg-white rounded">
                    <div class="form-row p-4 justify-content-between page-header mb-0 border-0">
                        <div class="col-6 col-sm-6 col-md-3">
                            <h1 class="main-title">Tasks</h1>
                        </div>
                        <div class="col-6 col-sm-6 col-md-9 text-right">
                            <a class="btn btn-link btn-sm" href="{{ url('/staff/tasks') }}">See All <i class="fa fa-eye"></i></a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table p-4 table-borderless table-rounded">
                            <tbody class="">
                            @foreach($tasks as $task)
                                <tr>
                                    <td scope="row" class="task-app-title">{{$task->name}}
                                        <br> <small>{{$task->description}} </small>
                                    </td>
                                    <td class="time-ago-warning">{{$task->due_date}}</td>
                                    <td class="text-right">
                                        <button type="button" class="btn btn-outline-primary btn-small-text fixed-width-25">
                                            View Details
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <div class="col-sm-12 col-md-12 col-lg-6 appointment-section">
                <div class="bg-white rounded">
                    <div class="form-row p-4 justify-content-between page-header mb-0 border-0">
                        <div class="col-6 col-sm-6 col-md-3">
                            <h1 class="main-title">Appointments</h1>
                        </div>
                        <div class="col-6 col-sm-6 col-md-9 text-right">
                            <a class="btn btn-link btn-sm" href="{{ url('/staff/appointment-calender') }}">See All <i class="fa fa-eye"></i></a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table p-4 table-borderless table-rounded">
                            <tbody class="">
                            @foreach($appointments as $appointment)
                                <tr>
                                    <td scope="row" class="date">{{$appointment->date}}</td>
                                    <td class="task-app-title">{{$appointment->title}}
                                        <br> <small>{{$appointment->description}}</small>
                                    </td>
                                    <td class="time">{{$appointment->from}}</td>
                                    <td class="text-right">
                                        <button type="button" class="btn btn-outline-primary btn-small-text fixed-width-25">
                                            View Details
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.datePicker').datepicker({
                format: 'dd-mm-yyyy'
            })
        })
    </script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load("current", {packages: ["corechart"]});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Genre', 'Pending', 'Dont Buy', 'Booked', 'Sold', 'Unresponsive',
                    'Spam', 'Literature', {role: 'annotation'}],
                // ['client 03', 10, 24, 20, 32, 18, 5, ''],
                // ['client 02', 16, 22, 23, 30, 16, 9, ''],
                ['client 09', 51, 59, 59, 50, 52, 53, 565, ''],
                ['client 08', 21, 39, 29, 20, 22, 23, 265, ''],
                ['client 07', 31, 39, 39, 30, 32, 33, 365, ''],
                ['client 06', 41, 49, 49, 40, 42, 43, 465, ''],
                ['client 05', 51, 59, 59, 50, 52, 53, 565, ''],
                ['client 04', 21, 19, 29, 30, 12, 13, 565, ''],
                ['client 03', 21, 19, 29, 30, 12, 13, 565, ''],
                ['client 02', 21, 19, 29, 30, 12, 13, 565, ''],
                ['client 01', 21, 19, 29, 30, 12, 13, 565, '']
            ]);
            var view = new google.visualization.DataView(data);
            view.setColumns([0, 1, 2, 3, 4, 5,
                {
                    calc: "stringify",
                    sourceColumn: 1,
                    type: "string",
                    role: "annotation"
                },
                2]);
            var options = {
                colors: [
                    '#DF7248',
                    '#3D7968',
                    '#b8abf3',
                    '#fdeb89',
                    '#edcdf7',
                    '#d7d7d7'
                ],
                chartArea: {left:80, top:0, width:'74.35%', height: '100%'},
                // height: 100,
                // legend: {position: 'bottom', alignment: 'start', maxLines: 3},
                // bar: {groupWidth: '30%'},
                height: 400,

                // width: 900,
                legend: {position: 'right', alignment: 'center', maxLines: 9},
                bar: {groupWidth: '30%', groupHeight: '80%'},

                isStacked: true
            };
            var chart = new google.visualization.BarChart(document.getElementById("barchart_values"));
            chart.draw(view, options);
        }
    </script>
@endsection




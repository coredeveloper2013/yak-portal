@extends('_v2.layout.dashboard')
@section('title',"Dashboard")
@section('top-bar-title',"Dashboard")
@section('content')
    <div class="dashboard">
        <h4 class="title_30 mb-4">Hello, <span> {{ Auth()->user()->name }}</span></h4>
        <div class="row mb-4">
            <div class=" col-lg-4 col-md-12 col-sm-12  mb-sm-4">
                <div class="card shadow-css fix-height">
                    <div class="row">
                        <div class="col-sm-12 col-md-4 text-center my-auto">
                            <img src="{{asset('/')}}images/v2/card_1.png" alt="" class="image-card">
                        </div>
                        <div class="col-sm-12 col-md-8">
                            <h4 class="title-blue">{{ $leads }}</h4>
                            <p class="card-text">Leads so far</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-12 col-sm-12 mb-sm-4">
                <div class="card shadow-css fix-height">
                    <div class="row">
                        <div class="col-sm-12 col-md-4 text-center my-auto">
                            <img src="{{asset('/')}}images/v2/card_2.png" alt="" class="image-card">
                        </div>
                        <div class="col-sm-12 col-md-8">
                            <h4 class="green">{{ $appointments->count() }}</h4>
                            <p class="card-text">Appointment Booked</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12 col-sm-12 mb-sm-4">
                <div class="card shadow-css fix-height">
                    <div class="row">
                        <div class="col-sm-12 col-md-4 text-center my-auto">
                            <img src="{{asset('/')}}images/v2/card_3.png" alt="" class="image-card">
                        </div>
                        <div class="col-sm-12 col-md-8">
                            <h4 class="red">£757.50</h4>
                            <p class="card-text">Amount Owed</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <h4 class="title_30 mb-4 font_blue font-weight_600">Leads Report</h4>
        <div class="row mb-4 bar_chart">
            <div class="col-md-12">
                <div class="card shadow-css ">
                    <div class="row">
                        <div class=" col-lg-9 col-md-12 col-sm-12 my-auto">
                            <div id="barchart_values"></div>

                        </div>
                        <div class=" col-lg-3 col-md-12 col-sm-12">
                            <form action="/action_page.php">
                                <select class="form-control " id="form" name="form">
                                    <option>01-01-2020</option>
                                    <option>02-01-2020</option>
                                    <div class="form-group mb-2 ">
                                        <label for="email">Form</label>
                                        <option>03-01-2020</option>
                                        <option>04-01-2020</option>
                                        <option>05-01-2020</option>

                                </select>
                        </div>
                        <div class="form-group">
                            <label for="email">To</label>
                            <select class="form-control" id="form" name="to">
                                <option>01-01-2020</option>
                                <option>02-01-2020</option>
                                <option>03-01-2020</option>
                                <option>04-01-2020</option>
                                <option>05-01-2020</option>

                            </select>
                        </div>

                        </form>

                    </div>
                </div>
                <h5 class="text-center view_more">View Leads</h5>
            </div>
        </div>
    </div>
    <div class="row mb-5" id="appointemts">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="d-flex block_in_xm">
                <h3 class="mr-auto p-2 title_30 black_title mt-3 mb-3 w-100">Appointments
                    <samll class="font_18 float-right pt-2"><a href="{{ url('/appointments') }}">Sell all<i
                                class="fa fa-eye"></i></a></samll>
                </h3>
                {{--                    <p class="p-2 font_18 black_title">See all <i class="fa fa-eye"></i></p>--}}
            </div>
            @foreach($appointments->get() as $appointment)

                <div class="card shadow-css mb-2">
                    <div class="row pl-3 pr-3">
                        <div class="col-md-5 col-sm-4 d-flex">
                            <div class="image mr-3 my-auto">
                                <img src="{{asset('/')}}images/v2/pp.png" alt="" class="profile-image">
                            </div>
                            <div class="text my-auto">
                                <p class="text">{{$appointment->title}} </p>
                                <p class="sub-text">{{ Carbon\Carbon::parse($appointment->date)->format('d-M-Y') }}</p>

                            </div>
                        </div>
                        <div class=" col-md-3 col-sm-3 align-self-end">

                            <p class="font_14 ">{{ date('h:i A', strtotime($appointment->from)) }}
                                - {{date('h:i A', strtotime($appointment->to))}}</p>
                        </div>
                        <div class=" col-md-2 col-sm-2 align-self-end">
                            <p class="font_14 ">{!! $appointment->description !!}</p>
                        </div>
                        <div class=" col-md-2 col-sm-3 my-auto px-md-0">
                            <button type="button"
                                    class="view_appointment btn btn-outline-primary btn-sm"
                                    data-target=".view" data-id="{{ $appointment->id }}"
                                    data-toggle="modal">
                                <i class="fa fa-eye mr-2"></i>View Details
                            </button>
                        </div>
                    </div>

                </div>
            @endforeach
        </div>
        {{--<div class="col-lg-4 col-md-12 col-sm-12 mb-sm-4">
            <div class="d-flex">
                <h4 class="mr-auto p-2 title_30 black_title mt-3 mb-3">Current Leads Statuses</h4>
            </div>
            <div class="card shadow-css h-287">
                <div id="piechart"></div>
            </div>
        </div>--}}
    </div>
    <!--View Modal -->
    <div class="modal fade bd-example-modal-lg view" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary text-white font-weight-bold">
                    <h5 class="modal-title" id="exampleModalLongTitle">View Appointment</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-4 col-form-label">Title</label>
                        <div class="col-sm-8" id="title_show"></div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-4 col-form-label">Date</label>
                        <div class="col-sm-8" id="date_show"></div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-4 col-form-label">Time</label>
                        <div class="col-sm-8" id="time_show"></div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-4 col-form-label">Member of Staff</label>
                        <div class="col-sm-8" id="member_show"></div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-4 col-form-label">Description</label>
                        <div class="col-sm-8" id="description_show"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal End -->
@endsection
@section('scripts')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            let id = '', url = "{{route('appointments.show', '')}}", paginationUrl = '';
            $('#title_show').text('');
            $('#member_show').text('');
            $('#date_show').text('');
            $('#time_show').text('');

            $('.view_appointment').click(function () {
                id = $(this).data('id');
                $.ajax({
                    url: `${url}/${id}`,
                    success: function (result) {
                        $('#title_show').text(result.title);
                        $('#member_show').text(result.member.name);
                        $('#date_show').text(dateFormat(result.date));
                        $('#time_show').text(timeFormat(result.from) + ' To ' + timeFormat(result.to));
                        $('#description_show').text(result.description);
                    }
                });
            });
        });

        google.charts.load("current", {packages: ["corechart"]});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Genre', 'Pending', 'Dont Buy', 'Booked', 'Sold', 'Unresponsive',
                    'Spam', 'Literature', {role: 'annotation'}],
                // ['client 03', 10, 24, 20, 32, 18, 5, ''],
                // ['client 02', 16, 22, 23, 30, 16, 9, ''],
                ['client 01', 28, 19, 29, 30, 12, 13, 565, '']
            ]);
            var view = new google.visualization.DataView(data);
            view.setColumns([0, 1, 2, 3, 4, 5,
                {
                    calc: "stringify",
                    sourceColumn: 1,
                    type: "string",
                    role: "annotation"
                },
                2]);
            var options = {
                colors: [
                    '#f9b69c',
                    '#8bd6c1',
                    '#b8abf3',
                    '#fdeb89',
                    '#edcdf7',
                    '#d7d7d7'],
                height: 100,
                legend: {position: 'bottom', alignment: 'start', maxLines: 3},
                bar: {groupWidth: '30%'},

                isStacked: true
            };
            var chart = new google.visualization.BarChart(document.getElementById("barchart_values"));
            chart.draw(view, options);
        }
    </script>
    <script type="text/javascript">
        google.charts.load('current', {'packages': ['corechart']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Pending', 'Hours per Day'],
                ['Pending 50.45%', 50.45],
                ['Dont Buy 34.00%', 34.00],
                ['Booked 10.23%', 10.23],
                ['Sold', 5.98],
                ['Unresponsive 0.00%', 0.00],
                ['Spam 0.00%', 0.00]
            ]);
            var options = {
                pieHole: 0.6,
                colors: [
                    '#f9b69c',
                    '#8bd6c1',
                    '#b8abf3',
                    '#fdeb89',
                ],
                //
                height: 240,
                width: '100%',
                pieSliceText: 'label',

            };
            var chart = new google.visualization.PieChart(document.getElementById('piechart'));
            chart.draw(data, options);
        }
    </script>
@endsection






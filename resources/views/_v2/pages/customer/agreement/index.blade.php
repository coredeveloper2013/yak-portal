@extends('_v2.layout.dashboard')

@section('title') Agreements @endsection

@section('content')
    {{--@dd($agreements)--}}
    <customer-agreements-index
        agreements="{{ json_encode($agreements) }}"
    ></customer-agreements-index>
@endsection

@extends('_v2.layout.dashboard')
@section('title', 'Clients')
@section('top-bar-title',"Clients")
@section('content')
    <div class="client" id="client">
        <div class="row justify-content-between page-header">
            <div class="col-md-1">
                <h1 class="main-title mt-2">Clients</h1>
            </div>
            <div class="col-md-11 pr-0">
                <div class="ml-auto">
                    <nav class="nav nav-pills nav-tabs justify-content-end m-b-25" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab"
                           href="#onboarding"
                           role="tab"
                           aria-controls="nav-home" aria-selected="true">Onboarding</a>
                        <a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab"
                           href="#lp-build" role="tab"
                           aria-controls="nav-home" aria-selected="true">LP Build</a>

                        <a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab"
                           href="#lp-ir" role="tab"
                           aria-controls="nav-home" aria-selected="true">LP IR</a>
                        <a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab"
                           href="#lp-er" role="tab"
                           aria-controls="nav-home" aria-selected="true">LP ER</a>
                        <a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab"
                           href="#campaign" role="tab"
                           aria-controls="nav-home" aria-selected="true">Campaign Build</a>
                        <a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab"
                           href="#ia" role="tab"
                           aria-controls="nav-home" aria-selected="true">IA</a>
                        <a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab"
                           href="#live" role="tab"
                           aria-controls="nav-home" aria-selected="true">LIVE</a>
                        <a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab"
                           href="#paused" role="tab"
                           aria-controls="nav-home" aria-selected="true">PAUSED</a>
                        <a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab"
                           href="#lost" role="tab"
                           aria-controls="nav-home" aria-selected="true">LOST</a>
                    </nav>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="tab-content w-100" id="nav-tabContent">
                <div class="tab-pane fade show active" id="onboarding" role="tabpanel"
                     aria-labelledby="onboarding">
                    <div class="form-row filter-bar">
                        <div class="col-md-2">
                            <button class="btn btn-primary btn-block">+ Add New</button>
                        </div>

                        <div class="col-md-2">
                            <div class="dropdown">
                                <button class="btn btn-white btn-block bg-white dropdown-toggle" type="button"
                                        id="dropdownMenuButton" data-toggle="dropdown"
                                        aria-haspopup="true"
                                        aria-expanded="false">
                                    Sort By : <span class="text-primary font-weight-bolder">Client</span>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="search-form">
                                <input type="text" placeholder="Search" class="form-control">
                                <i data-feather="search" class="ml-3"></i>
                            </div>

                        </div>

                        <div class="col-md-4">
                            <button class="btn btn-outline-primary bg-white text-primary float-right pl-5 pr-5">
                                <i class="fas fa-sliders-h fa-rotate-90"></i>
                                Filter
                            </button>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-12 bg-white rounded table-responsive">
                            <br>
                            <table class="table p-4 table-borderless table-rounded">
                                <thead>
                                <tr>
                                    <th scope="col">Business Name</th>
                                    <th scope="col">Payment Total</th>
                                    <th scope="col">Max Ad Spend</th>
                                    <th scope="col">Top Ups</th>
                                    <th scope="col">Last Top Up</th>
                                    <th scope="col">Last Comment</th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                                <tbody class="">
                                <tr>
                                    <td scope="row"> Cash Forex Group</td>
                                    <td>£500</td>
                                    <td>£220</td>
                                    <td>1</td>
                                    <td> 1 days</td>
                                     <td>1</td>
                                    <td class="text-right">
                                        <button type="button"
                                                class="btn btn-outline-primary btn-small-text fixed-width-25">
                                            View Details
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row"> Cash Forex Group</td>
                                    <td>£500</td>
                                    <td>£220</td>
                                    <td>1</td>
                                    <td> 1 days</td>
                                     <td>1</td>
                                    <td class="text-right">
                                        <button type="button"
                                                class="btn btn-outline-primary btn-small-text fixed-width-25">
                                            View Details
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row"> Cash Forex Group</td>
                                    <td>£500</td>
                                    <td>£220</td>
                                    <td>1</td>
                                    <td> 1 days</td>
                                     <td>1</td>
                                    <td class="text-right">
                                        <button type="button"
                                                class="btn btn-outline-primary btn-small-text fixed-width-25">
                                            View Details
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row"> Cash Forex Group</td>
                                    <td>£500</td>
                                    <td>£220</td>
                                    <td>1</td>
                                    <td> 1 days</td>
                                     <td>1</td>
                                    <td class="text-right">
                                        <button type="button"
                                                class="btn btn-outline-primary btn-small-text fixed-width-25">
                                            View Details
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row"> Cash Forex Group</td>
                                    <td>£500</td>
                                    <td>£220</td>
                                    <td>1</td>
                                    <td> 1 days</td>
                                     <td>1</td>
                                    <td class="text-right">
                                        <button type="button"
                                                class="btn btn-outline-primary btn-small-text fixed-width-25">
                                            View Details
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row"> Cash Forex Group</td>
                                    <td>£500</td>
                                    <td>£220</td>
                                    <td>1</td>
                                    <td> 1 days</td>
                                     <td>1</td>
                                    <td class="text-right">
                                        <button type="button"
                                                class="btn btn-outline-primary btn-small-text fixed-width-25">
                                            View Details
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row"> Cash Forex Group</td>
                                    <td>£500</td>
                                    <td>£220</td>
                                    <td>1</td>
                                    <td> 1 days</td>
                                     <td>1</td>
                                    <td class="text-right">
                                        <button type="button"
                                                class="btn btn-outline-primary btn-small-text fixed-width-25">
                                            View Details
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row"> Cash Forex Group</td>
                                    <td>£500</td>
                                    <td>£220</td>
                                    <td>1</td>
                                    <td> 1 days</td>
                                     <td>1</td>
                                    <td class="text-right">
                                        <button type="button"
                                                class="btn btn-outline-primary btn-small-text fixed-width-25">
                                            View Details
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row"> Cash Forex Group</td>
                                    <td>£500</td>
                                    <td>£220</td>
                                    <td>1</td>
                                    <td> 1 days</td>
                                     <td>1</td>
                                    <td class="text-right">
                                        <button type="button"
                                                class="btn btn-outline-primary btn-small-text fixed-width-25">
                                            View Details
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row"> Cash Forex Group</td>
                                    <td>£500</td>
                                    <td>£220</td>
                                    <td>1</td>
                                    <td> 1 days</td>
                                     <td>1</td>
                                    <td class="text-right">
                                        <button type="button"
                                                class="btn btn-outline-primary btn-small-text fixed-width-25">
                                            View Details
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row"> Cash Forex Group</td>
                                    <td>£500</td>
                                    <td>£220</td>
                                    <td>1</td>
                                    <td> 1 days</td>
                                     <td>1</td>
                                    <td class="text-right">
                                        <button type="button"
                                                class="btn btn-outline-primary btn-small-text fixed-width-25">
                                            View Details
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row"> Cash Forex Group</td>
                                    <td>£500</td>
                                    <td>£220</td>
                                    <td>1</td>
                                    <td> 1 days</td>
                                     <td>1</td>
                                    <td class="text-right">
                                        <button type="button"
                                                class="btn btn-outline-primary btn-small-text fixed-width-25">
                                            View Details
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                            <nav aria-label="Page navigation example" style="float: left">
                                <select name="" id="" class="form-control">
                                    <option value="">20</option>
                                    <option value="">50</option>
                                    <option value="">100</option>
                                </select>
                            </nav>
                            <nav aria-label="Page navigation example" style="float: right">
                                <ul class="pagination">
                                    <li class="page-item">
                                        <a class="page-link" href="#" aria-label="Previous">
                                            {{-- <span aria-hidden="true" >&#8249;</span> --}}
                                            <i class="fas fa-chevron-left"></i>
                                        </a>
                                    </li>
                                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item"><a class="page-link" href="#">...</a></li>
                                    <li class="page-item">
                                        <a class="page-link" href="#" aria-label="Next">
                                            <i class="fas fa-chevron-right"></i>
                                            {{-- <span aria-hidden="true">&#8250;</span> --}}
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="lp-build" role="tabpanel"
                     aria-labelledby="lp-build">
                    <div class="form-row filter-bar">
                                    <div class="col-md-2">
                                        <button class="btn btn-primary btn-block">+ Add New</button>
                                    </div>

                        <div class="col-md-2">
                            <div class="dropdown">
                                <button class="btn btn-white btn-block bg-white dropdown-toggle" type="button"
                                        id="dropdownMenuButton" data-toggle="dropdown"
                                        aria-haspopup="true"
                                        aria-expanded="false">
                                    Sort By : <span class="text-primary font-weight-bolder">Client</span>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="search-form">
                                <input type="text" placeholder="Search" class="form-control">
                                <i data-feather="search" class="ml-3"></i>
                            </div>

                        </div>

                        <div class="col-md-4">
                            <button class="btn btn-outline-primary bg-white text-primary float-right pl-5 pr-5">
                                <i class="fas fa-sliders-h fa-rotate-90"></i>
                                Filter
                            </button>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-12 bg-white rounded table-responsive">
                            <br>
                            <table class="table p-4 table-borderless table-rounded">
                                <thead>
                                <tr>
                                    <th scope="col">Business Name</th>
                                    <th scope="col">Payment Total</th>
                                    <th scope="col">Max Ad Spend</th>
                                    <th scope="col">Top Ups</th>
                                    <th scope="col">Last Top Up</th>
                                    <th scope="col">Last Comment</th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                                <tbody class="">
                                <tr>
                                    <td scope="row"> Cash Forex Group</td>
                                    <td>£500</td>
                                    <td>£220</td>
                                    <td>1</td>
                                    <td> 1 days</td>
                                     <td>1</td>
                                    <td class="text-right">
                                        <button type="button"
                                                class="btn btn-outline-primary btn-small-text fixed-width-25">
                                            View Details
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row"> Cash Forex Group</td>
                                    <td>£500</td>
                                    <td>£220</td>
                                    <td>1</td>
                                    <td> 1 days</td>
                                     <td>1</td>
                                    <td class="text-right">
                                        <button type="button"
                                                class="btn btn-outline-primary btn-small-text fixed-width-25">
                                            View Details
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                            <nav aria-label="Page navigation example" style="float: left">
                                <select name="" id="" class="form-control">
                                    <option value="">20</option>
                                    <option value="">50</option>
                                    <option value="">100</option>
                                </select>
                            </nav>
                            <nav aria-label="Page navigation example" style="float: right">
                                <ul class="pagination">
                                    <li class="page-item">
                                        <a class="page-link" href="#" aria-label="Previous">
                                            {{-- <span aria-hidden="true" >&#8249;</span> --}}
                                            <i class="fas fa-chevron-left"></i>
                                        </a>
                                    </li>
                                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item"><a class="page-link" href="#">...</a></li>
                                    <li class="page-item">
                                        <a class="page-link" href="#" aria-label="Next">
                                            <i class="fas fa-chevron-right"></i>
                                            {{-- <span aria-hidden="true">&#8250;</span> --}}
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="lp-ir" role="tabpanel"
                     aria-labelledby="lp-ir">
                    <div class="form-row filter-bar">
                                    <div class="col-md-2">
                                        <button class="btn btn-primary btn-block">+ Add New</button>
                                    </div>

                        <div class="col-md-2">
                            <div class="dropdown">
                                <button class="btn btn-white btn-block bg-white dropdown-toggle" type="button"
                                        id="dropdownMenuButton" data-toggle="dropdown"
                                        aria-haspopup="true"
                                        aria-expanded="false">
                                    Sort By : <span class="text-primary font-weight-bolder">Client</span>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="search-form">
                                <input type="text" placeholder="Search" class="form-control">
                                <i data-feather="search" class="ml-3"></i>
                            </div>

                        </div>

                        <div class="col-md-4">
                            <button class="btn btn-outline-primary bg-white text-primary float-right pl-5 pr-5">
                                <i class="fas fa-sliders-h fa-rotate-90"></i>
                                Filter
                            </button>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-12 bg-white rounded table-responsive">
                            <br>
                            <table class="table p-4 table-borderless table-rounded">
                                <thead>
                                <tr>
                                    <th scope="col">Business Name</th>
                                    <th scope="col">Payment Total</th>
                                    <th scope="col">Max Ad Spend</th>
                                    <th scope="col">Top Ups</th>
                                    <th scope="col">Last Top Up</th>
                                    <th scope="col">Last Comment</th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                                <tbody class="">
                                <tr>
                                    <td scope="row"> Cash Forex Group</td>
                                    <td>£500</td>
                                    <td>£220</td>
                                    <td>1</td>
                                    <td> 1 days</td>
                                     <td>1</td>
                                    <td class="text-right">
                                        <button type="button"
                                                class="btn btn-outline-primary btn-small-text fixed-width-25">
                                            View Details
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row"> Cash Forex Group</td>
                                    <td>£500</td>
                                    <td>£220</td>
                                    <td>1</td>
                                    <td> 1 days</td>
                                     <td>1</td>
                                    <td class="text-right">
                                        <button type="button"
                                                class="btn btn-outline-primary btn-small-text fixed-width-25">
                                            View Details
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row"> Cash Forex Group</td>
                                    <td>£500</td>
                                    <td>£220</td>
                                    <td>1</td>
                                    <td> 1 days</td>
                                     <td>1</td>
                                    <td class="text-right">
                                        <button type="button"
                                                class="btn btn-outline-primary btn-small-text fixed-width-25">
                                            View Details
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row"> Cash Forex Group</td>
                                    <td>£500</td>
                                    <td>£220</td>
                                    <td>1</td>
                                    <td> 1 days</td>
                                     <td>1</td>
                                    <td class="text-right">
                                        <button type="button"
                                                class="btn btn-outline-primary btn-small-text fixed-width-25">
                                            View Details
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row"> Cash Forex Group</td>
                                    <td>£500</td>
                                    <td>£220</td>
                                    <td>1</td>
                                    <td> 1 days</td>
                                     <td>1</td>
                                    <td class="text-right">
                                        <button type="button"
                                                class="btn btn-outline-primary btn-small-text fixed-width-25">
                                            View Details
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row"> Cash Forex Group</td>
                                    <td>£500</td>
                                    <td>£220</td>
                                    <td>1</td>
                                    <td> 1 days</td>
                                     <td>1</td>
                                    <td class="text-right">
                                        <button type="button"
                                                class="btn btn-outline-primary btn-small-text fixed-width-25">
                                            View Details
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row"> Cash Forex Group</td>
                                    <td>£500</td>
                                    <td>£220</td>
                                    <td>1</td>
                                    <td> 1 days</td>
                                     <td>1</td>
                                    <td class="text-right">
                                        <button type="button"
                                                class="btn btn-outline-primary btn-small-text fixed-width-25">
                                            View Details
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td scope="row"> Cash Forex Group</td>
                                    <td>£500</td>
                                    <td>£220</td>
                                    <td>1</td>
                                    <td> 1 days</td>
                                     <td>1</td>
                                    <td class="text-right">
                                        <button type="button"
                                                class="btn btn-outline-primary btn-small-text fixed-width-25">
                                            View Details
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                            <nav aria-label="Page navigation example" style="float: left">
                                <select name="" id="" class="form-control">
                                    <option value="">20</option>
                                    <option value="">50</option>
                                    <option value="">100</option>
                                </select>
                            </nav>
                            <nav aria-label="Page navigation example" style="float: right">
                                <ul class="pagination">
                                    <li class="page-item">
                                        <a class="page-link" href="#" aria-label="Previous">
                                            {{-- <span aria-hidden="true" >&#8249;</span> --}}
                                            <i class="fas fa-chevron-left"></i>
                                        </a>
                                    </li>
                                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item"><a class="page-link" href="#">...</a></li>
                                    <li class="page-item">
                                        <a class="page-link" href="#" aria-label="Next">
                                            <i class="fas fa-chevron-right"></i>
                                            {{-- <span aria-hidden="true">&#8250;</span> --}}
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

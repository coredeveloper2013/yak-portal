@extends('_v2.layout.dashboard')

@section('title') Getting Started @endsection
@section('top') Home 2 @endsection

@section('content')
    @php
        $header = $gettingStarteds[1][0];
        $other  = $gettingStarteds[0];
    @endphp
    <div id="dashboard">
        <div class="row mb-4">
            <div class="col-sm-12 col-md-12">
                <div class="card top-card mt-4">
                    <div class="ribon">
                        <div class="top-vector">
                            <img src="{{ asset('images/v2/Vector 3.png') }}" alt="">
                            <p><i class="fas fa-check"></i></p>
                        </div>
                        <div class="small-vector">
                            <img src="{{ asset('images/v2/Vector 4.png') }}" alt="">
                        </div>

                    </div>

                    <div class="one"><img src="images/v2/one.png" alt=""></div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4 col-sm-12 top-card-img">
                                <img src="{{ asset($header->image) }}" alt="">
                            </div>
                            <div class="col-md-8 col-sm-12 position-relative">

                                <h1 class="main-title">{{ $header->title }}</h1>
                                <p class="sub-title" style="margin-top: 29px">{{ $header->description }}</p>
                                <div class="text-right" style="margin-top: 53px">
                                    {{--<a href="{{ url('/appointments') }}" class="btn w-19px mr-lg-2 btn-info">Book Appointment</a>--}}

                                    @if(array_key_exists($header->id, $myGettingStartedHistories) && $myGettingStartedHistories[$header->id]['is_marked'])
                                        <button type="button" class="btn w-19px btn-success text-white" style="cursor: default;">Marked as Done</button>
                                    @else
                                        <a href="{{ route('_v2.customer.getting-started.markAsDone', [Auth()->user()->id, $header->id, 1]) }}" type="button" class="btn w-19px btn-light text-primary">Mark as Done</a>
                                    @endif
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mb-4">
            <div class="col-sm-12 col-md-12">
                <div class="row">
                    @foreach($other as $val)
                    <div class="col-md-3 col-sm-12 mb-sm-4 ">
                        <div class="card">
                            <div class="card-body  position-relative p-md-2">
                                <div class="vector text-right">
                                    @if(array_key_exists($val->id, $myGettingStartedHistories) && $myGettingStartedHistories[$val->id]['is_marked'])
                                        <img src="{{ asset('images/v2/svg/Vector-ok.svg') }}" alt="">
                                    @else
                                        <img src="{{ asset('images/v2/svg/Vector.svg') }}" alt="">
                                    @endif
                                </div>
                                <img src="{{ asset($val->image) }}" alt="" class="card-img" style="height: 234px;">
                                <div class="card-text">
                                    <h3 class="text-center">{!! $val->title !!}</h3>
                                    <p class="text-center">{!! $val->description !!}</p>
                                </div>
                            </div>

                            <div class="card-footer border-0">
                                <div class="text-center">
                                    @if(array_key_exists($val->id, $myGettingStartedHistories) && $myGettingStartedHistories[$val->id]['is_marked'])
                                        <button type="button" class="btn w-19px btn-success text-white" style="cursor: default;">Marked as Done</button>
                                    @else
                                        <a href="{{ route('_v2.customer.getting-started.markAsDone', [Auth()->user()->id, $val->id, 1]) }}" type="button" class="btn w-19px btn-light text-primary">Mark as Done</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>

        </div>
    </div>
@endsection

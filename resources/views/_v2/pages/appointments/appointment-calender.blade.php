@extends('_v2.layout.dashboard')
@section('title') Appointment Calender @endsection
@section('layout') container-fluid appointment @endsection

@section('content')
    <staff-calender-appointment-calender members="{{ json_encode($members) }}" events="{{$events}}" dots="{{ $dots }}"></staff-calender-appointment-calender>
@endsection



@extends('_v2.layout.dashboard')
@section('title') Home @endsection
@section('layout') container-fluid appointment @endsection
@section('css')
    <link rel="stylesheet" href="{{ asset('fullcalendar-5.3.2/lib/main.min.css') }}">
@endsection
@section('content')
    <div id="appointmentCalender">
        <div id="dashboard">
            <div class="row justify-content-between page-header">
                <div class="col-md-6">
                    <h1 class="main-title">Appointments</h1>
                </div>
            </div>

            <div class="form-row filter-bar">
                <div class="col-md-2">
                    <button class="btn btn-primary btn-block" data-target=".bd-example-modal-lg" data-toggle="modal"
                            type="button">+ New Appointments
                    </button>
                </div>

                <div class="col-md-2">

                </div>
                <div class="col-md-4">


                </div>

                <div class="col-md-4">
                    <button class="btn btn-outline-primary bg-white text-primary float-right pl-5 pr-5">
                        <i class="fas fa-sliders-h fa-rotate-90"></i>
                        Filter
                    </button>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">
                    <div class="main-content-area">
                        <div class="row">
                            <div class="col-xl-9">
                                <div class="schedule-callender-wrap">
                                    <div class="calendar-wrap">
                                        <div id='calendar'></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3">
                                <div class="dash-sidebar">
                                    <div class="team-members">
                                        <h3>Team Member</h3>
                                        <ul>
                                            <li><img src="images/check-green.svg" alt="">My Calander</li>
                                            <li><img src="images/check-red.svg" alt="">John mosey</li>
                                            <li><img src="images/check-yellow.svg" alt="">James Bishop</li>
                                            <li><img src="images/check-perano.svg" alt="">Adam James</li>
                                        </ul>
                                    </div>
                                    <div class="sidebar-calender">
                                        <div id="pb-calendar" class="pb-calendar"></div>
                                    </div>
                                </div>
                                <div class="dash-sidebar pt-0 mt-0">
                                    <div id="calender_datepicker" data-date="{{date('m/d/Y')}}"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="calendar_basic" style="width: 1000px; height: 350px;"></div>

                    {{-- <nav aria-label="Page navigation example" style="float: right">
                        <ul class="pagination">
                            <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                                <i class="fas fa-chevron-left"></i>
                            </a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">...</a></li>
                            <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                                <i class="fas fa-chevron-right"></i>
                            </a>
                            </li>
                        </ul>
                    </nav> --}}
                </div>
            </div>
        </div>

        <!--Create Modal -->
        <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-primary text-white font-weight-bold">
                        <h5 class="modal-title" id="exampleModalLongTitle">Add Appointments</h5>
                        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{route('appointments.store')}}" method="post">
                            @csrf
                            <div class="form-group row">
                                <label for="title" class="col-sm-4 col-form-label">Title</label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="title" id="title">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="member_id" class="col-sm-4 col-form-label">Member</label>
                                <div class="col-sm-8">
                                    <select class="select2 custom-select" name="member_id">
                                        <option selected="" disabled="">Select Member</option>
                                        @foreach($members as $member)
                                            <option value="{{  $member->id }}">{{ $member->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="date" class="col-sm-4 col-form-label">Date</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control datetimepicker" name="date" id="date">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="from" class="col-sm-4 col-form-label">From (time)</label>
                                <div class="col-sm-8">
                                    <input type="time" class="form-control timepicker" name="from" id="from">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="to" class="col-sm-4 col-form-label">To (time)</label>
                                <div class="col-sm-8">
                                    <input type="time" class="form-control timepicker" name="to" id="to">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="description" class="col-sm-4 col-form-label">Description</label>
                                <div class="col-sm-8">
                                    <textarea name="description" id="description" class="form-control"></textarea>
                                    {{--                                <input class="form-control" name="description" id="description">--}}
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="reset" class="btn btn-light">Reset</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal End -->
    </div>


@endsection



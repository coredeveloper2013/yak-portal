@extends('_v2.layout.dashboard')

@section('title') Appointment List @endsection

@section('content')
    <div id="dashboard" class="invoice">
        <div class="row justify-content-between page-header">
            <div class="col-md-6">
                <h1 class="main-title">Appointments</h1>
            </div>
        </div>
        {{--<div class="form-row filter-bar">
            <div class="col-md-2">

            </div>

            <div class="col-md-10">
                <button class="btn btn-outline-primary bg-white text-primary float-right pl-5 pr-5">
                    <i class="fas fa-sliders-h fa-rotate-90"></i>
                    Filter
                </button>
            </div>
        </div>--}}

        <div class="row mb-3">
            <div class="col-md-12 bg-white rounded table-responsive bg-transparent">
                <table class="table p-4 table-borderless table-rounded my-3">
                    <thead>
                    <tr>
                        <th scope="col" class="text-nowrap">Title</th>
                        <th scope="col" class="text-nowrap">Date</th>
                        <th scope="col" class="text-nowrap">Time</th>
                        <th scope="col" class="text-nowrap" colspan="2">Member of Staff</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($appointments)>0)
                        @foreach($appointments->groupBy('date') as $key=>$appointment)
                            <tr class="no-hover">
                                <td colspan="5" class="pb-1 pt-4 text-left mute">
                                    {{-- {{ \Carbon\Carbon::parse($key)->calendar()  }} --}}
                                    @if(\Carbon\Carbon::parse($key)->isTomorrow())
                                        Tomorrow
                                    @elseif(\Carbon\Carbon::parse($key)->isToday())
                                        Today
                                    @elseif(\Carbon\Carbon::parse($key)->isToday())
                                        Today
                                    @elseif(\Carbon\Carbon::parse($key)->isYesterday())
                                        Yesterday
                                    @else
                                        {{\Carbon\Carbon::parse($key)->format('d-M-Y')  }}
                                    @endif
                                </td>
                            </tr>
                            @foreach($appointment as $_appointment)
                                <tr class="bg-white">
                                    <td class="text-nowrap">
                                        {{$_appointment->title}}
                                    </td>
                                    <td class="text-nowrap mute">
                                        {{ \Carbon\Carbon::parse($_appointment->date)->format('d-M-Y')  }}
                                    </td>
                                    <td class="text-nowrap mute">
                                        {{ $_appointment->from }} - {{ $_appointment->to }}
                                    </td>
                                    <td class="text-nowrap mute">
                                        {{ $_appointment->member->name ?? '' }}
                                    </td>
                                    <td class="text-right">
                                        <button type="button"
                                                class="view_appointment btn btn-outline-primary btn-small-text fixed-width-25"
                                                data-target=".view" data-id="{{ $_appointment->id }}"
                                                data-toggle="modal">
                                            View More
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        @endforeach
                    @else
                        <tr class="bg-white">
                            <td class="text-center" colspan="4">
                               <strong> No Data found</strong>
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                @if($appointments->lastPage() > 1)
                    <div class="d-flex justify-content-between">
                        <nav aria-label="Page navigation example" style="float: left">
                            <select name="" id="per-page-items" class="form-control">
                                <option
                                    value="20" {{ request()->filled('items') ? (request()->items == '20' ? 'selected':''):'selected' }}>
                                    20
                                </option>
                                <option value="50" {{ request()->items == '50' ? 'selected':'' }}>50</option>
                                <option value="100" {{ request()->items == '100' ? 'selected':'' }}>100</option>
                            </select>
                        </nav>

                        {{ $appointments->links() }}
                        {{-- {{ $appointments->appends(['items' => request()->items ? request()->items:20])->links() }} --}}
                    </div>
                @endif
            </div>
        </div>
    </div>

    <!--Create Modal -->
    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary text-white font-weight-bold">
                    <h5 class="modal-title" id="exampleModalLongTitle">Add Appointments</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('appointments.store')}}" method="post">
                        @csrf
                        <div class="form-group row">
                            <label for="title" class="col-sm-4 col-form-label">Title</label>
                            <div class="col-sm-8">
                                <input class="form-control" name="title" id="title">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="member_id" class="col-sm-4 col-form-label">Member</label>
                            <div class="col-sm-8">
                                <select class="select2 custom-select" name="member_id">
                                    <option selected="" disabled="">Select Member</option>
                                    @foreach($members as $member)
                                        <option value="{{  $member->id }}">{{ $member->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="date" class="col-sm-4 col-form-label">Date</label>
                            <div class="col-sm-8">
                                <input type="date" class="form-control" name="date" id="date">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="from" class="col-sm-4 col-form-label">From (time)</label>
                            <div class="col-sm-8">
                                <input type="time" class="form-control" name="from" id="from">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="to" class="col-sm-4 col-form-label">To (time)</label>
                            <div class="col-sm-8">
                                <input type="time" class="form-control" name="to" id="to">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="description" class="col-sm-4 col-form-label">Description</label>
                            <div class="col-sm-8">
                                <textarea name="description" id="description" class="form-control"></textarea>
                                {{--                                <input class="form-control" name="description" id="description">--}}
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-light">Reset</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal End -->

    <!--View Modal -->
    <div class="modal fade bd-example-modal-lg view" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary text-white font-weight-bold">
                    <h5 class="modal-title" id="exampleModalLongTitle">View Appointment</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-4 col-form-label">Title</label>
                        <div class="col-sm-8" id="title_show"></div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-4 col-form-label">Date</label>
                        <div class="col-sm-8" id="date_show"></div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-4 col-form-label">Time</label>
                        <div class="col-sm-8" id="time_show"></div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-4 col-form-label">Member of Staff</label>
                        <div class="col-sm-8" id="member_show"></div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-4 col-form-label">Description</label>
                        <div class="col-sm-8" id="description_show"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal End -->
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            let id = '', url = "{{route('appointments.show', '')}}", paginationUrl = '';
            $('#title_show').text('');
            $('#member_show').text('');
            $('#date_show').text('');
            $('#time_show').text('');

            $('.view_appointment').click(function () {
                id = $(this).data('id');
                $.ajax({
                    url: `${url}/${id}`,
                    success: function (result) {
                        $('#title_show').text(result.title);
                        $('#member_show').text(result.member.name);
                        $('#date_show').text(dateFormat(result.date));
                        $('#time_show').text(timeFormat(result.from) + ' To ' + timeFormat(result.to));
                        $('#description_show').text(result.description);
                    }
                });
            });


            $('#per-page-items').on('change', function () {
                let items = $(this).val();
                let page = "{{request()->page}}";
                //paginationUrl = `${url}?items=${items}&page=${page}`;
                paginationUrl = `${url}?items=${items}&page=1`;

                window.location = paginationUrl;
            });

            $('#dropdownMenuButton > a').click(function () {
                $('#sort').val($(this).data('value'));
                $('#ordering').text($(this).text());
            });
        });
    </script>
@endsection

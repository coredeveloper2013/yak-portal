@extends('_v2.layout.dashboard')

@section('title',"Dashboard")
@section('top-bar-title',"Profile")

@section('content')
    <div class="dashboard profile-page">
        <div class="row justify-content-between page-header">
            <div class="col-md-2">
                <h1 class="main-title mt-2">My profile</h1>
            </div>
            <div class="col-md-10 pr-0">
                <div class="ml-auto">
                    <nav class="nav nav-pills nav-tabs justify-content-end" id="nav-tab" role="tablist">
                        <a  @if (!\Session::has('company') && !\Session::has('password'))
                            class="nav-item nav-link active"
                            @else
                            class="nav-item nav-link" @endif id="nav-home-tab" data-toggle="tab"
                           href="#your-details"
                           role="tab"
                           aria-controls="nav-home" aria-selected="true">YOUR DETAILS</a>
                        <a  @if (\Session::has('company'))
                            class="nav-item nav-link active"
                            @else
                            class="nav-item nav-link" @endif id="nav-home-tab" data-toggle="tab"
                           href="#business-details" role="tab"
                           aria-controls="nav-home" aria-selected="true">BUSINESS DETAILS</a>
                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#integrations"
                           role="tab"
                           aria-controls="nav-profile" aria-selected="false">INTEGRATIONS</a>
                        <a @if (\Session::has('password'))
                           class="nav-item nav-link active"
                           @else
                           class="nav-item nav-link" @endif id="nav-contact-tab" data-toggle="tab"
                           href="#account-details" role="tab"
                           aria-controls="nav-contact" aria-selected="false">ACCOUNT DETAILS</a>
                    </nav>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="tab-content w-100" id="nav-tabContent">
                <div @if (!\Session::has('company') && !\Session::has('password')) class="tab-pane fade active show" @else class="tab-pane fade" @endif  id="your-details" role="tabpanel"
                     aria-labelledby="nav-home-tab">
                    <p class="nav-sub-title rounded">YOUR DETAILS</p>
                    <div class="card bg-white rounded">
                        <form method="POST" action="{{route('update.profile')}}"
                              enctype="multipart/form-data" autocomplete="off">
                            @csrf
                            <div class="form-group row mb-4">
                                <label class="col-sm-3 col-form-label">Business Name</label>
                                <div class="col-sm-5">
                                    <input id="companyName"
                                           name="companyName" @if(Auth::user()->company->name == null)
                                           @else value="{{Auth::user()->company->name}}" @endif type="text" class="form-control input-border rounded">
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-sm-3 col-form-label">Your Name</label>
                                <div class="col-sm-5">
                                    <input type="text" @if(Auth::user()->name == null)
                                           @else value="{{Auth::user()->name}}" @endif id="name" name="name" class="form-control input-border rounded">
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-sm-3 col-form-label">Trading Name</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control input-border rounded">
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-sm-3 col-form-label">Contact Number</label>
                                <div class="col-sm-5">
                                    <input type="text" id="phone" name="phone" @if(Auth::user()->phone == null)
                                           @else value="{{Auth::user()->phone}}" @endif class="form-control input-border rounded">
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-sm-3 col-form-label">Website</label>
                                <div class="col-sm-5">
                                    <input type="text" id="website" name="website"  @if(Auth::user()->business_info->website == null)
                                    @else value="{{Auth::user()->business_info->website}}" @endif class="form-control input-border rounded">
                                </div>
                            </div>
                            @if (\Session::has('general') && \Session::get('general') != '')
                                <div class="alert alert-success">
                                    <small>{!! \Session::get('general') !!}</small>
                                </div>
                            @endif
                            <div class="form-group row mb-4">
                                <label for="save" class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-primary btn-block">Save</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
                <div class="tab-pane fade" id="business-details" role="tabpanel"
                     aria-labelledby="nav-home-tab">
                    <p class="nav-sub-title rounded">BUSINESS DETAILS</p>
                    <div class="card bg-white rounded">
                        <form>
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group mb-4">
                                        <label class="col-form-label">What does your business
                                            do?</label>
                                        <div class="">
                                            <textarea rows="3" class="form-control input-border rounded"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group mb-4">
                                        <label class="col-form-label">Describe your idea
                                            client?</label>
                                        <div class="">
                                            <textarea rows="3" class="form-control input-border rounded"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group mb-4">
                                        <label class="col-form-label">What locations do you want to
                                            target?</label>
                                        <div class="">
                                            <textarea rows="3" class="form-control input-border rounded"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group mb-4">
                                        <label class="col-form-label">Any reviews (or links review
                                            pages about your business) </label>
                                        <div class="">
                                            <textarea rows="3" class="form-control input-border rounded"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-12 col-form-label my-3">What are 4 frequently Asked
                                    Questions you get asked by potential customers? </label>
                                <div class="col-12 col-md-6">
                                    <div class="questions-answer-section">
                                        <div class="form-group mb-4">
                                            <input type="text" class="form-control input-border rounded" placeholder="Questions" id="questions1">
                                        </div>
                                        <div class="form-group mb-4 question_1">
                                            <textarea rows="4" class="form-control input-border rounded" placeholder="Answer"></textarea>
                                        </div>
                                    </div>
                                    <div class="questions-answer-section">
                                        <div class="form-group mb-4">
                                            <input type="text" class="form-control input-border rounded" placeholder="Questions" id="questions2">
                                        </div>
                                        <div class="form-group mb-4 question_2">
                                            <textarea rows="4" class="form-control input-border rounded" placeholder="Answer"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="questions-answer-section">
                                        <div class="form-group mb-4">
                                            <input type="text" class="form-control input-border rounded" placeholder="Questions" id="questions3">
                                        </div>
                                        <div class="form-group mb-4 question_3">
                                            <textarea rows="4" class="form-control input-border rounded" placeholder="Answer"></textarea>
                                        </div>
                                    </div>
                                    <div class="questions-answer-section">
                                        <div class="form-group mb-4">
                                            <input type="text" class="form-control input-border rounded" placeholder="Questions" id="questions4">
                                        </div>
                                        <div class="form-group mb-4 question_4">
                                            <textarea rows="4" class="form-control input-border rounded" placeholder="Answer"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group mb-4 float-right">
                                        <button class="btn btn-primary" type="button">+ Add Logo</button> &nbsp; &nbsp;
                                        <button class="btn btn-primary" type="button">+ Additional Photos</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group mb-4">
                                        <label class="col-form-label">Anything else you would like us
                                            to know? </label>
                                        <div class="">
                                            <textarea rows="3" class="form-control input-border rounded"></textarea>
                                        </div>
                                    </div>
                                    @if (\Session::has('company') && \Session::get('company') != '')
                                        <div class="alert alert-success">
                                            <small>{!! \Session::get('company') !!}</small>
                                        </div>
                                    @endif
                                    <div class="form-group mb-4">
                                        <div class="col-12 col-md-4 offset-0 offset-md-4">
                                            <button type="submit" class="btn btn-primary btn-block">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-pane fade" id="integrations" role="tabpanel">

                    <p class="nav-sub-title rounded">INTEGRATIONS</p>
                    <div class="card bg-white rounded">
                        <div class="row align-items-center justify-content-center" style="height:60vh;">
                            <div>This is feature we are currently working on. Check book soon!:)</div>
                        </div>
                    </div>

                </div>
                <div @if (\Session::has('password')) class="tab-pane fade active show" @else class="tab-pane fade" @endif id="account-details" role="tabpanel"
                     aria-labelledby="nav-contact-tab">

                    <p class="nav-sub-title rounded">ACCOUNT DETAILS</p>
                    <div class="card bg-white rounded">
                        <form action="{{route('update.password')}}" method="POST"
                              autocomplete="off">
                            @csrf
                            <div class="form-group row mb-4">
                                <label class="col-sm-3 col-form-label">Email</label>
                                <div class="col-sm-5">
                                    <input type="email" class="form-control input-border rounded" id="email">
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="password" class="col-sm-3 col-form-label">Change Password</label>
                                <div class="col-sm-5">
                                    <input type="password" class="form-control input-border rounded" id="old_password"
                                           name="old_password" aria-describedby="emailHelp"
                                           placeholder="Enter your current password">
                                    @error('old_password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-4">
                                <label for="password" class="col-sm-3 col-form-label">New Password</label>
                                <div class="col-sm-5">
                                    <input type="password" class="form-control input-border rounded" id="new_password"
                                           name="password" aria-describedby="emailHelp"
                                           placeholder="Enter your new password">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="confirm-password" class="col-sm-3 col-form-label">Confirm
                                    Password</label>
                                <div class="col-sm-5">
                                    <input type="password" class="form-control input-border rounded" id="confirm_new_pw"
                                           name="password_confirmation" aria-describedby="emailHelp"
                                           placeholder="Confirm your new password">
                                </div>
                            </div>
                            @if (\Session::has('password') && \Session::get('password') != '')
                                <div class="alert alert-success">
                                    <small>{!! \Session::get('password') !!}</small>
                                </div>
                            @endif
                            <div class="form-group row mb-4">
                                <label for="save" class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-primary btn-block">Save</button>
                                </div>
                            </div>

                        </form>
                    </div>

                </div>
            </div>
        </div>
@endsection




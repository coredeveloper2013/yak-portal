new Vue({
    el: '#campaigns',
    mounted() {
        this.getUserCampaigns(false);
        var context = this;
        $('input[name="daterange"]').daterangepicker({
            opens: "center",
            maxDate: moment(),
            autoUpdateInput: false,
            orientation: "auto",
            locale: {
                cancelLabel: "Clear"
            }
        });
        $('input[name="daterange"]').on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("MM/DD/YYYY") + " - " + picker.endDate.format("MM/DD/YYYY"));
            context.filter.selectedStartDate = picker.startDate.format("MM/DD/YYYY");
            context.filter.selectedEndDate = picker.endDate.format("MM/DD/YYYY");
            context.getUserCampaigns(false);
        });
        $('input[name="daterange"]').on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
            context.filter.selectedStartDate = "";
            context.filter.selectedEndDate = "";
            context.getUserCampaigns(false);
        });
    },
    data: {
        fields: [
            {key: 'id', label: 'Campaign ID'}, 
            {key: 'date', label: 'Start Date - End Date'}, 
            {key: 'name', label: 'Campaign Name'},
            {key: 'landing_page_name', label: 'Landing Page'}, 
            {key: 'amount', label: 'Budget'}, 
            'type', 
            'status', 
            'Actions'
        ],
        campaigns: [],
        campaignsMeta: {},
        filter: {
            selectedStartDate: '',
            selectedEndDate: '',
            status: '',
        },
        isBusy: false,
        getCampaingsUrl : App_url + '/data',
        leadsUrl : main_url + '/leads',
    },
    watch: {
        'campaignsMeta.current_page': function (val) {
            this.loadPaginatedData();
        }
    },
    methods: {
        getLeadsUrl: function(id){
            return `${this.leadsUrl}/${id}` ;
        },
        loadPaginatedData: function () {
            this.getUserCampaigns(this.getCampaingsUrl + '?page=' + this.campaignsMeta.current_page);
        },
        getUserCampaigns: function (url = false) {
            this.isBusy = true;
            if(url == false){
                url = this.getCampaingsUrl;
            }
            axios.post(url, {filter: this.filter})
                .then(response => {
                    if(response.data.status == 'success'){
                        this.isBusy = false;
                        this.campaigns = response.data.data;
                        this.campaignsMeta = response.data.meta;
                    }
                }).catch(error => {
                    console.log(error);
                    this.isBusy = false;
            });
        },
        resetFilter: function(){
            this.filter = {
                selectedStartDate : '',
                selectedEndDate : '',
                status : ''
            };
            $("#daterange").val('');
            this.getUserCampaigns(false);
        },
        numberWithComma: function(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        },
        Filtered(filteredItems) {
            // Trigger pagination to update the number of buttons/pages due to filtering
            this.campaignsMeta.totalRows = filteredItems.length;
            this.campaignsMeta.currentPage = 1;
        },
    }
});

@extends('_v2.layout.dashboard')

@section('title') Home @endsection

@section('content')
    {{--    style--}}
    <style>
        .container {
            max-width: 1170px;
            margin: auto;
        }

        img {
            max-width: 100%;
        }

        .inbox_people {
            background: #ffffff none repeat scroll 0 0;
            float: left;
            overflow: hidden;
            width: 40%;
            border-right: 1.5px solid #000000;
        }

        .inbox_msg {
            border: 1px solid #c4c4c4;
            clear: both;
            overflow: hidden;
        }

        .top_spac {
            margin: 20px 0 0;
        }

        .srch_bar {
            display: inline-block;
            text-align: right;
            width: 60%;
            padding:
        }

        .col-md-6.right {
            text-align: right;
        }

        .headind_srch {
            padding: 10px 29px 10px 20px;
            overflow: hidden;
            background-color: #ffffff;
            border-bottom: 1px solid #ffffff;
            height: 83px;
        }

        .recent_heading h4 {
            color: #05728f;
            font-size: 21px;
            margin: auto;
        }

        .srch_bar input {
            border: 1px solid #cdcdcd;
            border-width: 0 0 1px 0;
            width: 80%;
            padding: 2px 0 4px 6px;
            background: none;
        }

        .srch_bar .input-group-addon button {
            background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
            border: medium none;
            padding: 0;
            color: #707070;
            font-size: 18px;
        }

        .srch_bar .input-group-addon {
            margin: 0 0 0 -27px;
        }

        .chat_ib h5 {
            color: #464646;
            margin: 0 0 0 0;
        }

        .card-body.msg {
            width: 353px !important;
            border-radius: 6px;
            padding: 0.7rem;
            margin: 10px 0 0 0;
        }

        .chat_ib h5 span {
            font-size: 13px;
            float: right;
        }

        .chat_ib p {
            font-size: 14px;
            color: #989898;
            margin: auto
        }

        .chat_img {
            float: left;
            width: 11%;
        }

        .chat_ib {
            float: left;
            padding: 0 0 0 15px;
            width: 88%;
        }

        .chat_people {
            overflow: hidden;
            clear: both;
        }

        .chat_list {
            margin: 0;
        }

        .inbox_chat {
            height: 550px;
            /*overflow-y: scroll;*/
            background-color: #f5f9ff;
        }

        .active_chat {

        }

        .incoming_msg_img {
            display: inline-block;
            width: 100%;
        }
        .date-name h3 {
            font-size: 14px;
        }
        .incoming_msg_img {
            margin-top: 20px;
        }
        .user-pic img {
            height: 50px;
            float: left;
        }
        .received_msg {
            display: inline-block;
            padding: 0 0 0 10px;
            vertical-align: top;
            width: 100%;
        }

        .received_withd_msg p {
            background: #fff none repeat scroll 0 0;
            border-radius: 3px;
            color: #646464;
            font-size: 14px;
            margin: 0;
            padding: 5px 10px 5px 12px;
            width: 100%;
        }

        .time_date {
            color: #dadcdf;
            display: block;
            font-size: 12px;
            margin: 8px 0 0;
        }

        .received_withd_msg {
            width: 57%;
        }


        .sent_msg p {
            background: #0c97ce none repeat scroll 0 0;
            border-radius: 3px;
            font-size: 14px;
            margin: 10px;
            color: #fff;
            padding: 15px 10px 15px 12px;
            width: 100%;
        }

        .outgoing_msg {
            overflow: hidden;
            margin: 26px 0 26px;
        }

        .sent_msg {
            float: right;
            width: 46%;
        }
        .input_msg_write input {
            background: #fff;
            border: medium none;
            color: #4c4c4c;
            font-size: 15px;
            min-height: 48px;
            width: 100%;
            border: none !important;
        }

        .type_msg {
            /*border-top: 1px solid #c4c4c4;*/
            position: relative;
        }

        .msg_send_btn {
            background: #05728f none repeat scroll 0 0;
            border: medium none;
            border-radius: 50%;
            color: #fff;
            cursor: pointer;
            font-size: 17px;
            height: 33px;
            position: absolute;
            right: 0;
            top: 11px;
            width: 33px;
        }

        .messaging {
            padding: 0 0 50px 0;
        }

        .msg_history {
            height: 516px;
            overflow-y: auto;
        }

        .mesgs {
            float: left;
            /* padding: 30px 15px 0 25px; */
            /* width: 60%; */
            background-color: #f6f9ff;
        }

        .button {
            border: 1px solid #000000;
            background-color: #ffffff;
            padding: 10px 20px 10px 20px;
        }

        .img {
            max-width: 60% !important;
        }

        .live {
            color: #16137d !important;
            font-weight: bold;
        }

        .footer {
            background-color: #f5f9ff;
        }
        .text{
            color: #ffffff!important;
        }
        .received_withd_msg p {
            color: #fff !important;
        }
        div#live-chat h4 {
            color: #000;
        }
        .received_withd_msg p {
            color: #000 !important;
        }
        i.fa.fa-search {
            position: absolute;
            top: 115px;
            left: 27px;
        }
    </style>
    {{--style end--}}

    <div id="live-chat">
        <div>
            <div class="messaging">
                <div class="inbox_msg">
                    <div class="col-md-3 inbox_people p-0">
                        <div class="headind_srch">
                            <div class="recent_heading">
                                <h2 class="live mt-2">Live Chat</h2>
                            </div>
                        </div>

                        <div class="inbox_chat">
                            <div class="card-body">
                                                             <i class="fa fa-search"></i>
                                <input type="text" class="form-control" id="search-input" placeholder="Search Message">
                            </div>
                            <div class="chat_ib">
                                <div class="card-body msg bg-white">
                                    <h4 class="text-uppercase"><b>Max Robinson</b></h4>
                                    <h5>My Pet Limited</h5>
                                </div>
                            </div>
                            <br>
                            <div class="chat_ib">
                                <div class="card-body msg bg-white">
                                    <h4 class="text-uppercase"><b>Max Robinson</b></h4>
                                    <h5>My Pet Limited</h5>
                                </div>
                            </div>
                            <div class="chat_ib">
                                <div class="card-body msg bg-white">
                                    <h4 class="text-uppercase"><b>Max Robinson</b></h4>
                                    <h5>My Pet Limited</h5>
                                </div>
                            </div>
                            <br>
                            <div class="chat_ib">
                                <div class="card-body msg bg-white">
                                    <h4 class="text-uppercase"><b>Max Robinson</b></h4>
                                    <h5>My Pet Limited</h5>
                                </div>
                            </div>
                        </div>

                        <div class="card-body footer">
                            <button class="button my-2 my-sm-0 text-uppercase ml-5" type="submit">View Completed Chats
                            </button>
                        </div>


                    </div>

                    <div class="mesgs col-md-9 p-0">
                        <div class="headind_srch m-0 ">
                            <div class="recent_heading row">
                                <div class="chat_list col-md-6">
                                    <div class="chat_people">
                                        <div class="chat_img"><img
                                                src="https://ptetutorials.com/images/user-profile.png" alt="sunil">
                                        </div>
                                        <div class="chat_ib p-2">
                                            <h5>Fiona Putri</h5>
                                            <p>Stupid Company Limited</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 right mt-2">
                                    <button class="button my-2 my-sm-0 text-uppercase" type="submit">End Chat & Mark as
                                        Completed
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="msg_history p-4">

                            <div class="incoming_msg">
                                <div class="incoming_msg_img"><img
                                        src="https://ptetutorials.com/images/user-profile.png" alt="sunil"></div>
                                <div class="received_msg">
                                    <div class="received_withd_msg">
                                        <p>Test which is a new approach to have all
                                            solutions</p>
                                        <span class="time_date"> 11:01 AM    |    June 9</span></div>
                                </div>
                            </div>
                            <div class="outgoing_msg">
                                <div class="sent_msg">
                                    <p>Test which is a new approach to have all
                                        solutions</p>
                                    <span class="time_date"> 11:01 AM    |    June 9</span></div>
                            </div>
                            <div class="incoming_msg">
                                <div class="incoming_msg_img"><img
                                        src="https://ptetutorials.com/images/user-profile.png" alt="sunil"></div>
                                <div class="received_msg">
                                    <div class="received_withd_msg">
                                        <p>Test, which is a new approach to have</p>
                                        <span class="time_date"> 11:01 AM    |    Yesterday</span></div>
                                </div>
                            </div>
                            <div class="outgoing_msg">
                                <div class="sent_msg">
                                    <p>Apollo University, Delhi, India Test</p>
                                    <span class="time_date"> 11:01 AM    |    Today</span></div>
                            </div>
                            <div class="incoming_msg">

                                <div class="received_msg">
                                    <div class="received_withd_msg">
                                        <p>We work directly with our designers and suppliers,
                                            and sell direct to you, which means quality, exclusive
                                            products, at a price anyone can afford.</p>

                                </div>
                                <div class="incoming_msg_img">
                                    <div class="user-pic">
                                        <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil">
                                    </div>
                                    <div class="date-name">
                                        <h3>Name sadf as </h3>
                                        <span class="time_date"> 11:01 AM    |    Today</span>
                                    </div>
                                 </div>
                                </div>
                            </div>
                        </div>
                        <div class="type_msg">
                            <div class="input_msg_write">
                                <input type="text" class="write_msg" placeholder="Type a message"/>
                                <button class="msg_send_btn" type="button"><i class="fa fa-paper-plane-o"
                                                                              aria-hidden="true"></i></button>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        {{--        <div class="card">--}}
        {{--            <div class="row col-md-12 p-0" style="background-color: #f6f9ff!important;">--}}

        {{--                <div class="col-md-3 p-0">--}}
        {{--                        <div class="header" style="background-color: #ffffff!important; height: 50px;">--}}
        {{--                            Live Chat--}}
        {{--                        </div>--}}
        {{--                        <div class="body">--}}
        {{--                            <div class="card-body mt-5">--}}
        {{--                                <i data-feather="search" class="ml-3"></i>--}}
        {{--                                <input type="text" class="form-control" id="search-input" placeholder="Search Message">--}}
        {{--                            </div>--}}
        {{--                        </div>--}}
        {{--                        <div class="footer text-muted">--}}
        {{--                            <p class="text-uppercase">View Completed Chats</p>--}}
        {{--                        </div>--}}
        {{--                </div>--}}

        {{--                <div class="col-md-9 p-0">--}}
        {{--                    <div class="right">--}}
        {{--                        <div class="" style="background-color: #ffffff!important;">--}}
        {{--                            Featured--}}
        {{--                        </div>--}}
        {{--                        <div class="" >--}}
        {{--                            <h5 class="">Special title treatment</h5>--}}
        {{--                            <p class="">With supporting text below as a natural lead-in to additional content.</p>--}}
        {{--                            <a href="#" class="btn btn-primary">Go somewhere</a>--}}
        {{--                        </div>--}}
        {{--                        <div class=" text-muted" style="background-color: #ffffff!important;">--}}
        {{--                            2 days ago--}}
        {{--                        </div>--}}
        {{--                    </div>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--        </div>--}}
    </div>
@endsection

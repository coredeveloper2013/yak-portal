@extends('_v2.layout.dashboard')

@section('title') Live Chat @endsection

@section('layout') container-fluid @endsection

@section('content')

    <div class="livechat">
        <div class="people">
            <div class="search-form">
                <input type="text" placeholder="Search People" class="form-control">
            </div>

            <div class="chat-list">
                <div class="chat-person">
                    <img src="{{ asset('images/user.jpg') }}" alt="">
                    <div class="info">
                        <p class="activity">23:00</p>
                        <h3 class="name">Md Shahinur Rahman</h3>
                        <p class="description">Description</p>
                    </div>
                </div>

                <div class="chat-person">
                    <img src="{{ asset('images/users/user-4.jpg') }}" alt="">
                    <div class="info">
                        <p class="activity">Last scene weeks ago</p>
                        <h3 class="name">Rifat ul alom</h3>
                        <p class="description">Description</p>
                    </div>
                </div>

                <div class="chat-person">
                    <img src="{{ asset('images/users/user-3.jpg') }}" alt="">
                    <div class="info">
                        <p class="activity">Rubel Hassan</p>
                        <h3 class="name">Rubel Hassan</h3>
                        <p class="description">Description</p>
                    </div>
                </div>

                <div class="chat-person">
                    <img src="{{ asset('images/preson.png') }}" alt="">
                    <div class="info">
                        <p class="activity">01:15</p>
                        <h3 class="name">Ekramul Haque Sumon</h3>
                        <p class="description">Description</p>
                    </div>
                </div>

                <div class="chat-person">
                    <img src="{{ asset('images/users/user-1.jpg') }}" alt="">
                    <div class="info">
                        <p class="activity">Last scene years ago</p>
                        <h3 class="name">Peays Das</h3>
                        <p class="description">Description</p>
                    </div>
                </div>

                <div class="chat-person">
                    <img src="{{ asset('images/user.jpg') }}" alt="">
                    <div class="info">
                        <p class="activity">02:35</p>
                        <h3 class="name">Afzalur Rahman Sabbir</h3>
                        <p class="description">Description</p>
                    </div>
                </div>

                <div class="chat-person">
                    <img src="{{ asset('images/user.png') }}" alt="">
                    <div class="info">
                        <p class="activity">Last scene years ago</p>
                        <h3 class="name">Sukanto Bala</h3>
                        <p class="description">Description</p>
                    </div>
                </div>

            </div>
        </div>

        <div class="messagebox">
            <div class="chat-person">
                <img src="{{ asset('images/user.jpg') }}" alt="">
                <div class="info">
                    <p class="activity">Last scene years ago</p>
                    <h3 class="name">Sukanto Bala</h3>
                    <p class="description">Description</p>
                </div>
            </div>


            <div class="chat-history">
                <ul>
                    <li class="clearfix">
                        <div class="message-data align-right">
                            <span class="message-data-time">10:10 AM, Today</span> &nbsp; &nbsp;
                            <span class="message-data-name">{{ auth()->user()->name }}</span> <i class="fa fa-circle me"></i>

                        </div>
                        <div class="message other-message float-right">
                            Hi Sukanta Bala, how are you? How is the project coming along?
                        </div>
                    </li>

                    <li>
                        <div class="message-data">
                            <span class="message-data-name"><i class="fa fa-circle online"></i> Sukanta Bala</span>
                            <span class="message-data-time">10:12 AM, Today</span>
                        </div>
                        <div class="message my-message">
                            Are we meeting today? Project has been already finished and I have results to show you.
                        </div>
                    </li>

                    <li class="clearfix">
                        <div class="message-data align-right">
                            <span class="message-data-time">10:14 AM, Today</span> &nbsp; &nbsp;
                            <span class="message-data-name">{{ auth()->user()->name }}</span> <i class="fa fa-circle me"></i>

                        </div>
                        <div class="message other-message float-right">
                            Well I am not sure. The rest of the team is not here yet. Maybe in an hour or so? Have you faced any problems at the last
                            phase of the project?
                        </div>
                    </li>

                    <li>
                        <div class="message-data">
                            <span class="message-data-name"><i class="fa fa-circle online"></i> Sukanta Bala</span>
                            <span class="message-data-time">10:20 AM, Today</span>
                        </div>
                        <div class="message my-message">
                            Actually everything was fine. I'm very excited to show this to our team.
                        </div>
                    </li>

                    <li class="clearfix">
                        <div class="message-data align-right">
                            <span class="message-data-time">10:22 AM, Today</span> &nbsp; &nbsp;
                            <span class="message-data-name">{{ auth()->user()->name }}</span> <i class="fa fa-circle me"></i>

                        </div>
                        <div class="message other-message float-right">
                            Well I am not sure. The rest of the team is not here yet. Maybe in an hour or so? Have you faced any problems at the last
                            phase of the project?
                        </div>
                    </li>

                    <li>
                        <div class="message-data">
                            <span class="message-data-name"><i class="fa fa-circle online"></i> Sukanta Bala</span>
                            <span class="message-data-time">10:31 AM, Today</span>
                        </div>
                        <i class="fa fa-circle online"></i>
                        <i class="fa fa-circle online" style="color: #AED2A6"></i>
                        <i class="fa fa-circle online" style="color:#DAE9DA"></i>
                    </li>

                </ul>

            </div>

            <div class="send-box">


                <form action="#">
                    <textarea name="" id="" cols="30" rows="3" class="form-control"></textarea>
                    <button id="sendBtn" class="btn btn-outline-primary rounded-pill"><i class="far fa-paper-plane"></i></button>
                </form>
            </div>
        </div>
    </div>
@endsection

@extends('_v2.layout.dashboard')
@section('title', 'Leads')
@section('top-bar-title',"Leads")
@section('content')
    {{-- <div class="form-group mt-5">
        <input type="text" id="name" name="email" class="form-control" required>
        <label class="form-control-placeholder" for="name">Name</label>
        <i class="fas fa-at"></i>
    </div> --}}
    <div id="dashboard">
        <div class="row justify-content-between page-header">
            <div class="col-md-6">
                <h1 class="main-title">Leads</h1>
            </div>
        </div>
        <form action="{{ route('_v2.customer.leads.index') }}" method="get">
            <div class="form-row filter-bar">
                <div class="col-md-2">
                    <button class="btn btn-primary btn-block" data-target="#create-lead" data-toggle="modal"
                            type="button">+ New Lead
                    </button>
                </div>

                <div class="col-md-2">
                    <div class="dropdown">
                        <button class="btn btn-white btn-block bg-white dropdown-toggle" type="button"
                                id="dropdownMenuButton" data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false">
                            Sort By : <span class="text-primary font-weight-bolder" id="ordering">{{ request()->sort == 'asc' ? 'Ascending' : 'Descending' }}</span>
                        </button>
                        <input type="hidden" name="sort" id="sort" value="desc">
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" id="dropdownMenuButton">
                            <a class="dropdown-item" href="#" data-value="asc">Ascending</a>
                            <a class="dropdown-item" href="#" data-value="desc">Descending</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="search-form">
                        <input type="text" placeholder="Search" class="form-control" name="search" value="{{ Request::get('search') }}">
                        <i data-feather="search" class="ml-3"></i>
                    </div>

                </div>

                <div class="col-md-4">
                    <button class="btn btn-outline-primary bg-white text-primary float-right pl-5 pr-5" type="submit">
                        <i class="fas fa-sliders-h fa-rotate-90"></i>
                        Filter
                    </button>
                </div>
            </div>
        </form>
        @if($errors->any())
            <div class="row">
                <div class="alert alert-danger text-danger alert-dismissible fade show w-100" role="alert">
                    {!! '<ul class="mb-0">'.implode('', $errors->all('<li>:message</li>')).'</ul>' !!}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        @endif
        <div class="row mb-3">
            <div class="col-md-12 bg-white rounded table-responsive">
                <table class="table p-4 table-borderless table-rounded my-3">
                    <thead>
                        <tr>
                            <th scope="col">Date Created</th>
                            <th scope="col">Name</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Email</th>
                            <th scope="col" class="text-center">Status</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    @if($leads->total() > 0)
                    <tbody class="">
                    @foreach($leads as $lead)
                        <tr>
                            <td> {{ $lead->created_at ? date_format($lead->created_at,"M d, Y") : ''}}</td>
                            <td scope="row" class="text-capitalize">{{ $lead->name ? $lead->name : '' }}</td>
                            <td>{{  $lead->phone ? $lead->phone : '' }}</td>
                            <td>{{  $lead->email ? $lead->email : '' }}</td>
                           {{-- <td>{{ "GUI-". str_pad($lead->id,5,0,STR_PAD_LEFT)}}</td>--}}
                            <td class="text-center">
                                <span
                                    class="badge {{ getBadge($lead->status) }}" style="width: 150px;">{{ ucwords(str_replace('_', ' ', $lead->status)) }}</span>
                            </td>
                            <td class="text-right">
                                <button type="button"
                                        class="view_lead btn btn-outline-primary btn-small-text fixed-width-25"
                                        data-target=".view" data-id="{{ $lead->id }}" data-toggle="modal">
                                    View Lead
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    @else
                    <tbody v-else class="">
                        <tr>
                            <td colspan="100%" class="p-0 rounded-0 mt-3">
                                <h4 class="alert alert-primary d-block text-center mb-0 text-primary">No Data</h4>
                            </td>
                        </tr>
                    </tbody>
                    @endif
                </table>
                @php
                    function getBadge($status) {
                        $badge = 'badge-success';
                        switch ($status) {
                            case "untouched":
                                $badge = 'badge-primary';
                                break;
                            case "attempted":
                                $badge = 'badge-warning';
                                break;
                            case "not_interested":
                                $badge = 'badge-danger';
                                break;
                            case "deal_lost":
                                $badge = 'badge-danger';
                                break;
                            default:
                               $badge = 'badge-success';
                        }
                        return $badge;
                    }
                @endphp
                @if($leads->lastPage() > 1)
                <div class="d-flex justify-content-between">
                    <nav aria-label="Page navigation example" style="float: left">
                        <select name="" id="per-page-items" class="form-control">
                            <option
                                value="20" {{ request()->filled('items') ? (request()->items == '20' ? 'selected':''):'selected' }}>
                                20
                            </option>
                            <option value="50" {{ request()->items == '50' ? 'selected':'' }}>50</option>
                            <option value="100" {{ request()->items == '100' ? 'selected':'' }}>100</option>
                        </select>
                    </nav>
                    {{ $leads->appends(['items' => request()->items ? request()->items:20])->links() }}
                </div>
                @endif
            </div>
        </div>
        <!--Create Lead Modal -->
        <div class="modal fade bd-example-modal-lg" id="create-lead" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-primary text-white font-weight-bold">
                        <h5 class="modal-title" id="exampleModalLongTitle">Add Lead</h5>
                        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body container text-dark">
                        <form action="{{route('_v2.customer.leads.store')}}" method="post">
                            @csrf
                            <div class="question-body">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">Date</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" id="datepicker" name="lead_date">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">Name</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="lead_name">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="lead_phone" class="col-sm-2 col-form-label">Phone</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="lead_phone">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="lead_name" class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="lead_email">
                                    </div>
                                </div>
                                <div class="form-group row py-2 question-number">
                                    <label for="staticEmail" class="col-sm-2 col-form-label m-auto">Question 1</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="question[]"
                                               placeholder="Question">
                                        <input class="form-control mt-2" type="text" name="answer[]"
                                               placeholder="Answer">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-10 offset-2">

                                    <button type="button" class="btn btn-primary" id="add_question"><i
                                            class="fas fa-plus"></i> Add Question
                                    </button>
                                </div>
                            </div>

                            <div class="modal-footer">
                              {{--  <button type="reset" class="btn btn-light">Reset</button>--}}
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal End -->

        <!--View Lead Modal -->
        <div class="modal fade bd-example-modal-lg view" id="lead-view" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-primary text-white font-weight-bold">
                        <h5 class="modal-title" id="exampleModalLongTitle">View Lead</h5>
                        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{ route('_v2.customer.lead.update') }}" method="post">
                        @csrf
                    <div class="modal-body">
                        <div class="container">
                        <div class="form-group row">
                            <label for="lead_name" class="col-sm-3 col-form-label">Name</label>
                            <label for="lead_name" class="col-sm-9 col-form-label" id="lead_name"></label>
                        </div>
                        <div class="form-group row">
                            <label for="lead_phone" class="col-sm-3 col-form-label">Phone</label>
                            <label for="lead_phone" class="col-sm-9 col-form-label" id="lead_phone"></label>
                        </div>
                            <div class="form-group row">
                                <label for="lead_email" class="col-sm-3 col-form-label">Email</label>
                                <label for="lead_email" class="col-sm-9 col-form-label" id="lead_email"></label>
                            </div>
                        <div class="form-group row">
                            <label for="status" class="col-sm-3 col-form-label">Status</label>
                            <select class="form-control col-sm-9" id="status" name="status">
                                <option>Select status</option>
                                <option value="untouched">Untouched</option>
                                <option value="attempted">Attempted</option>
                                <option value="information_sent">Information Sent</option>
                                <option value="appointment_scheduled">Appointment Scheduled</option>
                                <option value="quoted">Quoted</option>
                                <option value="sold">Sold</option>
                                <option value="not_interested">Not Interested</option>
                                <option value="deal_lost">Deal lost</option>
                            </select>
                        </div>
                            <div class="form-group row">
                                <input type="hidden" id="id" name="lead_id">
                                <label for="lead_comment" class="col-sm-3 col-form-label">Comments</label>
                                <textarea class="col-sm-9 form-control" id="lead_comment" name="message"></textarea>
                                {{--<input class="col-sm-9 form-control" type="text" id="comment_datepicker" name="comment_date">--}}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-target="#edit-lead-modal" data-toggle="modal" id="edit-button">Edit</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Modal End -->
        <!--Edit Lead Modal -->
        <div class="modal fade bd-example-modal-lg" id="edit-lead-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-primary text-white font-weight-bold">
                        <h5 class="modal-title" id="exampleModalLongTitle">Edit Lead</h5>
                        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body container text-dark">
                        <form action="{{ route('_v2.customer.lead.update') }}" method="post">
                            @csrf
                            <input type="hidden" id="lead_id" name="lead_id">
                            <input type="text" hidden name="edit" value="1">
                            <div class="edit-question-body">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">Date</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" id="edit_datepicker"  name="lead_date">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-2 col-form-label" id="lead_name">Name</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="lead_name" id="edit_lead_name">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="lead_phone" class="col-sm-2 col-form-label">Phone</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="lead_phone" id="edit_lead_phone">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="lead_name" class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="lead_email" id="edit_lead_email">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-10 offset-2">

                                    <button type="button" class="btn btn-primary" id="add_edit_question"><i
                                            class="fas fa-plus"></i> Add Question
                                    </button>
                                </div>
                            </div>

                            <div class="modal-footer">
                                {{--  <button type="reset" class="btn btn-light">Reset</button>--}}
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal End -->
    </div>


@endsection
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
@section('scripts')

    <script>
        $(document).ready(function () {
            let data;
            let question;
            let id = '', url = "{{route('_v2.customer.leads.show', '')}}", paginationUrl = '';
            $('#campaign_name').text('');
            $('#user_phone').text('');
            $('#user_email').text('');
            $('#status').val('');

            $('.view_lead').click(function () {
                id = $(this).data('id');
                let userType;
                let date
                $('#id').val(id)
                $('#lead_id').val(id)
                $.ajax({
                    url: `${url}/${id}`,
                    success: function (result) {
                        console.log(result)
                        $('#lead_name').text(result.name);
                        $('#lead_phone').text(result.phone);
                        $('#lead_email').text(result.email);
                        $('#status').val(result.status);
                        date = moment(result.updated_at).format('D MMMM, YYYY');
                        if(result.updated_at==null) date = moment(result.created_at).format('D MMMM, YYYY');
                        $('#comment_datepicker').val(date);
                        data = result;
                        userType = result.user.type;
                        question = result.lead_deal_question;

                        if(userType!=='staff'){
                            $('#edit-button').hide();
                        }
                    }
                });
            });


            $('#per-page-items').on('change', function () {
                let items = $(this).val();
                let page = "{{request()->page}}";
                //paginationUrl = `${url}?items=${items}&page=${page}`;
                paginationUrl = `${url}?items=${items}&page=1`;

                window.location = paginationUrl;
            });

            $('#dropdownMenuButton > a').click(function () {
                $('#sort').val($(this).data('value'));
                $('#ordering').text($(this).text());
            });

            $('#add_question').click(function () {
                let questionNumber = parseInt($('.question-body').find('.question-number').length) + 1;

                let newQuestion = `<div class="form-group row py-2 question-number text-dark" >
                                <label for="question" class="col-sm-2 col-form-label m-auto">Question  <span class="count">${questionNumber}</span></label>
                                     <div class="col-sm-10">
                                    <input class="form-control" type="text" name="question[]" placeholder="Question">
                                    <input class="form-control mt-2" type="text" name="answer[]" placeholder="Answer">
                                </div>
                            </div>`
                $('.question-body').append(newQuestion);
            });

            $.fn.datepicker.defaults.format = "dd MM yyyy";
            $('#datepicker').datepicker().val(moment(new Date()).format('D MMM YYYY'))
            $('#comment_datepicker').datepicker().val(moment(new Date()).format('D MMM YYYY'))
            $('#edit_datepicker').datepicker().val()

            $('#edit-button').click(function (){
                $('#lead-view').modal('toggle');
                console.log(data);
                let date = moment(data.created_at).format('D-MMM-YYYY');
                console.log(date);

                $('#edit_datepicker').val(date);
                $('#edit_lead_name').val(data.name);
                $('#edit_lead_phone').val(data.phone);
                $('#edit_lead_email').val(data.email);

                $.each(question, function(i, item) {

                    $('.edit-question-body').append(
                        `<div class="form-group row py-2 edit-question-number text-dark" >
                                <label for="question" class="col-sm-2 col-form-label m-auto">Question  <span class="count">${i+1}</span></label>
                                     <div class="col-sm-10">
                                    <input class="form-control" type="text" name="question[]" placeholder="Question" value="${item.question}">
                                    <input class="form-control mt-2" type="text" name="answer[]" placeholder="Answer" value="${item.answer}">
                                </div>
                            </div>`)

                });
                $('#add_edit_question').click(function (){
                    let questionNumber = parseInt($('.edit-question-body').find('.edit-question-number').length) + 1;

                    $('.edit-question-body').append(
                        `<div class="form-group row py-2 edit-question-number text-dark" >
                                <label for="question" class="col-sm-2 col-form-label m-auto">Question  <span class="count">${questionNumber}</span></label>
                                     <div class="col-sm-10">
                                    <input class="form-control" type="text" name="question[]" placeholder="Question">
                                    <input class="form-control mt-2" type="text" name="answer[]" placeholder="Answer">
                                </div>
                            </div>`)
                });
            })
        });
    </script>
@endsection

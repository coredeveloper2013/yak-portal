@extends('layouts.app')

@section('content')
<style>
        .tble-agreement thead tr th {
            background-color: #fff !important;
        }

    </style>
    <!-- Page Content-->
    <div class="page-content" id="agreements">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="float-right">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Main</a></li>
                                <li class="breadcrumb-item active">Agreements</li>
                            </ol>
                        </div>
                    </div>
                    <!--end page-title-box-->
                </div>
                <!--end col-->
            </div>
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 mx-auto">

                            <div class="form-group filter-f dropdown-arrow float-left mr-3">
                                <input type="text" name="daterange" id="daterange" placeholder="Select date" class="form-control" />
                            </div>


                            <div class="form-group filter-c float-left">
                                <select class="form form-control form-control-sm" v-model="filter.status" @change="getUserAgreements(false)">
                                    <option value="" disabled>Select Status</option>
                                    <option value="completed">Completed</option>
                                    <option value="pending">Pending</option>
                                </select>
                            </div>
                            <div class="form-group reset-c float-left m-0">

                                <button type="button" class="btn" @click="resetFilter"><i class="fas fa-undo"></i></button>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">


                            <div class="table-responsive">
                                <template>
                                    <div>
                                        <b-table responsive :items="agreements" :fields="fields" :busy="isBusy"
                                                 show-empty class="td-block text-left tble-agreement" @filtered="Filtered">
                                            <template v-slot:emptyfiltered="scope">
                                                <h4>@{{ scope.emptyFilteredText }}</h4>
                                            </template>
                                            <template v-slot:table-busy>
                                                <div class="text-center text-danger my-2">
                                                    <b-spinner class="align-middle"></b-spinner>
                                                    <strong>Loading...</strong>
                                                </div>
                                            </template>
                                            <template slot="HEAD_name" slot-scope="data">
                                                <span class="m-0 pl-2">@{{data.label}}</span>
                                            </template>
                                            <template v-slot:cell(id)="data">
                                                    #@{{data.item.id}}
                                            </template>
                                            <template v-slot:cell(name)="data">
                                                    @{{data.item.user.name}}
                                            </template>
                                            <template v-slot:cell(email)="data">
                                                    @{{data.item.user.email}}
                                            </template>
                                            <template v-slot:cell(campaign)="data">
                                                    <span class="text-danger" v-if="data.item.campaign.deleted_at !== null">
                                                        @{{data.item.campaign.name}}
                                                    </span>
                                                    <span v-else>
                                                        @{{data.item.campaign.name}}
                                                    </span>
                                            </template>
                                            <template v-slot:cell(landing_page)="data">
                                                    @{{data.item.campaign.landing_page_name}}
                                            </template>
                                            <template v-slot:cell(docu_status)="data">
                                                <span class="mt-0 badge badge-boxed  badge-soft-success tm-2"
                                                       v-if="data.item.document.status == 'signed'">
                                                     @{{'Signed'}}
                                                </span>
                                                <span class="mt-0 badge badge-boxed  badge-soft-warning tm-2"
                                                       v-else-if="data.item.document.status == 'send'">
                                                     @{{'Sent'}}
                                                </span>
                                                <span class="mt-0 badge badge-boxed  badge-soft-danger tm-2" v-else>
                                                    @{{data.item.document.status}}
                                                </span>
                                            </template>
                                        </b-table>
                                        {{-- Row --}}
                                        <div class="row  m-auto">
                                            <div class="col-xl-6 col-lg-6 col-md-6 float-left">
                                                <p class="mb-0 p-1 small mt-1 float-left">Showing @{{
                                                    agreementsMeta.from }} to @{{ agreementsMeta.to }} of @{{
                                                    agreementsMeta.total }} records | Per Page: @{{
                                                    agreementsMeta.per_page }} | Current Page: @{{
                                                    agreementsMeta.current_page }}</p>
                                            </div>
                                            <div class="col-xl-6 col-lg-6 col-md-6 float-right t012 pager-resp">
                                                <b-pagination class=" agreement-m-resp float-right" size="sm"
                                                              :total-rows="agreementsMeta.total"
                                                              v-model="agreementsMeta.current_page"
                                                              :per-page="agreementsMeta.per_page" first-text="First"
                                                              prev-text="Previous" next-text="Next" last-text="Last"
                                                              ellipsis-text="More" variant="danger">
                                                </b-pagination>
                                            </div>
                                        </div>
                                        {{-- End Row --}}
                                    </div>
                                </template>
                            </div>
                            <!--end /tableresponsive-->
                        </div>
                        <!--end card-body-->
                    </div>
                    <!--end card-->
                </div>
            </div>


        </div>
        <!-- container -->
    @include('layouts.footer')
    <!--end footer-->
    </div>
    <!-- end page content -->
@endsection
@push('js')
    <script>
        var main_url = '{!! url('/') !!}';
        var App_url = '{!! url()->current() !!}';
    </script>
    {{--  Inject vue js files  --}}
    <script src="{{ asset('js/app-vue.js') }}"></script>
    <script src="{{ asset('js/agreement/vue.js') }}"></script>
@endpush

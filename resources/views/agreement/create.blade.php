@extends('layouts.app')

@section('css')
    <style>
        .invalid-feedback {
            display: block !important;
        }
    </style>

@endsection

@section('content')


    <!-- Page Content-->
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="wlcm-panel">
                        <div class="welcome-text text-center">
                            <p class="text-muted">Whether you are in individual or a Fortune 500 <br> enterprise, there
                                is a plan there is a plan that's right for you

                            </p>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xl-8 col-lg-10 col-md-10 mx-auto mx-flex">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" action="{{route('agreements.add')}}">
                                @csrf
                                <div class="row">
                                    <div class="col-xl-6">
                                        <div class="form-group">
                                            <label>Agreement Name<span class="req">*</span></label>
                                            <input type="text" name="name" class="form-control" value="{{old('name')}}">
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-xl-6">
                                        <div class="form-group">
                                            <label>Date</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="date" id="date" value="{{old('date')}}" placeholder="Select Date">
                                                @error('date')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i
                                                            class="dripicons-calendar"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xl-6">
                                        <div class="form-group">
                                            <label>Amount<span class="req">*</span></label>
                                            <input type="number" name="amount" class="form-control"
                                                   value="{{old('amount')}}" min="1">
                                            @error('amount')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-xl-6">
                                        <div class="form-group">
                                            <label>Status</label>
                                            <select class="form-control" name="status">
                                                <option disabled="" selected="">Select status</option>
                                                <option value="completed" {{ (old('status') === "completed" ? "selected" : "") }}>Completed</option>
                                                <option value="pending" {{ (old('status') === "pending" ? "selected" : "") }}>Pending</option>
                                            </select>
                                            @error('status')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-xl-12">
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-primary float-right"
                                                   value="Create Agreement">
                                        </div>
                                    </div>

                                </div>

                            </form>

                        </div>
                    </div>
                </div>
            </div>

            <!--end col-->
        </div>
        <!-- container -->
    @include('layouts.footer')
    <!--end footer-->
    </div>

    <!-- end page content -->
@endsection



@push('js')
    <script>
        $(function() {
            $('input[name="date"]').daterangepicker({
                opens: 'left',
                singleDatePicker: true,
                showDropdowns: true,
                minDate: moment(),
            }, function(start, end, label) {
                $("#date").val(start.format('MM/DD/YYYY'));
            });
        });
    </script>
@endpush

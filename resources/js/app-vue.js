window.Vue = require('vue');

import BootstrapVue from 'bootstrap-vue';
import Multiselect from "vue-multiselect";


Vue.use(BootstrapVue);
Vue.use(Multiselect);

Vue.component('subscription' , require('./components/SubscriptionManagement.vue').default);
Vue.component('staff_subscription' , require('./components/staff/SubscriptionManagement.vue').default);
Vue.component('chart' , require('./components/ChartComponent.vue').default);

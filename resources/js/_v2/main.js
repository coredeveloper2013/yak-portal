window.Vue = require('vue');
require('./bootstrap');
import swal from 'sweetalert2';
import toastr from 'toastr';

import VoerroTagsInput from '@voerro/vue-tagsinput';
import Vuelidate from 'vuelidate';
Vue.use(Vuelidate);

window.Swal = swal;
window.toastr = toastr;

window.Vue = require('vue');

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
// import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

require('./components/component-register');
require('./filter');

Vue.config.productionTip = false;
Vue.component('tags-input', VoerroTagsInput);

const app = new Vue({
    el: '#app',
});



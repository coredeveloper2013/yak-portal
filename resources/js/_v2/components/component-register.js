window.Vue = require('vue');

//Partials
Vue.component('pagination', require('laravel-vue-pagination'));

//Pages
Vue.component('staff-home-index', require('./Staff/Home/Index').default);
Vue.component('staff-holiday-index', require('./Staff/Holiday/Index').default);
Vue.component('staff-client-index', require('./Staff/Client/Index').default);
Vue.component('staff-client-details', require('./Staff/Client/Details').default);
Vue.component('staff-client-logs', require('./Staff/Client/Logs').default);
Vue.component('staff-client-edit', require('./Staff/Client/Edit').default);
Vue.component('staff-client-lead', require('./Staff/Client/Lead').default);
Vue.component('staff-client-billing', require('./Staff/Client/Billing').default);
Vue.component('staff-client-landing-page', require('./Staff/Client/DetailsLandingPage').default);
Vue.component('staff-my-staff-index', require('./Staff/MyStaff/Index').default);
Vue.component('staff-campaign-index', require('./Staff/Campaign/Index').default);
Vue.component('staff-campaign-create', require('./Staff/Campaign/Create').default);
Vue.component('staff-billing-index', require('./Staff/Billing/Index').default);
Vue.component('staff-lead-index', require('./Staff/Lead/Index').default);
Vue.component('staff-task-index', require('./Staff/Task/Index').default);
Vue.component('staff-unbounce-index', require('./Staff/Unbounce/Index').default);
Vue.component('staff-report', require('./Staff/Report/Report').default);
Vue.component('staff-agreements-index', require('./Staff/Agreements/Index').default);

Vue.component('customer-agreements-index', require('./Customer/Agreements/Index').default);
Vue.component('customer-billing-index', require('./Customer/Billing/Index').default);
Vue.component('customer-dashboard', require('./Customer/Dashboard').default);

Vue.component('staff-calender-appointment-calender', require('./Staff/Appointment/AppointmentCalender').default);

//Vue moment js to show human readable date
import moment from "moment"; //Import Moment
import _ from "lodash"; //Import Moment

//Vue Filter to make first letter Capital
Vue.filter("strToUpper", function (text) {
    return text.charAt(0).toUpperCase() + text.slice(1);
});

Vue.filter("dateFormat", function (date) {
    return moment(date).format('DD-MMM-YYYY');
});

Vue.filter("dateFormatTask", function (date) {
    return moment(date).format('Do MMMM YYYY');
});

Vue.filter("diffForHumans", function (date) {
    return moment(date).fromNow();
});

Vue.filter("replaceUnderscoreWithSpace", function (text) {
    return _.replace(text, /_/g, ' ');
});

Vue.filter("timeFormat", function(time) {
    return moment(time, "HH:mm:ss").format("hh:mm A")
});
Vue.filter("numberFormat", function (value) {
    return "£" + ' ' + parseFloat(value).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
});
Vue.filter("numberFormatWithOutCurrency", function (value) {
    return parseFloat(value).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
});
Vue.filter("pretty", function (value) {
    return JSON.stringify(JSON.parse(JSON.stringify(value)), null, 2);
});

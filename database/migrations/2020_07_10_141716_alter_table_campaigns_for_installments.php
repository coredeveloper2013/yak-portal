<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableCampaignsForInstallments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->enum('with_installment', ['Y', 'N'])->default('N')->after('campaign_specialist');
            $table->string('installment_count', 5)->nullable()->after('with_installment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->dropColumn(['with_installment', 'installment_count']);
        });
    }
}

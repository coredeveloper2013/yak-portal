<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUpdateAssignedModulesTableSettingDefaultValueOfModuleType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `assigned_modules` CHANGE `module_type` `module_type` ENUM('landing_page','other_projects') CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'landing_page'  NOT NULL;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assigned_modules', function (Blueprint $table) {
            //
        });
    }
}

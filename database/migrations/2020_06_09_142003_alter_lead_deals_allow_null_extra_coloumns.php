<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterLeadDealsAllowNullExtraColoumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lead_deals', function($table) {
            $table->string('name', 255)->nullable()->change();
            $table->string('budget', 255)->nullable()->change();
            $table->string('source', 255)->nullable()->change();
            $table->string('email', 255)->nullable()->change();
            $table->string('point_of_contact', 255)->nullable()->change();
            $table->string('phone', 255)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

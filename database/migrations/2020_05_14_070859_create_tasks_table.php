<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->comment('assigned to')->constrained()->onDelete('cascade');
            $table->string('name', 40);
            $table->text('description');
            $table->foreignId('assigned_by')->nullable()->constrained('users')->onDelete('cascade');
            $table->dateTime('due_date');
            $table->text('attachments');
            $table->enum('status', ['completed', 'pending']);
            $table->enum('priority', ['low', 'medium', 'high']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}

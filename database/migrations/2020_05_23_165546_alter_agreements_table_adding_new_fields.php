<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterAgreementsTableAddingNewFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `agreements`   
            ADD COLUMN `email` VARCHAR(255) NULL AFTER `name`,
            CHANGE `date` `date` DATE NULL,
            CHANGE `amount` `amount` DECIMAL(8,2) NULL,
            CHANGE `status` `status` ENUM('completed','pending') CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'pending'  NOT NULL;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

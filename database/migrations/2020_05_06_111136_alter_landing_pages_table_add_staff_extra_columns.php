<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterLandingPagesTableAddStaffExtraColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('landing_pages', function (Blueprint $table) {
//            $table->enum('status',
//                ['pending', 'completed', 'awaiting_build', 'business_review', 'customer_review', 'awaiting_changes'])
//                ->default('pending')->change();
            $table->decimal('budget', 8, 2)->after('date')->nullable();
            $table->unsignedBigInteger('staff_member')->nullable()->after('budget');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('landing_pages', function (Blueprint $table) {
            //
        });
    }
}

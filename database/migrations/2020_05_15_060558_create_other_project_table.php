<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOtherProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('other_projects', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->comment('assigned to customer')->constrained()->onDelete('cascade');
            $table->string('project_name', 255);
            $table->text('description');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->decimal('price', 8, 2);
            $table->decimal('budget', 8, 2);
            $table->enum('cost_type', ['one_off', 'monthly']);
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('other_projects');
    }
}

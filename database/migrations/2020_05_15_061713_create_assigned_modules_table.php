<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignedModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assigned_modules', function (Blueprint $table) {
            $table->id();
            $table->integer('module_id');
            $table->foreignId('assign_to')->comment('id of staff member')->constrained('users')->onDelete('cascade');
            $table->enum('module_type', ['landing_page', 'other_projects']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assigned_modules');
    }
}

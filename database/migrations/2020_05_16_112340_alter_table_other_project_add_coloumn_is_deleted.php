<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableOtherProjectAddColoumnIsDeleted extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('other_projects', function (Blueprint $table) {
            $table->enum('is_deleted', ['Y', 'N'])->default('N')->after('cost_type');
            $table->integer('deleted_by')->nullable()->after('updated_at');
            $table->dateTime('deleted_at')->nullable()->after('deleted_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUnbounceLink extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('unbounce_link', function (Blueprint $table) {
            $table->enum('cron_status', ['pending', 'executing', 'executed', 'already_exist'])->default('pending')->after('is_new_bussiness_lead');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('unbounce_link', function (Blueprint $table) {
            $table->dropColumn(['cron_status']);
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusToLeadDealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lead_deals', function (Blueprint $table) {
            DB::statement("ALTER TABLE `lead_deals` CHANGE `status` `status` ENUM('untouched','attempted','follow_up_schedule','pending_decision','deal_agreed','pending_signature','handed_over_in','handed_over','deal_completed','onboarding_call_booked') NOT NULL DEFAULT 'untouched'");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lead_deals', function (Blueprint $table) {
            //
        });
    }
}

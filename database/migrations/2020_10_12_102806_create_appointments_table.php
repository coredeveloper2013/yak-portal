<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->date('date')->nullable();
            $table->time('from')->nullable();
            $table->time('to')->nullable();
            $table->longText('description')->nullable();
            $table->tinyInteger('status')->default(1);

            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('member_id');

//            $table->foreign('user_id')->references('id')->on('users');
//            $table->foreign('member_id')->references('id')->on('users');


            /* $table->foreignId('member_id')->constrained('users','id')->onDelete('cascade');
             $table->foreignId('user_id')->constrained('users')->onDelete('cascade');*/

//            $table->unsignedBigInteger('member_id');

//            $table->unsignedBigInteger('user_id');
//            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

//            $table->foreignId('member_id')
//                ->constrained('users', 'id');
////
//            $table->foreignId('user_id')->constrained()->onDelete('cascade');
//            $table->foreignId('user_id')
//                ->constrained()
//                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterCampaignsTableRemoveCampaignIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->dropUnique('campaigns_slug_unique');
            $table->dropColumn('slug');
            $table->decimal('amount', 10, 2)->change()->charset(null);
            $table->enum('type', ['one_off', 'monthly'])->after('amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaigns', function (Blueprint $table) {
//            $table->string('slug')->nullable()->after('user_id');
            $table->text('amount')->change()->charset(null);
        });
    }
}

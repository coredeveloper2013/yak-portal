<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceInstallments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_installments', function (Blueprint $table) {
            $table->id();
            $table->string('user_id',255)->nullable();
            $table->string('module_id',255)->nullable();
            $table->enum('module_type', ['campaigns', 'projects'])->nullable();
            $table->decimal('amount', 8, 2)->nullable();
            $table->date('installment_date');
            $table->enum('is_sended', ['Y', 'N'])->default('N');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_installments');
    }
}

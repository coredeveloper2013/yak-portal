<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableProjectsForInstallments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('other_projects', function (Blueprint $table) {
            $table->enum('with_installment', ['Y', 'N'])->default('N')->after('other_status');
            $table->string('installment_count', 5)->nullable()->after('with_installment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('other_projects', function (Blueprint $table) {
            $table->dropColumn(['with_installment', 'installment_count']);
        });
    }
}

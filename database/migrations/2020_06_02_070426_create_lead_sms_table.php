<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeadSmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lead_sms', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('question_id');
            $table->unsignedBigInteger('lead_id');
            $table->unsignedBigInteger('alert_user');
            $table->unsignedTinyInteger('status')->default(0);

            // Manage Foreign Key Relationships

            $table->foreign('question_id')->references('id')->on('lead_questions');

            $table->foreign('alert_user')->references('id')->on('notification_alerts');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lead_sms');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterZohoDocumentListingAddingNewEnumMakrAsSigned extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `zoho_douments_listing`   
            CHANGE `status` `status` ENUM('pending','send','viewed','signed','failed','canceled','mark_as_signed') CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'pending'  NULL;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

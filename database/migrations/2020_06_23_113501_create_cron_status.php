<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCronStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cron_status', function (Blueprint $table) {
            $table->id();
            $table->string('type',255)->nullable();
            $table->string('alert_id',255)->nullable();
            $table->string('campaign_id',255)->nullable();
            $table->string('lead_id',255)->nullable();
            $table->string('module_type',255)->nullable();
            $table->string('send_on',255)->nullable();
            $table->string('send_by',255)->nullable();
            $table->text('response')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cron_status');
    }
}

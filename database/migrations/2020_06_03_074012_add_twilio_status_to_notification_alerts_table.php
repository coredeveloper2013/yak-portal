<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTwilioStatusToNotificationAlertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_alerts', function (Blueprint $table) {
            //
            $table->unsignedTinyInteger('twilio_status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notification_alerts', function (Blueprint $table) {
            //
            $table->dropColumn('twilio_status');
        });
    }
}

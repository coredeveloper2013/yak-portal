<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterCampaignsTableAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->float('compaign_amount')->default(0);
            $table->float('max_add_amount')->default(0);
            $table->string('name',255)->change();
            $table->foreignId('assigned_id')->nullable()->default(null)->constrained('users')->onDelete('cascade');
            $table->foreignId('created_by')->nullable()->default(null)->constrained('users')->onDelete('cascade');
            $table->foreignId('updated_by')->nullable()->default(null)->constrained('users')->onDelete('cascade');
        });
           
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->dropForeign(['assigned_id','created_by','updated_by']);
            $table->dropColumn(['compaign_amount','max_add_amount','assigned_id','created_by','updated_by']);
        });
    }
}

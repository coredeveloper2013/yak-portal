<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterLandingPagsTableAddNewColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('landing_pages', function (Blueprint $table) {
            $table->string('logo', 40)->after('name')->nullable();
            $table->text('trying_to_sell')->after('status')->nullable();
            $table->text('customer_benefit')->after('trying_to_sell')->nullable();
            $table->text('leads_questions')->after('customer_benefit')->nullable();
            $table->text('leads_contact_details')->after('leads_questions')->nullable();
            $table->text('review_pages')->after('leads_contact_details')->nullable();
            $table->text('reasons')->after('review_pages')->nullable();
            $table->text('reviews')->after('reasons')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('landing_pages', function (Blueprint $table) {
            //
        });
    }
}

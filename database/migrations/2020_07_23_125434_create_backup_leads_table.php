<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBackupLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('backup_leads', function (Blueprint $table) {
            $table->id();
            $table->integer('leads_id' );
            $table->integer('user_id' ) ->comment('assignee id.');
            $table->integer('campaign_id' )->nullable();
            $table->integer('change_by');
            $table->string('unbounce_id');
            $table->enum('is_newBusinessHub', ['Y', 'N'])->default('N');
            $table->timestamps();
               $table->softDeletes();
        
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('backup_leads');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeadDealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lead_deals', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users')->onDelete('cascade');
            $table->string('name', 250);
            $table->double('budget', 8, 2)->default(0.00);
            $table->string('source', 250);
            $table->enum('status', ['untouched', 'attempted', 'information_sent', 'appointment_scheduled', 'quoted', 'sold', 'not_interested', 'deal_lost'])->default('untouched');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lead_deals');
    }
}

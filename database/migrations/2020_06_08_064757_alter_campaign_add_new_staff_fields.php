<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterCampaignAddNewStaffFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaigns', function($table) {
            $table->foreignId('account_manager')->after('assigned_id')->default(NULL)->nullable()->constrained('users')->onDelete('cascade');
            $table->foreignId('lp_specialist')->after('account_manager')->default(NULL)->nullable()->constrained('users')->onDelete('cascade');
            $table->foreignId('campaign_specialist')->after('lp_specialist')->default(NULL)->nullable()->constrained('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

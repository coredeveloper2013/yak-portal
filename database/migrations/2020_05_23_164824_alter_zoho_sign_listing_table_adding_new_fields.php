<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterZohoSignListingTableAddingNewFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `zoho_douments_listing`   
            CHANGE `type` `type` ENUM('campaigns','landing_page','other_projects','agreements') CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'agreements'  NULL,
            ADD COLUMN `status` ENUM('pending','send','viewed','signed','failed','canceled') DEFAULT 'pending'  NULL AFTER `response`;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

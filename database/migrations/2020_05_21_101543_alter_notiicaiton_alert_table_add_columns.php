<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterNotiicaitonAlertTableAddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_alerts', function (Blueprint $table) {
            $table->enum('type', ['sms', 'email'])->after('send_on');
            $table->enum('status', ['completed', 'pending'])->after('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notification_alerts', function (Blueprint $table) {
            //
        });
    }
}

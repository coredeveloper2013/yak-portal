<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsChat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets_chat', function (Blueprint $table) {
            $table->id();
            $table->foreignId('ticket_id')->comment('Support Ticket Id')->constrained('support_tickets')->onDelete('cascade');
            $table->foreignId('user_id')->comment('User Id')->constrained('users')->onDelete('cascade');
            $table->string('attachment', 255)->nullable();
            $table->text('chat_text');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets_chat');
    }
}

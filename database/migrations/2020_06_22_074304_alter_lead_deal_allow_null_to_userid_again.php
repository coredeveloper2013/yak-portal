<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterLeadDealAllowNullToUseridAgain extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lead_deals', function (Blueprint $table) {
            DB::statement("ALTER TABLE `lead_deals` CHANGE `user_id` `user_id` BIGINT(20) UNSIGNED NULL;");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lead_deals', function (Blueprint $table) {
            //
        });
    }
}
